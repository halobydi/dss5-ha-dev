#!/bin/sh
##############################################################
#
# This script start the PHP application server running on
# redhat apache and php instacnce
#
##############################################################
application=$1
usage ()
{
  echo " "
  echo "./starts.sh application_name"
  echo " "
  exit 1
}
if [ -z "$application" ]; then
  echo " "
  echo "You must provide the application name to start the server "
  usage
fi

/opt/apache/servers/$1/bin/apachectl -f /opt/apache/servers/$1/conf/httpd.conf -k start

#ps -welf | grep $application
