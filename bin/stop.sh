#!/bin/sh
##############################################################
#
# This script start the PHP application server running on
# redhat apache and php instacnce
#
##############################################################
application=$1
action=$2
usage ()
{
  echo " "
  echo "./start.sh application_name start | stop | restart"
  echo " "
  exit 1
}
if [ -z "$application" ]; then
  echo " "
  echo "You must provide the application name to start the server "
  usage
fi
if [ -z "$action" ]; then
  echo " "
  echo "You must provide an action to start or stop or restart the server"
  usage
fi

/opt/apache/servers/$1/bin/apachectl -f /opt/apache/servers/$1/conf/httpd.conf -k $action

#ps -wwelf | grep $application
