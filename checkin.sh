#!/bin/bash

cd /opt/apache/servers/soteria/
tar -cf soteria_htdocs.tar htdocs
cd /vobs/soteria/staging/prod/
cleartool checkout -nc soteria_htdocs.tar
mv /opt/apache/servers/soteria/soteria_htdocs.tar soteria_htdocs.tar
cleartool checkin soteria_htdocs.tar
