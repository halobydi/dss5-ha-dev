var resizer = function() {
	var login = dojo.byId("login_container"); 	
	
	var wb = dojo.window.getBox();
	if(login){
		login.style.top = wb.h / 2 - login.offsetHeight / 2 + "px";
		login.style.left = wb.w / 2 - login.offsetWidth / 2 + "px";
	}
};

dojo.connect(window, "onresize", function() {
	resizer();
});

dojo.ready(function() {
	resizer();
});

function position(obj){
	var curLeft = 0;
	var curTop = 0;
	if (obj.offsetParent) {
		do {
			curLeft += obj.offsetLeft;
			curTop += obj.offsetTop;
		} while (obj = obj.offsetParent);
		
		return {left: curLeft, top: curTop};
	}
	
	curLeft = obj.offsetLeft;
	curTop = obj.offsetTop;
	
	return {left: curLeft, top: curTop};
}

var js_calendar;
function setup_cal(id, field) {
	if (!js_calendar) {
		js_calendar = Calendar.setup({
				animation: false,
				onSelect: function() { this.hide() }
		});
	}
	
	js_calendar.manageFields(id, field, '%m/%d/%Y');
}

dojo.registerModulePath('sot', '/mobile/src/js');

var p_count = 0;
var addPhoto = function() {
	var photo = document.createElement('div');
	photo.style.padding = '3px';
	photo.id = 'photo_' + p_count;
	
	var innerHTML =  "<span style='font-size: 10px;'>New Photo</span><br/><input type = 'hidden' name = 'pid_" + p_count + "' value = ''/>";
		innerHTML += "<input type = 'hidden' name = 'deleted_" + p_count + "' id = 'deleted_" + p_count + "' value = '0'/>";
		innerHTML += "<input style = 'border: 1px solid black; margin-right: 20px;' type = 'file' name = 'file_" + p_count + "'/><br/>";
		innerHTML += "<span style='font-size: 10px;'>Short Description of Photo</span><br/><textarea style = 'border: 1px solid black; width: 232px; height: 30px;' name = 'p_desc_" + p_count + "'></textarea>";
		innerHTML += "<span class = 'delete_a' style = 'cursor: pointer;' onclick = 'removePhoto(" + p_count + ");'>\
						<input style='' id = 'removeFile_" + p_count + "' type='button' class='btn' value='Remove'/>\
					</span>";
		
	photo.innerHTML = innerHTML;
	p_count++;

	document.getElementById("photos").appendChild(photo);
	document.getElementById("p_count").value = p_count;
};

var removePhoto = function(num, provideConfirmation) {
	if(provideConfirmation && !confirm("Are you sure you want to remove this photo?\nThis acction cannot be undone.")) {
		return;
	}
	
	try {
		document.getElementById('deleted_' + num).value = '1';
		document.getElementById('photo_' + num).style.display = 'none';
	} catch(ex) {
		
	}
};

dojo.require('sot.tools.cover');
dojo.require('sot.tools.menu');
dojo.require('sot.tools.ajax');

dojo.require('sot.home');
dojo.require('sot.swo');
dojo.require('sot.redtag');