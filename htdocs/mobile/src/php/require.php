<?php
	function errorHandler($errno, $errstr, $errfile, $errline, $errcontext){
		
	}
	set_error_handler('errorHandler');

	@require_once("mcl_Html.php");
	@require_once("mcl_Oci.php");
	@require_once("mcl_Ldap.php");
	
	mcl_Html::title('SOTeria - Safety Observation Tool');

	mcl_Html::html5(true);
	mcl_Html::no_cache(true);
	
	mcl_Html::js(mcl_Html::DOJO);
	mcl_Html::js(mcl_Html::DOJO_WINDOW);
	mcl_Html::js(mcl_Html::AJAX);
	
	require_once('/opt/apache/servers/soteria/htdocs/src/php/auth.php');

	mcl_Html::s(mcl_Html::INC_CSS, "src/css/menu.css");
	mcl_Html::s(mcl_Html::INC_CSS, "src/css/table.css");
	mcl_Html::s(mcl_Html::INC_CSS, "src/css/login.css");
	mcl_Html::s(mcl_Html::INC_CSS, "src/css/mobile.css");
	mcl_Html::s(mcl_Html::INC_CSS, "src/css/bootstrap.css");
	
	mcl_Html::s(mcl_Html::INC_JS, "src/sot.js");
	mcl_Html::s(mcl_Html::INC_JS, "src/js/plugins/jquery-1.8.3.min.js");
	
	$ua = $_SERVER['HTTP_USER_AGENT'];
	$checker = array(
	  'iphone'=>preg_match('/iPhone|iPod|iPad/', $ua),
	  'blackberry'=>preg_match('/BlackBerry/', $ua),
	  'android'=>preg_match('/Android/', $ua),
	);
	
	if($checker['blackberry'])
		header("Location: {$baseDir}/index.php");
	
	function quarter($returnDays = true){
		$tm = strtotime("now");  
		$q =  ceil(date("m", $tm) / 3);
		
		$days = date("z", strtotime("now")) - 1;
		$year = date("Y");
		
		if ($q == 1){
			$days_left = date("z", strtotime('03/31/' . $year)) - $days;
		} else if ($q == 2){
			$days_left = date("z", strtotime('06/30/' . $year)) - $days;
		} else if ($q == 3){
			$days_left = date("z", strtotime('09/30/' . $year)) - $days;
		} else if ($q == 4){
			$days_left = date("z", strtotime('12/31/' . $year)) - $days;
		}

		return $returnDays ? $days_left : $q;
	}

	//SUNDAY IS 0 (it goes 1, 2, 3, 4, 5, 6 0)
	
	$now = getDate();
	$start = mktime(0,0,0, date("m"), date("d") - (date("w") == 0 ? 7 : date("w")) - 6, date("Y"));
	$start =  date("m/d/Y", $start);

	$end = mktime(0,0,0, date("m"), date("d") - (date("w") == 0 ? 7 : date("w")), date("Y"));
	$end = date("m/d/Y", $end);
	
	$startWeekPast = $start;
	$endWeekPast = $end;
	
	$start = mktime(0,0,0, date("m"), date("d") - (date("w") == 0 ? 7 : date("w")) + 1, date("Y"));
	$start =  date("m/d/Y", $start);

	$end = mktime(0,0,0, date("m"), date("d") - (date("w") == 0 ? 7 : date("w")) + 7, date("Y"));
	$end =  date("m/d/Y", $end);
	
	$startWeek = $start;
	$endWeek = $end;
	
	$usid = $_GET["delegate"] == "true" ? $user["delegate"] : $user["usid"];
	
	if (!empty($_REQUEST["start"]) && !empty($_REQUEST["end"])){
		$start = $_REQUEST["start"];
		$end = $_REQUEST["end"];
	}
	
	if($_GET['mobile']){
		unset($_SESSION['desktop']);
	}
	
	$user = auth::check();

	if($user["status"] == "authorized"){
		if(is_array($user["delegates"])){
			foreach($user["delegates"] as $key=>$value){
				$active = 0;
				if($_GET["delegate"] == $key){
					$active = 1;
				} else {
					$active = 0;
				}
				
				mcl_Html::s(mcl_Html::SRC_JS, "
					dojo.ready(function(){
						sot.tools.menu.addDelegator({name: \"{$value["name"]}\", d_id: {$key}, active: {$active}});
					});
				");
			}
			
			mcl_Html::s(mcl_Html::SRC_JS, "
				dojo.ready(function(){
					sot.tools.menu.addDelegator({name: 'Turn Off', d_id: 0, active: 0});
				});
			");
		}
		
		$usid = (isset($_GET["delegate"]) && $_GET["delegate"] != 0 ? $user["delegates"][$_GET["delegate"]]["usid"] : $user["usid"]);
		$quarter = quarter(false);


		//Clean POST
		if (isset($_POST['username'])) {
			header("Location: " . $file);
		}
	} else {
		$err = $user["status"];
		
		echo "
			<div id='login' style='width: 100%;'/></div>
			<meta name='viewport' content='width=device-width, height=device-height'>
			<meta name='apple-mobile-web-app-status-bar-style' content='black'/>
			<div id='topHeader' style='position: relative;'>
				SOTeria
			</div>
		";
		
		$login = "
			<center>
				<form method='POST' action='index.php'>
					<div class='fancy'>
						<img src='src/img/bglock.png' height='25' width='25' style='margin-top: -4px;'>
						<span style='font-size: 16px;'>Login</span>
						<hr style='color: black; margin: 15px;'>
						Username:</br>
						<div class='input-prepend' style='width: 90.5%;'>
							<span class='add-on'><i class='icon-user'></i></span>
							<input class='span2' name='username' type='text' style='width: 80%;'>
						</div>
						Password:</br>
						<div class='input-prepend' style='width: 90.5%;'>
							<span class='add-on'><i class='icon-certificate'></i></span>
							<input class='span2' name='password' type='password' style='width: 80%;'>
						</div>
						<input class='btn btn-primary' type='submit' value='Login' style='margin-top: 10px;'/>
					</div>
				</form>
				<div class='fancy'>
					<input class='btn btn-primary' type='button' value='Create a New SWO' style='margin-top: 10px;' onclick='window.location=\"forms/epop.php?mobile=true\"'/> <br/>
					<input class='btn btn-primary' type='button' value='Submit a Near Miss' style='margin-top: 10px;' onclick='window.location=\"../forms/nearmiss.php?mobile=true\"'/>
				</div>
			</center>
		";
		//Cleanup Logout
		if (isset($_GET["logout"]) && $_GET["logout"] == "true") {
			//header("Location: " . $file);
			die($login);
		}
		
		die($login);
	}
	
	if($checker['iphone']){
		mcl_Html::s(mcl_Html::SRC_JS, "
			dojo.ready(function(){
				dojo.byId('menu').style.display = 'inline';

				resize();
				
				window.onresize = function(event) {			   
					resize();
					$('meta[name=viewport]').attr('content', 'width=' + width());
				}
				
				window.onorientationchange = function()
				{
					resize();
					$('meta[name=viewport]').attr('content', 'width=' + width());
				};
				
				$('#menuButton2').click(function(e) {
				   if(menu){
						$('#right').animate({ left: 0 }, 'fast', function() {
							menu = false;
						});
					}
					else{
						$('#right').animate({ left: 185 }, 'fast', function() {
							menu = true;
						});
					}
					e.stopPropagation();
				})
				
				if(dojo.byId('dsl0'))
					dojo.byId('dsl0').style.display = 'none';
			});
		");
	}
	else{
		mcl_Html::s(mcl_Html::SRC_JS, "
			dojo.ready(function(){
				dojo.byId('menu').style.display = 'inline';

				resize();
				
				window.onresize = function(event) {			   
					resize();
					$('#right').width(screen.width);
				}
				
				window.onorientationchange = function()
				{
					resize();
					$('#right').width(screen.width);
				};
				
				$('#menuButton2').click(function(e) {
				   if(menu){
						$('#right').animate({ left: 0 }, 'fast', function() {
							menu = false;
						});
					}
					else{
						$('#right').animate({ left: 185 }, 'fast', function() {
							menu = true;
						});
					}
					e.stopPropagation();
				})
				
				if(dojo.byId('dsl0'))
					dojo.byId('dsl0').style.display = 'none';
			});
		");
	}
	
	mcl_Html::s(mcl_Html::SRC_JS, "
		var menu = false;
		
		function hideMenu(){
			$('#right').animate({ left: 0 }, 'fast', function() {
				menu = false;
			});
		}
		
		function resize(){
			if(document.body.scrollHeight){
				var body = document.body,
					html = document.documentElement;

				var height = Math.max(body.scrollHeight, body.offsetHeight, 
									   html.clientHeight, html.scrollHeight, html.offsetHeight) + 'px';
			}
			else{
				var height = document.height + 'px';
			}
			
			dojo.byId('menu').style.height = height;
			dojo.byId('right').style.height = height;
		}
		
		function width(){
			return window.innerWidth||document.documentElement.clientWidth||document.body.clientWidth||0;
		}
	");
	
	if($_GET['obs'] == "rta")
		$rtaDisplay = true;
	else
		$epopDisplay = true;
?>
<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,minimum-scale=1,user-scalable=no"/>
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
<div id="menu">
	<ul>
		<li>
			<span class="placeHolder">
				<img src="src/img/icon_user.png" style="height: 20px;"/>
				<span style="position: relative; top: 2px;"><?= $_SESSION["name"] ?></span>
			</span>
		</li>
	</ul>
	<?php
		if(isset($user["delegates"])){
		?>
			<h3>Delegate</h3>
			<ul>
				<?php
					foreach($user["delegates"] as $key => $value){
					?>
						<li><a href="#" onClick='window.location="index.php?delegate=<?= $key ?>"'>
							<?= ($_GET["delegate"] == $key ? "<img src='src/img/white_checkmark.png' style='height: 12px; width: 10px;'/> " : "") ?>
							<?= $value["name"]; ?>
						</a></li>
					<?php
					}
				?>
				<li><?= (!isset($_GET["delegate"]) || $_GET["delegate"] == "" ? "<span class='placeHolder' style='color: gray;'>Turn Off</span>" : "<a href='index.php'>Turn Off</a>") ?></li>
			</ul>
		<?php
		}
	?>
	<h3>Observations</h3>
	<ul>
		<!--<li><a href="#">Anatomy of an Event</a></li>-->
		
		<li><a id='hpLink' href="#" onClick='window.location="../forms/hp.php?mobile=true<?= (isset($_GET["delegate"]) ? "&delegate=" . $_GET["delegate"] . "&" : "") ?>"'>Anatomy of An Event</a></li>
		<li><a id='gcLink' href="#" onClick='window.location="../forms/nearmiss.php?gc=true&mobile=true<?= (isset($_GET["delegate"]) ? "&delegate=" . $_GET["delegate"] . "&" : "") ?>"'>Good Catch</a></li>
		<li><a id='nmLink' href="#" onClick='window.location="../forms/nearmiss.php?mobile=true<?= (isset($_GET["delegate"]) ? "&delegate=" . $_GET["delegate"] . "&" : "") ?>"'>Near Miss</a></li>
		<li><a id='ppLink' href="#" onClick='window.location="forms/epop.php?<?= (isset($_GET["delegate"]) ? "delegate=" . $_GET["delegate"] . "&" : "") ?>pp"'>Paired Performance</a></li>
		<li><a id='qewLink' href="#" onClick='window.location="forms/epop.php?<?= (isset($_GET["delegate"]) ? "delegate=" . $_GET["delegate"] . "&" : "") ?>qew"'>Qualified Electrical Worker</a></li>
		<li><a id='rtaLink' href="#" onClick="sot.home.showRTA(); hideMenu(); return false;" class='<?= ($rtaDisplay ? "active" : "") ?>'>Red Tag Audit</a></li>
		<li><a id='epopLink' href="#" onClick="sot.home.showEPOP(); hideMenu(); return false;" class='<?= ($epopDisplay ? "active" : "") ?>'>Safe Worker</a></li>
		<?
			if($user["privileges"]["ACCESS_STORM_DUTY"]) echo "<li><a id='sdLink' href='#' onClick='window.location=\"../forms/stormduty.php?mobile=true".(isset($_GET["delegate"]) ? "&delegate=" . $_GET["delegate"] . "&" : "") ."\"'>Storm Duty Form</a></li>";
		?>
		<li><a id='sdLink' href="#" onClick='window.location="../forms/stormduty.php?mobile=true<?= (isset($_GET["delegate"]) ? "&delegate=" . $_GET["delegate"] . "&" : "") ?>"'>Storm Duty Form</a></li>
	</ul>
	<h3>View</h3>
	<ul>
		<li><a href="#" onClick='window.location="../../index.php?desktop=true"'>Desktop</a></li>
	</ul>
</div>

<div id="right" onClick='hideMenu();'>
	<div id="topHeader">
		SOTeria
		<div id='topHeaderButtons'>
			<input style='font-weight: bold;' type='button' class='btn btn-inverse btn-mini' value='Logout' onclick='window.location="index.php?logout=true";'>
		</div>
		<div class='btn btn-inverse btn-mini' id='menuButton2'>
			Menu
		</div>
	</div>
