<?php
require_once("mcl_Oci.php");
require_once("mcl_Ldap.php");
mcl_Ldap::session_start('SOTERIA');

class auth
{
	static $user;
	public static function check($key = false)
	{
		if (empty(self::$user)) {		
			$user = false;
			if (mcl_Ldap::authorized()) {
				$user = mcl_Ldap::lookup($_SESSION["username"]);
			} else {
				
			}
			
			if (!$user) {
				self::$user = array(
					'status' 		=> mcl_Ldap::get_error()
				);
		
			} else {
				$oci = new mcl_Oci('soteria');
				
				$sql = "
					SELECT 
						DELEGATOR, 
						E.NAME,  
						ADMIN,
						D_ID,
						ORG_CODE
					FROM USERS 
					FULL OUTER JOIN DELEGATES 
						ON USERS.USID = DELEGATES.DELEGATEE
					LEFT JOIN EMPLOYEES E
						ON E.USID = DELEGATES.DELEGATOR
					 WHERE (
						DELEGATEE = :usid
						OR USERS.USID = :usid
					)
				";
				$stmt = $oci->parse($sql);
				
				$oci->bind($stmt, array(
					"usid" => strtolower($user['username'])
				));
				
				$delegates = array();
				$org = '';
				$admin = false;
				while($row = $oci->fetch($stmt)){
					$admin = $row["ADMIN"] == 1 ? true : false;
					if(!empty($row["D_ID"]) && !empty($row["NAME"])){
						$delegates[$row["D_ID"]] = array(
							"name" => $row["NAME"],
							"usid"	=> $row["DELEGATOR"]
						);
					}
					
					$org = $row["ORG_CODE"];
				}
				
				self::$user = array(
					'status' 	=> 'authorized',
					'admin' 	=> $admin,
					'delegates' => empty($delegates) ? false : $delegates,
					'usid' 		=> strtolower($user['username']),
					'name' 		=> $user['fname'],
					'org' 		=> $org,
					'details' 	=> $user
				);
			}
		}
		
		if (isset($key) && !empty($key) && isset(self::$user[$key])) {
			return self::$user[$key];
		} else {
			return self::$user;
		}
	}
	
	public static function deny()
	{
		if (!empty(self::$user)) {
			self::$user['status'] = 'denied';
		}
	}
}

?>