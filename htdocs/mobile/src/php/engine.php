<?php
function errorHandler($errno, $errstr, $errfile, $errline, $errcontext){
	
}
set_error_handler('errorHandler');

require_once("mcl_Oci.php");
require_once('/opt/apache/servers/soteria/htdocs/src/php/auth.php');

class engine {
	
	private static $db = array();
	private static $reportsGenerated = array(); /*Username*/
	private static $lc_stmt = array();
	
	/*DATABASE CONNECTIONS*/
	private static function db($alias)
	{
		if (empty(self::$db[$alias])) {
			self::$db[$alias] = new mcl_Oci($alias);
		}
		
		return self::$db[$alias];
	}
	
	protected static function sot()
	{
		self::db("soteria")->dateFormat('MM/DD/YYYY');
		return self::db("soteria");
	}
	
	protected static function dot()
	{
		return self::db("dot");
	}
	
	static function reportsGenerated($usid){
		if(!isset(self::$reportsGenerated[$_REQUEST["usid"]])){
			$sql = "
				SELECT 	USID, 
						NAME, 
						SUPERVISOR,
						REPORTS, 
						LEADERS, 
						TEAM,
						CASE WHEN USID = '{$_REQUEST["usid"]}' THEN 1 ELSE 0 END AS ORDINAL
				FROM 	EMPLOYEES 
				WHERE 	(SUPERVISOR = '{$_REQUEST["usid"]}' OR USID = '{$_REQUEST["usid"]}')
				ORDER BY ORDINAL, NAME
			";
			
		
			while($row = self::sot()->fetch($sql)){
				$directTeam[$row["USID"]] = $row;
			}
			self::addReportsGenerated($_REQUEST["usid"], $directTeam);
		}
		
		return self::$reportsGenerated[$_REQUEST["usid"]];
	}
	public static function setReportsGenerated(){
		self::$reportsGenerated = $_SESSION["reportsGenerated"];
	}
	
	protected static function addReportsGenerated($usid, $directTeam){
		$_SESSION["reportsGenerated"][$usid] = $directTeam;
		self::$reportsGenerated[$usid] = $directTeam;
	}
	
	protected static function lcCompletion($usid, $leaderCt){
		
		//Add exclusions

		if($leaderCt == 0){
			return '--';
		}
		
		/*Added 11/2/2012, datediff was returning 2 when it should be 1*/
		$diff = @strtotime($_REQUEST["endDate"], 0) - @strtotime($_REQUEST["startDate"], 0);
		$weeks = @ceil($diff / 604800);
		
		if(!self::$lc_stmt["lc_ex"]){	
			self::$lc_stmt["lc_ex"] = self::sot()->parse("
				SELECT COUNT(USID) AS CT FROM EMPLOYEES E
					WHERE PATH LIKE :usid 
					AND LEADERS > 0
					AND EXISTS(
					 SELECT 1 FROM EXCLUSIONS EX 
						 WHERE EX.USID = E.USID 
						 AND (
							 START_DT >=  TO_DATE(:startDate, 'MM/DD/YYYY HH24:MI:SS')  OR START_DT IS NULL	
						 )
						 AND (
							END_DT <=  TO_DATE(:endDate, 'MM/DD/YYYY HH24:MI:SS')  OR END_DT IS NULL	
						 )
					)
			");
		}
		
		self::sot()->bind(self::$lc_stmt["lc_ex"], array(
			":usid"			=> "%{$usid}%",
			":startDate"	=> $_REQUEST["startDate"] . ' 00:00:00',
			":endDate"		=> $_REQUEST["endDate"] . ' 23:59:59'
		));
		
		$row = self::sot()->fetch(self::$lc_stmt["lc_ex"] );
		$leaderCt -= $row["CT"];
		if($leaderCt == 0){
			return '--';
		}
		
		$expected = $leaderCt * $weeks;
	
		if(!self::$lc_stmt["lc"]){
			self::$lc_stmt["lc"] = self::sot()->parse("
				WITH T AS (
					SELECT USID FROM EMPLOYEES E
						WHERE PATH LIKE :usid 
						AND LEADERS > 0
						AND NOT EXISTS(
						 SELECT 1 FROM EXCLUSIONS EX 
							 WHERE EX.USID = E.USID 
							 AND (
								START_DT >=  TO_DATE(:startDate, 'MM/DD/YYYY HH24:MI:SS')  OR START_DT IS NULL	
							 )
							 AND (
								END_DT <=  TO_DATE(:endDate, 'MM/DD/YYYY HH24:MI:SS')  OR END_DT IS NULL	
							 )
						)
				) 
				SELECT COUNT(*) AS CT FROM(
					SELECT OBSERVATION_CREDIT, NEXT_DAY(TRUNC(OBSERVED_DATE,'IW'), 'SUNDAY') /*Week Ending*/
					FROM T, EPOP_OBSERVATIONS O 
					WHERE T.USID = O.OBSERVATION_CREDIT
					AND OBSERVED_DATE BETWEEN TO_DATE(:startDate, 'MM/DD/YYYY HH24:MI:SS') 
						AND TO_DATE(:endDate, 'MM/DD/YYYY HH24:MI:SS') 
					AND SWO = 1
					GROUP BY OBSERVATION_CREDIT, NEXT_DAY(TRUNC(OBSERVED_DATE,'IW'), 'SUNDAY')
				)
			");
		}
		
		self::sot()->bind(self::$lc_stmt["lc"], array(
			":usid"			=> "%{$usid}%",
			":startDate"	=> $_REQUEST["startDate"] . ' 00:00:00',
			":endDate"		=> $_REQUEST["endDate"] . ' 23:59:59'
		));
		
		$row = self::sot()->fetch(self::$lc_stmt["lc"]);
		
		$actual = $row["CT"];
		$percent = @round(($actual / $expected) * 100, 2);
		
		return $percent;
	}
	
	static function printer($out){
		echo json_encode($out);
	}
	
	static function quarterDates($quarter, $year){
		if(!$year){
			$year = date("Y");
		}
	
		if($quarter == 1){
			$qstart = '01/01/' . $year;
			$qend = '03/31/' . $year;
		} else if($quarter == 2){
			$qstart = '04/01/' . $year;
			$qend = '06/30/' . $year;
		} else if($quarter == 3){
			$qstart = '07/01/' . $year;
			$qend = '09/30/' . $year;
		} else {
			$qstart = '10/01/' . $year;
			$qend = '12/31/' . $year;
		}
		
		return array("start" => $qstart, "end" => $qend);
	}
	
	public static function getDirectTeam(){
	
		$directTeam = array();
		$directTeam = self::reportsGenerated($_REQUEST["usid"]);
		
		$user = auth::check();
		$delegate = (isset($_REQUEST["delegate"]) != 0 && !empty($_REQUEST["delegate"])) ? "&delegate={$_REQUEST["delegate"]}" : "";

		$hasLeaders = $directTeam[$_REQUEST["usid"]]["LEADERS"] > 1 ? true : false;
		
		$weekly = "<div id='weeklyReport' style='display:none; font-size: 14px;'><div class='inner'>Weekly Observation Metric</div><br/>";
		
		if(!empty($directTeam)){
			foreach($directTeam as $key=>$value){
				$weekly .= "<div id='{$value["USID"]}_observedweek'><input id='{$value["USID"]}_observedweekbtn' type='button' class='btn' style='width: 80%; max-width: 500px; color: black; margin-bottom: 20px;' value='" . $value["NAME"] . "' onclick='window.location=\"forms/epop.php?usid={$value["USID"]}{$delegate}\"'><br/></div>";	
			}
		} else {
			$weekly .= "<div style='margin-bottom: 20px; font-size: 14px;'>There are no metrics to display.</div>";
		}
		
		$weekly .= "</div>";
	
		$quarterly .= "
				<div id='quarterlyReport' style='display:none;'>
					<div class='inner' style='padding-top: 5px;'>
						<div style='display: inline-block; margin-bottom: 20px;'>
							<button class='btn btn-info' style='margin-right: 5px; color: black;' onclick='sot.home.quarterOnClick(1);' id='q1box' class='" . ($_REQUEST["quarter"] == 1 ? "activeqbox" : "q1box") . "'>Q1</button>
							<button class='btn btn-success' style='margin-right: 5px; color: black;' onclick='sot.home.quarterOnClick(2);' id='q2box' class='" . ($_REQUEST["quarter"] == 2 ? "activeqbox" : "q2box") . "'>Q2</button>
							<button class='btn btn-danger' style='margin-right: 5px; color: black;' onclick='sot.home.quarterOnClick(3);' id='q3box' class='" . ($_REQUEST["quarter"] == 3 ? "activeqbox" : "q3box") . "'>Q3</button>
							<button class='btn btn-purple' style='margin-right: 20px; color: black;' onclick='sot.home.quarterOnClick(4);' id='q4box' class='" . ($_REQUEST["quarter"] == 4 ? "activeqbox" : "q4box") . "'>Q4</button>
						</div>
						<div style='display: inline-block; margin-bottom: 20px;'>
							<button class='btn' style='margin-right: 5px; color: black;' onclick='sot.home.yearToggle(-1);'><</button>
							<button class='btn btn-warning' style='margin-right: 5px; color: black;' class='qyear' id='year'>" . date("Y") ."</button>
							<button class='btn' style='margin-right: 5px; color: black;' onclick='sot.home.yearToggle(1);'>></button>
						</div>
					</div>
		";
		$quarterly .= "<div class='inner' style='font-size: 14px;'>Quarterly Observation Metric (Quarter <span id='quarter'>{$_REQUEST["quarter"]}</span>)</div><br/>";
		
		if(!empty($directTeam)){
			foreach($directTeam as $key=>$value){
				$quarterly .= "<div id='{$value["USID"]}_observedquarter'><input id='{$value["USID"]}_observedquarterbtn' type='button' class='btn' style='width: 80%; max-width: 500px; color: black; margin-bottom: 20px;' value='" . $value["NAME"] . "' onclick='window.location=\"forms/epop.php?usid={$value["USID"]}{$delegate}\"'><br/></div>";	
			}
		} else {
			$quarterly .= "<div style='margin-bottom: 20px; font-size: 14px;'>There are no metrics to display.</div>";
		}

		$quarterly .= "</div>";
		
		die("
			<div style='font-size: 14px; display: none;' id='weeklyLink'><a href='#' onClick='sot.home.showWeekly(); return false;'>Weekly</a> / <b>Quarterly</b></div>
			<div style='font-size: 14px; display: none;' id='quarterlyLink'><b>Weekly</b> / <a href='#' onClick='sot.home.showQuarterly(); return false;'>Quarterly</a></div>
			<br/>
			<div id='weekly'>{$weekly}</div>
			<div id='quarterly'>{$quarterly}</div>
			<div id='io'>{$io}</div>
		");
	}
	
	static function weeklyMetric(){
		//array("usid" => array("hasbeenobserved" => 1, "lcCompletion" => 1));
		$directTeam = self::reportsGenerated($_REQUEST["usid"]);
		
		if(empty($directTeam)){ self::printer(array()); return;}
		
		$stmt_w_o = self::sot()->parse("				
			SELECT 	MAX(SWO_ID) AS OBSERVED 
			FROM 	SWO_OBSERVATIONS
					WHERE EMPLOYEE = :usid
					AND OBSERVED_DATE BETWEEN TO_DATE(:startDate, 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE(:endDate, 'MM/DD/YYYY HH24:MI:SS')		
		");
		
		
		$stmt_w_c = self::sot()->parse("				
			SELECT 	MAX(SWO_ID) AS COMPLETED 
			FROM 	SWO_OBSERVATIONS
					WHERE OBSERVATION_CREDIT = :usid
					AND OBSERVED_DATE BETWEEN TO_DATE(:startDate, 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE(:endDate, 'MM/DD/YYYY HH24:MI:SS')		
		");
		
		foreach($directTeam as $key=>$value){
		
			self::sot()->bind($stmt_w_o, array(
				"usid" => $value["USID"],
				"startDate" => $_REQUEST["startDate"] . ' 00:00:00',
				"endDate" => $_REQUEST["endDate"] . ' 23:59:59'
			));
			
			$row = self::sot()->fetch($stmt_w_o, false);
			$out[$value["USID"]]["observed"] = $row["OBSERVED"];
			
			self::sot()->bind($stmt_w_c, array(
				"usid" => $value["USID"],
				"startDate" => $_REQUEST["startDate"] . ' 00:00:00',
				"endDate" => $_REQUEST["endDate"] . ' 23:59:59'
			));		
			$row = self::sot()->fetch($stmt_w_c, false);
			$out[$value["USID"]]["completed"] = empty($row["COMPLETED"]) ? ($value["LEADERS"] == 0 ? "--" : "") : $row["COMPLETED"];
		
			$out[$value["USID"]]["lc"] = self::lcCompletion($value["USID"], $directTeam[$value["USID"]]["LEADERS"]);
			
		}
		
		//print_r(self::sot()->error());
		$stmt_io = self::sot()->parse("
			WITH T AS (
				SELECT USID FROM EMPLOYEES WHERE PATH LIKE :usid
			)
			SELECT I.ITEM_CATEGORY, I.ITEM_TEXT, COUNT(*) AS CT 
			FROM SWO_OBSERVATIONS O, 
				T,
				SWO_ANSWERS A,
				SWO_ITEMS I 
			WHERE T.USID = O.EMPLOYEE
			AND A.ANSWER = 'IO'
			AND A.ITEM_NUM = I.ITEM_NUM
			AND A.SWO_ID = O.SWO_ID
			AND  OBSERVED_DATE BETWEEN TO_DATE(:startDate, 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE(:endDate, 'MM/DD/YYYY HH24:MI:SS')
			GROUP BY ITEM_CATEGORY, ITEM_TEXT
			ORDER BY COUNT(*) DESC
		");
		
		self::sot()->bind($stmt_io, array(
			"usid" => "%{$_REQUEST["usid"]}%",
			"startDate" => $_REQUEST["startDate"] . ' 00:00:00',
			"endDate" => $_REQUEST["endDate"] . ' 23:59:59'
		));
		
		$io = "<table style='display: none;' class='tbl' id='io_tbl'>";	
		$io .= "<tr><th colspan='3'><div class='inner'>Improvement Opportunities</div></th></tr>";
		$io .= "<tr>";
		$io .= "<th><div style='width:100px;' class='inner'>No.</div></th>";
		$io .= "<th><div style='width:600px;' class='inner'>Observation Item</div></th>";
		$io .= "<th><div style='width:100px;' class='inner'>IO Count</div></th>";
		$io .= "</tr>";
		
		$x = 0;
		$total = 0;
		$style = "style='text-align: left; width:600px;'";
		while($row = self::sot()->fetch($stmt_io)){
			
			if($x < 5){
				$display = "";
			} else {
				$display = "style='display: none;'";
			}
			
			$io .= "<tr class = '" . ($x++ % 2 == 0 ? 'even' : 'odd') . "' {$display}>";
			$io .= "<td>{$x}</td>";
			$io .= "<td {$style}>{$row["ITEM_TEXT"]}</td>";
			$io .= "<td>{$row["CT"]}</td>";
			$io .= "</tr>";
			
			$total += $row["CT"];
		}
		
		if($x == 0){
			$io .= "<tr><td colspan='3'>No improvement opportunites found for the current time frame ({$_REQUEST["startDate"]} - {$_REQUEST["endDate"]}).</td></tr>";
		} else if($x > 1){
			$border = "style='border: 1px solid black;'";
			//$style = "style='text-align: center; width:600px;'";
			
			$link = "<a href='#' onclick='sot.home.showAllIO(this);return false;'>{$total}</a>";
		
			$io .= "<tr {$border}>";
			$io .= "<td {$border}>--</td>";
			$io .= "<td {$border} {$style}>Grand Total Improvement Opportunities</td>";
			$io .= "<td {$border}>" . ($x > 5 ? $link : $total) ."</td>";
			$io .= "</tr>";
		}
		
		$out["io"] = $io;
		
		self::printer($out);
	}
	
	static function quarterlyMetric(){
		//array("usid" => array("hasbeenobserved" => 1, "totalemployeesobserved" => 1));
		$directTeam = self::reportsGenerated($_REQUEST["usid"]);
		if(empty($directTeam)){ self::printer(array()); return;}
		
		$dates = self::quarterDates($_REQUEST["quarter"], $_REQUEST["year"]);
		
		$stmt_q_o = self::sot()->parse( "				
			SELECT 	MAX(EPOP_ID) AS OBSERVED 
			FROM 	EPOP_OBSERVATIONS
					WHERE EMPLOYEE = :usid
					AND OBSERVED_DATE BETWEEN TO_DATE(:startDate, 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE(:endDate, 'MM/DD/YYYY HH24:MI:SS')	
					AND SWO = 1
		");
	
		//CHANGE HERE
		$stmt_q_c = self::sot()->parse("
			WITH T AS (
					SELECT 	USID 
					FROM 	EMPLOYEES 
					START WITH SUPERVISOR = :usid 
					CONNECT BY PRIOR USID = SUPERVISOR
			)
			SELECT 	COUNT(DISTINCT EMPLOYEE) AS CT 
			FROM 	EPOP_OBSERVATIONS O, T 
					WHERE T.USID = O.EMPLOYEE
					AND OBSERVED_DATE BETWEEN TO_DATE(:startDate, 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE(:endDate, 'MM/DD/YYYY HH24:MI:SS')
					AND SWO = 1
		
		");
		
		foreach($directTeam as $key=>$value){
			self::sot()->bind($stmt_q_o, array(
				"usid" => $value["USID"],
				"startDate" => $dates["start"] . ' 00:00:00',
				"endDate" => $dates["end"] . ' 23:59:59'
			));
			
			$row = self::sot()->fetch($stmt_q_o, false);
			
			$out[$value["USID"]]["observed"] = $row["OBSERVED"];
			
			if($value["TEAM"] > 0){
				self::sot()->bind($stmt_q_c, array(
					"usid" => $value["USID"],
					"startDate" => $dates["start"] . ' 00:00:00',
					"endDate" => $dates["end"] . ' 23:59:59'
				));
				
				$row = self::sot()->fetch($stmt_q_c, false);
				$out[$value["USID"]]["totalemployeesobserved"] = $row["CT"];
				$out[$value["USID"]]["percentcomplete"] = @round(($row["CT"]/$value["TEAM"]) * 100, 2);
			} else {
				$out[$value["USID"]]["totalemployeesobserved"] = '--';
				$out[$value["USID"]]["percentcomplete"] = '--';
			}
			
			
		}
		
		self::printer($out);
	}
	
	static function getInfo(){
		$stid = self::sot()->parse("
			SELECT NAME, ORG_CODE, LOCATION FROM EMPLOYEES WHERE USID = :usid
		");
		
		self::sot()->bind($stid, array(
			":usid"	=> $_REQUEST["usid"]
		));
		
		$employee = self::sot()->fetch($stid);
		
		if(empty($employee)){
			$employee = @mcl_ldap::lookup($_REQUEST["usid"]);
			$org_desc = substr($employee["all"][0]["dtebusinessunitdeptidlongdesc"][0], 5);
			$loc = $employee["all"][0]["positionlocationlongdesc"][0];
			$name = $employee["fname"];
			
			$stid = self::sot()->parse("
				SELECT ORG_CODE FROM ORGANIZATIONS WHERE ORG_DESCRIPTION LIKE :org_desc
			");
			
			self::sot()->bind($stid, array(
				":org_desc"	=> "%{$org_desc}%"
			));
			
			$org = self::sot()->fetch($stid);
			$org = $org["ORG_CODE"];
			
		} else {
			$org = $employee["ORG_CODE"];
			$name = $employee["NAME"];
			$loc = $employee["LOCATION"];
		}
		self::printer(array(
			"name"	=> $name,
			"org"	=> $org,
			"loc"	=> $loc
		));
	}
	
	static function subItems(){
		$officefield = !empty($_REQUEST["officefield"]) ? $_REQUEST["officefield"] : "OFFICE";
		$org = $_REQUEST["org"];
		$itemnum = $_REQUEST["itemnum"];
		
		
		if($officefield == "OFFICE"){
			$key = "OFFICE";
		} else {
			$key = "FIELD";
		}
		
		$stmt = self::sot()->parse( "
			SELECT 	* 
			FROM 	SWO_SUBITEMS S, 
					SWO_ORG_SUBITEMS SO
			WHERE 	S.SUBITEM_NUM = SO.SUBITEM_NUM
					AND ORG_CODE = :org
					AND ACTIVE = 1
					AND PARENT_ITEM_NUM = :itemnum
					AND {$key} = :officefield
		");
		
		self::sot()->bind($stmt, array(
			":itemnum"			=> $itemnum,
			":org"				=> $org,
			":officefield"		=> 1
		));
		
		$out = array();
		while($row = self::sot()->fetch($stmt)){
			$out[] = $row;
		}
		
		self::printer($out);
		
	}
	
	static function addItems(){
		
		if($_REQUEST["category"] == 'qew' ) { $category = 'Qualified Electrical Worker'; }
		else if($_REQUEST["category"] == 'pp' ) { $category = 'Paired Performance'; }
		else { $category = $_REQUEST['category']; }
		
		//office only
		
		$sql = "SELECT * FROM EPOP_ITEMS WHERE ACTIVE = 1 AND LOWER(ITEM_CATEGORY) = '" . strtolower($category) . "' ORDER BY ITEM_NUM";
		
		$out = array();
		while($row = self::sot()->fetch($sql)){
			$out[] = $row;
		}
		
		self::printer($out);
	}
	
	static function getLocations_redtag(){
		if($_REQUEST["org"] == "Engineering") {
			$where = "ORG IN('Primary', 'PERT', 'Cable Test')";
		} else if($_REQUEST["org"] == "Nuclear Generation") {
			$where = "ORG IN('E. Fermi 2')";
		} else if($_REQUEST["org"] == "System Operations") {
			$where = "ORG IN('Distribution', 'Tranmission/Sub-Transmission')";
		} else {
			$where = "ORG = '{$_REQUEST["org"]}'";
		}
		
		$sql = "
			SELECT DISTINCT LOCATION 
				FROM RED_TAG_AUDITS_LOCATIONS
				WHERE {$where}
			ORDER BY LOCATION
		";
		
		$out = array();
		while($row = self::sot()->fetch($sql)){
			$out[] = $row;
		}
		
		self::printer($out);
	}
	
	static function getLastAudit(){
		$row = false;
		$sql = "
				SELECT LOWER(TO_CHAR(TO_DATE(CREDIT_MONTH, 'MM'), 'MONTH')) AS MONTH, TO_CHAR(AUDIT_DATE, 'YYYY') AS YEAR FROM
					RED_TAG_AUDITS
					WHERE LOCATION = '{$_REQUEST["location"]}'
						AND ORG =  '{$_REQUEST["org"]}'
						AND CREDIT_MONTH = {$_REQUEST["credit_month"]}
						AND ROWNUM = 1
					ORDER BY AUDIT_DATE DESC
			";
			
		if(!$row = self::sot()->fetch($sql)){
			$sql = "
				SELECT LOWER(TO_CHAR(TO_DATE(CREDIT_MONTH, 'MM'), 'MONTH')) AS MONTH, TO_CHAR(AUDIT_DATE, 'YYYY') AS YEAR FROM
					RED_TAG_AUDITS
					WHERE LOCATION = '{$_REQUEST["location"]}'
						AND ORG =  '{$_REQUEST["org"]}'
					ORDER BY AUDIT_DATE DESC
			";
			$row = self::sot()->fetch($sql);
		}
		
		
		if($row){
			$row["MONTH"] = ucfirst($row["MONTH"]);
		} 
		self::printer($row);
	}
	
	static function test(){
		$user = auth::check();
		self::printer($user);
	}
	
	static function getRTAs(){
		$sql = "
			SELECT 
				R.LOCATION,
				R.PROTECTION_LEADER,
				R.RSD,
				R.ORG,
				R.OPERATION_AUTHORITY,
				R.LOCATION_DESC_EQP_SHUTDOWN,
				R.NATURE_OF_WORK,
				TO_CHAR(R.AUDIT_DATE, 'MM/DD/YYYY') AS AUDIT_DATE,
				TO_CHAR(TO_DATE(R.CREDIT_MONTH, 'MM'), 'Month') AS CREDIT_MONTH,
				R.COMPLETE,
				R.RTA_ID AS ID,
				TO_CHAR(R.COMPLETED_DATE, 'MM/DD/YYYY HH24:MI:SS') AS COMPLETED_DATE,
				R.COMPLETED_BY
			FROM	
				RED_TAG_AUDITS R
			LEFT JOIN 
				EMPLOYEES E ON E.USID = R.COMPLETED_BY
			WHERE	
				COMPLETED_BY IN (
					SELECT USID FROM EMPLOYEES WHERE PATH LIKE '%' || :usid || '%' UNION ALL SELECT :usid FROM DUAL
				)
			ORDER BY 
				AUDIT_DATE ASC	
		";
		
		$stmt = self::sot()->parse($sql);
		self::sot()->bind($stmt, array(
			":usid"	=> $_REQUEST['usid']
		));

		$return = "
			<table class='tbl' style='width: 90%;'>
				<thead>
					<tr>
						<th colspan=6><div class='inner_title'>Red Tag Audits Saved/Completed</div></th>
					</tr>
					<tr>
						<th><div class='inner'>Location (SC)</div></th>
						<th><div class='inner'>Org. Audited</div></th>
						<th><div class='inner'>Date Audited</div></th>
						<th><div class='inner'>Credit Month</div></th>
						<th><div class='inner'></div></th>
					</tr>
				</thead>
				<tbody>
		";
		
		$results = false;
		while($row = self::sot()->fetch($stmt)){
			$return .= "
				<tr class='" . ($x++ % 2 == 0 ? "even" : "odd") . "'>
					<td>{$row['LOCATION']}</td>
					<td>{$row['ORG']}</td>
					<td>{$row['AUDIT_DATE']}</td>
					<td>{$row['CREDIT_MONTH']}</td>
					<td>
						<a style='min-width: 35px; margin-top: 5px; margin-bottom: 5px;" . ($row["COMPLETE"] ? " display: none; " : "") . "' href = 'forms/redtag.php?id={$row["ID"]}' class='btn'>Edit</a> 
						<a target='_blank' style='min-width: 35px; margin-top: 5px; margin-bottom: 5px;" . ($row["COMPLETE"] ? "" : " display: none; ") . "' href = '../../forms/viewRedtag.php?id={$row["ID"]}' class='btn'>View</a>
					</td>
				</tr>
			";
			$results = true;
		}
		
		if(!$results){
			$return .= "
				<td colspan='6'>
					There are no audits to display with the currrent filters.
				</td>
			";
		}
		
		$return .= "
				</tbody>
			</table>
		";
		
		echo $return;
	}
}

$user = auth::check();

if($user["status"] = "authorized"){
	if(!empty($_REQUEST["f"])){
	
		engine::setReportsGenerated();
		call_user_func("engine::{$_REQUEST["f"]}");
	}
} else {
	engine::printer('unathorized');
}
?>