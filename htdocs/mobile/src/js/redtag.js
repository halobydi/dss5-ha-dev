var baseDir = window.location.origin + "/mobile";
dojo.provide('sot.redtag');

var parent = 'attachments';
var a_count = 0;
var count = 'a_count';
var last_incomplete;
		
sot.redtag = function(){
	var js_calendar;

	return {
		cal: function(id, field) {
			if (!js_calendar) {
				js_calendar = Calendar.setup({
						animation: false,
						onSelect: function() { this.hide() }
				});
			}
			
			js_calendar.manageFields(id, field, '%m/%d/%Y');
		},
		
		switchOrg: function(){
			locations = dojo.byId("location");
			locations.innerHTML = "";
			
			sot.tools.ajax.submit("get", "json", {
				f: "getLocations_redtag",
				org: dojo.byId("org").value
			}, function(_e){
				for(var i in _e){
					var option = document.createElement('option');
					option.setAttribute('value', _e[i].LOCATION)
					option.innerHTML = _e[i].LOCATION;
					locations.appendChild(option);
				}
				return true;
			}, false);
		},
		
		switchCreditMonth: function(){
			dojo.byId("reminder").innerHTML = "";
			var location_ = dojo.byId("location").value;
			var org = dojo.byId("org").value;
			
			if(location_ == ""){
				return;
			}
			
			sot.tools.ajax.submit("get", "json", {
				f: "getLastAudit",
				location: location_,
				org: org,
				credit_month: dojo.byId("credit_month").value
			}, function(_e){
				if(_e){
					dojo.byId("reminder").innerHTML = "Last audit performed for " + location_ + (org == "" ? "" : " ("  + org + ")") + " was on " + _e.MONTH + " " + _e.YEAR ;
				}
				return true;
			}, false);
		},
		
		imposeMaxLength: function(Object, MaxLen){
			return (Object.value.length <= MaxLen);
		},

		onSubmit: function(){
			sot.tools.cover(true);
			
			var inputs = document.getElementsByTagName('input');  
			var org = dojo.byId("org");
			var location_ = dojo.byId("location");
			var credit_month = dojo.byId("credit_month");
			var audit_date = dojo.byId("audit_date");
			
			if(org.value == ""){
				alert("Select org audited.");
				org.focus();
				sot.tools.cover(false);
				return false;
			}
			if(location_.value == ""){
				alert("Select location.");
				location_.focus();
				sot.tools.cover(false);
				return false;
			}
			if(!isValidDate(audit_date.value)){
				alert("Date format must be MM/DD/YYYY.");
				audit_date.focus();
				sot.tools.cover(false);
				return false;
			}
			if(credit_month.value == ""){
				alert("Select credit month.");
				credit_month.focus();
				sot.tools.cover(false);
				return false;
			}
			
			if(isNaN(dojo.byId("rsd").value)){
				alert('Invalid RSD# entered. Only numeric characters allowed.');
				dojo.byId("rsd").focus();
				sot.tools.cover(false);
				return false;
			}
			
			var x = 0; 
			var complete = true; 
			var type_checked = false;
			for( i = 0; i < inputs.length; i++ ) {
				if(inputs[i].name == "id"){continue;}
				if (inputs[i].type == 'radio'){
					var group = document.getElementsByName(inputs[i].name); 
					var marked = false;
					if (group != null){
						for (n = 0; n < group.length; n++){
							if (group[n].checked == true)
								marked = true;
						}
					}
					
					if(!marked){
						complete = false;
					}
				}
				
				if(inputs[i].type == 'text'){
					if(inputs[i].value == ''){
						complete = false;
						alert("One or more questions have not been answered.");
						inputs[i].focus();
						sot.tools.cover(false);
						return false;
					}
				}
				
				if(inputs[i].type == 'checkbox'){
					if(inputs[i].checked == true && isNaN(inputs[i].name)){
						type_checked = true;
					}
				}	
			}
			
			var textareas = document.getElementsByTagName('textarea');  
			for( i = 0; i < textareas.length; i++ ) {
				if(textareas[i].value == ''){
					complete = false;
					alert("One or more fields are blank.");
					textareas[i].focus();
					sot.tools.cover(false);
					return false;
				}
			}

			if(!complete){
				alert("One or more fields are blank.");
				sot.tools.cover(false);
				return false;
			}
			
			if(!type_checked) {
				alert("Select one or more audit type.");
				sot.tools.cover(false);
				return false;
			}
			
			if(a_count == 0){
				alert('Please attach images for individual crew members.');
				sot.tools.cover(false);
				return false;
			}
			
			function isValidDate(date)
			{
				var matches = /^(\d{2})[\/](\d{2})[\/](\d{4})$/.exec(date);
				if (matches == null) return false;
				var d = matches[2];
				var m = matches[1] - 1;
				var y = matches[3];
				var composedDate = new Date(y, m, d);
				return composedDate.getDate() == d &&
						composedDate.getMonth() == m &&
						composedDate.getFullYear() == y;
			}
			
			return true;
		},
		
		mark: function(obj, id, num){
			dojo.byId(id).checked = true;
			
			dojo.byId("bigRadio_YES_" + num).firstChild.className = "radio-btn-inner";
			dojo.byId("bigRadio_NO_" + num).firstChild.className = "radio-btn-inner";
			dojo.byId("bigRadio_NA_" + num).firstChild.className = "radio-btn-inner";
			obj.firstChild.className = "radio-btn-inner checked";
		},
		
		addAttachment: function(){
			var attachment = document.createElement('div');
			attachment.id = 'attachment_' + a_count;

			attachment.innerHTML = "<input type = 'hidden' name = 'aid_" + a_count + "' value = ''/>\
									<input type = 'hidden' name = 'deleted_" + a_count + "' id = 'deleted_" + a_count + "' value = '0'/>\
									<input class='btn' style = 'margin-right: 20px; visibility: hidden; width: 0px; padding: 0px; margin: 0px;' type = 'file' id = 'realFile_" + a_count + "' name = 'file_" + a_count + "'/>\
									<span style='display: inline-block;'>\
										<span style='position: relative; top: 5px;'>\
											Name <input type = 'text' id = 'a_name_" + a_count + "' name = 'a_name_" + a_count + "' value = ''/>\
										</span>\
										&nbsp;\
										<input class='btn' style = 'margin-right: 5px;' type = 'button' id = 'fakeFile_" + a_count + "' onClick='sot.redtag.locateFile(" + a_count + ");' value='Choose File'/>\
										<span class = 'delete_a' style = 'cursor: pointer;' onclick = 'sot.redtag.removeAttachment(" + a_count + ");'>\
											<input style='' id = 'removeFile_" + a_count + "' type='button' class='btn' value='Remove'/>\
										</span>\
									</span>\
								";

			a_count++;

			document.getElementById(parent).appendChild(attachment);
			document.getElementById(count).value = a_count;

		},
		
		locateFile: function(num){
			$('#realFile_' + num).change(function(){
				var filename = $('#realFile_' + num).val().split('\\').pop();
				filename = filename.substring(0, filename.lastIndexOf('.'));
				$('#a_name_' + num).val(filename);
				$('#fakeFile_' + num).hide();
				$('#removeFile_' + num).show();
			});
			$('#realFile_' + num).click();
		},
		
		removeAttachment: function(num){
			try {
				document.getElementById('deleted_' + num).value = '1';
				document.getElementById('attachment_' + num).style.display = 'none';
				document.getElementById(count).value = a_count;
			} catch(ex) {
				alert(ex.message);
			}
		},
		view: function(rta_id){
			window.open(baseDir + '/mobile/forms/viewRedtag.php?rtaId=' + rta_id, '_blank', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes');
		}
	};
}();