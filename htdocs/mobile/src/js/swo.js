var baseDir = window.location.origin + "/mobile";
dojo.provide('sot.swo');

sot.swo = function(){
	var items = {};
	var subitems = {};
	var subitemsActive = 0;
	var ioCt = 0;
	var js_calendar;
	var employees = {};
	var employeeCt = 1;
	var keyUpFired = false;
	
	return {
		cal: function(id, field) {
			if (!js_calendar) {
				js_calendar = Calendar.setup({
						animation: false,
						onSelect: function() { this.hide() }
				});
			}
			
			js_calendar.manageFields(id, field, '%m/%d/%Y');
		},
		
		inputUsid: function(input, num){
			var usid = input.value.replace(/\s/g, '');
			var event = window.event;
			
			if(usid.length == 6){
				var prevOrg = dojo.byId("org").value;
				/*
				if(keyUpFired && event.type == "change") {
					keyUpFired = false;
					return;
				}
				
				if(event.type == "keyup" && keyUpFired == false) {
					keyUpFired = true;
				}
				*/
				sot.tools.ajax.submit("get", "json", {
					f: "getInfo",
					usid: usid
				}, function(_e){
		
					dojo.byId("name_" + num).value = _e.name;
					dojo.byId("nameValidate_" + num).value = _e.name;
					dojo.byId("org").value = _e.org || '';
					dojo.byId("location").value = _e.loc || '';
					
					var newOrg = dojo.byId("org").value;
				
					if(newOrg != prevOrg) {
						sot.swo.switchOrg();
					}
							
					return true;
				}, true, false);
			} else {
				dojo.byId("name_" + num).value = '';
				dojo.byId("nameValidate_" + num).value = '';
				if(num == 1) {
					dojo.byId("org").value = '';
					dojo.byId("location").value = '';
				}
			}	
		},
		
		switchOrg: function(){
			if(subitemsActive > 0){
				for(var i in subitems){
					sot.swo.getSubItems(i);
				}
			}
			
	
			/*if(subitemsActive > 0){
				if(confirm("By changing this value, parts of the form will be reset./n Do you wish to proceed?")){
					for(var i in subitems){
						dojo.byId(i + "_subitems").innerHTML = "";
					}
					
					subitemsActive = 0;
					return true;
				} else {
					return false;
				}
			}
			*/
		},
		
		switchType: function(){
			if(subitemsActive > 0){
				for(var i in subitems){
					sot.swo.getSubItems(i);
				}
			}
			
			/*if(subitemsActive > 0){
				if(confirm("By changing this value, parts of the form will be reset./n Do you wish to proceed?")){
					for(var i in subitems){
						dojo.byId(i + "_subitems").innerHTML = "";
					}
					
					subitemsActive = 0;
					return true;
				} else {
					return false;
				}
			}
			
			*/
		},
		
		markItem: function(itemnum, value, hasSubItems, affectSubmit, crew, ohug){
			if(hasSubItems){
			
				if(items[itemnum] && items[itemnum] == value){
				
				} else {
					subitems[itemnum] = true;
					if(!items[itemnum] && value == "IO" || items[itemnum] && items[itemnum] !== "IO" && value == "IO"){
						
						if(crew && itemnum != 25){
							//do nothing only ppe should have subitems
						} else {
							sot.swo.getSubItems(itemnum, crew, ohug);
						}
						
						subitemsActive++;
					} else {
						dojo.byId(itemnum + "_subitems").innerHTML = "";
						
						if(subitems[itemnum]){
							delete subitems[itemnum];
						}
						
						if(items[itemnum] == "IO" && value != "IO"){
							subitemsActive--;
						}
					}
				}
			}
			
			var addComments = false;
			var removeComments = false;
			
			//if(disableEnable && disableEnable == true){
				if(!items[itemnum] && value == "IO"){
					ioCt++;
					addComments = true;
				} else if(items[itemnum] && items[itemnum] !== "IO" && value == "IO"){
					ioCt++;
					addComments = true;
				} else if(items[itemnum] == "IO" && value != "IO"){
					ioCt--;
					removeComments = true;
				}
				//dojo.byId("markAllSatisfactoryBtn").disabled =  (ioCt > 0 ? false : true);
				//dojo.byId("submit").disabled =  (ioCt > 0 ? false : true);
			//}

			if(addComments && affectSubmit) {
				if(dojo.byId(itemnum + "_comments").childNodes.length == 0) {
					sot.swo.addComment(itemnum, dojo.byId(itemnum + "_comments_img"));
					setTimeout(function() { try { dojo.byId(i + "_comments").childNodes[0].focus(); } catch(ex) {} }, 100);
				}
			} else if(removeComments && affectSubmit) {
				if(dojo.byId(itemnum + "_comments").childNodes.length == 1 && (dojo.byId(itemnum + "_comments").childNodes[0].value == "" || dojo.byId(itemnum + "_comments").childNodes[0].value == undefined)) {
					sot.swo.removeComment(itemnum, dojo.byId(itemnum + "_comments_img"));
				}
			}
			
			items[itemnum] = value;
		},
		
		addComment: function(itemnum, img){
			img.setAttribute('src', '../src/img/m.remove.png');
			img.setAttribute('alt', 'Remove Comment');
			img.onclick = function(){
				sot.swo.removeComment(itemnum, img);
			};
			dojo.byId(itemnum + "_comments").innerHTML = "<textarea style='width: 98%; height: 100px;' name='" + itemnum + "_comments' id='" + itemnum + "_comments_textarea' onkeypress='return sot.swo.imposeMaxLength(this, 1000);'></textarea>";
			
			setTimeout(function() {
				try {
					dojo.byId(itemnum + "_comments_textarea").focus();
				} catch(ex) {}
			}, 100);
		},
		
		removeComment: function(itemnum, img){
			img.setAttribute('src', '../src/img/m.comment.png');
			img.setAttribute('alt', 'Add Comment');
			img.setAttribute('alt', 'Add Comment');
			img.onclick = function(){
				sot.swo.addComment(itemnum, img);
			};
			dojo.byId(itemnum + "_comments").innerHTML = "";
		},
		
		getSubItems: function(itemnum){
			sot.tools.ajax.submit("get", "json", {
				f: "subItems",
				org: dojo.byId("org").value,
				officefield: dojo.byId("office_field").value,
				itemnum: itemnum
			}, function(_e){
				var tbl = "<table style='width: 100%; margin-top:0px;'>";
				var x = 0;
	
				for(var i in _e){
					var bg = (x++ % 2 == 0 ? "#ffffca" : "#ffffca");
			
					tbl += "<tr style = 'background-color: " + bg + ";'>";

					tbl += "\
						<td style='width:30px; text-align:center; vertical-align:top;'>\
							<input style='margin-left: 5px; display: none;' id='S_" + _e[i].SUBITEM_NUM + "' type='radio' " + (items[_e[i].SUBITEM_NUM] && items[_e[i].SUBITEM_NUM] == "S" ? "checked=checked" : "") + " class='S' name='" + _e[i].SUBITEM_NUM + "' value='S'  onclick='sot.swo.markItem(" + _e[i].SUBITEM_NUM + ", \"S\", false);' />\
							<div style='margin-left: 5px;' id='bigRadio_S_" + _e[i].SUBITEM_NUM + "' class='radio-btn' onClick='sot.swo.mark(this, \"S_" + _e[i].SUBITEM_NUM + "\", \"" + _e[i].SUBITEM_NUM + "\"); sot.swo.markItem(" + _e[i].SUBITEM_NUM + ", \"S\", false);'><div class='radio-btn-inner " + (items[_e[i].SUBITEM_NUM] && items[_e[i].SUBITEM_NUM] == "S" ? "checked" : "") + "'></div></div>\
						</td>\
						<td style='width:30px; text-align:center; vertical-align:top;'>\
							<input style='display: none;' id='IO_" + _e[i].SUBITEM_NUM + "' type='radio' " + (items[_e[i].SUBITEM_NUM] && items[_e[i].SUBITEM_NUM] == "IO" ? "checked=checked" : "") + "class='IO' name='" + _e[i].SUBITEM_NUM + "' value='IO'  onclick='sot.swo.markItem(" + _e[i].SUBITEM_NUM + ", \"IO\", false);' />\
							<div id='bigRadio_IO_" + _e[i].SUBITEM_NUM + "' class='radio-btn' onClick='sot.swo.mark(this, \"IO_" + _e[i].SUBITEM_NUM + "\", \"" + _e[i].SUBITEM_NUM + "\"); sot.swo.markItem(" + _e[i].SUBITEM_NUM + ", \"IO\", false);'><div class='radio-btn-inner " + (items[_e[i].SUBITEM_NUM] && items[_e[i].SUBITEM_NUM] == "IO" ? "checked" : "") + "'></div></div>\
						</td>\
						<td style='width:30px; text-align:center; vertical-align:top;'>\
							<input style='display: none;' id='NA_" + _e[i].SUBITEM_NUM + "' type='radio' " + (items[_e[i].SUBITEM_NUM] && items[_e[i].SUBITEM_NUM] == "NA" ? "checked=checked" : "") + "class='NA' name='" + _e[i].SUBITEM_NUM + "' value='NA'  onclick='sot.swo.markItem(" + _e[i].SUBITEM_NUM + ", \"NA\", false);' />\
							<div id='bigRadio_NA_" + _e[i].SUBITEM_NUM + "' class='radio-btn' onClick='sot.swo.mark(this, \"NA_" + _e[i].SUBITEM_NUM + "\", \"" + _e[i].SUBITEM_NUM + "\"); sot.swo.markItem(" + _e[i].SUBITEM_NUM + ", \"NA\", false);'><div class='radio-btn-inner " + (items[_e[i].SUBITEM_NUM] && items[_e[i].SUBITEM_NUM] == "NA" ? "checked" : "") + "'></div></div>\
						</td>\
						<td>" + _e[i].SUBITEM_TEXT + "</td>\
					";
					
					tbl += "</tr>";
				}

				tbl += "</table>";
				
				//dojo.byId(itemnum + "_subitems").style.display = "block";
				dojo.byId(itemnum + "_subitems").innerHTML = tbl;
				//Check if still IO, enable/disabled mark all & submit btn
				return true;
			}, true);
			
		},
		
		toggleSiteItems: function(value) {
			if(value == 'onsite') {
				sot.swo.addItems(true, value, 'Onsite');
				sot.swo.addItems(false, 'offsite'); //hide offsite questions
			} else {
				sot.swo.addItems(true, value, 'Offsite');
				sot.swo.addItems(false, 'onsite'); //hide onsite questions
			}
		},
		
		addItems: function(checked, category, header) {
			category = category.toLowerCase();
			
			if(checked){
				sot.tools.ajax.submit("get", "json", {
					f: "addItems",
					category: category,
				}, function(_e){
					var tbl = "<table style='width: 100%; margin-top:0px;'>";
					var x = 0;
				
					tbl += "<tr class='header'>";
					tbl += "<td style='width:30px; text-align:center;'>S</td>";
					tbl += "<td style='width:30px; text-align:center;'>IO</td>";
					tbl += "<td style='width:30px; text-align:center;'>NA</td>";
					tbl += "<td style='text-align:center;'>" + (header || category.toUpperCase()) + "</td>";
					tbl += "<td></td>";
					tbl += "</tr>";
					
					for(var i in _e){
						var bg = (x++ % 2 == 0 ? "#f0f0f0" : "#cecece");
				
						tbl += "<tr style = 'background-color: " + bg + ";'>";
						
						tbl += "\
							<td style='width:30px; text-align:center; vertical-align:top;'>\
								<input style='margin-left: 5px; display: none;' id='S_" + _e[i].ITEM_NUM + "' type='radio' " + (items[_e[i].ITEM_NUM] && items[_e[i].ITEM_NUM] == "S" ? "checked=checked" : "") + " class='S' name='" + _e[i].ITEM_NUM + "' value='S'  onclick='sot.swo.markItem(" + _e[i].ITEM_NUM + ", \"S\", false, true);' />\
								<div style='margin-left: 5px;' id='bigRadio_S_" + _e[i].ITEM_NUM + "' class='radio-btn' onClick='sot.swo.mark(this, \"S_" + _e[i].ITEM_NUM + "\", \"" + _e[i].ITEM_NUM + "\"); sot.swo.markItem(" + _e[i].ITEM_NUM + ", \"S\", false, true);'><div class='radio-btn-inner " + (items[_e[i].ITEM_NUM] && items[_e[i].ITEM_NUM] == "S" ? "checked" : "") + "'></div></div>\
							</td>\
							<td style='width:30px; text-align:center; vertical-align:top;'>\
								<input style='display: none;' id='IO_" + _e[i].ITEM_NUM + "' type='radio' " + (items[_e[i].ITEM_NUM] && items[_e[i].ITEM_NUM] == "IO" ? "checked=checked" : "") + "class='IO' name='" + _e[i].ITEM_NUM + "' value='IO'  onclick='sot.swo.markItem(" + _e[i].ITEM_NUM + ", \"IO\", false, true);' />\
								<div id='bigRadio_IO_" + _e[i].ITEM_NUM + "' class='radio-btn' onClick='sot.swo.mark(this, \"IO_" + _e[i].ITEM_NUM + "\", \"" + _e[i].ITEM_NUM + "\"); sot.swo.markItem(" + _e[i].ITEM_NUM + ", \"IO\", false, true);'><div class='radio-btn-inner " + (items[_e[i].ITEM_NUM] && items[_e[i].ITEM_NUM] == "IO" ? "checked" : "") + "'></div></div>\
							</td>\
							<td style='width:30px; text-align:center; vertical-align:top;'>\
								<input style='display: none;' id='NA_" + _e[i].ITEM_NUM + "' type='radio' " + (items[_e[i].ITEM_NUM] && items[_e[i].ITEM_NUM] == "NA" ? "checked=checked" : "") + "class='NA' name='" + _e[i].ITEM_NUM + "' value='NA'  onclick='sot.swo.markItem(" + _e[i].ITEM_NUM + ", \"NA\", false, true);' />\
								<div id='bigRadio_NA_" + _e[i].ITEM_NUM + "' class='radio-btn' onClick='sot.swo.mark(this, \"NA_" + _e[i].ITEM_NUM + "\", \"" + _e[i].ITEM_NUM + "\"); sot.swo.markItem(" + _e[i].ITEM_NUM + ", \"NA\", false, true);'><div class='radio-btn-inner " + (items[_e[i].ITEM_NUM] && items[_e[i].ITEM_NUM] == "NA" ? "checked" : "") + "'></div></div>\
							</td>\
						";
						
						tbl += "<td>" + _e[i].ITEM_TEXT + "</td>";
						tbl += "<td style='text-align:right;'><img id='" + _e[i].ITEM_NUM + "_comments_img' style='max-width: 32px; cursor: pointer; margin-right: 5px;' src='../src/img/m.comment.png' style='cursor:pointer;' alt='Add Comment' onclick='sot.swo.addComment(" + _e[i].ITEM_NUM  + ", this);'/></td>";
						tbl += "</tr>";
						
						tbl += "<tr style='background-color: " + bg + "; padding:0px; margin:0px;'>";
						tbl += "<td colspan='5' id='"  + _e[i].ITEM_NUM + "_comments'></td>";
						tbl += "</td>";
						tbl += "</tr>";
					}

					tbl += "</table>";
			
					dojo.byId(category).innerHTML = tbl;
					dojo.byId(category).style.display = "block";
				
					return true;
				}, true);
			} else {
				dojo.byId(category).innerHTML = "";
				dojo.byId(category).style.display = "none";
			}
		},
		
		imposeMaxLength: function(Object, MaxLen){
			return (Object.value.length <= MaxLen);
		},
		
		markAllSat: function(){
			var radios = document.getElementsByTagName('input');  
			var x = 0; 
			
			for( m = 0; m < radios.length; m++ ) {
				if (radios[m].type == 'radio'){
					var group = document.getElementsByName(radios[m].name); 
					var marked = false;
					if (group != null){
						for (n = 0; n < group.length; n++){
							if (group[n].checked == true)
								marked = true;
						}
					}
					if (!marked){
						if (group != null){
							for (n = 0; n < group.length; n++){
								if (group[n].className == 'S'){
									group[n].checked = true;
									dojo.byId("bigRadio_S_" + group[n].name).firstChild.className = "radio-btn-inner checked";
								}
							}
						}
					}					
				}
			}
		},
		mark: function(obj, id, num){
			dojo.byId(id).checked = true;
			
			dojo.byId("bigRadio_S_" + num).firstChild.className = "radio-btn-inner";
			dojo.byId("bigRadio_IO_" + num).firstChild.className = "radio-btn-inner";
			dojo.byId("bigRadio_NA_" + num).firstChild.className = "radio-btn-inner";
			obj.firstChild.className = "radio-btn-inner checked";
		},
		onSubmit: function(){
			var usid = dojo.byId("usid_1").value.replace(/\s/g, '');
			var usid1 = usid;
			
			var name = dojo.byId("name_1").value;
			var name1 = name;
			
			var nameValidate = dojo.byId("nameValidate_1").value;
			var observedby = dojo.byId("observed_by").value;
			var date = dojo.byId("observed_date").value;
			var org = dojo.byId("org").value;
	
			//if((name.replace(/\s/g, '') == "" && usid != "") || (!usid.match(/^[A-z]{1}\d{5}$/) && usid != "")){ --> Error for cb1234
			//u17623
			// || (!usid.replace(/\s/g, '').length == 7 && usid != "") Removed 03/14/2013
			
			//console.log(nameValidate.replace(/\s/g, '') == "" && usid != "");
			//console.log(!(usid.replace(/\s/g, '').length == 7))
			//false == 7 ===> false
			//console.log(usid.replace(/\s/g, '').length == 7)
			
			if((nameValidate.replace(/\s/g, '') == "" && usid != "")) {
				alert('Invalid Employee Username entered for who was observed. \n\nIf you would like to submit an anonymous observation for who was observed, leave the Employee Username field blank.\n\nIf you want to observe an individual who does not have a username, then leave the Employee Username field blank and enter their name in the Employee Name field.');
				dojo.byId('usid_1').focus();
				return false;
			}

			if(date.value == ""){
				alert('Enter date when this observation was performed.');
				date.focus();
				return false;
			}
			else if(!isValidDate(date)){
				alert("Date format must be MM/DD/YYYY.");
				date.focus();
				return false;
			}
			
			if(org == ""){
				alert('Select an organization.');
				return false;
			}
			
			var complete = true;
			var radios = document.getElementsByTagName('input'); 
			var complete = true;
			
			var incompleteRadios = [];
			
			for( m = 0; m < radios.length; m++ ) {
				if (radios[m].type == 'radio'){
					var group = document.getElementsByName(radios[m].name); 
					var marked = false;
					if (group != null){
						for (n = 0; n < group.length; n++){
							if (group[n].checked == true){
								marked = true;
							} 
						}
					}
					
					if (marked == false){
						complete = false;
						incompleteRadios.push(group);
					}
				}					
			}
			
			for(var i in items) {
				//if(items[i] == "IO" && document.forms[0].elements[i]) { //half works in IE
				if(items[i] == "IO" && dojo.byId(i + "_comments")) { //None works in IE
					if(dojo.byId("comments").value == "") { //additional comments
						if(dojo.byId(i + "_comments").childNodes.length == 0 || dojo.byId(i + "_comments").childNodes[0].value == "") { //no comment box open or comments for question empty
							alert("You have identified an Improvement Opportunity, but did not enter any comments.\n\nYou must either enter comments in the Additional Comments field at the bottom of the form or enter comments in the individual item comment field.");
							if(dojo.byId(i + "_comments").childNodes.length == 0) {
								sot.swo.addComment(i, dojo.byId(i + "_comments_img"));
							}
							setTimeout(function() { try { dojo.byId(i + "_comments").childNodes[0].focus(); } catch(ex) {} }, 100);
							return false;
						}
					}
				}
			}
			
			if(complete == false){
				if(confirm("One or more item have not been answered. Do you wish you continue?\nAll incomplete items will be saved as Not Applicable.")){
					var x = 0;
					for(var i in incompleteRadios){
						var marked = false;
						if (incompleteRadios[i] != null){
							for (n = 0; n < incompleteRadios[i].length; n++){
								if (incompleteRadios[i][n].className == "NA"){
									incompleteRadios[i][n].checked = true;
								} 
							}
						}
					}
				} else {
					return false;
				}
			}
			
			//not sure about this
			for(var i in employees){
				var nameValidate = dojo.byId("nameValidate_" + i).value.replace(/\s/g, '');
				var name = dojo.byId("name_" + i).value.replace(/\s/g, '');
				var usid = dojo.byId("usid_" + i).value.replace(/\s/g, '');
				
				if(usid == "" && name == ""){
					alert("You have added an additional employee to this observation but did not enter an employee username or name."); //contractors dont have id
					dojo.byId("usid_" + i).focus();
					return false;
				} else if((nameValidate.replace(/\s/g, '') == "" && usid != "")){
					alert('Invalid Employee Username entered for who was observed for an additional employee added to this observation.\n\nIf you want to observe an individual who does not have a username, then leave the Employee Username field blank and enter their name in the Employee Name field.');
					dojo.byId("usid_" + i).focus();
					return false;
				} 
			}

	
			if(name == "" && usid == ""){
				if(confirm("Are you sure you want to submit this observation without an employee username for who was observed?")){
					return true;
				} else {
					return false;
				}
			}
			
			function isValidDate(date)
			{
				var matches = /^(\d{2})[\/](\d{2})[\/](\d{4})$/.exec(date);
				if (matches == null) return false;
				var d = matches[2];
				var m = matches[1] - 1;
				var y = matches[3];
				var composedDate = new Date(y, m, d);
				return composedDate.getDate() == d &&
						composedDate.getMonth() == m &&
						composedDate.getFullYear() == y;
			}
			
			sot.tools.cover(true);
			return true;
		},
		addEmployee: function(){
			employeeCt++;
			var div = document.createElement('div');
			div.setAttribute('id', 'emp_' + employeeCt);
			div.innerHTML = "<span style='display: inline-block; padding-right: 10px;'><b>Employee Username: </b> <input type='text' name='usid[]' id='usid_" + employeeCt + "' maxlength='6' onkeyup='sot.swo.inputUsid(this, " + employeeCt + ");'/></span>";
			div.innerHTML += "<span style='display: inline-block; padding-right: 10px;'><b>Employee Name: </b> <input type='text' name='name[]' id='name_" + employeeCt + "'/><input type='hidden'  id='nameValidate_" + employeeCt + "'/>";
			div.innerHTML += "<button class='btn' style='vertical-align: top;' onclick='sot.swo.removeEmployee(" + employeeCt + "); return false;'>Remove</button></span>";
			
			dojo.byId("add_emp").appendChild(div);
			
			setTimeout(function(){
				try{
					dojo.byId("usid_" + employeeCt).focus();
				} catch(ex){}
			}, 300);
			
			employees[employeeCt] = true;

		},
		removeEmployee: function(num){
			try {
				dojo.byId("add_emp").removeChild(dojo.byId("emp_" + num));
			} catch(ex) {
				alert(ex.message);
			}
			
			if(employees[num]){
				delete employees[num];
			}
		}
	};
}();