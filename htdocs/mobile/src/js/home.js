var baseDir = window.location.origin + "/mobile";

dojo.provide('sot.home');

sot.home = function(){
	var quarter = null;
	var reportUsid = null;
	var reportDelegate = null;
	var reportsGenerated = [];
	var dates = {};
	
	var onBackHandler;
	
	var red = '#ffaaad';
	var yellow = '#ffffbd';
	var green = '#b5d3b5';
	
	var setQuarter = function(_quarter){
		quarter = _quarter;
	};
	
	var quarterOnClick = function(_quarter){
		//dojo.byId("q" + quarter + "box").setAttribute("className", "q" + quarter + "box");
		//dojo.byId("q" + quarter + "box").setAttribute("class", "q" + quarter + "box");
		
		setQuarter(_quarter);
		
		quarterlyReport();
	};
	
	var setDates = function(start, end){
		dates["start"] = start;
		dates["end"] = end;
	};
	
	var yearToggle = function(dir){
		dojo.byId("year").innerHTML = parseInt(dojo.byId("year").innerHTML) + dir;
		quarterlyReport();
	};
	
	var setReportUsid = function(usid){
		reportUsid = usid;
	};
	
	var setReportDelegate = function(key){
		reportDelegate = key || 0;
	};
	
	var setOnBackDisplay = function(){
		if(reportsGenerated.length == 1){
			//dojo.byId('back').style.display = 'none';
			if(onBackHandler){
				dojo.disconnect(onBackHandler);
				onBackHandler = null;
			}
		} else {
			//dojo.byId('back').style.display = 'block';
			if(!onBackHandler){
				onBackHandler = dojo.connect(document, 'keyup', function(){
					var e = window.event;
					if(e.keyCode == 27 || e.keyCode == 37){
						sot.home.back();
					}
				});
			}
		}		
	};
	
	var back = function(){
		reportsGenerated.pop();
		var usid = reportsGenerated[reportsGenerated.length - 1];
		initiate(usid, true);
			
		setOnBackDisplay();
	};
	
	var initiate = function(usid, back){
		reportUsid = usid;

		if(!back){
			reportsGenerated.push(usid);
		}

		sot.tools.ajax.submit("get", "text", {
			f: "getDirectTeam",
			usid: reportUsid,
			quarter: quarter,
			delegate: reportDelegate
		}, function(_e){
			dojo.byId('container').innerHTML = _e;
			
			weeklyReport();
			quarterlyReport();
			
			return true;
		}, true);
		
		setOnBackDisplay();
	};
	
	var weeklyReport = function(){
		sot.tools.ajax.submit("get", "json", {
			f: "weeklyMetric",
			usid: reportUsid,
			startDate: dates["start"],
			endDate: dates["end"]
		}, function(_e){
			//alert(_e);
			
			if(_e["io"]){
				dojo.byId("io").innerHTML = _e["io"] || "";
				delete _e["io"];
			}
			
			for(var _u in _e){
				if(_u != reportUsid){
					dojo.byId(_u + "_observedweekbtn").className = _e[_u].observed ? "btn btn-success" : "btn";
				} else {
					dojo.byId(_u + "_observedweek").style.display = 'none';
				}
				
				//dojo.byId(_u + "_completedweek").innerHTML = (_e[_u].completed ? (_e[_u].completed == "--" ? "--" : "<img src='src/img/check.png'/>") : "<img src='src/img/x.png'/>");
				
				if(dojo.byId(_u + "_lc")){
					//dojo.byId(_u + "_lc").innerHTML = _e[_u].lc;	
					
					if(_u == reportUsid){
						if(_e[_u].lc != "--"){
							if(_e[_u].lc < 60){
								//dojo.byId(_u + "_lc").parentNode.style.backgroundColor = red;
							} else if(_e[_u].lc < 80){
								//dojo.byId(_u + "_lc").parentNode.style.backgroundColor = yellow;
							} else if(_e[_u].lc >= 80){
								//dojo.byId(_u + "_lc").parentNode.style.backgroundColor = green;
							} 
						}
					}
				} else {
					if(_u == reportUsid){
						if(_e[_u].completed){
							//dojo.byId(_u + "_completedweek").parentNode.style.backgroundColor = green;
						} else {
							//dojo.byId(_u + "_completedweek").parentNode.style.backgroundColor = red;
						}
					}
				}
			}
			
			resize();
			return true;
		}, true, 'Calculating weekly metrics');
	};
	
	var quarterlyReport = function(){
		sot.tools.ajax.submit("get", "json", {
			f: "quarterlyMetric",
			usid: reportUsid,
			quarter: quarter,
			year: dojo.byId("year").innerHTML
		}, function(_e){
			dojo.byId("quarter").innerHTML = quarter;
			dojo.byId("q" + quarter + "box").setAttribute("className", "activeqbox");
			dojo.byId("q" + quarter + "box").setAttribute("className", "activeqbox");
			
			hasEmployees = false;
			
			for(var _u in _e){
				if(_u != reportUsid){
					dojo.byId(_u + "_observedquarterbtn").className = _e[_u].observed ? "btn btn-success" : "btn btn-danger";
					hasEmployees = true;
				}else {
					dojo.byId(_u + "_observedquarter").style.display = 'none';
				}
				
				//dojo.byId(_u + "_completedquarter").innerHTML = _e[_u].totalemployeesobserved;
				
				if(dojo.byId(_u + "_qc")){
					//dojo.byId(_u + "_qc").innerHTML = _e[_u].percentcomplete;	
					
					if(_u == reportUsid){
						if(_e[_u].percentcomplete != "--"){
							if(_e[_u].percentcomplete < 60){
								//dojo.byId(_u + "_qc").parentNode.style.backgroundColor = red;
							} else if(_e[_u].percentcomplete < 80){
								//dojo.byId(_u + "_qc").parentNode.style.backgroundColor = yellow;
							} else if(_e[_u].percentcomplete >= 80){
								//dojo.byId(_u + "_qc").parentNode.style.backgroundColor = green;
							} 
						}
					}
				}
			}
			
			if(hasEmployees){
				dojo.byId("quarterlyReport").style.display = 'block';
				dojo.byId('legend').style.display = "block";
				dojo.byId('weeklyLink').style.display = "block";
				dojo.byId('container').style.display = "block";
			}
			
			resize();
			return true;
		}, true, 'Calculating quarterly metrics');
	};
	
	var dateFilter = function(){

		setDates(dojo.byId("start").value, dojo.byId("end").value);
		weeklyReport(false);
		
	};
	
	var showAllIO = function(link){
		var tbl = dojo.byId("io_tbl");
		
		if(tbl){
			for(var i in tbl.rows){
				if(typeof tbl.rows[i] == "object"){
					tbl.rows[i].style.display = '';
				}
			}
		}
		
		link.style.color = '#000';
		link.style.cursor = 'text';
		link.focus();

	};
	
	var showQuarterly = function(){
		dojo.byId('weeklyReport').style.display = "none";
		dojo.byId('quarterlyLink').style.display = "none";
		dojo.byId('quarterlyReport').style.display = "block";
		dojo.byId('weeklyLink').style.display = "block";
		
		dojo.byId('quarter1').style.display = "block";
		dojo.byId('quarter2').style.display = "block";
		dojo.byId('week1').style.display = "none";
	};
	
	var showWeekly = function(){
		dojo.byId('quarterlyReport').style.display = "none";
		dojo.byId('weeklyLink').style.display = "none";
		dojo.byId('weeklyReport').style.display = "block";
		dojo.byId('quarterlyLink').style.display = "block";
		
		dojo.byId('quarter1').style.display = "none";
		dojo.byId('quarter2').style.display = "none";
		dojo.byId('week1').style.display = "block";
	};
	
	var showEPOP = function(){
		dojo.byId('epop').style.display = "block";
		dojo.byId('rta').style.display = "none";
		
		dojo.addClass('epopLink', "active");
		dojo.removeClass('rtaLink', "active");
		dojo.removeClass('ppLink', "active");
		dojo.removeClass('qewLink', "active");
	};
	
	var showRTA = function(){
		dojo.byId('rta').style.display = "block";
		dojo.byId('epop').style.display = "none";
		
		dojo.addClass('rtaLink', "active");
		dojo.removeClass('epopLink', "active");
		dojo.removeClass('ppLink', "active");
		dojo.removeClass('qewLink', "active");
	};
	
	var showPP = function(){
		dojo.addClass('ppLink', "active");
		dojo.removeClass('epopLink', "active");
		dojo.removeClass('rtaLink', "active");
		dojo.removeClass('qewLink', "active");
	};
	
	var showQEW = function(){
		dojo.addClass('qewLink', "active");
		dojo.removeClass('epopLink', "active");
		dojo.removeClass('ppLink', "active");
		dojo.removeClass('rtaLink', "active");
	};
	
	var getRTAs = function(){
		sot.tools.ajax.submit("get", "text", {
			f: "getRTAs",
			usid: reportUsid
		}, function(_e){
			dojo.byId('rta_container').innerHTML = _e;
			
			return true;
		}, true);
	};
	
	return {
		setQuarter: setQuarter,
		quarterOnClick: quarterOnClick,
		setDates: setDates,
		yearToggle: yearToggle,
		setReportUsid: setReportUsid,
		setReportDelegate: setReportDelegate,
		back: back,
		initiate: initiate,
		weeklyReport: weeklyReport,
		quarterlyReport: quarterlyReport,
		dateFilter: dateFilter,
		showAllIO: showAllIO,
		
		showQuarterly: showQuarterly,
		showWeekly: showWeekly,
		
		showEPOP: showEPOP,
		showRTA: showRTA,
		showPP: showPP,
		showQEW: showQEW,
		
		getRTAs: getRTAs
	};
}();