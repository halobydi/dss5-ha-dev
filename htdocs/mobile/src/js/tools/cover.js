dojo.provide('sot.tools.cover');

var cover;
sot.tools.cover = function(onOff, zIndex){
	
	if(!cover){
		cover = document.createElement('div');
		cover.setAttribute('id', 'cover');
		cover.style.zIndex = zIndex || 100000;
		cover.style.position = 'fixed';
		cover.style.left = '0';
		cover.style.top = '0'
		cover.style.width = '100%';
		cover.style.height = '100%';
		cover.style.backgroundColor = '#333333';
		cover.style.opacity = '0.5';
		cover.style.filter = 'alpha(opacity = 50)';
		cover.style.display = 'none';
		document.body.appendChild(cover);
	}
	
	cover.style.display = onOff ? 'block' : 'none';
	
};