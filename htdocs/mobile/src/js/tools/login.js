var baseDir = window.location.origin + "/mobile";

dojo.provide('sot.tools.login');

sot.tools.login = function(){
	var focusColor = '#f7f3d6';
	var error_box;
	var add_box;
	var add_toggle;
	var capslock_warning;
	var capslockOn = null;
	var capsLockTurnedOnElseWhere = true;
	
	var onSubmit = function(){
		capslock_warning.style.visibility = 'hidden';
		
		if(dojo.byId("username").value == ""){
			error_box.setAttribute('class', 'error');
			error_box.setAttribute('className', 'error');
			error_box.innerHTML = 'Please input username';
			dojo.byId("username").focus();
			return false;
		}
	
		if(dojo.byId("password").value == ""){
			error_box.setAttribute('class', 'error');
			error_box.setAttribute('className', 'error');
			error_box.innerHTML = 'Please input password';
			dojo.byId("password").focus();
			return false;
		}
		
		error_box.setAttribute('class', 'information');
		error_box.setAttribute('className', 'information');
		error_box.innerHTML = 'Processing login...';
		
		return true;
	};
	
	var onKeyPress = function(){
		var e = window.event;
		var s = String.fromCharCode(e.keyCode || e.which);
	
		if (s.toUpperCase() === s && !e.shiftKey && s.match(/^[a-zA-Z]+$/)) {
			capslock_warning.style.visibility = '';
			capslockOn = true;
			capsLockTurnedOnElseWhere = false;
		} else if(s.toLowerCase() === s && !e.shiftKey && s.match(/^[a-zA-Z]+$/)) {
			capslockOn = false;
			capslock_warning.style.visibility = 'hidden';
			capsLockTurnedOnElseWhere = false;
		}
	};
	
	var onKeyDown = function(){
		var e = window.event;
		var keyStroke = e.keyCode || e.which;
	
		if(capslockOn && keyStroke == 20){
			capslockOn = false;
			capslock_warning.style.visibility = 'hidden';
		} else if(!capslockOn && keyStroke == 20 && !capsLockTurnedOnElseWhere){
			capslockOn = true;
			capslock_warning.style.visibility = '';	
			capsLockTurnedOnElseWhere = false;
		}

	};
	
	var onFocus = function(_i){
		_i.style.backgroundColor =  focusColor;
	};
	
	var onBlur = function(_i){
		_i.style.backgroundColor = '#fff';
	};
	
	var resizeLogin = function(){
		var wb = dojo.window.getBox();
		dojo.byId("login_container").style.left = (wb.w / 2) - (dojo.byId("login_container").offsetWidth / 2)+ 'px';
		dojo.byId("login_container").style.top = (wb.h / 2) - (dojo.byId("login_container").offsetHeight / 2) + 'px';
	};
	
	var toggleAddBox = function(){
		var display = add_box.style.display == "block" ? false : true;
		
		add_box.style.display = (display ? "block" : "none");
		error_box.style.display = (display ? "none" : "block");
		
		add_toggle.innerHTML = (display ? "[x] " : "") + "Additional Options";
	};

	return {
		onSubmit: onSubmit,
		onKeyPress: onKeyPress,
		onKeyDown: onKeyDown,
		onFocus: onFocus,
		onBlur: onBlur,
		resizeLogin: resizeLogin,
		toggleAddBox: toggleAddBox
	};
}();
