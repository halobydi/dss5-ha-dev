dojo.provide("sot.tools.ajax");

sot.tools.ajax = function(){
	var ajaxInstances = {};
	var loaders = [];
	var ct = 0;
	var loader = null;
	var loadingText = null;
	var loadingImg = null;
	
	var loadingOnOff = function(isLoading, _loadingText){
		//var wb = dojo.window.getBox();
		
		if(isLoading){	
			sot.tools.cover(true);
			loaders.push({});
	
			/*
			loader = document.createElement('div');
			loader.setAttribute('id', 'loader');
			loader.style.textAlign = 'center';	
			loader.style.position = 'fixed';
			loader.style.top = wb.h / 2 + 'px';
			loader.style.left = wb.w / 2 + 'px';
		
			if(!loadingImg){
				loadingImg = document.createElement('img');
				loadingImg.setAttribute('src', 'src/img/loadingblue.gif');
				loader.appendChild(loadingImg);
			}
			
			if(!loadingText){
				loadingText = document.createElement('div')
				loader.appendChild(loadingText);						
			}
			
			if(_loadingText){
				loadingText.innerHTML = _loadingText + '...';
			}
			
			*/

		} else {
	
			if(loaders.length == 1){
				sot.tools.cover(false);
			} else {
				
			}
			
			loaders.pop();
		}
	};
	
	return {
		loadingOnOff: loadingOnOff,
		submit: function(method, handleAs, content, callback, loading, loadingText){
			/*ct++;
			ajaxInstances[ct] = {
				method: method,
				handleAs: handleAs,
				content: content,
				callback: callback,
				loading: loading,
				loadingText: loadingText
			};
			*/
			
			if(loading){
				loadingOnOff(true, loadingText || false);
				//loaders.push({});
			}
			
			mcl.ajax.submit(method, {
				url: '/mobile/src/php/engine.php',
				handleAs: handleAs || 'json',
				content: content,
				load: function(_e){
					if(_e){
						if(typeof callback == 'function'){
							if(callback(_e)){
								loadingOnOff(false);
								return;
							} else {
								loaders.pop();
							}
							return;
						}
					}
					
					if(loading){
						loadingOnOff(false);
						//loaders.pop();
					}
					
				
				},
				error: function(_e){
					alert('Unable to process request: ' + _e.message);
					loadingOnOff(false);
					//loaders.pop();
				}
			});
		}
	};
}();
