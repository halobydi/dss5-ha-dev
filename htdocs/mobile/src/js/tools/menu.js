dojo.provide('sot.tools.menu');

sot.tools.menu = function(){
	var bgColor = '#292829';

	var hoverBgColor = '#c6dfff';
	var hoverBorder = '#319aff';
	var disabledBgHoverColor = '';
	var disabledHoverBorder = '';
	var color = '#d6d3d6';
	var disabledColor = 'gray';
	var contextStartedWithAClick = false;
	var activeMenu = false;
	var activeDelegate  = false;

	var menuItems = {
		delegate:[
			
		],
		admin: [
			/*{ 
				title: 'Manage Users',
				img: 'user.png',
				onclick: function(){
		
					window.location = 'http://dqm/soteria/admin/manageusers.php' + (activeDelegate ? "?delegate=" + activeDelegate : "");
				}
			},*/
			{
				title: 'Manage Delegates',
				img: 'delegates.png',
				onclick: function(){
					window.location = 'http://dqm/soteria/admin/managedelegates.php' + (activeDelegate ? "?delegate=" + activeDelegate : "");
				}
			},
			{
				title: 'Manage Forms',
				subMenu: [
					{
						title: 'SWO Items',
						onclick: function(){
							window.location = 'http://dqm/soteria/admin/manageitems.php' + (activeDelegate ? "?delegate=" + activeDelegate : "");
						}
					},
					{
						title: 'SWO Sub-Items',
						onclick: function(){
							window.location = 'http://dqm/soteria/admin/managesubitems.php' + (activeDelegate ? "?delegate=" + activeDelegate : "");
						}
					}/*,
					{
						title: 'SWO QEW Items',
						onclick: function(){
							window.location = 'http://dqm/soteria/admin/managesubitems.php' + (activeDelegate ? "?delegate=" + activeDelegate : "");
						}
					}*/
				]
			},
			/*,
			{
				title: 'Mangage SWO Items',
				onclick: function(){
					window.location = 'http://dqm/soteria/admin/manageitems.php' + (activeDelegate ? "?delegate=" + activeDelegate : "");
				}
			}, 
			{
				title: 'Mangage Red Tag Audits'
			}*/, 
			{
				title: 'Exclusions'
			}/*, 
			{
				title: 'Employee Table'
			}, 
			{
				title: 'Alerts'
			}
			*/
		],
		individualreports: [
			{ 
				title: 'My Observations',
				onclick: function(){
					window.location = 'http://dqm/soteria/reports/individual/obscompleted.php' + (activeDelegate ? "?delegate=" + activeDelegate : "");
				}
				
			}, 
			{
				title: 'Leader Completions',
				onclick: function(){
					window.location = 'http://dqm/soteria/reports/individual/lc.php' + (activeDelegate ? "?delegate=" + activeDelegate : "");
				}
			}, 
			{
				title: 'Leaders Without SWO Completed',
				onclick: function(){
					window.location = 'http://dqm/soteria/reports/individual/leaderswoobs.php' + (activeDelegate ? "?delegate=" + activeDelegate : "");
				}
			}, 
			{
				title: 'SWO Comments',
				img: "comments.png",
				onclick: function(){
					window.location = 'http://dqm/soteria/reports/individual/comments.php' + (activeDelegate ? "?delegate=" + activeDelegate : "");
				}
			}, 
			{
				title: 'Excel Export',
				subMenu: [
					{
						title: "Current Week",
						onclick: function(){
							window.open('http://dqm/soteria/reports/individual/excel.php?method=week' + (activeDelegate ? "&delegate=" + activeDelegate : ""), "", "width=50, height=50");
						}
					},
					{
						title: "Current Month",
						onclick: function(){
							window.open('http://dqm/soteria/reports/individual/excel.php?method=month' + (activeDelegate ? "&delegate=" + activeDelegate : ""), "", "width=50, height=50");
						}
					},
					{
						title: "Current Year",
						onclick: function(){
							window.open('http://dqm/soteria/reports/individual/excel.php?method=year' + (activeDelegate ? "&delegate=" + activeDelegate : ""), "", "width=50, height=50");
						}
					},
					{
						title: "All Observations",
						onclick: function(){
							window.open('http://dqm/soteria/reports/individual/excel.php?method=all' + (activeDelegate ? "&delegate=" + activeDelegate : ""), "", "width=50, height=50");
						}
					}
				]
			}, 
			{
				title: 'Search Employee Observation',
				img: 'search.png',
				onclick: function(){
					window.location = 'http://dqm/soteria/reports/individual/searchobs.php' + (activeDelegate ? "?delegate=" + activeDelegate : "");
				}
			}
		],
		orgreports: [
			{ 
				title: 'Weekly Report-out',
				onclick: function(){
					window.location = 'http://dqm/soteria/reports/organization/reportout.php' + (activeDelegate ? "?delegate=" + activeDelegate : "");
				}
				
			}, 
			{
				title: 'IO Trending',
				onclick: function(){
					window.location = 'http://dqm/soteria/reports/organization/iotrending.php' + (activeDelegate ? "?delegate=" + activeDelegate : "");
				}
			}/*, 
			{
				title: 'DO SO Crew Report'
			}, 
			{
				title: 'Red Tag Audits'
			}
			*/
		],
		settings: [
			{ 
				title: 'Delegates',
				img: 'delegates.png'
			},
			{	
				title: 'Notifications'
			}
		],
		help: [
			{
				title: 'SOTeria Help'
			},
			{
				title: 'SOTeria Mobile Help'
			},
			{
				title: 'About SOTeria',
				onclick: function(){
					window.location = 'http://dqm/soteria/help/about.php' + (activeDelegate ? "?delegate=" + activeDelegate : "");
				}
			}
		]
		
	};

	var subMenu = function(menuItem){


	};
	
	var setUpOnOnclickOnMouse = function(e, subMenu, onclick, disabled){
		if(disabled){
			e.onmouseover = null;
			e.onmouseout = null;
			e.onclick = null;
		} else {
			e.onmouseover = function(){
				e.style.border = '1px solid ' + hoverBorder;
				e.style.backgroundColor = hoverBgColor;
				e.style.color = '#000';
				
				if(subMenu){
					subMenu.style.display = 'block';
				}
			};
			e.onmouseout = function(){
				e.style.border = '1px solid transparent';
				e.style.backgroundColor = bgColor;
				e.style.color = color;
				
				if(subMenu){
					subMenu.style.display = 'none';
				}
			};
			e.onclick = function(){
				if(onclick){ 
					onclick(); 
				}
				
				if(subMenu){
					subMenu.style.display = 'none';
				}
				
				if(e.parentNode.id.indexOf("subMenu") != -1){
					e.parentNode.style.display = 'none';
				}
				
				hideMenu();
			};
		}
		
	};

	var hideMenu = function(menuItem){
		try {
			dojo.byId(menuItem + "_menu").style.display = 'none';
		} catch (ex) {
			if(activeMenu){
				dojo.byId(activeMenu + "_menu").style.display = 'none';
			}
		}
		
		for(var i in subMenus){
			subMenus[i].style.display = 'none';
			//alert(subMenus[i].id);
		}
		
		activeMenu = false;
	};
	
	return {
		showMenu: function(menuItem){
		
			if(activeMenu && activeMenu != menuItem){
				hideMenu(activeMenu);
			}
			dojo.byId(menuItem).onclick = function(){
				sot.tools.menu.showMenu(menuItem);
				return false;
			};
			
			var _e = window.event;
			if(_e.type == 'click'){
				contextStartedWithAClick = true;
			}
			
			if(_e.type == 'mouseover' && !contextStartedWithAClick){
				return;
			}
			
			if(!menuItems[menuItem]){
				return false;
			}
			
			if(menuItems[menuItem].length == 0){
				return false;
			}
			
			if(dojo.byId(menuItem + "_menu")){ //if already build
				for(var i in subMenus){
					
				}
				dojo.byId(menuItem + "_menu").style.display = 'block';
				
			} else { //build
				var pos = position(dojo.byId(menuItem));
				var menu = document.createElement('div');
				menu.setAttribute('id', menuItem + "_menu");
				menu.style.position = 'absolute';
				menu.style.top = pos.top + 20 + 'px';
				menu.style.left = pos.left + 'px';
				menu.style.borderLeft = '1px solid #3d3d3d';
				menu.style.borderBottom = '1px solid #3d3d3d';
				menu.style.borderRight = '1px solid #3d3d3d';
				//menu.style.borderBottom = '2px inset #000';
				//menu.style.borderRight = '2px inset #000';
				menu.style.backgroundColor = bgColor;
				menu.style.padding = '1px';
				menu.style.zIndex = '9999';
				
				dojo.connect(document, 'onclick', function(){
					var e = window.event;
					if(activeMenu && dojo.byId(activeMenu) != e.srcElement){
						hideMenu();
						contextStartedWithAClick = false;
					}
					
				});
				
				menu.onmouseover = function(){
					menu.style.display = 'block';
				};
				
				var maxLength = 0;
				var menuItemsCt = 0;
				
				var divider = document.createElement('div');
				divider.style.borderTop = '1px solid #000'
				divider.style.borderBottom = '1px solid #3d3d3d';
				divider.style.marginBottom = '1px';
				
				subMenus = [];	
				for(var i in menuItems[menuItem]){
					
					var _menuItem = document.createElement('div');
					_menuItem.style.height = '18px';
					_menuItem.style.fontSize = '11px';
					_menuItem.style.paddingRight = '5px';
					_menuItem.style.fontFamily = 'Segoe ui';
					_menuItem.style.cursor = 'pointer';
					_menuItem.style.border = '1px solid transparent';
					_menuItem.style.color = color;
					
					var img = document.createElement('img');
					img.style.paddingRight = '15px';
					img.style.width = '15px';
					
					var text = document.createElement('span');
					text.style.top = '2px';
					text.style.position  = 'relative';
					
					if(menuItems[menuItem][i].img){
						img.setAttribute('src', "http://dqm/soteria/src/img/" + menuItems[menuItem][i].img);
						img.style.paddingTop = '1px';
						_menuItem.appendChild(img);
						
					} else if(menuItem == 'delegate' && menuItems[menuItem][i].active == 1){
						var _active = document.createElement('button');
						_active.style.width = '20px';
						_active.style.height = '18px';
						_active.style.textAlign = 'center';
						
						_active.style.border = '1px solid #6bb6ff';
						_active.style.backgroundColor = '#e7f3ff';
						_active.style.marginRight = '10px';
						
						var check = document.createElement('img');
						check.style.position = 'relative';
						check.style.top = '-3px';
						check.setAttribute('src', 'http://dqm/soteria/src/img/check.gif');
						_active.appendChild(check);
					
						text.style.top = '-2px';
						
						_menuItem.appendChild(_active);
						activeDelegate = menuItems[menuItem][i].d_id;

					} else {
						text.style.left = '30px';
					}
					
					text.innerHTML = menuItems[menuItem][i].title;
					
					_menuItem.appendChild(text);
					
					var _subMenu = null;
					var maxLength_s = 0
					if(menuItems[menuItem][i].subMenu && menuItems[menuItem][i].subMenu.length > 0){
						_subMenu = document.createElement('div');
						_subMenu.setAttribute('id', menuItem + "_subMenu_" + i);
						_subMenu.style.position = 'relative';
						_subMenu.style.top = '-15px';
						_subMenu.style.border = '1px solid #3d3d3d';
						_subMenu.style.backgroundColor = bgColor;
						_subMenu.style.padding = '1px';
						_subMenu.style.zIndex = '9998';
						_subMenu.style.display = 'none';
						
						for(var x in menuItems[menuItem][i].subMenu){
							var _subMenuItem = document.createElement('div');
							_subMenuItem.style.height = '18px';
							_subMenuItem.style.fontSize = '11px';
							_subMenuItem.style.paddingRight = '5px';
							_subMenuItem.style.fontFamily = 'Segoe ui';
							_subMenuItem.style.cursor = 'pointer';
							_subMenuItem.style.border = '1px solid transparent';
							_subMenuItem.style.paddingLeft = '30px';
							_subMenuItem.style.color = color;
							
							setUpOnOnclickOnMouse(_subMenuItem, null, menuItems[menuItem][i].subMenu[x].onclick);
							_subMenuItem.innerHTML = menuItems[menuItem][i].subMenu[x].title;
							
							if(menuItems[menuItem][i].subMenu[x].title == 'All Observations'){
								_subMenu.appendChild(divider);
							}
							
							_subMenu.appendChild(_subMenuItem);
							if(menuItems[menuItem][i].subMenu[x].title.length > maxLength_s){
								maxLength_s = menuItems[menuItem][i].subMenu[x].title.length;
							}
						}
						_subMenu.style.width  = (maxLength_s * 8) + 20 + 'px';
						_menuItem.appendChild(_subMenu);
						subMenus.push(_subMenu);
					}
					
					setUpOnOnclickOnMouse(_menuItem, _subMenu, menuItems[menuItem][i].onclick);
					
					if(menuItems[menuItem][i].title == 'About SOTeria'){
						menu.appendChild(divider);
					}
					
					if(menuItems[menuItem][i].title == 'Turn Off'){
						menu.appendChild(divider);
						if(!activeDelegate){
							_menuItem.style.color = 'gray';
							setUpOnOnclickOnMouse(_menuItem, null, menuItems[menuItem][i].onclick, true);
						} 
					}
			
					if(menuItems[menuItem][i].title.length > maxLength){
						maxLength = menuItems[menuItem][i].title.length;
					}
		
					menu.appendChild(_menuItem);
					menuItemsCt++;
				}
				
				menu.style.width = (maxLength * 8) + 20 + 'px';
				
				for(var i in subMenus){
					subMenus[i].style.left = (maxLength * 8) + 20 + 'px';
				}
				document.body.appendChild(menu);
			}
			
			activeMenu = menuItem;
		},
		
		addDelegator: function(_d){
			menuItems["delegate"].push({
				title: _d.name,
				onclick: function(){
					window.location = '?delegate=' + _d.d_id;
				},
				active: _d.active,
				d_id: _d.d_id
			});
			
			if(_d.active == 1){
				activeDelegate = _d.d_id;
			}
		}
	};
}();