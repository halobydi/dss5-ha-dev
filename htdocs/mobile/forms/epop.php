
   <script>
function openFieldForm() {
    window.location.assign("http://lnx829.dteco.com:63002/forms/test3.php")
}
</script>
<?php
	function errorHandler($errno, $errstr, $errfile, $errline, $errcontext){
		
	}
	set_error_handler('errorHandler');
	
	require_once('mcl_Html.php');
	require_once('mcl_Oci.php');
	require_once("/opt/apache/servers/soteria/htdocs/src/php/auth.php");
	$user = auth::check();
	$login_warning = '';
	if($user["status"] != "authorized"){
		$login_warning = '<div class="form_warning">
			<b>Warning</b>: You are not logged into the application. Submitting an observation without logging in will result in an anonymous observation. <a href="../index.php">Click here</a> to log in.
		<div>';
	}
	
	$type = $_GET['type'] == 'office' ? 'office' : 'field';
	$switchType = $_GET['type'] == 'office' ? 'field' : 'office';

	mcl_Html::title("SOTeria - Enterprise Performance Observation");
	mcl_Html::js(mcl_Html::DOJO);
	mcl_Html::js(mcl_Html::DOJO_WINDOW);
	mcl_Html::js(mcl_Html::AJAX);

	mcl_Html::s(mcl_Html::INC_CSS, "../src/css/form.css");
	mcl_Html::s(mcl_Html::INC_CSS, "../src/css/mobile.css");
	mcl_Html::s(mcl_Html::INC_CSS, "../src/css/bootstrap.css");
	mcl_Html::s(mcl_Html::INC_JS, "../src/sot.js");
	
	mcl_Html::s(mcl_Html::SRC_CSS, "
		.backgroundDiv{
			background: rgb(169,228,247); /* Old browsers */
			background: -moz-linear-gradient(top,  rgba(169,228,247,1) 0%, rgba(15,180,231,1) 100%); /* FF3.6+ */
			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(169,228,247,1)), color-stop(100%,rgba(15,180,231,1))); /* Chrome,Safari4+ */
			background: -webkit-linear-gradient(top,  rgba(169,228,247,1) 0%,rgba(15,180,231,1) 100%); /* Chrome10+,Safari5.1+ */
			background: -o-linear-gradient(top,  rgba(169,228,247,1) 0%,rgba(15,180,231,1) 100%); /* Opera 11.10+ */
			background: -ms-linear-gradient(top,  rgba(169,228,247,1) 0%,rgba(15,180,231,1) 100%); /* IE10+ */
			background: linear-gradient(to bottom,  rgba(169,228,247,1) 0%,rgba(15,180,231,1) 100%); /* W3C */
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#a9e4f7', endColorstr='#0fb4e7',GradientType=0 ); /* IE6-9 */
		}
		.form_warning {
			background-color: 	#ffd7d7; 
			border: 			1px solid red; 
			padding: 			3px 3px 3px 3px; 
			margin-top: 		1px; 
			text-align:			center;
			color: 				red; 
			word-wrap:			break-work; 
			font-size: 			10px; 
			font-weight: 		bold;
			margin:				0 auto;
		}
		
		tr.header {
			font-weight: bold;
			background-color: #59c9ff;
			height: 30px;
		}
	");
	
	$oci = new mcl_Oci('soteria');
	
	$sql = "
		SELECT * FROM ORGANIZATIONS WHERE ACTIVE = 1 ORDER BY ORG_CODE
	";
	
	$orgs = array();
	while($row = $oci->fetch($sql)){
		if(empty($org) && !empty($org_desc)){
			if($org_desc == $row["ORG_TITLE"]){
				$org = $row["ORG_CODE"];
			} else if(strpos($row["ORG_DESCRIPTION"], $org_desc)){
				$org = $row["ORG_CODE"];
			}
		}
		
		$orgs[$row["ORG_CODE"]] = array(
			"code"		=> $row["ORG_CODE"],
			"title"		=> $row["ORG_TITLE"]
		);	
	}
	
	$category = "";
	$swo = false;
	$qew = false;
	$pp = false;
	
	if(isset($_GET["swo"])){ $swo = true; }
	if(isset($_GET["qew"]) && !$swo){ $qew = true; }
	if(isset($_GET["pp"])){ $pp = true; }
	if(!$pp && !$qew){ $swo = true; }
	if($swo){ $category .= "'Leader', 'Individual', 'Organization'"; }
	if($qew){ $category .= (empty($category) ? "" : ", ") . "'Qualified Electrical Worker'"; }
	if($pp) { $category .= (empty($category) ? "" : ", ") . "'Paired Performance'";}

	$test = $_GET["delegate"];

    if(!empty($_GET["delegate"])){
        $x = 5+5;
    }

    if ($type == 'field' && $swo){
        $host  = $_SERVER['HTTP_HOST'];
        //$uri  = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
        $d = $_GET["delegate"];

        $u = $_GET["usid"];
        header( "Location: http://" . $host . "/forms/lifeCritical.php" . ( $d != null ? "?delegate=" . $d . "&" : "?"  )
            . ( $u != null ? "usid=" . $u : "")) ;

    }

	if(!empty($_GET["usid"])){
		mcl_Html::s(mcl_Html::SRC_JS,"
			window.onload = function(){
				sot.swo.inputUsid(dojo.byId('usid_1'), 1);
			}
		");
	}
?>
<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,minimum-scale=1,user-scalable=no"/>
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
<center>
<div class='backgroundDiv'/>
	<div id="topHeader">
		SOTeria
	</div>

	<div class='fancy' style='width: 85% !important;'>
		<div class='header' style='width: 100%;'>
			Enterprise Performance Observation
		</div>
		<?php
			$url = "epop.php?sid=" . time();
			$url2 = "";
			if($swo){ $url .= "&swo"; $url2 .= "&swo"; }
			if($qew){ $url .= "&qew"; $url2 .= "&qew";}
			if($pp){ $url .= "&pp"; $url2 .= "&pp";}
			
			$completed_by = auth::check("usid");
			if(!empty($_GET["delegate"])){
				$delegates = auth::check('delegates');
				if(!empty($delegates[$_GET["delegate"]]["name"])){
					$delegator = " for {$delegates[$_GET["delegate"]]["name"]}";
					$completed_by = $delegates[$_GET["delegate"]]["usid"];
				}
						
				echo "<div class='form_warning'>
					<b>Warning</b>: You are in delegate mode{$delegator}. This observation will be saved as completed by you, but your delegator will receive the leader completion credit. <a href='{$url}'>Click here</a> to turn off delegate mode.
				</div>";
			}
			if(!empty($login_warning)){
				echo "<div class='form_warning'>
					{$login_warning}	
				</div>";
			}
		?>
		<div id='container' style='width: 100%;'>
			<form name = 'swo' action = '../../src/php/engine.php?f=saveEPOP&mobile=true<? echo "{$url2}"; echo (!empty($_GET["delegate"]) ? "&delegate={$_GET["delegate"]}" : "")?>' method='POST' target='submit' onsubmit='return sot.swo.onSubmit();' target='submit' enctype="multipart/form-data">
				<center>
				<table style='width: 100%;'>
					<tr>
						<td style='width: 20%; font-weight:bold;'>Employee Username:</td>
						<td><input style='width: 80%;' type='text' maxlength='6' id='usid_1' name='usid[]' value="<?=$_GET["usid"]?>" maxlength='6' onkeyup='sot.swo.inputUsid(this, 1);'/></td>
					</tr>
					<tr>
						<td style='font-weight:bold;'>Date Observed:</td>
						<td>
							<input style='width: 80%;' type='text' id='observed_date' maxlength='10' name='observed_date' id='observed_date' value="<?=date("m/d/Y")?>"/>
						</td>
					</tr>
					<tr>
						<td style='font-weight:bold;'>Employee Name: </td>
						<td>
							<input style='width: 80%;' type='text' maxlength='350' id='name_1' name='name[]' value="<?=$name?>"/>
							<input type='hidden' id='nameValidate_1' value="<?php echo $name; ?>"/>
						</td>
					</tr>
					<tr>
						<td style='font-weight:bold;'>Observed By:</td>
						<td>
							<input style='width: 80%;' type='text' id='observed_by' name='observed_by' value="<?=is_array(auth::check("name")) ? "" : auth::check("name")?>"/>
							<input type='hidden' id='completed_by' name='completed_by' value="<?php echo $completed_by; ?>"/>
						</td>
					</tr>
					<tr>
						<td style='font-weight:bold;'>Organization:</td>
						<td>
							<select style='width: 80%;' name='org' id='org' onchange='sot.swo.switchOrg();'>
								<option></option>
								<?php
									foreach($orgs as $key=>$value){
										$selected = ($org == $key ? 'selected=selected' : '');
										echo "<option value='{$key}' {$selected}>{$value["title"]}</option>";
									}
								?>
							</select>
						</td>
					</tr>
					<tr>
						<td style='font-weight:bold;'>Observation Type:</td>
						<td>
                            <?php
                                $uri = "?type=" . $switchType;
                                if (!empty($_REQUEST['usid'])){
                                    $uri = $uri . "&usid=" . $_GET["usid"];
                                }
                                if (!empty($_REQUEST['delegate'])){
                                    $uri = $uri . "&delegate=" . $_GET["delegate"];
                                }
                                if ($swo){
                                    $uri = $uri . "&swo";
                                }
                                if ($qew){
                                    $uri = $uri . "&qew";
                                }
                                if ($pp){
                                    $uri = $uri . "&pp";
                                }
                            ?>

							<?=ucfirst($type)?>
							- <a href='<?=($uri)?>' id='switchType'>Switch to <?=ucfirst($switchType)?></a>
							<input type='hidden' name='office_field' id='office_field' value='<?=strtoupper($type)?>' />
						</td>
					</tr>
					<tr id='location_c' style=''>
						<td style='font-weight:bold;'>Location:</td>
						<td colspan='3'>
							<input style='width: 80%;' type='text' maxlength='350' id='location' name='location' value=""/>
						</td>
					</tr>
				</table>
				</center>
				
				<center style='margin-top:10px; margin-bottom: 10px;'>
					<button style='margin-bottom: 20px;' id="markAllSatisfactoryBtn" class='btn' onclick="sot.swo.markAllSat(); return false;">Mark All Satisfactory</button>&nbsp;
					<button style='margin-bottom: 20px;' class='btn' onclick='window.location="../index.php<?=(!empty($_GET["delegate"]) ? "?delegate={$_GET["delegate"]}" : "")?>"; return false;'>Cancel & Go Back</button>
				</center>
				<table style='width: 100%; word-break: break-word;'>
			<?php
				if($swo){
					echo "<input type='hidden' name='swo' value ='on'/>";
				}
				
				$sql = "SELECT * FROM EPOP_ITEMS WHERE ACTIVE = 1 AND ITEM_CATEGORY IN({$category}) AND {$type} = 1 ORDER BY ITEM_CATEGORY DESC, ITEM_NUM";
				$prev_category = '';
				$firstpass = true;
				
				function printHeader($category) {
					echo "
						<tr class='header'>
							<td style='width: 30px; text-align:center;'>S</td>
							<td style='width: 30px; text-align:center;'>IO</td>
							<td style='width: 30px; text-align:center;'>NA</td>
							<td style='text-align:center;'>{$category}</td>
							<td></td>
						</tr>
					";
				}
				
				function printQuestion($row, $x) {
					if($row["HAS_SUBITEMS"] == '1'){ $has_subItems = 'true'; } else { $has_subItems = 'false'; } 
					$bg = ($x++ % 2 == 0 ? 'even' : 'odd');
					
					echo "
						<tr class='" . $bg . "'>
							<td style='width: 30px; text-align: center; vertical-align: top;'>
								<input style='display: none; margin-left: 5px;' type='radio' class='S' id = 'S_{$row["ITEM_NUM"]}' name='{$row["ITEM_NUM"]}' value='S'/>
								<div style='margin-left: 5px;' id='bigRadio_S_{$row["ITEM_NUM"]}' class='radio-btn' onClick='sot.swo.mark(this, \"S_{$row["ITEM_NUM"]}\", \"{$row["ITEM_NUM"]}\"); sot.swo.markItem({$row["ITEM_NUM"]}, \"S\", {$has_subItems}, true);'><div class='radio-btn-inner'></div></div>
							</td>
							<td style='width: 30px; text-align: center; vertical-align: top;'>
								<input style='display: none;' type='radio' class='IO' id = 'IO_{$row["ITEM_NUM"]}' name='{$row["ITEM_NUM"]}' value='IO'/>
								<div id='bigRadio_IO_{$row["ITEM_NUM"]}' class='radio-btn' onClick='sot.swo.mark(this, \"IO_{$row["ITEM_NUM"]}\", \"{$row["ITEM_NUM"]}\"); sot.swo.markItem({$row["ITEM_NUM"]}, \"IO\", {$has_subItems}, true);'><div class='radio-btn-inner'></div></div>
							</td>
							<td style='width: 30px; text-align: center; vertical-align: top;'>
								<input style='display: none;' type='radio' class='NA' id = 'NA_{$row["ITEM_NUM"]}' name='{$row["ITEM_NUM"]}' value='NA'/>
								<div id='bigRadio_NA_{$row["ITEM_NUM"]}' class='radio-btn' onClick='sot.swo.mark(this, \"NA_{$row["ITEM_NUM"]}\", \"{$row["ITEM_NUM"]}\"); sot.swo.markItem({$row["ITEM_NUM"]}, \"NA\", {$has_subItems}, true);'><div class='radio-btn-inner'></div></div>
							</td>
							<td>{$row["ITEM_TEXT"]}</td>
							<td style='width: 30px; text-align: right;'>
								<img id='{$row["ITEM_NUM"]}_comments_img' style='max-width: 32px; cursor: pointer; margin-right: 5px;' src='../src/img/m.comment.png' style='cursor:pointer;' alt='Add Comment' onclick='sot.swo.addComment({$row["ITEM_NUM"]}, this);'/>
							</td>
						</tr>
						<tr class='" . $bg . "'>
							<td colspan='5' id='{$row["ITEM_NUM"]}_comments'></td>
						</tr>
					";
					
					if($has_subItems == 'true'){
						echo "<tr class='" . $bg . "' style='padding:0px; margin:0px;'>
								<td id='{$row["ITEM_NUM"]}_subitems' colspan='5' padding:0px; margin:0px; background-color:#ffffca;'>
								</td>
							</tr>
						";
					}	
				}
				
				while($row = $oci->fetch($sql)){
					if($row["ITEM_CATEGORY"] != $prev_category){
						$x = 0;
						
						if(!$firstpass){
							echo "
								<tr style='height:30px;'>
									<td colspan=5></td>
								</tr>
							";
						}
						printHeader($row['ITEM_CATEGORY']);
				
						$firstpass = false;
					}
			
					$x++;
					printQuestion($row, $x);
					$prev_category = $row["ITEM_CATEGORY"];		
				}
			?>
			</table>
			<?php	if(strtoupper($type) == 'OFFICE'): ?>
				<div style='margin-top: 10px; margin-bottom: 5px;'>
					Was this observation conducted at an <b>offsite</b> location?
					<select id='offsite_onsite' style='width: 80px;' onchange='sot.swo.toggleSiteItems(this.value);'>
						<option value='onsite'>
							No
						</option>
						<option value='offsite'>
							Yes
						</option>
					</select>
				</div>
				<div id='offsite' style='display: none;'>
				
				</div>
				<div id='onsite'>
					<?php 
						$sql = "SELECT * FROM EPOP_ITEMS WHERE ITEM_CATEGORY = 'Onsite' AND OFFICE = 1";
						$siteItems = array();
						while($row = $oci->fetch($sql)) {
							$siteItems[] = $row;
						}
					?>
					<table style='width: 100%;'>
						<?=printHeader('Onsite')?>
						<?php foreach($siteItems as $x=>$row): ?>
							<?=printQuestion($row, $x); ?>
						<?php endforeach; ?>
					</table>
				</div>
			<?php endif; ?>
			<?php
				if(!$qew && $swo){
				 echo <<<QEW
					<table style='width: 100%;'>
						<tr>
							<td style='font-weight:bold;vertical-align: bottom; margin-bottom: 5px;'>
								<div style='float: left; position: relative; z-index: 100;'>
									<input id='qew_checkbox' type = 'checkbox' name='qew' class="regular-checkbox big-checkbox" onclick='sot.swo.addItems(this.checked, "qew", "Qualified Electrical Worker");'/>
									<label for='qew_checkbox' style='vertical-align: bottom;'></label>
								</div>
								<div style="position: relative; top: 10px; left: 5px;">
									Qualified Electrical Worker Observation Items
								</div>
							</td>
						</tr>
					</table>
QEW;
				}
			?>
				<div id='qew' style='display: none;'>
				</div>
				<div style='margin-top: 10px; margin-bottom: 5px;'>
					Did a conversation take place with the observed individual?
					<select name='conversation' style='width: 80px;'>
						<option value=''></option>
						<option value='0'>No</option>
						<option value='1'>Yes</option>
					</select>
				</div>
				<div style='margin-top: 5px; margin-bottom: 5px;'>
					Was good safety behavior recognized during this observation?
					<select name='good_behavior' style='width: 80px;'>
						<option value=''></option>
						<option value='0'>No</option>
						<option value='1'>Yes</option>
					</select>
				</div>
				<div style='font-weight: bold; position: relative; margin-top: 10px;'>Photos</div>
				<div id='photos'>
					<input type='hidden' id='p_count' name='p_count' value='0'>
				</div>
				<button class='btn' onclick='addPhoto(); return false;'><i style="vertical-align: top; padding-right: 5px;" class="icon-plus icon-black"></i>Add Photo</button>
				
				<table style='width: 100%;'>
					<tr>
						<td style='font-weight:bold;vertical-align: bottom;'>Additional Comments</td>
					</tr>
					<tr>
						<td><textarea style='width:99%; height: 100px;' id='comments' name='comments' onkeypress='return sot.swo.imposeMaxLength(this, 1000);'></textarea></td>
					</tr>
				</table>
				<button class='btn' onclick='sot.swo.addEmployee(); return false;'><i style="vertical-align: top; padding-right: 5px;" class="icon-plus icon-black"></i>Add Additional Employee to Observation</button>
				<div id='add_emp' style='margin-top: 20px;'>
				</div>
				<center style='margin-top:10px; margin-bottom: 10px;'>
					<input type = 'hidden' value = '<?=$_GET["delegate"]?>' name = 'delegator'/>
					<input type = 'hidden' value = '<?php echo date("m/d/Y H:i:s"); ?>' name = 'begin_date'/>
					<input style='margin-bottom: 20px;' class='btn btn-primary' type = 'submit' id='submit' value='Submit'>&nbsp;
					<button style='margin-bottom: 20px;' class='btn' onclick='window.location="../index.php<?=(!empty($_GET["delegate"]) ? "?delegate={$_GET["delegate"]}" : "")?>"; return false;'>Cancel & Go Back</button>
				</center>
			</form>
		</div>
	</div>
	<iframe name='submit' style='width:0px; height:0px;'></iframe>
	</center>
</div>