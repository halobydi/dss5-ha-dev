<?php
	function errorHandler($errno, $errstr, $errfile, $errline, $errcontext){
		
	}
	set_error_handler('errorHandler');
	
	require_once('mcl_Html.php');
	require_once('mcl_Oci.php');
	require_once("/opt/apache/servers/soteria/htdocs/src/php/auth.php");
	$user = auth::check();
	if($user["status"] != "authorized"){
		header("Location: ../index.php");
	}
	
	mcl_Html::title("SOTeria - Red Tag Audits");
	mcl_Html::js(mcl_Html::DOJO);
	mcl_Html::js(mcl_Html::DOJO_WINDOW);
	mcl_Html::js(mcl_Html::AJAX);

	mcl_Html::s(mcl_Html::INC_CSS, "../src/css/form.css");
	mcl_Html::s(mcl_Html::INC_CSS, "../src/css/mobile.css");
	mcl_Html::s(mcl_Html::INC_CSS, "../src/css/bootstrap.css");
	mcl_Html::s(mcl_Html::INC_JS, "../src/sot.js");
	mcl_Html::s(mcl_Html::INC_JS, "../src/js/plugins/jquery-1.8.3.min.js");
	
	mcl_Html::s(mcl_Html::SRC_CSS, "
		.backgroundDiv{
			background: rgb(169,228,247); /* Old browsers */
			background: -moz-linear-gradient(top,  rgba(169,228,247,1) 0%, rgba(15,180,231,1) 100%); /* FF3.6+ */
			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(169,228,247,1)), color-stop(100%,rgba(15,180,231,1))); /* Chrome,Safari4+ */
			background: -webkit-linear-gradient(top,  rgba(169,228,247,1) 0%,rgba(15,180,231,1) 100%); /* Chrome10+,Safari5.1+ */
			background: -o-linear-gradient(top,  rgba(169,228,247,1) 0%,rgba(15,180,231,1) 100%); /* Opera 11.10+ */
			background: -ms-linear-gradient(top,  rgba(169,228,247,1) 0%,rgba(15,180,231,1) 100%); /* IE10+ */
			background: linear-gradient(to bottom,  rgba(169,228,247,1) 0%,rgba(15,180,231,1) 100%); /* W3C */
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#a9e4f7', endColorstr='#0fb4e7',GradientType=0 ); /* IE6-9 */
		}
		
		td {
			text-align: left;
		}
	");
	
	$oci = new mcl_Oci("soteria");
	
	if(!empty($_GET["id"])){
		
		$id = $_GET["id"];
		$sql = "SELECT R.*, TO_CHAR(R.AUDIT_DATE, 'MM/DD/YYYY') AS AUDIT_DATE  FROM RED_TAG_AUDITS R WHERE RTA_ID = {$id}";
	
		$data = $oci->fetch($sql);
		if($data["COMPLETE"] == 1){
			//die('Complete');
		}
		
		$sql = "SELECT * FROM RED_TAG_ANSWERS WHERE RTA_ID = {$id}";
		
		$data_answers = array();
		while($row = $oci->fetch($sql)){
			$data_answers[$row["ITEM_NUM"]] = $row["ANSWER"];
		}
		
		$sql = "SELECT RTA_ID, ATTACHMENT_ID, NAME AS A_NAME, EXTENSION FROM RED_TAG_ATTACHMENTS 
			WHERE RTA_ID = {$id}";
		
		$attachments = array();
		while($row = $oci->fetch($sql)){
			$attachments[] = $row;
		}
		
		if(!empty($data["ORG"])){
			$sql = "
				SELECT LOCATION FROM RED_TAG_AUDITS_REQ 
					WHERE ORG = '{$data["ORG"]}'
					ORDER BY LOCATION
			";
			
			while($row = $oci->fetch($sql)){
				$selected = ($row["LOCATION"] == $data["LOCATION"] ? "selected=selected" : "");
				$locations .= "<option value='{$row["LOCATION"]}' {$selected}>{$row["LOCATION"]}</option>";
			}
			
		}
		
	}
	
?>
<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,minimum-scale=1,user-scalable=no"/>
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
<center>
<div class='backgroundDiv'/>
	<div id="topHeader">
		SOTeria
	</div>

	<div class='fancy' style='width: 85% !important;'>
	<div class='header' style='width: 100%;'>
		Protective Tagging System Audit Checklist - OH/UG Lines
	</div>
	<div id='container' style='width: 100%;'>
		<form action = '../../src/php/engine.php?f=saveRedtag&mobile=true<?php echo (!empty($id) ? "?id={$id}" : ""); echo (!empty($_GET["delegate"]) ? "&delegate={$_GET["delegate"]}" : ""); ?>' method = 'POST' onsubmit = '' target= 'submit' enctype="multipart/form-data">
			<div class = 'container' style='width: 100%;'>
				<center>
				<table>
					<tr>
						<td style='width: 40%; text-align: left; height: 20px; font-weight: bold;' >
							Org. Audited &nbsp;
						</td>
						<td style='text-align: left;'>
							<select style='width: 80%;' name='org' id='org' class='select' onchange='sot.redtag.switchOrg();'>
								<option></option>

								<option value = 'Engineering' <?php echo ($data["ORG"] == "Engineering" ? "selected=selected" : "")?>>Engineering</option>
								<option value = 'Fuel Supply' <?php echo ($data["ORG"] == "Fuel Supply" ? "selected=selected" : "")?>>Fuel Supply</option>
								<option value = 'OH Lines' <?php echo ($data["ORG"] == "OH Lines" ? "selected=selected" : "")?>>OH Lines</option>
								<option value = 'Nuclear Generation' <?php echo ($data["ORG"] == "Nuclear Generation" ? "selected=selected" : "")?>>Nuclear Generation</option>
								<option value = 'Production' <?php echo ($data["ORG"] == "Production" ? "selected=selected" : "")?>>Production</option>
								<option value = 'Substation' <?php echo ($data["ORG"] == "Substation" ? "selected=selected" : "")?>>Substation</option>
								<option value = 'System Operations' <?php echo ($data["ORG"] == "System Operations" ? "selected=selected" : "")?>>System Operations</option>
								<option value = 'UG Lines' <?php echo ($data["ORG"] == "UG Lines" ? "selected=selected" : "")?>>UG Lines</option>
							</select>
						</td>
					</tr>
					<tr>
						<td style='width: 40%; text-align: left; font-weight: bold;' rowspan='1'>
							RSD #
						</td>
						<td>
							<input style='width: 80%;' type='text' name='rsd' id='rsd' maxlength='20' value='<?php echo $data["RSD"]?>'/>
						</td>
					</tr>
					<tr>
						<td style='width: 40%; text-align: left; font-weight: bold;' >
							Location &nbsp;
						</td>
						<td style='text-align: left;'>
							<select style='width: 80%;' name='location' class='select' id='location' onchange='sot.redtag.switchCreditMonth();'>
								<option></option>
								<?php echo $locations; ?>
							</select>
						</td>
					</tr>
					<tr>
						<td style='width: 40%; text-align: left;  font-weight: bold;'>
							Protection Leader(s) Name / ID
						</td>
						<td>
							<input style='width: 80%;' type='text' name='protection_leader' maxlength='250' value='<?php echo $data["PROTECTION_LEADER"]?>'/>
						</td>
					</tr>
					<tr>
						<td style='width: 40%; text-align: left;  font-weight: bold;'>
							Operating Authority
						</td>
						<td>
							<input style='width: 80%;' type='text' name='operating_authority' maxlength='495' value='<?php echo $data["OPERATION_AUTHORITY"]?>'/>
						</td>
					</tr>
					<tr>
						<td style='width: 40%; text-align: left;  font-weight: bold;'>
							Auditor(s) ID / Name / Signatures(s)
						</td>
						<td>
							<input style='width: 80%;' type='text' name='auditor' maxlength='250' value='<?php echo $data["AUDITORS"]?>'/>
						</td>
					</tr>
					<tr>
						<td style='width: 40%; text-align: left;  font-weight: bold;'>
							Location/Description of Equipment Shut Down
						</td>
						<td>
							<input style='width: 80%;' type='text' name='loc_desc_equip' maxlength='495' value='<?php echo $data["LOCATION_DESC_EQP_SHUTDOWN"]?>'/>
						</td>
					</tr>
					<tr>
						<td style='width: 40%; text-align: left;  font-weight: bold;'>
							Nature of Work
						</td>
						<td>
							<input style='width: 80%;' type='text' name='nature_work' maxlength='495' value='<?php echo $data["NATURE_OF_WORK"]?>'/>
						</td>
					</tr>
					<tr style='height: 20px;'>
						<td style='width: 40%; height: 20px; text-align: left;  font-weight: bold;'>
							Date of Audit
						</td>
						<td>
							<input style='width: 80%;' type='text' name='date' id='audit_date' maxlength='10' value='<?= ($data["AUDIT_DATE"] ? $data["AUDIT_DATE"] : date('m/d/Y')) ?>'/>
						</td>
					</tr>
					<tr>
						<td style='width: 40%; height: 20px;  font-weight: bold;'>
							Credit Month
						</td>
						<td style='height: 25px;'>
							<select style='width: 80%;' name='credit_month' id='credit_month' class='select' onchange='sot.redtag.switchCreditMonth();'>
							   <option value="1" <?php echo ($data["CREDIT_MONTH"] == "1" ? "selected=selected" : "")?>> January
							   <option value="2" <?php echo ($data["CREDIT_MONTH"] == "2" ? "selected=selected" : "")?>> February
							   <option value="3" <?php echo ($data["CREDIT_MONTH"] == "3" ? "selected=selected" : "")?>> March
							   <option value="4" <?php echo ($data["CREDIT_MONTH"] == "4" ? "selected=selected" : "")?>> April
							   <option value="5" <?php echo ($data["CREDIT_MONTH"] == "5" ? "selected=selected" : "")?>> May
							   <option value="6" <?php echo ($data["CREDIT_MONTH"] == "6" ? "selected=selected" : "")?>> June
							   <option value="7" <?php echo ($data["CREDIT_MONTH"] == "7" ? "selected=selected" : "")?>> July
							   <option value="8" <?php echo ($data["CREDIT_MONTH"] == "8" ? "selected=selected" : "")?>> August
							   <option value="9" <?php echo ($data["CREDIT_MONTH"] == "9" ? "selected=selected" : "")?>> September
							   <option value="10" <?php echo ($data["CREDIT_MONTH"] == "10" ? "selected=selected" : "")?>> October
							   <option value="11" <?php echo ($data["CREDIT_MONTH"] == "11" ? "selected=selected" : "")?>> November
							   <option value="12" <?php echo ($data["CREDIT_MONTH"] == "12" ? "selected=selected" : "")?>> December
							</select>
						</td>
					</tr>
				</table>
				</center>
			</div>
			<div style='width: 100%; color: red; text-align: center;' id='reminder'>
				&nbsp;
			</div>
			<div>
				<table style="width: 100%;">
				<?php

					$sql = "
						SELECT * FROM RED_TAG_ITEMS ORDER BY ITEM_NUM
					";
					
					while($row = $oci->fetch($sql)) {
						if($row["CATEGORY"] == "Interview - EM5-3 and Discipline Specific Procedure Comprehension:"){
							$checkbox = "
								<td style='width: 30px; text-align: center; vertical-align: top;'>
									<input id='checkbox_{$row["ITEM_NUM"]}' class='regular-checkbox big-checkbox' type='checkbox' " . ($data_answers[$row["ITEM_NUM"]] == "YES" ? "checked=checked" : "" ) ." onclick='dojo.byId(\"{$row["ITEM_NUM"]}\").value= this.checked ? \"YES\" : \"NO\"'/>
									<label for='checkbox_{$row["ITEM_NUM"]}' style='vertical-align: bottom; margin-top: 5px; margin-left: 5px;'></label>
									<input type='hidden' name='{$row["ITEM_NUM"]}' id='{$row["ITEM_NUM"]}' value='" . ($data_answers[$row["ITEM_NUM"]] == "YES" ? "NO" : "NO" ) ."'/>
								</td>
							";
							$radios = "";
						} else {
							$radios = "
								<td style='width: 30px; text-align: center; vertical-align: top;'>
									<input style='display: none;' type = 'radio' id='YES_{$row["ITEM_NUM"]}' name = '{$row["ITEM_NUM"]}' value = 'YES' " . ($data_answers[$row["ITEM_NUM"]] == 'YES' ? "checked=checked" : "") ."/>
									<div style='margin-left: 5px;' id='bigRadio_YES_{$row["ITEM_NUM"]}' class='radio-btn' onClick='sot.redtag.mark(this, \"YES_{$row["ITEM_NUM"]}\", \"{$row["ITEM_NUM"]}\");'><div class='radio-btn-inner " . ($data_answers[$row["ITEM_NUM"]] == 'YES' ? "checked" : "") . "'></div></div>
								</td>
								<td style='width: 30px; text-align: center; vertical-align: top;'>
									<input style='display: none;' type = 'radio' id='NO_{$row["ITEM_NUM"]}' name = '{$row["ITEM_NUM"]}' value = 'NO' " . ($data_answers[$row["ITEM_NUM"]] == 'NO' ? "checked=checked" : "") ."/>
									<div style='margin-left: 5px;' id='bigRadio_NO_{$row["ITEM_NUM"]}' class='radio-btn' onClick='sot.redtag.mark(this, \"NO_{$row["ITEM_NUM"]}\", \"{$row["ITEM_NUM"]}\");'><div class='radio-btn-inner " . ($data_answers[$row["ITEM_NUM"]] == 'NO' ? "checked" : "") . "'></div></div>
								</td>
								<td style='width: 30px; text-align: center; vertical-align: top;'>
									<input style='display: none;' type = 'radio' id='NA_{$row["ITEM_NUM"]}' name = '{$row["ITEM_NUM"]}' value = 'NA' " . ($data_answers[$row["ITEM_NUM"]] == 'NA' ? "checked=checked" : "") ."/>
									<div style='margin-left: 5px;' id='bigRadio_NA_{$row["ITEM_NUM"]}' class='radio-btn' onClick='sot.redtag.mark(this, \"NA_{$row["ITEM_NUM"]}\", \"{$row["ITEM_NUM"]}\");'><div class='radio-btn-inner " . ($data_answers[$row["ITEM_NUM"]] == 'NA' ? "checked" : "") . "'></div></div>
								</td>
							";
							$checkbox = "";
							
							if($row["CATEGORY"] != $prev) {
								echo "
									<tr class='" . ($x++ % 2 == 0 ? 'even' : 'odd') . "'>
										<td style = 'width: 30px; text-align: center; border-top: 1px solid black; font-weight: bold;'>&nbsp;Y</td>
										<td style = 'width: 30px; text-align: center; border-top: 1px solid black; font-weight: bold;'>&nbsp;&nbsp;N</td>
										<td style = 'width: 30px; text-align: center; border-top: 1px solid black; font-weight: bold;'>&nbsp;NA</td>
										<td style = 'text-align: center; border-top: 1px solid black; font-weight: bold;'>&nbsp;" . str_replace("\\", "", $row["CATEGORY"]) . "</td>
									</tr>
								";
							}
						}
						
						echo "
						<tr class='" . ($x++ % 2 == 0 ? 'even' : 'odd') . "'>
							{$radios}
							{$checkbox}
							<td style = 'padding-left: 15px;'>" . str_replace("\\", "", $row["ITEM_TEXT"]) . "</td>
						</tr>
						";
						if($row["ITEM_NUM"] == 23) {
							echo "
							</table>
							<table style='width: 100%;'>
								<tr style = 'border-top: 1px solid black; background-color:" . ($x++ % 2 == 0 ? '#d8d8d8' : '#ffffff') .";'>
									<td colspan=2 style = 'border-top: 1px solid black; font-weight: bold; text-align: center;'>&nbsp;Review all documents for times and dates to see that steps were handled in the appropriate, chronological order.</td>
								</tr>
							";
						}
						
						$prev = $row["CATEGORY"];
					}
				?>
				</table>
			</div>
			<div>
				<table style='font-weight: bold; width: 100%;'>
					<caption style="position: relative; bottom: 12px; margin-left: 5px;">Type of Audit (check all that apply)</caption>
					<tr>
						<td style = 'font-weight: normal; text-align: center;'>
							<div id='wrap-float-center'>
							<div id='content-float-center'>
								<div style="display: inline-block; text-align: left; float: left; padding-right: 10px;">
									<input id='audit_monthly' type = 'checkbox' name='audit_monthly' class="regular-checkbox big-checkbox" <?php echo ($data["AUDIT_MONTHLY"] == "1" ? "checked=checked" : "")?>/>
									<label style="vertical-align: bottom;" for="audit_monthly"></label><span style="position: relative; bottom: 12px; margin-left: 5px;">Monthly</span>
								</div>
								<div style="display: inline-block; float: left; padding-right: 10px;">
									<input id='audit_quarterly' type = 'checkbox' name='audit_quarterly' class="regular-checkbox big-checkbox" <?php echo ($data["AUDIT_QUARTERLY"] == "1" ? "checked=checked" : "")?>/>
									<label style="vertical-align: bottom;" for="audit_quarterly"></label><span style="position: relative; bottom: 12px; margin-left: 5px;">Quarterly</span>
								</div>
								<div style="display: inline-block; float: left; padding-right: 10px;">
									<input id='audit_followup_recheck' type = 'checkbox' name='audit_followup_recheck' class="regular-checkbox big-checkbox" <?php echo ($data["AUDIT_FOLLOWUP_RECHECK"] == "1" ? "checked=checked" : "")?>/>
									<label style="vertical-align: bottom;" for="audit_followup_recheck"></label><span style="position: relative; bottom: 12px; margin-left: 5px;">Follow-Up / Recheck</span>
								</div>
								<div style="display: inline-block; float: left; padding-right: 10px;">
									<input id='audit_onsite' type = 'checkbox' name='audit_onsite' class="regular-checkbox big-checkbox" <?php echo ($data["AUDIT_ON_SITE"] == "1" ? "checked=checked" : "")?>/>
									<label style="vertical-align: bottom;" for="audit_onsite"></label><span style="position: relative; bottom: 12px; margin-left: 5px;">On-Site</span>
								</div>
								<div style="display: inline-block; float: left;">
									<input id='audit_protection_authority' type = 'checkbox' name='audit_protection_authority' class="regular-checkbox big-checkbox" <?php echo ($data["AUDIT_PROTECTION_AUTHORITY"] == "1" ? "checked=checked" : "")?>/>
									<label style="vertical-align: bottom;" for="audit_protection_authority"></label><span style="position: relative; bottom: 12px; margin-left: 5px;">Protection Authority</span>
								</div>
							</div>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							Employees Interviewed: (Name / ID)
						</td>
					</tr>
					<tr>
						<td>
							<textarea style='width:99%; height: 100px;' name = 'employees_interviewed' onkeypress='return (this.value.length < 1000);'><?php echo str_replace("\\", "", $data["EMPLOYEES_INTERVIEW"]);?></textarea>
						</td>
					</tr>
					<tr>
						<td>
							Employees Supervisor(s): (Name / ID)
						</td>
					</tr>
						<tr>
						<td>
							<textarea style='width:99%; height: 100px;' name = 'employees_supervisors' onkeypress='return (this.value.length < 1000);'><?php echo str_replace("\\", "", $data["EMPLOYEES_SUPERVISORS"]);?></textarea>
						</td>
					</tr>		
					<tr>
						<td>
							Questions/Comments from Employees:
						</td>
					</tr>
					<tr>
						<td>
							<textarea style='width:99%; height: 100px;' name = 'questions_comments' onkeypress='return (this.value.length < 1000);'><?php echo str_replace("\\", "", $data["QUESTIONS_COMMENTS"]);?></textarea>
						</td>
					</tr>		
					<tr>
						<td>
							Findings:
						</td>
					</tr>
					<tr>
						<td>
							<textarea style='width:99%; height: 100px;' name = 'findings' onkeypress='return (this.value.length < 1000);'><?php echo str_replace("\\", "", $data["FINDINGS"]);?></textarea>
						</td>
					</tr>			
					<tr>
						<td>
							Recommendations:
						</td>
					</tr>
					<tr>
						<td>
							<textarea style='width:99%; height: 100px;' name = 'recommendations' onkeypress='return (this.value.length < 1000);'><?php echo str_replace("\\", "", $data["RECOMMENDATIONS"]);?></textarea>
						</td>
					</tr>			
					<tr>
						<td>
							Follow-Up / Resolution:
						</td>
					</tr>	
					<tr>
						<td>
							<textarea style='width:99%; height: 100px;' name = 'followup_resolution' onkeypress='return (this.value.length < 1000);'><?php echo str_replace("\\", "", $data["FOLLOWUP_RESOLUTION"]);?></textarea>
						</td>
					</tr>			
				</table>
			</div>
			<div style = 'margin-top: 5px; margin-bottom: 5px; text-align: center;'>
				<div style = 'font-weight: bold;'>
					Attached Images for Individual Crew Member Forms<br/>
					<button class='btn-link' onClick='window.open("../../help/download.php?FILE_NAME=red_tag_audit_attachments", "_blank"); return false;'/>Attachment SWI</button>
				</div>
				<input type = 'hidden' id = 'a_count' name = 'a_count' value = '0'/>
				<?php
					$x = 0;
					foreach($attachments as $value){
						echo "
						<div id = 'attachment_{$x}'>
							<input type = 'hidden' name = 'aid_{$x}' value = '{$value["ATTACHMENT_ID"]}'/>
							&bull;<input type = 'hidden' name = 'deleted_{$x}' id = 'deleted_{$x}' value = '0'/>
							<a href = '#' target = '_blank' onclick = 'window.open(\"../../forms/viewAttachment.php?id={$value["ATTACHMENT_ID"]}\", \"_blank\", \"menubar=0, toolbar=0, width=50, height=50\"); return false;'>{$value["A_NAME"]}</a>&nbsp;
							<span class = 'delete_a' onclick = 'sot.redtag.removeAttachment({$x});'>
								<a class='btn' style='margin-bottom: 10px;' href='#' onclick='return false;'>Remove</a>
							</span>
						</div>";

						$x++;
						
						echo "<script type = 'text/javascript'>
								a_count++;
							</script>
						";
					}

				?>
				<div id = 'attachments' style="padding-bottom: 20px;"></div>
				<div style='margin-margin-top: 5px;'>
					<button class='btn' onclick='sot.redtag.addAttachment(); return false;'><i style="vertical-align: top; padding-right: 5px;" class="icon-plus icon-black"></i>Add Attachment / Image</button>
				</div>
			</div>
			<br/>
			<div style="text-align: center;">
				<input style="display: none;" type = 'text' name = 'id' value = '<?php echo $_GET["id"]?>'/>
				<input style='margin-bottom: 20px;' class='btn btn-primary'  type = 'submit' value = 'Save' name='save' onclick = 'sot.tools.cover(true);'/>
				<input style='margin-bottom: 20px;' class='btn btn-primary'  type = 'submit' value = 'Save & Submit' name = 'submit' onclick = 'return sot.redtag.onSubmit();'/>
				<input style='margin-bottom: 20px;' class='btn'  type = "button" value = "Cancel & Go Back" name = "back" onclick = 'window.location = "../index.php?delegate=<?= $_GET['delegate'] ?>&obs=rta"; return false;'/>
			</div>
		</div>
	</div>
	<iframe name='submit' style='width:0px; height:0px;'></iframe>
	</center>
</div>