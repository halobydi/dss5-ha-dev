
   <script>
function openFieldForm() {
    window.location.assign("../forms/lifeCritical.php")
}
</script>
<?php
	require_once('src/php/require.php');
	mcl_Html::s(mcl_Html::SRC_JS, "
		dojo.ready(function(){
			sot.home.setReportDelegate({$_GET["delegate"]});
			sot.home.setQuarter({$quarter});
			sot.home.setDates('{$start}', '{$end}');
			sot.home.initiate('{$usid}');
		});
	");	
?>
<meta name="viewport" content="width=device-width, height=device-height">
<meta name='apple-mobile-web-app-status-bar-style' content='black'/>
<br/>
<center>
	<div style='max-width: 500px;'>
		<?php
			$forms = array(
				"s" => "Enterprise Performance Observation",
				"r" => "Red Tag Audit",
				"h" => "Anatomy of an Event Report",
				"nm" => "Near Miss Incident",
				"gc" => "Good Catch Incident",
				"sd" => "Storm Duty Form",
			);
			
			$class = array(
				"0"	=> "error",
				"1"	=> "success"
			);
			foreach($forms as $key=>$value) {
				if(isset($_GET[$key])) {
					echo "<div class='alert alert-{$class[$_GET[$key]]}'>";
					echo ($_GET[$key] == "1" ? "Successfully saved {$value}" : ($_GET[$key] == "0" ? "Unable to save {$value}" : ""));
					echo "</div>";
				}
			}
		?>
	</div>
	<div style= "display:<?= ($epopDisplay ? "block" : "none") ?>" id='epop'>
		<div id = 'container' style = 'clear: both; display: none;'></div>
		<input type='button' class='btn' style='width: 80%; max-width: 500px; margin-bottom: 20px;' type='button' value='Create New Field Observation' onclick='window.location="../forms/lifeCritical.php?type=field<?= (isset($_GET["delegate"]) ? "&delegate=" . $_GET["delegate"] : "") ?>"'/><br/>
		<input type='button' class='btn' style='width: 80%; max-width: 500px; margin-bottom: 20px;' type='button' value='Create New Office Observation' onClick='window.location="forms/epop.php?type=office<?= (isset($_GET["delegate"]) ? "&delegate=" . $_GET["delegate"] : "") ?>"'/><br/>
		<div style='display: none; max-width: 400px; font-size: 14px;' id='legend' class='pane'>
			<table>
				<caption><u>Legend</u></caption>
				<tr id='quarter1'>
					<td><div class='pane' style='border: 1px solid black; background-color:#56AD56;height:10px;width:10px;'></div></td>
					<td><div style='margin-left:4px;'>Employee has been observed for the quarter</div></td>
				</tr>
				<tr style='display: none;' id='week1'>
					<td><div class='pane' style='border: 1px solid black; background-color:#56AD56;height:10px;width:10px;'></div></td>
					<td><div style='margin-left:4px;'>Employee has been observed for the week</div></td>
				</tr>
				<tr id='quarter2'>
					<td><div class='pane' style='border: 1px solid black; background-color:#C33B34;height:10px;width:10px;'></div></td>
					<td><div style='margin-left:4px;'>Employee has not been observed for the quarter</div></td>
				</tr>
			</table>
		</div><br/>
	</div>

	<div style= "display:<?= ($rtaDisplay ? "block" : "none") ?>" id='rta'>
		<input type='button' class='btn' style='width: 80%; max-width: 500px; margin-bottom: 20px;' type='button' value='Create Red Tag Audit' onClick='window.location="forms/redtag.php?<?= (isset($_GET["delegate"]) ? "delegate=" . $_GET["delegate"] : "") ?>"'/><br/>
		<input type='button' class='btn' style='width: 80%; max-width: 500px; margin-bottom: 20px;' type='button' value='View Audits Created' onClick='sot.home.getRTAs();'/><br/>
		<div id='rta_container'></div>
	</div>
</center>