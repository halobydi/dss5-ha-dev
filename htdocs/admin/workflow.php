<?php
require_once("/opt/apache/servers/soteria/htdocs/src/php/require.php");

$privileges = auth::check('privileges');
if(!$privileges["MANAGE_NEAR_MISS"] && !$privileges["MANAGE_AOE"]){
	auth::deny();
}

$oci = new mcl_Oci("soteria");
$obs = array("NM"=>"Near Miss", "GC"=>"Good Catch", "HP"=>"Anatomy of an Event");

echo "<div style='border: 1px solid #000; margin: 5px; width: 500px; padding-bottom: 10px; background-color: #d8d8d8;'>";
	echo "<table style='width: 500px;'>";
		echo "
		<tr>
			<td colspan='2' style='font-weight: bold;'>Add an email to a Mailing List</td>
		</tr>
		";
		echo "
		<tr style='font-size: 10px;'>
			<td>Enter Email Address</td>
			<td>Select Workflow</td>
		</tr>
		";
		echo "
		<tr id='method_email'>
			<td><input type='text' style='border: 1px solid #000; width: 280px;' id='email'></td>
			<td>
				<select id='observation' style='width: 200px;'>";
				foreach($obs as $ob=>$name) {
					echo "<option value='{$ob}'>{$name}</option>";
				}
				echo "</select>
			</td>
		</tr>
		";
	echo "</table>";
echo "</div>";
echo "<input type='submit' value='Add' style='width: 70px; font-size: 10px; margin: 5px;' onclick='sot.home.addEmail();'/>";

foreach($obs as $ob=>$name) {
	$sql = "SELECT 		EMAIL
			FROM 		MAILING_LIST N 
			WHERE OBSERVATION = '{$ob}'
			ORDER BY 	EMAIL";

	echo "<div style='margin: 5px; width: 800px;'><b>Important Information About this Mailing List</b> - 
	Any employee on the Mailing List will recieve automatic emails when a {$name} observation is submitted or updated with workflow turned on. </div>";

	echo "<table class='tbl' style='width: 500px;'>";
	echo "<th><div class='inner' style='width: 420px;'>{$name} Emails</div></th>";
	echo "<th><div class='inner' style='width: 65px;'></div></th>";
	$x = 0;
	while($row = $oci->fetch($sql)){
		$style = "style = 'text-align: left; width: 300px;'";
		echo "<tr class = '" . ($x++ % 2 == 0 ? 'even' : 'odd') ."'>";
		echo "<td {$style}>{$row["EMAIL"]}</td>";
		echo "<td style='font-size: 10px;'>[ <a href = '#' onclick = 'sot.home.deleteEmail(\"{$row["EMAIL"]}\", \"{$ob}\"); return false;'>Delete</a> ]</td>";
		echo "</tr>";
	}
	if($x == 0 ) {
		echo "<tr><td colspan='2'>There are currently no emails in the mailing list.</td></tr>";
	}
	echo "</table>";
}
?>