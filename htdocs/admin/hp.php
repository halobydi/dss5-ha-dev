<?php
require_once("/opt/apache/servers/soteria/htdocs/src/php/require.php");
require_once("mcl_Oci.php");

mcl_Html::s(mcl_Html::SRC_CSS, "
	tr.even {
		background-color:	#f0f0f0;
	}
	tr.odd {
		background-color:	#d8d8d8;
	}
");

$privileges = auth::check('privileges');
if(!$privileges["MANAGE_AOE"]){
	auth::deny();
}
	
mcl_Html::js(mcl_Html::HIGHSTOCK);

if(empty($_GET["start"])) {
	$start = '01/01/' . date('Y');
}

$oci = new mcl_Oci("soteria");
$orgs_a = array();

$sql = "SELECT * FROM ORGANIZATIONS WHERE ACTIVE = 1 ORDER BY ORG_CODE";
$orgs_a = array();
while($row = $oci->fetch($sql)){
	$orgs_a[$row["ORG_CODE"]] = array(
		"code"		=> $row["ORG_CODE"],
		"title"		=> $row["ORG_TITLE"]
	);	
}
if(isset($_GET["org"])){ $org = $_GET["org"]; }
foreach($orgs_a as $key=>$value){
	$selected = ($org == $key ? 'selected=selected' : '');
	$org_s .= "<option value='{$key}' {$selected}>{$value["title"]}</option>";
}

mcl_Html::s(mcl_Html::SRC_JS, 
<<<JS
	var current = null;
	var toggle = function(form, hidePrevious) {
		if(dojo.byId(form).style.display == 'block') {
			dojo.byId(form).style.display = 'none';
			dojo.byId(form + '_toggle').innerHTML = '<img src=\"../../src/img/expand.png\"/>';
			current = form;
		} else {
			dojo.byId(form).style.display = 'block';
			dojo.byId(form + '_toggle').innerHTML = '<img src=\"../../src/img/collapse.png\"/>';
		}
	
	};

	var doExportHp = function() {
		var start = dojo.byId('start').value;
		var end = dojo.byId('end').value;
		var org = $('#org').val();
		var format = $('#exportFormType').val();
		if(format=='summary') {
			window.open("hp_excel_summary.php?start="+start+"&end="+end+((org !== '') ? "&org="+encodeURIComponent(org) : ""));
		}
		else {
			window.open("hp_excel_detail.php?start="+start+"&end="+end+"&format="+format+"&org="+encodeURIComponent(org));	
		}
		document.body.removeChild(dojo.byId("hp_popup"));
		sot.tools.cover(false);
	};

	var exportHp = function() {
		sot.tools.cover(true, 3);
		var wb = dojo.window.getBox();
		
		var hp_popup = document.createElement('div');
			hp_popup.setAttribute('id', 'hp_popup');
			hp_popup.style.padding = '0px';
			hp_popup.style.width = '450px';
			hp_popup.style.height = '450px';
			hp_popup.style.position = 'absolute';
			hp_popup.style.left = "50%";
			hp_popup.style.top = "50%";
			hp_popup.style.marginLeft = "-225px";
			hp_popup.style.marginRight = "-225px";
			hp_popup.style.zIndex = 8;
			
			var box = "<table class='tbl'>";
			box += "<tr><th colspan='2'><div class='inner_title'>Export Observations</div></th></tr>";
			box += "<tr>";
			box += "<th><div class='inner' style='width: 250px; height: 15px;'>Excel Format</div></th>";
			box += "<th><div class='inner' style='width: 200px; height: 15px;'></div></th>";
			box += "</tr>";
			
			var options = "";
			options += "<option value='summary' selected='selected'>Overview</option>";
			options += "<option value='single'>Single Line (No Countermeasures)</option>";
			options += "<option value='singlec2'>Single Line (Countermeasures)</option>";
			options += "<option value='multi'>Multi Line</option>";

			box += "<tr class='odd'>";
			box += "<td style='border: none;width:250px;'><select id='exportFormType' name='exportFormType'>" + options + "</select></td>";
			box += "<td style='border: none;width:200px;'><button style='width: 70px; height: 19px; position: relative;' onclick='doExportHp();'>Export</button>&nbsp;<button style='width: 70px; height: 19px; position: relative;' onclick='document.body.removeChild(dojo.byId(\"hp_popup\"));sot.tools.cover(false);'>Cancel</button></td>";
			box += "</tr>";
	
			box += "</table>";
			hp_popup.innerHTML = box;
			
		document.body.appendChild(hp_popup);
		setTimeout(function() {
			resizer();
			if(dojo.byId("hp_popup")) {
				//dojo.byId("hp_popup").style.visibility = 'visible';
			}
		}, 100);
	};
JS
);

echo "<div>
	<form method = 'GET' action='hp.php' style = 'overflow: hidden; width: 900px; padding: 5px;'>
		<table style = 'font-size: 12px; width:700px'>
			<tr><td colspan='4' style='font-size: 10px; font-weight: normal;'>Filter Reports by Date Submitted and/or Organization</td></tr>
			<tr style = 'vertical-align: bottom;'>
				<td>
					<select name='org' id='org' style = 'height: 20px; font-size: 10px;'>
						<option value=''>- All Organizations - </option>
						{$org_s}
					</select>
				</td>
				<td style='width:200'>
					<input style = 'height: 12px; border: 1px solid #000; width: 100px;' type = 'text' name = 'start' id = 'start' value = '{$start}'/> <img style = 'vertical-align: bottom;' src='../../src/img/calendar.gif' alt='' id='tcal' onmouseover='setup_cal(\"tcal\", \"start\");' />
				</td>
				<td>
					<input style = 'height: 12px; border: 1px solid #000; width: 100px;'  type = 'text' name = 'end' id = 'end' value = '{$end}' /> <img style = 'vertical-align: bottom;' src='../../src/img/calendar.gif' alt='' id='tcal2' onmouseover='setup_cal(\"tcal2\", \"end\");' />
				</td>
				<td style = 'text-align: left;'>
					" . (!empty($_GET["delegate"]) ? "<input type='hidden' name='delegate' value='{$_GET["delegate"]}'/>" : "") . "
					<input type = 'submit' style = \"height: 19px;\" value = 'Filter'/>
					<input onclick = \"dojo.byId('start').value = '{$startWeek}'; dojo.byId('end').value = '{$endWeek}';\" type = \"submit\" style = \"height: 19px;\" value = \"Current Week\"/>
					<input onclick = \"dojo.byId('start').value = '{$startWeekPast}'; dojo.byId('end').value = '{$endWeekPast}';\"  type = \"submit\" style = \"height: 19px;\" value = \"Past Week\"/>
				</td>
			</tr>
		</table>
	</form>
</div>
";

$sql = "
	SELECT	HP_ID AS \"ID\", 
			NVL(E.NAME, COMPLETED_BY) AS \"Completed By\", 
		    COMPLETED_BY_SUPERVISOR AS \"Supervisor\",
		    COMPLETED_BY_DIRECTOR AS \"Director\",
			TO_CHAR(INCIDENT_DATE, 'MM/DD/YYYY') AS \"Incident Date\",
			TO_CHAR(COMPLETED_DATE, 'MM/DD/YYYY') AS \"Completed Date\",
			EVENT_TYPE AS \"Event Type\",
			NVL(O.ORG_TITLE, 'Unknown') AS \"Organization\",
			COMMENTS AS \"Comments\"
	FROM	HP_OBSERVATIONS H
	LEFT JOIN EMPLOYEES E ON E.USID = COMPLETED_BY
	LEFT JOIN ( SELECT DISTINCT ORG_CODE, ORG_TITLE FROM ORGANIZATIONS) O ON O.ORG_CODE = H.ORG_CODE
	WHERE COMPLETED_DATE BETWEEN TO_DATE('{$start} 00:00:00', 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE('{$end} 23:59:59', 'MM/DD/YYYY HH24:MI:SS')
	".(($org) ? "AND H.ORG_CODE = '{$org}'" : '')."
	ORDER BY INCIDENT_DATE
	";
	
while($row = $oci->fetch($sql)) {
	//$user = mcl_Ldap::lookup($row['Completed By']);
	$activity["hp"][] = array(
		"ID" => $row['ID'],
		"Completed By" => $row['Completed By'],
		"Incident Date" => $row['Incident Date'],
		"Event Type" => $row['Event Type'],
		"Organization" => $row['Organization']
	);
}

$forms = array("hp"	=> array("Anatomy of an Event", "Includes all reports completed"));
$view = array("hp"	=> "sot.hp.view(ID_PLACEHOLDER); return false;");
// $delete = array("hp"	=> "sot.hp._delete(ID_PLACEHOLDER); return false");
$orgChart = array();

foreach($forms as $key=>$value) {
	echo "<div class='header'>";
	echo "<div class='caption' onclick='toggle(\"{$key}\", true);'>";
	echo "<span class='toggle' id='{$key}_toggle'><img src='../../src/img/expand.png'/></span>";
	echo "{$value[0]} <span style='font-size: 10px;'>{$value[1]}</span>";
	echo "</div>";
	//window.open(\"hp_excel.php?start={$start}&end={$end}".(($_GET['org']) ? "&org=".urlencode($_GET['org']) : "")."\");
	echo "<div class='excel' onclick='exportHp();'><div style='border-top: 1px solid #f0f0f0; border-left: 1px solid #f0f0f0; padding: 5px;'><img src='../../src/img/excel.png'/></div></div>";
	echo "<div class='count'><div style='border-top: 1px solid #f0f0f0; border-left: 1px solid #f0f0f0; padding: 5px;'>" . count($activity[$key]) ."</div></div>";
	echo "</div>";
	echo "<div id='{$key}' style='display: none;' class='activity'>";
		$firstpass = true;
		if(count($activity[$key]) > 0) {
			echo "<table style='width: 100%;'>";
			foreach((array)$activity[$key] as $value) {
				if($firstpass) {
					echo "<tr>";
					foreach($value as $columnHeader=>$columnValue) {
						echo "<td style='padding: 5px; border-bottom: 1px solid #000;'><div class='inner'>{$columnHeader}</div></td>";
					}
					echo "<td style='border-bottom: 1px solid #000;'>&nbsp;</td>";
					if(isset($edit[$key])) { echo "<td style='border-bottom: 1px solid #000;'>&nbsp;</td>"; }
					if(isset($delete[$key])) { echo "<td style='border-bottom: 1px solid #000;'>&nbsp;</td>"; }
					echo "</tr>";
					
					$firstpass = false;
				}
				
				echo "<tr class='" . (++$x % 2 == 0 ? 'even' : 'odd') . "'>";
					foreach($value as $columnHeader=>$columnValue) {
					echo "<td>" . ($columnValue === "1" ? "<div style='text-align: center;'><img src='../../src/img/check.png'></div>" : ($columnValue === "0" ? "<div style='text-align: center;'><img src='../../src/img/x.png'></div>" : $columnValue)) ."</td>";
				}
				
				$viewLink = str_replace("ID_PLACEHOLDER", "{$value["ID"]}", $view[$key]);
				echo "<td style='text-align: center; font-size: 10px;'>[ <a href='#' onclick='{$viewLink}'>View</a> ]</td>";
				if($edit[$key]) {
					$editLink = str_replace("ID_PLACEHOLDER", "{$value["ID"]}", $edit[$key]);
					echo "<td style='text-align: center; font-size: 10px;'>[ <a  " . ($value["Complete?"] == 1 ? "disabled=disabled href='#'" : "href='{$editLink}&returnto=../admin/nearmiss&delegate={$_GET["delegate"]}'") . " >Edit</a> ]</td>";
				}
				if($delete[$key]) {
					$deleteLink = str_replace("ID_PLACEHOLDER", "{$value["ID"]}", $delete[$key]);
					echo "<td style='text-align: center; font-size: 10px;'>[ <a href='#' onclick='{$deleteLink}'>Delete</a> ]</td>";
				}
				echo "</tr>";
				
				$orgChart[$value["Organization"]]++;
			}
			echo "</table>";
		}  else {
			echo "<div style='padding: 5px;'>There is no data to display with the current filter.</div>";
		}
	echo "</div>";
}
$categories = '';
$data = '';
asort($orgChart);
foreach($orgChart as $key => $value){
	$categories .= (empty($categories) ? "" : ", ") . "'" . str_replace("'", "\'", "{$key}") . "'";
	$data .= (empty($data) ? "" : ", ") . "'{$value}'";
}
$data = str_replace("'", "", $data);
if (!$_GET['org']) {
	mcl_Html::s(mcl_Html::SRC_JS, <<<JS
		var highlight;
		$('#chart').css('border', '1px solid #000');
		$(function () {
			var chart;
			$(document).ready(function() {
				chart = new Highcharts.Chart({
					chart: {
						renderTo: 'chart',
						type: 'column'
					},
					title: {
						text: 'Anatomy of an Event Reports'
					},
					subtitle: {
						text: ''
					},
					xAxis: [{
						categories: [{$categories}],
						labels: {
							enabled: true,
							align: 'center'
						}
					}],
					yAxis: {
						min: 0,
						title: {
							text: ''
						}
					},
					legend: {
						layout: 'vertical',
						x: 120,
						verticalAlign: 'top',
						y: 100,
						floating: true,
						backgroundColor: '#FFFFFF'
					},
					legend: {
						enabled: false
					},
					plotOptions: {
						series: {
							shadow: false
						}
					},
					series: [{
						name: 'Report Count',
						data: [{$data}],
						color: '#a4d1ff',
						dataLabels: {
							enabled: true
						}
			
					}]
				});
			});
			
		});	
JS
);
echo "<div id='chart' style='width: 1200px; margin-left: 5px; border: 1px solid #000;'></div>";
}
?>
<? if ($_GET['org']) echo "<script>toggle('hp', true);</script>";?>