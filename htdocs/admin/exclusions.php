<?php
require_once('/opt/apache/servers/soteria/htdocs/src/php/require.php'); 
$privileges = auth::check('privileges');
if(!$privileges["MANAGE_DELEGATES_EXCLUSIONS"]){
	auth::deny();
}

$oci = new mcl_Oci("soteria");
$msg = "";
if(!empty($_POST)){
	$error = false;
	if($_POST["delete"] == 'true'){
		$sql = "DELETE FROM EXCLUSIONS WHERE EX_ID = '{$_POST["ex_id"]}'";	
		$action = 'deleted';
	} else {
		//if(!empty($_POST["usid"])){
			$name = mcl_Ldap::lookup($_POST["usid"]);
		
			if(empty($name["fname"])) {
				$error = true;
				$msg = "Invalid username {$_POST["usid"]} entered. No employee found.";	
			} else {	
				$start = (!empty($_POST["start"]) ? "TO_DATE('{$_POST["start"]} : 00:00:00', 'MM/DD/YYYY HH24:MI:SS')" : "NULL");
				$end = (!empty($_POST["end"]) ? "TO_DATE('{$_POST["end"]} : 23:59:59', 'MM/DD/YYYY HH24:MI:SS')" : "NULL");
				
				$sql = "
					INSERT INTO EXCLUSIONS(
						EX_ID,
						USID,
						START_DT,
						END_DT,
						DESCRIPTION,
						ADDED_BY,
						ADDED
					) VALUES(
						EX_UNIQUE.NEXTVAL,
						'" . trim(strtolower($_POST["usid"])) . "',
						{$start},
						{$end},
						'" . $_POST["description"]  . "',
						'" . auth::check('usid') . "',
						SYSDATE
					)
				";
				$action = 'added';
			}
		//}
	}
	if($error == false) {
		if(!$oci->query($sql)){ 
			$error = true; 
		}	
	}

	//Clean Post
	if(!$error){
		header("Location: exclusions.php?success=1&action={$action}&delegate={$_GET["delegate"]}&order={$_GET["order"]}&order_by={$_GET["order_by"]}" . (isset($_GET["org"]) ? "&org={$_GET["org"]}" : ""));
	}
}


if($error === true){
	if(substr($error["message"], 0, 9) == 'ORA-01858'){
		$msg = "Invalid date entered.";
	} else if(substr($error["message"], 0, 9) == 'ORA-00001'){
		$msg = "An exclusion for the employee entered already exists.";
	} else {
		$error = $oci->error();
		if(empty($msg)) {
			$msg = $error["message"];
		}
	}
	
	echo "<div class='error' style='margin: 5px; width: 500px;'>Unable to add exclusion. {$msg}</div>";
} else if($_GET["success"] == "1") {
	echo "<div class='success' style='margin: 5px; width: 500px;'>Successfully {$_GET["action"]} exclusion.</div>";
}

?>
<div>
	<form id='ex_form' name='ex_form'  method = 'POST' action='exclusions.php?<?php echo "delegate={$_GET["delegate"]}&order={$_GET["order"]}&order_by={$_GET["order_by"]}" . (isset($_GET["org"]) ? "&org={$_GET["org"]}" : ""); ?>' style = 'overflow: hidden; padding: 5px;'>
		<table style='border: 1px solid #000; width: 512px;'>
			<tr>
				<td colspan= '2' style='background-color: #92b9dc; padding: 5px; font-weight: bold;'>Add an Exclusion</td>
			</tr>
			<tr style = ''>
				<td style='font-size: 10px; font-weight: normal; background-color: #e3e1e3; padding: 2px;'>Employee (username ie: u12345)</td>
				<td style='background-color: #e3e1e3; padding: 2px;'>
					<input type='text' name='usid' style = 'height: 12px; border: 1px solid #000; width: 200px;' maxlength='6' value='<?=$_POST["usid"]?>'/>
					<input type = 'hidden' name='ex_id' id='ex_id' maxlength='6' />
				</td>
			</tr>
			<tr style = ''>
				<td style='font-size: 10px; font-weight: normal; background-color: #f0f0f0; padding: 2px;'>Exlcude From (leave blank for indefinitely)</td>
				<td style='background-color: #f0f0f0; padding: 2px;'>
					<input style = 'height: 12px; border: 1px solid #000; width: 100px;' type = 'text' name = 'start' id = 'start' value = '<?php echo $_POST["start"];?>'/> 
					<img style = 'vertical-align: bottom;' src='../../src/img/calendar.gif' alt='' id='tcal' onmouseover='setup_cal("tcal", "start", true);' />
				</td>
			</tr>
			<tr style = ''>
				<td style='font-size: 10px; font-weight: normal; background-color: #e3e1e3; padding: 2px;'>Exlcude To (leave blank for indefinitely)</td>
				<td style='background-color: #e3e1e3; padding: 2px;'>
					<input style = 'height: 12px; border: 1px solid #000; width: 100px;'  type = 'text' name = 'end' id = 'end' value = '<?php echo $_POST["end"]; ?>' /> 
					<img style = 'vertical-align: bottom;' src='../../src/img/calendar.gif' alt='' id='tcal2' onmouseover='setup_cal("tcal2", "end", true);' />
				</td>
			</tr>
			<tr style = ''>
				<td style='font-size: 10px; font-weight: normal; background-color: #f0f0f0; padding: 2px;'>Short Description</td>
				<td style='background-color: #f0f0f0; padding: 2px;'>
					<textarea name='description' id='description' style='border: 1px solid #000; font-size: 10px; font-family: Segoe ui; width: 300px; height: 30px;' ><?=$_POST["description"]?></textarea>
				</td>
			</tr>
			<tr>
				<td style='font-size: 10px; font-weight: normal; background-color: #e3e1e3; padding: 2px;' ></td>
				<td style='background-color: #e3e1e3; padding: 2px;'>
					<input type = 'hidden' name = 'delete' id = 'delete' value='false'/>
					<input type = 'submit' value = 'Add Exclusion' style='width: 100px;'/>
				</td>
			</tr>
		</table>
	</form>
</div>
<?php

$order_by = !empty($_GET["order_by"]) ? $_GET["order_by"] : "EX_ID";
$order = !empty($_GET["order"]) ? $_GET["order"] : "DESC";

$ascdesc = ($order == "ASC" ? "DESC" : "ASC");
$order_img = ($order == "ASC" ? "up" : "down");

$sql = "
	SELECT 	EX.EX_ID, 
			EX.USID, 
			TO_CHAR(EX.START_DT, 'MM/DD/YYYY') AS EX_FROM, 
			TO_CHAR(EX.END_DT, 'MM/DD/YYYY') AS EX_TO , 
			EX.DESCRIPTION, 
			E.NAME, 
			E.TITLE,
			E.ORG_DESCRIPTION AS ORG,
			TO_CHAR(EX.ADDED, 'MM/DD/YYYY') AS ADDED_DT
	FROM	EXCLUSIONS EX
	LEFT JOIN EMPLOYEES E
		ON E.USID = EX.USID
	ORDER BY {$order_by} {$order}
";

//echo "<pre>{$sql}</pre>";

$x = 0;
$tbl = "";
$orgs_array = array();
$org = (isset($_GET["org"]) ? (!empty($_GET["org"]) ? $_GET["org"] : false) : str_replace("&", "and", auth::check("org")));

while($row = $oci->fetch($sql)){
	
	$name = $row["NAME"];
	$title = $row["TITLE"];
	
	if(empty($row["NAME"])){
		$user_ = @mcl_Ldap::lookup($row["USID"]);
		$name = $user_["fname"];
		$title = $user_["title"];
	}
	if(empty($row["NAME"])){
		$name = $row["USID"];
	}
	
	$row_org = str_replace("&", "and", trim(preg_replace("/\([^)]+\)/", "", $row["ORG"])));
	$orgs_array[$row_org] = true;
	/*
	echo $org . '<br/>';
	echo $row_org . '<br/>';
	
	echo $org == $row_org ? "true" : "false";
	echo "<br/>";
	echo "--<br/>";
	*/
	if($org && $org != $row_org) {
		continue;
	}
	

	$tbl .= "<tr class = '" . ($x++ % 2 == 0 ? 'even' : 'odd') . "'>
			<td style='text-align: left;'>{$row["EX_ID"]}</td>
			<td style='text-align: left;'>{$name}</td>
			<td style='text-align: left;'>{$title}</td>
			<td style='text-align: left; width: 200px;'>{$row_org}</td>
			<td style='text-align: center;'>{$row["EX_FROM"]}</td>
			<td style='text-align: center;'>{$row["EX_TO"]}</td>
			<td style='text-align: center;'>{$row["ADDED_DT"]}</td>
			<td style='text-align: left; width: 300px; word-break: break-word;'>{$row["DESCRIPTION"]}</td>
			<td style='font-size: 10px;'>[ <a href = '#' onclick = 'if(confirm(\"Are you sure you want to delete this user?\\nThis action cannot be undone.\")){ dojo.byId(\"ex_id\").value = \"{$row["EX_ID"]}\"; dojo.byId(\"delete\").value=\"true\";  dojo.byId(\"ex_form\").submit();} return false;'>Delete</a> ]</td>	
		</tr>
	";
	
}

$orgs = '<option>- Filter Organization -</option>';
//$orgs .= '<option>All</option>';

foreach($orgs_array as $key=>$value) {
	if(empty($key)) continue;
	
	$orgs .= "<option value='{$key}' " . ($org == $key ? "selected=selected" : "") .">{$key}</option>";
}


mcl_Html::s(mcl_Html::SRC_CSS, "
	table tr th div {
		position:	relative;
		text-align:	center;
		
	}
	table tr th div img {
		position:	absolute;
		right:		5px;
		top:		8px;
	}
");

ksort($orgs);
echo "<table class = 'tbl hover'>";
	echo "<tr>";
		echo "<th style='cursor: pointer;' onclick='window.location=\"exclusions.php?delegate={$_GET["delegate"]}&order_by=EX_ID&order={$ascdesc}&org=\" + dojo.byId(\"org\").value'>
			<div style = 'width: 100px;' class = 'inner'>
				ID
				<img src='../src/img/{$order_img}.png' style='cursor: pointer;'/>
			</div>
		</th>";
		echo "<th style='cursor: pointer;' onclick='window.location=\"exclusions.php?delegate={$_GET["delegate"]}&order_by=E.NAME&order={$ascdesc}&org=\" + dojo.byId(\"org\").value'>
			<div style = 'width: 150px;' class = 'inner'>
				Employee
				<img src='../src/img/{$order_img}.png' style='cursor: pointer;' />
			</div>
		</th>";
		echo "<th style='cursor: pointer;' onclick='window.location=\"exclusions.php?delegate={$_GET["delegate"]}&order_by=E.TITLE&order={$ascdesc}&org=\" + dojo.byId(\"org\").value'>
			<div style = 'width: 200px;' class = 'inner'>
				Title
				<img src='../src/img/{$order_img}.png' style='cursor: pointer;'/>
			</div>
		</th>";
		echo "<th>
			<div style = 'width: 200px; font-size: 10px;' class = 'inner'>
				<select name='org' id='org' style='font-size: 10px; width: 100%;' onchange='window.location=\"exclusions.php?delegate={$_GET["delegate"]}&order_by={$_GET["order_by"]}&order={$_GET["order"]}&org=\" + dojo.byId(\"org\").value' >
					{$orgs}
				</select>
			</div>
		</th>";
		echo "<th style='cursor: pointer;' onclick='window.location=\"exclusions.php?delegate={$_GET["delegate"]}&order_by=EX.START_DT&order={$ascdesc}&org=\" + dojo.byId(\"org\").value'>
			<div style = 'width: 100px;' class = 'inner'>
				From Date
				<img src='../src/img/{$order_img}.png' style='cursor: pointer;' />
			</div>
		</th>";
		echo "<th style='cursor: pointer;' onclick='window.location=\"exclusions.php?delegate={$_GET["delegate"]}&order_by=EX.END_DT&order={$ascdesc}&org=\" + dojo.byId(\"org\").value'>
			<div style = 'width: 100px;' class = 'inner'>
				To Date
				<img src='../src/img/{$order_img}.png' style='cursor: pointer;' />
			</div>
		</th>";
		echo "<th style='cursor: pointer;' onclick='window.location=\"exclusions.php?delegate={$_GET["delegate"]}&order_by=EX.ADDED&order={$ascdesc}&org=\" + dojo.byId(\"org\").value'>
			<div style = 'width: 100px;' class = 'inner'>
				Date Added
				<img src='../src/img/{$order_img}.png' style='cursor: pointer;' />
			</div>
		</th>";
		echo "<th class=''>
			<div style = 'width: 300px;' class = 'inner'>Description</div>
		</th>";
		echo "<th class=''>
			<div style = 'width: 60px;' class = 'inner'></div>
		</th>";
	echo "</tr>";
	echo $tbl;
	if($x == 0){
		echo "<tr>
			<td colspan='9'>
				There are no exlcusions to display.
			</td>
		</tr>";
	}
echo "</table>";
?>