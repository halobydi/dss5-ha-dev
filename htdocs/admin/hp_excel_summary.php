<?php
	function errorHandler($errno, $errstr, $errfile, $errline, $errcontext){
	}
	set_error_handler('errorHandler');
	require_once("mcl_Oci.php");
	require_once("/opt/apache/servers/soteria/htdocs/src/php/auth.php");
	$privileges = auth::check('privileges');
	if(!$privileges["MANAGE_AOE"]){
		auth::deny();
	}
	$start= $_GET['start'];
	$end = $_GET['end'];
	$oci = new mcl_Oci("soteria");
	$oci->dateFormat();
	$oci->query($sql);
	$org = $_GET['org'];
	
	$sql = "
	SELECT	O.HP_ID AS \"Anatomy of an Event Report ID\", 
			NVL(E.NAME, COMPLETED_BY) AS \"Completed By\",
			NVL(S.NAME, COMPLETED_BY_SUPERVISOR) AS \"Completed by Supervisor\",
			NVL(D.NAME, COMPLETED_BY_DIRECTOR) AS \"Completed By Director\",
			TO_CHAR(O.INCIDENT_DATE, 'MM/DD/YYYY') AS \"Incident Date\",
			TO_CHAR(O.COMPLETED_DATE, 'MM/DD/YYYY') AS \"Completed Date\",
			O.EVENT_TYPE AS \"Event Type\",
			O.ORG_CODE AS \"Organization\",
			O.COMMENTS AS \"Comments\"
	FROM	HP_OBSERVATIONS O
	LEFT JOIN EMPLOYEES E ON E.USID = O.COMPLETED_BY
	LEFT JOIN EMPLOYEES S ON S.USID = O.COMPLETED_BY_SUPERVISOR
	LEFT JOIN EMPLOYEES D ON D.USID = O.COMPLETED_BY_DIRECTOR
	WHERE COMPLETED_DATE BETWEEN TO_DATE('{$start} 00:00:00', 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE('{$end} 23:59:59', 'MM/DD/YYYY HH24:MI:SS')
	".(!empty($org) ? "AND ORG_CODE = '{$org}'" : '')."
	ORDER BY INCIDENT_DATE
	";

	// while($row = $oci->fetch($sql)) {
	// 	$user = mcl_Ldap::lookup($row['Completed By']);
	// 	$manager = mcl_Ldap::lookup($user["manager"]);
	// 	//find director
	// 	$leafusid = $user["usid"];
	// 	$loop = true;
	// 	$foundDirector = false;
	// 	do {
	// 		$leaf = @mcl_Ldap::lookup($leafusid);
	// 		$isDirector = strpos(strtolower($leaf["title"]), "director");
			
	// 		if($isDirector || $isDirector === 0) {
	// 			$loop = false;
	// 			$director = $leaf;
	// 			$foundDirector = true;
	// 		}
			
	// 		if(!$leaf["manager"]) {	$loop = false; } 
	// 		$leafusid = $leaf["manager"];
			
	// 	} while($loop); 

	// 	if(!$foundDirector) {
	// 		$director = @mcl_Ldap::lookup($manager["manager"]);
	// 	}
	// 	$activity["hp"][] = array(
	// 		"ID" => $row['ID'],
	// 		"Completed By" => $row['Completed By'],
	// 		"Incident Date" => $row['Incident Date'],
	// 		"Completed by Supervisor" => $manager['username'],
	// 		"Director" => $director['username'],
	// 		"Organization" => $row['Organization']
	// 	);
	// }

	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Cache-Control: private",false);
	header("Content-Type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"SOTeria_Export_" . time() . ".csv\";" );
	header("Content-Transfer-Encoding: binary");
	$ct = 0;
	$header = false;
	while($row = $oci->fetch($sql)) {
		if (!$header) {
			$x = 0;				
			foreach($row as $key=>$value) {
				echo ($x++ == 0 ? "" : ",") . $key;
			}
			echo "\n";
			$header = true;
		}		
				
		$x = 0;
		foreach($row as $key => $value) {
			echo ($x++ == 0 ? "" : ",") . "\"" . $value . "\"";
		}
		echo "\n";
		
		$ct++;
	}
?>