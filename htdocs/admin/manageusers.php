<?php
require_once("../src/php/require.php");
$privileges = auth::check('privileges');
if(!$privileges["MANAGE_ADMINS"]){
	auth::deny();
}

$oci = new mcl_Oci("soteria");
mcl_Html::s(mcl_Html::SRC_JS, "
	function edit_user(x) {
	  dojo.query('.form_' + x + '_edit').forEach(function(node){
			node.style.display = '';
	  });
	  dojo.query('.form_' + x + '_view').forEach(function(node){
			node.style.display = 'none';
	  });
	
	}
	
	function save_edit_user(x) {
		dojo.byId('form_' + x).submit();
	}
");

mcl_Html::s(mcl_Html::SRC_CSS, "
	input[type=checkbox] {
		margin: 	0px;
		padding: 	0px;
	}
");
		
if(!empty($_POST)){

	$error = false;
	if($_POST["delete"] == 'true'){
		$sql = "
			DELETE FROM USERS
			WHERE USID = '{$_POST["usid"]}'
		";
		
		$oci->query($sql);
	} else {
		if(!empty($_POST["usid"])){
			$sql = "
				MERGE INTO USERS USING DUAL ON (USID = '{$_POST["usid"]}')
				WHEN MATCHED THEN UPDATE
					SET MANAGE_RED_TAG_AUDITS = '" . ($_POST["1"] == "on" ? 1 : 0) . "',
						MANAGE_DELEGATES_EXCLUSIONS = '" . ($_POST["2"] == "on" ? 1 : 0) . "',
						MANAGE_ADMINS= '" . ($_POST["3"] == "on" ? 1 : 0) . "',
						MANAGE_SWO_ITEMS= '" . ($_POST["4"] == "on" ? 1 : 0) . "',
						MANAGE_AOE = '" . ($_POST["5"] == "on" ? 1 : 0) . "',
						MANAGE_NEAR_MISS = '" . ($_POST["6"] == "on" ? 1 : 0) . "',
						ACCESS_STORM_DUTY = '" . ($_POST["7"] == "on" ? 1 : 0) . "'
					WHERE USID = '{$_POST["usid"]}'
				WHEN NOT MATCHED THEN INSERT (
					USID,
					ADMIN,
					MANAGE_RED_TAG_AUDITS,
					MANAGE_DELEGATES_EXCLUSIONS,
					MANAGE_ADMINS,
					MANAGE_SWO_ITEMS,
					MANAGE_AOE,
					MANAGE_NEAR_MISS,
					ACCESS_STORM_DUTY
				) VALUES(
					'" . trim(strtolower($_POST["usid"])) . "',
					1,
					'" . ($_POST["1"] == "on" ? 1 : 0) . "',
					'" . ($_POST["2"] == "on" ? 1 : 0) . "',
					'" . ($_POST["3"] == "on" ? 1 : 0) . "',
					'" . ($_POST["4"] == "on" ? 1 : 0) . "',
					'" . ($_POST["5"] == "on" ? 1 : 0) . "',
					'" . ($_POST["6"] == "on" ? 1 : 0) . "',
					'" . ($_POST["7"] == "on" ? 1 : 0) . "'
				)
			";
			
			if(!$oci->query($sql)){
				$error = true;
			}
			
		}
	}
	//Clean Post
	if(!$error){
		header("Location: manageusers.php");
	}
}


if($error === true){
	$error = $oci->error();
	$msg = $error['message'];
	if(substr($error["message"], 0, 9) == 'ORA-12899'){
		$msg = "You can only use employee usernames to add a user.";
	} else if(substr($error["message"], 0, 9) == 'ORA-00001'){
		$msg = "User already exists.";
	}
	
	echo "<div class = 'error' style = 'margin: 5px;'>
			Unable to add edit/add. {$msg}
		</div>";
}

echo "
	<table class='tbl' style='width: 400px;'>
	<form method = 'POST' id = 'admin_user'>
		<th colspan=''><div class='inner'>Add New Administrator</span></div></th>
		<tr class='odd'>
			<td style='text-align: left; border: none; padding-left: 5px;'>
				<div style='font-size: 10px;'>&nbsp;Username (ie. u12345)</div>
				<input type = 'text' name='usid' id='usid' maxlength='6' style='width: 150px; border: 1px solid gray;'/>
			</td>
		</tr>
		<tr class='odd'>
			<td style='text-align: left; border: none; padding-left: 5px; font-size: 10px;'>
				<input type='checkbox' name='3' id='3'/>Add/edit administrators<br/>
				<input type='checkbox' name='4' id='4'/>Manage SWO items<br/>
				<input type='checkbox' name='2' id='2'/>Add/edit delegates and exclusions<br/>
				<input type='checkbox' name='1' id='1'/>Manage Red Tag audits<br/>
				<input type='checkbox' name='5' id='5'/>Manage Anatomy of an Event reports<br/>
				<input type='checkbox' name='6' id='6'/>Manage Near Miss<br/>
				<input type='checkbox' name='7' id='7'/>Access Storm Duty Form
			</td>
		</tr>
	</table>
	<input type = 'hidden' name = 'delete' id = 'delete' value='false'/>
	<input type = 'submit' value = 'Add' style='width: 100px; font-size: 10px; margin-left: 5px;'/>
	</form>
";

$sql = "
	SELECT U.*, E.NAME, E.TITLE
	FROM	USERS U
	LEFT JOIN EMPLOYEES E
		ON E.USID = U.USID
	ORDER BY E.NAME
";

echo "<div style='margin: 5px; margin-bottom: 10px; display: none;'>";
	echo "<div style='font-weight: bold;'>What does each privilege mean?</div>";
	echo "<div>Add/edit administrators - <span style='font-size: 11px;'>The individual would have access to this page. They can modify different user privileges.</span></div>";
	echo "<div>Manage SWO Items - <span style='font-size: 11px;'>The individual would have access add/edit/delete questions from the Enterprise Performance Observation form. They can also setup the drill down questions for different organizations.</span></div>";
	echo "<div>Add/edit delegates and exclusions - <span style='font-size: 11px;'>The individual would have access add/delete delegates for any individual in any Business Unit.</span></div>";
	echo "<div>Manage Red Tag audits - <span style='font-size: 11px;'>The individual would have access to view/delete all Red Tag Audits submitted in SOTeria. The individual also has the ability to edit the Business Unit requirments for the Organiation Report.</span></div>";
	echo "<div>Manage Anatomy of Event reports - <span style='font-size: 11px;'>The individual would have access to view all Anatomy of an Event reports submitted in SOTeria.</span></div>";
	echo "<div>Manage Near Miss - <span style='font-size: 11px;'>The individual would have access to edit/delete any Near Miss using the Organization Report and modify the Workflow Mailing List</span></div>";
	echo "<div>Access Storm Duty - <span style='font-size: 11px;'>The individual would have access to create/edit/delete any Storm Duty forms submitted</span></div>";

echo "</div>";

echo "
<table class = 'tbl hover'>
	<tr>
		<th class='double'><div style = 'width: 200px;' class = 'inner'>User</div></th>
		<th class='double'><div style = 'width: 300px;'class = 'inner'>Title</div></th>
		<th class='double'><div style = 'width: 100px; font-size: 10px;' class = 'inner'>Add/edit administrators</div></th>
		<th class='double'><div style = 'width: 60px; font-size: 10px;' class = 'inner'>Manage SWO Items</div></th>
		<th class='double'><div style = 'width: 80px; font-size: 10px;' class = 'inner'>Add/edit delegates and exclusions</div></th>
		<th class='double'><div style = 'width: 80px; font-size: 10px;' class = 'inner'>Mange Red-Tag</br>Audits</div></th>
		<th class='double'><div style = 'width: 80px; font-size: 10px;' class = 'inner'>Mange Anatomy</br> of an Event Reports</div></th>
		<th class='double'><div style = 'width: 80px; font-size: 10px;' class = 'inner'>Manage</br> Near Miss</div></th>
		<th class='double'><div style = 'width: 80px; font-size: 10px;' class = 'inner'>Access</br> Storm Duty</div></th>
		<th class='double'><div style = 'width: 50px; font-size: 10px;' class = 'inner'></div></th>
		<th class='double'><div style = 'width: 50px; font-size: 10px;' class = 'inner'></div></th>
	</tr>
";

$x = 0;
while($row = $oci->fetch($sql)){
	
	$name = $row["NAME"];
	$title = $row["TITLE"];
	if(empty($row["NAME"])){
		$user_ = mcl_Ldap::lookup($row["USID"]);
		$name = $user_["fname"];
		$title = $user_["title"];
		
		if(empty($name)) { continue; }
	}
	
	echo "<tr class = '" . ($x++ % 2 == 0 ? 'even' : 'odd') . "'>
		<td style='text-align: left;'>
			<form method='POST' id='form_{$x}'>
			<input type='hidden' name='usid' value='{$row["USID"]}'>
		" . $name ."</td>
		<td style='text-align: left;'>" . $title ."</td>
		
		<td><span class='form_{$x}_view'>" .  ($row["MANAGE_ADMINS"] == 1 ? "<img src='../src/img/check.png'/>" : "<img src='../src/img/x.png'/>") . "</span>"
		.  "<span class='form_{$x}_edit' style='display: none;'><input type='checkbox' name='3' " . ($row["MANAGE_ADMINS"] == 1 ? "checked=checked" : "") . " /></span></td>
		<td><span class='form_{$x}_view'>" .  ($row["MANAGE_SWO_ITEMS"] == 1 ? "<img src='../src/img/check.png'/>" : "<img src='../src/img/x.png'/>") . "</span>"
		.  "<span class='form_{$x}_edit' style='display: none;'><input type='checkbox' name='4' " . ($row["MANAGE_SWO_ITEMS"] == 1 ? "checked=checked" : "") . " /></span></td>
		<td><span class='form_{$x}_view'>" .  ($row["MANAGE_DELEGATES_EXCLUSIONS"] == 1 ? "<img src='../src/img/check.png'/>" : "<img src='../src/img/x.png'/>") . "</span>"
		.  "<span class='form_{$x}_edit' style='display: none;'><input type='checkbox' name='2' " . ($row["MANAGE_DELEGATES_EXCLUSIONS"] == 1 ? "checked=checked" : "") . " /></span></td>
		<td><span class='form_{$x}_view'>" .  ($row["MANAGE_RED_TAG_AUDITS"] == 1 ? "<img src='../src/img/check.png'/>" : "<img src='../src/img/x.png'/>") . "</span>"
		.  "<span class='form_{$x}_edit' style='display: none;'><input type='checkbox' name='1' " . ($row["MANAGE_RED_TAG_AUDITS"] == 1 ? "checked=checked" : "") . " /></span></td>
		<td><span class='form_{$x}_view'>" .  ($row["MANAGE_AOE"] == 1 ? "<img src='../src/img/check.png'/>" : "<img src='../src/img/x.png'/>") . "</span>"
		.  "<span class='form_{$x}_edit' style='display: none;'><input type='checkbox' name='5' " . ($row["MANAGE_AOE"] == 1 ? "checked=checked" : "") . " /></span></td>
		<td><span class='form_{$x}_view'>" .  ($row["MANAGE_NEAR_MISS"] == 1 ? "<img src='../src/img/check.png'/>" : "<img src='../src/img/x.png'/>") . "</span>"
		.  "<span class='form_{$x}_edit' style='display: none;'><input type='checkbox' name='6' " . ($row["MANAGE_NEAR_MISS"] == 1 ? "checked=checked" : "") . " /></span></td>
		<td><span class='form_{$x}_view'>" .  ($row["ACCESS_STORM_DUTY"] == 1 ? "<img src='../src/img/check.png'/>" : "<img src='../src/img/x.png'/>") . "</span>"
		.  "<span class='form_{$x}_edit' style='display: none;'><input type='checkbox' name='7' " . ($row["ACCESS_STORM_DUTY"] == 1 ? "checked=checked" : "") . " /></span></td>
		<td><button style='width: 45px;' onclick = 'if(confirm(\"Are you sure you want to delete this user?\\nThis action cannot be undone.\")){ dojo.byId(\"usid\").value = \"{$row["USID"]}\"; dojo.byId(\"delete\").value=\"true\"; dojo.byId(\"admin_user\").submit();} return false;'>Delete</button></td>
		<td><span class='form_{$x}_view'><button style='width: 50px;' onclick='edit_user({$x});'>Edit</button></span><span class='form_{$x}_edit' style='display: none;'><button style='width: 50px;' onclick='save_edit_user({$x});'>Save</button></span>
			</form>
		</td>
	</tr>";

}

?>