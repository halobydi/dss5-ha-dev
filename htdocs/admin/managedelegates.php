<?php
require_once("../src/php/require.php");
$privileges = auth::check('privileges');
if(!$privileges["MANAGE_DELEGATES_EXCLUSIONS"]){
	auth::deny();
}

$oci = new mcl_Oci('soteria');
if(!empty($_POST)){
	$error = false;
	if(!empty($_POST["d_id"])){
		$sql = "DELETE FROM DELEGATES WHERE D_ID = {$_POST["d_id"]}";
		$oci->query($sql);
		$action = 'deleted';
	} else {
		if(!empty($_POST["delegator"]) && !empty($_POST["delegatee"])){
			$delegator_name = @mcl_Ldap::lookup($_POST["delegator"]);
			$delegatee_name = @mcl_Ldap::lookup($_POST["delegatee"]);
			
			if(empty($delegator_name["fname"])) {
				$error = true;
				$msg = "Invalid Delegator username entered. No Employee found.";
			}
			if(empty($delegatee_name["fname"])) {
				$error = true;
				$msg = "Invalid Delegatee username entered. No Employee found.";
			}
			
			if($error == false) {
				$sql = "
					INSERT INTO DELEGATES 
					VALUES(
						DELEGATE_UNIQUE.NEXTVAL,
						'" . trim(strtolower($_POST["delegator"])) . "',
						'" . trim(strtolower($_POST["delegatee"])) . "'
					)
				";
				if(!$oci->query($sql)){ $error = true; }
			}
			$action = 'added';
		} else {
			$error = true;
			$msg = "You must enter a delegator and delegatee username.";
		}
	}
	//Clean Post
	if(!$error){
		header("Location: managedelegates.php?success=1&action={$action}&delegate={$_GET["delegate"]}&order={$_GET["order"]}&order_by={$_GET["order_by"]}" . (isset($_GET["org"]) ? "&org={$_GET["org"]}" : ""));
	}
}


if($error === true){
	$error = $oci->error();
	if(substr($error["message"], 0, 9) == 'ORA-12899'){
		$msg = "You can only use employee usernames to add a delegate.";
	} else if(substr($error["message"], 0, 9) == 'ORA-00001'){
		$msg = "Delegator and delegatee combination already exists.";
	}
	
	echo "<div class='error' style='margin: 5px; width: 300px;'>Unable to add delegate. {$msg}</div>";
} else if($_GET["success"] == "1") {
	echo "<div class='success' style='margin: 5px; width: 300px;'>Successfully {$_GET["action"]} delegate.</div>";
}

$order_by = !empty($_GET["order_by"]) ? $_GET["order_by"] : "D_ID";
$order = !empty($_GET["order"]) ? $_GET["order"] : "DESC";

$ascdesc = ($order == "ASC" ? "DESC" : "ASC");
$order_img = ($order == "ASC" ? "up" : "down");


$sql = "
	SELECT  D_ID,
			NVL(E.NAME, DELEGATOR) AS DELEGATOR,
			DELEGATEE,
			E2.NAME AS DELEGATEE_NAME,
			E.ORG_DESCRIPTION AS ORG
	FROM	DELEGATES D
	LEFT JOIN EMPLOYEES E
		ON E.USID = D.DELEGATOR
	LEFT JOIN EMPLOYEES E2
		ON E2.USID = D.DELEGATEE
	ORDER BY {$order_by} {$order}
";
?>

<div>
	<form id='delegates' name='delegates'  method = 'POST' action='managedelegates.php?<?php echo "delegate={$_GET["delegate"]}&order={$_GET["order"]}&order_by={$_GET["order_by"]}" . (isset($_GET["org"]) ? "&org={$_GET["org"]}" : ""); ?>' style = 'overflow: hidden; padding: 5px;'>
		<table style='border: 1px solid #000;'>
			<tr>
				<td colspan= '2' style='background-color: #92b9dc; padding: 5px; font-weight: bold;'>Add a Delegate</td>
			</tr>
			<tr>
				<td style='font-size: 10px; font-weight: normal; background-color: #e3e1e3; padding: 2px;'>Delegator (username ie: u14567)</td>
				<td style='background-color: #e3e1e3; padding: 2px;'><input type = 'text' name='delegator' maxlength='6'  style = 'height: 12px; border: 1px solid #000; width: 200px;'/></td>
			</tr>
			<tr>
				<td style='font-size: 10px; font-weight: normal; background-color: #f0f0f0; padding: 2px;'>Delegatee (username ie: u14567)</td>
				<td style='background-color: #f0f0f0; padding: 2px;'><input type = 'text' name='delegatee' maxlength='6'  style = 'height: 12px; border: 1px solid #000; width: 200px;'/></td>
			</tr>
			<tr>
				<td style='font-size: 10px; font-weight: normal; background-color: #e3e1e3; padding: 2px;' ></td>
				<td style='background-color: #e3e1e3; padding: 2px;'>
					<input type = 'hidden' name = 'd_id' id = 'd_id'/>
					<input type = 'submit' value = 'Add' style = 'height: 18px; width: 60px;'/>
				</td>
			</tr>
		</table>
	</form>
</div>
<?php
$tbl = '';
$orgs_array = array();
$org = (isset($_GET["org"]) ? (!empty($_GET["org"]) ? $_GET["org"] : false) : str_replace("&", "and", auth::check("org")));

while($row = $oci->fetch($sql)){
	$style = "style = 'text-align: left;'";
	

	$row_org = str_replace("&", "and", trim(preg_replace("/\([^)]+\)/", "", $row["ORG"])));
	$orgs_array[$row_org] = true;

	$name = $row["DELEGATEE_NAME"];
	
	if(empty($row["DELEGATEE_NAME"])){
		$user_ = @mcl_Ldap::lookup($row["DELEGATEE"]);
		$name = $user_["fname"];
	}
	if(empty($name)){
		$name = $row["DELEGATEE"];
	}
	
	if($org && $org != $row_org) {
		continue;
	}
	
	$tbl .= "<tr class = '" . ($x++ % 2 == 0 ? 'even' : 'odd') ."'>
		<td {$style}>{$row["D_ID"]}</td>
		<td {$style}>{$row["DELEGATOR"]}</td>
		<td {$style}>{$name}</td>
		<td {$style}>{$row_org}</td>
		<td style='font-size: 10px;'>[ <a href = '#' onclick = 'if(confirm(\"Are you sure you want to delete this delegate?\\nThis action cannot be undone.\")){ dojo.byId(\"d_id\").value = \"{$row["D_ID"]}\"; dojo.byId(\"delegates\").submit();} return false;'>Delete</a> ]</td>
	</tr>";
}

ksort($orgs_array);
$orgs .= "<option>- Filter Organization - </option>";
foreach($orgs_array as $key=>$value) {
	if(empty($key)) continue;
	$orgs .= "<option value='{$key}' " . ($org == $key ? "selected=selected" : "") .">{$key}</option>";
}

mcl_Html::s(mcl_Html::SRC_CSS, "
	table tr th div {
		position:	relative;
		text-align:	center;
		
	}
	table tr th div img {
		position:	absolute;
		right:		5px;
		top:		8px;
	}
");

?>
<table class = 'tbl hover'>
	<tr>
		<th style='cursor: pointer;' onclick='window.location="managedelegates.php?delegate=<?=$_GET["delegate"]?>&order_by=D_ID&order=<?=$ascdesc?>&org=" + dojo.byId("org").value'>
			<div style = 'width: 100px;' class = 'inner'>
				ID
				<img src='../src/img/<?=$order_img?>.png' style='cursor: pointer;'/>
			</div>
		</th>
		<th style='cursor: pointer;' onclick='window.location="managedelegates.php?delegate=<?=$_GET["delegate"]?>&order_by=E.NAME&order=<?=$ascdesc?>&org=" + dojo.byId("org").value'>
			<div style = 'width: 200px;' class = 'inner'>
				Delegator
				<img src='../src/img/<?=$order_img?>.png' style='cursor: pointer;'/>
			</div>
		</th>
		<th style='cursor: pointer;' onclick='window.location="managedelegates.php?delegate=<?=$_GET["delegate"]?>&order_by=E2.NAME&order=<?=$ascdesc?>&org=" + dojo.byId("org").value'>
			<div style = 'width: 200px;' class = 'inner'>
				Delegatee
				<img src='../src/img/<?=$order_img?>.png' style='cursor: pointer;'/>
			</div>
		</th>
		<th>
			<div style = 'width: 300px;'class = 'inner'>
				<?php 
					echo "
					<select name='org' id='org' style='font-size: 10px; width: 100%;' onchange='window.location=\"managedelegates.php?delegate={$_GET["delegate"]}&order_by={$_GET["order_by"]}&order={$_GET["order"]}&org=\" + dojo.byId(\"org\").value' >
						{$orgs}
					</select>
					";
				?>
			</div>
		</th>
		<th>
			<div style = 'width: 80px;'class = 'inner'></div>
		</th>
	</tr>
<?php
echo $tbl;
?>
</table>