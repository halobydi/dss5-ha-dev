<?php
	require_once("../src/php/require.php");
	$privileges = auth::check('privileges');

	if(!$privileges["MANAGE_RED_TAG_AUDITS"]){
		auth::deny();
	}

	$oci = new mcl_Oci('soteria');
	if(!empty($_POST)){

		$error = false;
		if(!empty($_POST["rta_id"])){
			$sql = "
				DELETE FROM RED_TAG_AUDITS
				WHERE RTA_ID = {$_POST["rta_id"]}
			";
			
			$oci->query($sql);
			
			$sql = "
				DELETE FROM RED_TAG_ATTACHMENTS
				WHERE RTA_ID = {$_POST["rta_id"]}
			";
			
			$oci->query($sql);
		} 
		
		//Clean Post
		header("Location: viewaudits.php");
		
	}
	
	echo "<form method='POST' action='' name='rta_delete'><input type='hidden' id='rta_id' name='rta_id' value=''/></form>";
	
	$sql = "
		SELECT DISTINCT TO_CHAR(AUDIT_DATE, 'YYYY') AS YEAR FROM RED_TAG_AUDITS WHERE AUDIT_DATE IS NOT NULL AND COMPLETE = 1
		UNION  SELECT TO_CHAR(SYSDATE, 'YYYY') FROM DUAL
	";


	$year = (!empty($_GET["year"]) ? $_GET["year"] : date('Y'));
	$month = intval((!empty($_GET["month"]) ? $_GET["month"] : date('m')));
	
	$select = "<select name='year' style = 'height: 20px; font-size: 10px;'>";
	$years = 0;
	while($row = $oci->fetch($sql)){
		$years++;
		$selected = $year == $row["YEAR"] ? "selected=selected" : "";
		$select .= "<option {$selected} value='{$row["YEAR"]}'>{$row["YEAR"]}</option>";
	}

	if($years == 0){
		$select .= "<option value='{$year}'>{$year}</option>";
	}
	$select .= "</select>";
	
	$months = array(
		1 => "January",
		2 => "February",
		3 => "March",
		4 => "April",
		5 => "May",
		6 => "June",
		7 => "July",
		8 => "August",
		9 => "September",
		10 => "October",
		11 => "November",
		12 => "December"
	);
	
	$select_months = "<select name='month' style='font-size: 10px;'>";
	foreach($months as $key=>$value) {
		$select_months .= "<option value='{$key}' " . ($month == $key ? "selected=selected" : "") . " >{$value}</option>";
	}
	$select_months .= "</select>";

	$tbl = "
				<tr>
					<th>
						<div class='inner' style='width: 200px;'>
							Location (S.C)
						</div>
					</th>
					</th>
					<th>
						<div class='inner' style='width: 200px;'>
							Org. Audited
						</div>
					</th>
					</th>
					<th>
						<div class='inner' style='width: 200px;'>
							Date Audited
						</div>
					</th>
					</th>
					<th>
						<div class='inner' style='width: 200px;'>
							Credit Month
						</div>
					</th>
					</th>
					<th>
						<div class='inner' style='width: 200px;'>
							Completed By
						</div>
					</th>
					<th colspan='2'>
						<div class='inner' style=''>
						</div>
					</th>
				</tr>
			";
			
			
	$sql = "
		SELECT R.LOCATION,
				R.ORG,
				TO_CHAR(TO_DATE(CREDIT_MONTH, 'MM'), 'Month') AS CREDIT_MONTH,
				TO_CHAR(AUDIT_DATE, 'MM/DD/YYYY') AS AUDIT_DATE,
				NVL(E.NAME, COMPLETED_BY) AS COMPLETED_BY,
				RTA_ID
		FROM	RED_TAG_AUDITS R
		LEFT JOIN EMPLOYEES E
			ON E.USID = R.COMPLETED_BY
		WHERE	COMPLETE = 1
				AND TO_CHAR(AUDIT_DATE, 'YYYY') = '{$year}'
				AND CREDIT_MONTH = '{$month}'
			ORDER BY AUDIT_DATE	
	";
	
	$x = 0;
	while($row = $oci->fetch($sql)){
		$tbl .= "<tr class='" . ($x++ % 2 == 0 ? 'even' : 'odd') ."'>
				<td style='text-align: left;'>{$row["LOCATION"]}</td>
				<td style='text-align: left;'>{$row["ORG"]}</td>
				<td>{$row["AUDIT_DATE"]}</td>
				<td style='text-align: left;'>{$row["CREDIT_MONTH"]}</td>
				<td style='text-align: left;'>{$row["COMPLETED_BY"]}</td>
				<td style='padding-right: 5px; padding-left: 5px; font-size: 10px;'>[ <a href='#' onclick='sot.redtag._delete({$row["RTA_ID"]}); return false;'>Delete ]</a></td>
				<td style='padding-right: 5px; padding-left: 5px; font-size: 10px;'>[ <a href='#' onclick='sot.redtag.view({$row["RTA_ID"]}); return false;'>View</a> ]</td>
			</tr>";
	}
	
	if($x == 0){
		$tbl .= "<tr>
					<td colspan='7'>
						There are no audits to display with the currrent filters ({$year}).
					</td>
				</tr>";
	}
	echo "
	<div style = 'margin-left: 5px;'>
		<form method = 'GET' style = 'overflow: hidden; padding: 0px;'>
			<table style = 'font-size: 10px;'>
				<tr><td colspan = '3'><span style='font-size: 10px;'>Auto populated from available audits</span></td></tr>
				<tr style = ''>
					<td style='padding-bottom: 3px; padding-right: 3px;'>Audit Date Year</td>
					<td>
						{$select}
					</td>
				</tr>
				<tr>
					<td style='padding-bottom: 3px; padding-right: 3px;'>Credit Month</td>
					<td>
						{$select_months}
					</td>
				</tr>
				<tr>
					<td></td>
					<td style = 'text-align: left; padding-bottom: 3px;'>
						" . (!empty($_GET["delegate"]) ? "<input type='hidden' name='delegate' value='{$_GET["delegate"]}'/>" : "") . "
						<input style = 'height: 18px; width: 50px; font-size: 11px;' type = 'submit' value = 'Filter'/>
					</td>
				</tr>
			</table>
		</form>
	</div>
	<div style = ''>
		<table class='tbl'>
			{$tbl}
		</table>
	</div>
	";
?>