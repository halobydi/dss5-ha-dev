<?php
require_once("../src/php/require.php");
$oci = new mcl_Oci("soteria");

$privileges = auth::check('privileges');
if(!$privileges["MANAGE_SWO_ITEMS"]){
	auth::deny();
}

$sql = "
	SELECT *
	FROM ORGANIZATIONS ORDER BY ORG_CODE
";

$orgs = array();
while($row = $oci->fetch($sql)){
	$orgs[$row["ORG_CODE"]] = array();
}
	
if(!empty($_POST)){
	$subitemnum = $_POST["subitemnum"];
	$text = $_POST["subitemtext"];
	$active = $_POST["active"];
	$office = $_POST["office"];
	$field = $_POST["field"];
	$org_p = $_POST["org"];

	$stmt_s_u = $oci->parse("
		UPDATE SWO_SUBITEMS
		SET SUBITEM_TEXT = :text,
			ACTIVE = :active,
			OFFICE = :office,
			FIELD = :field
		WHERE SUBITEM_NUM = :subitemnum
	");
	
	$stmt_s_i = $oci->parse("
		INSERT INTO SWO_SUBITEMS
		 (
			PARENT_ITEM_NUM,
			SUBITEM_NUM,
			SUBITEM_TEXT,
			ACTIVE,
			OFFICE,
			FIELD
		) VALUES(
			{$_POST["parent_item_num"]},
			EPOP_ITEM_UNIQUE.NEXTVAL,
			:text,
			:active,
			:office,
			:field
		) RETURNING SUBITEM_NUM INTO :subitemnum
	");
	
	$values['itemnum'] = array(0, SQLT_INT);
	
	$stmt_o_i = $oci->parse("
		INSERT INTO SWO_ORG_SUBITEMS
			 (
				ORG_CODE,
				SUBITEM_NUM
			) VALUES (
				:orgcode,
				:subitemnum
			)
	");
	$stmt_o_d = $oci->parse("
		DELETE FROM SWO_ORG_SUBITEMS
		WHERE SUBITEM_NUM = :subitemnum
		AND ORG_CODE = :orgcode
	");
	foreach($subitemnum as $key=>$value){
		if($_POST["deleted_{$key}"] == "true"){
			$oci->query("
				DELETE FROM SWO_SUBITEMS
				WHERE SUBITEM_NUM = {$value}
			");
			$oci->query("
				DELETE FROM SWO_ORG_SUBITEMS
				WHERE SUBITEM_NUM = {$value}
			");
		} else {
			if(!empty($text[$key])){
				$subitem_unique = false;
			
				if(!empty($value)){ //update
					$oci->bind($stmt_s_u, array(
						":text" 		=> $text[$key],
						":subitemnum" 	=> $value,
						":active" 		=> $active[$key] == "on" ? 1 : 0,
						":office" 		=> $office[$key] == "on" ? 1 : 0,
						":field" 		=> $field[$key] == "on" ? 1 : 0
					));
					
					$subitem_unique = $value;
				} else { //insert
					$subitem_unique = $oci->bind($stmt_s_i, array(
						":text" 	=> $text[$key],
						":active" 	=> $active[$key] == "on" ? 1 : 0,
						":office" 	=> $office[$key] == "on" ? 1 : 0,
						":field" 	=> $field[$key] == "on" ? 1 : 0,
						":subitemnum"  => array(0, SQLT_INT)
					), true, true);
				}

				foreach($orgs as $orgcode=>$v){
					
					if(is_array($subitem_unique)){
						$subitem_unique = $subitem_unique[":subitemnum"];
					}
					
					if(isset($org_p[$key][$orgcode])){
						$oci->bind($stmt_o_i, array(
							":orgcode"	=> $orgcode,
							":subitemnum" => $subitem_unique
						));
					} else {
						$oci->bind($stmt_o_d, array(
							":orgcode"	=> $orgcode,
							":subitemnum" => $subitem_unique
						));
					}
				}
			}
		}
	}
	
	header("Location: managesubitems.php" . (!empty($_GET["itemnum"]) ? "?itemnum={$_GET["itemnum"]}" : ""));
}

$sql = "
	SELECT  ITEM_NUM,
		ITEM_CATEGORY,
		ITEM_TEXT,
		SUM(
				CASE WHEN SUBITEM_NUM IS NULL THEN 0 ELSE 1 END
		) AS CT    
	FROM EPOP_ITEMS I
	LEFT JOIN SWO_SUBITEMS SI
		ON SI.PARENT_ITEM_NUM = ITEM_NUM
	WHERE HAS_SUBITEMS = 1
	GROUP BY ITEM_NUM,
		ITEM_CATEGORY,
		ITEM_TEXT
";
?>
<table class = 'tbl'>
	<tr>
		<th colspan = '4'><div class = 'inner_title'>Safe Worker Observation Items With Sub-Items</div></th>
	</tr>
	<tr>
		<th><div class = 'inner' style = 'width: 60px;'>Item No.</div></th>
		<th><div class = 'inner' style = 'width: 150px;'>Item Category</div></th>
		<th><div class = 'inner' style = 'width: 500px;'>Item Text</div></th>
		<th><div class = 'inner' style = 'width: 100px;'>No. of Sub-Items</div></th>
	</tr>
<?php
	while($row = $oci->fetch($sql)){
		$style = 'style="text-align: left; width: 500px; height: 30px;"';
		$bg = ($_GET["itemnum"] == $row["ITEM_NUM"] ? "style='background-color: #ffffca;'" : "");
		
		echo "
			<tr class = '" . ($x++ % 2 == 0 ? 'even' : 'odd') . "' {$bg}>
				<td>{$row["ITEM_NUM"]}</td>
				<td>{$row["ITEM_CATEGORY"]}</td>
				<td {$style}>{$row["ITEM_TEXT"]}</td>
				<td><a href = '?itemnum={$row["ITEM_NUM"]}'>{$row["CT"]}</a></td>
			</tr>
		";
	}
?>
</table>
<?php

if(!empty($_GET["itemnum"])){
	$sql = "
		SELECT * FROM SWO_SUBITEMS
		WHERE PARENT_ITEM_NUM = {$_GET["itemnum"]}
	";
	
	echo "
	<form action = '' method = 'POST'>
		<input type = 'submit' value = 'Add Item' style = 'margin-left: 5px;' onclick = 'dojo.byId(\"addNewItem\").style.display = \"\";
																							dojo.byId(\"nodata\").style.display = \"none\"; 
																							this.style.display = \"none\"; 
																							return false;'/>
		<input type = 'submit' name = 'submitter' value = 'Save Changes' style = 'margin-left: 5px;'/>
		<input type = 'hidden' name = 'parent_item_num' value = '{$_GET["itemnum"]}'/>
	";
	
	echo "
		<table class = 'tbl'>
			<tr>
				<th><div class = 'inner' style = 'width: 60px;'>Item No.</div></th>
				<th><div class = 'inner' style = 'width: 300px;'>Item Text</div></th>
				<th><div class = 'inner' style = 'width: 60px;'>Active</div></th>
				<th><div class = 'inner' style = 'width: 60px;'>Office</div></th>
				<th><div class = 'inner' style = 'width: 60px;'>Field</div></th>
				<th><div class = 'inner' style = 'width: 300px;'>Organizations</div></th>
				<th><div class='inner' style='width: 100px;'></div></th>
			</tr>
	";

	
	$stmt_o = $oci->parse("
		SELECT
			ORG_CODE
		FROM
			SWO_ORG_SUBITEMS
		WHERE
			SUBITEM_NUM = :subitem
	");
	
	$x = 0;
	$style = 'style="text-align: left; width: 300px;"';
	$class = ($x++ % 2 == 0 ? 'even' : 'odd');
	echo "
	<tr class = '{$class}' id = 'addNewItem' style= 'display: none;'>
		<td>
			<input type = 'hidden' name = 'subitemnum[{$x}]'/>
		</td>
		<td {$style}>
			<textarea name = 'subitemtext[{$x}]' style = 'width: 290px;'></textarea>
		</td>
		<td>
			<input type = 'checkbox' name = 'active[{$x}]'/>
		</td>
		<td>
			<input type = 'checkbox' name = 'field[{$x}]'/>
		</td>
		<td>
			<input type = 'checkbox' name = 'office[{$x}]'/>
		</td>
		<td style='text-align: left;'>
	";
	foreach($orgs as $key=>$value){
		echo "<input type = 'checkbox' name = 'org[{$x}][{$key}]'/> {$key}</br />";
	}
	
	echo "
			</td>
			<td>
			</td>
		</tr>
	";
			
	while($row = $oci->fetch($sql)){
		$org_subitem = array();
		$oci->bind($stmt_o, array(
			"subitem" => $row["SUBITEM_NUM"]
		));
		
		while($row2 = $oci->fetch($stmt_o, false)){
			$org_subitem[$row2["ORG_CODE"]] = true;
		}
		
		echo "
			<tr class = '" . ($x++ % 2 == 0 ? 'even' : 'odd') ."'>
				<td>
					<input type = 'hidden' value = '{$row["SUBITEM_NUM"]}' name = 'subitemnum[{$x}]'/>
					<input type = 'hidden' value = 'false' name = 'deleted_{$x}' id = 'deleted_{$x}'/>
					{$row["SUBITEM_NUM"]}
				</td>
				<td {$style}><textarea name = 'subitemtext[{$x}]' style = 'width: 290px;'>{$row["SUBITEM_TEXT"]}</textarea></td>
				<td><input type = 'checkbox' name = 'active[{$x}]'" . ($row["ACTIVE"] == 1 ? "checked=checked" : "") ."/></td>
				<td><input type = 'checkbox' name = 'office[{$x}]'" . ($row["OFFICE"] == 1 ? "checked=checked" : "") ."/></td>
				<td><input type = 'checkbox' name = 'field[{$x}]'"  . ($row["FIELD"] == 1 ? "checked=checked" : "") ."/></td>
				<td style='text-align: left;'>
		";
		
		foreach($orgs as $key=>$value){
			echo "<input type = 'checkbox' name = 'org[{$x}][{$key}]' " . ($org_subitem[$key] ? "checked=checked" : "") ."/>{$key}<br/>";
		}
		
		echo "
				</td>
				<td><a href = '#' onclick = 'this.parentNode.parentNode.style.display = \"none\"; dojo.byId(\"deleted_{$x}\").value = \"true\"; return false;'>Delete</a></td>
			</tr>
		";
	}
	
	echo "
		<tr id = 'nodata' style = 'display: " . ($x == 1 ? "" : "none") .";'>
			<td colspan = '" . (6 + count($orgs)) ."'>There are no sub-items for this item.</td>
		</tr>
	";
	
	
	echo "</form>
		</table>
	";
}

?>
