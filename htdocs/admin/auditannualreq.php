<?php
require_once("../src/php/require.php");
$privileges = auth::check('privileges');

if(!$privileges["MANAGE_RED_TAG_AUDITS"]){
	auth::deny();
}


$oci = new mcl_Oci("soteria");

$year = (!empty($_GET["year"]) ? $_GET["year"] : date('Y'));
$month = intval((!empty($_GET["month"]) ? $_GET["month"] : date('m')));

$select = "<select name='year' style = 'height: 20px; font-size: 10px;'>";
$years = 0;
$sql = "SELECT 	DISTINCT TO_CHAR(AUDIT_DATE, 'YYYY') AS YEAR 
		FROM 	RED_TAG_AUDITS 
		WHERE 	AUDIT_DATE IS NOT NULL 
				AND COMPLETE = 1
		UNION  
		SELECT TO_CHAR(SYSDATE, 'YYYY') 
		FROM 	DUAL
		UNION  
		SELECT TO_CHAR(SYSDATE + 365, 'YYYY') 
		FROM 	DUAL";
while($row = $oci->fetch($sql)){
	$years++;
	$selected = $year == $row["YEAR"] ? "selected=selected" : "";
	$select .= "<option {$selected} value='{$row["YEAR"]}'>{$row["YEAR"]}</option>";
}

if($years == 0){
	$select .= "<option value='{$year}'>{$year}</option>";
}
$select .= "</select>";

$months = array(
	1 => "January",
	2 => "February",
	3 => "March",
	4 => "April",
	5 => "May",
	6 => "June",
	7 => "July",
	8 => "August",
	9 => "September",
	10 => "October",
	11 => "November",
	12 => "December"
);

$select_months = "<select name='month' style='font-size: 10px;'>";
foreach($months as $key=>$value) {
	$select_months .= "<option value='{$key}' " . ($month == $key ? "selected=selected" : "") . " >{$value}</option>";
}
$select_months .= "</select>";

$sql = "
	SELECT  LOCATION,
			ORG,
			TITLE,
			NVL(TOTAL, 0) AS TOTAL,
			MONTH,
			YEAR,
			ID
				
	FROM    RED_TAG_AUDITS_LOCATIONS L  
	
	LEFT    JOIN RED_TAG_AUDITS_REQUIREMENTS
				ON LOCATION_ID = ID
				AND YEAR = {$year}
				AND MONTH = {$month}
    ORDER 	BY TITLE, ORG, LOCATION

";

echo "<div style = 'margin-left: 5px;'>
		<form method = 'GET' style = 'overflow: hidden; padding: 0px;'>
			<table style = 'font-size: 10px;'>
				<tr><td colspan = '3'><span style='font-size: 10px;'>Auto populated from available audits</span></td></tr>
				<tr style = ''>
					<td style='padding-bottom: 3px; padding-right: 3px;'>Audit Date Year</td>
					<td>
						{$select}
					</td>
				</tr>
				<tr>
					<td style='padding-bottom: 3px; padding-right: 3px;'>Credit Month</td>
					<td>
						{$select_months}
					</td>
				</tr>
				<tr>
					<td></td>
					<td style = 'text-align: left; padding-bottom: 3px;'>
						" . (!empty($_GET["delegate"]) ? "<input type='hidden' name='delegate' value='{$_GET["delegate"]}'/>" : "") . "
						<input style = 'height: 18px; width: 50px; font-size: 11px;' type = 'submit' value = 'Filter'/>
					</td>
				</tr>
			</table>
		</form>
	</div>
	<table class='tbl'>
		<tr>
			<th>
				<div class='inner' style='width: 200px;'>
					Location
				</div>
			</th>
			<th>
				<div class='inner' style='width: 200px;'>
					Organization
				</div>
			</th>
			<th>
				<div class='inner' style='width: 100px;'>
					Month
				</div>
			</th>
			<th>
				<div class='inner' style='width: 100px;'>
					Year
				</div>
			</th>
			<th>
				<div class='inner' style='width: 100px;'>
					Required
				</div>
			</th>
		</tr>
	";

while($row = $oci->fetch($sql)){
	if($row["TITLE"] != $prev_section){
		echo "<tr>
				<td style='border-top: 1px solid black; border-bottom: 1px solid black; font-weight: bold; text-align: left;' colspan='5'>{$row["TITLE"]}</td>
			</tr>";
	}
	$class = ($x++ % 2 == 0 ? 'even' : 'odd');
	echo "<tr class='{$class}'>";
		echo "<td style='text-align: left;'>{$row["LOCATION"]}";
		echo "<td style='text-align: left;'>{$row["ORG"]}</td>";
		echo "<td>{$months[$month]}</td>";
		echo "<td>{$year}</td>";
		echo "<td>
			<input 
				type='text' 
				style='width: 100%; font-style: italic; border: none; background-color: transparent; text-align: center;' 
				value='{$row["TOTAL"]}' 
				onfocus='this.parentNode.parentNode.className=\"active\"'; 
				onblur ='this.parentNode.parentNode.className=\"{$class}\"';
				onchange='sot.redtag.req({$row["ID"]}, {$month}, {$year}, this.value);'
			/>
		</td>";
	echo "</tr>";
	$prev_section = $row["TITLE"];
}

echo "
	</table>
</form>
";
?>