<?php
require_once("../src/php/require.php");
$oci = new mcl_Oci('soteria');

$privileges = auth::check('privileges');
if(!$privileges["MANAGE_SWO_ITEMS"]){
	auth::deny();
}

if(!empty($_POST)){
	$itemnum = $_POST["itemnum"];
	$category = $_POST["itemcategory"];
	$text = $_POST["itemtext"];
	$subitems = $_POST["subitems"];
	$active = $_POST["active"];
	$office = $_POST["office"];
	$field = $_POST["field"];
	$lfc = $_POST["lfc"];

	if($_POST["submitter"] == "Save Changes" && is_array($itemnum)){
		$sql = "
				UPDATE
					EPOP_ITEMS
				SET
					ITEM_CATEGORY = :category,
					ITEM_TEXT = :text,
					HAS_SUBITEMS = :subitems,
					ACTIVE = :active,
					OFFICE = :office,
					FIELD = :field,
					LIFE_CRITICAL = :lfc
				WHERE
					ITEM_NUM = :itemnum
			";

			
		$stmt = $oci->parse($sql);
		
		$error = array();
		foreach($itemnum as $key=>$value){

			if($_POST["deleted_{$key}"] === "true"){
					/** @TODO: Remove delete */
					$oci->query("DELETE FROM EPOP_ITEMS WHERE ITEM_NUM = {$value}");
			} else {
				if(!$oci->bind($stmt, array(
					":category" 	=> $category[$key],
					":text" 		=> $text[$key],
					":subitems" 	=> $subitems[$key] == "on" ? 1 : 0,
					":active" 		=> $active[$key] == "on" ? 1 : 0,
					":itemnum" 		=> $value,
					":office" 		=> $office[$key] == 'on' || ($category[$key] == 'Paired Performance' || $category[$key] == 'Qualified Electrical Worker')  ? 1 : 0,
					":field" 		=> $field[$key] == 'on' || ($category[$key] == 'Paired Performance' || $category[$key] == 'Qualified Electrical Worker')  ? 1 : 0,
					":lfc" 		=> $lfc[$key] == 'on' ? 1 : 0,
				))){
					$error[$value] == true;
				}
			}
		}
		
		$oci->commit();
	} else {
		$sql = "
			INSERT INTO EPOP_ITEMS
				VALUES(
					EPOP_ITEM_UNIQUE.NEXTVAL,
					:category,
					:text,
					:subitems,
					:active
					:itemnum,
					:office,
					:field,
					:lfc
				)
			";
			
		$stmt = $oci->parse($sql);
		
		$error = false;
		if(!$oci->bind($stmt, array(
			":category" 	=> $category,
			":text" 		=> $text,
			":office" 		=> $office == 'on' || ($category == 'Paired Performance' || $category == 'Qualified Electrical Worker') ? 1 : 0,
			":field" 		=> $field == 'on' || ($category == 'Paired Performance' || $category == 'Qualified Electrical Worker') ? 1 : 0,
			":subitems" 	=> $subitems == "on" ? 1 : 0,
			":active" 		=> $active == "on" ? 1 : 0,
			":lfc" 		=> $lfc == "on" ? 1 : 0,
		))){
			$error = true;
		}
	}

	//Clean Post
	if(empty($error)){
		header("Location: manageitems.php");
	}
}

if(!empty($error) && is_array($error)){
	foreach($error as $key=>$value){
		$no .= (empty($no) ? "" : ", ") . $key;
	}
	
	echo "<div class = 'error' style = 'margin: 5px'>
		Unable to save changes for item no. {$no}
	</div>";
} else if($error){
	$error = $oci->error();
	$msg = 'Unable to add new item.';
	if(substr($error["message"], 0, 9) == 'ORA-00001'){
		$msg = 'Item already exists.';
	}
	
	echo "<div class = 'error' style = 'margin: 5px'>
		{$msg} - {$error['message']}
	</div>";
}


$sql = "SELECT * FROM LIFE_CRITICAL";
$lifeCritical = array();
while($row = $oci->fetch($sql)) {
	$lifeCritical[$row['LC_CODE']] = $row["LC_TEXT"];
}

$sql = "
	SELECT
		*
	FROM
		EPOP_ITEMS
	ORDER BY
		ITEM_CATEGORY DESC,
		ITEM_NUM
";


mcl_Html::s(mcl_Html::SRC_JS, <<<JS
	function changeCategory(value) { 
		if(value == "Qualified Electrical Worker" || value == "Paired Performance") {
			dojo.byId('office').disabled = true;
			dojo.byId('field').disabled = true;
		} else {
			dojo.byId('office').disabled = false;
			dojo.byId('field').disabled = false;
		}
	}
JS
);
?>
<div id = 'addNewItem' style = 'display: none;'>
	<form action = '' method = 'POST'>
		<table class = 'tbl'>
			<tr>
				<th><div class = 'inner' style = 'width: 60px;'></div></th>
				<th><div class = 'inner' style = 'width: 230px;'>Item Category</div></th>
				<th><div class = 'inner' style = 'width: 450px;'>Item Text</div></th>
				<th><div class = 'inner' style = 'width: 100px;'>Life Critical Pilot?</div></th>
				<th><div class = 'inner' style = 'width: 100px;'>Has Sub-Items?</div></th>
				<th><div class = 'inner' style = 'width: 100px;'>Active</div></th>
				<th><div class = 'inner' style = 'width: 100px;'>Office</div></th>
				<th><div class = 'inner' style = 'width: 100px;'>Field</div></th>
				<th><div class = 'inner' style = 'width: 60px;'></div></th>
			</tr>
			<tr class = 'even'>
				<td style = 'border: none;'></td>
				<td style = 'border: none;'>
					<select name = 'itemcategory' onchange='changeCategory(this.value);'>
						<option value = 'Individual'>Individual</option>
						<option value = 'Leader'>Leader</option>
						<option value = 'Organization'>Organization</option>
						<option value = 'Offsite'>Offsite</option>
						<option value = 'Onsite'>Onsite</option>
						<option value = 'Paired Performance'>Paired Performance</option>
						<option value = 'Qualified Electrical Worker'>Qualified Electrical Worker</option>
						<?php foreach($lifeCritical as $key=>$value): ?>
								<option value="<?=$key?>"><?=$key?></option>
						<?php endforeach; ?>
					</select>
				</td>
				<td style = 'border: none;'>
					<textarea name = 'itemtext' style = 'width: 440px'></textarea>
				</td>
				<td style = 'border: none;'>
					<input name = 'lfc' id='lfc' type = 'checkbox'/>
				</td>
				<td style = 'border: none;'>
					<input name = 'subitems' type = 'checkbox'/>
				</td>
				<td style = 'border: none;'>
					<input name = 'active' type = 'checkbox'/>
				</td>
				<td style = 'border: none;'>
					<input name = 'office' id='office' type = 'checkbox'/>
				</td>
				<td style = 'border: none;'>
					<input name = 'field' id='field' type = 'checkbox'/>
				</td>
				<td style = 'border: none;'></td>
			</tr>
		</table>
		<input type = 'submit' name = 'submitter' value = 'Save Item' style = 'margin-left: 5px;'/>
		<input type = 'submit' value = 'Cancel' style = 'margin-left: 5px;' onclick = 'dojo.byId("addNewItem").style.display = "none"; dojo.byId("saveChanges").style.display = "block"; return false;'/>
	</form>
</div>
<div id = "saveChanges" style = 'margin-top: 5px;'>
	<form action = '' method = 'POST'>
		<input type = 'submit' value = 'Add Item' style = 'margin-left: 5px;' onclick = 'dojo.byId("addNewItem").style.display = "block"; dojo.byId("saveChanges").style.display = "none"; return false;'/>
		<input type = 'submit' name= 'submitter' value = 'Save Changes' style = 'margin-left: 5px;'/>
</div>
<table class = 'tbl'>
	<tr>
		<th colspan = '9'><div class = 'inner_title'>Safe Worker Observation Items</div></th>
	</tr>
	<tr>
		<th><div class = 'inner' style = 'width: 60px;'>Item No.</div></th>
		<th><div class = 'inner' style = 'width: 230px;'>Item Category</div></th>
		<th><div class = 'inner' style = 'width: 450px;'>Item Text</div></th>
		<th><div class = 'inner' style = 'width: 100px;'>Life Critical Pilot?</div></th>
		<th><div class = 'inner' style = 'width: 100px;'>Has Sub-Items?</div></th>
		<th><div class = 'inner' style = 'width: 100px;'>Active</div></th>
		<th><div class = 'inner' style = 'width: 100px;'>Office</div></th>
		<th><div class = 'inner' style = 'width: 100px;'>Field</div></th>
		<th><div class = 'inner' style = 'width: 60px;'></div></th>
	</tr>
<?php

while($row = $oci->fetch($sql)){

	$options = "
		<option value = 'Leader' " . ($row["ITEM_CATEGORY"] == "Leader" ? "selected=selected" : "") . " >Leader</option>
		<option value = 'Individual' " . ($row["ITEM_CATEGORY"] == "Individual" ? "selected=selected" : "") . " >Individual</option>
		<option value = 'Organization' " . ($row["ITEM_CATEGORY"] == "Organization" ? "selected=selected" : "") . " >Organization</option>
		<option value = 'Offsite' " . ($row["ITEM_CATEGORY"] == "Offsite" ? "selected=selected" : "") . " >Offsite</option>
		<option value = 'Onsite' " . ($row["ITEM_CATEGORY"] == "Onsite" ? "selected=selected" : "") . " >Onsite</option>
		<option value = 'Paired Performance' " . ($row["ITEM_CATEGORY"] == "Paired Performance" ? "selected=selected" : "") . " >Paired Performance</option>
		<option value = 'Qualified Electrical Worker' " . ($row["ITEM_CATEGORY"] == "Qualified Electrical Worker" ? "selected=selected" : "") . " >Qualified Electrical Worker</option>
	";

	foreach($lifeCritical as $key=>$value) {
		$options .= "<option value='{$key}' " . ($row['ITEM_CATEGORY'] == $key ? "selected=selected" : "") . ">{$key}</option>";
	}

	$disabled = ($row["ITEM_CATEGORY"] == 'Qualified Electrical Worker' || $row["ITEM_CATEGORY"] == 'Paired Performance' ? 'disabled=disabled' : '');
	echo "<tr class = '" . ($x++ % 2 == 0 ? 'even' : 'odd') . "'>
			<td>
				<input type = 'hidden' value = '{$row["ITEM_NUM"]}' name = 'itemnum[{$x}]'/>
				<input type = 'hidden' value = 'false' name = 'deleted_{$x}' id = 'deleted_{$x}'/>
				{$row["ITEM_NUM"]}
			</td>
			<td>
				<select name = 'itemcategory[{$x}]' style='width: 200px;'>
					{$options}
				</select>
			</td>
			<td><textarea name = 'itemtext[{$x}]' style = 'width: 440px;'>{$row["ITEM_TEXT"]}</textarea></td>
			<td><input name = 'lfc[{$x}]' type = 'checkbox' " . ($row["LIFE_CRITICAL"] == 1 ? "checked=checked" : "") . " {$disabled}/></td>
			<td><input name = 'subitems[{$x}]' type = 'checkbox' " . ($row["HAS_SUBITEMS"] == 1 ? "checked=checked" : "") . " {$disabled}/></td>
			<td><input name = 'active[{$x}]' type = 'checkbox' " . ($row["ACTIVE"] == 1 ? "checked=checked" : "") . " /></td>
			<td><input name = 'office[{$x}]' type = 'checkbox' " . ($row["OFFICE"] == 1 ? "checked=checked" : "") . " {$disabled}/></td>
			<td><input name = 'field[{$x}]' type = 'checkbox' " . ($row["FIELD"] == 1 ? "checked=checked" : "") . " {$disabled}/></td>
			<td><a href = '#' onclick = 'this.parentNode.parentNode.style.display = \"none\"; dojo.byId(\"deleted_{$x}\").value = \"true\"; return false;'>Delete</a></td>
	</tr>";
}

?>
</form>
</table>