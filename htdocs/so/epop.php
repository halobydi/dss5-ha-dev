<?php
	function errorHandler($errno, $errstr, $errfile, $errline, $errcontext){
		
	}
	header("X-UA-Compatible: IE=edge");
	header("Content-type: text/html; charset=iso-8859-1"); 
	set_error_handler('errorHandler');
	require_once('mcl_Html.php');
	require_once('mcl_Oci.php');
	require_once("/opt/apache/servers/soteria/htdocs/src/php/auth.php");
	
	mcl_Html::title("SOTeria - Enterprise Performance Observation");
	
	mcl_Html::js(mcl_Html::DOJO);
	mcl_Html::js(mcl_Html::DOJO_WINDOW);
	mcl_Html::js(mcl_Html::AJAX);
	mcl_Html::js(mcl_Html::CALENDAR);
	mcl_Html::s(mcl_Html::INC_JS, "../src/js/tools/cover.js");
	mcl_Html::s(mcl_Html::INC_JS, "../src/js/tools/ajax.js");
	mcl_Html::s(mcl_Html::INC_JS, "../src/js/swo.js");
    mcl_Html::s(mcl_Html::INC_JS, "../forms/js/jquery.js");
	mcl_Html::s(mcl_Html::INC_JS, "../forms/js/bootstrap.min.js");
	
	//mcl_Html::css(mcl_Html::CALENDAR);
	mcl_Html::s(mcl_Html::INC_CSS, "{$baseDir}/src/css/jscal2.css");
	mcl_Html::s(mcl_Html::INC_CSS, "../src/css/form.css");
	mcl_Html::s(mcl_Html::INC_CSS, "../forms/Content/bootstrap.min.css");

	if(auth::check("status") != "authorized") {
		header("Location: https://soteria.dteco.com/so");

	}
	
	$oci = new mcl_Oci('soteria');
	
	$crew = $_GET["crew"];
	$date = $_GET["date"];
	
	$m = substr($date, 0, 2);
	$d = substr($date, 3, 2);
	$y = substr($date, 6, 4);

	//$carma = new mcl_Oci("do_carma_rpt", "car0310do17", "pr82");
	$carma = new mcl_Oci("carma");
	
	$sql = "
		SELECT EPOP_ID,
				SUPERVISOR,
				COMPLETED_BY,
				TO_CHAR(COMPLETED_DATE, 'MM/DD/YYYY HH24:MI:SS') AS COMPLETED
		FROM EPOP_CREW_OBSERVATIONS
		WHERE CREW = {$crew}
		AND TO_CHAR(COMPLETED_DATE, 'MM/DD/YYYY') = '{$date}'
		ORDER BY COMPLETED_DATE DESC
	";
	

	$row = $oci->fetch($sql);
	
	$crew_observed  = false;
	$crew_observation = '';
	
	if($row){
		$crew_observed = true;
		$crew_observation = "<span style='color: red;'>Crew has already been observed on {$date} by {$row["SUPERVISOR"]}. <a href='#' onclick='sot.swo.viewCrew({$row["EPOP_ID"]});'>Click here</a> to view the observation.</span>";
	} 
	$sql = "
		SELECT	EMP_ID,
				EMP_NAME,
				CASE WHEN ECA.ECA_EMP_ID = C.CREW_EMP_ID THEN 1 ELSE 0 END AS LEADER
		FROM 	DAILY_ROSTER DR
		JOIN DAILY_CREW_REQUIREMENTS DCR
			ON DCR.DCR_DR_ID = DR.DR_ID
		LEFT JOIN EMPLOYEE_CREW_ASSIGNMENTS ECA
			ON ECA.ECA_DCR_ID = DCR.DCR_ID
		LEFT JOIN CREWS C
			ON C.CREW_ID = DCR.DCR_CREW_ID
		LEFT JOIN EMPLOYEES E
			ON E.EMP_ID = ECA.ECA_EMP_ID
		WHERE DCR_CREW_ID = {$crew} 
			AND TO_CHAR(DR.DR_SCHED_DT, 'YYYYMMDD') = '{$y}{$m}{$d}'
		ORDER BY LEADER ASC
	";

	$observed_stmt = $oci->parse("
		SELECT COUNT(*) AS CT 
		FROM 	EPOP_OBSERVATIONS 
		WHERE EMPLOYEE = :usid 
			AND TO_CHAR(OBSERVED_DATE, 'MM/DD/YYYY') = '{$date}'
			AND SWO = 1
			AND OFFICE_FIELD = 'FIELD'
		");
	$who_to_observe = '';
	
	$maximo = new mcl_Oci("maximo");
	$lookup = $maximo->parse("SELECT GIVEN_NAME || ' ' || LAST_NAME AS NAME, USER_ID AS USID FROM DOBW.EMPLOYEE WHERE SAP_ID = :sapid");
	while ($row = $carma->fetch($sql)){
		$maximo->bind($lookup, array(
			":sapid"	=> $row["EMP_ID"]
		));

		$employee = $maximo->fetch($lookup, false);	
		if(empty($employee)) { continue; }
		
		$oci->bind($observed_stmt, array(
			":usid"		=> $usid
		));
		
		
		$leader = ($row["LEADER"] == 1 ? " - <b>Leader</b>" : "");
		$row2 = $oci->fetch($observed_stmt, false);
		$x++;
		if ($row2["CT"] == 0){
			$name = $name["fname"];
			$who_to_observe .= "<span style='background-color:#fff;'/>
				<input type='hidden' name='name[{$x}]' value='{$employee["NAME"]}'/>
				<input type='checkbox' checked=checked name='usid[{$x}]' value='{$employee["USID"]}'/>{$employee["NAME"]} ({$employee["USID"]}) {$leader}
			</span> ";
		} else {
			$who_to_observe .= "<input type='checkbox' checked=checked disabled=disabled />{$employee["NAME"]} {$leader} ";
		}
	}
	
	$completed_by = auth::check("usid");

?>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>

<div id='container' class='center-block'>
	<div class='header'>
		Enterprise Performance Observation
	</div>
	<form name = 'swo' action = '../src/php/engine.php?f=saveCrewEPOP' method='POST' target='submit' onsubmit='return sot.swo.onSubmitCrew();' target='submit'>
	<table border = "0" style = "width: 800px;">
		<tr>
			<td style='font-weight: bold; width: 300px;'>
				Supervisor
			</td>
			<td style='width: 250px;'>
				<input type = "text" name='supervisor' value = "<?php echo auth::check('name'); ?>" onkeydown='return false;'/>
			</td>
			<td style='font-weight: bold; width: 200px;'>
				Service Center
			</td>
			<td>
				<input name = "sc" type = "text" value = "<?php echo $_GET["sc"]; ?>" onkeydown='return false;'/>
			</td>
		</tr>
		
		<tr>
			<td style='font-weight: bold; width: 300px;'>
				Crew
			</td>
			<td>
				<input name = "crew" type = "text" value = "<?php echo $_GET["crew"]; ?>" onkeydown='return false;'/>
			</td>
			<td style='font-weight: bold; width: 200px;'>
				OHUG
			</td>
			<td>
				<input name = "ohug" type = "text" value = "<?php echo $_GET["ohug"]; ?>" onkeydown='return false;'/>
			</td>
		</tr>
		<tr>
			<td style='font-weight: bold; width: 300px;'>
				Date
			</td>
			<td>
				<input name = "date" type = "text" value = "<?php echo $_GET["date"]; ?>" onkeydown='return false;'/>
				<input name = "observed_date" type = "hidden" value = "<?php echo date('m/d/Y'); ?>" onkeydown='return false;'/>
				&nbsp <img src = "../src/img/calendar.gif" alt = "" id = "tcal" onmouseover=  "sot.swo.cal('tcal', 'date');" />
			</td>
			<td style='font-weight: bold; width: 200px;'>
				Time Visited
			</td>
			<td>
				<input name = "time" type = "text" value = "<?php echo $_GET["time"]; ?>" maxlength='20'/>
			</td>
		</tr>
		<tr>
			<td style='font-weight: bold; width: 300px;'>
				Work Location
			</td>
			<td colspan = "3">
				<input name = "location" type = "text" value = "<?php echo $_GET["location"]; ?>" style = "width: 700px" />
			</td>
		</tr>
		<tr>
			<td style='font-weight: bold; width: 700px;'>
				Notes
			</td>
			<td colspan = "3">
				<input name = "notes" type = "text" value = "<?php echo $_GET["notes"]; ?>" style = "width: 700px" maxlength='500'/>
			</td>
		</tr>
		<tr>
			<td colspan = "4" style=''>
				<?php echo $crew_observation;?> <br />
				<b>Who to Observe - Check all that apply</b><br />
				<input type='checkbox' value='on' name='observe_crew' <?php echo ($crew_observed ? "" : "checked=checked"); ?> />Crew  <?php echo $crew; ?>
				<?php echo $who_to_observe; ?>
			</td>
		<tr>
		<tr>
			<td colspan = "4">
				<input name = "leader" type = "hidden" value = "<?php echo $_GET["leader"]; ?>" readonly=readonly/>
			</td>
		</tr>
	</table>
	</table>
	<div class="clearfix"></div>
	<div class="row" style="text-align: center;">
		<img style="text-align: center;" src="../src/img/lifeCriticalStandards.png" />
	</div>

	<center style='margin-top:10px; margin-bottom: 10px;'>
		<script>
			function doWork (){
				sot.swo.markAllSat();
				var naChecked = $("td:contains('there a PROC')").prev().children(":input")[0].checked;
				if (!naChecked) {
					var iden = $("td:contains('there a PROC')").prev().prev().prev().find("[class*='S']:first")[0].name;
					sot.swo.markItem(iden, "S", true, true, true, "", false, "*Required*  List the name of the procedure, Pre-Job Brief, SWI, JIT, etc.");
				}
			}
		</script>
		<button id="markAllSatisfactoryBtn" onclick="doWork(); return false;">Mark All Satisfactory</button>&nbsp;
		<button onclick='window.location="../so/index.php"; return false;'>Cancel & Go Back to Crew Roster</button>	</center>
	</center>
	<table>
	<?php
		
		echo "
			<input type='hidden' id='completed_by' name='completed_by' value='{$completed_by}'/>
			<input type='hidden' name='swo' value ='on'/>
			<input type='hidden' name='office_field' id='office_field'value='FIELD'/>
			<input type='hidden' name='org' id='org' value='DIST OPS'/>
			<input type='hidden' id='observed_by' name='observed_by' value=" .  (is_array(auth::check("name")) ? "" : auth::check("name")) ."/>
		";
	




		//$sql = "SELECT * FROM EPOP_ITEMS WHERE ACTIVE = 1 AND FIELD = 1 AND ITEM_CATEGORY IN('Leader', 'Individual', 'Organization') ORDER BY ITEM_CATEGORY DESC, ITEM_NUM";
        $sql = "
          SELECT
            *
          FROM
            EPOP_ITEMS
          WHERE
            ACTIVE = 1
            AND ITEM_CATEGORY IN('Organization')
            AND FIELD = 1
            AND LIFE_CRITICAL = 1
          ORDER BY
            ITEM_CATEGORY DESC,
            ITEM_NUM
          ";
        $prev_category = '';
		$firstpass = true;
		while($row = $oci->fetch($sql)){
			if($row["ITEM_CATEGORY"] != $prev_category){
				$x = 0;
				
				if(!$firstpass){
					echo "<tr style='height:30px;'>
						<td colspan=4></td>
					</tr>";
				}
				echo "<tr style='font-weight:bold;'>
						<td style='width:30px; text-align:center;'>S</td>
						<td style='width:30px; text-align:center;'>IO</td>
						<td style='width:30px; text-align:center;'>NA</td>
						<td style='width:710px; text-align:center;'>{$row["ITEM_CATEGORY"]}</td>
						
					</tr>";
				$firstpass = false;
			}
			
			$has_subItems = 'false';
			if($row["HAS_SUBITEMS"] == '1'){
				$has_subItems = 'true';
			}

            $forceHideComments = 'true';
            $customPlaceholder = "";
            if (strpos($row["ITEM_TEXT"], 'for the work being observed') !== false) {
                $forceHideComments = 'false';
                $customPlaceholder = "*Required*  List the name of the procedure, Pre-Job Brief, SWI, JIT, etc.";
            }
			//25 pp
			//29 hk
			echo "<tr class='" . ($x++ % 2 == 0 ? 'even' : 'odd') . "'>
					<td style='width:30px; text-align:center; vertical-align:top;'><input type='radio' class='S' name='{$row["ITEM_NUM"]}' value='S'  onclick='sot.swo.markItem({$row["ITEM_NUM"]}, \"S\", {$has_subItems}, true, true, true, {$forceHideComments}, \"{$customPlaceholder}\");' /></td>
					<td style='width:30px; text-align:center; vertical-align:top;'><input type='radio' class='IO' id = '{$row["ITEM_NUM"]}' name='{$row["ITEM_NUM"]}' value='IO' onclick='sot.swo.markItem({$row["ITEM_NUM"]}, \"IO\", {$has_subItems}, true, true, \"{$_GET["ohug"]}\", {$forceHideComments}, \"{$customPlaceholder}\");' /></td>
					<td style='width:30px; text-align:center; vertical-align:top;'><input type='radio' class='NA' name='{$row["ITEM_NUM"]}' value='NA' onclick='sot.swo.markItem({$row["ITEM_NUM"]}, \"NA\", {$has_subItems}, true, true, true, true);' /></td>
					<td style='width:710px;'>{$row["ITEM_TEXT"]}</td>
					<td style='text-align:right;'>
                    <img src='../src/img/comments.png' id='{$row["ITEM_NUM"]}_comments_img' style='cursor:pointer; display: none;' alt='Add Comment' onclick='sot.swo.addComment({$row["ITEM_NUM"]}, this);'/>
                    </td>
				</tr>
				<tr>
					<td style='width:800px;' colspan='5' id='{$row["ITEM_NUM"]}_comments'></td>
					</td>
				</tr>
			";
			
			$prev_category = $row["ITEM_CATEGORY"];
			
			if($has_subItems == 'true'){
				echo "<tr style='padding:0px; margin:0px;'>
						<td id='{$row["ITEM_NUM"]}_subitems' colspan='5' style='width: 800px; padding:0px; margin:0px; background-color:#ffffca;'>
						</td>
					</tr>
				";
			}	
		}
	?>
	</table>
        <table>
            <tr style=' background-color: #FFE019'>
                <td  style='width:806px; text-align:center; border-bottom: 1px solid #000; border-top: 1px solid #000; padding: 3px;'>Life Critical</td>
            </tr>
        </table>
        <?php
        $prevCategory = '';
        $firstPass = true;
        function printYesNoQuestion($name, $text, $requireComments = false) {
            global $oci;
            $sql = "SELECT * FROM EPOP_ITEMS WHERE ITEM_CATEGORY = '{$name}' ORDER BY ITEM_NUM";
            $subItems = array();
            while($row = $oci->fetch($sql)) {
                $subItems[] = $row;
            }
            $disabled = count($subItems) == 0 && $name != 'procedure_for_work';
            echo "
            <table>
                <tr style='font-weight:normal; background-color: " . ($disabled ? "#F0F0F0" : "#FFE019") . ";'>
                  <td colspan='3' style='padding-top: 3px; padding-bottom: 3px; padding-left: 7px; width: 90px; '>
                    <input style='vertical-align: sub;' type='radio' name='{$name}' id='{$name}' value='1' onclick='onClickYesNo(\"{$name}\", true)'/ " . ($disabled ? "disabled=disabled" : "") . "> Yes
                    &nbsp;<input style='vertical-align: sub;' type='radio' name='{$name}' id='{$name}' value='0' onclick='onClickYesNo(\"{$name}\", false)'/ " . ($disabled ? "disabled=disabled" : "") . "> No
                  </td>
                  <td colspan='2' style='padding-top: 6px; padding-bottom: 3px;width: 716px; font-weight: bold;'>{$text}</td>
                </tr>
              ";
            if($requireComments) {
                echo "<tr id='{$name}_required_comments' style='display: none;'>
                    <td colspan='5'>
                      <textarea name='{$name}_detail' id='{$name}_detail' style='width: 100%;' onkeypress='return sot.swo.imposeMaxLength(this, 1000);'></textarea>
                    </td>
                </tr>
              ";
            }
            echo "</table>";
            $x = 0;
            foreach($subItems as $row) {
                if($x == 0) {
                    echo "<table id='{$name}_io_questions' style='display: none;'>";
                    printHeader("Additional Questions for <i>" . $text . '</i>');
                }
                printQuestion($row, ++$x, true);
            }
            if($x > 0) { echo "</table>"; }
        }
        function printHeader($category) {
            $style = "text-align:center; border-bottom: 1px solid #000; border-top: 1px solid #000; padding: 3px;";
            echo "
              <tr style='font-weight:normal; background-color: #f0f0f0;'>
                <td style='width:30px; {$style}'>S</td>
                <td style='width:30px; {$style}'>IO</td>
                <td style='width:30px; {$style}'>NA</td>
                <td style='width:710px; {$style}'>{$category}</td>
                <td style='border-bottom: 1px solid #000; border-top: 1px solid #000; padding: 3px;'></td>
              </tr>
            ";
        }
        function printQuestion($row, $x, $lcPilot = '') {
            if($row["HAS_SUBITEMS"] == '1'){
                $hasSubItems = 'true';
            } else {
                $hasSubItems = 'false';
            }
            $class = ($x % 2 == 0 ? 'even' : 'odd');

            $forceHideComments = 'true';
            $customPlaceholder = "";
            if (strpos($row["ITEM_TEXT"], 'for the work being observed') !== false) {
                $forceHideComments = 'false';
                $customPlaceholder = "*Required*  List the name of the procedure, Pre-Job Brief, SWI, JIT, etc.";
            }

            echo "
            <tr class='{$class}'>
              <td style='width:30px; text-align:center; vertical-align:top;'>
                <input lc='{$lcPilot}' type='radio' class='S' name='{$row["ITEM_NUM"]}' value='S' onclick='sot.swo.markItem({$row["ITEM_NUM"]}, \"S\", {$hasSubItems}, true, {$x}, \"{$lcPilot}\", {$forceHideComments}, \"{$customPlaceholder}\" );' />
              </td>
              <td style='width:30px; text-align:center; vertical-align:top;'>
                <input lc='{$lcPilot}' type='radio' class='IO' id = '{$row["ITEM_NUM"]}' name='{$row["ITEM_NUM"]}' value='IO' onclick='sot.swo.markItem({$row["ITEM_NUM"]}, \"IO\", {$hasSubItems}, true, {$x}, \"{$lcPilot}\", {$forceHideComments}, \"{$customPlaceholder}\" );' />
              </td>
              <td style='width:30px; text-align:center; vertical-align:top;'>
                <input lc='{$lcPilot}' type='radio' class='NA' name='{$row["ITEM_NUM"]}' value='NA' onclick='sot.swo.markItem({$row["ITEM_NUM"]}, \"NA\", {$hasSubItems}, true, {$x}, \"{ $lcPilot}\", true, \"{$customPlaceholder}\" );' />
              </td>
              <td style='width:710px;'>
                {$row["ITEM_TEXT"]}
              </td>
              <td style='text-align:right;'>
                <img src='../src/img/comments.png' id='{$row["ITEM_NUM"]}_comments_img' style='cursor:pointer; display: none;' alt='Add Comment' onclick='sot.swo.addComment({$row["ITEM_NUM"]}, this);'/>
              </td>
            </tr>
            <tr style=''>
              <td style='width:800px;' colspan='5' id='{$row["ITEM_NUM"]}_comments'></td>
              </td>
            </tr>
          ";
            if($hasSubItems == 'true') {
                echo "
              <tr style='padding: 0px; margin: 0px;'>
                <td id='{$row["ITEM_NUM"]}_subitems'
                  colspan='5' style='width: 800px;
                  padding: 0px; margin: 0px;
                  background-color: #ffffca;'>
                </td>
              </tr>
            ";
            }
        }
        while($row = $oci->fetch($sql)){
            if($row["ITEM_CATEGORY"] != $prevCategory){
                $x = 0;
                if(!$firstPass){
                    echo "
                <tr style='height:30px;'>
                  <td colspan=5></td>
                </tr>
              ";
                }
                printHeader($row['ITEM_CATEGORY']);
                $firstPass = false;
            }
            $x++;
            printQuestion($row, $x);
            $prevCategory = $row["ITEM_CATEGORY"];
        }





        $yesNoQuestions = array();
            $sql = "SELECT * FROM LIFE_CRITICAL ORDER BY SORT_ORDER";
            while($row = $oci->fetch($sql)) {
                $yesNoQuestions[$row['LC_CODE']] = $row["LC_TEXT"];
            }
            foreach($yesNoQuestions as $key=>$value) {
                printYesNoQuestion($key, $value);
            }

        mcl_Html::s(mcl_Html::SRC_JS,<<<JS
  function onClickYesNo(name, yes) {
    if(dojo.byId(name + '_required_comments')) {
      if(yes) {
        dojo.byId(name + '_required_comments').style.display = '';
      } else {
        dojo.byId(name + '_required_comments').style.display = 'none';
      }
    }
    if(dojo.byId(name + '_io_questions')) {
      if(yes) {
        dojo.byId(name + '_io_questions').style.display = '';
      } else {
        dojo.byId(name + '_io_questions').style.display = 'none';
      }
    }
    
    //This unchecks all sub questions when you select NO for the LC question
    var children = $("#" + name + "_io_questions input:radio:checked");
    if (!!children) {
      for (var i = 0; i < children.length ; i++) {
        children[i].checked = false;
       // children[i].className = "";
      }
    }
  
  }
  function toggleComments() {
    if (dojo.byId('conversation').value == 1) {
      //dojo.byId("comment_box").style.display = '';
      document.getElementById("comments").required = true;
    } else {
      //dojo.byId("comment_box").style.display = 'none';
      document.getElementById("comments").required = false;
    }
  }
JS
        );

        ?>
		<br />
		<div class="col-md-12 form-horizontal">

			<div class="form-group">
				<label for="conversation" class="col-sm-8 control-label"> Was good safety behavior recognized during this observation?</label>
				<div class="col-sm-4">
					<select class="form-control" name='good_behavior' required>
						<option value=''></option>
						<option value='0'>No</option>
						<option value='1'>Yes</option>
					</select>
					<span class="error hide">Must select conversation !</span>
				</div>
			</div>
			<div class="form-group">
				<label for="conversation" class="col-sm-8 control-label"> Did a conversation take place with the observed individual?</label>
				<div class="col-sm-4">
					<select class="form-control" name='conversation' id='conversation' onchange='toggleComments();' required>
						<option value=''></option>
						<option value='0'>No</option>
						<option value='1'>Yes</option>
					</select>
					<span class="error hide">Must select conversation !</span>
				</div>
			</div>
			<div class="form-group">
				<label for="comment" class="col-md-2 control-label"></label>
				<div class="col-md-12">
					<table style='width:100%' id='comment_box'>
						<tr>
							<td style='font-weight:bold;vertical-align: bottom;'>
								Comments
							</td>
						</tr>
						<tr>
							<td>
								<textarea class="form-control"  placeholder="*Required* Detailed conversation + any additional comments"  name='comments' id='comments' onkeypress='return sot.swo.imposeMaxLength(this, 1000);' ></textarea>
							</td>
						</tr>
					</table>
				</div>

			</div>
		</div>



	<center style='margin-top:10px; margin-bottom: 10px;'>
		<input type = 'submit' id='submit' value='Submit'>&nbsp;
		<button onclick='window.location="../so/index.php"; return false;'>Cancel & Go Back to Crew Roster</button>	
	</center>
	<input type = 'hidden' value = '<?php echo date("m/d/Y H:i:s"); ?>' name = 'begin_date'/>



	</form>
</div>
<iframe name='submit' style='width:0px; height:0px;'></iframe>