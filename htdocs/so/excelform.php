<?php
// Changes Added by Hytham ALOBYDI for Task DS-148 and It's sub-tasks

// Change Log:
//
//<DS-148>  12/21/2016  Change the report outlook to its new form  GalaxE Detroit  Hytham ALOBYDI
//
//


?>
<html>
<head>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Pre-Job Briefing Form</title>
    <!-- Added By hytham to matsh the screen view -->

    <title>Pre-Job Briefing Form</title>
    <style type='text/css'>
        form {
            margin: 0;
            padding: 0;
        }
        html {
            padding: 0;
            margin: 0;
        }
        body {
            font-family: Calibri;
            text-align: center;

        }
        table {
            border-collapse: collapse;
        }

        .border {
            border: 1px solid #000;
        }
        .bold {
            font-weight: bold;
        }
        .center {
            text-align: center;
        }
        table.col-12 {
            width: 100%;
        }
        td {
            padding: 2px 1px 1px 5px;
            font-size: 17px;
            border-color: #000;
        }


        input {
            border: none;
            font-family: Calibri;
            font-size: 17px;
            width: 100%;
            background-color: transparent;
        }
        .large-font {
            font-size: 19px;
        }
        ul li {
            list-style: disc
        }

        div.container {
            width: 1100px;
            margin: 0 auto;
            height: auto;
        }

        @media print {

        }

        td.narrow {
            width: 30px !important;
        }

        /* Hytham ALOBYDI 12/20/2016 For the new Reports*/
        .formheader{
            font-size: 24px;

            font-weight: bold;
            color: #004990;
        }
        .haz_reco_txt{
            font-size: 16px;
            font-weight: bold;
            padding: 5px 5px 5px 5px
        }

        .haz_reco_txt_span{
            font-size: 12px;

            color: #4a4d4a;
        }
        .ehaz_reco_txt{
            color: #000;
            font-weight: bold;
            padding-top: 5px;
            padding-bottom: 5px;
        }
        .crew_loc_txt{
            color: #000;
            font-size: 18px;

        }



        .report-footer>li{
            font-size: 5px;
            list-style-type:disc;
        }

        .footer-list{
            list-style-type:disc;

        }
        .table_text_items{
            font-size: 20px;
            font-family: 'Times New Roman', Times, serif;

        }
        hr{
            border: none;
            height: 3px;

            color: #000;; /* old IE */
            background-color: #000; /* Modern Browsers */


        }

        .report-section{
            margin: -5px 0px 0px 0px
        }
        .small_cell {
            padding-top: 7px;
            padding-left: 5px;
            padding-bottom: 7px;

            font-size: 18px;
            height: 100%;
            font-family: 'Times New Roman', Times, serif
        }
        #footer-section{
            position: relative;
            top:15px;
        }
        /* Added by Hytham 12/ 22/2016  */
        .line_sep{
            border-bottom: #000 solid 3px
        }
        /*  ********************************  */

    </style>
</head>
<body>
<?php
ob_start();
function errorHandler($errno, $errstr, $errfile, $errline, $errcontext){}
header("Content-type: text/html; charset=iso-8859-1");
set_error_handler('errorHandler');

$post = $_POST;
$data = unserialize(base64_decode($post['data']));
$total = count($data);

for($crewIndex = 0 ; $crewIndex <= $total ; $crewIndex++) {
    if(isset($post[$crewIndex])) {
        $employeeNames = explode("&lt;br /&gt;", $data[$crewIndex]["EMP_NAME_1"]);
        $employeeIds = explode("&lt;br /&gt;", $data[$crewIndex]["EMP_ID_1"]);
        foreach($employeeIds as $key=>$value) {
            if(empty($value)) {
                unset($employeeIds[$key]);
                unset($employeeNames[$key]);
            }
        }

        $data[$crewIndex]["EMPLOYEE_IDS"] = $employeeIds;
        $data[$crewIndex]["EMPLOYEE_NAMES"] = $employeeNames;

    } else {
        unset($data[$crewIndex]);
    }
}

if($_GET['individual'] == 'true') {
    $oldData = $data;
    $data = array();
    foreach($oldData as $index=>$crewData) {
        foreach($crewData['EMPLOYEE_IDS'] as $index2=>$employee) {
            if(!empty($employee)) {
                $data[] = array(
                    "CREW_ID" => $crewData['CREW_ID'],
                    "LEAD_ID" => $crewData['LEAD_ID'],
                    'LEAD_NAME' => $crewData['LEAD_NAME'],
                    'EMPLOYEE_NAMES' => array(
                        $crewData['EMPLOYEE_NAMES'][$index2]
                    ),
                    'EMPLOYEE_IDS' => array(
                        $employee
                    )
                );
            }
        }
    }
}


foreach($data as $index=>$crewData):
    $crewSize = count($crewData['EMPLOYEE_IDS']);
    /* $locationCount = $crewSize > 4 ? $crewSize: 4; */
    $locationCount=5;

    ?>


    <div class='container' style='page-break-after: always; height: auto'>
        <table class='col-12'>
            <tbody>
            <tr>

                <td style=" font-size: 18px; font-weight: bold;width: 100%;" colspan="2">
                    <img src='dte_logo2.jpg' style="height: 50px"/>
                    <span class="formheader">Distribution Operations (Overhead-Underground)
								Pre Job Briefing</span><br/><h7><center><span style="font-size: 10px;">Version 8 -January 2017<span><center></h7>
                </td>
            </tr>
            </tbody>
        </table>
        <br/>
        <table class='col-12 line_sep' >
            <tr>
                <td rowspan="2" style="width: 10%;font-size: 14px;" >
                    Daily Assignment<br/>
                    Meeting
                </td>
                <td class='border' colspan='2'  style="height: 35px" >
                    <span class="table_text_items" style="font-size: 20px">Supervisor: <?=strtoupper($post['fname']);?></span>
                </td>
                <td class='border'>
                    <span class="table_text_items"  style="font-size: 20px">Date: <?=date('m/d/Y');?></span>
                </td>
            </tr>

            <tr>
                <td class="border">
                    <span class="table_text_items"  style="font-size: 20px">Crew #: <?=$crewData["CREW_ID"]?> </span>
                </td>
                <td class='border'>
                    <!--DS-187 -->
                    <span class="table_text_items"  style="font-size: 20px">Leader's Name:<br/>  <?=strtoupper($crewData["LEAD_NAME"])?> (<?=$crewData['LEAD_ID']?>)</span>
                </td>
                <td class="border">
                    <span class="table_text_items"  style="font-size: 20px">Ldr. Initial:<br/> <input style='width: 100px;' type='text' /></span>
                </td>
            </tr>

        </table>

        <table class="col-12 report-section line_sep">

            <tr>
                <td style="padding-top: 10px;"></td>
                <td style="padding-top: 10px;" class='bold center' colspan='2'>
                    <p>
                        <!-- DS-188 -->
                        <strong><span style="font-size: 20px">Mandatory Topics for Supervisor / Leader </span></strong> <Br/>
                        <span style="font-size: 14px;font-weight: 500"><i>(Briefing supervisor shall mark "Y" or "N/A" for each category)</i></></span>
                    </p>
                </td>
            </tr>
            <tr>
                <td class='large-font line_sep' style="width: 10%;font-size: 14px;">
                    Topics  <Br/> Discussed
                </td>
                <td colspan='3' style='padding: 0px;'>
                    <table class='col-12'>

                        <tr>
                            <td class='border' style="font-size: 18px">
                                Proper Tools and Equipment:
                            </td>
                            <td class='border' style='width: 50px;'>
                                <input type='text' />
                            </td>
                            <td class='border' style="font-size: 18px">
                                Assignment Details:
                            </td>
                            <td class='border' style='width: 50px;'>
                                <input type='text' />
                            </td>
                            <td class='border' style="font-size: 18px">
                                Special Requirements for the job:
                            </td>
                            <td class='border' style='width: 50px;'>
                                <input type='text' />
                            </td>
                        </tr>
                        <tr>
                            <td class='border' style="font-size: 18px">
                                Weather Conditions:
                            </td>
                            <td class='border' style='width: 50px;'>
                                <input type='text' />
                            </td>
                            <td class='border' style="font-size: 18px">
                                PPE:
                            </td>
                            <td class='border' style='width: 50px;'>
                                <input type='text' />
                            </td>
                            <td class='border' style="font-size: 18px">
                                Interaction with other groups:
                            </td>
                            <td class='border' style='width: 50px;'>
                                <input type='text' />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

        </table>


        <table class="col-12 report-section " >
            <tr>
                <td style="padding-top: 10px;width: 10%"></td>
                <td style="padding-top: 10px;" class='bold center' colspan='2'>
                    <div class="">
                        <strong><span style="font-size: 20px">Site Specific Pre-Job Briefing </span</strong> <Br/>
                        <!--DS-189-->
                        <span style="font-size: 14px;font-weight: 500"><i>(Location shall be identified with an address or nearest crossroad)</i></span>
                    </div>
                </td>
            </tr>
        </table>
        <table class="col-12" >
            <tr>
                <td class="large-font" style="width: 10%;font-size: 14px;">

                    <!--DS-189-->
                    Crew Leader <br/> shall return <Br/>
                    completed form
                    with timesheet at <Br/> end of shift.

                </td>
                <td colspan='3'>
                    <!--DS-190 -->
                    <table class='col-12' style="border-collapse: collapse;width:99%">
                        <tr>

                            <td class='border small_cell' style='width: 100px;height:35px;'>Location 1</td>
                            <td class='border small_cell'>
                                <input type='text' />
                            </td>
                            <td class='border small_cell' style="width: 20px;"><span class="td_span_location">City/Twp.</span></td>
                            <td class='border small_cell' style='width: 200px;'>
                                <input type='text' />
                            </td>
                        </tr>

                        <tr>
                            <td class='border small_cell' style='width: 100px;height:35px;'>Location 2</td>
                            <td class='border small_cell'>
                                <input type='text' />
                            </td>
                            <td class='border small_cell' style='width: 100px;'>City/Twp.</td>
                            <td class='border small_cell' style='width: 200px;'>
                                <input type='text' />
                            </td>
                        </tr>
                        <tr>
                            <td class='border small_cell' style='width: 100px;height:35px;'>Location 3</td>
                            <td class='border small_cell'>
                                <input type='text' />
                            </td>
                            <td class='border small_cell' style='width: 100px'>City/Twp.</td>
                            <td class='border small_cell' style='width: 200px;'>
                                <input type='text' />
                            </td>
                        </tr>
                        <tr>
                            <td class='border small_cell' style='width: 100px;height:35px;'>Location 4</td>
                            <td class='border small_cell'>
                                <input type='text' />
                            </td>
                            <td class='border small_cell' style='width: 100px'>City/Twp.</td>
                            <td class='border small_cell' style='width: 200px;'>
                                <input type='text' />
                            </td>
                        </tr>
                        <tr>
                            <td class='border small_cell' style='width: 100px;height:35px;'>Location 5</td>
                            <td class='border small_cell'>
                                <input type='text' />
                            </td>
                            <td class='border small_cell' style='width: 100px'>City/Twp.</td>
                            <td class='border small_cell' style='width: 200px;'>
                                <input type='text' />
                            </td>
                        </tr>
                        <tr>
                            <td class='border small_cell' style='width: 100px;height:35px;'>Location 6</td>
                            <td class='border small_cell'>
                                <input type='text' />
                            </td>
                            <td class='border small_cell' style='width: 100px'>City/Twp.</td>
                            <td class='border small_cell' style='width: 200px;'>
                                <input type='text' />
                            </td>
                        </tr>
                        <tr>
                            <td class='border small_cell' style='width: 100px;height:35px;'>Location 7</td>
                            <td class='border small_cell'>
                                <input type='text' />
                            </td>
                            <td class='border small_cell' style='width: 100px'>City/Twp.</td>
                            <td class='border small_cell' style='width: 200px;'>
                                <input type='text' />
                            </td>
                        </tr>

                    </table>
                </td>
            </tr>
        </table>
        <table class="col-12 report-section">
            <tr>
                <td style="padding-top: 10px;" class='bold center' colspan='3'>
                    <div >
                        <strong><span style="font-size: 20px">Hazard Recognition </span></strong>
                        <span style="font-size: 18px;font-weight: 500"><i>(Leader shall mark "Y" or "N/A" for each Location)</i></span>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan='4' style='padding: 0px;'>
                    <!--DS-191 -->
                    <table class='col-12'>

                        <tr>
                            <td style='width: 540px;border-left: 1px solid #fff; '></td>
                            <td class='border ' style="border-left: 1px solid #000;">
                                <B><center>Loc. 1</center></b>
                            </td>
                            <td class='border '>
                                <B><center>Loc. 2</center></b>
                            </td>
                            <td class='border '>
                                <B><center>Loc. 3</center></b>
                            </td>
                            <td class='border '>
                                <B><center>Loc. 4</center></b>
                            </td>
                            <td class='border '>
                                <B><center>Loc. 5</center></b>
                            </td>
                            <td class='border '>
                                <B><center>Loc. 6</center></b>
                            </td>
                            <td class='border'>
                                <B><center>Loc. 7</center></b>
                            </td>

                        </tr>

                        <tr>
                            <td class='border' style='width: 540px;'>
                                <div class="haz_reco_txt">Analyze all hazards associated with the job:<span class="haz_reco_txt_span"> (i.e. Electrical, Traffic, Animals ,Terrain. etc ...)</span><div>
                            </td>
                            <td class='border small_cell'><input type='text' /></td>
                            <td class='border small_cell'><input type='text' /></td>
                            <td class='border small_cell'><input type='text' /></td>
                            <td class='border small_cell'><input type='text' /></td>
                            <td class='border small_cell'><input type='text' /></td>
                            <td class='border small_cell'><input type='text' /></td>
                            <td class='border small_cell'><input type='text' /></td>
                        </tr>
                        <tr>
                            <td class='border' style='width: 540px;'>
                                <div class="haz_reco_txt">Work Procedures Reviewed:<span  class="haz_reco_txt_span"> (Reference Materials)</span><div>
                            </td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                        </tr>

                        <tr>
                            <td class='border' style='width: 540px;'>
                                <div class="haz_reco_txt">Energy Source Controls:<span  class="haz_reco_txt_span"> (i.e. Red tag, Grounding, Protective Devices, Reclosing, etc ...)</span><div>
                            </td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                        </tr>

                        <tr>
                            <td class='border' style='width: 540px;'>
                                <div class="haz_reco_txt">Review all PPE requirements: <div>
                            </td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                        </tr>

                        <tr>
                            <td class='border' style='width: 540px;'>
                                <div class="haz_reco_txt">Special Precautions: <span  class="haz_reco_txt_span"> (traffic control, New Equipment,Site Conditions ,General Housekeeping, etc ...)</span><div>
                            </td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                        </tr>

                    </table>
                </td>
            </tr>
            <tr>

                <td class='bold center' colspan='3'>
                    <div class="report-section" style="padding: 10px 0px 0px 0px">
                        <strong><span style="font-size: 20px">Electrical Hazard Recognition </span></strong>
                        <span style="font-size: 18px;font-weight: 500"><i>(Leader shall write actual voltage, distances, and  "Y" or "N/A" for each Location)</i></span>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan='4' style='padding: 0px;'>
                    <!--DS-192 -->
                    <table class='col-12'>


                        <tr>
                            <td class='border' style='width: 540px;'>
                                <div class="ehaz_reco_txt">Nominal Voltage= <div>
                            </td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                        </tr>
                        <tr>
                            <td class='border' style='width: 540px;'>
                                <div class="ehaz_reco_txt">Number of Sources= (1 or 2): <div>
                            </td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                        </tr>

                        <tr>
                            <td class='border' style='width: 540px;'>
                                <div class="ehaz_reco_txt">Energized Parts Identified (Y or N/A): <div>
                            </td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                        </tr>

                        <tr>
                            <td class='border' style='width: 540px;'>
                                <div class="ehaz_reco_txt">Limited Approach Boundary= <div>
                            </td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                        </tr>

                        <tr>
                            <td class='border' style='width: 540px;'>
                                <div class="ehaz_reco_txt">Minimum Approach Distance= <div>
                            </td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                        </tr>
                        <tr>
                            <td class='border' style='width: 540px;'>
                                <div class="ehaz_reco_txt">Arc Flash Protection Boundary= <div>
                            </td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                        </tr>

                        <tr>
                            <td class='border' style='width: 540px;'>
                                <div class="ehaz_reco_txt">Special Methods (Electrical Protective Equipment)<div>
                            </td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                            <td class='border'><input type='text' /></td>
                        </tr>

                    </table>
                </td>
            </tr>

            <tr>
        </table>
        <table class="col-12">
            <tr>
                <td class='bold center' colspan='3'>
                    <div style="padding: 10px 0px 0px 0px">

                        <strong><span style="font-size: 20px">Crew Members </span></strong>
                        <span style="font-size: 18px;font-weight: 500"><i>(Initial at each location)</i></span>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan='4' style='padding: 0px;'>
                    <table class='col-12'>

                        <?php for($employee = 0; $employee < $locationCount; $employee++): ?>
                            <tr>

                                <td class='border' style='width: 540px;'>
                                    <div class="crew_loc_txt">Name:

                                        <?php
                                        if($crewData['EMPLOYEE_IDS'][$employee]) {
                                            echo "{$crewData['EMPLOYEE_NAMES'][$employee]} ({$crewData['EMPLOYEE_IDS'][$employee]})";
                                        }
                                        ?>
                                        <div>
                                </td>

                                <td class='border'><input type='text' /></td>
                                <td class='border'><input type='text' /></td>
                                <td class='border'><input type='text' /></td>
                                <td class='border'><input type='text' /></td>
                                <td class='border'><input type='text' /></td>
                                <td class='border'><input type='text' /></td>
                                <td class='border'><input type='text' /></td>


                            </tr>
                        <?php endfor; ?>


                    </table>
                </td>
            </tr>

            <tr>

                <td style="padding-top: 15px;" class='bold center' colspan='3'>
                    <p>
                        <!--DS-193 -->
                        <strong> <span style="font-size: 20px">Comments </span></strong>
                        <span style="font-size: 18px;font-weight: 500"><i>(discussions with other groups)</i></span>
                    </p>
                </td>
            </tr>
            <tr>
                <td colspan="6" class="border" style="width: 100%;height: 100px"></td>
            </tr>


        </table>
    </div>

    <div class="container" style='page-break-after: always;'>
        <table class="col-12">
            <tr>
                <td colspan="2">
                    <!-- DS-173  -->
                    <img  src="QEW_Diagram_lifeCriticalStandards_v2.png" style="width: 1000px;text-align: center"/>
                </td>
            </tr>
        </table>
        <!-- DS - 174,175 -->
        <table style="margin-bottom: 5px;" class="col-12 line_sep">
            <tbody>
            <tr>
                <td colspan="3" class="border">
                    <p align="center" style="font-size: 32px">
                        <strong>SHOCK PROTECTION</strong>
                    </p>
                </td>
            </tr>
            <tr>
                <td  class="border">
                    <p align="center" style="vertical-align: top;font-size: 18px">
                        <strong>Nominal Voltage</strong>
                    </p>
                </td>
                <td class="border">
                    <p align="center" style="vertical-align: top">
                        <strong>Limited </strong>
                        <strong>Approach </strong>
                        <strong>Boundary</strong>
                        <br/>
                        <strong>Q</strong>ualified <strong>E</strong>lectrical <br/> <strong>W</strong>orker <u>ONLY</u> or
                        <br/>
                        Non-Qualified Escorted by a QEW
                    </p>
                </td>
                <td class="border" style="background-color: #a1a1a4;">
                    <p align="center" style="vertical-align: top;background-color: #a1a1a4;">
                        <strong>Minimum Approach Distance (MAD)</strong>
                        <br/>
                        <strong>Q</strong>ualified <strong>E</strong>lectrical <strong>W</strong>orker <u>ONLY</u>
                    </p>
                </td>
            </tr>
            <tr>
                <td class="border">
                    <p align="center">
                        &ge;50 to 600 V
                    </p>
                </td>
                <td class="border">
                    <p align="center">
                        5'
                    </p>
                </td>
                <td class="border">
                    <p align="center">
                        1' 1"
                    </p>
                </td>
            </tr>
            <tr>
                <td class="border">
                    <p align="center">
                        >600 to 13.2 kV
                    </p>
                </td>
                <td class="border">
                    <p align="center">
                        10'
                    </p>
                </td>
                <td class="border">
                    <p align="center">
                        2' 3"
                    </p>
                </td>
            </tr>
            <tr>
                <td class="border">
                    <p align="center">
                        >24 to 40 kV
                    </p>
                </td>
                <td class="border">
                    <p align="center">
                        10'
                    </p>
                </td>
                <td class="border">
                    <p align="center">
                        3' 6"
                    </p>
                </td>
            </tr>
            <tr>
                <td class="border">
                    <p align="center">
                        >120 kV
                    </p>
                </td>
                <td class="border">
                    <p align="center">
                        20'
                    </p>
                </td>
                <td class="border">
                    <p align="center">
                        4' 8"
                    </p>
                </td>
            </tr>
            <tr>
                <td class="border">
                    <p align="center">
                        >230 kV
                    </p>
                </td>
                <td class="border">
                    <p align="center">
                        20'
                    </p>
                </td>
                <td class="border">
                    <p align="center">
                        10' 1"
                    </p>
                </td>
            </tr>
            <tr>
                <td class="border">
                    <p align="center">
                        >345 kV
                    </p>
                </td>
                <td class="border">
                    <p align="center">
                        20'
                    </p>
                </td>
                <td  class="border">
                    <p align="center">
                        18' 1"
                    </p>
                </td>
            </tr>
        </table>
        <!-- DS - 176,177 -->
        <table class="col-12" style="margin-top: -35px;">
            <tr>
                <td colspan="3" style="padding: 5px;">&nbsp;</td>
                </td>
            </tr>
            <tr>
                <td colspan="5" class="border">
                    <p align="center" style="font-size: 32px">
                        <strong>ARC FLASH PROTECTION</strong>
                    </p>
                </td>
            </tr>
            <tr>
                <td colspan=2 class="border" style="text-align: center" > <b>Arc in Open Air <br/>Exposed Energized Conductor</b></td>
                <!-- DS-179,DS-183 -->
                <td colspan=3  class="border"  style="text-align: center" ><b>Arc in A Box  <br/> When performing work on <br/> Underground Vaults , Live Front, and Dead Front Pad-Mounted Equipment:</b></td>
            </tr>
            <tr>
                <td colspan='2'  class="border" style="text-align: center"  style='padding: 0px;'>
                    <b>Walk Down PPE </br>Must be worn</b>
                </td>
                <td  class="border" style="text-align: center" >
                    <b>Nominal System Voltage</b>
                </td>
                <td  class="border" style="text-align: center" >
                    <b>Arc Flash Protection </br> Boundary</b>
                </td>
                <td  class="border" style="text-align: center" >
                    <b>Level of PPE</b>
                </td>

            </tr>
            <tr>
                <td  class="border" style="text-align: center" >
                    <b>Nominal System Voltage</b>
                </td>
                <td  class="border" style="text-align: center" >
                    <b>Arc Flash Protection Boundary</b>
                </td>
                <td   class="border" style="text-align: left" >
                    Pontiac Vault System <br/> &le; 480 V
                </td>
                <td   class="border" style="text-align: center" > 3'6" -  1'2" </td>
                <td   class="border" style="text-align: center" >Level 4 PPE Must be worn</td>
            </tr>
            <tr>
                <td   class="border" style="text-align: left"> &ge;50 V  to 240 V</td>
                <td   class="border" style="text-align: center">1'1"</td>
                <td   class="border" style="text-align: left" rowspan=3 >Pontiac 8.3 kV  Vault System</td>
                <td   class="border" style="text-align: center"  rowspan=3>3'4" -  7" </td>
                <td   class="border" style="text-align: center"  rowspan=3>Level 4 PPE Must be worn</td>

            </tr>
            <tr>
                <td   class="border" style="text-align: left">  480 V</td>
                <td   class="border" style="text-align: center">2'2"</td>
            </tr>
            <tr>
                <td   class="border" style="text-align: left"> 4.8 kV, one source</td>
                <td   class="border" style="text-align: center">4'9"</td>
            </tr>

            <tr>
                <td   class="border" style="text-align: left"> 4.8 kV, two sources</td>
                <td   class="border" style="text-align: center">8'</td>
                <td   class="border" style="text-align: left">4.8 to 13.2 kV  Live Front</td>
                <td   class="border" style="text-align: center" >10' -  1'2" </td>
                <td   class="border" style="text-align: center">Level 4 PPE Must be worn</td>

            </tr>

            <tr>
                <td   class="border" style="text-align: left"> 8.3 kV, one source</td>
                <td   class="border" style="text-align: center">5'6"</td>
                <td   class="border" style="text-align: left">24kV to 40 kV</td>
                <td   class="border" style="text-align: center" >10' -  4'  </td>
                <td   class="border" style="text-align: center">Level 4 PPE Must be worn</td>

            </tr>
            <tr>
                <td   class="border" style="text-align: left"> 8.3 kV, two sources</td>
                <td   class="border" style="text-align: center">5'6"</td>
                <td   class="border" style="text-align: left">4.8 to 13.2 kV  Dead Front</td>
                <td   class="border" style="text-align: center" >4' -2' </td>
                <td   class="border" style="text-align: center">Level 2 PPE Must be worn</td>

            </tr>
            <tr>
                <td   class="border" style="text-align: left"> 13.2 kV, one source</td>
                <td   class="border" style="text-align: center">6'</td>
                <td   class="border" style="text-align: left">4.8 to 13.2 kV  Dead Front</td>
                <td   class="border" style="text-align: center" > &ge;4' </td>
                <td   class="border" style="text-align: center">WD PPE Must be worn</td>

            </tr>
            <tr>
                <td   class="border" style="text-align: left"> 13.2 kV, two sources</td>
                <td   class="border" style="text-align: center">8'</td>
                <!-- DS-182 -->
                <td   class="" style="text-align: center;border-right: 1px solid #000" rowspan=2 span colspan="3"><b>Arc in a Box <br/> OH/UG Secondary Meter Enclosures And Pad Mounts:</b></td>
            </tr>
            <tr>
                <td   class="border" style="text-align: left"> 24-40 kV, one source</td>
                <td   class="border" style="text-align: center"> 6'2"</td>
            </tr>

            <tr>
                <td   class="border" style="text-align: left"> 24-40 kV, two sources</td>
                <td   class="border" style="text-align: center">7'9"</td>
                <td   class="border" style="text-align: left"> &ge;50 V to 240 V</td>
                <td   class="border" style="text-align: center" >1'1" </td>
                <td   class="border" style="text-align: center">Walk Down PPE Must be worn</td>

            </tr>

            <tr>
                <td   class="border" style="text-align: left">120 kV </td>
                <td   class="border" style="text-align: center">4'4"</td>
                <td   class="border" style="text-align: left">  480 V</td>
                <td   class="border" style="text-align: center" >4'9" </td>
                <td   class="border" style="text-align: center">Level 2 PPE Must be worn</td>

            </tr>
            <tr>
                <td   class="border" style="text-align: left"> 230 kV </td>
                <td   class="border" style="text-align: center">27'</td>
                <td   class="border" style="text-align: left"> 480 V</td>
                <td   class="border" style="text-align: center" >3'6" - 1'2" </td>
                <td   class="border" style="text-align: center">Level 4 PPE Must be worn</td>

            </tr>
            <tr>
                <td   class="border" style="text-align: left"> 345 kV</td>
                <td   class="border" style="text-align: center">27'</td>
                <td   class="border" style="text-align: left"> 480 V -  &ge;1500 kVA </td>
                <td   class="border" style="text-align: center" >5'5" - 1'10"</td>
                <td   class="border" style="text-align: center">Level 4 PPE Must be worn</td>

            </tr>
            <tr>
                <td    style="text-align: center"></td>
                <td    style="text-align: center"></td>
                <td   class="border" style="text-align: left"> &gt;600 V to 1000 V </td>
                <td   class="border" style="text-align: center"> Refer to J.U. 196</td>
                <td   class="border" style="text-align: center">Level 4 PPE Must be worn</td>

            </tr>
            <tr>
                <td    style="text-align: center"></td>
                <td    style="text-align: center"></td>
                <td   style="text-align: center" colspan="3"> </td>


            </tr>
            </tbody>
        </table>
        <div id="footer-section" >
            <table>
                <tr>
                    <td>
                        <b>Walk Down PPE (WD)</b>
                    </td>
                    <td>
                        <b>Level 2 PPE</b>
                    </td>
                    <td>
                        <b>Level 4 PPE</b>
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align:top">


                        <ul class="footer-list" style="margin-top: 0px">
                            <li>Class E Hard Hat</li>
                            <li>Safety Glasses</li>
                            <li>100% Cotton/Arc Rated Tee Shirt</li>
                            <li>&ge; 8 cal/cm<sup>2</sup> Long Sleeve Shirt</li>
                            <li>&ge; 8 cal/cm<sup>2</sup>  Pants</li>
                        </ul>
                        </br><br/>

                    </td>
                    <td style="vertical-align:top">


                        <ul class="footer-list" style="margin-top: 0px">
                            <li>Class E Hard Hat</li>
                            <li>Safety Glasses</li>
                            <li>100% Cotton/Arc Rated Tee Shirt</li>
                            <li>&ge; 8 cal/cm<sup>2</sup> Long Sleeve Shirt</li>
                            <li>&ge; 8 cal/cm<sup>2</sup> Pants</li>
                            <li>&ge; 8 cal/cm<sup>2</sup> Face Shield</li>
                            <li>&ge; 8 cal/cm<sup>2</sup> Balaclava</li>
                            <li>Electric  Protective gloves and sleeves as required</li>
                            <li>Hearing Protection (Ear Canal Inserts)</li>
                        </ul>

                    </td>
                    <td style="vertical-align:top">


                        <ul class="footer-list" style="margin-top: 0px">
                            <li>Class E Hard Hat<span></li>
                            <li>Safety Glasses</li>
                            <li>100% Cotton/Arc Rated Tee Shirt</li>
                            <li>&ge; 8 cal/cm<sup>2</sup> Long Sleeve Shirt</li>
                            <li>&ge; 8 cal/cm<sup>2</sup>  Pants</li>
                            <li>&ge; 40 cal/cm<sup>2</sup> Balaclava and Goggles or Hood</li>
                            <li>&ge; 40 cal/cm<sup>2</sup> Jacket/Bibs OR</li>
                            <li>&ge; 40 cal/cm<sup>2</sup> Coveralls</li>
                            <li>Class 2 HV Gloves and sleeves</li>
                            <li>Hearing Protection (Ear Canal Inserts)</li>
                        </ul>

                    </td>
                </tr>

            </table>

        </div>

    </div>

    <?php
endforeach;
ob_end_flush();
?>
</body>
</html>