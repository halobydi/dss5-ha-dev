<?php
	function errorHandler($errno, $errstr, $errfile, $errline, $errcontext){
		
	}
	set_error_handler('errorHandler');
	header("Content-type: text/html; charset=iso-8859-1"); 
	$baseDir = 'http' . ($_SERVER['SERVER_PORT'] == '443' ? 's' : '') . '://'. $_SERVER['SERVER_NAME'] . ":" . $_SERVER['SERVER_PORT'];
	
	@require_once("mcl_Html.php");
	@require_once("mcl_Header.php");
	@require_once("mcl_Oci.php");
	@require_once("mcl_Ldap.php");

	$colors = array(
		"border-top" 		=> "#b9c1c6",
		"border-bottom" 	=> "#6598cb",
		"color" 			=> "#d6d3d6",
		"background-color" 	=> "#292829",
		"splitter" 			=> "#848284"
	);
	
	mcl_Html::title('SOTeria - Safety Observation Tool');
	mcl_Header::colors($colors);

	mcl_Html::html5(true);
	mcl_Html::no_cache(true);
	mcl_Html::js(mcl_Html::DOJO);
	mcl_Html::js(mcl_Html::DOJO_WINDOW);
	mcl_Html::js(mcl_Html::AJAX);
	
	mcl_Html::s(mcl_Html::INC_CSS, "{$baseDir}/src/css/jscal2.css");
	mcl_Html::js(mcl_Html::CALENDAR);
	
	require_once('/opt/apache/servers/soteria/htdocs/src/php/auth.php');
	
	$colors = array(
		"border-top" 		=> "#b9c1c6",
		"border-bottom" 	=> "#000",
		"color" 			=> "#d6d3d6",
		"background-color" 	=> "#292829",
		"splitter" 			=> "#848284"
	);

	mcl_Html::s(mcl_Html::INC_CSS, "{$baseDir}/src/css/general.css");
	mcl_Html::s(mcl_Html::INC_CSS, "{$baseDir}/src/css/menu.css");
	mcl_Html::s(mcl_Html::INC_CSS, "{$baseDir}/src/css/table.css");
	mcl_Html::s(mcl_Html::INC_CSS, "{$baseDir}/src/css/login.css");
	
	mcl_Html::s(mcl_Html::INC_JS, "{$baseDir}/src/sot.js");
	
	mcl_Header::version('2.4.2');
	mcl_Html::s(mcl_Html::SRC_CSS, "
		ul, li {
			margin: 0px;
			list-style-type: none;
		}
	");
	
	mcl_Html::s(mcl_Html::SRC_JS, <<<JS
	function allChecked(){
		var print = document.forms["print"];
		var x = document.forms["print"]["count"].value - 1;

		var count = 0;
		for (var i = 0; i < print.length; i++) {  
			if (print[i].type == "checkbox" && print[i].checked && print[i].id != 'checkAll') {  
				count++;
			}  
		} 
	
		var checkbox = document.getElementById("checkAll");
		if (count == x) {
			checkbox.checked = true;
		} else if(count != x) {
			checkbox.checked = false;	
		}
		
		return true;
	}

	function makeCheck(checkbox){
		var print = document.forms["print"];
		var checkAll = checkbox.checked;
		
		if(checkAll){
			for (var i = 0; i < print.length; i++) {  
				if (print[i].type == "checkbox") {  
					print[i].checked = true;
				}  
			} 
			
		} else {
			for (var i = 0; i < print.length; i++) {  
				if (print[i].type == "checkbox") {  
					print[i].checked = false;
				}  
			} 
		}
		
		return true;
	}
	
	function form(name){
		return document.forms["filter"][name];
	}

	function check_checkbox(region, toggle) {
		if (toggle) {
			if (region == 'nw') {
				form('sc_nw_pon').checked = form('region_nw').checked;
				form('sc_nw_hwl').checked = form('region_nw').checked;
				form('sc_nw_sby').checked = form('region_nw').checked;
			} else if (region == 'ne') {
				form('sc_ne_mtc').checked = form('region_ne').checked;
				form('sc_ne_mry').checked = form('region_ne').checked;
				form('sc_ne_lap').checked = form('region_ne').checked;
				form('sc_ne_naec').checked = form('region_ne').checked;
			} else if (region == 'sw') {
				form('sc_sw_ann').checked = form('region_sw').checked;
				form('sc_sw_wws').checked = form('region_sw').checked;
				form('sc_sw_npt').checked = form('region_sw').checked;
			} else if (region == 'se') {
				form('sc_se_rfd').checked = form('region_se').checked;
				form('sc_se_can').checked = form('region_se').checked;
				form('sc_se_trm').checked = form('region_se').checked;
			}
		} else {
			if (region == 'nw') {
				form('region_nw').checked = (form('sc_nw_pon').checked &&
				form('sc_nw_hwl').checked &&
				form('sc_nw_sby').checked);
			} else if (region == 'ne') {
				form('region_ne').checked = (form('sc_ne_mtc').checked &&
				form('sc_ne_mry').checked &&
				form('sc_ne_lap').checked &&
				form('sc_ne_naec').checked);
			} else if (region == 'sw') {
				form('region_sw').checked = (form('sc_sw_ann').checked &&
				form('sc_sw_wws').checked &&
				form('sc_sw_npt').checked);
			} else if (region == 'se') {
				form('region_se').checked = (form('sc_se_rfd').checked &&
				form('sc_se_can').checked &&
				form('sc_se_trm').checked);
			}
		}
	
	}
		
	
	function subm(f, newAction){
		f.action = newAction ;
		f.submit();
	}
JS
);
	$user = auth::check();
	
	$file = $baseDir . "/so/index.php";
	if($user["status"] == "authorized"){
		mcl_Header::auth($user["name"]);
		mcl_Header::build('SOTeria - Service Operations');

		//Clean POST
		if (isset($_POST['username'])) {
			header("Location: " . $file);
		}
		
		//logout
		mcl_Html::s(mcl_Html::SRC_JS, "
			dojo.ready(function(){
				var logoutDiv = dojo.byId('header_login_auth');
				for(var i in logoutDiv.childNodes){
					if(typeof logoutDiv.childNodes[i] == 'object'){
						if(logoutDiv.childNodes[i].href){
							logoutDiv.childNodes[i].onclick = function(){
								window.location = '{$file}?logout=true';
							}
						}
					}
				}
			});
		");
	
	} else {
		mcl_Header::hide_auth(true);
		mcl_Header::build('SOTeria - Service Operations');
		
		$err = $user["status"];
		mcl_Html::s(mcl_Html::SRC_JS, "
			dojo.ready(function(){
				sot.tools.login.showLogin('{$err}');
			});
		");
		
		//Cleanup Logout
		if (isset($_GET["logout"]) && $_GET["logout"] == "true") {
			header("Location: " . $file);
			die();
		}
		
		die();
	}
	
?>
<!--<div style="padding: 5px; border: 1px solid #000; width: 200px; background-color: #dfe2e3; font-size: 11px; margin: 5px; display: none;">-->
<div style="">
	<form name='filter' style=''>
	<table style='border: 1px solid black; margin-top: 5px; margin-left: 5px; border-collapse: collapse;'>
		<tr>
			<td style='padding: 0px;'>
				<div style='background-color: #d5d9db; border-left:	1px solid #dedede; border-top: 1px solid #dedede;'>
					<table style='border-collapse: collapse;'>
						<tr style=''>
							<td style='vertical-align: top;'>
								<input type="checkbox" value="NW" name="region_nw" <?= (isset($_GET['region_nw']) ? "checked='checked'" : ""); ?> onclick="check_checkbox('nw', true);" /><b>Northwest</b><br/>
								<input type="checkbox" name="sc_nw_pon" <?= (isset($_GET['sc_nw_pon']) ? "checked='checked'" : ""); ?> onclick="check_checkbox('nw');" />Pontiac<br/>
								<input type="checkbox" name="sc_nw_hwl" <?= (isset($_GET['sc_nw_hwl']) ? "checked='checked'" : ""); ?> onclick="check_checkbox('nw');" />Howell<br/>
								<input type="checkbox" name="sc_nw_sby" <?= (isset($_GET['sc_nw_sby']) ? "checked='checked'" : ""); ?> onclick="check_checkbox('nw');" />Shelby<br/>
							</td>
							<td style='vertical-align: top;'>
								<input type="checkbox" value="NE" name="region_ne" <?= (isset($_GET['region_ne']) ? "checked='checked'" : ""); ?> onclick="check_checkbox('ne', true);" /><b>Northeast</b><br/>
								<input type="checkbox" name="sc_ne_mtc" <?= (isset($_GET['sc_ne_mtc']) ? "checked='checked'" : ""); ?> onclick="check_checkbox('ne');" />Mt. Clements<br/>
								<input type="checkbox" name="sc_ne_mry" <?= (isset($_GET['sc_ne_mry']) ? "checked='checked'" : ""); ?> onclick="check_checkbox('ne');" />Marysville<br/>
								<input type="checkbox" name="sc_ne_lap" <?= (isset($_GET['sc_ne_lap']) ? "checked='checked'" : ""); ?> onclick="check_checkbox('ne');" />Lapeer<br/>
								<input type="checkbox" name="sc_ne_naec" <?= (isset($_GET['sc_ne_naec']) ? "checked='checked'" : ""); ?> onclick="check_checkbox('ne');" />NAEC<br/>
							</td>
							<td style='vertical-align: top;'>
								<input type="checkbox" value="SW" name="region_sw" <?= (isset($_GET['region_sw']) ? "checked='checked'" : ""); ?> onclick="check_checkbox('sw', true);" /><b>Southwest</b></br>
								<input type="checkbox" name="sc_sw_wws" <?= (isset($_GET['sc_sw_wws']) ? "checked='checked'" : ""); ?> onclick="check_checkbox('sw');" />Western Wayne<br/>
								<input type="checkbox" name="sc_sw_ann" <?= (isset($_GET['sc_sw_ann']) ? "checked='checked'" : ""); ?> onclick="check_checkbox('sw');" />Ann Arbor<br/>
								<input type="checkbox" name="sc_sw_npt" <?= (isset($_GET['sc_sw_npt']) ? "checked='checked'" : ""); ?> onclick="check_checkbox('sw');" />Newport<br/>
							</td>
							<td style='vertical-align: top;'>
								<input type="checkbox" value="SE" name="region_se" <?= (isset($_GET['region_se']) ? "checked='checked'" : ""); ?> onclick="check_checkbox('se', true);" /><b>Southeast</b></br>
								<input type="checkbox" name="sc_se_rfd" <?= (isset($_GET['sc_se_rfd']) ? "checked='checked'" : ""); ?> onclick="check_checkbox('se');" />Redford</br>
								<input type="checkbox" name="sc_se_can" <?= (isset($_GET['sc_se_can']) ? "checked='checked'" : ""); ?> onclick="check_checkbox('se');" />Caniff</br>
								<input type="checkbox" name="sc_se_trm" <?= (isset($_GET['sc_se_trm']) ? "checked='checked'" : ""); ?> onclick="check_checkbox('se');" />Trombly</br>
								<select name="sppu">
									<option value="sp" <?=($_GET['sppu']=='sp'?'selected="selected"':'');?> >Splicing</option>
									<option value="pu" <?=($_GET['sppu']=='pu'?'selected="selected"':'');?> >Pulling</option>
									<option value="both" <?=($_GET['sppu']=='both'?'selected="selected"':'');?> >Both</option>
								</select>
							</td>
							<td style='vertical-align: top; padding-left: 10px;'>
								<table>
									<tr>
										<td>
											Date:</br>
											<input type='text' name='date' style='margin-right: 2px;' value = "<?php echo (empty($_GET["date"]) ? date("m/d/Y") : $_GET["date"]); ?>">
											<img src='../src/img/calendar.gif' alt='' id="fcal" onmouseover="setup_cal('fcal', 'date');" />
										</td>
									</tr>
									<tr>
										<td>
											Shift:</br>
											<select name="shift">
												<option value="07:30:00" <?=($_GET['shift']=='07:30:00'?'selected="selected"':'');?> >07:30-15:30</option>
												<option value="15:30:00" <?=($_GET['shift']=='15:30:00'?'selected="selected"':'');?> >15:30-23:30</option>
												<option value="23:30:00" <?=($_GET['shift']=='23:30:00'?'selected="selected"':'');?> >23:30-07:30</option>
												<option value="ALL" <?=($_GET['shift']=='ALL'?'selected="selected"':'');?> >All</option>
											</select>
										</td>
									</tr>
									<tr>
										<td>
											OH/UG:</br>
											<select name="ohug">
												<option value="oh" <?=($_GET['ohug']=='oh'?'selected="selected"':'');?> >OH</option>
												<option value="ug" <?=($_GET['ohug']=='ug'?'selected="selected"':'');?> >UG</option>
											</select>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</div>
				</td>
			</tr>
		</table>
		<input type="hidden" value="true" name="submit" />
		<input class = "submit" type="submit" value="Show Crew Roster" style="margin: 0 auto; cursor: pointer; margin-left: 5px; margin-top: 5px;" />
	</form>
</div>

<?php
	if(isset($_GET["submit"])){
	echo '
	<div style="margin-top: 5px;">
		<form action="" method="post" name = "print">	
	';
		$oci = new mcl_Oci("carma");
		
		if (isset($_GET['date']) && !empty($_GET['date'])){
			$date2 = $_GET['date'];
		
			$date2 = explode('/', $_GET['date'], 3);
			$date = $date2[2] . $date2[0] . $date2[1];

		} else{
			$date = "TO_CHAR(SYSDATE, 'YYYYMMDD')";
			$date2 = array(date('m'), date('d'), date('Y'));
		}
		
		$sc = array();
		if (isset($_GET['region_nw'])) {
			$array[] = 'PON';
			$array[] = 'HOW';
			$array[] = 'SBY';
		} else {
			if (isset($_GET['sc_nw_pon']))
				$array[] = 'PON';
			if (isset($_GET['sc_nw_hwl']))
				$array[] = 'HOW';
			if (isset($_GET['sc_nw_sby']))
				$array[] = 'SBY';
		}
		
		if (isset($_GET['region_ne'])) {
			$array[] = 'MTC';
			$array[] = 'MAR';
			$array[] = 'LAP';
			$array[] = 'NAEC';
		} else {
			if (isset($_GET['sc_ne_mtc']))
				$array[] = 'MTC';
			if (isset($_GET['sc_ne_mry']))
				$array[] = 'MAR';
			if (isset($_GET['sc_ne_lap']))
				$array[] = 'LAP';
			if (isset($_GET['sc_ne_naec']))
				$array[] = 'NAEC';
		}
		
		if (isset($_GET['region_sw'])) {
			$array[] = 'WW';
			$array[] = 'AA';
			$array[] = 'NPT';
		} else {
			if (isset($_GET['sc_sw_wws']))
				$array[] = 'WW';
			if (isset($_GET['sc_sw_ann']))
				$array[] = 'AA';
			if (isset($_GET['sc_sw_npt']))
				$array[] = 'NPT';
		}
		
		if (isset($_GET['region_se'])) {
			$array[] = 'RED';
			$array[] = 'CAN';
			$array[] = 'TRM';
		} else {
			if (isset($_GET['sc_se_rfd']))
				$array[] = 'RED';
			if (isset($_GET['sc_se_can']))
				$array[] = 'CAN';
			if (isset($_GET['sc_se_trm']))
				$array[] = 'TRM';
		}
		
		$where = "";
		if (isset($array) && !empty($array) && is_array($array)) {
			foreach($array as $value) {
				$where .= (empty($where) ? "" : "OR") . "
					C.CREW_SC_ID = '{$value}'
				";
			}
		}
		
		$sql = "
			SELECT
				DISTINCT
				CREW_SC_ID,
				CREW_ID,
				DCR_CREW_SIZE,
				DCR_NOTES,
				TO_CHAR(DCR_START_TIME, 'HH24:MI:SS') AS START_TIME,
				DCR_TROUBLE_CREW,
				EQP_ID,
				EQP_RADIO_SELECT_NO,
				OCL_COMMENTS,
				E2.EMP_ID AS LEAD_ID,
				E2.EMP_NAME AS LEAD_NAME,
				E2.EMP_CALL_OUT_PHONE,
				LTRIM(XMLAGG(XMLELEMENT(\"E.EMP_NAME\", E.EMP_NAME || '<br />')).EXTRACT('//text()'), '&lt;br /&gt;') AS EMP_NAME_1,
				LTRIM(XMLAGG(XMLELEMENT(\"E.EMP_ID\", E.EMP_ID || '<br />')).EXTRACT('//text()'), '&lt;br /&gt;') AS EMP_ID_1,
				OCLD.OCLD_CONTACT_NBR AS OC_NUM,
				OCLD.OCLD_COMMENTS AS OC_COMM,
				E3.EMP_NAME AS OC_NAME
			FROM
				DAILY_ROSTER DR
			JOIN
				DAILY_CREW_REQUIREMENTS DCR
				ON DCR.DCR_DR_ID = DR.DR_ID
			JOIN
				SERVICE_CENTER_GROUPS SCG
				ON DR.DR_SCG_ID = SCG.SCG_ID
			LEFT JOIN
				EMPLOYEE_CREW_ASSIGNMENTS ECA
				ON ECA.ECA_DCR_ID = DCR.DCR_ID
			LEFT JOIN
				EQUIPMENTS E
				ON E.EQP_ID = DCR.DCR_EQP_ID
			LEFT JOIN
				ON_CALL_LISTS OCL
				ON OCL.OCL_SCG_ID = DR_SCG_ID AND TO_CHAR(OCL.OCL_ON_CALL_DT, 'YYYYMMDD') = {$date}
			LEFT JOIN
				ON_CALL_LIST_DETAILS OCLD
				ON OCLD.OCLD_OCL_ID = OCL.OCL_ID
			LEFT JOIN
				EMPLOYEES E3
				ON E3.EMP_ID = OCLD.OCLD_EMP_ID
			LEFT JOIN
				CREWS C
				ON C.CREW_ID = DCR.DCR_CREW_ID
			LEFT JOIN
				EMPLOYEES E
				ON E.EMP_ID = ECA.ECA_EMP_ID
				AND E.EMP_ID != C.CREW_EMP_ID
			LEFT JOIN
				 EMPLOYEES E2
				 ON E2.EMP_ID = C.CREW_EMP_ID
			WHERE
				TO_CHAR(DR.DR_SCHED_DT, 'YYYYMMDD') = {$date}
				" . ($_GET['shift'] != 'ALL' ? "AND TO_CHAR(DCR_START_TIME, 'HH24:MI:SS') = '{$_GET['shift']}'" : "") . "
				AND (
				1=1
				" . ($_GET['ohug'] != 'both' ? ("AND SCG.SCG_DESCR LIKE '%" . ($_GET['ohug'] == 'oh' ? "Overhead" : "Underground") . "'") : "") . "
				" . (isset($_GET['sc_se_trm']) && $_GET['sppu'] != 'both' ? "OR SCG.SCG_DESCR = 'Trombly Underground " . ($_GET['sppu'] == 'sp' ? 'Splicing' : 'Pulling') . "'" : "OR SCG.SCG_DESCR LIKE 'Trombly Underground%'") . "
				)
		";
		
		if (!empty($where)){
			$sql .= "AND ({$where})";
		}
		
		$sql .= "
			GROUP BY
				CREW_SC_ID,
				CREW_ID,
				DCR_CREW_SIZE,
				DCR_NOTES,
				TO_CHAR(DCR_START_TIME, 'HH24:MI:SS'),
				DCR_TROUBLE_CREW,
				EQP_ID,
				EQP_RADIO_SELECT_NO,
				OCL_COMMENTS,
				E2.EMP_ID,
				E2.EMP_NAME,
				E2.EMP_CALL_OUT_PHONE,
				OCLD.OCLD_CONTACT_NBR,
				OCLD.OCLD_COMMENTS,
				E3.EMP_NAME
			ORDER BY
				C.CREW_SC_ID ASC,
				C.CREW_ID";
			
		$index = $oci->query($sql);
		
	}

	if (isset($oci) && isset($index)) {			
		echo "
		<div style='font-size: 11px; margin-left: 5px; margin-right: 5px; padding-bottom: 3px; padding-top: 3px;'>
			<input type='checkbox' id='checkAll' onclick='return makeCheck(this);' /> Check All to Print
			<br />
			<br />
			<img src='../../src/img/print.png' /> <a href='#' onclick = \"document.print.action = 'excelform.php'; document.print.submit();\">Print PJB for each crew</a>
			<br />
			<img src='../../src/img/print.png' /> <a href='#' onclick = \"document.print.action = 'excelform.php?individual=true'; document.print.submit();\"> Print individual PJB for each crew member</a>
			<br />
		</div>
	";
		
		$x = 0;
		$auto = 0;
		$last_sc = "";
		$count = 1;
		while($row = $oci->fetch($index)) {
		
			if(empty($row["CREW_SC_ID"])){
				continue;
			}
			
			if (empty($last_sc) || $last_sc != $row["CREW_SC_ID"]) {					
				if (!empty($last_sc))
					echo "</tbody></table>";
				
				$info =  strtoupper($_GET["ohug"]);
				if ($info == 'BOTH')
					$info = 'OH/UG';
				
				if ($row["CREW_SC_ID"] == "TRM") {
					$info = $_GET['sppu'] == 'sp' ? 'SPLICING' : ($_GET['sppu'] == 'pu' ? 'PULLING' : 'SPLICING/PULLING');
				}
				
				if($x > 0){
					echo "</table>";
				}
				echo "
					<table class = 'tbl' style='margin-top: 10px;'>
						<tr>
							<th colspan='14' style='text-align: left;'>
								<div class='inner_title'>
									<b>{$row["CREW_SC_ID"]} - " . $info . " @ {$date2[0]}/{$date2[1]}/{$date2[2]}</b> On Call List Details - [" . $row["OC_NAME"] . " " . $row["OC_NUM"] . " (" . $row["OC_COMM"] . ")]
								</div>
							</td>
						</tr>
						<tr>
							<th ><div class='inner' style='width: 35px;'>Print</div></th>
							<th ><div class='inner' style='width: 35px;'>SC</div></th>
							<th ><div class='inner' style='width: 35px;'>Crew</div></th>
							<th ><div class='inner' style='width: 70px;'>Crew Size</div></th>
							<th ><div class='inner' style='width: 80px;'>Start Time</div></th>
							<th ><div class='inner' style='width: 60px;'>Trouble</div></th>
							<th ><div class='inner' style='width: 80px;'>Equipment</div></th>
							<th ><div class='inner' style='width: 60px;'>Radio</div></th>
							<th ><div class='inner' style='width: 150px;'>Employees</div></th>
							<th ><div class='inner' style='width: 150px;'>Lead Name</div></th>
							<th ><div class='inner' style='width: 120px;'>Lead Phone</div></th>
							<th ><div class='inner' style=''>Notes</div></th>
						</tr>
				";
			}
			
			$dateFormat = $date2[0]."/".$date2[1]."/".$date2[2];
	
			if($last_crew != $row["CREW_ID"]){
				if ((++$x) % 2 == 0) {
					$class = "even";
				} else {
					$class = "odd";
				}
				echo "
					<tr class='{$class}'>
						<td style=''><input type=checkbox name='" . $count . "' value = '{$row[CREW_ID]}' onclick='allChecked()' /></td>
						<td style=''>{$row["CREW_SC_ID"]}</td>
						<td style=''><a href='epop.php?sc=" . $row["CREW_SC_ID"] . "&date=". $dateFormat . "&ohug=" . $info . "&time=" . $row["START_TIME"] . "&crew=" . $row["CREW_ID"] . "&leader=" . $row["LEAD_NAME"] . "&notes=" . $row["DCR_NOTES"] . "'>" . $row["CREW_ID"] . "</a>";
						
						echo" </td>
						<td style=''>{$row["DCR_CREW_SIZE"]}</td>
						<td style=''>{$row["START_TIME"]}</td>
						<td style=''>{$row["DCR_TROUBLE_CREW"]}</td>
						<td style=''>{$row["EQP_ID"]}</td>
						<td style=''>{$row["EQP_RADIO_SELECT_NO"]}</td>
						<td style='text-align: left;'>" . str_replace("&apos;", "'", str_replace("&lt;br /&gt;", "<br />", $row["EMP_NAME_1"])) . "</td>
						<td style='text-align: left;'>{$row["LEAD_NAME"]}</td>
						<td style='width: 120px;'>{$row["EMP_CALL_OUT_PHONE"]}</td>
						<td style='text-align: left; width: 250px;'>" . trim(str_replace(".", "", $row["DCR_NOTES"])) . "</td>
					</tr>
				";
				
				$count++;	
				$auto++;
				$bigPost[$auto] = $row;
			}
			
			$last_crew = $row["CREW_ID"];
			$last_sc = $row["CREW_SC_ID"];
		}
			echo "<input type = 'hidden' value = '{$count}' name = 'count'/>
		</table>";
	}
?>
</div style="clear: both;"></div>
<?php
	if (!empty($bigPost)):
?>
	<input type="hidden" name="data" value= " <? $safeString =  base64_encode(serialize($bigPost)); echo $safeString; ?>"  />
	<input type="hidden" name="fname" value= " <? echo $user["name"]; ?>"  />
</form> 
<?php
	endif;
?>


			

