<?php require_once("/opt/apache/servers/soteria/htdocs/src/php/require.php"); ?>
<div style='text-align: center; width: 500px; margin: 0 auto;'>
	<div style='margin-top: 5px;'>
	</div>
	<table class = 'tbl'>
		<th colspan='2'>
			<div class = 'inner'>Developed by the DQM Group</div>
		</th>
		<tr class='even'>
			<td style='text-align: left; width: 225px;'>Robert K. Carpenter</td>
			<td style='text-align: left; width: 225px;'>Manager</td>
		</tr>	
		<tr class='odd'>
			<td style='text-align: left; width: 225px;'>Ledian Dibra</td>
			<td style='text-align: left; width: 225px;'>Project Lead</td>
		</tr>
		<tr class='even' style='color: gray;'>
			<td style='text-align: left; width: 225px;'>Michael E. Crowe</td>
			<td style='text-align: left; width: 225px;'>Analyst - AD Staffing</td>
		</tr>
		<tr class='odd'>
			<td style='text-align: left; width: 225px;'>Mariam A. Mohamed</td>
			<td style='text-align: left; width: 225px;'>Student Developer</td>
		</tr>
		<tr class='even'>
			<td style='text-align: left; width: 225px;'>Melissa A. Klimushyn</td>
			<td style='text-align: left; width: 225px;'>Student Developer</td>
		</tr>
		<tr class='odd'  style='color: gray;'>
			<td style='text-align: left; width: 225px;'>James E. Zawacki</td>
			<td style='text-align: left; width: 225px;'>Student Developer (Mobile)</td>
		</tr>
	</table>
	<table class = 'tbl'>
		<th colspan='2'>
			<div class = 'inner'>IT & Corporate Safety</div>
		</th>
		<tr class='even'>
			<td style='text-align: left; width: 225px;'>Cindy K Pollen Allard</td>
			<td style='text-align: left; width: 225px;'>IT Senior Business Analyst </td>
		</tr>	
		<tr class='odd'>
			<td style='text-align: left; width: 225px;'>Donald K Zanotti</td>
			<td style='text-align: left; width: 225px;'>IT Senior Middle Tier Engineer</td>
		</tr>
		<tr class='even'>
			<td style='text-align: left; width: 225px;'>Michael A Reuter</td>
			<td style='text-align: left; width: 225px;'>IT Technical Analyst</td>
		</tr>
		<tr class='odd'>
			<td style='text-align: left; width: 225px;'>John W Olson</td>
			<td style='text-align: left; width: 225px;'>IT Senior Database Engineer</td>
		</tr><tr class='even'>
			<td style='text-align: left; width: 225px;'>Scott D Johnson</td>
			<td style='text-align: left; width: 225px;'>Supervisor - Safety & Industrial Hygiene</td>
		</tr>
	</table>
	<div style='font-weight: bold;'>PHP Production Enterprise Environment - January 2013</div>
		
	<table class = 'tbl'>
		<tr>
			<th colspan='2'>
				<div class = 'inner_title' style='text-align: left;'>Changelog</div>
			</th>
		</tr>
		<tr>
			<th>
				<div class = 'inner'>Version</div>
			</th>
			<th>
				<div class = 'inner'>Changes</div>
			</th>
		</tr>
		<tr class='odd'>
			<td style='width: 50px; vertical-align: top;'>2.4.17<br/>
			<span style='font-size: 10px;'>08/05/2017</span></td>
			<td style='text-align: left; width: 400px'>
				- Created new Life Critical form for Field
				<br /> - GalaxE SOW Work
			</td>
		</tr>
		<tr class='even'>
			<td style='width: 50px; vertical-align: top;'>2.4.16<br/>
			<span style='font-size: 10px;'>10/29/2015</span></td>
			<td style='text-align: left; width: 400px'>
				- Added Life Critical Pilot SOW
			</td>
		</tr>
		<tr class='odd'>
			<td style='width: 50px; vertical-align: top;'>2.4.15<br/>
			<span style='font-size: 10px;'>05/05/2015</span></td>
			<td style='text-align: left; width: 400px'>
				- Paired observations now count toward leader completions and quarterly observations
				<br /> - Added Priority to Near Miss form
				<br /> - Category is required on Near Miss & Good Catch
			</td>
		</tr>
		<tr class='even'>
			<td style='width: 50px; vertical-align: top;'>2.4.14<br/>
			<span style='font-size: 10px;'>04/24/2015</span></td>
			<td style='text-align: left; width: 400px'>
				- Enhancements to Leader Completions reports
				- Fixed excel export issue
			</td>
		</tr>
		<tr class='odd'>
			<td style='width: 50px; vertical-align: top;'>2.4.13<br/>
			<span style='font-size: 10px;'>2/15/2015</span></td>
			<td style='text-align: left; width: 400px'>
				- Added observation notifications to person observed, observer and observer's supervisor
			</td>
		</tr>
		<tr class='even'>
			<td style='width: 50px; vertical-align: top;'>2.4.12<br/>
			<span style='font-size: 10px;'>12/4/2014</span></td>
			<td style='text-align: left; width: 400px'>
				- Fixed bugs with weekly notification email & near miss
				<br/>- Updated DO Pre-Job Brief form
				<br/>- New report to display employees who have NOT had an observation performed on them
			</td>
		</tr>
		<tr class='odd'>
			<td style='width: 50px; vertical-align: top;'>2.4.11<br/>
			<span style='font-size: 10px;'>08/28/2014</span></td>
			<td style='text-align: left; width: 400px'>
				- Enhanced Safe Worker form into two different forms: Office & Field
				<br/>- Added qualitative questions to SWO, EPOP & QEW form
				<br/>- Fixed issue with Near Miss email notification showing wrong location
				<br/>- Storm Duty form bug fixes
				<br/>- Other bug fixes
			</td>
		</tr>
		<tr class='even'>
			<td style='width: 50px; vertical-align: top;'>2.4.10<br/><span style='font-size: 10px;'>07/08/2014</span></td>
			<td style='text-align: left; width: 400px'>
				- Storm Duty Form can be accessed by Corp Safety employees
				<br/>- Fixed issue with duplicate organizations in selection drop down
			</td>
		</tr>
		<tr class='odd'>
			<td style='width: 50px; vertical-align: top;'>2.4.9<br/><span style='font-size: 10px;'>07/03/2014</span></td>
			<td style='text-align: left; width: 400px'>
				- Added Storm Duty Form (limited user access)
				<br/>- Enhanced email reminder - added Leader Completion report with SWO %
				<br/>- Added VP/President & Other to Weekly Report Out
				<br/>- Weekly Report Out can be run for multiple weeks
				<br/>- Added Edit Role to Near Miss form
				<br/>- Added "Tools Used" to Good Catch form
				<br/>- Minor changes to AOE
			</td>
		</tr>
		<tr class='even'>
			<td style='width: 50px; vertical-align: top;'>2.4.8<br/><span style='font-size: 10px;'>01/30/2014</span></td>
			<td style='text-align: left; width: 400px'>
				- Added Fossgen to Red Tag Audits
				<br/>- Added requirements by month for Red Tag Audits
				<br/>- Modified formula for calculating weekly SWO Leader Completions to calculate on a weekly basis (accounting for exclusions) and using the average
				<br/>- Minor changes to Leader Completions excel export
			</td>
		</tr>
		<tr class='odd'>
			<td style='width: 50px; vertical-align: top;'>2.4.7<br/><span style='font-size: 10px;'>12/10/2013</span></td>
			<td style='text-align: left; width: 400px'>
				- Added Red Tag Audit Dashboard
				<br/>- Added 'Required Immediate Action' question to Near Miss
				<br/>- Fixed bug with QEW indicator when submitting SWO form on desktop
			</td>
		</tr>
		<tr class='even'>
			<td style='width: 50px; vertical-align: top;'>2.4.6<br/><span style='font-size: 10px;'>10/31/2013</span></td>
			<td style='text-align: left; width: 400px'>
				- Added Good Catch
			</td>
		</tr>
		<tr class='odd'>
			<td style='width: 50px; vertical-align: top;'>2.4.4<br/><span style='font-size: 10px;'>09/17/2013</span></td>
			<td style='text-align: left; width: 400px'>
				- Added Feedback feature
				<br/>- Added ability to submit an exclusion directly (under Settings or Help)
				<br/>- SOTERIA@dteenergy.com now Available!
				<br/>- Engineering and SOC added to Red Tag Audits
				<br/>- Totals added Red Tag Audits Org report
				<br/>- Added sort and filter to Admin pages
				<br/>- Fix bug in Near Miss Email - info missing
				<br/>- Added  Employee Director and SAPID to Leader Completions Excel Export
				<br/>- Added Employee Organization Hierarchy for each observations
				<br/>- Fixed session expire - observation credit not given issue
				<br/>- Begin and Complete Timestamp for observations
				<br/>- Added Export to Excel for Comments Report
			</td>
		</tr>
		<tr class='even'>
			<td style='width: 50px; vertical-align: top;'>2.4.0</td>
			<td style='text-align: left; width: 400px'>
				- Added Near Miss form
				<br/>- Added Anatomy of an Event form
				<br/>- Removed <b>My Observations</b> and <b>My Audits</b> reports
				<br/>- All form submissions displayed in one report: <b>My Activity</b>
				<br/>- Audits displayed in <b> My Activity</b> show all audits completed by your team
				<br/>- Added photo capability to all forms
				<br/>- Minor layout changes
			</td>
		</tr>
		<tr class='odd'>
			<td style='width: 50px; vertical-align: top;'>2.3</td>
			<td style='text-align: left; width: 400px'>
				- Fixed excel report timeout
				<br/>- Added Leader Completions YTD Report
				<br/>- Added anonymous filter to Search Employee Observation Report
				<br/>- Added excel export to Leader Completions (LC) report (includes % comp.)
				<br/>- Added Supervisor of Observer column to LC Report & Excel export
				<br/>- Added filter by observation, date to Organizational IO Trending report
				<br/>- Paired Performance observations filtered by Completed By Person
				<br/>- Minor changes to form filter layout to some reports
			</td>
		</tr>
		<tr class='even'>
			<td style='width: 50px; vertical-align: top;'>2.2</td>
			<td style='text-align: left; width: 400px'>
				- More excel reporting options
				<br/>- Removed IO requirement
				<br/>- Added comments with IO requirement
				<br/>- Safe Worker only counted toward completion metrics
			</td>
		</tr>
		<tr class='odd'>
			<td style='width: 50px; vertical-align: top;'>2.1</td>
			<td style='text-align: left; width: 400px'>
				- Fixed issue with exclusions
				<br/>- Added print feature
				<br/>- Added delete feature to Red Tag Audits
			</td>
		</tr>	
		<tr class='even'>
			<td style='width: 50px; vertical-align: top;'>2.0</td>
			<td style='text-align: left; width: 400px'>- Initial Corporate release</td>
		</tr>	
	</table>
</div>
