<?php $style = "style='text-align: left; width: 200px;'";?>
<?php require_once("/opt/apache/servers/soteria/htdocs/src/php/require.php"); ?>
<?php
	$questions = array(
		"Who is expected to complete a Safe Worker Observation?" 
			=> "All directors, managers, general supervisors, and supervisors, regardless of whether they have direct reports are expected to complete a Safe Worker Observation per week. Supervisors without direct reports are not required to perform Safe Worker Observations only if they have been specifically been made exempt by their leadership.",
		"When must the observation be conducted in order to comply with the weekly requirement?" 
			=> "The observation must be completed and submitted in SOTeria any time during a given week (Monday thru Sunday), for the leader to take credit for performing that observation on that given week.",
		"Where can an observation be performed?" 
			=> "An observation can be performed anywhere the employee being observed is located. This could be at the employee's desk, out at a construction job-site, in a warehouse, or even in areas like the cafeteria, if safety improvement opportunities are observed.",
		"What feedback am I expected to give to the worker being observed?" 
			=> "If a safety improvement opportunity is observed this should be shared with the employee. Positive feedback from the observation, such as work areas clear of tripping hazards should also be reinforced to the employee.",
		"How do I record an observation?" 
			=> "SOTeria (our Safety Observations Tool) must be used to record the safety observations. SOTeria is available as a web-based application, and is accessible universally through a desktop computer, laptop, MDT, tablets (iPad/Android), and smart-phones (Android/iPhone/Blackberry/Windows/Symbian). Please refer to the appropriate User's Guide for specific step-by-step instructions, located under the Help menu when you log-in to SOTeria. In cases where none of the electronic devices are available, please print copies of the form from SOTeria and use the paper copies to record the observation at the job-site. It is important that the this hard-copy be then entered into SOTeria in order for the observation to be recorded in the system and counted.",
		"What feedback am I expected to give to the worker being observed?" 
			=> "If a safety improvement opportunity is observed this should be shared with the employee. Positive feedback from the observation, such as work areas clear of tripping hazards should also be reinforced to the employee.",
		"What are the Output Metrics and how are they calculated?" 
			=> "1) Weekly Observations (Internal) - This is the ratio of all leaders who performed an observation in the given week, to the total number of leaders.</br>
				2) Weekly Observations (Corporate Safety) - The Weekly Observations metric that is reported to corporate safety excludes leaders below the manager title with no direct reports, however, as explained above, those leaders are expected to perform an observation, and will be counted in the internal Weekly Observations metric.</br>
				3) Quarterly Observations - This metric is to ensure all employees are being observed at least once per quarter. The metric is calculated for each leader by taking the ratio of employees that were observed in a given quarter to the total number of employees reporting up to that leader (with the goal being 100% observed). If an employee was observed by someone other than their direct leader they are still counted as an observation for their leader.",
		/*"What reports are available to me?"	
			=> "<b>Individual Reports</b>
				<br /><b>Organizational Reports</b>",*/
		"How to disable/enable weekly notifications" 
			=> "After you log into SOTeria, from the main menu click on Settings > Notifications. From here, you can modify your notification settings.",
		"How do I add a delegate for myself"
			=> "After you log into SOTeria, from the main menu click on Settings > Delegates. From here, you can add or remove your delegetes.",
	);
	
	ksort($questions);
	
	$x = 0;
	echo "<div style='margin: 5px; width: 800px;'>";
	/*foreach($questions as $key=> $value){
		$x++;
		echo "<div>
				<a href='#{$x}' onclick='if(last){last.style.backgroundColor=\"#fff\";}  last = {$x}; dojo.byId(last).style.backgroundColor=\"yellow\"; return false;'>{$key}</a>
			</div>";
	}
	*/
	
	$x = 0;
	foreach($questions as $key=> $value){
		$x++;
		echo "
		<div style='margin-bottom: 10px;' id='{$x}'>
		<div style='font-size: 14px; text-decoration: underline;'>
				<b>{$key}</b>
			</div>";
		echo "<div style='padding-left: 0px;'>
				{$value}
			</div>
		</div>";	
	}
	
	echo "</div>";
?>