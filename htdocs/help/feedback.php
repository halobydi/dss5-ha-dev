<?php require_once("/opt/apache/servers/soteria/htdocs/src/php/require.php"); ?>
<?php
	if($_GET["exclusion"] == "true"):
?>
<div style='width: 615px; margin: 0 auto; text-align: center;'>
	<div style='margin-top: 5px; text-align: left; padding: 5px;'>
		An exclusion will tell the system you are not able to complete an observation therefore, you will not get marked as incomplete. 
		Request an exclusion ahead of time for planned absences. Note: If a Supervisor is aware of unplanned absences, then they should request exclusions for their employees. 
		Start dates should be on a Monday and end dates should be on Sunday for a an exclusion.
	</div>
	<table class='tbl'>
		<tr>
			<th>
				<div class='inner' style='text-align: center; width: 296px;'>
					Exclude From
				</div>
			</th>
			<th>
				<div class='inner' style='text-align: center; width: 296px;'>
					Exclude To
				</div>
			</th>
		</tr>
		<tr class='odd'>
			<td>
				<input type='text' name='from' id='from' style='width: 260px; padding: 3px; border: 1px solid #000;' onkeydown='return false;'/> 
				<img src='../src/img/calendar.gif' alt='' id="tcal" onmouseover="setup_cal('tcal', 'from', true);" />
				
			</td>
			<td>
				<input type='text' name='to' id='to' style='width: 260px; padding: 3px; border: 1px solid #000;' onkeydown='return false;'/> 
				<img src='../src/img/calendar.gif' alt='' id="tcal2" onmouseover="setup_cal('tcal2', 'to', true);" />
			</td>
		</tr>
	</table>
	<table class='tbl'>
		<tr>
			<th>
				<div class='inner' style='text-align: left; width: 600px;'>
					&nbsp;Employee To Be Exluded
				</div>
			</th>
		</tr>
		<tr class='odd'>
			<td style='text-align: left;'>
				<input type='checkbox' id='self' checked=checked onclick='sot.feedback.checkSelfOther(this.checked, "self");'/> I am requesting an exclusion for myself (<?=$user["name"]?>)
			</td>
		</tr>
		<tr class='odd'>
			<td style='text-align: left; border-right: none;'>
				<input type='checkbox' id='other' onclick='sot.feedback.checkSelfOther(this.checked, "other");'/> I am requesting an exclusion for another employee&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type='text' style='width: 260px; padding: 3px; border: 1px solid #000;' id='usid' maxlength='6' value='Enter their username (ie: u12345)' onfocus='defaultValueHelper(this, "Enter their username (ie: u12345)");' onblur='defaultValueHelper(this, "Enter their username (ie: u12345)");'/>
			</td>
		</tr>
	</table>
	<table class='tbl'>
		<tr>
			<th>
				<div class='inner' style='text-align: left; width: 600px;'>
					&nbsp;Remarks
				</div>
			</th>
		</tr>
		<tr class='odd'>
			<td>
				<textarea style='height: 50px; width: 590px; border: 1px solid #000;' id='remarks' onfocus='defaultValueHelper(this, "Please provide a short description for this exclusion. Note all exclusions are sent to SOTERIA@dteenergy.com and to your Business Unit Representatives for review.");' onblur='defaultValueHelper(this, "Please provide a short description for this exclusion. Note all exclusions are sent to SOTERIA@dteenergy.com and to your Business Unit Representatives for review.");'>Please provide a short description for this exclusion. Note all exclusions are sent to SOTERIA@dteenergy.com and to your Business Unit Representatives for review.</textarea>
			</td>
		</tr>
	</table>
	<div style='text-align: right;'>
		<button style='width: 100px;' onclick='sot.feedback.sendFeedback();'>Submit</button>
	</div>
</div>
<?php	
	else:
?>
<div style='width: 615px; margin: 0 auto; text-align: center;'>
	<table class='tbl'>
		<tr>
			<th>
				<div class='inner' style='text-align: left; width: 600px;'>
					Feedback
					<span style='font-size: 10px;'> (ie: Question, Requests, Exlusions, etc. All information is sent to SOTERIA@dteenergy.com and processed accordingly)</span>
				</div>
			</th>
		</tr>
		<tr class='even'>
			<td>
				<textarea style='height: 100px; width: 590px;' id='feedback'></textarea>
			</td>
		</tr>
	</table>
	<div style='text-align: right;'>
		<button style='width: 100px;' onclick='sot.feedback.sendFeedback();'>Submit</button>
	</div>
</div>
<?php	
	endif;
?>