<?php require_once('src/php/require.php'); ?>
<?php $delegate = (!empty($_GET["delegate"]) && $_GET["delegate"] != 0 ? "&delegate={$_GET["delegate"]}" : "") ?>
<?php
	mcl_Html::s(mcl_Html::SRC_JS, "
		dojo.ready(function(){
			sot.home.setReportDelegate({$_GET["delegate"]});
			sot.home.setQuarter({$quarter});
			sot.home.setDates('{$start}', '{$end}');
			sot.home.initiate('{$usid}');
		});
	");
	
?>
<div id = 'info'>
	<div style = 'float: left; padding-left: 5px;'>
		<?php
			$forms = array(
				"s" => "Enterprise Performance Observation",
				"r" => "Red Tag Audit",
				"h" => "Anatomy of an Event Report",
				"nm" => "Near Miss Incident",
				"gc" => "Good Catch Incident",
				"sd" => "Storm Duty Observation",
			);
			
			$class = array(
				"0"	=> "error",
				"1"	=> "success"
			);
			foreach($forms as $key=>$value) {
				if(isset($_GET[$key])) {
					echo "<div class='{$class[$_GET[$key]]}'>";
					echo ($_GET[$key] == "1" ? "Successfully saved {$value}" : ($_GET[$key] == "0" ? "Unable to save {$value}" : ""));
					echo "</div>";
				}
			}
		?>
		<div style='font-size: 13px;'>
			<?php
				$days_left = quarter();
				echo 'Today is '. $now["weekday"] . ', ' . $now["month"] . ' ' . $now["mday"] . ' ' . $now["year"] . 
					'. There ' . (($days_left > 1 || $days_left == 0) ? 'are' : 'is') . ' ' . $days_left .' day' . (($days_left > 1 || $days_left == 0) ? 's' : '') . '  left in this quarter.';
			?>
		</div>
		<div>
			<div>
				<a href = 'help/feedback.php?sid=<?php echo time(); echo $delegate; ?>'>Send Feedback</a> <span style='font-size: 10px;'>(ie. question, comments, request, etc.)</span>
			</div>
			<div>
				<a href = 'help/feedback.php?exclusion=true&sid=<?php echo time(); echo $delegate; ?>'>Submit an Exclusion</a> <span style='font-size: 10px;'></span>
			</div>
			<div>
				<!--<img src='src/img/newdocument.png' style='cursor: pointer; height: 13px; margin-left: 12px;' alt='Create New SWO Observation' onclick='window.location="forms/epop.php?swo<?php echo $delegate; ?>"'/>-->
				<a href = 'forms/epop.php?swo<?php echo $delegate; ?>'>Create New SWO Observation</a>
			</div>
			
		</div>
	</div>
	<div style = 'float: right' style = 'width: 510px; overflow: hidden; text-align: right;'>
		<table>
			<tr style = 'vertical-align: bottom;'>
				<td>
					<input type = 'text' style = 'height: 12px; width: 100px;' name = 'start' id = 'start' value = "<?php echo $start; ?>"/> <img src='src/img/calendar.gif' alt='' id="tcal" onmouseover="setup_cal('tcal', 'start');" />
				</td> 
				<td>
					<input type = 'text' style = 'height: 12px; width: 100px;' name = 'end' id = 'end' value = "<?php echo $end; ?>" /> <img src='src/img/calendar.gif' alt='' id="tcal2" onmouseover="setup_cal('tcal2', 'end');" />
				</td>
				<td style = 'text-align: right;'>
					<input onclick = "sot.home.dateFilter();" type = "submit"  name = "submitter" value = "Filter"/>
					<input onclick = "dojo.byId('start').value = '<?php echo $startWeek; ?>'; dojo.byId('end').value = '<?php echo $endWeek; ?>'; sot.home.dateFilter();" type = "submit" name = "submitter" value = "Current Week"/>
					<input onclick = "dojo.byId('start').value = '<?php echo $startWeekPast; ?>'; dojo.byId('end').value = '<?php echo $endWeekPast; ?>'; sot.home.dateFilter();"  type = "submit" name = "submitter" value = "Past Week"/>
				</td>
			</tr>
		</table>
	</div>
</div>
<div id = 'container' style = 'clear: both;'>
</div>

<?php
	if($checker['iphone'] || $checker['android']){
		echo "<div style='text-align: center; margin-top: 20px;'> Desktop / <a href='{$baseDir}/mobile/index.php?mobile=true'>Mobile</a></div>";
	}
?>