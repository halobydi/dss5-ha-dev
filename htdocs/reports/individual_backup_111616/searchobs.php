<?php
require_once("../../src/php/require.php");

$sql = "
	SELECT 	* 
	FROM 	EMPLOYEES 
			WHERE PATH LIKE '%{$usid}%'
	ORDER BY NAME
";
$oci = new mcl_Oci("soteria");

$employees_post = $_POST["employees"];

$x = 0;
while($row = $oci->fetch($sql)){
	$employees[$row["USID"]] = $row;
	$checked = (isset($employees_post[$row["USID"]]) ? "checked=checked" : "");
	//style='background-color: " . ($x++ % 2 == 0 ? '#cecece' : '#f0f0f0') . "'
	$employees_checkboxes .= "<div>
								<input type='checkbox' name='employees[{$row["USID"]}]' value='{$row["USID"]}' {$checked} /> {$row["NAME"]}
						</div>";
}

if(empty($employees_checkboxes)){
	$employees_checkboxes  = "<div style='text-align: center;'>--</div>";
}

$anonymous = $_POST["anonymous"] == "on" ? true : false;

echo "
<form method = 'POST'  action='searchobs.php" . (!empty($_GET["delegate"]) && $_GET["delegate"] != 0 ? "?delegate={$_GET["delegate"]}" : "") . "' style='margin: 0px; padding: 0px;'>
	<table style = 'font-size: 12px; margin: 3px;'>
		<tr>
			<td>Date Observed:</td>
		</tr>
		<tr>
			<td>
				<input style = 'height: 12px; width: 100px;' type = 'text' name = 'start' id = 'start' value = '{$start}'/> <img style = 'vertical-align: bottom;' src='../../src/img/calendar.gif' alt='' id='tcal' onmouseover='setup_cal(\"tcal\", \"start\");' />
				<input style = 'height: 12px; width: 100px;'  type = 'text' name = 'end' id = 'end' value = '{$end}' /> <img style = 'vertical-align: bottom;' src='../../src/img/calendar.gif'  alt='' id='tcal2' onmouseover='setup_cal(\"tcal2\", \"end\");' />
			</td>
		</tr>
		<tr>
			<td>Employee(s):</td>
		</tr>
		<tr>
			<td>
				<div style='width: 300px; max-height: 300px; overflow: auto; border: 1px solid #aaa; font-weight: normal;'>
					{$employees_checkboxes}
				</div>
			</td>
		</tr>
		<tr>
			<td style='text-align: left;'>
				<input type='checkbox' style='' name='anonymous' " . ($anonymous ? "checked=checked": "") . "/> Search anonymous observations by my team (employee observed = anonymous)
			</td>
		</tr>
		<tr>
			<td>Other observations (ie. vendor name):</td>
		</tr>
		<tr>
			<td>
				<input type='text' style='width: 200px;' name='search_by_text' value='{$_POST["search_by_text"]}'/>
			</td>
		</tr>
		<tr>
			<td style = 'text-align: left;'>
				<input type = 'submit' style = \"height: 19px; width: 100px;\" value = 'Search'/>
			</td>
		</tr>
	</table>
</form>
";

$search_by_text = str_replace("'", "''", strtoupper(trim($_POST["search_by_text"])));

if(!empty($employees_post) || !empty($search_by_text) || $anonymous){
	foreach($employees_post as $key=>$value){
		$employees_in .= (empty($employees_in) ? "" : " UNION ALL ") . "SELECT '{$key}' AS USID FROM DUAL";
	}
	
	$oCt = 0;
	if(!empty($employees_in) && !empty($search_by_text)) {
		$operatora = "OR";
	}

	if((!empty($employees_in) || !empty($search_by_text)) && $anonymous) {
		$operatorb = "OR";
	}
		
	if(!empty($employees_in)){
		$employees_in = "(EMPLOYEE = T.USID 
					AND EMPLOYEE IN (
					{$employees_in}
				))";
	}
	
	if(!empty($search_by_text)){
		$search_by_text = "(
			(UPPER(O.NAME) LIKE '%{$search_by_text}%') 
			AND O.COMPLETED_BY = T.USID
		)";
	}
	
	if($anonymous) {
		$search_by_anonymous = "(
			O.NAME IS NULL 
			AND O.COMPLETED_BY = T.USID
		)";
	}
	
	$sql = "
		WITH T AS(
			SELECT USID FROM EMPLOYEES WHERE PATH LIKE '%{$usid}%'
		)
		SELECT  O.EPOP_ID, 
				O.NAME, 
				O.TITLE,
				NVL(E.NAME, COMPLETED_BY) AS COMPLETED_BY,
				TO_CHAR(OBSERVED_DATE, 'MM/DD/YYYY') AS DT 
		FROM(
			SELECT 	O.* 
			FROM 	EPOP_OBSERVATIONS O,
					T 
			WHERE 	OBSERVED_DATE BETWEEN TO_DATE('{$start} 00:00:00', 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE('{$end} 23:59:59', 'MM/DD/YYYY HH24:MI:SS')
					AND ({$employees_in} {$operatora} {$search_by_text} {$operatorb} {$search_by_anonymous}) 
			) O LEFT JOIN EMPLOYEES E
			ON E.USID = O.COMPLETED_BY
			ORDER BY O.NAME, O.OBSERVED_DATE, O.NAME
	";
	
	//echo "<pre>{$sql}</pre>";
	
	$x = 0;
	while($row = $oci->fetch($sql)){
		$tbl .= "
			<tr class = '" . ($x++ % 2 == 0 ? 'even' : 'odd'). "'>
				<td style = 'text-align: left; width: 200px;'>{$row["NAME"]}</td>
				<td style = 'text-align: left; width: 300px;'>{$row["TITLE"]}</td>
				<td style = 'text-align: left; width: 200px;'>{$row["COMPLETED_BY"]}</td>
				<td style = 'text-align: center; width: 100px;'>{$row["DT"]}</td>
				<td style = 'text-align: center; width: 50xp;'><a href = '#'; onclick = 'sot.swo.view({$row["EPOP_ID"]}); return false;'>View</a></td>
			</tr>
		";
	}
	if($x == 0){
		$tbl = "<tr>
					<td colspan='5' style='text-align: center;'>
						No observations found with the current filters.
					</td>
				</tr>
			";
	}
	
	echo "
	<table class='tbl'>
		<tr>
			<th>
				<div class='inner' style='width: 200px;'>
					Employee Observed
				</div>
			</th>
			<th>
				<div class='inner' style='width: 300px;'>
					Title
				</div>
			</th>
			<th>
				<div class='inner' style='width: 200px;'>
					Observed By
				</div>
			</th>
			<th>
				<div class='inner' style='width: 100px;'>
					Date Observed
				</div>
			</th>		
			<th>
				<div class='inner' style='width: 50px;'>
				</div>
			</th>
		</tr>
		{$tbl}
	</table>
	";
}
?>