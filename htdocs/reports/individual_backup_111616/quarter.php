<?php

require_once("../../src/php/require.php");
require_once("../../src/php/engine.php");

$days_remaining = quarter(true);
$quarter = quarter(false);
$quarterDates = engine::quarterDates($quarter);
	
if(empty($_GET['start']) || empty($_GET['end'])) {
	$start = $quarterDates['start'];
	$end = $quarterDates['end'];
}

$oci = new mcl_Oci("soteria");

$sql = "SELECT * FROM EMPLOYEES WHERE USID = '{$usid}'";
$row = $oci->fetch($sql);
$totalEmployees = $row['TEAM'] ?: 0;

$sql = "
	SELECT  
		E.USID,
		E.NAME,
		E.TITLE,
		E2.NAME AS SUPERVISOR,
		TO_CHAR(MAX(O.OBSERVED_DATE), 'MM/DD/YYYY') AS LAST_OBSERVATION
	FROM 
		EMPLOYEES E
	LEFT JOIN
		EMPLOYEES E2 ON E2.USID = E.SUPERVISOR
	LEFT JOIN
		EPOP_OBSERVATIONS O
	ON
		O.EMPLOYEE = E.USID
	WHERE
		E.PATH LIKE '%{$usid}%'
		AND E.USID != '{$usid}'
		AND NOT EXISTS (
		      SELECT
		             1
		      FROM
		              EPOP_OBSERVATIONS O2
		      INNER JOIN
		              EMPLOYEES E3
		              ON E3.USID = O2.EMPLOYEE
		              AND E3.PATH LIKE '%{$usid}%'
		      WHERE
		              OBSERVED_DATE BETWEEN TO_DATE('{$start} 00:00:00', 'MM/DD/YYYY HH24:MI:SS')  AND TO_DATE('{$end} 23:59:59', 'MM/DD/YYYY HH24:MI:SS') 
		              AND E3.USID = E.USID
		)
		AND (OBSERVED_DATE <= TO_DATE('{$end} 23:59:59', 'MM/DD/YYYY HH24:MI:SS') OR OBSERVED_DATE IS NULL)
	GROUP BY
		E.USID,
		E.NAME,
		E.TITLE,
		E2.Name
	ORDER BY
		CASE WHEN MAX(O.OBSERVED_DATE) IS NULL THEN 0 ELSE 1 END,
		MAX(O.OBSERVED_DATE),
		E.NAME
";

$totalEmployeesNotObserved = 0;
$rows = array();
while($row = $oci->fetch($sql)) {
	$totalEmployeesNotObserved++;
	$rows[] = $row;
}

?>

<div style='text-align: left;'>
	<form method = 'GET' action='quarter.php' style = 'overflow: hidden; padding: 5px;'>
		<table>
			<tr style = 'vertical-align: bottom;'>
				<td style='font-size: 10px; font-weight: normal;  vertical-align: top;'>
					Start & End Dates - Reminder: There are <?=$days_remaining?> day<?=($days_remaining > 1 ? 's' : '')?> in this quarter (<?=$quarterDates['start']?> thru <?=$quarterDates['end']?>)
				</td>
			</tr>
			<tr>
				<td>
					<input style = 'height: 12px; border: 1px solid #000; width: 100px;' type = 'text' name = 'start' id = 'start' value = '<?php echo $start;?>'/> 
					<img style = 'vertical-align: bottom;' src='../../src/img/calendar.gif' alt='' id='tcal' onmouseover='setup_cal("tcal", "start");' />
					<span style='font-size: 10px;'>to </span> 
					<input style = 'height: 12px; border: 1px solid #000; width: 100px;'  type = 'text' name = 'end' id = 'end' value = '<?php echo $end; ?>' /> 
					<img style = 'vertical-align: bottom;' src='../../src/img/calendar.gif' alt='' id='tcal2' onmouseover='setup_cal("tcal2", "end");' />
					<?php if(!empty($_GET['delegate'])): ?><input type='hidden' name='delegate' value='<?=$_GET["delegate"]?>'/> <?php endif; ?>
					<input type = 'submit' style = "height: 18px; font-size: 10px;" value = 'Submit'/>
				</td>
			</tr>
		</table>
	</form>
</div>
<?php 
	$totalObserved = $totalEmployees - $totalEmployeesNotObserved;
	$percentObserved = @round(($totalObserved/ $totalEmployees) * 100, 2);
?>
<?php if($totalEmployees > 0): ?>
	<div style='padding: 5px;'>
		<table>
			<tr>
				<td colspan='2' style='font-weight: bold; border-bottom: 1px solid #000;'>
					Quarter Summary
				</td>
			</tr>
			<tr>
				<td style='width: 100px; border-bottom: 1px solid #000;'>
					Total Employees
				</td>
				<td style='font-weight: bold; padding: 5px; text-align: right; border-bottom: 1px solid #000;'>
					<?=$totalEmployees?>
				</td>
			</tr>
			<tr>
				<td style='border-bottom: 1px solid #000;'>
					Total Employees<br/>Observed
					<br />
					<span style='font-size: 10px;'>(Observations included<br/>are SWO and Paired)</span>
				</td>
				<td style='font-weight: bold; padding: 5px; text-align: right; border-bottom: 1px solid #000;'>
					<?=$totalObserved?>
				</td>
			</tr>
			<tr>
				<td style='border-bottom: 1px solid #000;'>
					Percent Observed
				</td>
				<td style='font-weight: bold; border-bottom: 1px solid #000; padding: 5px; text-align: right; background-color: <?=($percentObserved < 60 ? "#ffaaad" : ($percentObserved < 80 ? "#ffffbd" : "#b5d3b5"))?>;'>
					<?=$percentObserved?>%
				</td>
			</tr>
		</table>
	</div>		
<?php endif; ?>		
<div style='padding: 5px; font-weight: bold;'>
	The following employees have not had an observation performed on them between <?=$start?> and <?=$end?> - <a href='#' onclick='excel(true);'>Export to Excel</a>
</div>
<div id='exportArea'>
	<table class='tbl' style='width: 95%;'>
		<tr>
			<th><div class='inner'>Employee ID</div></th>
			<th><div class='inner' >Name</div></th>
			<th><div class='inner' >Title</div></th>
			<th><div class='inner' >Supervisor</div></th>
			<th><div class='inner'>Last Date Observed</div></th>
			<th><div class='inner'></div></th>
		</tr>
		<?php foreach($rows as $row): ?>
			<tr class='<?=($x++ % 2 == 0 ? 'even'  : 'odd')?>'>
				<td style='text-align: left;'><?= $row['USID'] ?></td>
				<td style='text-align: left;'><?= $row['NAME'] ?></td>
				<td style='text-align: left;'><?= $row['TITLE'] ?></td>
				<td style='text-align: left;'><?= $row['SUPERVISOR'] ?></td>
				<td style='text-align: center; color: red;'><?= $row['LAST_OBSERVATION'] ?: "<img src='../../src/img/x.png'/>" ?></td>
				<td><a href='../../forms/epop.php?usid=<?=$row['USID']?>&back=../reports/individual/quarter.php<?=($_GET['delegate'] ? "&delegate={$_GET['delegate']}" : "")?>'>Observe</a></td>
			</tr>
		<?php endforeach; ?>
		<?php if($totalEmployeesNotObserved == 0): ?>
			<tr>
				<td colspan='6'>
					There is no data to display!
				</td>
			</tr>
		<?php endif; ?>
	</table>
</div>
<div id='temp_clipboard'></div>