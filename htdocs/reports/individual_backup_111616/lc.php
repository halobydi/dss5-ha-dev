<?php
	require_once("../../src/php/require.php");
	
  	$completions = array();
	$week_array = array();
	$employee_array = array();
	
	$oci = new mcl_Oci("soteria");
	
	//week of 03/10/2013 - 03/17/2013
	//+1 in next_day because it would give you the begining of sunday and not inclue any observations 
	//completed on sunday so we want the next day starting at 00:00:00 which is one second difference from date 23:59:59
	$weeks = 0; //current week
	
	while($weeks < 4){
		$sql = "
			WITH T AS (
				SELECT  
					USID, 
					TITLE, 
					NAME,
					SUPERVISOR
				FROM    
					EMPLOYEES X
				WHERE 
					PATH LIKE '%{$usid}%'
					AND LEADERS != 0
					AND NOT EXISTS (
						SELECT 
							1 
						FROM 
							EXCLUSIONS E
						WHERE 
							E.USID = X.USID
							AND (E.START_DT <= (TRUNC(SYSDATE, 'IW') - " . ($weeks * 7) . ") OR START_DT IS NULL)
							AND (E.END_DT >= (NEXT_DAY(TRUNC(SYSDATE,'IW'), 'SUNDAY') - " . ($weeks * 7) . ") + .999 OR END_DT IS NULL)
					)
			),
			T2 AS (
				SELECT 		
					T.USID, 
					COUNT(EPOP_ID) AS CT 
				FROM 
					T
				LEFT JOIN 
					EPOP_OBSERVATIONS O 
					ON T.USID = O.OBSERVATION_CREDIT
				WHERE 
					O.OBSERVED_DATE BETWEEN (TRUNC(SYSDATE, 'IW') - " . ($weeks * 7) . ") AND (NEXT_DAY(TRUNC(SYSDATE,'IW'), 'SUNDAY') - " . ($weeks * 7) . ") + .999 
				GROUP BY USID
			)
			SELECT 
				T.USID, 
				T.NAME, 
				E.NAME AS SUPERVISOR,
				T.TITLE, 
				NVL(T2.CT, 0) COMPLETE,
				TO_CHAR(TRUNC(SYSDATE, 'IW') - " . ($weeks * 7) . ", 'MM/DD/YYYY') AS WEEK_START,
				TO_CHAR(NEXT_DAY(TRUNC(SYSDATE,'IW'), 'SUNDAY') - " . ($weeks * 7) . ", 'MM/DD/YYYY') AS WEEK_END
			FROM T
			LEFT JOIN T2 ON T.USID = T2.USID
			LEFT JOIN EMPLOYEES E
				ON T.SUPERVISOR = E.USID
			ORDER BY T.NAME
		";
		
		$dt = date('m/d/Y', strtotime("-{$weeks} weeks", mktime(0,0,0, date("m"), date("d") - (date("w") == 0 ? 7 : date("w")) + 1, date("Y"))));
		$week_array[$weeks] = array(
			"start"		=> $dt,
		);
		
		while($row = $oci->fetch($sql)){
			$completions[$row["USID"]][$weeks] = $row["COMPLETE"];
			
			$employee_array[$row["USID"]] = array(
				"name" 		=> $row["NAME"],
				"title" 	=> $row["TITLE"], 
				"supervisor"	=> $row["SUPERVISOR"]
			);
		}
		
		$weeks++;
	}
	
	$currentYear = intval(date('Y'));
	$currentWeek = intval(date('W'));
	mcl_Html::s(mcl_Html::SRC_JS, <<<JS
		function excelLc() {
			var weeks = document.getElementById('lc_wks') ?  document.getElementById('lc_wks').value : 4;
			var weekStop = {$currentWeek};
			var weekStart = weekStop - weeks + 1;
			var year = {$currentYear};

			var url = 'lcexcel.php?delegate={$_GET["delegate"]}&excel=true&weekStop=' + weekStop + '&weekStart=' + weekStart + '&year=' + year;

			window.open(url, '', 'width=50, height=50, menubar=0');
		}
		
JS
);
	
	/*Calculate week # in quarter*/
	$weeksInto = ceil((90 - quarter(true)) / 7);

	echo "<div style='margin: 5px;'>
			Note: Number of observations completed in the week is displayed to the right of the <img src='../../src/img/check.png'/> or <img src='../../src/img/x.png'/><br/>	
			<img src='../../src/img/excel.png' style='width: 14px; height: 14px;'/> <a href='#' onclick='excelLc();'>Click here</a> to export this report to excel 
				for a <input type='text' id='lc_wks' style='width: 18px; height: 11px; border: 1px solid black;' value='4'/> week overview.
				<span style='font-size: 10px;'>(We are {$weeksInto} week" . ($weeksInto > 1 ? "s" : "") . " into the current quarter - inclusive of this week. Note: Entering a large number for weeks may cause the report to be slow.)</span><br/>
		</div>
		<div id='tbl'>
		<table class='tbl'>
			<tr>
				<th colspan='" . ($weeks + 3) . "'><div class='inner_title'>SWO Leader Completions</div></th>
			</tr>
			<tr>
				<th class='double'><div class='inner' style='width: 150px;'>Leader</div></th>
				<th class='double'><div class='inner' style='width: 250px;'>Title</div></th>
				<th class='double'><div class='inner' style='width: 150px;'>Supervisor</div></th>
			";
			
			$weekCt = 0;
			while($weekCt < $weeks){
				echo "<th class='double'><div class='inner' style='width: 150px;'>" . 
					($weekCt == 0 ? "Completed an Observation this Week?" : 
						($weekCt == 1 ? "Completed an Observation Last Week?" : 
							"Completed an Observation Week of " . ($week_array[$weekCt]["start"]) . "?")) ."</div></th>";
				$weekCt++;
			}
	echo "</tr>";
	
	$x = 0;
	function sortbyname($a, $b){
		if ($a["name"] == $b["name"]) {
			return 0;
		}
		
		return ($a["name"] < $b["name"]) ? -1 : 1;
	}

	uasort($employee_array, "sortbyname");

	foreach($employee_array as $key=>$value){
		echo "<tr class='" . ($x++ % 2 == 0 ? 'even' : 'odd') . "'>
				<td style='text-align: left; width: 150px;'>{$value["name"]}</td>
				<td style='text-align: left; width: 250px;'>{$value["title"]}</td>
				<td style='text-align: left; width: 150px;'>{$value["supervisor"]}</td>
			";
		
		$weekCt = 0;
		while($weekCt < $weeks){
			$complete = $completions[$key][$weekCt] == 0 ? "<img src='../../src/img/x.png'/>" : "<img src='../../src/img/check.png'/>"; 
			if(!isset($completions[$key][$weekCt])) { $complete = '<span style="font-size: 10px;">Excluded</span>'; }
			$ct = "<span class='lc_ct' style='margin-left: 10px;'>{$completions[$key][$weekCt]}</span>";
			echo "<td>{$complete} {$ct}</td>";

			$weekCt++;
		}	
		
		echo "</tr>";
	}
	
	if($x == 0){
		echo "<tr>
				<td colspan='" . ($weeks + 3) . "'>There are no leader completions to display.</td>
			</tr>";
	}
	
	echo "</table>";
	echo "</div>";
	
	


?>