<?php
	require_once("../../src/php/require.php");
	
	$oci = new mcl_Oci("soteria");
	
	require_once('lc_include.php');

	mcl_Html::s(mcl_Html::SRC_JS, <<<JS
		var max = ({$weekStop});
		var min = ({$weekStart});
		var current = ({$weekStop} - {$display})  + 1;
		var display = {$display};

		function move(dir) {
			if((current + display - 1) == max && dir == 1){
				return;
			} else if(current == min && dir == -1){
				return;
			}
			
			current += dir;
			var rows = document.getElementById('tbl').rows;
			var cell = current + 1;
		
			for(var i = 0; i < rows.length; i++){
				if(dir == -1){ //prev
					rows[i].cells[(cell - 1) + display].style.display = 'none';
					rows[i].cells[cell - 1].style.display = '';
					
				} else { //next
					rows[i].cells[(cell - 2) + display].style.display = '';
					rows[i].cells[cell - 2].style.display = 'none';
					
				}
			}
		}

		function excelLc() {
			window.open('lcexcel.php?delegate={$_GET["delegate"]}&excel=true&weekStop={$weekStop}&weekStart={$weekStart}&year={$year}', '', 'width=50, height=50, menubar=0');
		}

		function updateWeeks() {
			var weekStart = document.getElementById('weekStart').value;
			var weekStop = document.getElementById('weekStop').value;
			var year = document.getElementById('year').value;
			window.location = 'lcytd.php?delegate={$_GET["delegate"]}&weekStop=' + weekStop + '&weekStart=' + weekStart + '&year=' + year;
		}

		function onKeyUpUpdateWeeks(event) {
			var keyCode = event.keyCode;
			if(keyCode == 13) {
				updateWeeks();
			}
		}
JS
);
	
	mcl_Html::s(mcl_Html::SRC_CSS, "
		table {
			padding:			0;
			margin:				5px;
			border-collapse:	collapse;
			border: none;
			
		}
		
		table tr th {
			background-color:	#b5cfe8;
			border:				1px solid black;
			font-weight: 		normal;
		}
		
		table tr th div {
			font-size:			11px;
			background-color:	#6598cb;
			padding:			5px;
		}
		
		td {
			border:				1px solid black;
			padding-top:		5px;
			padding-bottom:		5px;
		}

		table tr.even {
			background-color:	#cecece;
		}
		
		table tr.odd {
			background-color:	#f0f0f0;
		}
		
		table tr.odd  {
			vertical-align:		top;
			padding-right: 		5px;
		}
		
		table tr:hover  {
			background-color: lightyellow;
		}
		
		form {
			text-align: left;
			margin-top: 10px;
			margin-left: 5px;
			margin-bottom: 5px;
		}
		
		input[name=start],
		input[name=end] {
			border: 1px solid #000;
			padding: 3px;
			font-size: 13px;
			
		}
	");

?>
	<div style='margin: 5px; vertical-align: bottom: text-align: bottom;'>
		Displaying metrics for week
		<input type='text' id='weekStart' onkeyup="onKeyUpUpdateWeeks(event)" style='width: 18px; height: 11px; border: 1px solid black;' value='<?=$weekStart?>'/>
		(<b><?=$startDate?></b>) through week
		<input type='text' id='weekStop' onkeyup="onKeyUpUpdateWeeks(event)" style='width: 18px; height: 11px; border: 1px solid black;' value='<?=$weekStop?>'/>
		(<b><?=$endDate?></b>)
		for the year
		<input type='text' id='year' onkeyup="onKeyUpUpdateWeeks(event)" style='width: 25px; height: 11px; border: 1px solid black;' value='<?=$year?>'/>
		<a href="#" onclick="updateWeeks(); return false;">Change</a>
	</div>
	<div style='margin: 5px;'>
		<img src='../../src/img/excel.png' style='width: 14px; height: 14px;'/>
		<a href='#' onclick='excelLc(); return false;'>Click here</a> to export this report to excel for week <?=$weekStart?> through week <?=$weekStop?>.
		<span style='font-size: 10px; font-style: italic;'>Export also contains employee username, sapid, and director</span>
	</div>
	<?php

	echo "
	<table id = 'tbl' style='margin-top: 5px;'>
		<tr>
			<th style='text-align: left;'><div style='width: 200px;'>Leader</div></th>
		";	

	$w = $weekStart - 1;
	while($w++ < $weekStop){
		if($week_array) $title = "{$week_array[$w]['start']} to {$week_array[$w]['end']}";
		echo "
			<th " . ($w < $start ? "style='display: none;'" : "") . ">
				<div id='wk{$w}' style='width: 40px;' title='{$title}' >
					 wk. " .  ($w ) . "&nbsp;
				</div>
			</th>
		";
	}

	$sql = "SELECT * FROM EMPLOYEES WHERE PATH LIKE '%{$usid}%' AND LEADERS > 0 ORDER BY NAME";

	$allEmployees = array();
	while ($row = $oci->fetch($sql)){
		$allEmployees[$row["USID"]] = $row["NAME"];
	}
	
	if(count($allEmployees) == 0 ) {
		$colspan = $display + 1;
		die("
			<tr>
				<td colspan='{$colspan}' style='text-align: center;'>There are no leader completions to display.</td>
			</tr>
		</table>
		");
	} else if($weekStop - $weekStart > $display) {
		echo "
			<th style = 'font-size: 10px; font-weight: normal; border: none; background-color: #fff;'>
				&nbsp;<a href = '#' onclick = 'move(-1);' id = 'prev'>Prev</a> | <a href = '#' onclick = 'move(1);' id = 'next'>Next</a>
			</th>
		</tr>
		";
	} else {
		echo "</tr>";
	}
	
	
	$red = "#ffaaad";
	$yellow = "#ffffbd";
	$green = "#b5d3b5";
	
	foreach($allEmployees as $key=>$value){
		$value = trim($value);
		echo "
			<tr class = '" . ($x++ % 2 == 0 ? 'even' : 'odd') . "'>
				<td style='text-align: left; padding-left: 5px;'>{$value}</td>
		";

		$w = $weekStart - 1;
		$numerator = 0;
		$denominator = 0;
		while($w++ < $weekStop){
			$excluded = "";

			$bgColor = "";
			if(!isset($completions[$key][$w])) { 
				$echo = "<span style='font-size: 10px;'>Excluded</span>";
				$tooltip = "Employee was excluded for wk. {$w}";
			} else { 
				$echo = $completions[$key][$w];
				
				$tooltip = "{$value} completed {$echo} observation" .  ($echo == 1 ? "" : "s") ." on wk. {$w}";
				
				if($echo == 0) {
					$bgColor = $red;
				}
				
				if($completions[$key][$w] > 0) { $numerator++; $week_array[$w]["completed_ct"]++; }
				
				$denominator++;
				$week_array[$w]["employee_ct"]++; 
			}

			$id = $value . "_" . $w . $year;
			echo "
				<td title = \"{$tooltip}\" id = '{$id}' class = '{$w}' style = 'display: " . ($w  < $start ? "none" : "") . "; text-align: center; background-color: {$bgColor}'>
					{$echo}
				</td>
			";
		}
		
		$percent = @round(($numerator / $denominator) * 100, 2);
		$bgColor = ($percent < 80 ? $percent <= 60 ? $red : $yellow : $green);
		echo "<td id = 'TOTAL_{$value}' style = 'text-align: center; width: 50px; background-color: {$bgColor};' title=\"{$value}'s Year to Date Leader Completion is {$percent}%\" >{$percent}%</td>";
		echo "</tr>";
	}
	
	$totalCompleted = 0;
	$totalExpected = 0;
	
	echo "<tr>
			<td style='border: none; background-color: #fff;'>&nbsp</td>";
	$w = $weekStart - 1;

	if(count($allEmployees) > 0) {
		while($w++ < $weekStop){
			$value = $week_array[$w];
			$percent = @round(($value["completed_ct"] / $value["employee_ct"]) * 100, 2);
			
			$totalCompleted += $value["completed_ct"];
			$totalExpected += $value["employee_ct"];
			
			$bgColor = ($percent < 80 ? $percent <= 60 ? $red : $yellow : $green);
			$show = ($w  < $start ? "none" : "show");
			echo "<td id = 'TOTAL_{$w}' style = 'display: " . ($w  < $start ? "none" : "") . "; text-align: center; width: 50px; background-color: {$bgColor};' title='Team Leader Completions for wk. {$w} is {$percent}%'>{$percent}%</td>";
			
		}

		$percent = @round(($totalCompleted / $totalExpected ) * 100, 2);

		$bgColor = ($percent < 80 ? $percent <= 60 ? $red : $yellow : $green);
		echo "
				<td id = 'TOTAL_TOTAL' style = 'text-align: center; width: 50px; background-color: {$bgColor};' title='Team Leader Completions Year to Date is {$percent}%'>{$percent}%</td>
			</tr>
		</table>
		";
	}
	
	
?>