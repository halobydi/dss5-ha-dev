<?php
require_once("../../src/php/require.php");

$sql = "
	SELECT 	ITEM_CATEGORY, ITEM_NUM, ITEM_TEXT
	FROM 	EPOP_ITEMS 
	WHERE 	ITEM_CATEGORY IN ('Leader', 'Organization', 'Individual')
	ORDER BY ITEM_TEXT
";

$oci = new mcl_Oci("soteria");

$items_post = $_POST["items"];
$prev_category = "";

$items = array("comments" => "Additional Comments");

while($row = $oci->fetch($sql)){
	$items[$row["ITEM_NUM"]] = $row["ITEM_TEXT"];
	$checked = (isset($items_post[$row["ITEM_NUM"]]) ? "checked=checked" : "");
	
	if($row["ITEM_CATEGORY"] != $prev_category ){
		//$items_checkboxes .= "<div style='padding: 3px; font-weight: bold; background-color:#ffffc4'>{$row["ITEM_CATEGORY"]}</div>";
	}
	//style='background-color: " . ($x++ % 2 == 0 ? '#cecece' : '#f0f0f0') . "'
	$items_checkboxes .= "<div>
							<input type='checkbox' name='items[{$row["ITEM_NUM"]}]' value='{$row["ITEM_NUM"]}' {$checked} />
							{$row["ITEM_TEXT"]}
						</div>";
	
	$prev_category  = $row["ITEM_CATEGORY"];
}

if(empty($items_checkboxes)){
	$items_checkboxes  = "<div style='text-align: center;'>--</div>";
}

$additional_comments = false;
if(empty($_POST) || isset($_POST["additional_comments"])){
	$additional_comments = true;
} 

mcl_Html::s(mcl_Html::SRC_JS, <<<JS
	function checkAll(checked) {
		var checkboxes = document.getElementById('checkboxes').getElementsByTagName('INPUT');
		for (var x=0; x<checkboxes.length; x++) {
			checkboxes[x].checked = checked;	
		}
	}
JS
);

echo "
<form method = 'POST'  action='comments.php" . (!empty($_GET["delegate"]) && $_GET["delegate"] != 0 ? "?delegate={$_GET["delegate"]}" : "") . "' style='margin: 0px; padding: 0px;'>
	<table style = 'font-size: 12px; margin: 3px;'>
		<tr>
			<td>Date Observed:</td>
		</tr>
		<tr>
			<td>
				<input style = 'height: 12px; width: 100px;' type = 'text' name = 'start' id = 'start' value = '{$start}'/> <img style = 'vertical-align: bottom;' src='../../src/img/calendar.gif'  alt='' id='tcal' onmouseover='setup_cal(\"tcal\", \"start\");' />
				<input style = 'height: 12px; width: 100px;'  type = 'text' name = 'end' id = 'end' value = '{$end}' /> <img style = 'vertical-align: bottom;' src='../../src/img/calendar.gif' alt='' id='tcal2' onmouseover='setup_cal(\"tcal2\", \"end\");' />
			</td>
		</tr>
		<tr>
			<td style='vertical-align: bottom;'>
				<input type='checkbox' name='additional_comments' style='position: relative; left: -3px;' " . ($additional_comments ? "checked=checked" : "") . "/>Show additional comments entered for observations
			</td>
		</tr>
		<tr>
			<td>
				Show comments for different observation items:
				<br />
				<input type='checkbox'  onclick='checkAll(this.checked)'/> Check All
			</td>
			
		</tr>
		<tr>
			<td>
				<div style='width: 900px; max-height: 300px; overflow: auto; border: 1px solid #aaa; font-weight: normal;' id='checkboxes'>
					{$items_checkboxes}
				</div>
			</td>
		</tr>
		<tr>
			<td style = 'text-align: left;'>
				<input type = 'submit' style = \"height: 19px; width: 100px;\" value = 'Search'/>
			</td>
		</tr>
	</table>
</form>
";

if(is_array($items_post)){
	foreach($items_post as $key=>$value){
		$items_in .= (empty($items_in) ? "" : " UNION ALL ") . "SELECT '{$key}' AS ITEM_NUM FROM DUAL";
	}
}

$sql = "
	WITH T AS (
		SELECT * FROM EMPLOYEES WHERE PATH LIKE '%{$usid}%'
	)
	SELECT O.*,
		NVL(E.NAME, O.COMPLETED_BY) AS COMPLETED_BY
	FROM(
	" . (!empty($items_in) ? "
		SELECT 	
			O.EPOP_ID, 
			O.NAME, 
			O.TITLE,
			COMPLETED_BY,
			TO_CHAR(OBSERVED_DATE, 'MM/DD/YYYY') AS DT,
			TO_CHAR(A.ITEM_NUM) AS ITEM_NUM,
			C.COMMENTS
		FROM 	EPOP_OBSERVATIONS O,
				EPOP_ANSWERS A,
				EPOP_COMMENTS C,
				T
		WHERE 	T.USID = NVL(O.EMPLOYEE, O.COMPLETED_BY )
			AND A.EPOP_ID = O.EPOP_ID
			AND	A.ITEM_NUM IN(
				{$items_in}
			)
			AND	A.ITEM_NUM = C.ITEM_NUM
			AND O.EPOP_ID = C.EPOP_ID
			AND C.COMMENTS IS NOT NULL
			AND OBSERVED_DATE BETWEEN TO_DATE('{$start} 00:00:00', 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE('{$end} 23:59:59', 'MM/DD/YYYY HH24:MI:SS')
		" : "") . (!empty($items_in) && $additional_comments ? "
			UNION ALL
		" : "
		"). ($additional_comments ? "
		SELECT 	
				O.EPOP_ID, 
				O.NAME, 
				O.TITLE,
				COMPLETED_BY,
				TO_CHAR(OBSERVED_DATE, 'MM/DD/YYYY') AS DT,
				'comments' AS ITEM_NUM,
				COMMENTS
		FROM 	EPOP_OBSERVATIONS O,
				T
		WHERE 	T.USID = NVL(O.EMPLOYEE, O.COMPLETED_BY )
		AND OBSERVED_DATE BETWEEN TO_DATE('{$start} 00:00:00', 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE('{$end} 23:59:59', 'MM/DD/YYYY HH24:MI:SS')
		AND COMMENTS IS NOT NULL
		/*
		UNION ALL
		SELECT 	
				O.EPOP_ID, 
				O.NAME, 
				O.TITLE,
				COMPLETED_BY,
				TO_CHAR(OBSERVED_DATE, 'MM/DD/YYYY') AS DT,
				'situation' AS ITEM_NUM,
				SITUATION
		FROM 	EPOP_OBSERVATIONS O,
				T
		WHERE 	T.USID = NVL(O.EMPLOYEE, O.COMPLETED_BY )
		AND OBSERVED_DATE BETWEEN TO_DATE('{$start} 00:00:00', 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE('{$end} 23:59:59', 'MM/DD/YYYY HH24:MI:SS')
		AND SITUATION IS NOT NULL
		UNION ALL
		SELECT 	
				O.EPOP_ID, 
				O.NAME, 
				O.TITLE,
				COMPLETED_BY,
				TO_CHAR(OBSERVED_DATE, 'MM/DD/YYYY') AS DT,
				'actions' AS ITEM_NUM,
				ACTIONS
		FROM 	EPOP_OBSERVATIONS O,
				T
		WHERE 	T.USID = NVL(O.EMPLOYEE, O.COMPLETED_BY )
		AND OBSERVED_DATE BETWEEN TO_DATE('{$start} 00:00:00', 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE('{$end} 23:59:59', 'MM/DD/YYYY HH24:MI:SS')
		AND ACTIONS IS NOT NULL
		UNION ALL
		SELECT 	
				O.EPOP_ID, 
				O.NAME, 
				O.TITLE,
				COMPLETED_BY,
				TO_CHAR(OBSERVED_DATE, 'MM/DD/YYYY') AS DT,
				'results' AS ITEM_NUM,
				RESULTS
		FROM 	EPOP_OBSERVATIONS O,
				T
		WHERE 	T.USID = NVL(O.EMPLOYEE, O.COMPLETED_BY )
		AND OBSERVED_DATE BETWEEN TO_DATE('{$start} 00:00:00', 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE('{$end} 23:59:59', 'MM/DD/YYYY HH24:MI:SS')
		AND RESULTS IS NOT NULL
		*/
		" : "
	") . "
) O 
LEFT JOIN EMPLOYEES E
	ON E.USID = O.COMPLETED_BY
";
$x = 0;

echo "<div style='width: 1252px; text-align: right;'>";
echo "<input type = 'submit' style = \"height: 19px; width: 100px;\" value = 'Excel' onclick='excel(true)'/>";
echo "</div>";

echo "<div id='exportArea'>";
echo "<table class='tbl' id='tbl'>
	<tr>
		<th>
			<div class='inner' style='width: 120px;'>
				Employee Observed
			</div>
		</th>
		<th>
			<div class='inner' style='width: 200px;'>
				Title
			</div>
		</th>
		<th>
			<div class='inner' style='width: 120px;'>
				Observed By
			</div>
		</th>
		<th>
			<div class='inner' style='width: 100px;'>
				Date Observed
			</div>
		</th>
		<th>
			<div class='inner' style='width: 300px;'>
				Observation
			</div>
		</th>
		<th>
			<div class='inner' style='width: 300px;'>
				Comments
			</div>
		</th>			
		<th>
			<div class='inner' style='width: 50px;'>
			</div>
		</th>
	</tr>
";

 //echo "<pre>{$sql}</pre>";

while($row = $oci->fetch($sql)){
	echo "
		<tr class = '" . ($x++ % 2 == 0 ? 'even' : 'odd'). "'>
			<td style = 'text-align: left; width: 120px;'>{$row["NAME"]}</td>
			<td style = 'text-align: left; width: 200px;'>{$row["TITLE"]}</td>
			<td style = 'text-align: left; width: 120px;'>{$row["COMPLETED_BY"]}</td>
			<td style = 'text-align: center; width: 100px;'>{$row["DT"]}</td>
			<td style = 'text-align: center; width: 300px; text-align: left;'>
				<div style='width: 300px; max-height: 50px; overflow:auto;'>
					" . strip_tags($items[$row["ITEM_NUM"]]) ."
				</div>
			</td>
			<td style = 'text-align: center; width: 300px; text-align: left;'>
				<div style='width: 300px; max-height: 50px; overflow:auto;'>
					" . (str_replace("\\", "", $row["COMMENTS"])) ."
				</div>
			</td>
			<td style = 'text-align: center; width: 50xp; font-size: 10px;'>[ <a href = '#'; onclick = 'sot.swo.view({$row["EPOP_ID"]}); return false;'>View</a> ]</td>
		</tr>
	";

}

if($x == 0){
	echo "<tr>
				<td colspan='7' style='width: 1150px;'>
					No comments found with the current filters.
				</td>
			</tr>
		";
}

echo "
</table>
</div>
<div id='temp_clipboard'></div>
";



?>