<?php
require_once("../../src/php/require.php");

$oci = new mcl_Oci("soteria");
$sql = "
	SELECT 	HP_ID,
			TO_CHAR(INCIDENT_DATE, 'MM/DD/YYYY') AS DT 
	FROM 	HP_OBSERVATIONS O 
	WHERE 	COMPLETED_BY = '{$usid}'
			AND INCIDENT_DATE BETWEEN TO_DATE('{$start} 00:00:00', 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE('{$end} 23:59:59', 'MM/DD/YYYY HH24:MI:SS')
	ORDER BY INCIDENT_DATE DESC
";



$x = 0;
while($row = $oci->fetch($sql)){
	$tbl .= "
		<tr class = '" . ($x++ % 2 == 0 ? 'even' : 'odd'). "'>
			<td style = 'text-align: center; width: 100px;'>{$row["DT"]}</td>
			<td style = 'text-align: center; width: 50xp;'><a href = '#' onclick = 'sot.hp.view({$row["HP_ID"]}); return false;'>View</a></td>	
		</tr>
	";
}

if($x == 0){
	$tbl = "<tr>
				<td colspan='2' style='width: 250px;'>
					No observations found for the current time frame ({$start} - {$end}).
				</td>
			</tr>
		";
}

echo "
<div style = 'float: left;'>
	<table class='tbl' style=''>
		<tr>
			<th colspan='2'>
				<div class='inner_title'>
					My Observations
				</div>
			</th>
		</tr>
		<tr>
			<th>
				<div class='inner' style='width: 200px;'>
					Incident Date
				</div>
			</th>
			<th>
				<div class='inner' style='width: 50px;'>
				</div>
			</th>
		</tr>
		{$tbl}
	</table>
</div>
<div style = 'float: right;'>
	<form method = 'GET' action='hpscompleted.php' style = 'overflow: hidden; width: 510px; float: right; padding: 5px;'>
		<table style = 'font-size: 12px; font-weight: bold'>
			<!--<tr>
				<td>From</td>
				<td>To</td>
			</tr>-->
			<tr style = 'vertical-align: bottom;'>
				<td>
					<input style = 'height: 12px; width: 100px;' type = 'text' name = 'start' id = 'start' value = '{$start}'/> <img style = 'vertical-align: bottom;' src='../../src/img/calendar.gif' alt='' id='tcal' onmouseover='setup_cal(\"tcal\", \"start\");' />
				</td>
				<td>
					<input style = 'height: 12px; width: 100px;'  type = 'text' name = 'end' id = 'end' value = '{$end}' /> <img style = 'vertical-align: bottom;' src='../../src/img/calendar.gif' alt='' id='tcal2' onmouseover='setup_cal(\"tcal2\", \"end\");' />
				</td>
				<td style = 'text-align: right;'>
					" . (!empty($_GET["delegate"]) ? "<input type='hidden' name='delegate' value='{$_GET["delegate"]}'/>" : "") . "
					<input type = 'submit' style = \"height: 19px;\" value = 'Filter'/>
					<input onclick = \"dojo.byId('start').value = '{$startWeek}'; dojo.byId('end').value = '{$endWeek}';\" type = \"submit\" style = \"height: 19px;\" value = \"Current Week\"/>
					<input onclick = \"dojo.byId('start').value = '{$startWeekPast}'; dojo.byId('end').value = '{$endWeekPast}';\"  type = \"submit\" style = \"height: 19px;\" value = \"Past Week\"/>
				</td>
			</tr>
		</table>
	</form>
</div>
";

$sql = "
	SELECT 	HP_ID, 
			O.NAME, 
			O.TITLE,
			TO_CHAR(INCIDENT_DATE, 'MM/DD/YYYY') AS DT,
			NVL(E.NAME, O.COMPLETED_BY) AS COMPLETED_BY
	FROM 	HP_OBSERVATIONS O 
	LEFT JOIN EMPLOYEES E
		ON E.USID = O.COMPLETED_BY
	WHERE 	COMPLETED_BY != '{$usid}'
			AND OBSERVATION_CREDIT = '{$usid}'
			AND INCIDENT_DATE BETWEEN TO_DATE('{$start} 00:00:00', 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE('{$end} 23:59:59', 'MM/DD/YYYY HH24:MI:SS')
	ORDER BY INCIDENT_DATE DESC
";


$x = 0;
$tbl = '';
while($row = $oci->fetch($sql)){
	$tbl .= "
		<tr class = '" . ($x++ % 2 == 0 ? 'even' : 'odd'). "'>
			<td style = 'text-align: center; width: 100px;'>{$row["DT"]}</td>
			<td style = 'text-align: center; width: 100px;'>{$row["COMPLETED_BY"]}</td>
			<td style = 'text-align: center; width: 50xp;'><a href = '#' onclick = 'sot.hp.view({$row["HP_ID"]}); return false;'>View</a></td>	
		</tr>
	";
}

if($x == 0){
	$tbl = "<tr>
				<td colspan='3' style='text-align: center;'>
					No observations found for the current time frame ({$start} - {$end}).
				</td>
			</tr>
		";
}

echo "
<div style='clear: both;'>
	<table class='tbl' style='float: left;width:250px'>
		<tr>
			<th colspan='3'>
				<div class='inner_title'>
					Observations Completed By Delegates
				</div>
			</th>
		</tr>
		<tr>
			<th>
				<div class='inner' style='width: 100px;'>
					Date Observed
				</div>
			</th>
			<th>
				<div class='inner' style='width: 100px;'>
					Observed by
				</div>
			</th>
			<th>
				<div class='inner' style='width: 50px;'>
				</div>
			</th>
		</tr>
		{$tbl}
	</table>
</div>
";
?>