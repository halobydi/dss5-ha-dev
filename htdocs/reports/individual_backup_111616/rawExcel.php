<?php
	class Question {
		public $itemNum = "";
		public $parentCategory = "";
		public $itemCategory = "";
		public $subCategory = "";
		public $itemText = "";
		public $answer = array();

		//new Question($n,$p,$i,$s,$t);
		function __construct($iNum,$pCat,$iCat,$sCat,$text,$ans) {
			$this->itemNum = $iNum;
			$this->parentCategory = $pCat;
			$this->itemCategory = $iCat;
			$this->subCategory = $sCat;
			$this->itemText = $text;
			$this->answer = $ans;
		}
	}

	$answerKeys = array(
		"IO" => "Improvment Opportunity",
		"S"  => "Satisfactory",
		"NA" => "Not Applicable",
		""	 => "No Response"
	);

	function errorHandler($errno, $errstr, $errfile, $errline, $errcontext){
		//Todo: Should log somewhere..
		echo $errstr;
	}

	ini_set('memory_limit','2048M');
	ini_set('max_execution_time', 300);
	set_error_handler('errorHandler');

	require_once("mcl_Oci.php");
	require_once("../../src/php/auth.php");

	$oci = new mcl_Oci("soteria");

	//check login status
	$user = auth::check();
	if(!$user["status"]) {
		auth::deny();
	}
	$usid = (!empty($_GET["delegate"]) ? $user["delegates"][$_GET["delegate"]]["usid"] : $user["usid"]);

	// Time frame parameters
	$tm_frame = "";
	if($_GET["method"] == "week"){
		$tm_frame = "AND OBSERVED_DATE >= TRUNC(SYSDATE, 'IW')";
	} else if($_GET["method"] == "month"){
		$tm_frame = "AND OBSERVED_DATE >= TRUNC(SYSDATE, 'MONTH')";
	} else if($_GET["method"] == "year"){
		$tm_frame = "AND OBSERVED_DATE >= TRUNC(SYSDATE, 'YEAR')";
	} else if($_GET["method"] == "dates"){
		$tm_frame = "AND OBSERVED_DATE BETWEEN TO_DATE('{$_GET["start"]} 00:00:00', 'MM/DD/YYYY HH24:MI:SS') 
				AND TO_DATE('{$_GET["end"]} 23:59:59', 'MM/DD/YYYY HH24:MI:SS')";
	}

	// Employee observed vs employee parameters
	if(empty($_REQUEST["employee"])) {
		$employee = "CASE WHEN PP = 1 THEN O.OBSERVATION_CREDIT ELSE NVL(O.EMPLOYEE, O.OBSERVATION_CREDIT ) END";
	} else {
		$employee = "O.{$_REQUEST["employee"]}";
	}

	// All Active Questions & their sub-questions
	$parentCategory = "PARENT_CATEGORY";
	$itemCategory = "ITEM_CATEGORY";
	$itemNumber = "ITEM_NUM";
	$itemText = "ITEM_TEXT";

	$sql_AllQuestions = "
				SELECT 
					{$itemCategory} AS {$parentCategory},
					{$itemCategory}, 
					{$itemText}, 
					{$itemNumber} 
				FROM 
					EPOP_ITEMS 
				
				UNION ALL
				
				SELECT 
					I.ITEM_CATEGORY AS PARENT_CATEGORY,
					ITEM_TEXT, 
					SUBITEM_TEXT, 
					SUBITEM_NUM 
				FROM 
					SWO_SUBITEMS SU, 
					EPOP_ITEMS I 
				WHERE 
					I.ITEM_NUM = SU.PARENT_ITEM_NUM
		";

	$questions = array();
	$strongQuestions = array();

	while ($row = $oci->fetch($sql_AllQuestions)){
		// Flatten to (ItemNum, Row)
		// Row = ParentCat,ItemCat,ItemText,ItemNum
		$questions[$row[$itemNumber]] = $row;
	}

	foreach ($questions as $key => $value){
		$category = $value[$itemCategory];
		$isSubQuestion = false;

		$n = $value[$itemNumber];
		$p = $value[$parentCategory];
		$t = $value[$itemText];

		// Look for the parent question
		foreach ($questions as $k => $v){
			// Now $v  is the parent question
			if ($v[$itemText] == $category){
				// Giving the sub-question the proper Item Category
				$i = $v[$itemCategory];
				// And making the Item Category the Sub-Item Category
				$s = $value[$itemCategory];
				$newQuestion = new Question($n,$p,$i,$s,$t, array());
				array_push($strongQuestions,$newQuestion);
				// We've found the parent question, so move on...
				$isSubQuestion = true;
				break;
			}
		}

		// If it is a main question
		if (!$isSubQuestion) {
			$i = $value[$itemCategory];
			// There is no "sub-item category" for a parent question
			$s = "";
			$newQuestion = new Question($n,$p,$i,$s,$t, array());
			array_push($strongQuestions,$newQuestion);
		}
	}

	$orderedQuestions = array();
	foreach ($strongQuestions as $k => $v){
		$orderedQuestions[$v->itemNum] = $v;
	}
	ksort($orderedQuestions);

	$lit_obsId    = "Observation ID";
	$lit_empObs   = "Employee Observed";
	$lit_name     = "Name";
	$lit_org      = "Organization";
	$lit_title    = "Title";
	$lit_loc      = "Location";
	$lit_dateObs  = "Date Observed";
	$lit_obsBy    = "Observed By";
	$lit_obsByLoc = "Observed By Employee Location";
	$lit_obsBySup = "Observed By Supervisor";
	$lit_itmNum   = "Item Number";
	$lit_obs	  = "Observation";
	$lit_answer   = "Answer";
	$lit_comments = "Comments";
	$lit_conv     = "Conversation Took Place";
	$lit_behv     = "Recognized Good Behavior";
	$lit_pWork    = "Procedure for Work";
	$lit_pWorkDet = "Procedure for Work Details";
	$lit_lifeCrit = "Life Critical";
	$lit_lcVeh    = $lit_lifeCrit . " Vehicle";
	$lit_lcHeight = $lit_lifeCrit . " Heights";
	$lit_lcLift   = $lit_lifeCrit . " Lifting";
	$lit_lcTrench = $lit_lifeCrit . " Trenching";
	$lit_lcConf   = $lit_lifeCrit . " Confined";
	$lit_lcHot    = $lit_lifeCrit . " Hot Work";
	$lit_lcHaz    = $lit_lifeCrit . " Hazard Energy";
	$lit_oOrF     = "Office or Field";

	$sql = "
			WITH T AS(
				SELECT * FROM EMPLOYEES WHERE PATH LIKE '%{$usid}%'
			)
			SELECT
				O.EPOP_ID  	    					 AS \"{$lit_obsId}\",
				O.EMPLOYEE                           AS \"{$lit_empObs}\",
				O.NAME                               AS \"{$lit_name}\",
				O.ORG_CODE 							 AS \"{$lit_org}\",
				O.TITLE    						     AS \"{$lit_title}\",
				O.LOCATION 							 AS \"{$lit_loc}\",
				TO_CHAR(OBSERVED_DATE, 'MM/DD/YYYY') AS \"{$lit_dateObs}\",
				NVL(E2.NAME, O.OBSERVATION_CREDIT)   AS \"{$lit_obsBy}\",
				E2.LOCATION 				         AS \"{$lit_obsByLoc}\",
				E3.NAME 							 AS \"{$lit_obsBySup}\",
				O.ITEM_NUM 							 AS \"{$lit_obs}\",
				O.ANSWER							 AS \"{$lit_answer}\",
				C.COMMENTS 							 AS \"{$lit_comments}\",
				O.CONVERSATION 						 AS \"{$lit_conv}\",
				O.GOOD_BEHAVIOR 					 AS \"{$lit_behv}\",
				O.PROCEDURE_FOR_WORK 				 AS \"{$lit_pWork}\",
				O.PROCEDURE_FOR_WORK_DETAIL          AS \"{$lit_pWorkDet}\",
				O.LC_VEHICLE 						 AS \"{$lit_lcVeh}\",
				O.LC_HEIGHTS 						 AS \"{$lit_lcHeight}\",
				O.LC_LIFTING 						 AS \"{$lit_lcLift}\",
				O.LC_TRENCHING 						 AS \"{$lit_lcTrench}\",
				O.LC_CONFINED						 AS \"{$lit_lcConf}\",
				O.LC_HOT_WORK						 AS \"{$lit_lcHot}\",
				O.LC_HAZ_ENERGY 					 AS \"{$lit_lcHaz}\",
				O.OFFICE_FIELD 						 AS \"{$lit_oOrF}\",
				IT.ITEM_TEXT 					     AS \"ItemText\",
				IT.ITEM_CATEGORY                     AS \"ItemCategory\"
			FROM(
				SELECT 
					EMPLOYEE, 
					O.NAME, 
					O.TITLE, 
					O.LOCATION,
					O.ORG_CODE,
					OBSERVED_DATE,
					O.OBSERVATION_CREDIT, 
					O.EPOP_ID,
					A.ITEM_NUM, 
					A.ANSWER ,
					O.OFFICE_FIELD,
					
					DECODE(O.PROCEDURE_FOR_WORK, 1, 'Yes', 'No') AS PROCEDURE_FOR_WORK,
					O.PROCEDURE_FOR_WORK_DETAIL,
					DECODE(O.LC_VEHICLE, 1, 'Yes', 'No') AS LC_VEHICLE,
					DECODE(O.LC_HEIGHTS, 1, 'Yes', 'No') AS LC_HEIGHTS,
					DECODE(O.LC_LIFTING, 1, 'Yes', 'No') AS LC_LIFTING,
					DECODE(O.LC_TRENCHING, 1, 'Yes', 'No') AS LC_TRENCHING,
					DECODE(O.LC_CONFINED, 1, 'Yes', 'No') AS LC_CONFINED,
					DECODE(O.LC_HOT_WORK, 1, 'Yes', 'No') AS LC_HOT_WORK,
					DECODE(O.LC_HAZ_ENERGY, 1, 'Yes', 'No') AS LC_HAZ_ENERGY,
			
					CASE WHEN COMPLETED_DATE < TO_DATE('08/30/2014', 'MM/DD/YYYY') AND CONVERSATION_TOOK_PLACE = 0 THEN 'NA' ELSE DECODE(CONVERSATION_TOOK_PLACE, 1, 'Yes', 'No') END AS CONVERSATION,
					CASE WHEN COMPLETED_DATE < TO_DATE('08/30/2014', 'MM/DD/YYYY') AND RECOGNIZED_GOOD_BEHAVIOR = 0 THEN 'NA' ELSE DECODE(RECOGNIZED_GOOD_BEHAVIOR, 1, 'Yes', 'No') END AS GOOD_BEHAVIOR
			
				FROM EPOP_OBSERVATIONS O, 
					EPOP_ANSWERS A, T
				WHERE (T.USID = {$employee})
					AND A.EPOP_ID = O.EPOP_ID
					{$tm_frame}
					AND SWO = 1
			) O
		
			LEFT JOIN EPOP_COMMENTS C
				ON C.EPOP_ID = O.EPOP_ID
				AND C.ITEM_NUM = O.ITEM_NUM
			LEFT JOIN EMPLOYEES E2
				ON E2.USID = O.OBSERVATION_CREDIT
			LEFT JOIN EMPLOYEES E3
				ON E3.USID = E2.SUPERVISOR
			LEFT JOIN EPOP_ITEMS IT
				ON IT.ITEM_NUM = O.ITEM_NUM
		";

	$sql_observations = "
			WITH T AS(
				SELECT * FROM EMPLOYEES WHERE PATH LIKE '%{$usid}%'
			)
			SELECT DISTINCT
				O.EPOP_ID  	    					 AS \"{$lit_obsId}\",
				O.EMPLOYEE                           AS \"{$lit_empObs}\",
				O.NAME                               AS \"{$lit_name}\",
				O.ORG_CODE 							 AS \"{$lit_org}\",
				O.TITLE    						     AS \"{$lit_title}\",
				O.LOCATION 							 AS \"{$lit_loc}\",
				TO_CHAR(OBSERVED_DATE, 'MM/DD/YYYY') AS \"{$lit_dateObs}\",
				NVL(E2.NAME, O.OBSERVATION_CREDIT)   AS \"{$lit_obsBy}\",
				E2.LOCATION 				         AS \"{$lit_obsByLoc}\",
				E3.NAME 							 AS \"{$lit_obsBySup}\",
				O.CONVERSATION 						 AS \"{$lit_conv}\",
				O.GOOD_BEHAVIOR 					 AS \"{$lit_behv}\",
				O.LC_VEHICLE 						 AS \"{$lit_lcVeh}\",
				O.LC_HEIGHTS 						 AS \"{$lit_lcHeight}\",
				O.LC_LIFTING 						 AS \"{$lit_lcLift}\",
				O.LC_TRENCHING 						 AS \"{$lit_lcTrench}\",
				O.LC_CONFINED						 AS \"{$lit_lcConf}\",
				O.LC_HOT_WORK						 AS \"{$lit_lcHot}\",
				O.LC_HAZ_ENERGY 					 AS \"{$lit_lcHaz}\",
				O.OFFICE_FIELD 						 AS \"{$lit_oOrF}\"			
			FROM(
				SELECT 
					EMPLOYEE, 
					O.NAME, 
					O.TITLE, 
					O.LOCATION,
					O.ORG_CODE,
					OBSERVED_DATE,
					O.OBSERVATION_CREDIT, 
					O.EPOP_ID,
					O.OFFICE_FIELD,
					
					DECODE(O.PROCEDURE_FOR_WORK, 1, 'Yes', 'No') AS PROCEDURE_FOR_WORK,
					O.PROCEDURE_FOR_WORK_DETAIL,
					DECODE(O.LC_VEHICLE, 1, 'Yes', 'No') AS LC_VEHICLE,
					DECODE(O.LC_HEIGHTS, 1, 'Yes', 'No') AS LC_HEIGHTS,
					DECODE(O.LC_LIFTING, 1, 'Yes', 'No') AS LC_LIFTING,
					DECODE(O.LC_TRENCHING, 1, 'Yes', 'No') AS LC_TRENCHING,
					DECODE(O.LC_CONFINED, 1, 'Yes', 'No') AS LC_CONFINED,
					DECODE(O.LC_HOT_WORK, 1, 'Yes', 'No') AS LC_HOT_WORK,
					DECODE(O.LC_HAZ_ENERGY, 1, 'Yes', 'No') AS LC_HAZ_ENERGY,
			
					CASE WHEN COMPLETED_DATE < TO_DATE('08/30/2014', 'MM/DD/YYYY') AND CONVERSATION_TOOK_PLACE = 0 THEN 'NA' ELSE DECODE(CONVERSATION_TOOK_PLACE, 1, 'Yes', 'No') END AS CONVERSATION,
					CASE WHEN COMPLETED_DATE < TO_DATE('08/30/2014', 'MM/DD/YYYY') AND RECOGNIZED_GOOD_BEHAVIOR = 0 THEN 'NA' ELSE DECODE(RECOGNIZED_GOOD_BEHAVIOR, 1, 'Yes', 'No') END AS GOOD_BEHAVIOR
			
				FROM EPOP_OBSERVATIONS O, 
					EPOP_ANSWERS A, T
				WHERE (T.USID = {$employee})
					AND A.EPOP_ID = O.EPOP_ID
					{$tm_frame}
					AND SWO = 1
			) O
			LEFT JOIN EMPLOYEES E2
				ON E2.USID = O.OBSERVATION_CREDIT
			LEFT JOIN EMPLOYEES E3
				ON E3.USID = E2.SUPERVISOR
		";

	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Cache-Control: private",false);
	header("Content-Type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"SOTeria_Export_" . time() . ".csv\";" );
	header("Content-Transfer-Encoding: binary");

	function removeQuotes($str){
		$str = strip_tags($str);
		return str_replace('"', ' ', $str);
	}

	function writeCSV($row){
		// Removes unnecessary columns
		unset ($row[10],$row[11],$row[12],$row[15],$row[16],$row[25],$row[26]);
		$row = array_map("removeQuotes",$row);
		if ($row[1] == ""){
			$row[1] = "Anonymous";
		}
		$csvRow = implode("\",\"",$row);
		$csvRow = "\"" . $csvRow . "\"\n";
		$csvRow = str_replace("-"," ",$csvRow);
		echo $csvRow;
	}

	// Create header row
	function makeDetailedHeaders($qs){
		$headers = array();
		foreach ($qs as $itemID => $question){
			$header = "";
			if ($question->subCategory == ""){
				$header = $question->itemCategory . " / " . $question->itemText;
			} else {
				$header = $question->itemCategory . " / " . $question->subCategory . " / " . $question->itemText;
			}
			$item = array();
			$item["IDEN"] = $itemID;
			$item["TEXT"] = $header;
			$headers[ ] = $item;
		}
		return $headers;
	}
	$questionTitles = makeDetailedHeaders($orderedQuestions);

	$sql_answers = $oci->parse("
			SELECT 	A.ITEM_NUM,
					A.ANSWER
			FROM 	EPOP_ANSWERS A
			WHERE 	A.EPOP_ID = :epopId
		");

	$firstPass = true;

	function printBasicHeader($headerData){
		$row = array_keys($headerData);
		$row = implode("\",\"",$row);
		$row = "\"" . $row . "\"";
		$row = str_replace("-"," ",$row);
		echo $row . ",";
	}

	function printQuestionsHeader($questionsData){
		$prettyHeader = array();
		foreach	($questionsData as $value){
			if (is_array($value)){
				$prettyHeader[ ] = $value["TEXT"];
			} else {
				$prettyHeader[ ] = $value;
			}
		}
		$row = array_map("removeQuotes",$prettyHeader);
		$csvRow = implode("\",\"",$row);
		$csvRow = "\"" . $csvRow . "\"";
		$csvRow = str_replace("-"," ",$csvRow);
		echo $csvRow;
	}

	function printHeader($basicInfo, $questionTitles){
		printBasicHeader($basicInfo);
		printQuestionsHeader($questionTitles);
	}

	function printLine($oci, $sql_answers, $questionTitles, $answerKeys, $observation){
		// Formatting the basic info section \\
		$info = array_map("removeQuotes",$observation);
		if ($info["Employee Observed"] == ""){
			$info["Employee Observed"] = "Anonymous";
		}
		$info = implode("\",\"",$info);
		$info = "\"" . $info . "\"";
		$info = str_replace("-"," ",$info);
		echo $info;
		// Get all the questions for this obs \\
		$oci->bind($sql_answers, array(
			":epopId"	=> $observation["Observation ID"]
		));
		$answersArray = array();
		while($answer = $oci->fetch($sql_answers, false)){
			$answersArray[$answer["ITEM_NUM"]] = $answer["ANSWER"];
		}

		// Create question info columns \\
		for ($i = 0; $i < count($questionTitles); $i++){
			$questionIden = $questionTitles[$i]["IDEN"];
			$answer = "";

			if (array_key_exists($questionIden, $answersArray)){
				$answer = $answerKeys[$answersArray[$questionIden]];
			} else {
				$answer = "No Response";
			}

			echo "," . $answer;
		}
	}

	while ($observation = $oci->fetch($sql_observations)){
		if ($firstPass){
			printHeader($observation, $questionTitles);
			$firstPass = false;
		} else {
			printLine($oci, $sql_answers, $questionTitles, $answerKeys, $observation);
		}
		echo "\n";
	}
?>