<?php
require_once("../../src/php/require.php");
mcl_Html::js(mcl_Html::HIGHSTOCK);
mcl_Html::s(mcl_Html::INC_JS, "../../src/js/stormduty.js");

$oci = new mcl_Oci("soteria");


mcl_Html::s(mcl_Html::SRC_CSS, "
	table.sortable {
		width:				100%;
		
	}
	tr.even {
		background-color:	#f0f0f0;
	}
	tr.odd {
		background-color:	#d8d8d8;
	}
	table.sortable td {
		padding:		5px;
	}
");

if(isset($_GET["r"])){
	echo "<div class = '" . ($_GET["r"] == "1" ? "success" : ($_GET["r"] == "0" ? "error" : "")) . "' style='margin-top: 5px; margin-left: 5px;'>
		" . ($_GET["r"] == "1" ? "Successfully saved Red Tag Audit." : ($_GET["r"] == "0" ? "Unable to save Red Tag Audit." : "")) . "
	</div>";
} else if(isset($_GET["nm"])){
	echo "<div class = '" . ($_GET["nm"] == "1" ? "success" : ($_GET["nm"] == "0" ? "error" : "")) . "' style='margin-top: 5px; margin-left: 5px;'>
		" . ($_GET["nm"] == "1" ? "Successfully saved Near Miss Incident." : ($_GET["nm"] == "0" ? "Unable to save Near Miss Incident." : "")) . "
	</div>";
} else if(isset($_GET["gc"])){
	echo "<div class = '" . ($_GET["gc"] == "1" ? "success" : ($_GET["gc"] == "0" ? "error" : "")) . "' style='margin-top: 5px; margin-left: 5px;'>
		" . ($_GET["gc"] == "1" ? "Successfully saved Good Catch." : ($_GET["sd"] == "0" ? "Unable to save Good Catch." : "")) . "
	</div>";
} else if(isset($_GET["sd"])){
	echo "<div class = '" . ($_GET["gc"] == "1" ? "success" : ($_GET["gc"] == "0" ? "error" : "")) . "' style='margin-top: 5px; margin-left: 5px;'>
		" . ($_GET["sd"] == "1" ? "Successfully saved Storm Duty Observation." : ($_GET["sd"] == "0" ? "Unable to save Storm Duty Observation." : "")) . "
	</div>";
}

if(empty($_GET["start"]) && empty($_GET["end"])) {
	$start = date("m") . "/01/" . date("Y");
	$end = date("m/t/Y");
}

mcl_Html::s(mcl_Html::SRC_JS, "
	var current = null;
		var toggle = function(form, hidePrevious) {
			
			if(hidePrevious && current && current != form) {
				dojo.byId(current).style.display = 'none';
				dojo.byId(current + '_toggle').innerHTML = '<img src=\"../../src/img/expand.png\"/>';
			}
			
			if(dojo.byId(form).style.display == 'block') {
				dojo.byId(form).style.display = 'none';
				dojo.byId(form + '_toggle').innerHTML = '<img src=\"../../src/img/expand.png\"/>';	
			} else {
				dojo.byId(form).style.display = 'block';
				dojo.byId(form + '_toggle').innerHTML = '<img src=\"../../src/img/collapse.png\"/>';
			}
			current = form;
		
		};
");
echo "<div>
	<form method = 'GET' action='activity.php' style = 'overflow: hidden; width: 510px; padding: 5px;'>
		<table style = 'font-size: 12px;'>
			<tr><td colspan='3' style='font-size: 10px; font-weight: normal;'>Filter Activity by Date Submitted</td></tr>
			<tr style = 'vertical-align: bottom;'>
				<td>
					<input style = 'height: 12px; border: 1px solid #000; width: 100px;' type = 'text' name = 'start' id = 'start' value = '{$start}'/> <img style = 'vertical-align: bottom;' src='../../src/img/calendar.gif' alt='' id='tcal' onmouseover='setup_cal(\"tcal\", \"start\");' />
				</td>
				<td>
					<input style = 'height: 12px; border: 1px solid #000; width: 100px;'  type = 'text' name = 'end' id = 'end' value = '{$end}' /> <img style = 'vertical-align: bottom;' src='../../src/img/calendar.gif' alt='' id='tcal2' onmouseover='setup_cal(\"tcal2\", \"end\");' />
				</td>
				<td style = 'text-align: right;'>
					" . (!empty($_GET["delegate"]) ? "<input type='hidden' name='delegate' value='{$_GET["delegate"]}'/>" : "") . "
					<input type = 'submit' style = \"height: 19px;\" value = 'Filter'/>
					<input onclick = \"dojo.byId('start').value = '{$startWeek}'; dojo.byId('end').value = '{$endWeek}';\" type = \"submit\" style = \"height: 19px;\" value = \"Current Week\"/>
					<input onclick = \"dojo.byId('start').value = '{$startWeekPast}'; dojo.byId('end').value = '{$endWeekPast}';\"  type = \"submit\" style = \"height: 19px;\" value = \"Past Week\"/>
				</td>
			</tr>
		</table>
	</form>
</div>
";

$activity = array();
$sql = <<<SQL
	SELECT 	EPOP_ID AS "ID", 
			NAME AS "Employee Observed", 
			TO_CHAR(OBSERVED_DATE, 'MM/DD/YYYY') AS "Date Observed",
			CASE WHEN SWO = 1 THEN 1 ELSE NULL END AS "Safe Worker",
			CASE WHEN PP = 1 THEN 1 ELSE NULL END AS "Paired Performance",
			CASE WHEN QEW = 1 THEN 1 ELSE NULL END AS "Qualified Electrical Worker",
			CASE WHEN OBSERVATION_CREDIT = COMPLETED_BY THEN NULL ELSE 1 END AS "Completed by Delegate?"
	FROM 	EPOP_OBSERVATIONS O 
	WHERE 	(OBSERVATION_CREDIT = '{$usid}' OR COMPLETED_BY = '{$usid}')
			AND COMPLETED_DATE BETWEEN TO_DATE('{$start} 00:00:00', 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE('{$end} 23:59:59', 'MM/DD/YYYY HH24:MI:SS')
	ORDER BY EPOP_ID DESC
SQL;

while($row = $oci->fetch($sql)) {
	$activity["epop"][] = $row;
}

$sql = <<<SQL
	SELECT 	RTA_ID AS "ID",
			R.LOCATION AS "Location",
			TO_CHAR(TO_DATE(CREDIT_MONTH, 'MM'), 'Month') AS "Credit Month",
			TO_CHAR(AUDIT_DATE, 'MM/DD/YYYY') AS "Audit Date",
			NVL(E.NAME, COMPLETED_BY) AS "Completed By",
			COMPLETE AS "Complete?"
	FROM	RED_TAG_AUDITS R
	LEFT JOIN EMPLOYEES E
		ON E.USID = R.COMPLETED_BY
	WHERE	COMPLETED_BY IN (
		SELECT USID FROM EMPLOYEES WHERE PATH LIKE '%{$usid}%' UNION ALL SELECT '{$usid}' FROM DUAL
	)
	AND COMPLETED_DATE BETWEEN TO_DATE('{$start} 00:00:00', 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE('{$end} 23:59:59', 'MM/DD/YYYY HH24:MI:SS')
	ORDER BY RTA_ID DESC					
SQL;

while($row = $oci->fetch($sql)) {
	$activity["rta"][] = $row;
}

$sql = <<<SQL
	SELECT	NM_ID AS "ID", 
			NVL(E.NAME, COMPLETED_BY) AS "Completed By",
			TO_CHAR(INCIDENT_DATE, 'MM/DD/YYYY') AS "Incident Date",
			CASE WHEN REQUIRES_INVESTIGATION = 0 THEN NULL ELSE DECODE(TRIM(INVESTIGATION), NULL, INVESTIGATION_COMPLETE, 1) END AS "Investigation Complete?",
			NVL(S.NAME, COMPLETED_BY_SUPERVISOR) AS "Completed by Supervisor",
			CASE WHEN SUPERVISOR_COMMENTS IS NULL THEN 0 ELSE 1 END AS "Supervisor Comments Entered?",
			NVL(D.NAME, DIRECTOR) AS "Director",
			CASE WHEN DIRECTOR_COMMENTS IS NULL THEN 0 ELSE 1 END AS "Director Comments Entered?",
			C.CAT_TEXT AS "Category",
			P.PLANT AS "Good Catch DTE Location",
			LOCATION_DESCRIPTION AS "Location Description",
			CASE WHEN PRIORITY = 'H' THEN 'High' WHEN PRIORITY = 'M' THEN 'Medium' WHEN PRIORITY = 'L' THEN 'Low' ELSE NULL END AS "Priority"
	FROM	NEAR_MISS_OBSERVATIONS N
	LEFT JOIN EMPLOYEES E ON E.USID = N.COMPLETED_BY
	LEFT JOIN EMPLOYEES S ON S.USID = N.COMPLETED_BY_SUPERVISOR
	LEFT JOIN EMPLOYEES D ON D.USID = N.DIRECTOR
	LEFT JOIN NEAR_MISS_CATEGORIES C ON C.CAT_ID = N.CATEGORY
	LEFT JOIN NEAR_MISS_ACCIDENT_LOCATIONS AL ON AL.AL_ID = N.LOCATION
	LEFT JOIN NEAR_MISS_PLANTS P ON P.PLANT_ID = N.PLANT
	WHERE	(
				COMPLETED_BY IN (SELECT USID FROM EMPLOYEES WHERE PATH LIKE '%{$usid}%' UNION ALL SELECT '{$usid}' FROM DUAL)
				OR COMPLETED_BY_SUPERVISOR = '{$usid}'
				OR DIRECTOR = '{$usid}'	
				OR EDIT_USID = '{$usid}'
			)
			AND COMPLETED_DATE BETWEEN TO_DATE('{$start} 00:00:00', 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE('{$end} 23:59:59', 'MM/DD/YYYY HH24:MI:SS')
			ORDER BY NM_ID DESC
SQL;

while($row = $oci->fetch($sql)) {
	$activity["nm"][] = $row;
}

$sql = <<<SQL
	SELECT	GC_ID AS "ID", 
			NVL(E.NAME, COMPLETED_BY) AS "Completed By",
			TO_CHAR(INCIDENT_DATE, 'MM/DD/YYYY') AS "Good Catch Date",
			DECODE(CLOSED, 1, 'Yes', 'No') AS "Good Catch Closed?",
			NVL(S.NAME, COMPLETED_BY_SUPERVISOR) AS "Completed by Supervisor",
			NVL(D.NAME, DIRECTOR) AS "Completed By Director",
			C.CAT_TEXT AS "Category",
			P.PLANT AS "Good Catch DTE Location",
			LOCATION_DESCRIPTION AS "Location Description"
	FROM	GOOD_CATCH_OBSERVATIONS G
	LEFT JOIN EMPLOYEES E ON E.USID = G.COMPLETED_BY
	LEFT JOIN EMPLOYEES S ON S.USID = G.COMPLETED_BY_SUPERVISOR
	LEFT JOIN EMPLOYEES D ON D.USID = G.DIRECTOR
	LEFT JOIN NEAR_MISS_CATEGORIES C ON C.CAT_ID = G.CATEGORY
	LEFT JOIN NEAR_MISS_ACCIDENT_LOCATIONS AL ON AL.AL_ID = G.LOCATION
	LEFT JOIN NEAR_MISS_PLANTS P ON P.PLANT_ID = G.PLANT
	WHERE	(
				COMPLETED_BY IN (SELECT USID FROM EMPLOYEES WHERE PATH LIKE '%{$usid}%' UNION ALL SELECT '{$usid}' FROM DUAL) OR
				COMPLETED_BY_SUPERVISOR = '{$usid}' OR
				DIRECTOR = '{$usid}'	
				OR PERSON_RECOGNIZED = '{$usid}'
			)
			AND COMPLETED_DATE BETWEEN TO_DATE('{$start} 00:00:00', 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE('{$end} 23:59:59', 'MM/DD/YYYY HH24:MI:SS')
			ORDER BY GC_ID DESC
SQL
	;
	
while($row = $oci->fetch($sql)) {
	$activity["gc"][] = $row;
}

$sql = <<<SQL
	SELECT	HP_ID AS "ID", 
			COMPLETED_BY AS "Completed By",
			TO_CHAR(INCIDENT_DATE, 'MM/DD/YYYY') AS "Incident Date",
			EVENT_TYPE AS "Event Type",
			NVL(S.NAME, COMPLETED_BY_SUPERVISOR) AS "Completed by Supervisor",
			NVL(D.NAME, COMPLETED_BY_DIRECTOR) AS "Completed by Director",
			DECODE(COMPLETED, 'Y', 1, 0) AS "Complete?"
	FROM	HP_OBSERVATIONS N
	LEFT JOIN EMPLOYEES E ON E.USID = N.COMPLETED_BY
	LEFT JOIN EMPLOYEES S ON S.USID = N.COMPLETED_BY_SUPERVISOR
	LEFT JOIN EMPLOYEES D ON D.USID = N.COMPLETED_BY_DIRECTOR
	WHERE	(
				COMPLETED_BY IN (SELECT USID FROM EMPLOYEES WHERE PATH LIKE '%{$usid}%' UNION ALL SELECT '{$usid}' FROM DUAL)
				OR COMPLETED_BY_SUPERVISOR = '{$usid}'
				OR COMPLETED_BY_DIRECTOR = '{$usid}'	
			)
			AND COMPLETED_DATE BETWEEN TO_DATE('{$start} 00:00:00', 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE('{$end} 23:59:59', 'MM/DD/YYYY HH24:MI:SS')
			ORDER BY HP_ID DESC
SQL
	;
while($row = $oci->fetch($sql)) {
	$activity["hp"][] = $row;
}

$sql = <<<SQL
	SELECT 	SD.SD_ID AS "ID",
			TO_CHAR(OBSERVED_DATE, 'MM/DD/YYYY') AS "Date Observed",
			OBSERVED_TIME AS "Time Observed",
			NVL(E.NAME, OBSERVED_BY) AS "Observed By",
			'#' || PUBLIC_SAFETY_TEAM_NUMBER AS "Publy Safety Team",
			SD.LOCATION AS "Location",
			ORG_TITLE AS "Organization"
	FROM	STORM_DUTY_OBSERVATIONS SD
	LEFT	JOIN EMPLOYEES E
			ON E.USID = OBSERVED_BY
	LEFT	JOIN ORGANIZATIONS ORG
			ON ORG.ORG_CODE = SD.ORG_CODE
	LEFT	JOIN STORM_DUTY_MEMBERS SDM
			ON SD.SD_ID = SDM.SD_ID
			AND SDM.MEMBER_USID = '{$usid}'
	WHERE 	SD.OBSERVED_DATE BETWEEN TO_DATE('{$start} 00:00:00', 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE('{$end} 00:00:00', 'MM/DD/YYYY HH24:MI:SS')
			AND (OBSERVED_BY = '{$usid}' OR SDM.MEMBER_USID = '{$usid}')
	ORDER	BY SD.SD_ID
SQL;

while($row = $oci->fetch($sql)) {
	$activity["sd"][] = $row;
}

$forms = array(
	"epop"	=> array("Enterprise Performance Observations", "Includes Safe Worker, Paired Performance, Qualified Electrical Worker"),
	"rta"	=> array("Red Tag Audits", "Includes all audits completed by you and your team (hierarchical)"),
	"nm"	=> array("Near Miss Incidents", "Includes all incidents completed by you and your team (hierarchical) or incidents assigned to you for investigation"),
	"gc"	=> array("Good Catch", "Includes all submissions completed by you and your team (hierarchical) or submissions recognized by you"),
	"hp"	=> array("Anatomy of an Event Reports ", "Includes all reports completed by you and your team (hierarchical)"),
	'sd' 	=> array('Storm Duty Observations', 'Includes all submissions observed by you (completed by you) or submissions that included you as a Public Safety Member')
);

$view = array(
	"epop"	=> "sot.swo.view(ID_PLACEHOLDER); return false;",
	"rta"	=> "sot.redtag.view(ID_PLACEHOLDER); return false;",
	"nm"	=> "sot.nm.view(ID_PLACEHOLDER); return false;",
	"gc"	=> "sot.gc.view(ID_PLACEHOLDER); return false;",
	"hp"	=> "sot.hp.view(ID_PLACEHOLDER); return false;",
	"sd"	=> "sot.stormduty.view(ID_PLACEHOLDER); return false;",
);

$edit = array(
	"rta"	=> "../../forms/redtag.php?id=ID_PLACEHOLDER",
	"nm"	=> "../../forms/nearmiss.php?nm_id=ID_PLACEHOLDER&mode=edit",
	"gc"	=> "../../forms/nearmiss.php?gc_id=ID_PLACEHOLDER&mode=edit&gc=true",
	"hp"	=> "../../forms/hp.php?hp_id=ID_PLACEHOLDER&mode=edit",
	"sd"	=> "../../forms/stormduty.php?sd_id=ID_PLACEHOLDER",
);

$delete = array(
	"rta"	=> "sot.redtag._delete(ID_PLACEHOLDER); return false;",
	"nm"	=> "sot.nm._delete(ID_PLACEHOLDER); return false",
	"gc"	=> "sot.gc._delete(ID_PLACEHOLDER); return false",
	"epop"	=> "sot.swo._delete(ID_PLACEHOLDER); return false;",
	"hp"	=> "sot.hp._delete(ID_PLACEHOLDER); return false;",
	"sd"	=> "sot.stormduty._delete(ID_PLACEHOLDER); return false;",
);

foreach($forms as $key=>$value) {
	echo "<div class='header'>";
	echo "<div class='caption' onclick='toggle(\"{$key}\", true);'>";
	echo "<span class='toggle' id='{$key}_toggle'><img src='../../src/img/expand.png'/></span>";
	echo "{$value[0]} <span style='font-size: 10px;'>{$value[1]}</span>";
	echo "</div>";
	echo "<div class='count'><div style='border-top: 1px solid #f0f0f0; border-left: 1px solid #f0f0f0; padding: 5px;'>" . count($activity[$key]) ."</div></div>";
	echo "</div>";
	echo "<div id='{$key}' style='display: none;' class='activity'>";
		$firstpass = true;
		if(count($activity[$key]) > 0) {
			echo "<table class='sortable'>";
			foreach((array)$activity[$key] as $value) {
				if($firstpass) {
					echo "<tr>";
					foreach($value as $columnHeader=>$columnValue) {
						echo "<th style='padding: 5px; border-bottom: 1px solid #000;'><div class='inner'>{$columnHeader}</div></th>";
					}
					echo "<td style='border-bottom: 1px solid #000;'>&nbsp;</td>";
					if(isset($edit[$key])) { echo "<td style='border-bottom: 1px solid #000;'>&nbsp;</td>"; }
					if(isset($delete[$key])) { echo "<td style='border-bottom: 1px solid #000;'>&nbsp;</td>"; }
					echo "</tr>";
					
					$firstpass = false;
				}
				
				echo "<tr class='" . (++$x % 2 == 0 ? 'even' : 'odd') . "'>";
					foreach($value as $columnHeader=>$columnValue) {
					echo "<td>" . ($columnValue === "1" ? "<div style='text-align: center;'><img src='../../src/img/check.png'></div>" : ($columnValue === "0" ? "<div style='text-align: center;'><img src='../../src/img/x.png'></div>" : $columnValue)) ."</td>";
				}
				
				$viewLink = str_replace("ID_PLACEHOLDER", "{$value["ID"]}", $view[$key]);
				echo "<td style='text-align: center; font-size: 10px;'>[ <a href='#' onclick='{$viewLink}'>View</a> ]</td>";
				if($edit[$key]) {
					$editLink = str_replace("ID_PLACEHOLDER", "{$value["ID"]}", $edit[$key]);
					echo "<td style='text-align: center; font-size: 10px;'>[ <a  " . ($value["Complete?"] == 1 ? "disabled=disabled href='#'" : "href='{$editLink}&returnto=../reports/individual/activity&delegate={$_GET["delegate"]}'") . " >Edit</a> ]</td>";
				}
				if($delete[$key]) {
					$deleteLink = str_replace("ID_PLACEHOLDER", "{$value["ID"]}", $delete[$key]);
					echo "<td style='text-align: center; font-size: 10px;'>[ <a href='#' onclick='{$deleteLink}'>Delete</a> ]</td>";
				}
				echo "</tr>";
			}
			echo "</table>";
		} else {
			echo "<div style='padding: 5px;'>There is no data to display with the current filter.</div>";
		}
	echo "</div>";
}

?>