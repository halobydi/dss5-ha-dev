<?php
require_once("../../src/php/require.php");

$oci = new mcl_Oci('soteria');

$sql = "
	WITH T AS (
		SELECT USID FROM EMPLOYEES WHERE PATH LIKE '%{$usid}%'
	)
	SELECT  I.ITEM_NUM,
			I.ITEM_CATEGORY, 
			I.ITEM_TEXT, 
			COUNT(*) AS CT 
	FROM EPOP_OBSERVATIONS O, 
		T,
		EPOP_ANSWERS A,
		EPOP_ITEMS I 
	WHERE T.USID = NVL(O.EMPLOYEE, O.COMPLETED_BY )
	AND A.ANSWER = 'IO'
	AND A.ITEM_NUM = I.ITEM_NUM
	AND A.EPOP_ID = O.EPOP_ID
	AND  OBSERVED_DATE BETWEEN TO_DATE('{$start} 00:00:00', 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE('{$end} 23:59:59', 'MM/DD/YYYY HH24:MI:SS')
	GROUP BY I.ITEM_NUM, ITEM_CATEGORY, ITEM_TEXT
	ORDER BY COUNT(*) DESC
";

/* No need to include % comments since it will always be 100%
$sql = "
    WITH T AS (
		SELECT USID FROM EMPLOYEES WHERE PATH LIKE '%{$usid}%'
	),
	O AS (
		SELECT  O.EPOP_ID,
				O.COMMENTS
		FROM EPOP_OBSERVATIONS O, 
				T 
		WHERE T.USID = NVL(O.EMPLOYEE, O.COMPLETED_BY )
		AND  OBSERVED_DATE BETWEEN TO_DATE('{$start} 00:00:00', 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE('{$end} 23:59:59', 'MM/DD/YYYY HH24:MI:SS')
		AND SWO = 1
	),
	IO AS (
		SELECT A.ITEM_NUM,
			   COUNT(*) AS CT,
			   SUM(CASE WHEN C.COMMENTS IS NOT NULL OR O.COMMENTS IS NOT NULL THEN 1 ELSE 0 END) AS COMMENTS
		FROM   O
		LEFT JOIN EPOP_ANSWERS A ON A.EPOP_ID = O.EPOP_ID
		LEFT JOIN EPOP_COMMENTS C ON C.EPOP_ID = A.EPOP_ID AND C.ITEM_NUM = A.ITEM_NUM
		LEFT JOIN EPOP_ITEMS I ON I.ITEM_NUM = A.ITEM_NUM      
		WHERE A.ANSWER = 'IO'
		GROUP BY A.ITEM_NUM
	)
	SELECT  IO.ITEM_NUM,
			ITEM_TEXT, 
			CT, 
			COMMENTS 
	FROM    IO, EPOP_ITEMS
	WHERE   IO.ITEM_NUM = EPOP_ITEMS.ITEM_NUM
	
";
*/
$x = 0;
$style = "style='text-align: left; width:580px;'";

$io = array();
//$totalComments = 0;
while($row = $oci->fetch($sql)){
	
	$url = "io.php?sid=" . time();
	$url .= !empty($_GET["start"]) ? "&start={$_GET["start"]}" : "";
	$url .= !empty($_GET["end"]) ? "&end={$_GET["end"]}" : "";
	$url .= !empty($_GET["delegate"]) ? "&delegate={$_GET["delegate"]}" : "";
	$url .= "&itemnum={$row["ITEM_NUM"]}";
	
	$tbl .= "<tr class = '" . ($x++ % 2 == 0 ? 'even' : 'odd') . "'>";
	$tbl .= "<td>{$x}</td>";
	$tbl .= "<td {$style}>{$row["ITEM_TEXT"]}</td>";
	$tbl .= "<td onclick='window.location=\"{$url}\"' style='cursor:pointer;'><a href='#' onclick='return false;'>{$row["CT"]}</a></td>";
	
	//$percentComments = @round(($row["CT"] / $row["COMMENTS"]) * 100, 2);
	//$tbl .= "<td>{$percentComments}%</td>";
	$tbl .= "</tr>";
	
	$total += $row["CT"];
	//$totalComments += $row["COMMENTS"];
	$io[$row["ITEM_NUM"]] = $row["ITEM_TEXT"];
}

if($x == 0){
	$tbl .= "<tr><td colspan='3'>No improvement opportunites found for the current time frame ({$start} - {$end}).</td></tr>";
} else if($x > 1){
	$border = "style='border: 1px solid black;'";
	$tbl .= "<tr {$border}>";
	$tbl .= "<td {$border}>--</td>";
	$tbl .= "<td {$border} {$style}>Grand Total Improvement Opportunities</td>";
	$tbl .= "<td {$border}>{$total}</td>";
	//$totalPercentComments = @round(($totalComments / $total) * 100, 2);
	//$tbl .= "<td {$border}>{$totalPercentComments}%</td>";
	$tbl .= "</tr>";
}

echo "
<form method = 'GET' action='io.php' style = 'margin-left: 2px; padding: 0px;'>
	<table style = 'font-size: 12px; font-weight: bold'>
		<tr style = 'vertical-align: bottom;'>
			<td>
				<input style = 'height: 12px; width: 100px;' type = 'text' name = 'start' id = 'start' value = '{$start}'/> <img style = 'vertical-align: bottom;' src='../../src/img/calendar.gif' alt='' id='tcal' onmouseover='setup_cal(\"tcal\", \"start\");' />
			</td>
			<td>
				<input style = 'height: 12px; width: 100px;'  type = 'text' name = 'end' id = 'end' value = '{$end}' /> <img style = 'vertical-align: bottom;' src='../../src/img/calendar.gif' alt='' id='tcal2' onmouseover='setup_cal(\"tcal2\", \"end\");' />
			</td>
			<td style = 'text-align: right;'>
				" . (!empty($_GET["delegate"]) ? "<input type='hidden' name='delegate' value='{$_GET["delegate"]}'/>" : "") . "
				<input type = 'submit' style = \"height: 19px;\" value = 'Filter'/>
				<input onclick = \"dojo.byId('start').value = '{$startWeek}'; dojo.byId('end').value = '{$endWeek}';\" type = \"submit\" style = \"height: 19px;\" value = \"Current Week\"/>
				<input onclick = \"dojo.byId('start').value = '{$startWeekPast}'; dojo.byId('end').value = '{$endWeekPast}';\"  type = \"submit\" style = \"height: 19px;\" value = \"Past Week\"/>
			</td>
		</tr>
	</table>
</form>
<table class='tbl' style=''>
	<tr>
		<th colspan='3'>
			<div class='inner_title'>Improvement Opportunities</div>
		</th>
	</tr>
	<tr>
		<th>
			<div style='width:100px;' class='inner'>No.</div>
		</th>
		<th>
			<div style='width:600px;' class='inner'>Observation Item</div>
		</th>
		<th>
			<div style='width:100px;' class='inner'>IO Count</div>
		</th>
	</tr>
	{$tbl}
</table>
";

if(!empty($_GET["itemnum"])){
	$sql = "
		WITH T AS (
			SELECT USID FROM EMPLOYEES WHERE PATH LIKE '%{$usid}%'
		)
		SELECT  O.EPOP_ID, 
				O.NAME, 
				O.TITLE,
				O.LOCATION,
				NVL(E.NAME, COMPLETED_BY) AS COMPLETED_BY,
				TO_CHAR(OBSERVED_DATE, 'MM/DD/YYYY') AS DT 
		FROM(
			SELECT  O.*
			FROM EPOP_OBSERVATIONS O, 
				T,
				EPOP_ANSWERS A,
				EPOP_ITEMS I 
			WHERE T.USID = NVL(O.EMPLOYEE, O.COMPLETED_BY )
			AND A.ANSWER = 'IO'
			AND I.ITEM_NUM = {$_GET["itemnum"]}
			AND A.ITEM_NUM = I.ITEM_NUM
			AND A.EPOP_ID = O.EPOP_ID
			AND  OBSERVED_DATE BETWEEN TO_DATE('{$start} 00:00:00', 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE('{$end} 23:59:59', 'MM/DD/YYYY HH24:MI:SS')
			AND SWO = 1
		) O LEFT JOIN EMPLOYEES E
			ON E.USID = O.COMPLETED_BY
		ORDER BY OBSERVED_DATE
	";
	
	$tbl = "";
	$x = 0;
	while($row = $oci->fetch($sql)){
		$tbl .= "
			<tr class = '" . ($x++ % 2 == 0 ? 'even' : 'odd'). "'>
				<td style = 'text-align: left; width: 200px;'>{$row["NAME"]}</td>
				<td style = 'text-align: left; width: 200px;'>{$row["TITLE"]}</td>
				<td style = 'text-align: left; width: 200px;'>{$row["LOCATION"]}</td>
				<td style = 'text-align: left; width: 200px;'>{$row["COMPLETED_BY"]}</td>
				<td style = 'text-align: center; width: 100px;'>{$row["DT"]}</td>
				<td style = 'text-align: center; width: 50xp;'><a href = '#'; onclick = 'sot.swo.view({$row["EPOP_ID"]}); return false;'>View</a></td>
			</tr>
		";
	}
	if($x == 0){
		$tbl = "<tr>
					<td colspan='5' style='width: 650px;'>
						No observations found with the current filters.
					</td>
				</tr>
			";
	}
	
	echo "
	<div style='width: 850px; margin: 5px; margin-top: 10px;'>Observations with the following improvement opportunity: {$io[$_GET["itemnum"]]}</div>
	<table class='tbl' style=''>
		<tr>
			<th>
				<div class='inner' style='width: 200px;'>
					Employee Observed
				</div>
			</th>
			<th>
				<div class='inner' style='width: 2-0px;'>
					Title
				</div>
			</th>
			<th>
				<div class='inner' style='width: 200px;'>
					Location
				</div>
			</th>
			<th>
				<div class='inner' style='width: 200px;'>
					Observed By
				</div>
			</th>
			<th>
				<div class='inner' style='width: 100px;'>
					Date Observed
				</div>
			</th>		
			<th>
				<div class='inner' style='width: 50px;'>
				</div>
			</th>
		</tr>
		{$tbl}
	</table>
";
}
?>