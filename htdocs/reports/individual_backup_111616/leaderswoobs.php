<?php
require_once("../../src/php/require.php");

$sql = "
WITH T AS (
		SELECT 	USID, 
				NAME, 
				TITLE, 
				SUPERVISOR 
		FROM 	EMPLOYEES 
		WHERE 	LEADERS > 0 
				AND ORG_DESCRIPTION IN (
					SELECT 	ORG_DESCRIPTION 
					FROM 	ORGANIZATIONS
				)
				AND PATH LIKE '%{$usid}%' 
	),
	T2 AS (
		SELECT 	T.USID, 
				T.NAME, 
				T.TITLE, 
				E.NAME AS SUPERVISOR 
		FROM 	T 
		LEFT JOIN EMPLOYEES E 
			ON T.SUPERVISOR = E.USID
	),
	T3 AS (
		SELECT 
			* 
		FROM 
			T2 
		WHERE 
			USID NOT IN(
				SELECT 	
					OBSERVATION_CREDIT 
				FROM 	
					EPOP_OBSERVATIONS 
				WHERE 	
					OBSERVED_DATE BETWEEN TRUNC(SYSDATE, 'IW') AND TRUNC(SYSDATE, 'IW') + 7
					AND OBSERVATION_CREDIT IS NOT NULL
			) AND NOT EXISTS (
				SELECT 	1 
				FROM 	EXCLUSIONS LE 
				WHERE 	T2.USID = LE.USID 
						AND (TRUNC(SYSDATE, 'IW') - 7 <= LE.END_DT OR LE.END_DT IS NULL)
			)
	),
	T4 AS (
		SELECT 	OBSERVATION_CREDIT, 
				MAX(OBSERVED_DATE) AS LAST_OBSERVATION 
		FROM 	EPOP_OBSERVATIONS O, 
				T3 
		WHERE 	O.OBSERVATION_CREDIT = T3.USID
			GROUP BY OBSERVATION_CREDIT
	)
	SELECT 
		USID,
		NAME,
		TITLE,
		SUPERVISOR,
		ROUND((TRUNC(SYSDATE, 'IW') - TRUNC(T4.LAST_OBSERVATION, 'IW')) / 7) AS WEEKS,
		TO_CHAR(T4.LAST_OBSERVATION, 'MM/DD/YYYY') AS LAST_OBSERVATION
	FROM T3 
	LEFT JOIN T4 
		ON T3.USID = T4.OBSERVATION_CREDIT
	ORDER BY WEEKS DESC, NAME
";

	//echo "<pre>{$sql}</pre>";
	$oci = new mcl_Oci("soteria");

	$x = 0;
	echo "
	<div style='margin: 5px; width: 600px;'>
		Note: The number of weeks is including the current week.
		<span style='font-size: 10px;'>(For example, if the description states an employee has not completed an observation in 2 weeks, 
		this means they have not completed an observation this week and last week.)</span>
	</div>
	";
	$header =  "
		<table class='tbl'>
			<tr>
			<th style='width: 200px;'>
				<div class='inner'>
					Leader
				</div>
			</th>
			<th style='width: 300px;'>
				<div class='inner'>
					Title
				</div>
			</th>
			<th style='width: 200px;'>
				<div class='inner'>
					Supervisor
				</div>
			</th>
		</tr>
		";
	
	$prev = "";
	$firstpass = true;
	$has_consecutive = false;
	while($row = $oci->fetch($sql)){
	
		if($row["WEEKS"] === '1'|| $row['WEEKS'] === '0'){ continue; }
		if($row["WEEKS"] != $prev || $firstpass){
			$firstpass = false;
			if(!empty($row["WEEKS"])){
				$weeks = "The following employees have not completed a SWO, QEW or Paired observation in " . $row["WEEKS"] . " week" . ($row["WEEKS"]  > 1 ? "s" : "") . "";
				if($row["WEEKS"] == 2) { $weeks .= " <span style='font-size: 10px;'>(this week & last week)</span>";}
				$weeks .= ".";
				$has_consecutive = true;
				echo "
				" . (!$firstpass ? "</table>" : "") . "
				<div style='margin-left: 5px; margin-top: 5px;'>
					{$weeks}
				</div>
				{$header}";
			} else {
				
				$weeks = "";
				echo "</table>
				<div style='margin-left: 5px; margin-top: 5px;'>
					The following employees have not completed an observation using SOTeria.
				</div>
					{$header}
				";
			}
			$border = "border-top: 1px solid black;";
		} else {
			$weeks = "";
			$border = "";
		}
		
		echo "<tr class = '" . ($r++ % 2 == 0 ? 'even' : 'odd'). "'>
			<td style = 'text-align: left; {$border}'>{$row["NAME"]}</td>
			<td style = 'text-align: left; {$border}'>{$row["TITLE"]}</td>
			<td style = 'text-align: left; {$border}'>{$row["SUPERVISOR"]}</td>
		</tr>
		";
		
		$prev = $row["WEEKS"];
		$x++;
	}
	

	if($x == 0){
		echo "{$header}
		<tr>
			<td colspan='3' style=''>
				Each leader in your team has completed an observation in the past 2 weeks.
			</td>
		</tr>
	</table>";
	}


?>
