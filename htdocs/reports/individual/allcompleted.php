<?php
require_once("../../src/php/require.php");

$oci = new mcl_Oci("soteria");
$sql = "
	SELECT 	HP_ID,
			TO_CHAR(INCIDENT_DATE, 'MM/DD/YYYY') AS DT 
	FROM 	HP_OBSERVATIONS O 
	WHERE 	COMPLETED_BY = '{$usid}'
			AND INCIDENT_DATE BETWEEN TO_DATE('{$start} 00:00:00', 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE('{$end} 23:59:59', 'MM/DD/YYYY HH24:MI:SS')
	ORDER BY INCIDENT_DATE DESC
";



$x = 0;
while($row = $oci->fetch($sql)){
	$tbl .= "
		<tr class = '" . ($x++ % 2 == 0 ? 'even' : 'odd'). "'>
			<td style = 'text-align: center; width: 100px;'>{$row["DT"]}</td>
			<td style = 'text-align: center; width: 50xp;'><a href = '#' onclick = 'sot.hp.view({$row["HP_ID"]}); return false;'>View</a></td>	
		</tr>
	";
}

if($x == 0){
	$tbl = "<tr>
				<td colspan='2' style='width: 250px;'>
					No observations found for the current time frame ({$start} - {$end}).
				</td>
			</tr>
		";
}

echo "
<div style = 'float: left;'>
	<table class='tbl' style=''>
		<tr>
			<th colspan='2'>
				<div class='inner_title'>
					Human Performance Observations
				</div>
			</th>
		</tr>
		<tr>
			<th>
				<div class='inner' style='width: 200px;'>
					Incident Date
				</div>
			</th>
			<th>
				<div class='inner' style='width: 50px;'>
				</div>
			</th>
		</tr>
		{$tbl}
	</table>
</div>
<div style = 'float: right;'>
	<form method = 'GET' action='allcompleted.php' style = 'overflow: hidden; width: 510px; float: right; padding: 5px;'>
		<table style = 'font-size: 12px; font-weight: bold'>
			<!--<tr>
				<td>From</td>
				<td>To</td>
			</tr>-->
			<tr style = 'vertical-align: bottom;'>
				<td>
					<input style = 'height: 12px; width: 100px;' type = 'text' name = 'start' id = 'start' value = '{$start}'/> <img style = 'vertical-align: bottom;' src='../../src/img/calendar.gif' alt='' id='tcal' onmouseover='setup_cal(\"tcal\", \"start\");' />
				</td>
				<td>
					<input style = 'height: 12px; width: 100px;'  type = 'text' name = 'end' id = 'end' value = '{$end}' /> <img style = 'vertical-align: bottom;' src='../../src/img/calendar.gif' alt='' id='tcal2' onmouseover='setup_cal(\"tcal2\", \"end\");' />
				</td>
				<td style = 'text-align: right;'>
					" . (!empty($_GET["delegate"]) ? "<input type='hidden' name='delegate' value='{$_GET["delegate"]}'/>" : "") . "
					<input type = 'submit' style = \"height: 19px;\" value = 'Filter'/>
					<input onclick = \"dojo.byId('start').value = '{$startWeek}'; dojo.byId('end').value = '{$endWeek}';\" type = \"submit\" style = \"height: 19px;\" value = \"Current Week\"/>
					<input onclick = \"dojo.byId('start').value = '{$startWeekPast}'; dojo.byId('end').value = '{$endWeekPast}';\"  type = \"submit\" style = \"height: 19px;\" value = \"Past Week\"/>
				</td>
			</tr>
		</table>
	</form>
</div>
";

$sql = "
	SELECT 	HP_ID, 
			O.NAME, 
			O.TITLE,
			TO_CHAR(INCIDENT_DATE, 'MM/DD/YYYY') AS DT,
			NVL(E.NAME, O.COMPLETED_BY) AS COMPLETED_BY
	FROM 	HP_OBSERVATIONS O 
	LEFT JOIN EMPLOYEES E
		ON E.USID = O.COMPLETED_BY
	WHERE 	COMPLETED_BY != '{$usid}'
			AND OBSERVATION_CREDIT = '{$usid}'
			AND INCIDENT_DATE BETWEEN TO_DATE('{$start} 00:00:00', 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE('{$end} 23:59:59', 'MM/DD/YYYY HH24:MI:SS')
	ORDER BY INCIDENT_DATE DESC
";


$x = 0;
$tbl = '';
while($row = $oci->fetch($sql)){
	$tbl .= "
		<tr class = '" . ($x++ % 2 == 0 ? 'even' : 'odd'). "'>
			<td style = 'text-align: center; width: 100px;'>{$row["DT"]}</td>
			<td style = 'text-align: center; width: 100px;'>{$row["COMPLETED_BY"]}</td>
			<td style = 'text-align: center; width: 50xp;'><a href = '#' onclick = 'sot.hp.view({$row["HP_ID"]}); return false;'>View</a></td>	
		</tr>
	";
}

if($x == 0){
	$tbl = "<tr>
				<td colspan='3' style='text-align: center;'>
					No observations found for the current time frame ({$start} - {$end}).
				</td>
			</tr>
		";
}

echo "
<div style='clear: both;'>
	<table class='tbl' style='float: left;width:350px'>
		<tr>
			<th colspan='3'>
				<div class='inner_title'>
					Human Performance Observations Completed By Delegates
				</div>
			</th>
		</tr>
		<tr>
			<th>
				<div class='inner' style='width: 100px;'>
					Date Observed
				</div>
			</th>
			<th>
				<div class='inner' style='width: 200px;'>
					Observed by
				</div>
			</th>
			<th>
				<div class='inner' style='width: 50px;'>
				</div>
			</th>
		</tr>
		{$tbl}
	</table>
</div>
";

//epop
$tbl = '';
$sql = "
	SELECT 	EPOP_ID, 
			NAME, 
			TITLE,
			TO_CHAR(OBSERVED_DATE, 'MM/DD/YYYY') AS DT 
	FROM 	EPOP_OBSERVATIONS O 
	WHERE 	COMPLETED_BY = '{$usid}'
			AND OBSERVED_DATE BETWEEN TO_DATE('{$start} 00:00:00', 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE('{$end} 23:59:59', 'MM/DD/YYYY HH24:MI:SS')
	ORDER BY OBSERVED_DATE DESC
";



$x = 0;
while($row = $oci->fetch($sql)){
	$tbl .= "
		<tr class = '" . ($x++ % 2 == 0 ? 'even' : 'odd'). "'>
			<td style = 'text-align: left; width: 200px;'>{$row["NAME"]}</td>
			<td style = 'text-align: left; width: 300px;'>{$row["TITLE"]}</td>
			<td style = 'text-align: center; width: 100px;'>{$row["DT"]}</td>
			<td style = 'text-align: center; width: 50xp;'><a href = '#'; onclick = 'sot.swo.view({$row["EPOP_ID"]}); return false;'>View</a></td>	
		</tr>
	";
}

if($x == 0){
	$tbl = "<tr>
				<td colspan='4' style='width: 650px;'>
					No observations found for the current time frame ({$start} - {$end}).
				</td>
			</tr>
		";
}

echo "
<div style='clear: both;'/>
<div style = 'float: left;margin-top:15px;'>
	<table class='tbl' style=''>
		<tr>
			<th colspan='4'>
				<div class='inner_title'>
					Employee Observations
				</div>
			</th>
		</tr>
		<tr>
			<th>
				<div class='inner' style='width: 200px;'>
					Employee Observed
				</div>
			</th>
			<th>
				<div class='inner' style='width: 300px;'>
					Title
				</div>
			</th>
			<th>
				<div class='inner' style='width: 100px;'>
					Date Observed
				</div>
			</th>
			<th>
				<div class='inner' style='width: 50px;'>
				</div>
			</th>
		</tr>
		{$tbl}
	</table>
</div>
";

$sql = "
	SELECT 	EPOP_ID, 
			O.NAME, 
			O.TITLE,
			TO_CHAR(OBSERVED_DATE, 'MM/DD/YYYY') AS DT,
			NVL(E.NAME, O.COMPLETED_BY) AS COMPLETED_BY
	FROM 	EPOP_OBSERVATIONS O 
	LEFT JOIN EMPLOYEES E
		ON E.USID = O.COMPLETED_BY
	WHERE 	COMPLETED_BY != '{$usid}'
			AND OBSERVATION_CREDIT = '{$usid}'
			AND OBSERVED_DATE BETWEEN TO_DATE('{$start} 00:00:00', 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE('{$end} 23:59:59', 'MM/DD/YYYY HH24:MI:SS')
	ORDER BY OBSERVED_DATE DESC
";

//red tag
$x = 0;
$tbl = '';
while($row = $oci->fetch($sql)){
	$tbl .= "
		<tr class = '" . ($x++ % 2 == 0 ? 'even' : 'odd'). "'>
			<td style = 'text-align: left; width: 200px;'>{$row["NAME"]}</td>
			<td style = 'text-align: left; width: 300px;'>{$row["TITLE"]}</td>
			<td style = 'text-align: center; width: 100px;'>{$row["DT"]}</td>
			<td style = 'text-align: center; width: 100px;'>{$row["COMPLETED_BY"]}</td>
			<td style = 'text-align: center; width: 50xp;'><a href = '#'; onclick = 'sot.swo.view({$row["EPOP_ID"]}); return false;'>View</a></td>	
		</tr>
	";
}

if($x == 0){
	$tbl = "<tr>
				<td colspan='5' style='text-align: center;'>
					No observations found for the current time frame ({$start} - {$end}).
				</td>
			</tr>
		";
}

echo "
<div style='clear: both;'>
	<table class='tbl' style='float: left;'>
		<tr>
			<th colspan='5'>
				<div class='inner_title'>
					Employee Observations Completed By Delegates
				</div>
			</th>
		</tr>
		<tr>
			<th>
				<div class='inner' style='width: 200px;'>
					Employee Observed
				</div>
			</th>
			<th>
				<div class='inner' style='width: 300px;'>
					Title
				</div>
			</th>
			<th>
				<div class='inner' style='width: 100px;'>
					Date Observed
				</div>
			</th>
			<th>
				<div class='inner' style='width: 100px;'>
					Observed by
				</div>
			</th>
			<th>
				<div class='inner' style='width: 50px;'>
				</div>
			</th>
		</tr>
		{$tbl}
	</table>
</div>";

	$privileges = auth::check('privileges');

	$oci = new mcl_Oci('soteria');
	
	if(isset($_GET["r"])){
		echo "<div class = '" . ($_GET["r"] == "1" ? "success" : ($_GET["r"] == "0" ? "error" : "")) . "' style='margin-left: 5px; margin-top: 5px;'>
			" . ($_GET["r"] == "1" ? "Successfully saved audit." : ($_GET["r"] == "0" ? "Unable to save audit." : "")) . "
		</div>";
	}
	
	$tbl = "
				<tr >
					<th colspan='6'>
						<div class='inner_title'>
							Red Tag Audits Completed
						</div>
					</th>
				</tr>
				<tr>
					<th>
						<div class='inner' style='width: 200px;'>
							Location (SC)
						</div>
					</th>
					</th>
					<th>
						<div class='inner' style='width: 200px;'>
							Org. Audited
						</div>
					</th>
					</th>
					<th>
						<div class='inner' style='width: 200px;'>
							Date Audited
						</div>
					</th>
					</th>
					<th>
						<div class='inner' style='width: 200px;'>
							Credit Month
						</div>
					</th>
					</th>
					<th>
						<div class='inner' style='width: 200px;'>
							Completed By
						</div>
					</th>
					<th>
						<div class='inner' style='width: 50px;'>
						</div>
					</th>
				</tr>
			";
			
			
	$sql = "
		SELECT R.LOCATION,
				R.ORG,
				TO_CHAR(TO_DATE(CREDIT_MONTH, 'MM'), 'Month') AS CREDIT_MONTH,
				TO_CHAR(AUDIT_DATE, 'MM/DD/YYYY') AS AUDIT_DATE,
				NVL(E.NAME, COMPLETED_BY) AS COMPLETED_BY,
				COMPLETE,
				RTA_ID
		FROM	RED_TAG_AUDITS R
		LEFT JOIN EMPLOYEES E
			ON E.USID = R.COMPLETED_BY
		WHERE	COMPLETED_BY = '{$usid}'
			ORDER BY AUDIT_DATE	
				
	";
	
	$x = 0;
	while($row = $oci->fetch($sql)){
		$tbl .= "<tr class='" . ($x++ % 2 == 0 ? 'even' : 'odd') ."'>
				<td>{$row["LOCATION"]}</td>
				<td>{$row["ORG"]}</td>
				<td>{$row["AUDIT_DATE"]}</td>
				<td>{$row["CREDIT_MONTH"]}</td>
				<td>{$row["COMPLETED_BY"]}</td>
				<td>" . ($row["COMPLETE"] == 1 ? "<a href='#' onclick='sot.redtag.view({$row["RTA_ID"]}); return false;'>View</a>" 
				: "<a href='../../forms/redtag.php?id={$row["RTA_ID"]}&returnto=reports/individual/auditscompleted'>Edit</a>") ."</td>
			</tr>";
	}
	
	if($x == 0){
		$tbl .= "<tr>
					<td colspan='6'>
						There are no audits to display with the currrent filters.
					</td>
				</tr>";
	}
	echo "<div style = ''>
		<table class='tbl'>
			{$tbl}
		</table>
	</div>
	";
?>