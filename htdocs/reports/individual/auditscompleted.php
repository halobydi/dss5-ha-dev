<?php
	require_once("../../src/php/require.php");
	$privileges = auth::check('privileges');

	$oci = new mcl_Oci('soteria');
	
	if(isset($_GET["r"])){
		echo "<div class = '" . ($_GET["r"] == "1" ? "success" : ($_GET["r"] == "0" ? "error" : "")) . "' style='margin-left: 5px; margin-top: 5px;'>
			" . ($_GET["r"] == "1" ? "Successfully saved audit." : ($_GET["r"] == "0" ? "Unable to save audit." : "")) . "
		</div>";
	}
	
	$tbl = "
				<tr >
					<th colspan='6'>
						<div class='inner_title'>
							Red Tag Audits Completed
						</div>
					</th>
				</tr>
				<tr>
					<th>
						<div class='inner' style='width: 200px;'>
							Location (SC)
						</div>
					</th>
					</th>
					<th>
						<div class='inner' style='width: 200px;'>
							Org. Audited
						</div>
					</th>
					</th>
					<th>
						<div class='inner' style='width: 200px;'>
							Date Audited
						</div>
					</th>
					</th>
					<th>
						<div class='inner' style='width: 200px;'>
							Credit Month
						</div>
					</th>
					</th>
					<th>
						<div class='inner' style='width: 200px;'>
							Completed By
						</div>
					</th>
					<th>
						<div class='inner' style='width: 50px;'>
						</div>
					</th>
				</tr>
			";
			
			
	$sql = "
		SELECT R.LOCATION,
				R.ORG,
				TO_CHAR(TO_DATE(CREDIT_MONTH, 'MM'), 'Month') AS CREDIT_MONTH,
				TO_CHAR(AUDIT_DATE, 'MM/DD/YYYY') AS AUDIT_DATE,
				NVL(E.NAME, COMPLETED_BY) AS COMPLETED_BY,
				COMPLETE,
				RTA_ID
		FROM	RED_TAG_AUDITS R
		LEFT JOIN EMPLOYEES E
			ON E.USID = R.COMPLETED_BY
		WHERE	COMPLETED_BY = '{$usid}'
			ORDER BY AUDIT_DATE	
				
	";
	
	$x = 0;
	while($row = $oci->fetch($sql)){
		$tbl .= "<tr class='" . ($x++ % 2 == 0 ? 'even' : 'odd') ."'>
				<td>{$row["LOCATION"]}</td>
				<td>{$row["ORG"]}</td>
				<td>{$row["AUDIT_DATE"]}</td>
				<td>{$row["CREDIT_MONTH"]}</td>
				<td>{$row["COMPLETED_BY"]}</td>
				<td>" . ($row["COMPLETE"] == 1 ? "<a href='#' onclick='sot.redtag.view({$row["RTA_ID"]}); return false;'>View</a>" 
				: "<a href='../../forms/redtag.php?id={$row["RTA_ID"]}&returnto=reports/individual/auditscompleted'>Edit</a>") ."</td>
			</tr>";
	}
	
	if($x == 0){
		$tbl .= "<tr>
					<td colspan='6'>
						There are no audits to display with the currrent filters.
					</td>
				</tr>";
	}
	echo "<div style = ''>
		<table class='tbl'>
			{$tbl}
		</table>
	</div>
	";
?>