<?php

    $currentWeek = intval(date('W'));
    $currentYear = intval(date('Y'));

    $display = 20;
    if(isset($_GET['year']) && !empty($_GET['year']) && is_numeric($_GET['year'])) {
        $year = intval($_GET['year']);
        if($year > $currentYear) {
            $year = $currentYear;
        }
    } else {
        $year = date("Y");
    }

    if(isset($_GET['weekStart']) && !empty($_GET['weekStart']) && is_numeric($_GET['weekStart'])) {
        $weekStart = intval($_GET['weekStart']);
        if($weekStart <= 0) {
            $weekStart = 1;
        }
    } else {
        $weekStart = 1;
    }

    $w = $weeks = $weekStart;

    if(isset($_GET['weekStop']) && !empty($_GET['weekStop']) && is_numeric($_GET['weekStop'])) {
        $weekStop = $_GET['weekStop'];
        if($year == $currentYear && $weekStop > $currentWeek) {
            $weekStop = $currentWeek;
        }
        if($weekStop > 52) {
            $weekStop = 52;
        }

    } else {
        $weekStop = $currentWeek - 1;
    }

    if($weekStop < $weekStart) {
        $weekStart = 1;
        $weekStop = $currentWeek - 1;
    }

    $sql = "
            SELECT
                TO_CHAR((TRUNC(TO_DATE('01/01/{$year}', 'MM/DD/YYYY'), 'IW') + " . ($weekStart * 7) . ") - 7, 'MM/DD/YYYY') AS START_DT,
                TO_CHAR((TRUNC(TO_DATE('01/01/{$year}', 'MM/DD/YYYY'), 'IW') + " . ($weekStop * 7) . ") - 1, 'MM/DD/YYYY') AS END_DT
            FROM
                DUAL
        ";
    $row = $oci->fetch($sql);
    $startDate = $row['START_DT'];
    $endDate = $row['END_DT'];

    //wk 14 (52 - 14) = 38 .. (0 - 38)
    $start = ($weekStop - ($display - 1));

    $completions = array();

    $weekNumber = $weekStart - 1;
    while($weekNumber < $weekStop) {

        $sql = "SELECT TO_CHAR((TRUNC(TO_DATE('01/01/2015', 'MM/DD/YYYY'), 'IW') + " . ($weekNumber * 7) . "), 'MM/DD/YYYY') AS DT FROM DUAL";
        $row = $oci->fetch($sql);
        $date = $row['DT'];

        $sql = "
                WITH T AS (
                    SELECT
                        USID,
                        TITLE,
                        NAME,
                        SUPERVISOR,
                        SAP_ID,
                        (SELECT NAME FROM EMPLOYEES E WHERE  TITLE LIKE '%Dir%' AND INSTR(X.PATH, E.USID) > 0 AND ROWNUM = 1) AS DIRECTOR
                    FROM
                        EMPLOYEES X
                    WHERE
                        PATH LIKE '%{$usid}%'
                    AND
                        LEADERS != 0
                    AND NOT EXISTS (
                        SELECT
                            1
                        FROM
                            EXCLUSIONS E
                        WHERE
                            E.USID = X.USID
                            AND (E.START_DT <= (TRUNC(TO_DATE('01/01/{$year}', 'MM/DD/YYYY'), 'IW') + " . ($weekNumber * 7) . ") OR START_DT IS NULL)
                            AND (E.END_DT >= (NEXT_DAY(TRUNC(TO_DATE('01/01/{$year}', 'MM/DD/YYYY'),'IW'), 'SUNDAY') + " . ($weekNumber * 7) . ") + .999 OR END_DT IS NULL)
                    )
                ),
                T2 AS (
                    SELECT
                        T.USID,
                        COUNT(EPOP_ID) AS CT
                    FROM
                        T
                    LEFT JOIN
                        EPOP_OBSERVATIONS O
                    ON
                        T.USID = O.OBSERVATION_CREDIT
                    WHERE
                        O.OBSERVED_DATE BETWEEN (TRUNC(TO_DATE('01/01/{$year}', 'MM/DD/YYYY'), 'IW') + " . ($weekNumber * 7) . ") AND (NEXT_DAY(TRUNC(TO_DATE('01/01/{$year}', 'MM/DD/YYYY'),'IW'), 'SUNDAY') + " . ($weekNumber * 7) . ") + .999
                        AND (SWO = 1  OR PP = 1)
                    GROUP BY USID
                )
                SELECT
                    T.USID,
                    T.NAME,
                    E.NAME AS SUPERVISOR,
                    T.TITLE,
                    T.DIRECTOR,
                    T.SAP_ID,
                    NVL(T2.CT, 0) COMPLETE,
                    TO_CHAR(TRUNC(TO_DATE('01/01/{$year}', 'MM/DD/YYYY'), 'IW') + " . ($weekNumber * 7) . ", 'MM/DD/YYYY') AS WEEK_START,
                    TO_CHAR(NEXT_DAY(TRUNC(TO_DATE('01/01/{$year}', 'MM/DD/YYYY'),'IW'), 'SUNDAY') + " . ($weekNumber * 7) . ", 'MM/DD/YYYY') AS WEEK_END
                FROM
                    T
                LEFT JOIN
                    T2 ON T.USID = T2.USID
                LEFT JOIN
                    EMPLOYEES E
                ON
                    T.SUPERVISOR = E.USID
                ORDER BY
                    T.NAME
            ";

        $week_array[$weekNumber + 1] = array(
            "total"			=> 0,
            "employee_ct" 	=> 0, /*Don't count excluded ones*/
            "complete_ct" 	=> 0 /*1 per week for each employee is only counted*/
        );

        $total = 0;
        while($row = $oci->fetch($sql)){
            $completions[$row["USID"]][$weekNumber + 1] = $row["COMPLETE"];
            $completions[$row["USID"]]["total"] += $row["COMPLETE"];
            $week_array[$weekNumber + 1]["total"] += $row["COMPLETE"];
            $week_array[$weekNumber + 1]['start'] = $row['WEEK_START'];
            $week_array[$weekNumber + 1]['end'] = $row['WEEK_END'];

            $total += $row["COMPLETE"];

            $employee_array[$row["USID"]] = array(
                "name" 		=> $row["NAME"],
                "title"=> $row["TITLE"],
                "supervisor" => $row["SUPERVISOR"],
                'sapid' => $row['SAP_ID'],
                'usid' => $row['USID'],
                'director' => $row['DIRECTOR']
            );
        }

        $weekNumber++;
    }