<?php
	function errorHandler($errno, $errstr, $errfile, $errline, $errcontext){}

	set_error_handler('errorHandler');
	require_once("mcl_Oci.php");
	require_once("../../src/php/auth.php");
	
	$oci = new mcl_Oci("soteria");

	$user = auth::check();
	$usid = (!empty($_GET["delegate"]) ? $user["delegates"][$_GET["delegate"]]["usid"] : $user["usid"]);

	require_once('lc_include.php');
	
	function sortbyname($a, $b){
		if ($a["name"] == $b["name"]) {
			return 0;
		}
		
		return ($a["name"] < $b["name"]) ? -1 : 1;
	}

	uasort($employee_array, "sortbyname");
	
	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Cache-Control: private",false);
	header("Content-Type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"SOTeria_LC_Export_" . time() . ".csv\";" );
	header("Content-Transfer-Encoding: binary");

	echo "\"Leader\"";
	echo ",\"Username\"";
	echo ",\"Sap ID\"";
	echo ",\"Title\"";
	echo ",\"Supervisor\"";
	echo ",\"Director\"";

	foreach($week_array as $key => $value){
		echo ",\"Observations Completed Week of " . ($value['start']) . "\"";
	}
	
	//echo ",\"Actual Complete (1/week)\"";
	//echo ",\"Expected Complete\"";
	echo ",\"Percent Complete\"";
	echo "\n";
	
	foreach($employee_array as $key=>$value){
		echo "\"{$value["name"]}\"";
		echo ",\"{$value["usid"]}\"";
		echo ",\"{$value["sapid"]}\"";
		echo ",\"{$value["title"]}\"";
		echo ",\"{$value["supervisor"]}\"";
		echo ",\"{$value["director"]}\"";

		$numerator = 0;
		$denominator = 0;
		foreach($week_array as $week=>$value){
			$excluded = "";
			
			if(!isset($completions[$key][$week])) {
				echo ",\"Excluded\""; 
			} else { 
				echo ",\"{$completions[$key][$week]}\"";
				
				if($completions[$key][$week] > 0) { $numerator++; $week_array[$week]["completed_ct"]++; }
				
				$denominator++;
				$week_array[$week]["employee_ct"]++;
			}
		}
		
		$percent = @round(($numerator / $denominator) * 100, 2);
		echo ",\"{$percent}\"%";
		echo "\n";
	}
	
	echo "\n\n";
	
	$totalCompleted = 0;
	$totalExpected = 0;
	
	/*Acatual Completed by Week*/
	echo "Actual Complete (1/leader/week),--,--,--,--,--";
	foreach($week_array as $key=>$value) {
		echo ",\"{$value['completed_ct']}\"";
		$totalCompleted += $value["completed_ct"];
	}
	echo ",{$totalCompleted}\n";
	
	/*Expected Completed by Week*/
	echo "Expected Complete (1/leader/week),--,--,--,--,--";
	foreach($week_array as $key=>$value) {
		echo ",\"{$value['employee_ct']}\"";
		$totalExpected += $value["employee_ct"];
	}
	echo ",{$totalExpected}\n";
	
	/*Percent Completed by Week*/
	echo "Percent Complete (1/leader/week),--,--,--,--,--";
	
	
	foreach($week_array as $key=>$value) {
		$percent = @round(($value["completed_ct"] / $value["employee_ct"]) * 100, 2);
		echo ",\"{$percent}\"%";
	}
	
	$percent = @round(($totalCompleted / $totalExpected ) * 100, 2);
	echo ",\"{$percent}%\"";
	
	//echo ob_get_clean();
	
?>