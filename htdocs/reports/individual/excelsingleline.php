<?php
function errorHandler($errno, $errstr, $errfile, $errline, $errcontext){

}

set_error_handler('errorHandler');
require_once("mcl_Oci.php");
require_once("../../src/php/auth.php");

$category = "";
$swo = false;
$qew = false;
$pp = false;
$lc = false;
if($_GET['form'] == 'swo_lc') { $lc = true; }

if($_GET["form"] == "swo" || $_GET['form'] == 'swo_lc') { $swo = true; }
if($_GET["form"] == "qew"){ $qew = true; }
if($_GET["form"] == "pp"){ $pp = true; }
if(!$pp && !$qew){ $swo = true; }

$form = "";
if($swo) { $form = "SWO"; }
if($pp){ $form = "PP"; }
if($qew) { $form = "QEW"; }

$category = '';
if($swo){
    $category = "'Leader', 'Individual', 'Organization'";
    if($lc) {
       // $category .= ",'LC_VEHICLE', 'LC_HEIGHTS', 'LC_LIFTING', 'LC_TRENCHING', 'LC_CONFINED', 'LC_HAZ_ENERGY', 'LC_HOT_WORK'";
    }
}
if($qew){ $category = "'Qualified Electrical Worker'"; }
if($pp) { $category = "'Paired Performance'";}

$oci = new mcl_Oci("soteria");
$sql = "
		WITH T AS(
			SELECT 	
				ITEM_NUM AS ORDINAL, 
				NULL AS PARENT_TEXT, 
				ITEM_NUM, 
				ITEM_CATEGORY, 
				ITEM_TEXT, 
				0 AS IS_SUB 
			FROM 
				EPOP_ITEMS
		)
		SELECT 	
			* 
		FROM 	
			T 
		WHERE 	
			ITEM_CATEGORY IN({$category}) 
		ORDER BY 
			ORDINAL, 
			ITEM_NUM	
	";

$items = array();
while($row = $oci->fetch($sql)){
    $items[$row["ITEM_NUM"]] = $row;
}

$answers = array(
    "IO" => "Improvment Opportunity",
    "S"  => "Satisfactory",
    "NA" => "Not Applicable",
    ""	 => "No Response"
);

//check login status
$user = auth::check();
if(!$user["status"]) {
    auth::deny();
}

$usid = (!empty($_GET["delegate"]) ? $user["delegates"][$_GET["delegate"]]["usid"] : $user["usid"]);

$tm_frame = "";
if($_GET["method"] == "week"){
    $tm_frame = "AND OBSERVED_DATE >= TRUNC(SYSDATE, 'IW')";
} else if($_GET["method"] == "month"){
    $tm_frame = "AND OBSERVED_DATE >= TRUNC(SYSDATE, 'MONTH')";
} else if($_GET["method"] == "year"){
    $tm_frame = "AND OBSERVED_DATE >= TRUNC(SYSDATE, 'YEAR')";
} else if($_GET["method"] == "dates"){
    $tm_frame = "AND OBSERVED_DATE BETWEEN TO_DATE('{$_GET["start"]} 00:00:00', 'MM/DD/YYYY HH24:MI:SS') 
			AND TO_DATE('{$_GET["end"]} 23:59:59', 'MM/DD/YYYY HH24:MI:SS')";
}

if(empty($_REQUEST["employee"])) {
    $employee = "CASE WHEN PP = 1 THEN O.OBSERVATION_CREDIT ELSE NVL(O.EMPLOYEE, O.OBSERVATION_CREDIT ) END";
} else {
    $employee = "O.{$_REQUEST["employee"]}";
}

$whereLc = "";
$lcColumns = "";
$lcColumnsFormatted = "";
if($lc) {
    $whereLc = 'AND LC_PILOT = 1';
    $lcColumns = "
				DECODE(O.PROCEDURE_FOR_WORK, 1, 'Yes', 'No') AS PROCEDURE_FOR_WORK,
				O.PROCEDURE_FOR_WORK_DETAIL,
				DECODE(O.LC_VEHICLE, 1, 'Yes', 'No') AS LC_VEHICLE,
				DECODE(O.LC_HEIGHTS, 1, 'Yes', 'No') AS LC_HEIGHTS,
				DECODE(O.LC_LIFTING, 1, 'Yes', 'No') AS LC_LIFTING,
				DECODE(O.LC_TRENCHING, 1, 'Yes', 'No') AS LC_TRENCHING,
				DECODE(O.LC_CONFINED, 1, 'Yes', 'No') AS LC_CONFINED,
				DECODE(O.LC_HOT_WORK, 1, 'Yes', 'No') AS LC_HOT_WORK,
				DECODE(O.LC_HAZ_ENERGY, 1, 'Yes', 'No') AS LC_HAZ_ENERGY,
			";
    $lcColumnsFormatted = <<<COLUMNS
				O.PROCEDURE_FOR_WORK AS "Procedure for Work",
				O.PROCEDURE_FOR_WORK_DETAIL AS "Procedure for Work Details",
				O.LC_VEHICLE AS "Life Critical Vehicle",
				O.LC_HEIGHTS AS "Life Critical Heights",
				O.LC_LIFTING AS "Life Critical Lifting",
				O.LC_TRENCHING AS "Life Critical Trenching",
				O.LC_CONFINED AS "Life Critical Confined",
				O.LC_HOT_WORK AS "Life Critical Hot Work",
				O.LC_HAZ_ENERGY AS "Life Critical Hazard Energy",
COLUMNS
    ;
}

$sql = <<<SQL
		WITH T AS(
			SELECT * FROM EMPLOYEES WHERE PATH LIKE '%{$usid}%'
		)
		SELECT 	O.EPOP_ID AS "Observation ID",
				O.EMPLOYEE AS "Employee Observed",
				O.NAME AS "Name",
				O.TITLE AS "Title",
				O.ORG_CODE AS "Organization",
				O.LOCATION AS "Location",
				TO_CHAR(OBSERVED_DATE, 'MM/DD/YYYY') AS "Date Observed",
				O.BEGIN_DATE AS "Begin Timestamp",
				O.COMPLETED_DATE AS "Completed Timestamp",
				NVL(E2.NAME, O.OBSERVATION_CREDIT) AS "Observed By",
				E2.LOCATION AS "Observed By Employee Location",
				E3.NAME AS "Observed By Supervisor",
				
				O.COMMENTS AS "Additional Comments",
				O.CONVERSATION AS "Conversation Took Place",
				O.GOOD_BEHAVIOR AS "Recognized Good Behavior",
				{$lcColumnsFormatted}
				O.OFFICE_FIELD AS "Office or Field"
		FROM(
            SELECT 
					EMPLOYEE, 
					O.NAME, 
					O.TITLE, 
					O.ORG_CODE,
					O.LOCATION,
					OBSERVED_DATE,
					O.OBSERVATION_CREDIT, 
					O.EPOP_ID,
					O.COMMENTS,
				
					O.COMPLETED_DATE,
					O.BEGIN_DATE,
					O.OFFICE_FIELD,
					{$lcColumns}
					CASE WHEN COMPLETED_DATE < TO_DATE('08/30/2014', 'MM/DD/YYYY') AND CONVERSATION_TOOK_PLACE = 0 THEN 'NA' ELSE DECODE(CONVERSATION_TOOK_PLACE, 1, 'Yes', 'No') END AS CONVERSATION,
					CASE WHEN COMPLETED_DATE < TO_DATE('08/30/2014', 'MM/DD/YYYY') AND RECOGNIZED_GOOD_BEHAVIOR = 0 THEN 'NA' ELSE DECODE(RECOGNIZED_GOOD_BEHAVIOR, 1, 'Yes', 'No') END AS GOOD_BEHAVIOR
			FROM EPOP_OBSERVATIONS O, T
			WHERE  (T.USID = {$employee})
					{$tm_frame}
					AND {$form} = 1
					  
		) O
		LEFT JOIN EMPLOYEES E2
			ON E2.USID = O.OBSERVATION_CREDIT
		LEFT JOIN EMPLOYEES E3
			ON E3.USID = E2.SUPERVISOR		
SQL
;


$oci->dateFormat();
$stmt = $oci->parse("
		SELECT 	A.ITEM_NUM,
				A.ANSWER,
				C.COMMENTS
		FROM 	EPOP_ANSWERS A
		LEFT JOIN EPOP_COMMENTS C
			ON A.EPOP_ID = C.EPOP_ID
		WHERE 	A.EPOP_ID = :epopId
	");

header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private",false);
header("Content-Type: application/octet-stream");
header("Content-Disposition: attachment; filename=\"SOTeria_Export_" . time() . ".csv\";" );
header("Content-Transfer-Encoding: binary");

$ct = 0;
$header = false;

while($row = $oci->fetch($sql)) {

    if (!$header) {
        $x = 0;
        foreach($row as $key=>$value) {

            echo ($x++ == 0 ? "" : ",") . $key;
        }

        foreach($items as $item) {
            echo ($x++ == 0 ? "" : ",") . "\"" . strip_tags($item["ITEM_TEXT"]) .  ($item["IS_SUB"] == 1 ? " (" . strip_tags($item["PARENT_TEXT"]) . ")": "") . "\"";
        }

        echo "\n";
        $header = true;
    }

    $x = 0;
    $answersToItems = array();
    foreach($row as $key => $value) {
        if($key == "Observation ID"){
            $oci->bind($stmt, array(
                ":epopId"	=> $value
            ));

            while($row2 = $oci->fetch($stmt, false)){
                $answersToItems[$row2["ITEM_NUM"]] = $row2;
            }

            echo ($x++ == 0 ? "" : ",") . "\"" . $value . "\"";

        } else if($key == "Answer"){
            echo ($x++ == 0 ? "" : ",") . "\"" . $answers[$value] . "\"";
        } else {
            echo ($x++ == 0 ? "" : ",") . "\"" . str_replace(array(",", "\"", "'"), "", $value) . "\"";
        }

    }

    foreach($items as $item) {
        echo ($x++ == 0 ? "" : ",") . "\"{$answers[$answersToItems[$item["ITEM_NUM"]]["ANSWER"]]}\"";
    }
    echo "\n";

    $ct++;
}

?>