<?php
require_once("../../src/php/require.php");
$oci = new mcl_Oci("soteria");

echo $usid;
$start = '01/01/2012';
$end = '01/01/2014';

$sql = "SELECT	HP_ID AS \"ID\", 
			COMPLETED_BY AS \"Completed By\",
			TO_CHAR(INCIDENT_DATE, 'MM/DD/YYYY') AS \"Incident Date\",
			EVENT_TYPE AS \"Event Type\",
			NVL(S.NAME, COMPLETED_BY_SUPERVISOR) AS \"Completed by Supervisor\",
			NVL(D.NAME, COMPLETED_BY_DIRECTOR) AS \"Completed by Director\",
			DECODE(COMPLETED, 'Y', 1, 0) AS \"Complete?\"
	FROM	HP_OBSERVATIONS N
	LEFT JOIN EMPLOYEES E ON E.USID = N.COMPLETED_BY
	LEFT JOIN EMPLOYEES S ON S.USID = N.COMPLETED_BY_SUPERVISOR
	LEFT JOIN EMPLOYEES D ON D.USID = N.COMPLETED_BY_DIRECTOR
	WHERE	(
				COMPLETED_BY IN (SELECT USID FROM EMPLOYEES WHERE PATH LIKE '%{$usid}%' UNION ALL SELECT '{$usid}' FROM DUAL)
				OR COMPLETED_BY_SUPERVISOR = '{$usid}'
				OR COMPLETED_BY_DIRECTOR = '{$usid}'	
			)
			AND COMPLETED_DATE BETWEEN TO_DATE('{$start} 00:00:00', 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE('{$end} 23:59:59', 'MM/DD/YYYY HH24:MI:SS')
	ORDER BY HP_ID";

echo $sql;

$results = array();

while($row = $oci->fetch($sql)) {
	$id = $row['ID'];
	echo $id;
	$results[$id] = array('answers'=>array());
	foreach($row as $key=>$val) {
		$results[$id][strtolower($key)] = $val;
	}

	$stmt2 = $oci->parse("SELECT * FROM HP_ANSWERS WHERE HP_ID = :id");
	$stmt2 = $oci->bind(array(':id'=>$id));
	while($a = $oci->fetch($stmt2)) {
		$results[$id]['answers'][$a['ITEM_NUM']][$a['SUBITEM_NUM']][] = $a['ANSWER'];
	}
}

$err = $oci->error();

echo "<pre>{$results}</pre>";


?>