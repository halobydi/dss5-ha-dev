<?php
/**
 * Created by PhpStorm.
 * User: halobydi
 * Date: 12/16/2016
 * Time: 10:29 AM
 * To Meet the requirments for adding the Observation Compliance and Observstion Quality Reports
 */
mcl_Html::js(mcl_Html::HIGHSTOCK);
$oci = new mcl_Oci("soteria");
if(!empty($_GET["start"])){
     $start = $_GET["start"];
} else {
    $start = date('m/d/Y', strtotime('-365 days'));
}
if(!empty($_GET["end"])){
    $end = $_GET["end"];
} else {
    $end = date('m/d/Y');
}

mcl_Html::s(mcl_Html::SRC_JS, <<<JS
	var highlight;
	$(function () {
		var chart;
		$(document).ready(function() {
		    /*
			chart = new Highcharts.Chart({
				chart: {
					renderTo: 'container',
					type: 'column'
				},
				title: {
					text: 'Improvement Opportunities Trending'
				},
				subtitle: {
					text: '{$orgs_a[$org]["title"]}'
				},
				xAxis: [{
					categories: [{$categories}]
				}],
				yAxis: {
					min: 0,
					title: {
						text: ''
					}
				},
				legend: {
					layout: 'vertical',
					x: 120,
					verticalAlign: 'top',
					y: 100,
					floating: true,
					backgroundColor: '#FFFFFF'
				},
				/*tooltip: {
					formatter: function(){
						
					}
				},*/
				legend: {
					enabled: true
				},
				plotOptions: {
					series: {
						shadow:false,
						cursor: 'pointer',
						point: {
							events: {
								click: function() { 
									if(highlight){
										highlight.style.backgroundColor = (highlight.className == 'even' ? '#cecece' : '#f0f0f0');
									}
									
									highlight = document.getElementById(this.config[2]);
									highlight.style.backgroundColor = '#ffffca';
								}	
							}
						}
					}
				},
				series: [{
						name: 'Top 1',
						data: [{$data1}],
						color: '#a80000',
						dataLabels: {
							enabled: true
						}
			
					}, {
						name: 'Top 2',
						data: [{$data2}],
						color: '#ff8448',
						dataLabels: {
							enabled: true
						}
			
					}, {
						name: 'Top 3',
						data: [{$data3}],
						color: '#a4d1ff',
						dataLabels: {
							enabled: true
						}
			
					}]
			});
		       */
		});
		
	});	
JS
);

echo "
<div style = 'text-align: left; margin-right: 10px;'>
	<form method = 'GET' action='obscomplianceandquality.php' style = 'overflow: hidden; padding: 5px;'>
		<table style = 'font-size: 11px;' border=0>
			<tr style='font-size: 10px;'>
				<td>Organization</td>
				<td>Observation Form</td>
				<td>Start</td>
				<td>End</td>
				<td></td>
			</tr>	
			<tr style = 'vertical-align: bottom;'>
				<td>
					<select name='org' style = 'height: 20px; font-size: 10px;'>
						<option value=''>- All Organizations - </option>
						{$org_select}
					</select>
				</td>
				<td>
					<select name='form' style = 'height: 20px; font-size: 10px;'>
						<option value=''>- All Observations - </option>
						{$form_s}
					</select>
				</td>
				<td style ='padding-bottom: 3px;'>
					<input style = 'height: 12px; width: 100px;' type = 'text' name = 'start' id = 'start' value = '{$start}'/> <img style = 'vertical-align: bottom;' src='../../src/img/calendar.gif' alt='' id='tcal' onmouseover='setup_cal(\"tcal\", \"start\");' />
				</td>
				<td style ='padding-bottom: 3px;'>
					<input style = 'height: 12px; width: 100px;' type = 'text' name = 'end' id = 'end' value = '{$end}'/> <img style = 'vertical-align: bottom;' src='../../src/img/calendar.gif' alt='' id='tcal2' onmouseover='setup_cal(\"tcal2\", \"end\");' />
				</td>
				<td>
						&nbsp;<input type='checkbox' name='lc_pilot' value='1' " . ($lc ? "checked=checked" : "") . "/> Life Critical Pilot Only&nbsp;&nbsp;
				</td>
				<td style = 'text-align: right; padding-bottom: 3px;'>
					" . (!empty($_GET["delegate"]) ? "<input type='hidden' name='delegate' value='{$_GET["delegate"]}'/>" : "") . "
					<input style = 'height: 18px;' type = 'submit' value = 'Filter'/>
				</td>
			</tr>
		</table>
	</form>
</div>
<div style = ''>
	<div id = 'container' style = 'width: 1100px; border: 1px solid #000; margin: 5px; height: 300px;'></div>
</div>
";
?>