
<?php
/**
 * Created by PhpStorm.
 * User: shinds
 * Date: 12/29/2016
 * Time: 4:11 PM
 */
header("X-UA-Compatible: IE=11");
require_once("../../src/php/require.php");?>
<?php $delegate = (!empty($_GET["delegate"]) && $_GET["delegate"] != 0 ? "&delegate={$_GET["delegate"]}" : "") ?>
<?php
mcl_Html::s(mcl_Html::SRC_JS, "
		dojo.ready(function(){
			sot.home.setReportDelegate({$_GET["delegate"]});
			sot.home.initiate('{$usid}');
		});
	");



$year=date("Y");

echo "
<div style='padding-top: 15px; padding-left: 270px;'>


    <div  style = 'width: 510px; overflow: hidden; text-align: right;'>
       
        <BUTTON id='next' class=btn>
                <IMG style='HEIGHT: 12px; WIDTH: 12px; POSITION: relative; TOP: -2px' src='http://lnx829:63002/src/img/arrowBack.png'>
        </BUTTON>
             
             <input  style='width:3em' align='center' readonly' id='year' name='year' value='" . $year . "' />
             
        <BUTTON id='previous' class=btn>
                <IMG style='HEIGHT: 12px; WIDTH: 12px; POSITION: relative; TOP: -1px' src='http://lnx829:63002/src/img/arrowForward.png'>
        </BUTTON>
        <button  id='btn-export'>Save As Picture </button> 
        <span id='txtYearMessage'></span>
        <span id='txtWeekMessage'></span>
    </div>
</div>

<div id='myModal' class='modal'>


 <div class='modal-content shadow close' id='btn-close'>&times;
 <div><img src='' title='Right click and save image' style='width:90%;height:auto;align-content: center; ' id='chart-imgs'></div>
 </div>
</div>
<div style='width: 100%;text-align: center'>
    <div id='meter'>
        <span style='width:25%'></span>
    </div>
</div>

<div id='newmeter' style='width: 100%;height: 100%;text-align: center '>
    <canvas id='canvas' style='padding: 10px; border: solid  #004990;  border-image: none;left: 20px; top:40px; position: relative'></canvas>
</div>

";

?>

<script src="../../src/js/chart/Chart.bundle.js"></script>

<script>

    (function(){

        var chart;


        var MONTHS = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        var minscale=0;            // The minimum scales

        // Data Sets
        var data1= [];  // Percentage
        var data2= [];     // YTD

        var t= [];                  // Target series

        // Render the chart
        function RenderChart(){
            // The chart configuration values
            var config= {
                // The data set
                type:"bar",
                data: {
                    labels: MONTHS,
                    datasets: [
                        {
                            label: "Target",
                            type:"line",
                            data:t,
                            borderDash: [15, 15],
                            backgroundColor: "#49a942",
                            borderColor: "#49a942",
                            fill: false,
                            pointRadius:0,
                            pointHoverRadius:3,
                            borderWidth:3
                        },
                        {
                            label: "YTD",
                            type:"line",
                            data:data2,
                            backgroundColor: "#b31937",
                            borderColor: "#b31937",
                            fill: false,
                            lineTension:0,
                            pointRadius:0,
                            pointHoverRadius:3,
                            borderWidth:0
                        },
                        {
                            label: "Months (" + GetYear() +")",
                            data:data1,
                            backgroundColor: "#004990",
                            borderColor: "#004990",
                            fill: false,
                            borderDash: [5, 5],
                            pointRadius: 15,
                            pointHoverRadius: 3,
                            borderWidth:0
                        }
                    ]
                },

                // The chart options
                options: {
                    responsive: true,
                    legend: {
                        position: 'bottom'
                    },
                    hover: {
                        mode: 'index'
                    },
                    scales: {
                        xAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Month'
                            }
                        }],
                        yAxes: [{
                            afterTickToLabelConversion:function(data){
                                var ylab=data.ticks;
                                ylab.forEach(function(labels,i){
                                    if(i%10 == 0){

                                    } else {
                                        ylab[i]='';
                                    }
                                })
                            },
                            display: true,

                            ticks:{
                                min: minscale,
                                max:100,
                                stepSize:1,
                                beginAtZero: true

                            },
                            gridLines:{
                                tickMarkLength	:1
                            },
                            scaleLabel: {
                                display: true,
                                labelString: '% Compliant'
                            }


                        }]
                    },
                    title: {
                        display: true,
                        text: 'Observation Compliance'
                    }

                }

            };

            // Create and plot the chart object
            chart =new Chart(
                document.getElementById("canvas").getContext("2d"),
                config
            );
        }

        // get  the usrid from the hidden field
        function GetUserid(){
            // return document.getElementById("hdnUserid").value;// window.userid;
            // return "e52716";
            // return window.userid;
            return '<?php echo $usid;?>';
        }

        // Get the end Date
        function GetYear(){
            return document.getElementById('year').value;
            //return "2016";
        }

        // Get the data from the backend
        function GetData(){
            ShowMessage("Loading Data ...... ");
            sot.tools.ajax.submit("get", "json", {
                f:"getObsComplianceData",
                userId:GetUserid(),
                yr:GetYear()
            }, function(_e){
                console.log(_e);
                // Parse the JSOn response
                var json=_e;


                data1=[]; // Percentage
                data2=[]; // Target



                // Parse & prepare the data
                for(var i=0;i<json.length;i++){
                    if(json[i].status!=0)
                    {
                        switch (json[i].status)
                        {
                            case(1):
                                // No Team
                                ShowMessage("No Team Found","Error");
                                break;
                            case(2):
                                // No Data
                                ShowMessage("No Data Found","Error");
                                break;
                            case(3):
                                // No LD Data
                                ShowMessage("No LD Data Found","Error");
                                break;
                        }
                        return true;
                    }
                    if(json[i].ytd>=0)
                    {
                        data1.push(json[i].percentage);
                        data2.push(json[i].ytd);
                    }
                }
                console.log(data1);
                console.log(data2);
                ShowMessage("");

                // Render the chart
                RenderChart();
                return true;
            }, true, true);
        }

        // Get the target
        function GetTarget(){
            ShowMessage("Target Data ...... ");
            sot.tools.ajax.submit("get", "text", {
                f: "getObsTarget",
                TARGETTYPE:"OC",
                yr:GetYear()
            }, function(_e){

                _e=JSON.parse(_e);
                console.log(_e);


                //  if(_e.target=="-1")
                //      return true;

                // Create target array
                t=[];
                for(var i=0;i<=11;i++){
                    t.push( _e.target);
                }

                // Show the Message for the user about the duration of the report

                ShowWeekMessage(
                    GetYear(), // Get target year from from the text box
                    _e.startDate, // The start date of the year
                    _e.endDate, // The end date of the year
                    _e.currentWeek // The current reporting week number
                );


                console.log(t);
                // Start Getting data
                GetData();
                //RenderChart();

                return true;
            }, true, true);
        }

        // Refresh date
        function Refresh() {
            ShowMessage("Loading ...... ","Info");
            ShowWeekMessage("","","",-1)
            GetTarget();

        }

        // Show the message to the usr when it is loading or an error
        // The message to display
        function ShowMessage(msg){
            document.getElementById("meter").innerText=msg;
        }

        // Display the message if the week
        // yr: the targeted year
        // syr: Start date
        // eyr: End date
        // cwn: Current year week num
        function ShowWeekMessage(yr,syr,eyr,cwn){
            if(yr=="")
            {
                document.getElementById("txtYearMessage").innerText=
                    document.getElementById("txtWeekMessage").innerText ="";

            }
            else
            {

                document.getElementById("txtYearMessage").innerText =
                    yr + " Compliance Year: " + syr + " to " +eyr;

                if(cwn>0){



                    document.getElementById("txtWeekMessage").innerText =
                        "Week " + cwn;

                }
            }

        }



        /////////////////////////////////////
        // On page load will  trigger the refresh action to connect to the backend and grab the data
        window.onload = function(){
            try{
                // Set the time out in case there is an error on the backend
                /*
                 var timeout=true;
                 setTimeout(function () {
                 if(timeout)
                 ShowMessage("","");
                 },30000);
                 */
                // document.getElementById("year").value="2016";

                // Export to PNG Button
                var btn=document.getElementById("btn-export");

                // The popup  modal
                var modal = document.getElementById('myModal');

                // Get the <span> element that closes the modal
                var span = document.getElementById("btn-close");

                // When the user clicks the button, open the modal
                btn.onclick = function() {
                    var d= document.getElementById("canvas").toDataURL("image/png");
                    document.getElementById("chart-imgs").setAttribute("src",d);
                    modal.style.display = "block";
                };

                // When the user clicks on <span> (x), close the modal
                span.onclick = function() {
                    modal.style.display = "none";
                };

                //////////////////////////////////////
                //  Add onclick events for buttons to navigate throw the years and reflect the new changes
                // When the user click on the next button
                document.getElementById("previous").onclick=function(){
                    document.getElementById("year").value
                        = (++document.getElementById("year").value); // Increment year
                    Refresh(); // refresh the page
                };

                // When the user click on the previous button
                document.getElementById("next").onclick=function(){
                    document.getElementById("year").value
                        = (--document.getElementById("year").value); // Decrement year
                    Refresh(); // refresh the page
                };

                // When the user clicks anywhere outside of the modal, close it
                window.onclick = function(event) {
                    if (event.target == modal) {
                        modal.style.display = "none";
                    }
                };

                Refresh();


            }
            catch (ex){
                console.log(ex);

            }

        };

    })();



</script>
<style>
    canvas{
        width: 100% !important;
        max-width: 800px;
        height: auto !important;
        display: block;
        margin: 0 auto;
    }
</style>
