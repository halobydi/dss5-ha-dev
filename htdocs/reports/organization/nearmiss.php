<?php
/*
Change Log
DS-148 12-21-2016  priority replaced with Serious? GalaxE Detroit Edward King

*/
require_once("/opt/apache/servers/soteria/htdocs/src/php/require.php");
mcl_Html::js(mcl_Html::HIGHSTOCK);

$privileges = auth::check('privileges');
$admin = false;
if($privileges["MANAGE_NEAR_MISS"]){
	$admin = true;
}

if(empty($_GET["start"])) {
	$start = '01/01/' . date('Y');
}

$oci = new mcl_Oci("soteria");
if(isset($_GET["nm"])){
	echo "<div class = '" . ($_GET["nm"] == "1" ? "success" : ($_GET["nm"] == "0" ? "error" : "")) . "' style='margin-top: 5px; margin-left: 5px;'>
		" . ($_GET["nm"] == "1" ? "Successfully saved Near Miss Incident." : ($_GET["nm"] == "0" ? "Unable to save Near Miss Incident." : "")) . "
	</div>";
}
//DS-196
mcl_Html::s(mcl_Html::SRC_CSS, "
	table.sortable {
		width:				100%;
	}
	tr.even {
		background-color:	#f0f0f0;
	}
	tr.odd {
		background-color:	#d8d8d8;
	}
	div.nearMissDetails
	{
	    text-align: left;
	}
");

mcl_Html::s(mcl_Html::SRC_JS, "
	var current = null;
	var toggle = function(form, hidePrevious) {
		if(dojo.byId(form).style.display == 'block') {
			dojo.byId(form).style.display = 'none';
			dojo.byId(form + '_toggle').innerHTML = '<img src=\"../../src/img/expand.png\"/>';
			current = form;
		} else {
			dojo.byId(form).style.display = 'block';
			dojo.byId(form + '_toggle').innerHTML = '<img src=\"../../src/img/collapse.png\"/>';
		}
	
	};
");

$sql = "SELECT * FROM NEAR_MISS_ACCIDENT_LOCATIONS ORDER BY AL_LOCATION";
$locations = "";
while($row = $oci->fetch($sql)){
	$locations .= "<option value='{$row["AL_ID"]}' " . ($_GET["loc"] == $row["AL_ID"] ? "selected=selected" : "") . ">{$row["AL_LOCATION"]}</option>";
}

$sql = "SELECT * FROM NEAR_MISS_PLANTS ORDER BY PLANT";
$plants = "";
while($row = $oci->fetch($sql)){
	$plants .= "<option value='{$row["PLANT_ID"]}' " . ($_GET["dte_loc"] == $row["PLANT_ID"] ? "selected=selected" : "") . ">{$row["PLANT"]}</option>";
	$completed_by_plants .= "<option value='{$row["PLANT_ID"]}' " . ($_GET["work_dte_loc"] == $row["PLANT_ID"] ? "selected=selected" : "") . ">{$row["PLANT"]}</option>";
}

$sql = "SELECT * FROM NEAR_MISS_CATEGORIES WHERE PARENT_CAT_ID IS NULL ORDER BY CAT_TEXT";
$categories = "";
while($row = $oci->fetch($sql)){
	$categories .= "<option value='{$row["CAT_ID"]}' " . ($_GET["cat"] == $row["CAT_ID"] ? "selected=selected" : "") . ">{$row["CAT_TEXT"]}</option>";
}
?>	
<div>
	<form method = 'GET' action='nearmiss.php' style = 'overflow: hidden; padding: 5px;'>
		<table style='border: 1px solid #000;'>
			<!--<tr><td style='background-color: #f0f0f0; padding: 2px;'>Filter Near Miss</td><td style='background-color: #f0f0f0; padding: 2px;'></td></tr>-->
			<tr>
				<td colspan= '2' style='background-color: #92b9dc; border: 1px solid #000;padding: 5px; font-weight: bold;'>Filter Near Miss</td>
			</tr>
			<tr style = 'vertical-align: bottom;'>
				<td style='font-size: 10px; font-weight: normal; background-color: #e3e1e3; padding: 2px;'>Near Miss Date</td>
				<td style='background-color: #e3e1e3; padding: 2px;'>
					<input style = 'height: 12px; border: 1px solid #000; width: 100px;' type = 'text' name = 'start' id = 'start' value = '<?php echo $start;?>'/> 
					<img style = 'vertical-align: bottom;' src='../../src/img/calendar.gif' alt='' id='tcal' onmouseover='setup_cal("tcal", "start");' />
					<span style='font-size: 10px;'>to </span> 
					<input style = 'height: 12px; border: 1px solid #000; width: 100px;'  type = 'text' name = 'end' id = 'end' value = '<?php echo $end; ?>' /> 
					<img style = 'vertical-align: bottom;' src='../../src/img/calendar.gif' alt='' id='tcal2' onmouseover='setup_cal("tcal2", "end");' />
				</td>
			</tr>
			<tr style = 'vertical-align: bottom;'>
				<td style='font-size: 10px; font-weight: normal; background-color: #f0f0f0; padding: 2px;'>Organization</td>
				<td style='background-color: #f0f0f0; padding: 2px;'>
					<select name='org' id='org'>
						<option value=''></option>
						<?php echo $org_select; ?>
					</select> <span style='font-size: 10px; font-style: italic;'>(ie: Distribution Operations)</span>
				</td>
			</tr>
			<tr style = 'vertical-align: bottom;'>
				<td style='font-size: 10px; font-weight: normal; background-color: #e3e1e3; padding: 2px;'>DTE Work Location<br/>Reported From</td>
				<td style='background-color: #e3e1e3; padding: 2px;'>
					<select name='work_dte_loc' style='width: 250px;'>
						<option value=''></option>
						<?php echo $completed_by_plants; ?>
					<select> <span style='font-size: 10px; font-style: italic;'>(ie: Ann Arbor Service Center)</span>
				</td>
			</tr>
			<tr style = 'vertical-align: bottom;'>
				<td style='font-size: 10px; font-weight: normal; background-color: #f0f0f0; padding: 2px;'>Near Miss DTE Work Location</td>
				<td style='background-color: #f0f0f0; padding: 2px;'>
					<select name='dte_loc' style='width: 250px;'>
						<option value=''></option>
						<?php echo $plants; ?>
					<select> <span style='font-size: 10px; font-style: italic;'>(ie: Pontiac Service Center)</span>
				</td>
			</tr>
			<tr style = 'vertical-align: bottom;'>
				<td style='font-size: 10px; font-weight: normal; background-color: #e3e1e3; padding: 2px;'>Near Miss Location</td>
				<td style='background-color: #e3e1e3; padding: 2px;'>
					<select name='loc'>
						<option value=''></option>
						<?php echo $locations; ?>
					<select> <span style='font-size: 10px; font-style: italic;'>(ie: Parking Lot)</span>
				</td>
			</tr>
			<tr style = 'vertical-align: bottom;'>
				<td style='font-size: 10px; font-weight: normal; background-color: #f0f0f0; padding: 2px;'>Near Miss Category</td>
				<td style='background-color: #f0f0f0; padding: 2px;'>
					<select name='cat' style='width: 300px;'>
						<option value=''></option>
						<?php echo $categories; ?>
					</select>
				</td>
			</tr>
            <!--
			<tr style = 'vertical-align: bottom;'>
				<td style='font-size: 10px; font-weight: normal; background-color: #f0f0f0; padding: 2px;'>Priority</td>
				<td style='background-color: #f0f0f0; padding: 2px;'>
					<select name='priority' style='width: 300px;'>
						<option value=''></option>
						<option value='L' <//?=$_GET['priority'] == 'L' ? "selected=selected" : ""?>>Low</option>
						<option value='M' <//?=$_GET['priority'] == 'M' ? "selected=selected" : ""?>>Medium</option>
						<option value='H' <//?=$_GET['priority'] == 'H' ? "selected=selected" : ""?>>High</option>
					</select>
				</td>
			</tr>
			-->
            <!--DS-194 -->
            <tr style = 'vertical-align: bottom;'>
                <td style='font-size: 10px; font-weight: normal; background-color: #f0f0f0; padding: 2px;'>Serious?</td>
                <td style='background-color: #f0f0f0; padding: 2px;'>
                    <select name='isserious' style='width: 300px;'>
                        <option value=''></option>
                        <option value='1' <?=$_GET['isserious'] == '1' ? "selected=selected" : ""?>>Yes</option>
                        <option value='0' <?=$_GET['isserious'] == '0' ? "selected=selected" : ""?>>No</option>
                        <option value='2' <?=$_GET['isserious'] == '2' ? "selected=selected" : ""?>>NA</option>
                    </select>
                </td>
            </tr>
			<tr style = 'vertical-align: bottom;'>
				<td style='font-size: 10px; font-weight: normal; background-color: #e3e1e3; padding: 2px;'>Requires Investigation?</td>
				<td style='background-color: #e3e1e3; padding: 2px;'>
					<select style='width: 50px;' name='req_inv'>
						<option value=''></option>
						<option value='1' <?php echo ($_GET["req_inv"] == "1" ? "selected=selected" : "") ?> >Yes</option>
						<option value='0' <?php echo ($_GET["req_inv"] == "0" ? "selected=selected" : "") ?> >No</option>
					</select>
				</td>
			</tr>
			<tr style = 'vertical-align: bottom;'>
				<td style='font-size: 10px; font-weight: normal; background-color: #f0f0f0; padding: 2px;'>Investigation Complete?</td>
				<td style='background-color: #f0f0f0; padding: 2px;'>
					<select style='width: 50px;' name='inv'>
						<option value=''></option>
						<option value='1' <?php echo ($_GET["inv"] == "1" ? "selected=selected" : "") ?> >Yes</option>
						<option value='0' <?php echo ($_GET["inv"] == "0" ? "selected=selected" : "") ?> >No</option>
					</select>
				</td>
			</tr>

<?php

		echo "<tr>
				<td style='background-color: #e3e1e3; padding: 2px;'></td>
				<td style='text-align: left; background-color: #e3e1e3; padding: 2px;'>
					" . (!empty($_GET["delegate"]) ? "<input type='hidden' name='delegate' value='{$_GET["delegate"]}'/>" : "") . "
					<input type = 'submit' style = \"height: 19px;\" value = 'Filter'/>
					<input onclick = \"dojo.byId('start').value = '{$startWeek}'; dojo.byId('end').value = '{$endWeek}';\" type = \"submit\" style = \"height: 19px;\" value = \"Current Week\"/>
					<input onclick = \"dojo.byId('start').value = '{$startWeekPast}'; dojo.byId('end').value = '{$endWeekPast}';\"  type = \"submit\" style = \"height: 19px;\" value = \"Past Week\"/>
				</td>
			</tr>
		</table>
	</form>
</div>				
";

$activity = array();
if($org != "") {
	$where .= " AND N.ORG_CODE = '{$org}'";
}
if($_GET["work_dte_loc"] != "" && isset($_GET["work_dte_loc"])) {
	$where .= " AND N.COMPLETED_BY_PLANT = '{$_GET["work_dte_loc"]}'";
}
if($_GET["dte_loc"] != "" && isset($_GET["dte_loc"])) {
	$where .= " AND N.PLANT = '{$_GET["dte_loc"]}'";
}
if($_GET["loc"] != "" && isset($_GET["loc"])) {
	$where .= " AND N.LOCATION = '{$_GET["loc"]}'";
}
if($_GET["cat"] != "" && isset($_GET["cat"])) {
	$where .= " AND CATEGORY = '{$_GET["cat"]}'";
}
if($_GET["inv"] != "" && isset($_GET["inv"])) {
	$where .= " AND (CASE WHEN INVESTIGATION_COMPLETE = 1 OR TRIM(INVESTIGATION) IS NOT NULL THEN 1 ELSE 0 END = {$_GET["inv"]} OR REQUIRES_INVESTIGATION = 0)";
}
if($_GET["req_inv"] != "" && isset($_GET["req_inv"])) {
	$where .= " AND REQUIRES_INVESTIGATION = {$_GET["req_inv"]}";
}
/*if($_GET['priority'] != "") {
	$priority = strtoupper($_GET["priority"]);
	if(strlen($priority) > 1) $priority = 1;
	$where .= " AND PRIORITY = '{$priority}'";
}*/
//DS-194
if($_GET["isserious"] !=""){
    if($_GET["isserious"]==2){
        $where .= " AND ISSERIOUS IS NULL";
    }
    else
    {
        $where .= " AND ISSERIOUS = {$_GET["isserious"]}";
    }

}
//DS-195
$sql = <<<SQL
	SELECT	NM_ID AS "ID", 
			TO_CHAR(INCIDENT_DATE, 'MM/DD/YYYY') AS "Incident Date",
			INCIDENT_TIME AS "Incident Time",
			C.CAT_TEXT AS "Category",
			NVL(O.ORG_TITLE, 'Unknown') AS "Organization",
			--NVL(O.ORG_CODE, 'UNKNOWN') AS "Organization",
			P2.PLANT AS "Employee DTE Work Location",
			P.PLANT AS "Near Miss DTE Location",
			AL.AL_LOCATION AS "Location",
			LOCATION_DESCRIPTION AS "Location Description",
			DECODE(REQUIRES_INVESTIGATION, 1, 'Yes', 'No') AS "Investigation Required?",
			CASE WHEN REQUIRES_INVESTIGATION = 0 THEN NULL ELSE DECODE(TRIM(INVESTIGATION), NULL, INVESTIGATION_COMPLETE, 1) END AS "*Investigation Complete?",
			E.PATH,
			CASE WHEN PRIORITY = 'H' THEN 'High' WHEN PRIORITY = 'M' THEN 'Medium' WHEN PRIORITY = 'L' THEN 'Low' ELSE 'NA' END AS "Priority",
			CASE WHEN ISSERIOUS  IS NULL THEN 'NA' WHEN ISSERIOUS = 0 THEN 'No' WHEN ISSERIOUS = 1 THEN 'Yes' ELSE 'NA' END AS "Serious?"
	FROM	NEAR_MISS_OBSERVATIONS N
	LEFT JOIN EMPLOYEES E ON E.USID = N.COMPLETED_BY
	--LEFT JOIN EMPLOYEES S ON S.USID = N.COMPLETED_BY_SUPERVISOR
	--LEFT JOIN EMPLOYEES D ON D.USID = N.DIRECTOR
	LEFT JOIN NEAR_MISS_CATEGORIES C ON C.CAT_ID = N.CATEGORY
	--LEFT JOIN NEAR_MISS_CATEGORIES C_SUB ON C.CAT_ID = N.SUB_CATEGORY
	LEFT JOIN NEAR_MISS_ACCIDENT_LOCATIONS AL ON AL.AL_ID = N.LOCATION
	LEFT JOIN NEAR_MISS_PLANTS P ON P.PLANT_ID = N.PLANT
	LEFT JOIN NEAR_MISS_PLANTS P2 ON P2.PLANT_ID = N.COMPLETED_BY_PLANT
	LEFT JOIN (SELECT DISTINCT ORG_CODE, ORG_TITLE FROM ORGANIZATIONS GROUP BY ORG_CODE, ORG_TITLE) O ON O.ORG_CODE = N.ORG_CODE
	WHERE	INCIDENT_DATE BETWEEN TO_DATE('{$start} 00:00:00', 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE('{$end} 23:59:59', 'MM/DD/YYYY HH24:MI:SS')
	{$where}
	ORDER BY NM_ID DESC
SQL
;


while($row = $oci->fetch($sql)) {
	$activity["nm"][] = $row;
}

$forms = array("nm"	=> array("Near Miss Incidents", "Includes all incidents completed"));
$view = array("nm"	=> "sot.nm.view(ID_PLACEHOLDER); return false;");

$orgChart = array();

echo "<div style='margin: 5px; font-size: 10px;'>";
echo "*A Near Miss is categorized as <b>complete</b> if the user inputs 'Investigation Results' or if the user responds with a 'Yes' to the following question: 'Has the investigation been completed?'";
echo "</div>";
foreach($forms as $key=>$value) {
	echo "<div class='header' style='width: 1350px;'>";
	echo "<div class='caption' onclick='toggle(\"{$key}\", true);'>";
	echo "<span class='toggle' id='{$key}_toggle'><img src='../../src/img/" . ($org == "" ? "expand" : "collapse") . ".png'/></span>";
	echo "{$value[0]} <span style='font-size: 10px;'>{$value[1]}</span>";
	echo "</div>";
	echo "<div title='Export to Excel' class='excel' onclick='window.open(\"nearmiss_excel.php?start={$start}&end={$end}&org={$org}&loc={$_GET["loc"]}&dte_work_loc={$_GET["dte_work_loc"]}&cat={$_GET["cat"]}&inv={$_GET["inv"]}&req_inv={$_GET["req_inv"]}&isserious={$_GET["isserious"]}\");'><div style='border-top: 1px solid #f0f0f0; border-left: 1px solid #f0f0f0; padding: 5px;'><img src='../../src/img/excel.png'/></div></div>";
	echo "<div title='Total Count' class='count'><div style='border-top: 1px solid #f0f0f0; border-left: 1px solid #f0f0f0; padding: 5px;'>" . count($activity[$key]) ."</div></div>";
	echo "</div>";
	echo "<div id='{$key}' style='width: 1350px; display: " . ($org == "" ? "none" : "block") . ";' class='activity'>";
		$firstpass = true;
		if(count($activity[$key]) > 0) {
			echo "<table class='sortable'>";
			foreach((array)$activity[$key] as $value) {	
				$in_path = strpos($value["PATH"], $user["usid"]);
				unset($value["PATH"]);
				if($admin || ($in_path !== false)) {
					$edit = array("nm"	=> "../../forms/nearmiss.php?nm_id=ID_PLACEHOLDER&mode=edit");
					$delete = array("nm"	=> "sot.nm._delete(ID_PLACEHOLDER); return false");
				} else {
					$edit = array("nm"	=> false);
					$delete = array("nm"	=> false);
				}
				
				if($firstpass) {
					echo "<tr>";
						foreach($value as $columnHeader=>$columnValue) {
							echo "<th style='padding: 5px; border-bottom: 1px solid #000;'><div class='nearMissDetails'>{$columnHeader}</div></th>";
						}
						echo "<td style='border-bottom: 1px solid #000; width: 40px;'>&nbsp;</td>";
						echo "<td style='border-bottom: 1px solid #000; width: 40px;'>&nbsp;</td>";
						echo "<td style='border-bottom: 1px solid #000; width: 40px;'>&nbsp;</td>";
					echo "</tr>";
					
					$firstpass = false;
				}
				
				//echo "<tr style='background-color:" . (++$x % 2 == 0 ? '#f0f0f0' : '#d8d8d8') . "'>";
				echo "<tr class='" . (++$x % 2 == 0 ? 'even' : 'odd') . "'>";
					foreach($value as $columnHeader=>$columnValue) {
					    /*DS-196
					    $priorityClass = "";
					DS-196
					Taking out the Css of the class
					if($columnHeader == 'Priority') {
						//$priorityClass = strtolower($columnValue);
					}*/
					echo "<td ". ($columnValue == "Yes" || $columnValue == "No" ? "" : "") . ">" . ($columnValue === "1"? "<div style='text-align: left;'><img src='../../src/img/check.png'></div>" : ($columnValue === "0" ? "<div style='text-align: left;'><img src='../../src/img/x.png'></div>" : $columnValue)) ."</td>";
				}
				
				$viewLink = str_replace("ID_PLACEHOLDER", "{$value["ID"]}", $view[$key]);
				echo "<td style='text-align: center; font-size: 10px;'>[ <a href='#' onclick='{$viewLink}'>View</a> ]</td>";
				if($edit[$key]) {
					$editLink = str_replace("ID_PLACEHOLDER", "{$value["ID"]}", $edit[$key]);
					echo "<td style='text-align: center; font-size: 10px;'>[ <a  " . ($value["Complete?"] == 1 ? "disabled=disabled href='#'" : "href='{$editLink}&returnto=../reports/organization/nearmiss&delegate={$_GET["delegate"]}'") . " >Edit</a> ]</td>";
				} else {
					echo "<td style='text-align: center; font-size: 10px;'>[ <a href='#' onclick='return false;' disabled=disabled >Edit</a> ]</td>";
				}
				if($delete[$key]) {
					$deleteLink = str_replace("ID_PLACEHOLDER", "{$value["ID"]}", $delete[$key]);
					echo "<td style='text-align: center; font-size: 10px;'>[ <a href='#' onclick='{$deleteLink}'>Delete</a> ]</td>";
				} else {
					echo "<td style='text-align: center; font-size: 10px;'>[ <a href='#' onclick='return false;' disabled=disabled >Delete</a> ]</td>";
				}
				echo "</tr>";
				
				$orgChart[$value["Organization"]]++;
			}
			echo "</table>";
		} else {
			echo "<div style='padding: 5px;'>There is no data to display with the current filter.</div>";
		}
	echo "</div>";
}


$categories = '';
$data = '';
asort($orgChart);
foreach($orgChart as $key => $value){
	$categories .= (empty($categories) ? "" : ", ") . "'" . str_replace("'", "\'", "{$key}") . "'";
	$data .= (empty($data) ? "" : ", ") . "'{$value}'";
}
$data = str_replace("'", "", $data);

if($org == "") {
mcl_Html::s(mcl_Html::SRC_JS, <<<JS
	var highlight;
	$(function () {
		var chart;
		$(document).ready(function() {
			chart = new Highcharts.Chart({
				chart: {
					renderTo: 'chart',
					type: 'column'
				},
				title: {
					text: 'Near Miss by Organization'
				},
				subtitle: {
					text: ''
				},
				xAxis: [{
					categories: [{$categories}],
					labels: {
						enabled: true,
						align: 'center'
					},
					title: {
						text: ''
					}
				}],
				yAxis: {
					min: 0,
					title: {
						text: ''
					}
				},
				legend: {
					enabled: false
				},
				plotOptions: {
					series: {
						shadow: false
					}
				},
				series: [{
					name: 'Near Miss Count',
					data: [{$data}],
					color: '#a80000',
					dataLabels: {
						enabled: true
					}
		
				}]
			});
		});
	});	
JS
);
echo "<div id='chart' style='width: 1350px; border: 1px solid #000; margin-left: 5px;'></div>";
}
?>
