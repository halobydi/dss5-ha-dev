<?php

// function errorHandler($errno, $errstr, $errfile, $errline, $errcontext){}
// set_error_handler('errorHandler');
require_once("mcl_Oci.php");
$oci = new mcl_Oci('soteria');
$countermeasures = array(150, 151, 152, 153, 153.5, 154, 154.5, 155, 156, 157);

function doubleQuote($str) {
	$unsafeChars = array("'", '"');
	$repChars = array("''", '""');
	return str_replace($unsafeChars, $repChars, $str);
}

$where = '';
if(isset  ($_REQUEST['WHERE'])) {
	foreach($_REQUEST['WHERE'] as $key=>$val){
		$where .= "AND {$key} = " . doubleQuote($val);
	}
}
$obSql = 'SELECT
		      HP_ID as "ID",
		      ORGANIZATION AS "Organization",
		      COMPLETED_BY AS "Completed By",
		      TO_CHAR(COMPLETED_DATE, \'MM/DD/YYYY\') AS "Completed Date",
		      TO_CHAR(INCIDENT_DATE, \'MM/DD/YYYY\') AS "Incident Date",
		      COMMENTS AS "Comments",
		      COMPLETED_BY_SUPERVISOR AS "Completed by Supervisor",
		      COMPLETED_BY_DIRECTOR AS  "Completed by Director",
		      EVENT_TYPE AS "Event Type",
		      COMPLETED AS "Completed",
		      INCIDENT_TYPE AS "Incident Type",
		      CATEGORY AS "Category"
          FROM HP_OBSERVATIONS 
          WHERE
		      1=1
		      '.$where.'
		  ORDER BY HP_ID';

$observations = array();
while($row = $oci->fetch($obSql)) {
	$id = $row['ID'];
	$observations[$id] = $row;
	$anSql = $oci->parse('SELECT 
                              ITEM_NUM, 
                              SUBITEM_NUM, 
                              ANSWER 
                          FROM 
                              HP_ANSWERS 
                          WHERE HP_ID = :id 
                          ORDER BY ITEM_NUM, SUBITEM_NUM');
	$oci->bind($anSql, array(':id'=>$id));
	while($row = $oci->fetch($anSql)) {
		if($row['SUBITEM_NUM']==null)
			$observations[$id]['answers'][$row['ITEM_NUM']]['a'] = $row['ANSWER'];
		else
			$observations[$id]['answers'][$row['ITEM_NUM']]['subquestions'][$row['SUBITEM_NUM']] = $row['ANSWER'];
	}
}

$questions = array();
$qSql = "SELECT 
             I.ITEM_NUM, 
             S.SUBITEM_ID, 
             I.ITEM_CATEGORY, 
             I.ITEM_TEXT, 
             S.SUBITEM_TEXT 
         FROM 
             HP_ITEMS I 
         LEFT JOIN 
             (
             SELECT 
                 * 
             FROM 
                 HP_SUBITEMS 
             WHERE 
                 ACTIVE = 1
             ) 
             S 
             ON 
             ITEM_NUM = PARENT_ID 
         WHERE 
             (I.ACTIVE = 1 OR I.ITEM_NUM IN (300, 301, 304, 305))
         ORDER BY 
             I.ITEM_NUM, 
             S.SUBITEM_ID";
while($row = $oci->fetch($qSql)) {
	$qID = $row['ITEM_NUM'];
	$questions[$qID]['text'] = $row['ITEM_TEXT'];
	$questions[$qID]['category'] = $row['ITEM_CATEGORY'];
	if($row['SUBITEM_ID']) {
		$questions[$qID]['subquestions'][$row['SUBITEM_ID']] = $row['SUBITEM_TEXT'];
	}
}

//using built in php function to write to csv
$csv = '';
$out = fopen('php://output', 'w');
ob_start();

//single line version
if(isset($_REQUEST['single'])) {
	$headers = array();

	//build headers
	foreach($observations[$id] as $key=>$val) {
		if($key=='text'||$key=='category'||$key=='subquestions'||$key=='answers') continue;
		$headers[] = $key;
	}
	$row = $headers;
	foreach($questions as $key=>$val) {
		$row[] = $questions[$key]['text'];
		if(isset($questions[$key]['subquestions'])) {
			foreach($questions[$key]['subquestions'] as $key => $val)
				$row[] = $val;
		}
	}

	fputcsv($out, $row);

	$row = array();
	//build observations
	foreach($observations as $key=>$val) {
		$obs = $observations[$key];

		//grab summary data first
		foreach($obs as $q => $val) {
			if($q=='answers') continue;
			$row[] = $val;
		}

		//grab all non null answers
		foreach($questions as $q=>$arr) {
			if(isset($obs['answers'][$q]['a']))
				$row[] = $obs['answers'][$q]['a'];
			else
				$row[] = '';

			//grab all non null subquestion answers, when applicable
			if(isset($arr['subquestions'])) {
				foreach($arr['subquestions'] as $subq=>$qtext) {
					if(isset($obs['answers'][$q]['subquestions'][$subq]))
						$row[] = $obs['answers'][$q]['subquestions'][$subq];
					else
						$row[] = '';
				}
			}
		}
		fputcsv($out, $row);
		$row = array();
	}

	//shows countermeasures
	if(isset($_REQUEST['c2s'])) {
		$row = array();
		fputcsv($out, $row);
		$row = array('Countermeasures by Observation');
		fputcsv($out, $row);

		//print headers
		$row = array('ID');
		foreach($countermeasures as $qNum) {
			$row[] = $questions[$qNum]['text'];
		}
		fputcsv($out, $row);

		foreach($observations as $key=>$val) {
			$metadata = array($val['ID']);
			$obs = $val['answers'];
			$answeredCs = array();
			if(!isset($obs[152]['subquestions'])) {
				$numCs = 0;
				foreach($countermeasures as $qNum) {
					if(isset($obs[$qNum]['subquestions'])) {
						$answeredCs = array_keys($obs[$qNum]['subquestions']);
						$numCs = true;
					}
				}
				if(!$numCs) continue;
			}
			else
				$answeredCs = array_keys($obs[152]['subquestions']);
			foreach($answeredCs as $c) {
				$row = $metadata;
				foreach($countermeasures as $cQ) {
					$row[] = (isset($obs[$cQ]['subquestions'][$c])) ? $obs[$cQ]['subquestions'][$c] : '';
				}
				fputcsv($out, $row);
			}

		}
	}
}
//multi-line version
else if(isset($_REQUEST['multi'])) {
	//build headers
	foreach($observations[$id] as $key=>$val) {
		if($key=='text'||$key=='category'||$key=='subquestions'||$key=='answers') continue;
		$row[] = $key;
	}
	$row[] = 'Question';
	$row[] = 'Answer';

	fputcsv($out, $row);

	foreach($observations as $key=>$val) {
		$obs = $observations[$key];
		$metadata = array();
		foreach($obs as $key=>$val) {
			if($key=='text'||$key=='category'||$key=='subquestions'||$key=='answers') continue;
			$metadata[] = $val;
		}

		foreach($obs['answers'] as $q=>$arr) {
			$answer = $obs['answers'][$q];
			if(isset($answer['a']) && $answer['a'] != '') {
				$row = $metadata;
				$row[] = $questions[$q]['text'];
				$row[] = $answer['a'];
				fputcsv($out, $row);
			}

			if(isset($answer['subquestions'])) {
				foreach($answer['subquestions'] as $subq=>$subans) {
					if($subans != ''&&(in_array($subq, $countermeasures))) {
						$row = $metadata;
						$row[] = $questions[$q]['subquestions'][$subq];
						$row[] = $subans;
						fputcsv($out, $row);
					}
				}
			}
		}
	}
}

fclose($out);

$csv = ob_get_clean();


header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private",false);
header("Content-Type: application/octet-stream");
header("Content-Disposition: attachment; filename=\"SOTeria_Export_" . time() . ".csv\";" );
header("Content-Transfer-Encoding: binary");


echo $csv;

//for debugging
// echo "<pre>";
// echo $csv."<br>";
// print_r($questions);
// print_r($observations);
// echo "</pre>";

?>