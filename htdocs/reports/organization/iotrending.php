<?php
require_once("../../src/php/require.php");
mcl_Html::js(mcl_Html::HIGHSTOCK);

$oci = new mcl_Oci("soteria");

$form = "swo";
$lc = $_REQUEST["lc_pilot"];


if(isset($_GET["org"])){ $org = $_GET["org"]; }
if(isset($_GET["form"])){ $form = $_GET["form"]; }

if($form == "swo") {
	$categoriesForm = "'Leader', 'Individual', 'Organization'";
} else if($form == "pp") {
	$categoriesForm = "'Paired Performance'";
} else if($form == "qew") {
	$categoriesForm = "'Qualified Electrical Worker'";
}

if(!empty($_GET["start"])){ 
	$start = $_GET["start"]; 
} else {
	$start = date('m/d/Y', strtotime('-365 days'));
}
if(!empty($_GET["end"])){ 
	$end = $_GET["end"]; 
} else {
	$end = date('m/d/Y');
}


$form_a = array("pp" => "Paired Performance", "qew" => "Qualified Electrical Worker", "swo" => "Safe Worker");
foreach($form_a as $key=>$value){
	$selected = ($form == $key ? 'selected=selected' : '');
	$form_s .= "<option value='{$key}' {$selected}>{$value}</option>";
}

$sql = "
	WITH T AS (		
		SELECT 
			TO_CHAR(O.OBSERVED_DATE, 'MM/YYYY') AS MM, 
			Q.ITEM_TEXT, 
			Q.ITEM_NUM, 
			Q.HAS_SUBITEMS,
			COUNT(*) AS CT 
		FROM EPOP_OBSERVATIONS O, 
				EPOP_ANSWERS A,
				EPOP_ITEMS Q 
		WHERE  A.ANSWER = 'IO'
		AND A.ITEM_NUM = Q.ITEM_NUM
		AND A.EPOP_ID = O.EPOP_ID
		AND OBSERVED_DATE BETWEEN TO_DATE('{$start} 00:00:00', 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE('{$end} 23:59:59', 'MM/DD/YYYY HH24:MI:SS')
		" . (!empty($form) ? "
			AND Q.ITEM_CATEGORY IN ({$categoriesForm})
		" : "") . "
		" . (!empty($org) ? "
			AND ORG_CODE = '" . $org ."'
		" : "") . "
		" . ($lc ? "AND O.LC_PILOT = 1" : "") . "
		GROUP BY TO_CHAR(O.OBSERVED_DATE, 'MM/YYYY'), ITEM_TEXT, Q.ITEM_NUM, Q.HAS_SUBITEMS
		ORDER BY TO_DATE(MM, 'MM/YYYY'), CT DESC
	)
	SELECT 
			TO_CHAR(TO_DATE(MM, 'MM/YYYY'), 'MON') AS MONTH, 
			TO_CHAR(TO_DATE(MM, 'MM/YYYY'), 'YYYY') AS YEAR, 
			T.ITEM_NUM, 
			T.ITEM_TEXT,
			CT 
	FROM T 
	ORDER BY TO_DATE(MM, 'MM/YYYY'), CT DESC
";

$categories = '';
$data1 = '';
$data2 = '';
$data3 = '';

$q = array();
$months = array();
while($row = $oci->fetch($sql)){
	$row["MONTH"] = trim($row["MONTH"]) . " " . trim($row["YEAR"]);
	
	if($months[$row["MONTH"]]["ct"] >= 3) {
		continue;
	}
	
	if(!isset($months[$row["MONTH"]])) 
		$months[$row["MONTH"]]["ct"] = 0;
	
	
	$months[$row["MONTH"]]["ct"]++;
	
	$months[$row["MONTH"]]["io"][] = array(
		"q" => strip_tags($row["ITEM_TEXT"]),
		"ct" => $row["CT"],
		"qnum" => $row["ITEM_NUM"]
	);
}

foreach($months as $key => $value){
	$categories .= (empty($categories) ? "" : ", ") . "'" . ucfirst(strtolower($key)) . "'";
	$data1 .= (empty($data1) ? "" : ", ") . "['" . $value["io"][0]["q"] . "', "  . ($value["io"][0]["ct"] ? $value["io"][0]["ct"] : 0) . ", " . ($value["io"][0]["qnum"] ? $value["io"][0]["qnum"] : 0). "]";
	$data2 .= (empty($data2) ? "" : ", ") . "['" . $value["io"][1]["q"] . "', "  . ($value["io"][1]["ct"] ? $value["io"][1]["ct"] : 0). ", "  . ($value["io"][1]["qnum"] ? $value["io"][1]["qnum"] : 0) . "]";
	$data3 .= (empty($data3) ? "" : ", ") . "['" . $value["io"][2]["q"] . "', "  . ($value["io"][2]["ct"] ? $value["io"][2]["ct"] : 0) . ", " . ($value["io"][2]["qnum"] ? $value["io"][2]["qnum"] : 0) . "]";

	if($value["io"][0]["qnum"] != NULL){ $q[$value["io"][0]["qnum"]] = true;}
	if($value["io"][1]["qnum"] != NULL){ $q[$value["io"][1]["qnum"]] = true;}
	if($value["io"][2]["qnum"] != NULL){ $q[$value["io"][2]["qnum"]] = true;}
}

mcl_Html::s(mcl_Html::SRC_JS, <<<JS
	var highlight;
	$(function () {
		var chart;
		$(document).ready(function() {
			chart = new Highcharts.Chart({
				chart: {
					renderTo: 'container',
					type: 'column'
				},
				title: {
					text: 'Improvement Opportunities Trending'
				},
				subtitle: {
					text: '{$orgs_a[$org]["title"]}'
				},
				xAxis: [{
					categories: [{$categories}]
				}],
				yAxis: {
					min: 0,
					title: {
						text: ''
					}
				},
				legend: {
					layout: 'vertical',
					x: 120,
					verticalAlign: 'top',
					y: 100,
					floating: true,
					backgroundColor: '#FFFFFF'
				},
				/*tooltip: {
					formatter: function(){
						
					}
				},*/
				legend: {
					enabled: true
				},
				plotOptions: {
					series: {
						shadow:false,
						cursor: 'pointer',
						point: {
							events: {
								click: function() { 
									if(highlight){
										highlight.style.backgroundColor = (highlight.className == 'even' ? '#cecece' : '#f0f0f0');
									}
									
									highlight = document.getElementById(this.config[2]);
									highlight.style.backgroundColor = '#ffffca';
								}	
							}
						}
					}
				},
				series: [{
						name: 'Top 1',
						data: [{$data1}],
						color: '#a80000',
						dataLabels: {
							enabled: true
						}
			
					}, {
						name: 'Top 2',
						data: [{$data2}],
						color: '#ff8448',
						dataLabels: {
							enabled: true
						}
			
					}, {
						name: 'Top 3',
						data: [{$data3}],
						color: '#a4d1ff',
						dataLabels: {
							enabled: true
						}
			
					}]
			});
		});
		
	});	
JS
);

echo "
<div style = 'text-align: left; margin-right: 10px;'>
	<form method = 'GET' action='iotrending.php' style = 'overflow: hidden; padding: 5px;'>
		<table style = 'font-size: 11px;' border=0>
			<tr style='font-size: 10px;'>
				<td>Organization</td>
				<td>Observation Form</td>
				<td>Start</td>
				<td>End</td>
				<td></td>
			</tr>	
			<tr style = 'vertical-align: bottom;'>
				<td>
					<select name='org' style = 'height: 20px; font-size: 10px;'>
						<option value=''>- All Organizations - </option>
						{$org_select}
					</select>
				</td>
				<td>
					<select name='form' style = 'height: 20px; font-size: 10px;'>
						<option value=''>- All Observations - </option>
						{$form_s}
					</select>
				</td>
				<td style ='padding-bottom: 3px;'>
					<input style = 'height: 12px; width: 100px;' type = 'text' name = 'start' id = 'start' value = '{$start}'/> <img style = 'vertical-align: bottom;' src='../../src/img/calendar.gif' alt='' id='tcal' onmouseover='setup_cal(\"tcal\", \"start\");' />
				</td>
				<td style ='padding-bottom: 3px;'>
					<input style = 'height: 12px; width: 100px;' type = 'text' name = 'end' id = 'end' value = '{$end}'/> <img style = 'vertical-align: bottom;' src='../../src/img/calendar.gif' alt='' id='tcal2' onmouseover='setup_cal(\"tcal2\", \"end\");' />
				</td>
				<td>
						&nbsp;<input type='checkbox' name='lc_pilot' value='1' " . ($lc ? "checked=checked" : "") . "/> Life Critical Pilot Only&nbsp;&nbsp;
				</td>
				<td style = 'text-align: right; padding-bottom: 3px;'>
					" . (!empty($_GET["delegate"]) ? "<input type='hidden' name='delegate' value='{$_GET["delegate"]}'/>" : "") . "
					<input style = 'height: 18px;' type = 'submit' value = 'Filter'/>
				</td>
			</tr>
		</table>
	</form>
</div>
<div style = ''>
	<div id = 'container' style = 'width: 1100px; border: 1px solid #000; margin: 5px; height: 300px;'></div>
</div>
";

	$qnums = '';
	foreach($q as $x => $y){
		$qnums .= (empty($qnums) ? "" : ", ") . ($x);
	}
	
	if(!empty($qnums)){
		$sql = "
			SELECT TO_CHAR(O.OBSERVED_DATE, 'MM') AS MM,
				    TO_CHAR(O.OBSERVED_DATE, 'YYYY') AS YYYY,			
					Q.ITEM_TEXT, 
					Q.ITEM_NUM, 
					Q.HAS_SUBITEMS,
					COUNT(*) AS CT 
				FROM EPOP_OBSERVATIONS O, 
						EPOP_ANSWERS A,
						EPOP_ITEMS Q 
				WHERE  A.ANSWER = 'IO'
				AND A.ITEM_NUM = Q.ITEM_NUM
				AND A.EPOP_ID = O.EPOP_ID
				AND Q.ITEM_NUM IN ({$qnums})
				AND OBSERVED_DATE BETWEEN TO_DATE('{$start} 00:00:00', 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE('{$end} 23:59:59', 'MM/DD/YYYY HH24:MI:SS')
				" . (!empty($form) ? "
					AND Q.ITEM_CATEGORY IN ({$categoriesForm})
				" : "") . "
				" . (!empty($org) ? "
					AND ORG_CODE = '" . $org ."'
				" : "") . "
				GROUP BY TO_CHAR(O.OBSERVED_DATE, 'MM'), 
							TO_CHAR(O.OBSERVED_DATE, 'YYYY'),
							ITEM_TEXT, 
							Q.ITEM_NUM,
							Q.HAS_SUBITEMS
				ORDER BY TO_CHAR(O.OBSERVED_DATE, 'YYYY'), TO_CHAR(O.OBSERVED_DATE, 'MM'), CT DESC
		";
		
		$questions = array();
		$ios = array();
		$months = array();
		
		while($row = $oci->fetch($sql)){
			$ios[$row["MM"]][$row["ITEM_NUM"]] = $row["CT"];
			$questions[$row["ITEM_NUM"]]["TEXT"] = $row["ITEM_TEXT"];
			$questions[$row["ITEM_NUM"]]["HAS_SUBITEMS"] = $row["HAS_SUBITEMS"];
			$months[$row["MM"]] = $row["MM"];
		}
		

		$tbl = '<table class = "tbl" style = "width: 1000px; font-size: 12px;">
				<tr>
					<th><div class="inner_title">Improvement Opportunities Trending by Month</div></th>
					<th colspan = "' . (count($months)) .'">
						<div class="inner_title">
							Count
						</div>
					</th>
				</tr>
				<tr>
					<th>
						<div class="inner" >
							Improvement Opportunity
						</div>
					</th>
		';
		foreach($months as $key => $value){
			$tbl .= "<th>
						<div class='inner'>
							" . date('M', mktime(0, 0, 0, $key, 1, date('Y'))) . "
						</div>
					</th>	
				";
		}
		
		echo "</tr>
		";
		
		$x = 0;
		
		
		$stmt_sub = $oci->parse("
			SELECT TO_CHAR(O.OBSERVED_DATE, 'MM') AS MM,
					TO_CHAR(O.OBSERVED_DATE, 'YYYY') AS YYYY,			
					Q.SUBITEM_TEXT, 
					Q.SUBITEM_NUM,
					COUNT(*) AS CT 
			FROM EPOP_OBSERVATIONS O, 
				EPOP_ANSWERS A,
				SWO_SUBITEMS Q 
			WHERE  A.ANSWER = 'IO'
			AND A.ITEM_NUM = Q.SUBITEM_NUM
			AND A.EPOP_ID = O.EPOP_ID
			AND Q.PARENT_ITEM_NUM = :parent_item_num
			AND OBSERVED_DATE BETWEEN TO_DATE('{$start} 00:00:00', 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE('{$end} 23:59:59', 'MM/DD/YYYY HH24:MI:SS')
			" . (!empty($org) ? "
				AND ORG_CODE = '" . $org ."'
			" : "") . "
			GROUP BY TO_CHAR(O.OBSERVED_DATE, 'MM'),
					TO_CHAR(O.OBSERVED_DATE, 'YYYY'),			
						SUBITEM_TEXT, 
						Q.SUBITEM_NUM
		");
		
		foreach($questions as $key => $value){
			$tbl .= "<tr class = '" . ($x++ % 2 == 0 ? 'even' : 'odd'). "' id='{$key}'>
					<td style = 'text-align: left;'>{$value["TEXT"]}</td>
			";
			
			foreach($months as $mm => $val2){
				$tbl .= "<td style = 'width: 40px;'>" . (!empty($ios[$mm][$key]) ?  $ios[$mm][$key] : 0). "</td>";
			}
			
			$tbl .= "</tr>";
			
			if($value["HAS_SUBITEMS"]){
				$oci->bind($stmt_sub, array(
					":parent_item_num"	=> $key
				));
				
				$sub_ios = array();
				$sub_questions = array();
				while($row2 = $oci->fetch($stmt_sub, false)){
					$sub_ios[$row2["MM"]][$row2["SUBITEM_NUM"]] = $row2["CT"];
					$sub_questions[$row2["SUBITEM_NUM"]]= $row2["SUBITEM_TEXT"];
				}
				
				if(!empty($sub_ios)){
					foreach($sub_questions as $key2 => $value2){
						$tbl .= "<tr class = '" . ($x++ % 2 == 0 ? 'even' : 'odd'). "' id='{$key2}'>
							<td style = 'text-align: left; padding-left: 20px;'>{$value2}</td>
						";
						
						foreach($months as $mm => $val2){
							$tbl .= "<td style = 'width: 40px;'>" . (!empty($sub_ios[$mm][$key2]) ?  $sub_ios[$mm][$key2] : 0). "</td>";
						}
					}
				}
				
			$tbl .= "</tr>";
			
			}
		}
		
		if($x == 0){
			$tbl .= "<td></td>";
		}
		
		$tbl .= "</table>";
		
		echo "{$tbl}";
	}
?>
