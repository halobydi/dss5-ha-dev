<?php
	require_once("../../src/php/require.php");

	$oci = new mcl_Oci('soteria');
	$sql = "
		SELECT 	DISTINCT TO_CHAR(AUDIT_DATE, 'YYYY') AS YEAR 
		FROM 	RED_TAG_AUDITS 
		WHERE 	AUDIT_DATE IS NOT NULL 
				AND COMPLETE = 1
		UNION  
		SELECT TO_CHAR(SYSDATE, 'YYYY') 
		FROM 	DUAL";

	$year = (!empty($_GET["year"]) ? $_GET["year"] : date('Y'));
	$select = "<select name='year' style = 'height: 20px; font-size: 10px;'>";
	$years = 0;
	while($row = $oci->fetch($sql)){
		$years++;
		$selected = $year == $row["YEAR"] ? "selected=selected" : "";
		$select .= "<option {$selected} value='{$row["YEAR"]}'>{$row["YEAR"]}</option>";
	}

	if($years == 0){
		$select .= "<option value='{$year}'>{$year}</option>";
	}
	$select .= "</select>";

	$header = "	<table class = 'tbl'>
				<tr>
					<th colspan='17'>
						<div class='inner_title'>
							{$year} Number of Completed Audits
						</div>
					</th>
				</tr>
				<tr>
					<th>
						<div class='inner' style='width: 200px;'>
							Location
						</div>
					</th>
					<th>
						<div class='inner' style='width: 200px;'>
							Org. Audited
						</div>
					</th>
					<th>
						<div class='inner' style='width: 100px;'>
							Annual Req.
						</div>
					</th>
					<th>
						<div class='inner' style='width: 80px;'>
							Actual Comp.
						</div>
					</th>
					<th>
						<div class='inner' style='width: 50px;'>
							% Comp
						</div>
					</th>
					<th>
						<div class='inner' style='width: 35px;'>
							Jan.
						</div>
					</th>
					<th>
						<div class='inner' style='width: 35px;'>
							Feb.
						</div>
					</th>
					<th>
						<div class='inner' style='width: 35px;'>
							Mar.
						</div>
					</th>
					<th>
						<div class='inner' style='width: 35px;'>
							Apr.
						</div>
					</th>
					<th>
						<div class='inner' style='width: 35px;'>
							May
						</div>
					</th>
					<th>
						<div class='inner' style='width: 35px;'>
							June
						</div>
					</th>
					<th>
						<div class='inner' style='width: 35px;'>
							July
						</div>
					</th>
					<th>
						<div class='inner' style='width: 35px;'>
							Aug.
						</div>
					</th>
					<th>
						<div class='inner' style='width: 35px;'>
							Sept.
						</div>
					</th>
					<th>
						<div class='inner' style='width: 35px;'>
							Oct.
						</div>
					</th>
					<th>
						<div class='inner' style='width: 35px;'>
							Nov.
						</div>
					</th>
					<th>
						<div class='inner' style='width: 35px;'>
							Dec.
						</div>
					</th>
				</tr>
			";
	
	$location = array();
	$locations2 = array();
	$monthly_requirements = array();
	
	function bgColor() {
		$args = func_get_args();
		if(count($args) == 3 ) {
			list($actual, $target, $month) = $args;
			
			$percent = ( $actual / $target ) * 100;
			global $year;
			
			if(($year >= date("Y") && $month > intval(date("m"))) || $year > date("Y")) {
				return '';
			}
			
		
		} else {
			$percent = $args[0];
		}
	
		if($percent  < 50) {
			$color = '#ffaaad';
		} else if($percent < 95) {
			$color = '#ffffc6';
		} else {
			$color = '#b5d3b5';
		}
		
		return $color;
	}
	
	$sql = "
		SELECT	LOCATION,
				ORG,
				TITLE,
				NVL(TOTAL, 0) AS TOTAL,
				MONTH,
				YEAR
				
		FROM RED_TAG_AUDITS_LOCATIONS
		LEFT	JOIN RED_TAG_AUDITS_REQUIREMENTS
					ON ID = LOCATION_ID
					AND YEAR = {$year}
				ORDER BY TITLE, LOCATION, ORG
	";
	
	
	$charts = array();
	while($row = $oci->fetch($sql)){
		$monthly_requirements[$row["ORG"]][$row["LOCATION"]][$row["MONTH"]] = $row["TOTAL"];
		$location[$row["TITLE"]][$row["LOCATION"]][$row["ORG"]] += $row["TOTAL"];
		$location2[$row["ORG"]] = $row["TITLE"];
		
		$charts[$row["TITLE"]]["TARGET"][$row["MONTH"]] += empty($row["TOTAL"]) ? 0 : $row['TOTAL'];
	}

	$sql = "
		SELECT 	CASE WHEN ORG IN ('Service Operations', 'System Operations', 'Engineering') THEN LOCATION ELSE ORG END AS ORG, 
				LOCATION, 
				CREDIT_MONTH AS MONTH, 
				COUNT(*) AS CT 
		FROM RED_TAG_AUDITS
		WHERE TO_CHAR(AUDIT_DATE, 'YYYY') = '{$year}'
		AND COMPLETE = 1
		GROUP BY CASE WHEN ORG IN ('Service Operations', 'System Operations', 'Engineering') THEN LOCATION ELSE ORG END, 
				LOCATION, 
				CREDIT_MONTH
	";

	while($row = $oci->fetch($sql)){
		$data[$row["ORG"]][$row["LOCATION"]][$row["MONTH"]] = $row["CT"];
		$data[$row["ORG"]][$row["LOCATION"]]["TOTAL"] += $row["CT"];
		
		
		$charts[$location2[$row["ORG"]]]["ACTUAL"][$row["MONTH"]] += empty($row["CT"]) ? 0 : $row['CT'];
	}
	
	$months = array(
		1 => "January",
		2 => "February",
		3 => "March",
		4 => "April",
		5 => "May",
		6 => "June",
		7 => "July",
		8 => "August",
		9 => "September",
		10 => "October",
		11 => "November",
		12 => "December"
	);
	
	$total = array();
	$req = array();
	foreach($location as $key => $value){
		foreach($value as $key2 => $section){
			foreach($section as $org=>$ct){
				if($key != $prev_section){
					$summary .= "<tr>
							<td style='border-top: 1px solid black; border-bottom: 1px solid black; font-weight: bold;'>{$location2[$org]}</td>
							<td style='border-top: 1px solid black; border-bottom: 1px solid black;' colspan='16'></td>
						</tr>";
				}
				
				$summary .= "<tr class = '" . ($x++ % 2 == 0 ? 'even' : 'odd') ."'>
					<td style = 'text-align: left;' {$border}>" . ($prev_sc != $key2 ? $key2 : "") . "</td>
					<td {$border}>" . ($org != $key2 ? $org : "") . "</td>
					<td {$border}>{$ct}</td>
				";
				
	
				$summary .= "<td {$border}>" . $data[$org][$key2]["TOTAL"] . "</td>";
				$percent = round(($data[$org][$key2]["TOTAL"] / $ct) * 100, 2);
				$summary .= "<td {$border} style='background-color: " . bgColor($percent) . "'>" . (isset($data[$org][$key2]["TOTAL"]) ? $percent . "%" : "") . "</td>";
	
			
				for($i = 1; $i <= 12; $i++) {
					$summary .= "<td title='{$monthly_requirements[$org][$key2][$i]} audits are required for {$location2[$org]} ({$key2}) in {$months[$i]}' {$border} style='background-color: " . bgColor($data[$org][$key2][$i], $monthly_requirements[$org][$key2][$i], $i) . "'>" . $data[$org][$key2][$i] . "</td>";
					$total[$i] += $data[$org][$key2][$i];
					$req[$i] += $monthly_requirements[$org][$key2][$i];
				}
			
				$total["req"] += $ct;
				$total["comp"] += $data[$org][$key2]["TOTAL"];
				
				$summary .= "</tr>";
				$prev_sc = $key2;
				$prev_section = $key;
			}
		}
	}
	
	$border = "style='border-top: 1px solid #000;'";
	$summary .= "<tr>
		<td style = 'text-align: center; font-weight: bold;' {$border}>Total</td>
		<td {$border}>--</td>
		<td {$border}>{$total["req"]}</td>
	";				

	$summary .= "<td {$border}>" . $total["comp"] . "</td>";
	$percent = round(($total["comp"] / $total["req"]) * 100, 2);
	$summary .= "<td {$border} style='background-color: " . bgColor($percent) . "'>" . (isset($total["comp"]) ? $percent . "%" : "") . "</td>";

	for($i = 1; $i <= 12; $i++) {
		$summary .= "<td {$border} style='background-color: " . bgColor($total[$i], $req[$i] , $i) . "' title='Required: {$req[$i]} Actual: {$total[$i]}'>" . $total[$i] . "</td>";	
	}

	echo "
		<div style = 'margin-left: 5px;'>
		<form method = 'GET' style = 'overflow: hidden; padding: 0px;'>
			<table style = 'font-size: 12px;'>
				<tr><td colspan = '3'><span style='font-size: 10px;'>Auto populated from available audits</span></td></tr>
				<tr style = 'vertical-align: bottom;'>
					<td style='padding-bottom: 3px;'>Audit Date Year</td>
					<td>
						{$select}
					</td>
					<td style = 'text-align: right; padding-bottom: 3px;'>
						" . (!empty($_GET["delegate"]) ? "<input type='hidden' name='delegate' value='{$_GET["delegate"]}'/>" : "") . "
						<input style = 'height: 18px;' type = 'submit' value = 'Filter'/>
					</td>
				</tr>
			</table>
		</form>
	</div>
	<div style = 'float: left;'>
		<table class = 'tbl'>
		{$header}
		{$summary}
	</table>
	</div>
	";
	echo "<br/>";
?>