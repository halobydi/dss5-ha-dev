<?php

require_once("../../src/php/require.php");
mcl_Html::js(mcl_Html::HIGHSTOCK);

$oci = new mcl_Oci("soteria");

if(!empty($_GET["start"])){ 
	$start = $_GET["start"]; 
} else {
	$start = date('m/d/Y', strtotime('-365 days'));
}
if(!empty($_GET["end"])){ 
	$end = $_GET["end"]; 
} else {
	$end = date('m/d/Y');
}

$form_a = array("hp" => "Anatomy of an Event", "nm" => "Near Miss");
$hp_event = $_GET['hp_event'];
$sql = '';
$form = $_GET["form"]; 
 
if($form) {
	$sql = "
		WITH F AS (
				SELECT 
					".(($form == 'hp') ? 'HP' : 'NEAR_MISS')."_OBSERVATIONS.{$form}_ID as OBSERVATION_ID, 
					UPPER('{$form}') as FORM
				FROM 
					".(($form == 'hp') ? 'HP' : 'NEAR_MISS')."_OBSERVATIONS
						" . ($form == 'hp' && $hp_event != '' ? "
						JOIN HP_ANSWERS A
							ON A.HP_ID = HP_OBSERVATIONS.HP_ID AND A.ITEM_NUM = 500 AND A.ANSWER = '{$hp_event}'
					" : "") . "
				WHERE 
					INCIDENT_DATE BETWEEN TO_DATE('{$start} 00:00:00', 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE('{$end} 23:59:59', 'MM/DD/YYYY HH24:MI:SS') 
					".($org ? "AND ORG_CODE = '{$org}'" : "")."
			)

			SELECT 
				ITEM_CATEGORY, 
				T.ITEM_NUM, ITEM_TEXT, 
				COUNT(ANSWER) AS CT
			FROM 
				TWIN_ANSWERS T, F, 
				HP_ITEMS I
			WHERE 
				T.OBSERVATION_ID=F.OBSERVATION_ID
				AND ANSWER = 'YES'
				AND T.OBSERVATION_FORM = F.FORM
				AND I.ITEM_NUM = T.ITEM_NUM
			GROUP BY 
				ITEM_CATEGORY, 
				T.ITEM_NUM, 
				ITEM_TEXT
			ORDER BY 
				CT DESC
	";
} else {
	$sql = "WITH F AS (
				SELECT 
					HP_OBSERVATIONS.HP_ID as OBSERVATION_ID,
					'HP' as FORM
				FROM 
					HP_OBSERVATIONS 
				" . ($hp_event != '' ? "
					 JOIN HP_ANSWERS A
						ON A.HP_ID = HP_OBSERVATIONS.HP_ID AND A.ITEM_NUM = 500 AND A.ANSWER = '{$hp_event}'
				" : "") . "
				WHERE 
					INCIDENT_DATE BETWEEN TO_DATE('{$start} 00:00:00', 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE('{$end} 23:59:59', 'MM/DD/YYYY HH24:MI:SS') 
					".($org ? "AND ORG_CODE = '{$org}'" : "")."
						

				UNION ALL

				SELECT NM_ID, 'NM'
				FROM NEAR_MISS_OBSERVATIONS
				WHERE INCIDENT_DATE BETWEEN TO_DATE('{$start} 00:00:00', 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE('{$end} 23:59:59', 'MM/DD/YYYY HH24:MI:SS') 
					".($org ? "AND ORG_CODE = '{$org}'" : "")."
			)

			SELECT 
				ITEM_CATEGORY, 
				T.ITEM_NUM, ITEM_TEXT, 
				COUNT(ANSWER) AS CT
			FROM 
				TWIN_ANSWERS T, F, 
				HP_ITEMS I
			WHERE 
				T.OBSERVATION_ID=F.OBSERVATION_ID
				AND ANSWER = 'YES'
				AND T.OBSERVATION_FORM = F.FORM
				AND I.ITEM_NUM = T.ITEM_NUM
			GROUP BY 
				ITEM_CATEGORY, 
				T.ITEM_NUM, 
				ITEM_TEXT
			ORDER BY 
				CT DESC
			
			";
}

//echo "<pre>"; echo $sql; echo "</pre>";

foreach($form_a as $key=>$value){
	$selected = ($form == $key ? 'selected=selected' : '');
	$form_s .= "<option value='{$key}' {$selected}>{$value}</option>";
}

$categories = '';
$data = '';
$arrData = array();
$x = 0;
while (($row = $oci->fetch($sql)) && ($x < 10)) {
	$categories .= (empty($categories) ? "" : ", ") . "'{$row['ITEM_CATEGORY']}: {$row['ITEM_TEXT']}'";
	$data .= (empty($data) ? "" : ", ") . "{$row['CT']}";
	$arrData[$x] = $row;
	$x++;
}

mcl_Html::s(mcl_Html::SRC_JS, <<<JS
	var highlight;
	$(function () {
		var chart;
		$(document).ready(function() {
			chart = new Highcharts.Chart({
				chart: {
					renderTo: 'container',
					type: 'column'
				},
				title: {
					text: 'Top Ten Precursors'
				},
				subtitle: {
					text: ''
				},
				xAxis: [{
					categories: [{$categories}],
					labels: {
						enabled: true
					}
				}],
				yAxis: {
					min: 0,
					title: {
						text: ''
					}
				},
				legend: {
					layout: 'vertical',
					x: 120,
					verticalAlign: 'top',
					y: 100,
					floating: true,
					backgroundColor: '#FFFFFF'
				},
				legend: {
					enabled: false
				},
				plotOptions: {
					series: {
						shadow: false
					}
				},
				series: [{
					name: 'Count',
					data: [{$data}],
					color: '#a80000',
					dataLabels: {
						enabled: true
					}
		
				}]
			});
		});
	});	
	
	$(document).ready(function() {
		$("select#form").change(function() {
			var form = $(this).val();
			if(form == 'nm') {
				$("#hp_event_title").hide();
				$("#hp_event_container").hide();
			} else {
				$("#hp_event_title").show();
				$("#hp_event_container").show();
			}
		});
	});
JS
);

$hp_events = array(
	'Safety (Reset = OSHA recordable)',
	'Environmental (Reset = requiring report in less than 30 days)',
	'Protection Process (Reset = protection violation)',
	'Regulatory (Reset = requiring report in less than 30 days)',
	'Company Vehicle Accident (Reset = resulting in damage in excess of $5000)',
	'Property or Equipment Damage (Reset = damage in excess of $15000)'
);
$hp_events_options = '';
foreach($hp_events as $event) {
	$hp_events_options .= "<option value='{$event}' " . ($hp_event == $event ? "selected=selected" : "") . ">{$event}</option>";
}

echo "
	<div>
		<form method = 'GET' action='twin.php' style = 'overflow: hidden; padding: 5px;'>
			<table style = 'font-size: 11px;' border=0>
				<tr>
					<td>Organization</td>
					<td>Observation Form</td>
					<td style='" . ($form == 'nm' ? 'display: none;' : '') . "' id='hp_event_title'>HP Event/Reset Category</td>
					<td>Start</td>
					<td>End</td>
				</tr>	
				<tr style = 'vertical-align: bottom;'>
					<td>
						<select name='org' style = 'height: 20px; font-size: 10px;'>
							<option value=''>- All Organizations - </option>
							{$org_select}
						</select>
					</td>
					<td>
						<select id='form' name='form' style = 'height: 20px; font-size: 10px;'>
							<option value=''>- All Observations - </option>
							{$form_s}
						</select>
					</td>
					<td id='hp_event_container' style='" . ($form == 'nm' ? 'display: none;' : '') . "'>
						<select name='hp_event' style = 'height: 20px; font-size: 10px;'>
							<option value=''></option>
							" . $hp_events_options . "
						</select>
					</td>
					<td style ='padding-bottom: 3px;'>
						<input style = 'height: 12px; width: 100px;' type = 'text' name = 'start' id = 'start' value = '{$start}'/> <img style = 'vertical-align: bottom;' src='../../src/img/calendar.gif' alt='' id='tcal' onmouseover='setup_cal(\"tcal\", \"start\");' />
					</td>
					<td style ='padding-bottom: 3px;'>
						<input style = 'height: 12px; width: 100px;' type = 'text' name = 'end' id = 'end' value = '{$end}'/> <img style = 'vertical-align: bottom;' src='../../src/img/calendar.gif' alt='' id='tcal2' onmouseover='setup_cal(\"tcal2\", \"end\");' />
					</td>
					<td style = 'text-align: right; padding-bottom: 3px;'>
						" . (!empty($_GET["delegate"]) ? "<input type='hidden' name='delegate' value='{$_GET["delegate"]}'/>" : "") . "
						<input style = 'height: 18px;' type = 'submit' value = 'Filter'/>
					</td>
				</tr>
			</table>
		</form>
	</div>
	<div style = ''>
		<div id = 'container' style = 'width: 998px; border: 1px solid #000; margin: 5px; height: 300px;'></div>
	</div>
	";
	$tbl = '<table class = "tbl" style = "width: 1000px; font-size: 12px;">
			<tr>
				<th>
					<div class="inner" >
						Precursor (Note: items in <em>italics</em> are more prevalent.)
					</div>
				</th>
				<th colspan>
					<div class="inner">
						Count
					</div>
				</th>
			</tr>
	';
	
	for($x = 0; $x < count($arrData); $x++) {
		$tbl .= "<tr class = '" . ($x % 2 == 0 ? 'even' : 'odd'). "' id='{$x}'>
					<td style = 'text-align: left;'>{$arrData[$x]["ITEM_CATEGORY"]}: {$arrData[$x]["ITEM_TEXT"]}</td>
					<td style = 'text-align: center;'>{$arrData[$x]["CT"]}</td>
				</tr>
		";
	}
	
	$tbl .= "</table>";
	
	echo "{$tbl}";
?>
