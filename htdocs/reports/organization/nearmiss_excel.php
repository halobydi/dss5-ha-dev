<?php
/*
Change Log
DS-148 12-21-2016  priority replaced with Serious? GalaxE Detroit Edward King
DS-249  1-19-2017  Added sub-category in Excel export GalaxE Detroit Seneca Hinds

*/
	function errorHandler($errno, $errstr, $errfile, $errline, $errcontext){
	}
	set_error_handler('errorHandler');
	require_once("mcl_Oci.php");

	$oci = new mcl_Oci("soteria");
	$oci->dateFormat();
	$oci->query($sql);
	
	$auth = "";
	/*if($_GET["auth"] == "true") {
		$user = auth::check();
		$usid = (!empty($_GET["delegate"]) ? $user["delegates"][$_GET["delegate"]]["usid"] : $user["usid"]);
		$auth = "(COMPLETED_BY IN (SELECT USID FROM EMPLOYEES WHERE PATH LIKE '%{$usid}%' UNION ALL SELECT '{$usid}' FROM DUAL)
					OR COMPLETED_BY_SUPERVISOR = '{$usid}' OR DIRECTOR = '{$usid}')";
	}*/
	
	if($_GET["org"] != "") {
		$where .= " AND N.ORG_CODE = '{$_GET["org"]}'";
	}
	if($_GET["work_dte_loc"] != "" && isset($_GET["work_dte_loc"])) {
		$where .= " AND N.COMPLETED_BY_PLANT = '{$_GET["work_dte_loc"]}'";
	}
	if($_GET["dte_loc"] != "" && isset($_GET["dte_loc"])) {
		$where .= " AND N.PLANT = '{$_GET["dte_loc"]}'";
	}
	if($_GET["loc"] != "" && isset($_GET["loc"])) {
		$where .= " AND N.LOCATION = '{$_GET["loc"]}'";
	}
	if($_GET["cat"] != "" && isset($_GET["cat"])) {
		$where .= " AND CATEGORY = '{$_GET["cat"]}'";
	}
	if($_GET["inv"] != "" && isset($_GET["inv"])) {
		$where .= " AND (CASE WHEN INVESTIGATION_COMPLETE = 1 OR TRIM(INVESTIGATION) IS NOT NULL THEN 1 ELSE 0 END = {$_GET["inv"]} OR REQUIRES_INVESTIGATION = 0)";
	}
	if($_GET["req_inv"] != "" && isset($_GET["req_inv"])) {
		$where .= " AND REQUIRES_INVESTIGATION = {$_GET["req_inv"]}";
	}
//DS-198
	/*if($_GET['priority'] != "") {
		$priority = strtoupper($_GET["priority"]);
		if(strlen($priority) > 1) $priority = 1;
		$where .= " AND PRIORITY = '{$priority}'";
	}*/
    if($_GET["isserious"] !=""){
        if($_GET["isserious"]==2){
            $where .= " AND ISSERIOUS IS NULL";
        }
        else
        {
            $where .= " AND ISSERIOUS = {$_GET["isserious"]}";
        }
    }

	$sql = <<<SQL
	SELECT	NM_ID AS "Near Miss ID", 
			CASE WHEN ISSERIOUS  IS NULL THEN 'NA' WHEN ISSERIOUS = 0 THEN 'No' WHEN ISSERIOUS = 1 THEN 'Yes' ELSE 'NA' END AS "Serious?",
			CASE WHEN PRIORITY = 'H' THEN 'High' WHEN PRIORITY = 'M' THEN 'Medium' WHEN PRIORITY = 'L' THEN 'Low' ELSE 'NA' END AS "Priority",
			NVL(E.NAME, COMPLETED_BY) AS "Completed By",
			COMPLETED_BY_PHONE AS "Completed By Phone",
			COMPLETED_BY_TITLE AS "Completed By Title",
			COMPLETED_DATE AS "Completed Date",
			TO_CHAR(INCIDENT_DATE, 'MM/DD/YYYY') AS "Incident Date",
			INCIDENT_TIME AS "Incident Time",
			C.CAT_TEXT AS "Category",
			C_SUB.CAT_TEXT AS "Sub Category",
			NVL(O.ORG_TITLE, 'Unknown') AS "Organization",
			P2.PLANT AS "Employee DTE Work Location",
			P.PLANT AS "Near Miss DTE Location",
			AL.AL_LOCATION AS "Location",
			LOCATION_DESCRIPTION AS "Location Description",
			HAPPENED_BEFORE AS "Happpened Before",
			HAPPENED_DURING AS "Happened During",
			HAPPENED_AFTER AS "Happened After", 	
			INVESTIGATION "Investigation",
			NVL(S.NAME, COMPLETED_BY_SUPERVISOR) AS "Completed by Supervisor",
			COMPLETED_BY_SUPERVISOR_PHONE AS "Completed by Supervisor Phone",
			SUPERVISOR_COMMENTS_ENTERED AS "Supervisor Comments Entered",
			SUPERVISOR_COMMENTS  "Supervisor Comments",
			NVL(D.NAME, DIRECTOR) AS "Director",
			DIRECTOR_COMMENTS AS "Director Comments",
			DIRECTOR_COMMENTS_ENTERED AS "Director Comments Entered",	
			CASE WHEN ASSIST_INVESTIGATION = 1 THEN 'Yes' ELSE 'No' END AS "Assist with Investigation",
			CASE WHEN ASSIST_SOLUTION = 1 THEN 'Yes' ELSE 'No' END AS "Assist with Solution",
			CASE WHEN REQUIRES_INVESTIGATION = 1 THEN 'Yes' ELSE 'No' END AS "Requires Investigation",
			CASE WHEN INVESTIGATION_COMPLETE = 1 OR TRIM(INVESTIGATION) IS NOT NULL THEN 'Yes' ELSE 'No' END AS "Investigation Complete"		
	FROM	NEAR_MISS_OBSERVATIONS N
	LEFT JOIN EMPLOYEES E ON E.USID = N.COMPLETED_BY
	LEFT JOIN EMPLOYEES S ON S.USID = N.COMPLETED_BY_SUPERVISOR
	LEFT JOIN EMPLOYEES D ON D.USID = N.DIRECTOR
	LEFT JOIN NEAR_MISS_CATEGORIES C ON C.CAT_ID = N.CATEGORY
	LEFT JOIN NEAR_MISS_CATEGORIES C_SUB ON C_SUB.CAT_ID = N.SUB_CATEGORY
	LEFT JOIN NEAR_MISS_ACCIDENT_LOCATIONS AL ON AL.AL_ID = N.LOCATION
	LEFT JOIN NEAR_MISS_PLANTS P ON P.PLANT_ID = N.PLANT
	LEFT JOIN NEAR_MISS_PLANTS P2 ON P2.PLANT_ID = N.COMPLETED_BY_PLANT
	LEFT JOIN (SELECT DISTINCT ORG_CODE, ORG_TITLE FROM ORGANIZATIONS GROUP BY ORG_CODE, ORG_TITLE) O ON O.ORG_CODE = N.ORG_CODE
	WHERE	INCIDENT_DATE BETWEEN TO_DATE('{$_GET["start"]} 00:00:00', 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE('{$_GET["end"]} 23:59:59', 'MM/DD/YYYY HH24:MI:SS')
		--{//$auth}
		{$where}
SQL
;
	
	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Cache-Control: private",false);
	header("Content-Type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"SOTeria_Export_" . time() . ".csv\";" );
	header("Content-Transfer-Encoding: binary");
	
	$ct = 0;
	$header = false;
	
	while($row = $oci->fetch($sql)) {
		if (!$header) {
			$x = 0;				
			foreach($row as $key=>$value) {
				echo ($x++ == 0 ? "" : ",") . $key;
			}
			echo "\n";
			$header = true;
		}		
				
		$x = 0;
		foreach($row as $key => $value) {
			echo ($x++ == 0 ? "" : ",") . "\"" . $value . "\"";
		}
		echo "\n";
		
		$ct++;
	}
?>