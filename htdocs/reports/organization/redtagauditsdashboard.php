<?php
	require_once("../../src/php/require.php");

	$oci = new mcl_Oci('soteria');
	$sql = "
		SELECT 	DISTINCT TO_CHAR(AUDIT_DATE, 'YYYY') AS YEAR 
		FROM 	RED_TAG_AUDITS 
		WHERE 	AUDIT_DATE IS NOT NULL 
				AND COMPLETE = 1
		UNION  
		SELECT TO_CHAR(SYSDATE, 'YYYY') 
		FROM 	DUAL";

	$year = (!empty($_GET["year"]) ? $_GET["year"] : date('Y'));
	$select = "<select name='year' style = 'height: 20px; font-size: 10px;'>";
	$years = 0;
	while($row = $oci->fetch($sql)){
		$years++;
		$selected = $year == $row["YEAR"] ? "selected=selected" : "";
		$select .= "<option {$selected} value='{$row["YEAR"]}'>{$row["YEAR"]}</option>";
	}

	if($years == 0){
		$select .= "<option value='{$year}'>{$year}</option>";
	}
	$select .= "</select>";
	
	$location = array();
	$locations2 = array();
	$monthly_requirements = array();

	$sql = "
		SELECT	LOCATION,
				ORG,
				TITLE,
				NVL(TOTAL, 0) AS TOTAL,
				MONTH,
				YEAR
				
		FROM RED_TAG_AUDITS_LOCATIONS
		LEFT	JOIN RED_TAG_AUDITS_REQUIREMENTS
				ON ID = LOCATION_ID
				AND YEAR = {$year}
				ORDER BY TITLE, LOCATION, ORG
	";
	
	
	$charts = array();
	while($row = $oci->fetch($sql)){
		$monthly_requirements[$row["ORG"]][$row["LOCATION"]][$row["MONTH"]] = $row["TOTAL"];
		$location[$row["TITLE"]][$row["LOCATION"]][$row["ORG"]] += $row["TOTAL"];
		$location2[$row["ORG"]] = $row["TITLE"];
		
		$charts[$row["TITLE"]]["TARGET"][$row["MONTH"]] += empty($row["TOTAL"]) ? 0 : $row['TOTAL'];
	}

	$sql = "
		SELECT 	CASE WHEN ORG IN ('Service Operations', 'System Operations', 'Engineering') THEN LOCATION ELSE ORG END AS ORG, 
				LOCATION, 
				CREDIT_MONTH AS MONTH, 
				COUNT(*) AS CT 
		FROM RED_TAG_AUDITS
		WHERE TO_CHAR(AUDIT_DATE, 'YYYY') = '{$year}'
		AND COMPLETE = 1
		GROUP BY CASE WHEN ORG IN ('Service Operations', 'System Operations', 'Engineering') THEN LOCATION ELSE ORG END, 
				LOCATION, 
				CREDIT_MONTH
	";

	while($row = $oci->fetch($sql)){
		$data[$row["ORG"]][$row["LOCATION"]][$row["MONTH"]] = $row["CT"];
		$data[$row["ORG"]][$row["LOCATION"]]["TOTAL"] += $row["CT"];
		
		
		$charts[$location2[$row["ORG"]]]["ACTUAL"][$row["MONTH"]] += empty($row["CT"]) ? 0 : $row['CT'];
	}


	mcl_Html::js(mcl_Html::HIGHSTOCK);
	mcl_Html::s(mcl_Html::SRC_JS, <<<JS
		var build_chart = function(complete, expected, container) {
			if(!complete) {
				complete = {};
			}
			
			if(!expected) {
				expected = {};
			}
			
			var months = [
				"Jan.",
				"Feb.",
				"March",
				"April",
				"May",
				"June",
				"July",
				"Aug.",
				"Sept.",
				"Oct.",
				"Nov.",
				"Dec."
			];
			
			var actual = [];
			var pareto = [];
			var target = [];
			var percent = [];
			
			var total_target = 0;
			var total_actual = 0;
			var total_target_all = 0;
			
			for(var i = 1; i <= 12; i++) {
				total_target_all += parseInt(expected[i]);
			}
			for(var i = 1; i <= 12; i++) {
				if(complete[i]) {
					
					actual.push(parseInt(complete[i]));
					total_actual += parseInt(complete[i]);
				} else {
					actual.push(0);
					total_actual += 0;
				}
				
				if(expected[i]) {
					
					total_target += parseInt(expected[i]);
				} else {
					total_target += 0;
				}
				
				target.push(total_target);
				pareto.push(total_actual);
				percent.push(Math.round((total_actual / total_target_all) * 100));
			}
		
			$(function () {
				var chart;
				$(document).ready(function() {
					chart = new Highcharts.Chart({
						chart: {
							renderTo: container || 'chart',
							type: 'column',
							backgroundColor: '#ffffff'
						},
						title: {
							text: container || ''
						},
						subtitle: {
							text: ''
						},
						xAxis: [{
							categories: months,
							labels: {
								enabled: true,
								align: 'center'
							},
							title: {
								text: 'Month'
							}
						}],
						yAxis: [{
							gridLineWidth: 1,
							min: 0,
							title: {
								text: ''
							},
							allowDecimals: false
						}, {
							gridLineWidth: 1,
							min: 0,
							title: {
								text: ''
							},
							allowDecimals: false,
							opposite: true
						}, {
							gridLineWidth: 1,
							min: 0,
							max: 100,
							title: {
								text: ''
							},
							allowDecimals: false,
							opposite: true,
							labels: {
								formatter: function() {
									return this.value + '%';
								}
							}
						}],
						legend: {
							enabled: false
						},
						plotOptions: {
							series: {
								shadow: false,
								borderColor: '#639ace',
								 marker: {
									enabled: false
								}
							}
						},
						series: [{
							name: 'Month Actual',
							data: actual,
							dataLabels: {
								enabled: true
							},
							type: 'column',
							yAxis: 0
				
						}, {
							name: 'YTD Actual',
							data: pareto,
							type: 'spline',
							yAxis: 1
				
						}, {
							name: 'YTD Target',
							data: target,
							type: 'spline',
							yAxis: 1
				
						}, {
							name: 'Annual Percent Complete',
							data: percent,
							type: 'spline',
							dashStyle: 'shortdash',
							yAxis: 2
				
						}]
					});
				});
			});	
		};
JS
);
	mcl_Html::s(mcl_Html::SRC_CSS, "
		div.chart {
			border:	1px solid #aaa;
			width:	48%;
			height: 300px;
			margin: 5px;
			float:	left;
			padding: 5px;
		}
	");
	foreach($charts as $key=>$value) {
		if(empty($key)) continue;
		$containers .= "<div class='chart' id='{$key}'></div>";
		
	
		$actual = json_encode($value["ACTUAL"]);
		$target = json_encode($value["TARGET"]);
		
		mcl_Html::s(mcl_Html::SRC_JS, "
			dojo.addOnLoad(function() { 
				build_chart({$actual}, {$target}, '{$key}');  
			});
		");
	}
	
	echo "
		<div style = 'margin-left: 5px;'>
		<form method = 'GET' style = 'overflow: hidden; padding: 0px;'>
			<table style = 'font-size: 12px;'>
				<tr><td colspan = '3'><span style='font-size: 10px;'>Auto populated from available audits</span></td></tr>
				<tr style = 'vertical-align: bottom;'>
					<td style='padding-bottom: 3px;'>Audit Date Year</td>
					<td>
						{$select}
					</td>
					<td style = 'text-align: right; padding-bottom: 3px;'>
						" . (!empty($_GET["delegate"]) ? "<input type='hidden' name='delegate' value='{$_GET["delegate"]}'/>" : "") . "
						<input style = 'height: 18px;' type = 'submit' value = 'Filter'/>
					</td>
				</tr>
			</table>
		</form>
	</div>
	{$containers}
	<div style='clear: both;'></div>
	";
?>