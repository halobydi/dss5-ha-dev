<?php
require_once("../../src/php/require.php");

$oci = new mcl_Oci("soteria");

$startDate = @strtotime($start, 0);
$endDate = @strtotime($end, 0);
$weeks = @ceil(($endDate - $startDate) / 604800);

$lc = $_REQUEST["lc_pilot"];

$values = array();

for($i = 0; $i < $weeks; $i++) {
	
	$startSQL = "(TRUNC(TO_DATE('{$start}', 'MM/DD/YYYY HH24:MI:SS'), 'IW') + {$i} * 7)";
	$endSQL = "(NEXT_DAY(TRUNC(TO_DATE('{$start}', 'MM/DD/YYYY HH24:MI:SS'),'IW'), 'SUNDAY') + {$i} * 7) + .999";
	$sql = "
	WITH T AS(
			SELECT 
				USID, 
				NAME, 
				TITLE, 
				REPORTS,
				CASE 
						WHEN TITLE LIKE '%President%' OR TITLE LIKE '%VP%' OR TITLE LIKE '%Vice President%' THEN 'President / VP'
						WHEN TITLE LIKE '%Director%' OR TITLE LIKE 'Dir%' OR TITLE LIKE 'Exec Dir%' THEN 'Director'
						WHEN TITLE LIKE '%Manager%' OR TITLE LIKE 'Mgr%' THEN 'Manager'
						WHEN TITLE LIKE '%Supervis%' OR TITLE LIKE '%Principal Sup%' THEN 'Supervisor'
						ELSE 'Other'
				END AS LEADER_TYPE
			FROM    EMPLOYEES  E 
			WHERE   LEADERS > 0 
					" . ($org ? "AND ORG_CODE = '{$org}'" : "") . "
					AND NOT EXISTS (
						SELECT 	1 
						FROM 	EXCLUSIONS EX 
						WHERE 	EX.USID = E.USID 
								AND ( START_DT <= {$startSQL} OR START_DT IS NULL)
								AND ( END_DT >= {$endSQL} OR END_DT IS NULL	)
					)
		),
		T2 AS (
				SELECT 	USID,
						MAX(EPOP_ID) AS EPOP_ID
				FROM 	T,
						EPOP_OBSERVATIONS O
				WHERE	T.USID = O.OBSERVATION_CREDIT
						AND O.OBSERVED_DATE BETWEEN {$startSQL} AND {$endSQL}
						" . ($lc ? "AND O.LC_PILOT = 1" : "") . "
				GROUP BY USID
		)
		SELECT
				LEADER_TYPE,
				SUM(1) AS TOTAL,
                SUM(DECODE(EPOP_ID, NULL, 0, 1)) AS \"YES\",
                SUM(DECODE(EPOP_ID, NULL, 1, 0)) As \"NO\" 
        FROM 	T
		LEFT JOIN T2 ON T.USID = T2.USID     
        GROUP BY LEADER_TYPE
	";

	while($row = $oci->fetch($sql)){
		$values[$row['LEADER_TYPE']]['YES'] += $row['YES'];
		$values[$row['LEADER_TYPE']]['NO'] += $row['NO'];
		$values[$row['LEADER_TYPE']]['TOTAL'] += $row['TOTAL'];
		
		$values['Total']['YES'] += $row['YES'];
		$values['Total']['NO'] += $row['NO'];
		$values['Total']['TOTAL'] += $row['TOTAL'];
	}			
}

$x = 0;
$summary = array();
$headers = array(
	'President / VP',
	'Director',
	'Manager',
	'Supervisor',
	'Other',
	'Total'
);


$headers_print = "";
foreach($headers as $header) {
	$headers_print .= "
		<th>
			<div class='inner' style='width: 100px;'>{$header}</div>
		</th>
	";
}

$metrics = array(
	'TOTAL' => 'Total Expected (1/Leader/Week)', 
	'YES' => 'Completed Observation', 
	'NO' => 'Did Not Complete Observation'
);
$rows = "";
foreach($metrics as $metric=>$print_metric) {

	$rows .= "<tr class='" . ($x++ % 2 == 0 ? 'even' : 'odd') . "'>";
	$rows .= "<td style='text-align: left;'>{$print_metric}</td>";
	
	foreach($headers as $header) {
		$value = isset($values[$header][$metric]) ? number_format($values[$header][$metric]) : 0;
		$rows .= "<td>{$value}</td>";
	}
	
	$rows .= "</tr>";
}
//percent complete
$rows .= "<tr>";
$rows .= "<td style='text-align: left; border: 1px solid #000; font-weight: bold;'>Percent Complete</td>";
foreach($headers as $header) {
	if($values[$header]['TOTAL']) {
		$value = (round($values[$header]['YES'] / $values[$header]['TOTAL'], 2) * 100) . '%';
	} else {
		$value = 'NA';
	}
	
	$rows .= "<td style='border: 1px solid #000; font-weight: bold;'>{$value}</td>";
}
$rows .= "</tr>";

$summary_tbl = "
	<table class = 'tbl' style = 'clear: both;'>
		<tr>
			<th colspan = '7'>
				<div class='inner_title'>
					Leader Completion ({$start} - {$end})
				</div>
			</th>
		</tr>
		<tr>
			<th>
				<div class='inner' style='width: 200px;'>
					
				</div>
			</th>
			{$headers_print}
		</tr>
		{$rows}
	</table>
";

//Summary
echo "
<div style = 'margin-left: 5px;'>
	<form method = 'GET' action='reportout.php' style = 'padding: 0px;'>
		<table style = 'font-size: 12px;'>
			<tr style='font-size: 10px;'>
				<td>Organization</td>
				<td>Start</td>
				<td>End</td>
			</tr>
			<tr style = 'vertical-align: bottom;'>
				<td>
					<select name='org' style = 'height: 20px; font-size: 10px;'>
						<option value=''>- All Organizations -</option>
						{$org_select}
					</select>
				</td>
				<td style='padding-bottom: 2px;'>
					<input style = 'height: 12px; width: 100px;' type = 'text' name = 'start' id = 'start' value = '{$start}'/> <img style = 'vertical-align: bottom;' src='../../src/img/calendar.gif' alt='' id='tcal' onmouseover='setup_cal(\"tcal\", \"start\");' />
				</td>
				<td style='padding-bottom: 2px;'>
					<input style = 'height: 12px; width: 100px;'type = 'text' name = 'end' id = 'end' value = '{$end}' /> <img style = 'vertical-align: bottom;' src='../../src/img/calendar.gif' alt='' id='tcal2' onmouseover='setup_cal(\"tcal2\", \"end\");' />
				</td>
				<td>
						<input type='checkbox' name='lc_pilot' value='1' " . ($lc ? "checked=checked" : "") . "/> Life Critical Pilot Only
				</td>
				<td style = 'text-align: right; padding-bottom: 3px;'>
					" . (!empty($_GET["delegate"]) ? "<input type='hidden' name='delegate' value='{$_GET["delegate"]}'/>" : "") . "
					<input style = 'height: 18px; font-size: 10px;' type = 'submit' value = 'Filter'/>
					<input onclick = \"dojo.byId('start').value = '{$startWeek}'; dojo.byId('end').value = '{$endWeek}';\" type = 'submit' style = 'height: 18px; font-size: 10px;' value = 'Current Week'/>
					<input onclick = \"dojo.byId('start').value = '{$startWeekPast}'; dojo.byId('end').value = '{$endWeekPast}';\"  type = 'submit' style = 'height: 18px; font-size: 10px;' value = 'Past Week'/>
				</td>
			</tr>
		</table>
	</form>
</div>
<div>
	<div style='padding: 5px;'>
	*Includes supervisors without direct reports
	<br />
	*Includes SWO, QEW & Paired completions
	</div>
	{$summary_tbl}
</div>
";


$sql_io = "
	SELECT 	I.ITEM_CATEGORY, 
			I.ITEM_TEXT, 
			I.ITEM_NUM, 
			I.HAS_SUBITEMS,
			COUNT(*) AS CT 
	FROM EPOP_OBSERVATIONS O,
		EPOP_ANSWERS A,
		EPOP_ITEMS I 
	WHERE A.ANSWER = 'IO'
	AND A.ITEM_NUM = I.ITEM_NUM
	AND A.EPOP_ID = O.EPOP_ID
	AND  OBSERVED_DATE BETWEEN TO_DATE('{$start} 00:00:00', 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE('{$end} 23:59:59', 'MM/DD/YYYY HH24:MI:SS')
	" . (!empty($org) ? "
		AND ORG_CODE = '" . $org ."'
	" : "") . "
		" . ($lc ? "AND LC_PILOT = 1" : "") . "
	GROUP BY ITEM_CATEGORY, ITEM_TEXT, I.ITEM_NUM, I.HAS_SUBITEMS
	ORDER BY COUNT(*) DESC
";

$stmt_sub = $oci->parse("
	SELECT 	Q.SUBITEM_TEXT, 
			Q.SUBITEM_NUM,
			COUNT(*) AS CT 
	FROM EPOP_OBSERVATIONS O, 
		EPOP_ANSWERS A,
		SWO_SUBITEMS Q 
	WHERE  A.ANSWER = 'IO'
	AND A.ITEM_NUM = Q.SUBITEM_NUM
	AND A.EPOP_ID = O.EPOP_ID
	AND O.OBSERVED_DATE BETWEEN TO_DATE('{$start} 00:00:00', 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE('{$end} 23:59:59', 'MM/DD/YYYY HH24:MI:SS')
	AND Q.PARENT_ITEM_NUM = :parent_item_num
	" . (!empty($org) ? "AND ORG_CODE = '" . $org ."'" : "") . "
	GROUP BY SUBITEM_TEXT, 
				Q.SUBITEM_NUM
		ORDER BY CT DESC
");
$io = "<table class='tbl' id='io_tbl'>";	
$io .= "<tr><th colspan='3'><div class='inner_title'>Improvement Opportunities</div></th></tr>";
$io .= "<tr>";
$io .= "<th><div style='width:100px;' class='inner'>No.</div></th>";
$io .= "<th><div style='width:632px;' class='inner'>Observation Item</div></th>";
$io .= "<th><div style='width:100px;' class='inner'>IO Count</div></th>";
$io .= "</tr>";

$x = 0;
$total = 0;
$style = "style='text-align: left; width:580px;'";
while($row = $oci->fetch($sql_io)){
	
	$io .= "<tr class = '" . ($x++ % 2 == 0 ? 'even' : 'odd') . "'>";
	$io .= "<td>{$x}</td>";
	$io .= "<td {$style}>{$row["ITEM_TEXT"]}</td>";
	$io .= "<td>{$row["CT"]}</td>";
	$io .= "</tr>";
	
	if($row["HAS_SUBITEMS"]){
		$oci->bind($stmt_sub, array(
			":parent_item_num"	=> $row["ITEM_NUM"]
		));
		
		while($row2 = $oci->fetch($stmt_sub, false)){
			$io .= "<tr class = '" . ($x++ % 2 == 0 ? 'even' : 'odd') . "'>";
			$io .= "<td></td>";
			$io .= "<td {$style} style='padding-left: 20px;'>{$row2["SUBITEM_TEXT"]}</td>";
			$io .= "<td>{$row2["CT"]}</td>";
			$io .= "</tr>";
		}
		$io .= "</tr>";
	} else {
	}
	
	$total += $row["CT"];
}

if($x == 0){
	$io .= "<tr><td colspan='3'>No improvement opportunites found for the current time frame ({$start} - {$end}).</td></tr>";
} else if($x > 1){
	$border = "style='border: 1px solid black;'";
	$io .= "<tr {$border}>";
	$io .= "<td {$border}>--</td>";
	$io .= "<td {$border} {$style}>Grand Total Improvement Opportunities</td>";
	$io .= "<td {$border}>{$total}</td>";
	$io .= "</tr>";
}

echo "{$breakdown }{$io}";
?>