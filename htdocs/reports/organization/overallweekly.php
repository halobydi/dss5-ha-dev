<?php
	require_once("../../src/php/require.php");
	
	$reporting_orgs = array(
		"CORP SERV" => array(1, '1/week/leader', 40),
		"CUST SERV" => array(1, '1/week/leader', 69),
		"DIST OPS" 	=> array(1, '1/week/leader', 241),
		"FOSS GEN" 	=> array(1, '1/week/leader', 50),
		"MEP" 		=> array(1, '1/week/leader', 63),
		//"IT" 		=> array(1, '1/week/leader'),
		"GAS OPS" 	=> array(1, '1/week/leader', 'TBA'),
		"HUM RES" 	=> array(1, 'Paired 3/Employee*', 21)
	);
	
	$oci = new mcl_Oci("soteria");
	$sql = "SELECT * FROM ORGANIZATIONS";
	
	$orgs = array();
	while($row = $oci->fetch($sql)) {
		if(isset($reporting_orgs[$row["ORG_CODE"]])) {
			$orgs[$row["ORG_CODE"]] = $row["ORG_TITLE"];
		}
	}
	
	$orgs["HUM RES"] = "Corporate Safety";
	
	$leaders_ct_stmt = $oci->parse("
		SELECT 	NVL(COUNT(*), 0) AS LEADERS
		FROM	EMPLOYEES
		WHERE	ORG_CODE = :org_code
				AND REPORTS > 0
				/*AND USID NOT IN(
					SELECT 	USID 
					FROM 	EXCLUSIONS
					WHERE 	(START_DT <= TO_DATE('{$start} 00:00:00', 'MM/DD/YYYY HH24:MI:SS') OR START_DT IS NULL)
							AND (END_DT >= TO_DATE('{$end} 23:59:59', 'MM/DD/YYYY HH24:MI:SS') OR END_DT IS NULL)
				)*/
	");
	
	$swo_weekly_stmt = $oci->parse("
		WITH T AS (
			SELECT	USID,
					CASE WHEN TITLE LIKE '%Director%' THEN 'Director'
						 WHEN TITLE LIKE '%Manager%' THEN 'Manager'
					ELSE 'Supervisor' 
					END AS LEADER_TYPE
			FROM 	EMPLOYEES
			WHERE	REPORTS > 0
					AND ORG_CODE = :org_code
		),
		T2 AS(
			SELECT 	OBSERVATION_CREDIT, 
					MAX(EPOP_ID) AS OBSERVATION 
			FROM 	EPOP_OBSERVATIONS O, T
			WHERE 	OBSERVATION_CREDIT = T.USID
					AND O.OBSERVED_DATE BETWEEN TO_DATE('{$start} 00:00:00', 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE('{$end} 23:59:59', 'MM/DD/YYYY HH24:MI:SS')
					--AND SWO = 1
            GROUP 	BY OBSERVATION_CREDIT
		),
		T3 AS(
			SELECT  T.*,
					DECODE(OBSERVATION, NULL, 0, 1) AS COMPLETE
			FROM    T
			LEFT    JOIN T2
					ON T2.OBSERVATION_CREDIT = T.USID
		)
		SELECT  LEADER_TYPE,
				SUM(COMPLETE) AS COMPLETE
		FROM    T3
		GROUP   BY LEADER_TYPE
	");
	
	$swo_ytd_stmt = $oci->parse("
		SELECT	COUNT(*) AS CT
		FROM	EPOP_OBSERVATIONS
		WHERE	COMPLETED_DATE BETWEEN TRUNC(SYSDATE, 'YEAR') AND SYSDATE
				AND ORG_CODE = :org_code
				AND SWO = 1
	");
	
	$io_stmt = $oci->parse("
		SELECT 	I.ITEM_CATEGORY, 
				I.ITEM_TEXT, 
				I.ITEM_NUM, 
				COUNT(*) AS CT 
		FROM 	EPOP_OBSERVATIONS O,
				EPOP_ANSWERS A,
				EPOP_ITEMS I 
		WHERE 	A.ANSWER = 'IO'
				AND A.ITEM_NUM = I.ITEM_NUM
				AND A.EPOP_ID = O.EPOP_ID
				AND O.SWO = 1
				AND ITEM_CATEGORY IN('Leader', 'Individual', 'Organization')
				AND OBSERVED_DATE BETWEEN TO_DATE('{$start} 00:00:00', 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE('{$end} 23:59:59', 'MM/DD/YYYY HH24:MI:SS')
				AND O.ORG_CODE = :org_code
		GROUP 	BY ITEM_CATEGORY, 
				ITEM_TEXT, 
				I.ITEM_NUM
		ORDER 	BY COUNT(*) DESC
	");

	$week = date("W");
?>
	<div>
		<form method = 'GET' action='overallweekly.php' style = 'overflow: hidden; padding: 5px;'>
			<table style='border: 1px solid #000;'>
				<tr>
					<td colspan= '2' style='background-color: #92b9dc; padding: 5px; font-weight: bold;'>Filter Time Period (Weekly Calculations)</td>
				</tr>
				<tr style = 'vertical-align: bottom;'>
					<td style='font-size: 10px; font-weight: normal; background-color: #e3e1e3; padding: 2px;'>Week Start & End</td>
					<td style='background-color: #e3e1e3; padding: 2px;'>
						<input style = 'height: 12px; border: 1px solid #000; width: 100px;' type = 'text' name = 'start' id = 'start' value = '<?php echo $start;?>'/> 
						<img style = 'vertical-align: bottom;' src='../../src/img/calendar.gif' alt='' id='tcal' onmouseover='setup_cal("tcal", "start");' />
						<span style='font-size: 10px;'>to </span> 
						<input style = 'height: 12px; border: 1px solid #000; width: 100px;'  type = 'text' name = 'end' id = 'end' value = '<?php echo $end; ?>' /> 
						<img style = 'vertical-align: bottom;' src='../../src/img/calendar.gif' alt='' id='tcal2' onmouseover='setup_cal("tcal2", "end");' />
					</td>
				</tr>
				<?php
				echo "<tr>
					<td style='background-color: #e3e1e3; padding: 2px;'></td>
					<td style='text-align: left; background-color: #e3e1e3; padding: 2px;'>
						" . (!empty($_GET["delegate"]) ? "<input type='hidden' name='delegate' value='{$_GET["delegate"]}'/>" : "") . "
						<input type = 'submit' style = \"height: 19px;\" value = 'Filter'/>
						<input onclick = \"dojo.byId('start').value = '{$startWeek}'; dojo.byId('end').value = '{$endWeek}';\" type = \"submit\" style = \"height: 19px;\" value = \"Current Week\"/>
						<input onclick = \"dojo.byId('start').value = '{$startWeekPast}'; dojo.byId('end').value = '{$endWeekPast}';\"  type = \"submit\" style = \"height: 19px;\" value = \"Past Week\"/>
					</td>
				</tr>
			</table>
		</form>
	</div>";
?>
	<div>
		<div style='text-align: center; margin: 5px; font-weight: normal; font-size: 13px;'>Weekly Observation Report <?=$start?> - <?=$end?></div>
		<table style='margin: 5px; '>
			<tr style='background-color: #92b9dc;font-weight: bold;'>
				<th style='width: 150px;' rowspan='2'>Business Unit</th>
				<th style='width: 100px;' rowspan='2'>Minimum<br/>Observations<br/>Unit</th>
				<th style='width: 100px;' rowspan='2'>Weekly<br/>Total<br/>Expected</th>
				<th style='width: 100px;' rowspan='2'>No. of<br/>Leaders</th>
				<th colspan='3'>Weekly Total Completed</th>
				<th style='width: 70px;' rowspan='2'>Weekly<br/>Expectation Met</br>(G, Y, R)</th>
				<th style='width: 70px;' rowspan='2'>YTD Expected</th>
				<th style='width: 70px;' rowspan='2'>YTD Completed</th>
				<th colspan='3'>Weekly Top Trends Observed</th>
			</tr>
			<tr style='background-color: #92b9dc;font-weight: bold;'>
				<th style='width: 70px;'>Directors</th>
				<th style='width: 70px;'>Managers</th>
				<th style='width: 70px;'>Supervisors</th>
				
				<th style='width: 200px;'>1st</th>
				<th style='width: 200px;'>2nd</th>
				<th style='width: 200px;'>3rd</th>
			</tr>
<?php
	foreach($reporting_orgs as $key=>$value) {
	
		echo "<tr>";
			echo "<td style='text-align: left;'>{$orgs[$key]}</td>";
			echo "<td>{$value[1]}</td>";
			echo "<td style='text-align: center;'>{$value[2]}</td>";
			
			$oci->bind($leaders_ct_stmt, array(
				":org_code"	=> $key
			));
			$row = $oci->fetch($leaders_ct_stmt, false);
			$leaders = $row["LEADERS"];
			echo "<td style='text-align: center;'>{$leaders}</td>";
			
			$oci->bind($swo_weekly_stmt, array(
				":org_code"	=> $key
			));
			$complete = array();
			$total = 0;
			while($row = $oci->fetch($swo_weekly_stmt, false)) {
				$complete[$row["LEADER_TYPE"]] = $row["COMPLETE"];
				$total += $row["COMPLETE"];
			}
			echo "<td style='text-align: center;'>{$complete["Director"]}</td>";
			echo "<td style='text-align: center;'>{$complete["Manager"]}</td>";
			echo "<td style='text-align: center;'>{$complete["Supervisor"]}</td>";
			echo "<td style='text-align: center;'>{$total}</td>";
			
			echo "<td style='text-align: center;'>" . ($value[2] * $week) . "</td>";
			
			$oci->bind($swo_ytd_stmt, array(
				":org_code"	=> $key
			));
			$row = $oci->fetch($swo_ytd_stmt, false);
			$total_ytd = $row["CT"];
			echo "<td style='text-align: center;'>{$total_ytd}</td>";
			
			if($key != "HUM RES") {
				$oci->bind($io_stmt, array(
					":org_code"	=> $key
				));
				
				$io = array();
				$x = 0;
				while($row = $oci->fetch($io_stmt, false)) {
					$x++;
					$io[$x] = $row["ITEM_TEXT"];
					if($x == 3) { break; }
				}
			} else {
				$sql = "
					SELECT 	I.ITEM_CATEGORY, 
							I.ITEM_TEXT, 
							I.ITEM_NUM, 
							COUNT(*) AS CT 
					FROM 	EPOP_OBSERVATIONS O,
							EPOP_ANSWERS A,
							EPOP_ITEMS I,
							EMPLOYEES E
					WHERE 	E.USID = O.COMPLETED_BY
							AND A.ANSWER = 'IO'
							AND A.ITEM_NUM = I.ITEM_NUM
							AND A.EPOP_ID = O.EPOP_ID
							AND O.PP = 1
							AND ITEM_CATEGORY IN('Paired Performance')
							AND OBSERVED_DATE BETWEEN TO_DATE('{$start} 00:00:00', 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE('{$end} 23:59:59', 'MM/DD/YYYY HH24:MI:SS')
							AND E.ORG_CODE = '{$key}'
					GROUP 	BY ITEM_CATEGORY, 
							ITEM_TEXT, 
							I.ITEM_NUM
					ORDER 	BY COUNT(*) DESC
				";
				//echo "<pre>{$sql}</pre>";
				
				$io = array();
				$x = 0;
				while($row = $oci->fetch($sql)) {
					$x++;
					$io[$x] = $row["ITEM_TEXT"];
					if($x == 3) { break; }
				}
			}
			echo "<td style='text-align: left; width: 200px; word-break: break-word;'>{$io[1]}</td>";
			echo "<td style='text-align: left; width: 200px; word-break: break-word;'>{$io[2]}</td>";
			echo "<td style='text-align: left; width: 200px; word-break: break-word;'>{$io[3]}</td>";
			
		echo "</tr>";
	}
?>
			<tr>
				<td style='padding: 5px;'>&nbsp;</td>
			</tr>
			<tr>
				<td rowspan='3' style='text-align: right; padding-right: 5px; font-weight: bold; vertical-align: top;'>Expectation Key</td>
				<td style='text-align: left;'>Observation > 80%</td>
				<td style='width: 100px; background-color: #b5d3b5;'>&nbsp;</td>
			</tr>
			<tr>
				<td style='text-align: left;'>Observation  60% - 80%</td>
				<td style='width: 100px; background-color: #ffffbd;'>&nbsp;</td>
			</tr>
			<tr>
				<td style='text-align: left;'>Observation  < 60%</td>
				<td style='width: 100px; background-color: #ffaaad;'>&nbsp;</td>
			</tr>
		</table>
	</div>