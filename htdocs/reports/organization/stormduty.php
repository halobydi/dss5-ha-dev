<?php
require_once("../../src/php/require.php");

$privileges = auth::check('privileges');
if(!$privileges["ACCESS_STORM_DUTY"]){
	auth::deny();
}

mcl_Html::js(mcl_Html::JQUERY);
mcl_Html::s(mcl_Html::INC_JS, "../../src/js/stormduty.js");

$oci = new mcl_Oci("soteria");

$sql = " 
		SELECT 	SD_ID,
				TO_CHAR(OBSERVED_DATE, 'MM/DD/YYYY') AS OBSERVED_DATE,
				OBSERVED_TIME,
				NVL(E.NAME, OBSERVED_BY) AS OBSERVED_BY,
				PUBLIC_SAFETY_TEAM_NUMBER,
				SD.LOCATION,
				ORG_TITLE AS ORGANIZATION
		FROM	STORM_DUTY_OBSERVATIONS SD
		LEFT	JOIN EMPLOYEES E
				ON E.USID = OBSERVED_BY
		LEFT	JOIN ORGANIZATIONS ORG
				ON ORG.ORG_CODE = SD.ORG_CODE
		WHERE 	SD.OBSERVED_DATE BETWEEN TO_DATE('{$start} 00:00:00', 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE('{$end} 00:00:00', 'MM/DD/YYYY HH24:MI:SS')
				" . ($org ? " AND SD.ORG_CODE = '{$org}'" : "") ."
		ORDER	BY SD_ID
";

mcl_Html::s(mcl_Html::SRC_CSS, "
	h6 {
		margin: 1px;
		margin-left: 5px;
		margin-top: 5px;
	}
");

if(isset($_GET["sd"])){
	echo "<div class = '" . ($_GET["sd"] == "1" ? "success" : ($_GET["sd"] == "0" ? "error" : "")) . "' style='margin-top: 5px; margin-left: 5px;'>
		" . ($_GET["sd"] == "1" ? "Successfully saved Storm Duty Observation." : ($_GET["sd"] == "0" ? "Unable to save Storm Duty Observation." : "")) . "
	</div>";
}

?>
<h6>Filter</h6>
<div>
	<form method = 'GET' action='stormduty.php' style = 'overflow: hidden; padding: 5px;'>
		<table style='border: 1px solid #000;'>
			<tr style = 'vertical-align: bottom;'>
				<td style='font-size: 10px; font-weight: normal; background-color: #e3e1e3; padding: 2px;'>Date Observed</td>
				<td style='background-color: #e3e1e3; padding: 2px;'>
					<input style = 'height: 12px; border: 1px solid #000; width: 100px;' type = 'text' name = 'start' id = 'start' value = '<?php echo $start;?>'/> 
					<img style = 'vertical-align: bottom;' src='../../src/img/calendar.gif' alt='' id='tcal' onmouseover='setup_cal("tcal", "start");' />
					<span style='font-size: 10px;'>to </span> 
					<input style = 'height: 12px; border: 1px solid #000; width: 100px;'  type = 'text' name = 'end' id = 'end' value = '<?php echo $end; ?>' /> 
					<img style = 'vertical-align: bottom;' src='../../src/img/calendar.gif' alt='' id='tcal2' onmouseover='setup_cal("tcal2", "end");' />
				</td>
			</tr>
			<tr style = 'vertical-align: bottom;'>
				<td style='font-size: 10px; font-weight: normal; background-color: #f0f0f0; padding: 2px;'>Organization</td>
				<td style='background-color: #f0f0f0; padding: 2px;'>
					<select name='org' id='org'>
						<option value=''></option>
						<?php echo $org_select; ?>
					</select>
				</td>
			</tr>
<?php

		echo "<tr>
				<td style='background-color: #e3e1e3; padding: 2px;'></td>
				<td style='text-align: left; background-color: #e3e1e3; padding: 2px;'>
					" . (!empty($_GET["delegate"]) ? "<input type='hidden' name='delegate' value='{$_GET["delegate"]}'/>" : "") . "
					<input type = 'submit' style = \"height: 19px;\" value = 'Filter'/>
					<input onclick = \"dojo.byId('start').value = '{$startWeek}'; dojo.byId('end').value = '{$endWeek}';\" type = \"submit\" style = \"height: 19px;\" value = \"Current Week\"/>
					<input onclick = \"dojo.byId('start').value = '{$startWeekPast}'; dojo.byId('end').value = '{$endWeekPast}';\"  type = \"submit\" style = \"height: 19px;\" value = \"Past Week\"/>
				</td>
			</tr>
		</table>
	</form>
</div>				
";
?>
<h6>Storm Duty Submissions</h6>
<table class='tbl'>
	<tr>
		<th><div class='inner' style='width: 80px;'>Observation ID</div></th>
		<th><div class='inner' style='width: 100px;'>Date</div></th>
		<th><div class='inner' style='width: 100px;'>Time</div></th>
		<th><div class='inner' style='width: 150px;'>Observer</div></th>
		<th><div class='inner' style='width: 100px;'>Team Number</div></th>
		<th><div class='inner' style='width: 150px;'>Organization</div></th>
		<th><div class='inner' >Location</div></th>
		<th><div class='inner' style='width: 100px;'></div></th>
		<th><div class='inner' style='width: 100px;'></div></th>
		<th><div class='inner' style='width: 100px;'></div></th>
	</tr>
	<?php while($row = $oci->fetch($sql)): ?>
		<tr class='<?=($x++ % 2 == 0 ? 'even'  : 'odd')?>'>
			<td><?= $row['SD_ID'] ?></td>
			<td><?= $row['OBSERVED_DATE'] ?></td>
			<td><?= $row['OBSERVED_TIME'] ?></td>
			<td><?= $row['OBSERVED_BY'] ?></td>
			<td><?= $row['PUBLIC_SAFETY_TEAM_NUMBER'] ?></td>
			<td><?= $row['ORGANIZATION'] ?></td>
			<td><?= $row['LOCATION'] ?></td>
			<td><a href='#' onclick='sot.stormduty.view(<?=$row['SD_ID']?>)'>View</a></td>
			<td><a href='../../forms/stormduty.php?sd_id=<?=$row['SD_ID']?>&returnto=../../reports/organization/stormduty'>Edit</a></td>
			<td><a href='#' onclick='sot.stormduty._delete(<?=$row['SD_ID']?>);'>Delete</a></td>
		</tr>
	<?php endwhile; ?>
</table>
<?php
$sql = " 
	SELECT	ITEM,
			COMMENTS
	FROM	STORM_DUTY_OBSERVATIONS SD		
	LEFT	JOIN STORM_DUTY_ANSWERS SDA
			ON SDA.SD_ID = SD.SD_ID
	LEFT	JOIN STORM_DUTY_ITEMS SDQ
			ON SDA.ITEM_NUM = SDQ.ITEM_NUM
			WHERE COMMENTS IS NOT NULL
			AND SD.OBSERVED_DATE BETWEEN TO_DATE('{$start} 00:00:00', 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE('{$end} 00:00:00', 'MM/DD/YYYY HH24:MI:SS')
			" . ($org ? " AND SD.ORG_CODE = '{$org}'" : "") ."
";

?>
<h6>Comments</h6>
<table class='tbl'>
	<tr>
		<th><div class='inner' style='width: 400px;'>Observation</div></th>
		<th><div class='inner'>Comments</div></th>
	<?php while($row = $oci->fetch($sql)): ?>
		<tr class='<?=($x++ % 2 == 0 ? 'even'  : 'odd')?>'>
			<td style='text-align: left; width: 400px;'><?= $row['ITEM'] ?></td>
			<td style='text-align: left; width: 400px;'><?= $row['COMMENTS'] ?></td>
		</tr>
	<?php endwhile; ?>
</table>

<?php
$sql = " 
	SELECT	ITEM,
			SUM(CASE WHEN ANSWER = 'S' THEN 1 ELSE 0 END) AS SAT,
			SUM(CASE WHEN ANSWER = 'IO' THEN 1 ELSE 0 END) AS IO,
			SUM(CASE WHEN ANSWER = 'NA' THEN 1 ELSE 0 END) AS NA
	FROM	STORM_DUTY_OBSERVATIONS SD		
	LEFT	JOIN STORM_DUTY_ANSWERS SDA
			ON SDA.SD_ID = SD.SD_ID
	LEFT	JOIN STORM_DUTY_ITEMS SDQ
			ON SDA.ITEM_NUM = SDQ.ITEM_NUM
	WHERE	SD.OBSERVED_DATE BETWEEN TO_DATE('{$start} 00:00:00', 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE('{$end} 00:00:00', 'MM/DD/YYYY HH24:MI:SS')
			" . ($org ? " AND SD.ORG_CODE = '{$org}'" : "") ."
	GROUP BY ITEM
";

?>
<h6>S / IO / NA Responses</h6>
<table class='tbl'>
	<tr>
		<th><div class='inner' style='width: 400px;'>Observation</div></th>
		<th><div class='inner' style='width: 100px;'>Satisfactory</div></th>
		<th><div class='inner' style='width: 100px;'>Improvement (IO)</div></th>
		<th><div class='inner' style='width: 150px;'>Not Applicable</div></th>
	</tr>
	<?php while($row = $oci->fetch($sql)): ?>
		<tr class='<?=($x++ % 2 == 0 ? 'even'  : 'odd')?>'>
			<td style='text-align: left; width: 400px;'><?= $row['ITEM'] ?></td>
			<td><?= $row['SAT'] ?></td>
			<td><?= $row['IO'] ?></td>
			<td><?= $row['NA'] ?></td>
		</tr>
	<?php endwhile; ?>
</table>