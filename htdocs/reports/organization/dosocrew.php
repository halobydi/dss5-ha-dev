<?php
require_once("../../src/php/require.php");
$oci = new mcl_Oci('soteria');

$where = "C.DATE_VISITED BETWEEN TO_DATE('". $start . " 00:00:00', 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE('". $end . " 23:59:59', 'MM/DD/YYYY HH24:MI:SS')";
if (isset($_REQUEST["filter"])) {
	$where .= "AND C.SC LIKE '".$_REQUEST["sc"]."'";
	$where .= "AND C.OHUG LIKE '".$_REQUEST["ohug"]."'";
} 
?>
<form method = "GET" action = "dosocrew.php" style = "float: left; padding: 5px; font-size: 12px; font-weight: normal;">
	From:</br>
	<input  type = 'text' name = 'start' id = 'start' value = "<?=$start?>"/> <img src='../../src/img/calendar.gif' alt='' id="tcal" onmouseover="setup_cal('tcal', 'start');" />
	</br>
		To:</br>
	<input type = 'text' name = 'end' id = 'end' value = "<?=$end?>" /> <img src='../../src/img/calendar.gif' alt='' id="tcal2" onmouseover="setup_cal('tcal2', 'end');" />
	</br>
		SC:
	</br>
	<select name = "sc" id = "sc" style = "border: 1px solid black;">
		<option value = "%" <?php $sc = ($_REQUEST["sc"] == "%" ? ' selected = selected ':  ''); echo $sc; ?>>All</option>
		<option value = "PON" <?php $sc = ($_REQUEST["sc"] == "PON" ? ' selected = selected ':  ''); echo $sc; ?>>Pontiac</option>
		<option value = "HOW" <?php $sc = ($_REQUEST["sc"] == "HOW" ? ' selected = selected ':  ''); echo $sc; ?>>Howell</option>
		<option value = "SBY" <?php $sc = ($_REQUEST["sc"] == "SBY" ? ' selected = selected ':  ''); echo $sc; ?>>Shelby</option>
		<option value = "MTC" <?php $sc = ($_REQUEST["sc"] == "MTC" ? ' selected = selected ':  ''); echo $sc; ?>>Mt. Clemens</option>
		<option value = "MAR" <?php $sc = ($_REQUEST["sc"] == "MAR" ? ' selected = selected ':  ''); echo $sc; ?>>Marysville</option>
		<option value = "LAP" <?php $sc = ($_REQUEST["sc"] == "LAP" ? ' selected = selected ':  ''); echo $sc; ?>>Lapeer</option>
		<option value = "NAEC" <?php $sc = ($_REQUEST["sc"] == "NAEC" ? ' selected = selected ':  ''); echo $sc; ?>>NAEC</option>
		<option value = "WW" <?php $sc = ($_REQUEST["sc"] == "WW" ? ' selected = selected ':  ''); echo $sc; ?>>Western Wayne</option>
		<option value = "AA" <?php $sc = ($_REQUEST["sc"] == "AA" ? ' selected = selected ':  ''); echo $sc; ?>>Ann Arbor</option>
		<option value = "NPT" <?php $sc = ($_REQUEST["sc"] == "NPT" ? ' selected = selected ':  ''); echo $sc; ?>>Newport</option>
		<option value = "RED" <?php $sc = ($_REQUEST["sc"] == "RED" ? ' selected = selected ':  ''); echo $sc; ?>>Redford</option>
		<option value = "CAN" <?php $sc = ($_REQUEST["sc"] == "CAN" ? ' selected = selected ':  ''); echo $sc; ?>>Caniff</option>
		<option value = "TRM" <?php $sc = ($_REQUEST["sc"] == "TRM" ? ' selected = selected ':  ''); echo $sc; ?>>Tromby</option>
	</select>
	</br>
	OH/UG:
	</br>
	<select name = "ohug" id = "ohug">
		<option value = "%" <?php $ohug = ($_REQUEST["ohug"] == "%" ? ' selected = selected ':  ''); echo $ohug; ?>>Both</option>
		<option value = "OH" <?php $ohug = ($_REQUEST["ohug"] == "OH" ? ' selected = selected ': ''); echo $ohug;?>>OH</option>
		<option value = "UG" <?php $ohug = ($_REQUEST["ohug"] == "UG" ?  ' selected = selected ': ''); echo $ohug;?>>UG</option>
	</select>
	</br>
	</br>
	
	<input style = "width: 90px;" type = "submit" value = "Filter"/>
	<input type = "hidden" name = 'filter' value = "true"/>
	
</form>
<div style="position: absolute; left: 200px; margin-top: 5px;">
<?php
	
	//Summary
		$sql = "
			SELECT 	TOTAL.SC,
					TOTAL.CT,
					NVL(IO.CT, 0) AS IO_CT
			FROM(
				SELECT 	SC AS SC, 
						COUNT(*) AS CT
				FROM 	EPOP_CREW_OBSERVATIONS C
				WHERE {$where}
				GROUP BY SC
			) TOTAL
			LEFT JOIN(
				SELECT	SC, 
						COUNT(ANSWER) AS CT
				FROM 	EPOP_ANSWERS A
				JOIN	EPOP_CREW_OBSERVATIONS C
					ON	A.EPOP_ID = C.EPOP_ID
				WHERE ANSWER = 'IO'
				AND {$where}
				GROUP BY SC, ANSWER
			) IO
			ON TOTAL.SC = IO.SC
			ORDER BY SC
		";
		
		$r = 0;
		echo '<table class = "tbl">
				<tr class = "header">
					<th colspan = "3">
						<div class="inner_title">
							Summary
						</div>
					</th>
				</tr>
				<tr class = "header">
					<th><div class="inner" style="width: 100px;">SC</div></th>
					<th><div class="inner" style="width: 200px;">Total No. Of SWO Completed</div></th>
					<th><div class="inner" style="width: 200px;">Total No. of IO Ratings Found</div></th>
				</tr>
		';
		
		
		while($row = $oci->fetch($sql)) {
		
		$class = ($r++ % 2 == 0 ? 'even' : 'odd');
		
			echo '<tr class = "'.$class.'">
					<td style = "padding: 2px 10px 2px 10px;">'.$row["SC"].'</td>
					<td>'. ($row["CT"] == "" ? 0 : $row["CT"]).'</td>
					<td>'. ($row["IO_CT"] == "" ? 0 : $row["IO_CT"]) .'</td>
				</tr>';
		
		}
		
		if ($r == 0)
		echo '<tr>
				<td colspan = "3">There is no data to display</td>
			</tr>';
			
		echo '</table>';
		
		//IO ratings count
		
		$sql = "
			SELECT 	ITEM_TEXT, 
					CT 
			FROM(
			SELECT 
				ITEM_NUM,
				COUNT(ANSWER) AS CT
			FROM
				EPOP_ANSWERS A, 
				EPOP_CREW_OBSERVATIONS C
			WHERE 
				A.EPOP_ID = C.EPOP_ID   
				AND A.ANSWER = 'IO'
				AND {$where}
			GROUP BY ITEM_NUM
			) A
			JOIN(
				SELECT	ITEM_CATEGORY,
						ITEM_NUM,
						ITEM_TEXT
				 FROM	EPOP_ITEMS
			)B
			ON A.ITEM_NUM = B.ITEM_NUM
		";
	
		$r = 0;
		echo '<table class = "tbl">
				<tr class = "header">
					<th colspan = "2">
						<div class="inner_title">
							IO Ratings Found
						</div>
					</th>
				</tr>
				<tr class = "header">
					<th><div class="inner" style="width: 650px;">Item</div></th>
					<th><div class="inner" style="width: 100px;">IO Count</div></th>
				</tr>
		';
		
		while($row = $oci->fetch($sql)) {
		
			$class = ($r++ % 2 == 0 ? 'even' : 'odd');
		
			echo '<tr class = "'.$class.'">
					<td style = "text-align: left; width: 650px;" >'.$row["ITEM_TEXT"].'</td>
					<td>'.$row["CT"].'</td>
				</tr>';
		
		}
		if ($r == 0)
		echo '<tr>
				<td colspan = "2">There is no data to display</td>
			</tr>';
			
		echo '</table>';
		
		//List of swos
		$sql = "SELECT 
					SC AS SC,
					CREW,
					LEADER,
					OHUG,
					TO_CHAR(DATE_VISITED, 'MM/DD/YYYY HH24:MI:SS') AS DT,
					NVL(TIME_VISITED, '00:00') AS TIME,
					EPOP_ID
				FROM
					EPOP_CREW_OBSERVATIONS C
				WHERE {$where}
				ORDER BY 
					SC ASC, DT DESC
			";
		

		$r = 0;
		echo '<table class = "tbl">
				<tr class = "header">
					<th colspan = "8">
						<div class="inner_title">
							List of SWO Completed
						</div>
					</th>
				</tr>
				<tr class = "header">
					<th><div class="inner" style="width: 50px;"></div></th>
					<th><div class="inner" style="width: 100px;">SC</div></th>
					<th><div class="inner" style="width: 80px;">Crew Num.</div></th>
					<th><div class="inner" style="width: 150px;">Leader</div></th>
					<th><div class="inner" style="width: 80px;">OHUG</div></th>
					<th><div class="inner" style="width: 100px;">Date Visited</div></th>
					<th><div class="inner" style="width: 80px;">Time Visited</th>
					<th><div class="inner" style="width: 40px;"></th>
				</tr>
		';
		$r = 0;
		while($row = $oci->fetch($sql)) {
		
		$class = ($r++ % 2 == 0 ? 'even' : 'odd');
		
		$sc = true;
		echo '<tr class = "'.$class.'">
				<td>'.$r.'.</td>';
			foreach ($row as $key => $value) {
				
				if($key == "EPOP_ID")
					continue;
						
				if ($sc) {
					$style = 'style = "padding: 2px 10px 2px 10px;"';
					$sc = false;
				} else $style = "";
				
				echo '<td '.$style.'>' . $value .'</td>';
				
			}
			
			echo "
				<td>
					<a href = '#' onclick = 'sot.swo.viewCrew({$row["EPOP_ID"]});'>View</a>
				</td>
			</tr>";
		
		}
		if ($r == 0)
		echo '<tr>
				<td colspan = "8">There is no data to display</td>
			</tr>';
			
		echo '</table>';
?>
</div>
