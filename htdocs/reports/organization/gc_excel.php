<?php
	function errorHandler($errno, $errstr, $errfile, $errline, $errcontext){
	}
	set_error_handler('errorHandler');
	require_once("mcl_Oci.php");

	$oci = new mcl_Oci("soteria");
	$oci->dateFormat();
	$oci->query($sql);

	
	if($_GET["org"] != "") {
		$where .= " AND G.ORG_CODE = '{$_GET["org"]}'";
	}

	$sql = <<<SQL
	SELECT	GC_ID AS "Good Catch ID", 
			NVL(E.NAME, COMPLETED_BY) AS "Completed By",
			COMPLETED_BY_PHONE AS "Completed By Phone",
			COMPLETED_BY_TITLE AS "Completed By Title",
			COMPLETED_DATE AS "Completed Date",
			TO_CHAR(INCIDENT_DATE, 'MM/DD/YYYY') AS "Incident Date",
			INCIDENT_TIME AS "Incident Time",
			C.CAT_TEXT AS "Category",
			NVL(O.ORG_TITLE, 'Unknown') AS "Organization",
			P2.PLANT AS "Employee DTE Work Location",
			P.PLANT AS "Good Catch DTE Location",
			AL.AL_LOCATION AS "Location",
			LOCATION_DESCRIPTION AS "Location Description",
			HAPPENED_BEFORE AS "Happpened Before",
			HAPPENED_DURING AS "Happened During",
			HAPPENED_AFTER AS "Happened After", 	
			NVL(S.NAME, COMPLETED_BY_SUPERVISOR) AS "Completed by Supervisor",
			COMPLETED_BY_SUPERVISOR_PHONE AS "Completed by Supervisor Phone",
			NVL(D.NAME, DIRECTOR) AS "Director",
			NVL(R.NAME, PERSON_RECOGNIZED) AS "Person Recognized",
			CASE WHEN CLOSED = 1 THEN 'Yes' ELSE 'No' END AS "Good Catch Closed"	
	FROM	GOOD_CATCH_OBSERVATIONS G
	LEFT JOIN EMPLOYEES E ON E.USID = G.COMPLETED_BY
	LEFT JOIN EMPLOYEES S ON S.USID = G.COMPLETED_BY_SUPERVISOR
	LEFT JOIN EMPLOYEES D ON D.USID = G.DIRECTOR
	LEFT JOIN EMPLOYEES R ON R.USID = G.PERSON_RECOGNIZED
	LEFT JOIN NEAR_MISS_CATEGORIES C ON C.CAT_ID = G.CATEGORY
	LEFT JOIN NEAR_MISS_ACCIDENT_LOCATIONS AL ON AL.AL_ID = G.LOCATION
	LEFT JOIN NEAR_MISS_PLANTS P ON P.PLANT_ID = G.PLANT
	LEFT JOIN NEAR_MISS_PLANTS P2 ON P2.PLANT_ID = G.COMPLETED_BY_PLANT
	LEFT JOIN (SELECT DISTINCT ORG_CODE, ORG_TITLE FROM ORGANIZATIONS GROUP BY ORG_CODE, ORG_TITLE) O ON O.ORG_CODE = G.ORG_CODE
	WHERE	INCIDENT_DATE BETWEEN TO_DATE('{$_GET["start"]} 00:00:00', 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE('{$_GET["end"]} 23:59:59', 'MM/DD/YYYY HH24:MI:SS')
		{$where}
SQL
;

	

	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Cache-Control: private",false);
	header("Content-Type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"SOTeria_Export_" . time() . ".csv\";" );
	header("Content-Transfer-Encoding: binary");
	
	$ct = 0;
	$header = false;
	
	while($row = $oci->fetch($sql)) {
		if (!$header) {
			$x = 0;				
			foreach($row as $key=>$value) {
				echo ($x++ == 0 ? "" : ",") . $key;
			}
			echo "\n";
			$header = true;
		}		
				
		$x = 0;
		foreach($row as $key => $value) {
			echo ($x++ == 0 ? "" : ",") . "\"" . $value . "\"";
		}
		echo "\n";
		
		$ct++;
	}
?>