<?php
	require_once("/opt/apache/servers/soteria/htdocs/src/php/require.php");
	mcl_Html::js(mcl_Html::HIGHSTOCK);
	$oci = new mcl_Oci("soteria");

	mcl_Html::s(mcl_Html::SRC_CSS, "
		div.chart {
			margin-bottom: 5px;
			width:	1200px;
			border: 1px solid #000;
			margin-left: 5px;
			
		}
	");

	mcl_Html::s(mcl_Html::SRC_JS, "
		var build_chart = function(x, y, title, container) {
			$(function () {
			var chart;
			$(document).ready(function() {
				chart = new Highcharts.Chart({
					chart: {
						renderTo: container || 'chart',
						type: 'column',
						backgroundColor: '#ffffff'
					},
					title: {
						text: ''
					},
					subtitle: {
						text: ''
					},
					
					tooltip: {
						formatter: function() {
							return this.point.xValue + '<br/><b>' + this.point.y + '</b>';
						}
					},
					xAxis: [{
						categories: x,
						labels: {
							enabled: true,
							align: 'center'
							//,rotation: -90
							//,y: 35
							
							,style: {
								fontSize: '10px'
							}
						},
						
						title: {
							text: ''
						}
					}],
					yAxis: {
						gridLineWidth: 0,
						min: 0,
						title: {
							text: ''
						},
						allowDecimals: false
					},
					legend: {
						enabled: false
					},
					plotOptions: {
						series: {
							shadow: false,
							borderColor: '#639ace'
						}
					},
					series: [{
						name: title,
						data: y,
						dataLabels: {
							enabled: true
						},
						color: '#4271a5'
			
					}]
				});
			});
		});	
		};
	");

	if(empty($_GET["start"])) { $start = '01/01/' . date('Y'); }

	if($_GET["form"] == "NEAR_MISS") {
		$form = 'NEAR_MISS';
	} else {
		$form = 'GOOD_CATCH';
	}

	$sql = "
		SELECT	C.CAT_TEXT AS CATEGORY,
				NVL(O.ORG_TITLE, 'Unknown') AS ORGANIZATION,
				P.PLANT AS LOCATION,
				AL.AL_LOCATION AS LOCATION,
				TO_CHAR(INCIDENT_DATE, 'MM Month') AS GC_MONTH,
				NVL(D.NAME, DIRECTOR) AS DIRECTOR,
				NVL(S.NAME, COMPLETED_BY_SUPERVISOR) AS SUPERVISOR
				
		FROM	{$form}_OBSERVATIONS T
		LEFT JOIN EMPLOYEES D ON D.USID = T.DIRECTOR
		LEFT JOIN EMPLOYEES S ON S.USID = T.COMPLETED_BY_SUPERVISOR
		LEFT JOIN NEAR_MISS_CATEGORIES C ON C.CAT_ID = T.CATEGORY
		LEFT JOIN NEAR_MISS_ACCIDENT_LOCATIONS AL ON AL.AL_ID = T.LOCATION
		LEFT JOIN NEAR_MISS_PLANTS P ON P.PLANT_ID = T.PLANT
		LEFT JOIN (SELECT DISTINCT ORG_CODE, ORG_TITLE FROM ORGANIZATIONS GROUP BY ORG_CODE, ORG_TITLE) O ON O.ORG_CODE = T.ORG_CODE
		WHERE	INCIDENT_DATE BETWEEN TO_DATE('{$start} 00:00:00', 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE('{$end} 23:59:59', 'MM/DD/YYYY HH24:MI:SS')
		" . (!empty($org) ? "AND T.ORG_CODE = '{$org}'" : "") ."
	";
	

	$chart_data = array();
	$charts = array(
		"ORGANIZATION" => "Organization", 
		"DIRECTOR" => "Director", 
		"SUPERVISOR" => "Supervisor (Note - Only displaying Supervisors with more than 1 " . ucwords(strtolower(str_replace("_", " ", $form))) . " submitted)", 
		"LOCATION"	=> "Location", 
		"GC_MONTH" => "Month", 
		"CATEGORY" => "Category"
	);
	
	if($org != "") {
		unset($charts["ORGANIZATION"]);
	} else {
		unset($charts["DIRECTOR"]);
		unset($charts["SUPERVISOR"]);
	}
	while($row = $oci->fetch($sql)) {
		foreach($charts as $key=>$value) {
			$chart_data[$key][$row[$key]]++;
		}
	}

	$x = '';
	$y = '';
	
	foreach($charts as $key=>$title) {
		if($key !== 'GC_MONTH') { @asort($chart_data[$key]); }
		else { @ksort($chart_data[$key]); }
		foreach((array)$chart_data[$key] as $xValue =>$yValue) {
			
			if($key === 'GC_MONTH') { $xValue = substr($xValue, 2); }
			$xValue = str_replace("'", "\'", "{$xValue}");
			if(empty($xValue)) {
				$xValue = 'Unknown';
			}
			
			if($key === 'SUPERVISOR') {
				if($yValue <= 1) {
					continue;
				}
			}
			
			$length = count($chart_data[$key]) > 10 ? 4 : 10;
			
			$temp = substr($xValue, 0, $length). (strlen($xValue) > $length ? '...' : '');
		
			$x .= (empty($x) ? "" : ", ") . "'" .  $temp . "'";
			$y .= (empty($y) ? "" : ", ") . "{y: {$yValue}, xValue: '{$xValue}'}";
		}
		mcl_Html::s(mcl_Html::SRC_JS, <<<JS
			build_chart([{$x}], [{$y}], "{$title}", "{$key}");
JS
);
		
		$x = '';
		$y = '';
	}	
	
	$excel = ($form == 'NEAR_MISS' ? "nearmiss_excel" : "gc_excel") . ".php?start={$start}&end={$end}&org={$org}";

?>
<div style = 'text-align: left; margin-right: 10px;'>
	<form method = 'GET' action='gc.php' style = 'overflow: hidden; padding: 5px;'>
		<table style = 'font-size: 11px;' border=0>
			<tr style='font-size: 10px;'>
				<td>Organization</td>
				<td>Incident Type</td>
				<td>Start</td>
				<td>End</td>
			</tr>	
			<tr style = 'vertical-align: bottom;'>
				<td>
					<select name='org' style = 'height: 20px; font-size: 10px;'>
						<option value=''>- All Organizations - </option>
						<?=$org_select?>
					</select>
				</td>
				<td>
					<select name='form' id='form' style='font-size: 10px;'>
						<option value='GOOD_CATCH'>Good Catch</option>
						<option value='NEAR_MISS' <?=($form == 'NEAR_MISS' ? "selected=selected" : "")?>>Near Miss</option>
					</select>
				</td>
				<td style ='padding-bottom: 3px;'>
					<input style = 'height: 12px; width: 100px;' type = 'text' name = 'start' id = 'start' value = '<?=$start?>'/> <img style = 'vertical-align: bottom;' src='../../src/img/calendar.gif' alt='' id='tcal' onmouseover='setup_cal("tcal", "start");' />
				</td>
				<td style ='padding-bottom: 3px;'>
					<input style = 'height: 12px; width: 100px;' type = 'text' name = 'end' id = 'end' value = '<?=$end?>'/> <img style = 'vertical-align: bottom;' src='../../src/img/calendar.gif' alt='' id='tcal2' onmouseover='setup_cal("tcal2", "end");' />
				</td>
				<td style = 'text-align: right; padding-bottom: 3px;'>
					<?=(!empty($_GET["delegate"]) ? "<input type='hidden' name='delegate' value='{$_GET["delegate"]}'/>" : "")?>
					<input style = 'height: 18px; width: 100px;' type = 'submit' value = 'Filter'/>
					<input style = 'height: 18px; width: 100px' type = 'submit' value = 'Excel' onclick='window.open(($("#form").val() == "NEAR_MISS" ? "nearmiss_excel" : "gc_excel") + ".php?org=<?=$org?>&start=" + $("#start").val() + "&end=" + $("#end").val()); return false;'/>
				</td>
			</tr>
		</table>
	</form>
</div>
<?php
	mcl_Html::s(mcl_Html::SRC_JS, "
		var current = 'CATEGORY';
		var toggle = function(form, hidePrevious) {
			
			if(hidePrevious && current && current != form) {
				dojo.byId(current).style.display = 'none';
				dojo.byId(current + '_toggle').innerHTML = '<img src=\"../../src/img/expand.png\"/>';
			}
			
			if(dojo.byId(form).style.display == 'block') {
				dojo.byId(form).style.display = 'none';
				dojo.byId(form + '_toggle').innerHTML = '<img src=\"../../src/img/expand.png\"/>';	
			} else {
				dojo.byId(form).style.display = 'block';
				dojo.byId(form + '_toggle').innerHTML = '<img src=\"../../src/img/collapse.png\"/>';
			}
			current = form;
		
		};
	");
	foreach($charts as $key=>$value) {
		echo "<div class='header'>";
		echo "<div class='caption' onclick='toggle(\"{$key}\", true);'>";
			echo "<span class='toggle' id='{$key}_toggle'><img src='../../src/img/" . ($key == "CATEGORY" ? "collapse" : "expand") .".png'/></span>";
			echo ucwords(strtolower(str_replace("_", " ", $form))) . " by {$value}";
		echo "</div>";
		echo "</div>";
		echo "<div id='{$key}' class='chart' style='display: " . ($key == "CATEGORY" ? "block" : "none") .";'></div>";
	}
?>
