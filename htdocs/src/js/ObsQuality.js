/**
 * Created by halobydi on 1/3/2017.
 */



(function(){

    var chart;
    var MONTHS = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    var minscale=0;            // The minimum scales

    // Data Sets
    var data1= new Array();  // Percentage
    var data2= new Array();     // YTD
    var data3= new Array();     // Partial
    var t= [];                  // Target series

    // Render the chart
    function RenderChart(){
        // The chart configuration values
        var config= {
            // The data set
            type:"bar",
            data: {
                labels: MONTHS,
                datasets: [
                    {
                        label: "Target",
                        type:"line",
                        data:t,
                        borderDash: [15, 15],
                        backgroundColor: "#49a942",
                        borderColor: "#49a942",
                        fill: false,
                        pointRadius:0,
                        pointHoverRadius:3,
                        borderWidth:3
                    },
                    {
                        label: "YTD",
                        type:"line",
                        data:[92, 94, 91],//data2,
                        backgroundColor: "#b31937",
                        borderColor: "#b31937",
                        fill: false,
                        lineTension:0,
                        pointRadius:0,
                        pointHoverRadius:3,
                        borderWidth:0
                    },
                    {
                        label: "Monthly",
                        data:data1,
                        backgroundColor: "#004990",
                        borderColor: "#004990",
                        fill: false,
                        borderDash: [5, 5],
                        pointRadius: 15,
                        pointHoverRadius: 3,
                        borderWidth:0
                    }
                    /*,
                     {
                     label: "Partial Month",
                     data:data3,
                     backgroundColor: "#fce003",
                     borderColor: "#fce003",
                     fill: false,
                     borderDash: [5, 5],
                     pointRadius: 15,
                     pointHoverRadius: 3,
                     borderWidth:0
                     }*/
                ]
            },

            // The chart options
            options: {
                responsive: true,
                legend: {
                    position: 'bottom'
                },
                hover: {
                    mode: 'index'
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Month'
                        }
                    }],
                    yAxes: [{
                        afterTickToLabelConversion:function(data){
                            var ylab=data.ticks;
                            ylab.forEach(function(labels,i){
                                if(i%10 == 0){

                                } else {
                                    ylab[i]='';
                                }
                            })
                        },
                        display: true,

                        ticks:{
                            min: minscale,
                            max:100,
                            stepSize:1,
                            beginAtZero: true,

                        },
                        gridLines:{
                            tickMarkLength	:1
                        },
                        scaleLabel: {
                            display: true,
                            labelString: '% Quality'
                        }


                    }]
                },
                title: {
                    display: true,
                    text: 'Observation Quality'
                }

            }

        }

        // Create and plot the chart object
        chart =new Chart(
            document.getElementById("canvas").getContext("2d"),
            config
        );
    };

    // get  the usrid from the hidden field
    function GetUserid(){
        // return document.getElementById("hdnUserid").value;// window.userid;
        return "e52716";
    }

    // Get the end Date
    function GetYear(){
      //  return document.getElementById('year').value;
      //  return "2016";
        return window.userid;

    }

    // Get the data from the backend
    function GetData(){
        sot.tools.ajax.submit("get", "text", {
            f:"getObsComplianceData",
            userId:GetUserid(),
            yr:GetYear()
        }, function(_e){
            console.log(_e);
            // Parse the JSOn response
            var json=JSON.parse(_e);


            data1=new Array(); // Percentage
            data2=new Array(); // Target



            // Parse & prepare the data
            for(var i=0;i<json.length;i++){
                if(json[i].status!=0)
                {
                    switch (json[i].status)
                    {
                        case(1):
                            // No Team
                            ShowMessage("No Team Found","Error");
                            break;
                        case(2):
                            // No Data
                            ShowMessage("No Data Found","Error");
                            break;
                        case(3):
                            // No LD Data
                            ShowMessage("No LD Data Found","Error");
                            break;
                    }
                    return true;
                }
                if(json[i].ytd>=0)
                {
                    data1.push(json[i].percentage);
                    data2.push(json[i].ytd);
                }
            }
            console.log(data1);
            console.log(data2);
             ShowMessage("","");

            // Render the chart
            RenderChart();
            return true;
        }, true, true);
    }

    // Get the target
    function GetTarget(){

        sot.tools.ajax.submit("get", "text", {
            f: "getObsTarget",
            TARGETTYPE:"OC",
            yr:GetYear()
        }, function(_e){

            _e=JSON.parse(_e);
            console.log(_e);


            //  if(_e.target=="-1")
            //      return true;

            // Create target array
            t=new Array();
            for(var i=0;i<=11;i++){
                t.push( _e.target);
            }

            // Show the Message for the user about the duration of the report
            /*
             ShowWeekMessage(
             GetYear(), // Get target year from from the text box
             _e.startDate, // The start date of the year
             _e.endDate, // The end date of the year
             _e.currentWeek // The current reporting week number
             );
             */

            console.log(t);
            // Start Getting data
            GetData();
            // RenderChart();

            return true;
        }, true, true);
    }

    // Refresh date
    function Refresh() {
        // ShowMessage("Loading ...... ","Info");
        GetTarget();
    }

    // Show the message to the usr when it is loading or an error
    // The message to display
    function ShowMessage(msg){
        document.getElementById("meter").innerText=msg;
    }

    // Display the message if the week
    // yr: the targeted year
    // syr: Start date
    // eyr: End date
    // cwn: Current year week num
    function ShowWeekMessage(yr,syr,eyr,cwn){
        //   if(cwn>0){

        document.getElementById("txtYearMessage").innerText =
            yr + " Compliance Year: " + syr + " to  " +eyr;

        //    document.getElementById("txtWeekMessage").value =
        //       "Week " + cwn;

        //  }
    }



    /////////////////////////////////////
    // On page load will  trigger the refresh action to connect to the backend and grab the data
    window.onload = function(){
        try{
            // Set the time out in case there is an error on the backend
            var timeout=true;
            setTimeout(function () {
                if(timeout)
                    ShowMessage("","");
            },30000);

            // document.getElementById("year").value="2016";

            // Export to PNG Button
            var btn=document.getElementById("btn-export");

            // The popup  modal
            var modal = document.getElementById('myModal');

            // Get the <span> element that closes the modal
            var span = document.getElementById("btn-close");

            // When the user clicks the button, open the modal
            btn.onclick = function() {
                var d= document.getElementById("canvas").toDataURL("image/png");
                document.getElementById("chart-imgs").setAttribute("src",d);
                modal.style.display = "block";
            };

            // When the user clicks on <span> (x), close the modal
            span.onclick = function() {
                modal.style.display = "none";
            };

            //////////////////////////////////////
            //  Add onclick events for buttons to navigate throw the years and reflect the new changes
            // When the user click on the next button
            document.getElementById("previous").onclick=function(){
                document.getElementById("year").value
                    = (++document.getElementById("year").value); // Increment year
                Refresh(); // refresh the page
            }

            // When the user click on the previous button
            document.getElementById("next").onclick=function(){
                document.getElementById("year").value
                    = (--document.getElementById("year").value); // Decrement year
                Refresh(); // refresh the page
            }

            // When the user clicks anywhere outside of the modal, close it
            window.onclick = function(event) {
                if (event.target == modal) {
                    modal.style.display = "none";
                }
            }

            Refresh();

        }
        catch (ex){
            console.log(ex);

        }

    };

})()

function setUserID(usrid){
    window.userid=usrid;
}




