dojo.provide("sot.nm");
var baseDir = window.location.protocol + "//" + window.location.host;

sot.nm = function() {
	return {
		view: function(nm_id) {
			window.open(baseDir + "/forms/nearmiss.php?nm_id=" + nm_id + "&mode=readonly", "_blank", "width=1000, toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes");
		},
		_delete: function(nm_id) {
			if(confirm("Are you sure you want to delete this Near Miss Incident (Incident #" + nm_id + ")?\nThis action cannot be undone.")) {
				sot.tools.ajax.submit("get", "json", {
					f: "deleteNearMiss",
					nm_id: nm_id
				}, function(_e){
					window.location.reload();
					
					return true;
				}, true, true);
			}
		}
	};
	
}();