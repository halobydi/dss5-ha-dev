dojo.provide('sot.hp');
var baseDir = window.location.protocol + "//" + window.location.host;

sot.hp = function(){
	var js_calendar;
	var c2Html = '';
	var c2Counter = 0;
	//var keyUpFired = false;

	return {
		edits: function() {
			var answerBox = dojo.byId('prev_answers');
			if(!answerBox) {
				return;
			}
			var answers = (answerBox.value) ? JSON.parse(answerBox.value) : '';
			var c2Counter = -1;
			console.log('answers', answers);
			dojo.byId('comments').value = (answers['header']['COMMENTS'] != null) ? answers['header']['COMMENTS'] : '';
			dojo.byId('incident_date').value = answers['header']['INCIDENT_DATE'];
			var org = dojo.byId('org');
			for(var j = 0; j < org.options.length; j++) {
				if (org.options[j].text ===  answers['header']['ORGANIZATION'])
					org.selectedIndex = j;
			}
			sot.hp.switchOrg();
			var eventType = dojo.byId('event_type');
			for(var j = 0; j < eventType.options.length; j++) {
				if (eventType.options[j].text ===  answers['header']['EVENT_TYPE'])
					eventType.selectedIndex = j;
			}
			var cats = (answers['header']['CATEGORY']) ? answers['header']['CATEGORY'].split(';') : '';
			var incs = (answers['header']['INCIDENT_TYPE']) ? answers['header']['INCIDENT_TYPE'].split(';') : '';
			for(var i in cats) {
				$('[name=category][value="'+cats[i]+'"]').attr('checked', 'checked');
			}
			for(var i in incs) {
				$('[name=incident_type][value="'+incs[i]+'"]').attr('checked', 'checked');
			}


			sot.hp.switchType();

			//fill out all visible fields with their answers
			var c2s = [];
			var c2complete = true;
			var c2qs = [150, 151, 152, 153, 153.5, 154, 154.5, 155, 156, 157];
			for(var i in answers['152']) {
				// console.log('151', i);
				if(i === 'null') {
					var allNull = true;
					for(var j in c2qs) {
						// console.log('cycling', answers[j]);
						if(answers[j]!=null) allNull = false;
					}
					if(allNull)continue;
				}
				sot.hp.addCountermeasure();
				c2s.push(i);
			}
			// console.log('c2s', c2s);
			var items = $('[data-item][data-subitem]');
			// console.log(items);
			for(var i = 0; i<items.length; i++) {
				var item = items[i];
				var q = item.getAttribute('data-item');
				if(!answers[q]) {
					// console.log(q);
					continue;
				}
				var subq = item.getAttribute('data-subitem');
				if(150 <= q&&q <= 157) {
					subq = c2s[subq];
				}
				var type = item.getAttribute('type');
				var tagname = item.tagName;
				if((type === 'text'||tagname === 'TEXTAREA')&&(subq !== 'twin'))
					item.value = (answers[q][subq]) ? answers[q][subq] : '';
				else if(type == 'checkbox'||type == 'CHECKBOX') {
					if(typeof answers[q][subq] === 'object') {
						if (answers[q][subq]['answer']==='YES'||answers[q][subq]['answer']=='1') {
							item.setAttribute('checked', 'checked');
							item.onclick();
							dojo.byId('ex'+q).value = answers[q][subq]['explanation'];
						}
					}
					if (answers[q][subq]==='YES'||answers[q][subq]=='1') {
						item.setAttribute('checked', 'checked');
						if(item.onclick!==null) {
							item.onclick();
						}
					}
				}
				else if(type === 'radio') {
					if (item.className === answers[q][subq]) {
						item.setAttribute('checked', 'checked');
						if(item.onclick!==null) {
							item.onclick();
						}
					}
				}
				else if (tagname === 'SELECT'||tagname==='select') {
					for(var j = 0; j < item.options.length; j++) {
						if (item.options[j].text === answers[q][subq]) {
							item.selectedIndex = j;
							if(item.onchange!==null) {
								item.onchange();
							}
						}
					}
				}
			}
			dojo.byId('incident_date').focus();
		},

		cal: function(id, field) {
			if (!js_calendar) {
				js_calendar = Calendar.setup({
						animation: false,
						onSelect: function() { this.hide() }
				});
			}

			js_calendar.manageFields(id, field, '%m/%d/%Y');
			sot.hp.markComp();
		},
		markComp: function() {
			var total = 0, y = 0;
			total = $('[data-item=152]:visible').length;
			y = $('[data-item=151][value=Y]:visible:checked').length;
			dt = $('[data-item="154.5"][value!=""]:visible').length;
			if(total>0&&total===y&&dt===total)
				$('#complete').prop('checked', 'checked');
			else
				$('#complete').removeAttr('checked');
			// console.log(total, y, dt, $('#complete').prop('checked'));

		},
		view: function(hpId){
			window.open(baseDir + '/forms/viewHp.php?hpid=' + hpId, '_blank', 'width=1000, toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes');
		},
		inputUsid: function(input, num) {
			var val = $(input).val();
			// console.log('val', val, 'input', input);
			var usid = val.replace(/\s/g, '');
			num = $(input).attr('data-item');
			if(num == 153) num = 153.5;
			var rep = $(input).attr('data-subitem');
			var event = window.event;
			
			if(usid.length == 6){
				var prevOrg = dojo.byId("org").value;
				sot.tools.ajax.submit("get", "json", {
					f: "getInfo",
					usid: usid
				}, function(_e){
					$('[id="'+num+'_auto"][data-subitem='+rep+']').val(_e.name);	
					return true;
				}, true, false);
			} else {
				$('[id="'+num+'_auto"][data-subitem='+rep+']').val('');
			}
		},
		twinClick: function(checkbox) {
			var id = checkbox.id+'row';
			if(checkbox.checked) $('#'+id).show();
			else $('#'+id).hide();
		},
		disable: function() {
			$(':input').attr('onclick', 'return false;');
		},
		
		addCountermeasure: function() {
			space = document.getElementById('c2Additional')
			if(space.style.display == 'none') {
				space.style.display = 'inline';
				content = document.getElementById('c2ContentBlock')
				c2Html = content.innerHTML.replace(/data-subitem="0"/g, 'data-subitem="1"').replace(/-entry0/g, "-entry1");
				c2Counter = 0;
				$('div[title]').not('.twin_cell').qtip({ 
					position: {
						corner: {
							target: 'topMiddle',
							tooltip: 'bottomMiddle'
						}
					},
					show: {
						delay: 0
					},
					style: {
						tip:'bottomMiddle',
						width: 200,
						border: {
							width: 2
						}
					}
				});
			}
			else {
				var re = new RegExp('data-subitem="'+c2Counter+'"', 'g');
				var re2 = new RegExp('-entry'+c2Counter, 'g');
				var div = document.createElement('div');
				c2Counter++;
				// console.log(re, c2Counter, c2Html.substr(944, 16));
				c2Html = c2Html.replace(re, 'data-subitem="'+c2Counter+'"').replace(re2, '-entry'+c2Counter);
				div.innerHTML = c2Html;
				space.appendChild(div);
				$('div[title]').not('.twin_cell').qtip({ 
					position: {
						corner: {
							target: 'topMiddle',
							tooltip: 'bottomMiddle'
						}
					},
					show: {
						delay: 0
					},
					style: {
						tip:'bottomMiddle',
						width: 200,
						border: {
							width: 2
						}
					}
				});
			}
			sot.hp.markComp();
		},
		
		getMatches: function(id, itemTag, idTag) {
			var rows = top.document.getElementsByTagName(itemTag);
			var result = [];
			for(var i = 0; i<rows.length; i++) {
				if(rows[i].getAttribute(idTag) === id) {
					result.push(rows[i]);
				}
			}
			// console.log(result);
			return result;
		},
		
		subClick: function(selected, id, condition) {
			display = ((selected.className == condition)||(condition == 'A')) ? 'table-row' : 'none';
			$('[id^=subitem_'+id+']').css('display', display);
			if(condition == 'D') {
				if(selected.className == 'Y') {
					$('[id=subitem_32][data-subitem2=112]').hide();
					$('[id=subitem_32][data-subitem2=111]').show();
					$('#warningMsg_111').html('<b>An SAP IA form is required for this incident.</b>');
				}
				else if(selected.className == 'N'){
					// $('[id^=subitem_'+id+']').css('display', 'table-row');
					$('[id=subitem_32][data-subitem2=111]').hide();
					$('[id=subitem_32][data-subitem2=112]').show();
					$('#warningMsg_112').html('<b>A near miss form is required for this incident.</b>');
				}
				else{
					// $('[id^=subitem_'+id+']').css('display', 'table-row');
					$('[id=subitem_32][data-subitem2=111]').hide();
					$('[id=subitem_32][data-subitem2=112]').hide();
				}
			}
		},
		
		switchOrg: function(){
			var org = $('#org').val();
			if(org === 'DIST OPS') {
				dojo.byId('resCat1').innerHTML = 'Reset Category:';
				dojo.byId('resCat2').innerHTML = 
					"<p style='font-weight:bold'>Note: All these events must be the result of human error.</p>\
					<input type='CHECKBOX' class='header2' name='category' value='Customer Outage'></input>&nbsp;Customer Outage<br>\
					<input type='CHECKBOX' class='header2' name='category' value='Any re-design, that occurs after a project is released for construction, that results in a return for a significant re-design effort.  Significant is defined as requiring modifications that impact at least 35% of the original estimated cost and exceeds $35,000, or redesign costs exceeding $100,000 (large projects).'></input>&nbsp;Any re-design, that occurs after a project is released for construction, that results in a return for a significant re-design effort.  Significant is defined as requiring modifications that impact at least 35% of the original estimated cost and exceeds $35,000, or redesign costs exceeding $100,000 (large projects).<br>\
					<input type='CHECKBOX' class='header2' name='category' value='Mis-operation of equipment due to a design error.'></input>&nbsp;Mis-operation of equipment due to a design error.<br>\
					<input type='CHECKBOX' class='header2' name='category' value='Any missed customer commitment job, due to design delays, improper scheduling or ineffective execution resulting in a corporate or MPSC complaint.  '></input>&nbsp;Any missed customer commitment job, due to design delays, improper scheduling or ineffective execution resulting in a corporate or MPSC complaint.  <br>\
					<input type='CHECKBOX' class='header2' name='category' value='Multiple crews at restore location (extra personnel not needed), other critical team not present, or multiple visits to resolve a problem.'></input>&nbsp;Multiple crews at restore location (extra personnel not needed), other critical team not present, or multiple visits to resolve a problem.<br>\
					";
			}
			else if(org === 'FOSS GEN') {
				dojo.byId('resCat1').innerHTML = 'Reset Category:';
				dojo.byId('resCat2').innerHTML = 
					"<input type='CHECKBOX' class='header2' name='category' value='Unit Derate, Unit Outage/Trip'></input>&nbsp;Unit Derate, Unit Outage/Trip<br>\
					";
			}
			else {
				dojo.byId('resCat1').innerHTML = '';
				dojo.byId('resCat2').innerHTML = '';			
			}				
		},
		
		switchType: function(){
			document.getElementById('Electrical Event').style.display = 'none';
			document.getElementById('Electrical Contact Event').style.display = 'none';
			document.getElementById('Arc Flash Event').style.display = 'none';
			$('.elecPicker').hide();
			var typeMenu = document.getElementById("event_type");
			var selected = typeMenu.options[typeMenu.selectedIndex].value;
			// if(selected == 'OTHER' || !document.getElementById(selected)) return;
			// document.getElementById(selected).style.display = 'inline';
			if(selected.indexOf("Electrical") >= 0) {
				// console.log(selected, 'match');
				document.getElementById('Electrical Event').style.display = 'block';
				$('.elecPicker').show();

				// document.getElementById('Electrical Contact Event').style.display = 'block';
				// document.getElementById('Arc Flash Event').style.display = 'block';
			}
			if(dojo.byId('Electrical Contact Event_check').checked)
				document.getElementById('Electrical Contact Event').style.display = 'block';
			if(dojo.byId('Arc Flash Event_check').checked)
				document.getElementById('Arc Flash Event').style.display = 'block';
		},		
		imposeMaxLength: function(Object, MaxLen){
			return (Object.value.length <= MaxLen);
		},
		// originally this was written to submit via AJAX, but later on photo upload capability was added into the form.
		  // you can't submit files via ajax for security reasons, so instead of making the ajax call I pushed all the validated/ajax-arranged
		  // data into a hidden field on a form surrounding the photo upload on the php, and then submitted that form. 
		onSubmit: function(){ 
			sot.hp.markComp();
			var completed = (dojo.byId('complete').checked) ? true : false;
			var inputObjs = {};
			var radioWarned = false;
			var id = $('#hp_id').val();
			headerInfo = $(':input.header');
			inputObjs['header_info'] = {};
			inputObjs['header_info']['completed'] = (completed) ? 'Y' : 'N';
			inputObjs['header_info']['hp_id'] = (id) ? id : 0;

			// grab header info
			for(var i = 0; i < headerInfo.length; i++) {
				var val = '';
				if(headerInfo[i].name == 'org') {
					val = headerInfo[i].options[headerInfo[i].selectedIndex].value;
					inputObjs['header_info']['org_name'] = headerInfo[i].options[headerInfo[i].selectedIndex].text;
					inputObjs['header_info']['org_code'] = val;
				}
				else {
					val = headerInfo[i].value;
					inputObjs['header_info'][headerInfo[i].name] = headerInfo[i].value;
				}
				// if(val == ''&&(!(headerInfo[i].id == 'comments'))) {
				// 	alert('Please provide all required information.');
				// 	headerInfo[i].focus();
				// 	return;
				// }
			}

			headerInfo = $(':input.header2');
			inputObjs['header_info']['category'] = '';
			inputObjs['header_info']['incident_type'] = '';
			for(var i = 0; i < headerInfo.length; i++) {
				if(headerInfo[i].checked) {
					inputObjs['header_info'][headerInfo[i].name] += (headerInfo[i].checked) ? headerInfo[i].value+';' : '';
				}
			}

			inputObjs['header_info']['event_type'] = $('#event_type').val();
			// if(inputObjs['header_info']['event_type'] == '') {
			// 	alert('Please provide all required information.');
			// 	headerInfo[i].focus();
			// 	return;
			// }

			// validate inputs
			var inputs = $(':input:visible:not(".header"):not("[name^=p_desc]"):not(".header2"):not("#complete")'); //all visible inputs that aren't part of the header info or photo upload
			inputObjs['answers'] = {};
			inputObjs['twin'] = {};
			for(var i = 0; i < inputs.length; i++) {
				// if($(inputs[i]).closest('tr').css('display') == 'none') continue;
				// if($(inputs[i]).closest('div').css('display') == 'none') continue;
				// if($(inputs[i]).closest('table').css('display') == 'none') continue;
				if($(inputs[i]).parents(':hidden:not("#c2Additional")').length) {
					// console.log('hidden parents', $(inputs[i]).parents(':hidden:not("#c2Additional")'));
					continue;
				}
				var type = inputs[i].type;
				var name = inputs[i].name;
				name = (name) ? name.replace('.', '\\.') : name;
				var id = inputs[i].id;
				var val = '';
				var freeform = 0;
				var item_id = inputs[i].getAttribute('data-item');
				if(!item_id) console.log('No item number:', inputs[i]);
				var subitem_id = inputs[i].getAttribute('data-subitem');
				if(type == 'radio') {
					val = $('[name='+name+']:checked').attr('class');
					if(val === undefined) {
						// if(!radioWarned) {
						// 	var y = confirm('Not all radio options are checked. Do you want to continue? All empty radio options will be marked "Not Applicable".')
						// 	if(!y) return;
						// 	radioWarned = true;
						// }
						// val = 'NA';
						continue;
					}
				}
				else if(type == 'select-one') {
					type = 'select';
					val = inputs[i].options[inputs[i].selectedIndex].text;
					// if(val == 'Choose'||val =='') {
					// 	alert("Please select an option.");
					// 	inputs[i].focus();
					// 	return;
					// }
				}
				else if(type == 'checkbox') {
					val = (inputs[i].checked) ? 1 : 0;
				}
				else if(type == 'text'||type == 'textarea') {
					val = inputs[i].value;
					freeform = 1;
					// if(val == '') {
					// 	alert("All text fields are required.");
					// 	inputs[i].focus();
					// 	return;
					// }
					if(name == '154'&&val !== '') {
						var regex = /^(?:(0[1-9]|1[012])[\/.](0[1-9]|[12][0-9]|3[01])[\/.](19|20)[0-9]{2})$/;
						if(!regex.test(val)) {
							alert("Date not in MM/DD/YYYY format and/or within the years 1900-2090.");
							inputs[i].focus();
							return;
						}
					}
				}
				else continue;
				if(item_id == '') console.log('no item id', inputs[i]);
				var inputObj = {};
				inputObj['id'] = name;
				inputObj['val'] = val;
				inputObj['item_id'] = item_id;
				subitem_id = (subitem_id == 'null') ? null : subitem_id;
				if(item_id == null) console.log('no item id', inputs[i]);
				if(subitem_id == 'twin') {
					if(name.substr(0, 2)=='ex') {
						inputObjs['twin'][item_id]['comment'] = val;
					}
					else
						inputObjs['twin'][item_id] = {'answer':val};
					if(val) {
						if(dojo.byId('ex'+item_id).value === '') {
							alert('Explanations for precursors are required.');
							dojo.byId('ex'+item_id).focus();
							return;
						}
					}
				}
				else {
					inputObj['subitem_id'] = subitem_id;
					inputObj['type'] = type;
					inputObj['freeform'] = freeform;
					inputObjs['answers'][i] = inputObj;	
				}
			}
			// console.log('input objs', inputObjs);
			var jString = JSON.stringify(inputObjs);
			dojo.byId('inputObjs').value = JSON.stringify(inputObjs);
			window.top.window.sot.tools.cover(true);
		 	document.photoInp.submit();
			return true;
		},
		_delete: function(hp_id) {
			if(confirm("Are you sure you want to delete this Anatomy of an Event Report (#" + hp_id + ")?\nThis action cannot be undone.")) {
				sot.tools.ajax.submit("get", "json", {
					f: "deleteHP",
					hp_id: hp_id
				}, function(_e){
					window.location.reload();
					
					return true;
				}, true, true);
			}
		},
		getHtml: function() {
			// $(':inputs').each({
			// 	$(this).attr('value', $(this).val());
			// })
			$('img').each(function() {
				$(this).removeAttr('src');
			});
			dojo.byId('formHtml').value = $('#container').html();
			viewPDF.submit();

		}
	};
}();