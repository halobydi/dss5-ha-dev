dojo.provide('sot.swo');
var baseDir = window.location.protocol + "//" + window.location.host;

sot.swo = function(){
	var items = {};
	var subitems = {};
	var subitemsActive = 0;
	var ioCt = 0;
	var js_calendar;
	var employees = {};
	var employeeCt = 1;
	//var keyUpFired = false;
	
	return {
		cal: function(id, field) {
			if (!js_calendar) {
				js_calendar = Calendar.setup({
						animation: false,
						onSelect: function() { this.hide() }
				});
			}
			
			js_calendar.manageFields(id, field, '%m/%d/%Y');
		},
		
		inputUsid: function(input, num){
			var usid = input.value.replace(/\s/g, '');
			var event = window.event;
			
			if(usid.length == 6){
				var prevOrg = dojo.byId("org").value;
				sot.tools.ajax.submit("get", "json", {
					f: "getInfo",
					usid: usid
				}, function(_e){
		
					dojo.byId("name_" + num).value = _e.name;
					dojo.byId("nameValidate_" + num).value = _e.name;
					dojo.byId("org").value = _e.org || '';
					dojo.byId("location").value = _e.loc || '';
					
					var newOrg = dojo.byId("org").value;
				
					if(newOrg != prevOrg) {
						sot.swo.switchOrg();
					}
							
					return true;
				}, true, false);
			} else {
				dojo.byId("name_" + num).value = '';
				dojo.byId("nameValidate_" + num).value = '';
				if(num == 1) {
					dojo.byId("org").value = '';
					dojo.byId("location").value = '';
				}
			}
			
			return false;
		
		},
		
		switchOrg: function(){
			if(subitemsActive > 0){
				for(var i in subitems){
					sot.swo.getSubItems(i);
				}
			}
		},
		
		switchType: function(value){
			if(value.toLowerCase() == 'onsite') {
				dojo.byId('onsite').style.display = 'block';
				dojo.byId('offsite').style.display = 'none';
			} else {
				dojo.byId('onsite').style.display = 'none';
				dojo.byId('offsite').style.display = 'block';
			}
		},

		qewMarkAddComment: function(itemNum, value){
			sot.swo.markItem(itemNum,value,false,true);
			var imgObj = $("#" + itemNum + "_comments_img");
			sot.swo.addResponsiveComment(itemNum,imgObj);
		},

		qewMarkRemoveComment: function(itemNum, value){
			sot.swo.markItem(itemNum,value,false,true);
			var imgObj = $("#" + itemNum + "_comments_img");
			sot.swo.removeResponsiveComment(itemNum,imgObj);
		},

		// ForceHideComments was added because some forms, such as LifeCritical, do not need comments - MH
		markItem: function(itemnum, value, hasSubItems, affectSubmit, crew, ohug, forceHideComments, customPlaceholder){
			if(hasSubItems){
			
				if(items[itemnum] && items[itemnum] == value){
					//do nothing they just double clicked on the same questions/same response
				} else {
					subitems[itemnum] = true;
					if(!items[itemnum] && value == "IO" || items[itemnum] && items[itemnum] !== "IO" && value == "IO"){
						
						//if(crew && itemnum != 25) {} else {
						//	sot.swo.getSubItems(itemnum, crew, ohug);
						//}
						if(crew) {
							sot.swo.getSubItems(itemnum, crew, ohug);
						}
						
						subitemsActive++;
					} else {
						dojo.byId(itemnum + "_subitems").innerHTML = "";
						
						if(subitems[itemnum]){
							delete subitems[itemnum];
						}
						
						if(items[itemnum] == "IO" && value != "IO"){
							subitemsActive--;
						}
					}
				}
			}
			
			var addComments = false;
			var removeComments = false;
			
			if(!items[itemnum] && value == "IO"){
				ioCt++;
				addComments = true;
			} else if(items[itemnum] && items[itemnum] !== "IO" && value == "IO"){
				ioCt++;
				addComments = true;
			} else if(items[itemnum] == "IO" && value != "IO"){
				ioCt--;
				removeComments = true;
			}
			else if(items[itemnum] == "S" && value != "S"){
				ioCt--;
				removeComments = true;
			}

			if( typeof forceHideComments !== 'undefined' ) {
				if (forceHideComments) { addComments = false; }
				if (!forceHideComments) { addComments = true; }
			}
			
			if(addComments && affectSubmit) {
				if(dojo.byId(itemnum + "_comments").childNodes.length == 0) {
					sot.swo.addComment(itemnum, dojo.byId(itemnum + "_comments_img"));
					var commentsBox = document.getElementById(itemnum + "_comments_textarea");
					commentsBox.required = true;
					//$( commentsBox ).css("width","100%");
                    if (customPlaceholder != undefined && customPlaceholder != "") {
                        commentsBox.placeholder = customPlaceholder;
                    }
				}
			} else if(removeComments && affectSubmit) {
				if(dojo.byId(itemnum + "_comments").childNodes.length == 1 && dojo.byId(itemnum + "_comments").childNodes[0].value == "") {
					document.getElementById(itemnum + "_comments_textarea").required = false;
					sot.swo.removeComment(itemnum, dojo.byId(itemnum + "_comments_img"));
				}
			}
			
			items[itemnum] = value;
		},
		
		addComment: function(itemnum, img){
		    if (!itemnum == "2141"){
                img.setAttribute('src', baseDir + '/src/img/removecomments.png');
                img.setAttribute('alt', 'Remove Comment');
                img.onclick = function(){
                    sot.swo.removeComment(itemnum, img);
                };
                dojo.byId(itemnum + "_comments").innerHTML = "<textarea style='width: 793px' placeholder='*Required*' name='" + itemnum + "_comments' id='" + itemnum + "_comments_textarea' onkeypress='return sot.swo.imposeMaxLength(this, 1000);'></textarea>";
            } else {
                sot.swo.addResponsiveComment(itemnum);
            }
        },

		addResponsiveComment: function(itemnum){
			var img = $("#" + itemnum + "_comments_img")[0];
			img.setAttribute('src', baseDir + '/src/img/removecomments.png');
			img.setAttribute('alt', 'Remove Comment');
			img.onclick = function(){
				sot.swo.removeResponsiveComment(itemnum);
			};
			dojo.byId(itemnum + "_comments").innerHTML = "<textarea class='form-control' required placeholder='*Required*' name=" + itemnum + "_comments' id='" + itemnum + "_comments_textarea' onkeypress='return sot.swo.imposeMaxLength(this, 1000);'></textarea>";
		},
		
		removeComment: function(itemnum, img){
			img.setAttribute('src', baseDir + '/src/img/comments.png');
			img.setAttribute('alt', 'Add Comment');
			img.onclick = function(){
				sot.swo.addComment(itemnum, img);
			};
			dojo.byId(itemnum + "_comments").innerHTML = "";
		},

		removeResponsiveComment: function(itemnum){
			var img = $("#" + itemnum + "_comments_img")[0];
			img.setAttribute('src', baseDir + '/src/img/comments.png');
			img.setAttribute('alt', 'Add Comment');
			img.onclick = function(){
				sot.swo.addResponsiveComment(itemnum);
			};
			dojo.byId(itemnum + "_comments").innerHTML = "";
		},
		
		getSubItems: function(itemnum, crew, ohug) {
			sot.tools.ajax.submit("get", "json", {
				f: crew ? "subItemsCrew" : "subItems",
				org: dojo.byId("org").value,
				officefield: dojo.byId("office_field").value,
				ohug: ohug || '',
				itemnum: itemnum
			}, function(_e){
				var tbl = "<table style='margin-top: 0px;'>";
				var x = 0;
	
				for(var i in _e){
					tbl += "<tr>";
					tbl += "<td style='width:31px; text-align:center; vertical-align:top;'><input type='radio' " + (items[_e[i].SUBITEM_NUM] && items[_e[i].SUBITEM_NUM] == "S" ? "checked=checked" : "") + " class='S' name='" + _e[i].SUBITEM_NUM + "' value='S'  onclick='sot.swo.markItem(" + _e[i].SUBITEM_NUM + ", \"S\", false, false);' /></td>";
					tbl += "<td style='width:34px; text-align:center; vertical-align:top;'><input type='radio' " + (items[_e[i].SUBITEM_NUM] && items[_e[i].SUBITEM_NUM] == "IO" ? "checked=checked" : "") + "class='IO' name='" + _e[i].SUBITEM_NUM + "' value='IO'  onclick='sot.swo.markItem(" + _e[i].SUBITEM_NUM + ", \"IO\", false, false);' /></td>";
					tbl += "<td style='width:33px; text-align:center; vertical-align:top;'><input type='radio' " + (items[_e[i].SUBITEM_NUM] && items[_e[i].SUBITEM_NUM] == "NA" ? "checked=checked" : "") + "class='NA' name='" + _e[i].SUBITEM_NUM + "' value='NA'  onclick='sot.swo.markItem(" + _e[i].SUBITEM_NUM + ", \"NA\", false, false);' /></td>";
					tbl += "<td style='width:702px;'>" + _e[i].SUBITEM_TEXT + "</td>";
					tbl += "</tr>";
				}

				tbl += "</table>";
				
				dojo.byId(itemnum + "_subitems").innerHTML = tbl;
				//Check if still IO, enable/disabled mark all & submit btn
				return true;
			}, true, false);
			
		},
		
		toggleSiteItems: function(value) {
			if(value == 'onsite') {
				sot.swo.addItems(true, value, 'Onsite');
				sot.swo.addItems(false, 'offsite'); //hide offsite questions
			} else {
				sot.swo.addItems(true, value, 'Offsite');
				sot.swo.addItems(false, 'onsite'); //hide onsite questions
			}
		},
		
		addItems: function(checked, category, header){
			category = category.toLowerCase();
			var lfc = 0;
			if(dojo.byId("lfc")) {
				lfc = dojo.byId("lfc").value;
			}

			if(checked) {
				sot.tools.ajax.submit("get", "json", {
					f: "addItems",
					category: category,
					lfc: lfc || '0'
				}, function(_e){

					var tbl = "<table style='margin-top:0px;'>";
					var x = 0;

					tbl += "<tr style='font-weight:normal; background-color: #f0f0f0;'>";
					tbl += "<td style='width:30px; text-align:center; border-bottom: 1px solid #000; border-top: 1px solid #000; padding: 3px;'>S</td>";
					tbl += "<td style='width:30px; text-align:center; border-bottom: 1px solid #000; border-top: 1px solid #000; padding: 3px;'>IO</td>";
					tbl += "<td style='width:30px; text-align:center; border-bottom: 1px solid #000; border-top: 1px solid #000; padding: 3px;'>NA</td>";
					tbl += "<td style='width:710px; text-align:center; border-bottom: 1px solid #000; border-top: 1px solid #000; padding: 3px;'>" + (header || category.toUpperCase()) + "</td>";
					tbl += "<td style='text-align:center; border-bottom: 1px solid #000; border-top: 1px solid #000; padding: 3px;'></td>";
					tbl += "</tr>";

					for(var i in _e){
						var bg = (x++ % 2 == 0 ? "#f0f0f0" : "#cecece");

						tbl += "<tr style = 'background-color: " + bg + ";'>";

						tbl += "<td style='width:30px; text-align:center; vertical-align:top;'><input type='radio' " + (items[_e[i].ITEM_NUM] && items[_e[i].ITEM_NUM] == "S" ? "checked=checked" : "") + " class='S' name='" + _e[i].ITEM_NUM + "' value='S'  onclick='sot.swo.markItem(" + _e[i].ITEM_NUM + ", \"S\", false, true);' /></td>";
						tbl += "<td style='width:30px; text-align:center; vertical-align:top;'><input type='radio' " + (items[_e[i].ITEM_NUM] && items[_e[i].ITEM_NUM] == "IO" ? "checked=checked" : "") + " id='" + _e[i].ITEM_NUM + "' class='IO' name='" + _e[i].ITEM_NUM + "' id='" + _e[i].ITEM_NUM + "' value='IO'  onclick='sot.swo.markItem(" + _e[i].ITEM_NUM + ", \"IO\", false, true);' /></td>";
						tbl += "<td style='width:30px; text-align:center; vertical-align:top;'><input type='radio' " + (items[_e[i].ITEM_NUM] && items[_e[i].ITEM_NUM] == "NA" ? "checked=checked" : "") + " class='NA' name='" + _e[i].ITEM_NUM + "' value='NA'  onclick='sot.swo.markItem(" + _e[i].ITEM_NUM + ", \"NA\", false, true);' /></td>";

						tbl += "<td style='width:710px;'>" + _e[i].ITEM_TEXT + "</td>";
						tbl += "<td style='text-align:right;'><img src='../src/img/comments.png' style='cursor:pointer;' id='" + _e[i].ITEM_NUM + "_comments_img' alt='Add Comment' onclick='sot.swo.addComment(" + _e[i].ITEM_NUM  + ", this);'/></td>";
						tbl += "</tr>";

						tbl += "<tr style='padding:0px; margin:0px;'>";
						tbl += "<td style='width:800px;' colspan='5' id='"  + _e[i].ITEM_NUM + "_comments'></td>";
						tbl += "</td>";
						tbl += "</tr>";
					}

					tbl += "</table>";

					if(x == 0) tbl = "";

					dojo.byId(category).innerHTML = tbl;
					dojo.byId(category).style.display = "block";

					return true;
				}, true, true);
			} else {
				dojo.byId(category).innerHTML = "";
				dojo.byId(category).style.display = "none";
			}
		},

		addQewForLifeCritical: function(checked){
			category = "qew";
			header = "Qualified Electrical Worker";
			var lfc = 0;
			if(dojo.byId("lfc")) {
				lfc = dojo.byId("lfc").value;
			}

			if(checked) {
				sot.tools.ajax.submit("get", "json", {
					f: "addItems",
					category: category,
					lfc: lfc || '0'
				}, function(_e){

					var tbl = "<table style='margin-top:0px;'>";
					var x = 0;

					tbl += "<tr style='font-weight:normal; background-color: #f0f0f0;'>";
					tbl += "<td style='width:30px; text-align:center; border-bottom: 1px solid #000; border-top: 1px solid #000; padding: 3px;'>S</td>";
					tbl += "<td style='width:30px; text-align:center; border-bottom: 1px solid #000; border-top: 1px solid #000; padding: 3px;'>IO</td>";
					tbl += "<td style='width:30px; text-align:center; border-bottom: 1px solid #000; border-top: 1px solid #000; padding: 3px;'>NA</td>";
					tbl += "<td style='width:710px; text-align:center; border-bottom: 1px solid #000; border-top: 1px solid #000; padding: 3px;'>" + (header || category.toUpperCase()) + "</td>";
					tbl += "<td style='text-align:center; border-bottom: 1px solid #000; border-top: 1px solid #000; padding: 3px;'></td>";
					tbl += "</tr>";

					for(var i in _e){
						var bg = (x++ % 2 == 0 ? "#f0f0f0" : "#cecece");

						tbl += "<tr style = 'background-color: " + bg + ";'>";

						tbl += "<td style='width:30px; text-align:center; vertical-align:top;'><input type='radio' " + (items[_e[i].ITEM_NUM] && items[_e[i].ITEM_NUM] == "S" ? "checked=checked" : "") + " class='S' name='" + _e[i].ITEM_NUM + "' value='S'  onclick='sot.swo.qewMarkRemoveComment(" + _e[i].ITEM_NUM + ", \"S\");' /></td>";
						tbl += "<td style='width:30px; text-align:center; vertical-align:top;'><input type='radio' " + (items[_e[i].ITEM_NUM] && items[_e[i].ITEM_NUM] == "IO" ? "checked=checked" : "") + " id='" + _e[i].ITEM_NUM + "' class='IO' name='" + _e[i].ITEM_NUM + "' id='" + _e[i].ITEM_NUM + "' value='IO'  onclick='sot.swo.qewMarkAddComment(" + _e[i].ITEM_NUM + ", \"IO\");' /></td>";
						tbl += "<td style='width:30px; text-align:center; vertical-align:top;'><input type='radio' " + (items[_e[i].ITEM_NUM] && items[_e[i].ITEM_NUM] == "NA" ? "checked=checked" : "") + " class='NA' name='" + _e[i].ITEM_NUM + "' value='NA'  onclick='sot.swo.qewMarkRemoveComment(" + _e[i].ITEM_NUM + ", \"NA\");' /></td>";

						tbl += "<td style='width:710px;'>" + _e[i].ITEM_TEXT + "</td>";
						tbl += "<td style='text-align:right;'><img src='../src/img/comments.png' style='cursor:pointer;' id='" + _e[i].ITEM_NUM + "_comments_img' alt='Add Comment' onclick='sot.swo.addResponsiveComment(" + _e[i].ITEM_NUM  + ");'/></td>";
						tbl += "</tr>";

						tbl += "<tr style='padding:0px; margin:0px;'>";
						tbl += "<td style='width:800px;' colspan='5' id='"  + _e[i].ITEM_NUM + "_comments'></td>";
						tbl += "</td>";
						tbl += "</tr>";
					}

					tbl += "</table>";

					if(x == 0) tbl = "";

					dojo.byId(category).innerHTML = tbl;
					dojo.byId(category).style.display = "block";

					return true;
				}, true, true);
			} else {
				dojo.byId(category).innerHTML = "";
				dojo.byId(category).style.display = "none";
			}
		},
		
		imposeMaxLength: function(Object, MaxLen){
			return (Object.value.length <= MaxLen);
		},
		
		markAllSat: function(){
			var radios = document.getElementsByTagName('input');  
			var x = 0; 
			
			for( m = 0; m < radios.length; m++ ) {
				if (radios[m].type == 'radio'){
					var group = document.getElementsByName(radios[m].name);
					var lc = document.getElementsByName(radios[m].name)[0].getAttribute('lc');
					if(lc) {
						continue;
					}
					var marked = false;
					if (group != null){
						for (n = 0; n < group.length; n++){
							if (group[n].checked == true)
								marked = true;
						}
					}
					if (!marked){
						if (group != null){
							for (n = 0; n < group.length; n++){
								if (group[n].className == 'S')
									group[n].checked = true;
							}
						}
					}					
				}
			}
		},
		onSubmit: function(){
			var usid = dojo.byId("usid_1").value.replace(/\s/g, '');
			var usid1 = usid;
			
			var name = dojo.byId("name_1").value;
			var name1 = name;
			
			var nameValidate = dojo.byId("nameValidate_1").value;
			var observedby = dojo.byId("observed_by").value;
			var date = dojo.byId("observed_date").value;
			var org = dojo.byId("org").value;

			var lcPilot = dojo.byId("lc_pilot") ? 1 : 0;
			
			if((nameValidate.replace(/\s/g, '') == "" && usid != "") && (!lcPilot)) {
				alert('Invalid Employee Username entered for who was observed. \n\nIf you would like to submit an anonymous observation for who was observed, leave the Employee Username field blank.\n\nIf you want to observe an individual who does not have a username, then leave the Employee Username field blank and enter their name in the Employee Name field.');
				dojo.byId('usid_1').focus();
				return false;
			}

			if(date == ""){
				alert('Enter date when this observation was performed.');
				dojo.byId("observed_date").focus();
				return false;
			}
			
			if(org == ""){
				alert('Select an organization.');
				dojo.byId("org").focus();
				return false;
			}

			// Validates the YES || NO Life critical questions..
			var exit = false;
			if (lcPilot){

				//if (false) { // remove this check
					/////////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
					// iOS bootstrap validation issue \\
					var isIphone = /(iPhone)/i.test(navigator.userAgent);
					var isSafari = !!navigator.userAgent.match(/Version\/[\d\.]+.*Safari/);

					if (isIphone && isSafari) {
						// check procedure box?
						// todo if this is the case...
                        var proc = $("#2141_comments_textarea").is(":visible");
                        var pLength = $("#2141_comments_textarea").val().length;
                        if (proc & (pLength == 0)){
                            alert("Please list the name of the procedure in the comments box for the first Core Question.")
                        }
						// Check question 1
						var gbVal = $("#goodBehaviorSelect").val();
						if (gbVal == ""){
							alert("Please answer: Was good safety behavior recognized during this observation?");
							return false; // Todo: May have to make higer level
						}
						// Check question 2
						var cVal = $("#conversation").val();
						if (cVal == ""){
							alert("Please answer: Did a conversation take place with the observed individual?");
							return false;
						} else if ((cVal == "1") && ($("#comments").val().length == 0)) {
							alert("Please fill out detailed comments regarding the conversation.");
							return false;
						}
					}
					/////////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
				//}

				function validateInputs (a, b){
					if (!a.checked && !b.checked){
						return true;
					} else {
						return false;
					}
				}
				function validateYesNo(strTableName){
					var inputs = $(strTableName).prev().find("input");
					var a = inputs[0];
					var b = inputs[1];
					var result = validateInputs(a,b);
					return result;
				}
                function reverseString(str) {
                    return str.split("").reverse().join("");
                }
				function extractName(str){
                    return (reverseString((reverseString(str.substring(4))).substring(13))).replace(/_/, ' ');
                }

				var lcQuestions =
                    ["#LC_CONFINED_io_questions"
                    ,"#LC_HOT_WORK_io_questions"
                    ,"#LC_HAZ_ENERGY_io_questions"
                    ,"#LC_HEIGHTS_io_questions"
                    ,"#LC_TRENCHING_io_questions"
                    ,"#LC_LIFTING_io_questions"
                    ,"#LC_VEHICLE_io_questions"];

                var failed = false;
                var message = "";
                for (var i = 0; i < lcQuestions.length; i++){
                    if (validateYesNo(lcQuestions[i])){
                        message = lcQuestions[i];
                        failed = true;
                        break;
                    }
                }
                if (failed) {
                    alert("You need to answer all of the main Life Critical questions.");
                    exit = true;
                }
			}
			if (exit){
				return false;
			}

			var complete = true;
			var radios = document.getElementsByTagName('input'); 

			var hasComments = false;
			var incompleteRadios = [];
			for( m = 0; m < radios.length; m++ ) {
				if (radios[m].type == 'radio'){
					var groupName = radios[m].name;
					var group = document.getElementsByName(groupName);
					marked = false;

					if (group != null){
						for (n = 0; n < group.length; n++){
							if (group[n].checked == true){
								marked = true;
								if (lcPilot) {
									var parentTable = $(group[n]).parents("table:first")[0];
									$( parentTable ).css("outline","");
								}
							} 
						}
					}


					var skip = false;
					if (marked == false){
						if(lcPilot) {
							for (n = 0; n < group.length; n++) {
								var parentTable = $(group[n]).parents("table:first")[0];
								var display = parentTable.style.display;

								if (display == "none") {
									continue;
									skip = true;
								}
								if (parentTable.id.substring(0, 2) == 'LC') {
									$( parentTable ).css("outline","thick solid red");
									alert("You must complete the Life Critical sub-question section for which you've selected YES.");
									return false;
								} else {
									// Gets the ID of the parent question "main level", if it is NOT Life critical
									var test = $(group[n]).parents("table:first").parents("td:first");
									if (test.length != 0) {
										var iden = test.attr('id').match(/.+?(?=_)/)[0];
										// Checks to see if IO is checked for the parent..
										if($("#" + iden)[0].checked){
											alert("You selected IO for a question. Please complete all related sub-questions.");
											return false;
										}
									}
								}
							}
						}

						if(groupName.substring(0, 2) == 'LC') {
							if(radios[m].disabled) continue;  
							radios[m+1].checked = true;
						} else {
							if (!skip){
								complete = false;
								incompleteRadios.push(group);
							}
						}
					}
				}					
			}


			var newInc = $("input:radio:visible");
			var incompleteRadiosVisible = [];
			for (var i = 0; i < newInc.length; i++){
				var item = newInc[i];
				var group =	$( "[name='" + item.name + "']" );
				var checked = false;
				for (var i2 = 0; i2 < group.length; i2++){
					var groupItem = group[i2];
					if (groupItem.checked){
						checked = true;
						break;
					}
				}
				if (!checked) {
					incompleteRadiosVisible.push(group);
					complete = false;
				}
			}
			if (incompleteRadiosVisible.length == 0){
				complete = true;
			}

			if(complete == false){
				if(confirm("One or more items have not been answered. Do you wish you continue?\nAll incomplete items will be saved as Not Applicable.")){
					var x = 0;
					for(var i in incompleteRadiosVisible){
						var marked = false;
						if (incompleteRadiosVisible[i] != null){
							for (var n = 0; n < incompleteRadiosVisible[i].length; n++){
								if (incompleteRadiosVisible[i][n].className == "NA"){
									incompleteRadiosVisible[i][n].checked = true;
								} 
							}
						}
					}
				} else {
				 	return false;
				}
			}

			for(var i in incompleteRadios){
				var marked = false;
				if (incompleteRadios[i] != null){
					for (var n = 0; n < incompleteRadios[i].length; n++){
						if (incompleteRadios[i][n].className == "NA"){
							incompleteRadios[i][n].checked = true;
						}
					}
				}
			}

			//not sure about this
			for(var i in employees){
				var nameValidate = dojo.byId("nameValidate_" + i).value.replace(/\s/g, '');
				var name = dojo.byId("name_" + i).value.replace(/\s/g, '');
				var usid = dojo.byId("usid_" + i).value.replace(/\s/g, '');
				
				if(usid == "" && name == ""){
					alert("You have added an additional employee to this observation but did not enter an employee username or name."); //contractors dont have id
					dojo.byId("usid_" + i).focus();
					return false;
				} else if((nameValidate.replace(/\s/g, '') == "" && usid != "")){
					alert('Invalid Employee Username entered for who was observed for an additional employee added to this observation.\n\nIf you want to observe an individual who does not have a username, then leave the Employee Username field blank and enter their name in the Employee Name field.');
					dojo.byId("usid_" + i).focus();
					return false;
				} 
			}

			if(name1 == "" && usid1 == ""){
				if(confirm("Are you sure you want to submit this observation without an employee username or name for who was observed?")){
					return true;
				} else {
					return false;
				}
			}
			
			sot.tools.cover(true);
			return true;
		},
		addEmployee: function(){
			employeeCt++;
			var div = document.createElement('div');
			div.setAttribute('id', 'emp_' + employeeCt);
			div.innerHTML = "<b>Employee Username: </b> <input type='text' autocomplete='off' name='usid[]' id='usid_" + employeeCt + "' maxlength='6' onkeyup='sot.swo.inputUsid(this, " + employeeCt + ");' onchange='sot.swo.inputUsid(this, " + employeeCt + ");'/>&nbsp;&nbsp;&nbsp;";
			div.innerHTML += "<b>Employee Name: </b> <input type='text' name='name[]' id='name_" + employeeCt + "'/><input type='hidden'  id='nameValidate_" + employeeCt + "'/>&nbsp;";
			div.innerHTML += "<span style='font-size: 10px;'>[ <img style='width: 10px; height: 10px; vertical-align: bottom;' src=\"https://soteria.dteco.com/src/img/x.png\" > <a href='#' onclick='sot.swo.removeEmployee(" + employeeCt + "); return false;'>Remove</a> ]</span>";
			
			dojo.byId("add_emp").appendChild(div);
			
			setTimeout(function(){
				try{
					dojo.byId("usid_" + employeeCt).focus();
				} catch(ex){}
			}, 300);
			
			employees[employeeCt] = true;


		},
		removeEmployee: function(num){
			try {
				dojo.byId("add_emp").removeChild(dojo.byId("emp_" + num));
			} catch(ex) {
			}
			
			if(employees[num]){
				delete employees[num];
			}
		},
		view: function(epopId){
			window.open(baseDir + '/forms/viewEpop.php?epopid=' + epopId, '_blank', 'width=1000, toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes');
		},
		viewCrew: function(epopId){
			window.open(baseDir + '/forms/viewCrewEpop.php?epopid=' + epopId, '_blank', 'width=1000, toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes');
		},
		
		onSubmitCrew: function(){
			var radios = document.getElementsByTagName('input'); 
			var complete = true;
			
			var incompleteRadios = [];

			// Validates the YES || NO Life critical questions..
			var exit = false;
			if (true){
				function validateInputs (a, b){
					if (!a.checked && !b.checked){
						return true;
					} else {
						return false;
					}
				}
				function validateYesNo(strTableName){
					var inputs = $(strTableName).prev().find("input");
					var a = inputs[0];
					var b = inputs[1];
					var result = validateInputs(a,b);
					return result;
				}
				function reverseString(str) {
					return str.split("").reverse().join("");
				}
				function extractName(str){
					return (reverseString((reverseString(str.substring(4))).substring(13))).replace(/_/, ' ');
				}

				var lcQuestions =
					["#LC_CONFINED_io_questions"
						,"#LC_HOT_WORK_io_questions"
						,"#LC_HAZ_ENERGY_io_questions"
						,"#LC_HEIGHTS_io_questions"
						,"#LC_TRENCHING_io_questions"
						,"#LC_LIFTING_io_questions"
						,"#LC_VEHICLE_io_questions"];

				var failed = false;
				var message = "";
				for (var i = 0; i < lcQuestions.length; i++){
					if (validateYesNo(lcQuestions[i])){
						message = lcQuestions[i];
						failed = true;
						break;
					}
				}
				if (failed) {
					alert("You need to answer all of the main Life Critical questions.");
					exit = true;
				}
			}
			if (exit){
				return false;
			}

            var complete = true;
            var radios = document.getElementsByTagName('input');

            var hasComments = false;
            var incompleteRadios = [];
            for( m = 0; m < radios.length; m++ ) {
                if (radios[m].type == 'radio'){
                    var groupName = radios[m].name;
                    var group = document.getElementsByName(groupName);
                    marked = false;

                    if (group != null){
                        for (n = 0; n < group.length; n++){
                            if (group[n].checked == true){
                                marked = true;
                                if (true) {
                                    var parentTable = $(group[n]).parents("table:first")[0];
                                    $( parentTable ).css("outline","");
                                }
                            }
                        }
                    }

                    if (marked == false){
                        if(true) {
                            for (n = 0; n < group.length; n++) {
                                var parentTable = $(group[n]).parents("table:first")[0];
                                var display = parentTable.style.display;
                                if (display == "none") {
                                    continue;
                                }
                                if (parentTable.id.substring(0, 2) == 'LC') {
                                    $( parentTable ).css("outline","thick solid red");
                                    alert("You must complete the Life Critical sub-question section for which you've selected YES.");
                                    return false;
								} else {
									// Gets the ID of the parent question "main level", if it is NOT Life critical
									var test = $(group[n]).parents("table:first").parents("td:first");
									if (test.length != 0) {
										var iden = test.attr('id').match(/.+?(?=_)/)[0];
										// Checks to see if IO is checked for the parent..
										if($("#" + iden)[0].checked){
											alert("You selected IO for a question. Please complete all related sub-questions.");
											return false;
										}
									}
								}
                            }
                        }

                        if(groupName.substring(0, 2) == 'LC') {
                            if(radios[m].disabled) continue;
                            radios[m+1].checked = true;
                        } else {
                            complete = false;
                            incompleteRadios.push(group);
                        }
                    }
                }
            }

			var newInc = $("input:radio:visible");
			var incompleteRadiosVisible = [];
			for (var i = 0; i < newInc.length; i++){
				var item = newInc[i];
				var group =	$( "[name='" + item.name + "']" );
				var checked = false;
				for (var i2 = 0; i2 < group.length; i2++){
					var groupItem = group[i2];
					if (groupItem.checked){
						checked = true;
						break;
					}
				}
				if (!checked) {
					incompleteRadiosVisible.push(group);
					complete = false;
				}
			}
			if (incompleteRadiosVisible.length == 0){
				complete = true;
			}

			if(complete == false){
				if(confirm("One or more items have not been answered. Do you wish you continue?\nAll incomplete items will be saved as Not Applicable.")){
					var x = 0;
					for(var i in incompleteRadiosVisible){
						var marked = false;
						if (incompleteRadiosVisible[i] != null){
							for (var n = 0; n < incompleteRadiosVisible[i].length; n++){
								if (incompleteRadiosVisible[i][n].className == "NA"){
									incompleteRadiosVisible[i][n].checked = true;
								}
							}
						}
					}
				} else {
					return false;
				}
			}

			for(var i in incompleteRadios){
				var marked = false;
				if (incompleteRadios[i] != null){
					for (var n = 0; n < incompleteRadios[i].length; n++){
						if (incompleteRadios[i][n].className == "NA"){
							incompleteRadios[i][n].checked = true;
						}
					}
				}
			}
			
			sot.tools.cover(true);
			return true;
		},
		_delete: function(epopid) {
			if(confirm("Are you sure you want to delete this Enterprise Performance Observation (Observation# " + epopid + ")?\nThis action cannot be undone.")) {
				sot.tools.ajax.submit("get", "json", {
					f: "deleteEpop",
					epopid: epopid
				}, function(_e){
					window.location.reload();
					
					return true;
				}, true, true);
			}
		}
		
	};
}();