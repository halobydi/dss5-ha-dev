dojo.provide("sot.feedback");
var baseDir = window.location.protocol + "//" + window.location.host;

sot.feedback = function() {
	return {
		sendFeedback: function() {
			var exclusion = dojo.byId("from") && dojo.byId("to");
			
			var remarks = dojo.byId("remarks") || {};
			var from = dojo.byId("from") || {};
			var to = dojo.byId("to") || {};
			var feedback = dojo.byId("feedback") || {};
			var usid = dojo.byId("usid") || {};
			var self = dojo.byId("self") && dojo.byId("self").checked ? true : false;
			var other = dojo.byId("other") && dojo.byId("other").checked ? true : false;
				
			if(exclusion) {
				if(from.value == "") {
					alert("You must enter a start date for this exclusion.");
					from.focus();
					return;
				}
				if(to.value == "") {
					alert("You must enter a end date for this exclusion.");
					to.focus();
					return;
				}
				
				if(other) {
					if(usid.value.replace(/\s+/g, ' ') == "" || usid.value.replace(/\s+/g, ' ').length != 6 || usid.value == "Enter their username (ie: u12345)") {
						alert("Invalid or empty username provided for Employee To Be Excluded.");
						usid.focus();
						setTimeout(function() { usid.value = "" }, 100);
						return;
					}
				}
				
				if(remarks.value == "" || remarks.value == "Please provide a short description for this exclusion. Note all exclusions are sent to SOTERIA@dteenergy.com and to your Business Unit Representatives for review.") {
					alert("You must provide some remarks for this exclusion.");
					remarks.focus();
					setTimeout(function() { remarks.value = "" }, 100);
					return;
				}
				
				if(!confirm("Submitting the following exclusion request to SOTERIA@dteenergy.com\n\nFrom: " + from.value + "\nTo: " + to.value + "\nRemarks: " + remarks.value + "\n\nClick OK to send or Cancel")){
					return;
				}
			} else {
				if(feedback.value == "") {
					alert("You must provide some feedback to send.");
					feedback.focus();
					return;
				}
				
				if(!confirm("Sending the following feedback: " + feedback.value + " to SOTERIA@dteenergy.com.\n\nClick OK to send or Cancel")){
					return;
				}
			}
			sot.tools.ajax.submit("get", "json", {
				f: "sendFeedback",
				feedback: feedback ? feedback.value : "",
				from: from ? from.value : "",
				to: to ? to.value : "",
				remarks: remarks ? remarks.value : "",
				exclusion: exclusion ? "true" : "false",
				usid: usid ? usid.value : "",
				self: self,
				other: other
				
			}, function(_e){
				if(_e.error) {
					alert(_e.error);
				} else {
					alert("Your " + (exclusion ? "exclusion request" : "feedback") + " has been successfully submitted.");
					window.location.reload();
				} 
				
				return true;
			}, true, true);
		},
		checkSelfOther: function(checked, id) {
			var uncheck = id == "self" ? "other" : "self";
			if(checked) {
				dojo.byId(uncheck).checked = false;
			}
		}
	};
	
}();