dojo.provide('sot.tools.menu');

sot.tools.menu = function(){
	var bgColor = '#292829';
	
	var hoverBgColor = '#c6dfff';
	var hoverBorder = '#319aff';
	var disabledBgHoverColor = '';
	var disabledHoverBorder = '';
	var color = '#d6d3d6';
	var disabledColor = 'gray';
	var contextStartedWithAClick = false;
	var activeMenu = false;
	var activeDelegate  = false;
	var privileges = [];

	var menuItems = {
		delegate:[],
		obs: [
			{
				title: 'Paired Performance',
				onclick: function(){
					window.location = baseDir + '/forms/epop.php?pp' + (activeDelegate ? "&delegate=" + activeDelegate : "");
				}
			},
			{
				title: 'Qualified Electrical Worker',
				onclick: function(){
					window.location = baseDir + '/forms/epop.php?qew' + (activeDelegate ? "&delegate=" + activeDelegate : "");
				}
			},
			{ 
				title: 'Red Tag Audit',
				onclick: function(){
		
					window.location = baseDir + '/forms/redtag.php' + (activeDelegate ? "?delegate=" + activeDelegate : "");
				}
			},
			{
				title: 'Safe Worker - Field',
				onclick: function(){
					window.location = baseDir + '/forms/lifeCritical.php' + (activeDelegate ? "?delegate=" + activeDelegate : "");
				}
			},
			{
				title: 'Safe Worker - Office',
				onclick: function(){
					window.location = baseDir + '/forms/epop.php?type=office' + (activeDelegate ? "&delegate=" + activeDelegate : "");
				}
			},
			{
				title: 'Storm Duty',
				onclick: function(){
					window.location = baseDir + '/forms/stormduty.php' + (activeDelegate ? "?delegate=" + activeDelegate : "");
				}
			},
			{
				title: 'Near Miss',
				onclick: function(){
					window.location = baseDir + '/forms/nearmiss.php' + (activeDelegate ? "?delegate=" + activeDelegate : "");
				}
			},
			{
				title: 'Good Catch',
				onclick: function(){
					window.location = baseDir + '/forms/nearmiss.php?gc=true' + (activeDelegate ? "&delegate=" + activeDelegate : "");
				}
			},
			{
				title: 'Anatomy of an Event',
				onclick: function(){
					window.location = baseDir + '/forms/hp.php' + (activeDelegate ? "?delegate=" + activeDelegate : "");
				}
			}
		],
		admin: privileges,
		individualreports: [
			{
				title: 'My Activity',
				onclick: function(){
					window.location = baseDir + '/reports/individual/activity.php' + (activeDelegate ? "?delegate=" + activeDelegate : "");
				}
			},
			{
				title: 'Excel Export',
				subMenu: [
					{
						title: "Current Week",
						onclick: function(){
							window.open(baseDir + '/reports/individual/excel.php?method=week' + (activeDelegate ? "&delegate=" + activeDelegate : ""), "", "width=50, height=50");
						}
					},
					{
						title: "Current Month",
						onclick: function(){
							window.open(baseDir + '/reports/individual/excel.php?method=month' + (activeDelegate ? "&delegate=" + activeDelegate : ""), "", "width=50, height=50");
						}
					},
					{
						title: "Current Year",
						onclick: function(){
							window.open(baseDir + '/reports/individual/excel.php?method=year' + (activeDelegate ? "&delegate=" + activeDelegate : ""), "", "width=50, height=50");
						}
					},
					{},
					{
						title: "More Excel Options",
						onclick: function(){
							sot.home.exportExcel();
						}
					}
				]


			},

            {
                title: 'Improvement Opportunities',
                onclick: function(){
                    window.location = baseDir + '/reports/individual/io.php' + (activeDelegate ? "?delegate=" + activeDelegate : "");
                }
            },

            {
                title: 'Observation Compliance',
                onclick: function(){
                    window.location = baseDir + '/reports/individual/ObsCompliance.php' + (activeDelegate ? "?delegate=" + activeDelegate : "");
                }
            },

            {
				title: 'Observation Quality',
				onclick: function(){
					window.location = baseDir + '/reports/individual/ObsQuality.php' + (activeDelegate ? "?delegate=" + activeDelegate : "");
				}
			},
			{
				title: 'Leader Completions',
				subMenu: [
					{
						title: "Four Week Overview + Excel",
						onclick: function(){
							window.location = baseDir + '/reports/individual/lc.php' + (activeDelegate ? "?delegate=" + activeDelegate : "");
						}
					},
					{
						title: "Year to Date",
						onclick: function(){
							window.location = baseDir + '/reports/individual/lcytd.php' + (activeDelegate ? "?delegate=" + activeDelegate : "");
						}
					}
				]
			}, 
			{
				title: 'Leaders Without Observation Completed',
				onclick: function(){
					window.location = baseDir + '/reports/individual/leaderswoobs.php' + (activeDelegate ? "?delegate=" + activeDelegate : "");
				}
			},  
			{
				title: 'Employees Without Observation Performed (Quarter)',
				onclick: function(){
					window.location = baseDir + '/reports/individual/quarter.php' + (activeDelegate ? "?delegate=" + activeDelegate : "");
				}
			}, 
			{
				title: 'Search Employee Observation',
				img: 'search.png',
				onclick: function(){
					window.location = baseDir + '/reports/individual/searchobs.php' + (activeDelegate ? "?delegate=" + activeDelegate : "");
				}
			},
			{
				title: 'SWO Comments',
				img: "comments.png",
				onclick: function(){
					window.location = baseDir + '/reports/individual/comments.php' + (activeDelegate ? "?delegate=" + activeDelegate : "");
				}
			}
			
		],
		orgreports: [
			{
				title: 'DO SO Crew Report',
				onclick: function(){
					window.location = baseDir + '/reports/organization/dosocrew.php' + (activeDelegate ? "?delegate=" + activeDelegate : "");
				}
				
			},
			{
				title: 'Good Catch Trends & Export',
				onclick: function(){
					window.location = baseDir + '/reports/organization/gc.php' + (activeDelegate ? "?delegate=" + activeDelegate : "");
				}
			}, 
			{
				title: 'IO Trending',
				onclick: function(){
					window.location = baseDir + '/reports/organization/iotrending.php' + (activeDelegate ? "?delegate=" + activeDelegate : "");
				}
			}, 
			{
				title: 'Near Miss Details',
				onclick: function(){
					window.location = baseDir + '/reports/organization/nearmiss.php' + (activeDelegate ? "?delegate=" + activeDelegate : "");
				}
			},
			{
				title: 'Near Miss Trends',
				onclick: function(){
					window.location = baseDir + '/reports/organization/gc.php?form=NEAR_MISS' + (activeDelegate ? "&delegate=" + activeDelegate : "");
				}
			},
			{
				title: 'Red Tag Audits',
				onclick: function(){
					window.location = baseDir + '/reports/organization/redtagaudits.php' + (activeDelegate ? "?delegate=" + activeDelegate : "");
				}
			},
			{
				title: 'Red Tag Audits Dashboard',
				onclick: function(){
					window.location = baseDir + '/reports/organization/redtagauditsdashboard.php' + (activeDelegate ? "?delegate=" + activeDelegate : "");
				}
			},
			{	
				title: 'Storm Duty Submissions',
				onclick: function(){
					window.location = baseDir + '/reports/organization/stormduty.php' + (activeDelegate ? "?delegate=" + activeDelegate : "");
				}
			},
			{	
				title: 'Twin Analysis Precursors',
				onclick: function(){
					window.location = baseDir + '/reports/organization/twin.php' + (activeDelegate ? "?delegate=" + activeDelegate : "");
				}
			},
			{ 
				title: 'Weekly Report-out',
				onclick: function(){
					window.location = baseDir + '/reports/organization/reportout.php' + (activeDelegate ? "?delegate=" + activeDelegate : "");
				}
				
			}
		],
		print: [
			{ 
				title: 'Current Window',
				onclick: function(){
					setTimeout(function() {
						window.print();
					}, 100);
				}
				
			},
			{},
			{	
				title: 'Paired Performance',
				onclick: function(){
					var w = window.open(baseDir + '/forms/epop.php?print=true&pp=1', 'printform', 'width=50,height=50,menubar=0');
				}
			},
			{	
				title: 'Qualified Electrical Worker',
				onclick: function(){
					var w = window.open(baseDir + '/forms/epop.php?print=true&qew=1', 'printform', 'width=50,height=50,menubar=0');
				}
			},
			{	
				title: 'Red Tag Audit',
				onclick: function(){
					var w = window.open(baseDir + '/forms/redtag.php?print=true', 'printform', 'width=50,height=50,menubar=0');
				}
			},
			{	
				title: 'Safe Worker',
				onclick: function(){
					var w = window.open(baseDir + '/forms/epop.php?print=true', 'printform', 'width=50,height=50,menubar=0');
				}
			},
			{
				title: 'Safe Worker - Life Critical',
				onclick: function(){
					var w = window.open(baseDir + '/forms/epopLFC.php?print=true', 'printform', 'width=50,height=50,menubar=0');
				}
			},
			{	
				title: 'Storm Duty Observation',
				onclick: function(){
					var w = window.open(baseDir + '/forms/stormduty.php?print=true', 'printform', 'width=50,height=50,menubar=0');
				}
			}
		],
		settings: [
			{ 
				title: 'Delegates',
				img: 'delegates.png',
				onclick: function(){
					window.location = baseDir + '/settings/delegates.php' + (activeDelegate ? "?delegate=" + activeDelegate : "");
				}
			},
			{
				title: 'Exclusions',
				onclick: function(){
					window.location = baseDir + '/help/feedback.php?exclusion=true' + (activeDelegate ? "&delegate=" + activeDelegate : "");
				}
			},
			{	
				title: 'Notifications',
				onclick: function(){
					window.location = baseDir + '/settings/notifications.php' + (activeDelegate ? "?delegate=" + activeDelegate : "");
				}
			}
		],
		help: [
			{
				title: 'Frequently Asked Questions',
				onclick: function(){
					window.location = baseDir + '/help/faq.php' + (activeDelegate ? "?delegate=" + activeDelegate : "");
				}
			},
			{
				title: 'Near Miss SWI',
				onclick: function(){
					window.open(baseDir + '/help/SWI/Near Miss Using Soteria.pdf');
				}
			},
			{
				title: 'Observation Descriptions',
				onclick: function(){
					window.open(baseDir + '/help/Observation Descriptions.pdf');
				}
			},
			{
				title: 'Red Tag Attachments (Mobile) SWI',
				onclick: function(){
					window.open(baseDir + '/help/SWI/red_tag_audit_attachments.pdf');
				}
			},
			{
				title: 'Send Feedback',
				onclick: function(){
					window.location = baseDir + '/help/feedback.php' + (activeDelegate ? "?delegate=" + activeDelegate : "");
				}
			},
			{
				title: 'SOTeria SWI (Desktop)',
				onclick: function(){
					window.open(baseDir + '/help/SWI/SOTeria SWI - Desktop.pdf');
				}
			},
			{
				title: 'SOTeria SWI (Mobile)',
				onclick: function(){
					window.open(baseDir + '/help/SWI/SOTeria User Guide - Mobile.pdf');
				}
			},
			{
				title: 'SOTeria Wiki',
				onclick: function(){
					window.open("https://questlink.dteco.com/wikis/home?lang=en_US#/wiki/SOTeria");
				}
			},
			{
				title: 'Submit an Exclusion',
				onclick: function(){
					window.location = baseDir + '/help/feedback.php?exclusion=true' + (activeDelegate ? "&delegate=" + activeDelegate : "");
				}
			},
			{},
			{
				title: 'About SOTeria',
				onclick: function(){
					window.location = baseDir + '/help/about.php' + (activeDelegate ? "?delegate=" + activeDelegate : "");
				}
			}
		]
		
	};

	var subMenu = function(menuItem){


	};
	
	var setUpOnOnclickOnMouse = function(e, subMenu, onclick, disabled){
		if(disabled){
			e.onmouseover = null;
			e.onmouseout = null;
			e.onclick = null;
		} else {
			e.onmouseover = function(){
				e.style.border = '1px solid ' + hoverBorder;
				e.style.backgroundColor = hoverBgColor;
				e.style.color = '#000';
				
				if(subMenu){
					subMenu.style.display = 'block';
				}
			};
			e.onmouseout = function(){
				e.style.border = '1px solid transparent';
				e.style.backgroundColor = bgColor;
				e.style.color = color;
				
				if(subMenu){
					subMenu.style.display = 'none';
				}
			};
			e.onclick = function(){
				if(onclick){ 
					onclick(); 
				}
				
				if(subMenu){
					subMenu.style.display = 'none';
				}
				
				if(e.parentNode.id.indexOf("subMenu") != -1){
					e.parentNode.style.display = 'none';
				}
				
				hideMenu();
			};
		}
		
	};

	var hideMenu = function(menuItem){
		try {
			dojo.byId(menuItem + "_menu").style.display = 'none';
		} catch (ex) {
			if(activeMenu){
				dojo.byId(activeMenu + "_menu").style.display = 'none';
			}
		}
		
		for(var i in subMenus){
			subMenus[i].style.display = 'none';
			//alert(subMenus[i].id);
		}
		
		activeMenu = false;
	};
	
	return {
		showMenu: function(menuItem){
		
			if(activeMenu && activeMenu != menuItem){
				hideMenu(activeMenu);
			}
			dojo.byId(menuItem).onclick = function(){
				sot.tools.menu.showMenu(menuItem);
				return false;
			};
			
			var _e = window.event;
			if(_e.type == 'click'){
				contextStartedWithAClick = true;
			}
			
			if(_e.type == 'mouseover' && !contextStartedWithAClick){
				return;
			}
			
			if(!menuItems[menuItem]){
				return false;
			}
			
			if(menuItems[menuItem].length == 0){
				return false;
			}
			
			if(dojo.byId(menuItem + "_menu")){ //if already build
				for(var i in subMenus){
					
				}
				dojo.byId(menuItem + "_menu").style.display = 'block';
				
			} else { //build
				var pos = position(dojo.byId(menuItem));
				var menu = document.createElement('div');
				menu.setAttribute('id', menuItem + "_menu");
				menu.style.position = 'absolute';
				menu.style.top = pos.top + 20 + 'px';
				menu.style.left = pos.left + 'px';
				menu.style.borderLeft = '1px solid #3d3d3d';
				menu.style.borderBottom = '1px solid #3d3d3d';
				menu.style.borderRight = '1px solid #3d3d3d';
				//menu.style.borderBottom = '2px inset #000';
				//menu.style.borderRight = '2px inset #000';
				menu.style.backgroundColor = bgColor;
				menu.style.padding = '1px';
				menu.style.zIndex = '9999';
				
				dojo.connect(document, 'onclick', function(){
					var e = window.event;
					if(activeMenu && dojo.byId(activeMenu) != e.srcElement){
						hideMenu();
						contextStartedWithAClick = false;
					}
					
				});
				
				menu.onmouseover = function(){
					menu.style.display = 'block';
				};
				
				var maxLength = 0;
				var menuItemsCt = 0;
				
				var divider = document.createElement('div');
				divider.style.borderTop = '1px solid #000'
				divider.style.borderBottom = '1px solid #3d3d3d';
				divider.style.marginBottom = '1px';
				
				subMenus = [];	
				for(var i in menuItems[menuItem]){
					var keys = 0;
					for(var k in menuItems[menuItem][i]){
						++keys;
					}
					
					if(keys == 0) {
						menu.appendChild(divider);
						continue;
					}
					var _menuItem = document.createElement('div');
					_menuItem.style.height = '18px';
					_menuItem.style.fontSize = '11px';
					_menuItem.style.paddingRight = '5px';
					_menuItem.style.fontFamily = 'Segoe ui';
					_menuItem.style.cursor = 'pointer';
					_menuItem.style.border = '1px solid transparent';
					_menuItem.style.color = color;
					
					var img = document.createElement('img');
					img.style.paddingRight = '15px';
					img.style.width = '15px';
					
					var text = document.createElement('span');
					text.style.top = '2px';
					text.style.position  = 'relative';
					
					if(menuItems[menuItem][i].img){
						img.setAttribute('src', baseDir + "/src/img/" + menuItems[menuItem][i].img);
						img.style.paddingTop = '1px';
						_menuItem.appendChild(img);
						
					} else if(menuItem == 'delegate' && menuItems[menuItem][i].active == 1){
						var _active = document.createElement('button');
						_active.style.width = '20px';
						_active.style.height = '18px';
						_active.style.textAlign = 'center';
						
						_active.style.border = '1px solid #6bb6ff';
						_active.style.backgroundColor = '#e7f3ff';
						_active.style.marginRight = '10px';
						
						var check = document.createElement('img');
						check.style.position = 'relative';
						check.style.top = '-2px';
						check.setAttribute('src', baseDir + '/src/img/check.gif');
						_active.appendChild(check);
					
						text.style.top = '-2px';
						
						_menuItem.appendChild(_active);
						activeDelegate = menuItems[menuItem][i].d_id;

					} else {
						text.style.left = '30px';
					}
					
					var _hasSubMenu = (menuItems[menuItem][i].subMenu && menuItems[menuItem][i].subMenu.length > 0 ? true : false);
					text.innerHTML = menuItems[menuItem][i].title;
					
					_menuItem.appendChild(text);
		
					if(_hasSubMenu){
						var subMenuIcon = document.createElement('img');
						subMenuIcon.setAttribute('src', baseDir + '/src/img/submenu.png');
						subMenuIcon.style.position = 'absolute';
						subMenuIcon.style.paddingTop = '5px';
						subMenuIcon.style.right = '5px';
						_menuItem.appendChild(subMenuIcon);
					}
					
					var _subMenu = null;
					var maxLength_s = 0
					if(_hasSubMenu){
						_subMenu = document.createElement('div');
						_subMenu.setAttribute('id', menuItem + "_subMenu_" + i);
						_subMenu.style.position = 'relative';
						_subMenu.style.top = '-15px';
						_subMenu.style.border = '1px solid #3d3d3d';
						_subMenu.style.backgroundColor = bgColor;
						_subMenu.style.padding = '1px';
						_subMenu.style.zIndex = '9999';
						_subMenu.style.display = 'none';
						
						for(var x in menuItems[menuItem][i].subMenu){
							var _subMenuItem = document.createElement('div');
							_subMenuItem.style.height = '18px';
							_subMenuItem.style.fontSize = '11px';
							_subMenuItem.style.paddingRight = '5px';
							_subMenuItem.style.fontFamily = 'Segoe ui';
							_subMenuItem.style.cursor = 'pointer';
							_subMenuItem.style.border = '1px solid transparent';
							_subMenuItem.style.paddingLeft = '30px';
							_subMenuItem.style.color = color;
							
							var keys = 0;
							for(var k in menuItems[menuItem][i].subMenu[x]){
								++keys;
							}
							
							if(keys == 0) {
								_subMenu.appendChild(divider);
								continue;
							}

							setUpOnOnclickOnMouse(_subMenuItem, null, menuItems[menuItem][i].subMenu[x].onclick);
							_subMenuItem.innerHTML = menuItems[menuItem][i].subMenu[x].title;
							
							_subMenu.appendChild(_subMenuItem);
							if(menuItems[menuItem][i].subMenu[x].title.length > maxLength_s){
								maxLength_s = menuItems[menuItem][i].subMenu[x].title.length;
							}
						}
						_subMenu.style.width  = (maxLength_s * 8) + 20 + 'px';
						_menuItem.appendChild(_subMenu);
						subMenus.push(_subMenu);
					}
					
					setUpOnOnclickOnMouse(_menuItem, _subMenu, menuItems[menuItem][i].onclick);
					
					if(menuItems[menuItem][i].title == 'Turn Off'){
						menu.appendChild(divider);
						if(!activeDelegate){
							_menuItem.style.color = 'gray';
							setUpOnOnclickOnMouse(_menuItem, null, menuItems[menuItem][i].onclick, true);
						} 
					}
			
					if(menuItems[menuItem][i].title.length > maxLength){
						maxLength = menuItems[menuItem][i].title.length;
					}
		
					menu.appendChild(_menuItem);
					menuItemsCt++;
				}
				
				menu.style.width = (maxLength * 8) + 20 + 'px';
				
				for(var i in subMenus){
					subMenus[i].style.left = (maxLength * 8) + 17 + 'px';
				}
				document.body.appendChild(menu);
			}
			
			activeMenu = menuItem;
		},
		
		addDelegator: function(_d){
			menuItems["delegate"].push({
				title: _d.name,
				onclick: function(){
					if(_d.active) {
						window.location = '?sid=' + (new Date().getTime());
						return;
					}
					window.location = '?delegate=' + _d.d_id;
				},
				active: _d.active,
				d_id: _d.d_id
			});
			
			if(_d.active == 1){
				activeDelegate = _d.d_id;
			}
		},
		
		addPrivilege: function(_p){
			if(_p == "MANAGE_ADMINS"){
				privileges.push({ 
					title: 'Manage Users',
					img: 'user.png',
					onclick: function(){
			
						window.location = baseDir + '/admin/manageusers.php' + (activeDelegate ? "?delegate=" + activeDelegate : "");
					}
				});
			} else if(_p == "MANAGE_DELEGATES_EXCLUSIONS"){
				privileges.push({
					title: 'Manage Delegates',
					img: 'delegates.png',
					onclick: function(){
						window.location = baseDir + '/admin/managedelegates.php' + (activeDelegate ? "?delegate=" + activeDelegate : "");
					}
				});
				privileges.push({
					title: 'Exclusions',
					onclick: function(){
						window.location = baseDir + '/admin/exclusions.php' + (activeDelegate ? "?delegate=" + activeDelegate : "");
					}
				});
			} else if(_p == "MANAGE_SWO_ITEMS"){
				privileges.push({
					title: 'Manage Forms',
					subMenu: [
						{
							title: 'EPOP Items',
							onclick: function(){
								window.location = baseDir + '/admin/manageitems.php' + (activeDelegate ? "?delegate=" + activeDelegate : "");
							}
						},
						{
							title: 'EPOP Sub-Items',
							onclick: function(){
								window.location = baseDir + '/admin/managesubitems.php' + (activeDelegate ? "?delegate=" + activeDelegate : "");
							}
						}
					]
				});
			} else if(_p == "MANAGE_RED_TAG_AUDITS"){
			
				privileges.push({
					title: 'Manage Red Tag Audits',
					subMenu: [
						{
							title: 'View Audits Created',
							onclick: function(){
								window.location = baseDir + '/admin/viewaudits.php' + (activeDelegate ? "?delegate=" + activeDelegate : "");
							}
						},
						{
							title: 'Manage Annual Requirments',
							onclick: function(){
								window.location = baseDir + '/admin/auditannualreq.php' + (activeDelegate ? "?delegate=" + activeDelegate : "");
							}
						}
					]
				});
			} else if(_p == "MANAGE_NEAR_MISS"){
				privileges.push({
					title: 'Workflow',
					onclick: function(){
						window.location = baseDir + '/admin/workflow.php' + (activeDelegate ? "?delegate=" + activeDelegate : "");
					}
				});
			} else if(_p == "MANAGE_AOE"){
				privileges.push({
					title: 'Anatomy of an Event',
					onclick: function(){
						window.location = baseDir + '/admin/hp.php' + (activeDelegate ? "?delegate=" + activeDelegate : "");
					}
				});
			}
		},
		
		setStormDutyAccess: function(access) {
			if(access != 1) {
				try {
					delete menuItems['obs'][5];
					delete menuItems['orgreports'][7];
				} catch(ex) {}
			}
		},
		getActiveDelegate: function(){
			return activeDelegate;
		}
	};
}();