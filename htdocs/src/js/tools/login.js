dojo.provide('sot.tools.login');
sot.tools.login = function(){
	var focusColor = '#f7f3d6';
	var error_box;
	var add_box;
	var add_toggle;
	var capslock_warning;
	var capslockOn = null;
	var capsLockTurnedOnElseWhere = true;
	
	var showLogin = function(error){
		
		sot.tools.cover(true, 1);
		var lheight = 140;
		var lwidth = 400;
		
		var login_container = document.createElement('div');
		login_container.setAttribute('id', 'login_container');
		
		var login_box = document.createElement('div');
		login_box.setAttribute('id', 'login_box');
		login_container.appendChild(login_box);
		
		var title = document.createElement('div');
		title.setAttribute('class', 'title');
		title.setAttribute('className', 'title');
		login_box.appendChild(title);
	
		//alert(baseDir);
		var img = document.createElement('img');
		img.setAttribute('src', baseDir + '/src/img/bglock.png');
		img.setAttribute('height', '25');
		img.setAttribute('width', '25');
		img.style.marginTop = '5px';
		title.appendChild(img);
		
		var text = document.createElement('div');
		text.setAttribute('class', 'text');
		text.setAttribute('className', 'text');
		text.innerHTML = 'Login';
		title.appendChild(text);
		
		var login = document.createElement('div');
		login.setAttribute('class', 'login');
		login.setAttribute('className', 'login');
		
		var form = document.createElement('form');
		var appendDir = "";
		//var location_ = window.location;
		var location_ =  window.location.toString();
		if(location_.indexOf("dteco.com/so") != -1){
			appendDir = "/so";
		}

		var action = baseDir + appendDir + '/index.php';
		if(location_.indexOf("redirect_login") != -1) {
			action = location_;
		}
		
		form.setAttribute('action', action);
		form.setAttribute('method', 'POST');
		forubmit = function(){
			return sot.tools.login.onSubmit();
		};
		
		var username_label = document.createElement('div');
		username_label.innerHTML = 'Username';
		form.appendChild(username_label);
		
		var username = document.createElement('input');
		username.setAttribute('type', 'text');
		username.setAttribute('class', 'login');
		username.setAttribute('className', 'login');
		username.setAttribute('name', 'username');
		username.setAttribute('id', 'username');
		username.onkeypress = function(){
			sot.tools.login.onKeyPress();
		};
		username.onkeydown = function(){
			sot.tools.login.onKeyDown();
		};
		username.onfocus = function(){
			sot.tools.login.onFocus(username);
		};
		username.onblur = function(){
			sot.tools.login.onBlur(username);
		};

		form.appendChild(username);
		var password_label = document.createElement('div');
		password_label.innerHTML = 'Password';
		form.appendChild(password_label);
		
		var password = document.createElement('input');
		password.setAttribute('type', 'password');
		password.setAttribute('class', 'login');
		password.setAttribute('className', 'login');
		password.setAttribute('name', 'password');
		password.setAttribute('id', 'password');
		password.onkeypress = function(){
			sot.tools.login.onKeyPress();
		};
		password.onkeydown = function(){
			sot.tools.login.onKeyDown();
		};
		password.onfocus = function(){
			sot.tools.login.onFocus(password);
		};
		password.onblur = function(){
			sot.tools.login.onBlur(password);
		};

		form.appendChild(password);
		
		var submit = document.createElement('input');
		submit.setAttribute('type', 'submit');
		submit.setAttribute('class', 'submitter');
		submit.setAttribute('className', 'submitter');
		submit.setAttribute('name', 'submitter');
		submit.setAttribute('value', 'Login');
		submit.style.marginLeft = '5px';
		form.appendChild(submit);
		
		login.appendChild(form);
		login_box.appendChild(login);
		
		capslock_warning = document.createElement('div');
		capslock_warning.style.visibility = 'hidden';
		capslock_warning.style.fontFamily = 'Arial';
		capslock_warning.style.textAlign = 'center';
		capslock_warning.style.backgroundColor = "#e5e5e5";
		capslock_warning.style.width = '100px';
		capslock_warning.style.position = 'relative';
		capslock_warning.style.left = '150px';
		capslock_warning.style.top = '-3px';
		capslock_warning.style.padding = '2px';
		capslock_warning.style.border = '1px solid black';

		var capslock_warningText1 = document.createElement('span');
		capslock_warningText1.innerHTML += '<img src="' + baseDir + '/src/img/notice.png" height="12"/>Caps Lock is on';
		capslock_warningText1.style.fontSize = '10px';
		capslock_warningText1.style.position = 'relative';
		capslock_warningText1.style.veritcalAlign = 'bottom';
		capslock_warning.appendChild(capslock_warningText1);
		login_box.appendChild(capslock_warning);
		
		error_box = document.createElement("div");
		error_box.style.padding = '5px 0px 5px 0px';
		error_box.style.width = '400px';
		error_box.style.marginTop = '5px';
		error_box.style.textAlign = 'center';
		
		if(error && error != ""){
			error_box.setAttribute('class', 'error');
			error_box.setAttribute('className', 'error');
			error_box.innerHTML = error;
		} else {
			error_box.setAttribute('class', 'attention');
			error_box.setAttribute('className', 'attention');
			error_box.innerHTML = 'Please login to continue';
		}
		login_container.appendChild(error_box);
		
		var add_container = document.createElement('div');
		add_container.style.position = 'relative';
		add_container.style.paddingRight = '5px';
		add_container.style.top = '-15px';
		add_container.style.textAlign = 'right';
		
		add_toggle = document.createElement('a');
		add_toggle.setAttribute("href", "#");
		add_toggle.style.fontSize = "10px";
		add_toggle.onclick = function(){
			sot.tools.login.toggleAddBox();
			return false;
		};
		
		add_toggle.innerHTML += "Additional Options";
		login_box.appendChild(add_container);
		add_container.appendChild(add_toggle);
		
		add_box = document.createElement("div");
		add_box.style.padding = '5px 0px 5px 0px';
		add_box.setAttribute('class', 'add_box');
		add_box.setAttribute('className', 'add_box');
		add_box.style.width = '400px';
		add_box.style.marginTop = '5px';
		add_box.style.fontSize = '10px';
		add_box.style.textAlign = 'right';
		add_box.style.display = 'none';
		add_box.innerHTML = "<img src='" + baseDir + "/src/img/newdocument.png' height='12' width='11' style='cursor: pointer;' alt='Create New SWO Observation' onclick='window.location=\"forms/epop.php?swo\"'/> ";
		add_box.innerHTML += '<a href="#" onclick="window.location=\'' + baseDir + '/forms/epop.php\'">Create New SWO Observation</a>&nbsp;<br/>';
		add_box.innerHTML += "<img src='" + baseDir + "/src/img/newdocument.png' height='12' width='11' style='cursor: pointer;' alt='Submit a Near Iss' onclick='window.location=\"forms/nearmiss.php\"'/> ";
		add_box.innerHTML += '<a href="#" onclick="window.location=\'' + baseDir + '/forms/nearmiss.php\'">Submit a Near Miss</a>&nbsp;<br/>';
		login_container.appendChild(add_box);
		
		
		document.body.appendChild(login_container);
		resizer();
		

		setTimeout(function(){
			var username = dojo.byId('username');

			if (username.value == '') {
				try {
					var x = new ActiveXObject("WScript.Network");
					username.value = x.UserName;
					
				} catch (_e) { }
			}
		
			if (username.value != "") {
				dojo.byId('password').focus();
			} else {
				username.focus();
			}
		}, 300);
	
	};
	
	var onSubmit = function(){
		capslock_warning.style.visibility = 'hidden';
		error_box.style.display = 'block';
		if(dojo.byId("username").value == ""){
			error_box.setAttribute('class', 'error');
			error_box.setAttribute('className', 'error');
			error_box.innerHTML = 'Please input username';
			dojo.byId("username").focus();
			return false;
		}
	
		if(dojo.byId("password").value == ""){
			error_box.setAttribute('class', 'error');
			error_box.setAttribute('className', 'error');
			error_box.innerHTML = 'Please input password';
			dojo.byId("password").focus();
			return false;
		}
		
		error_box.setAttribute('class', 'information');
		error_box.setAttribute('className', 'information');
		error_box.innerHTML = 'Processing login...';
		
		return true;
	};
	
	var onKeyPress = function(){
		var e = window.event;
		var s = String.fromCharCode(e.keyCode || e.which);
	
		if (s.toUpperCase() === s && !e.shiftKey && s.match(/^[a-zA-Z]+$/)) {
			capslock_warning.style.visibility = '';
			capslockOn = true;
			capsLockTurnedOnElseWhere = false;
		} else if(s.toLowerCase() === s && !e.shiftKey && s.match(/^[a-zA-Z]+$/)) {
			capslockOn = false;
			capslock_warning.style.visibility = 'hidden';
			capsLockTurnedOnElseWhere = false;
		}
	};
	
	var onKeyDown = function(){
		var e = window.event;
		var keyStroke = e.keyCode || e.which;
	
		if(capslockOn && keyStroke == 20){
			capslockOn = false;
			capslock_warning.style.visibility = 'hidden';
		} else if(!capslockOn && keyStroke == 20 && !capsLockTurnedOnElseWhere){
			capslockOn = true;
			capslock_warning.style.visibility = '';	
			capsLockTurnedOnElseWhere = false;
		}

	};
	
	var onFocus = function(_i){
		_i.style.backgroundColor =  focusColor;
	};
	
	var onBlur = function(_i){
		_i.style.backgroundColor = '#fff';
	};
	
	var resizeLogin = function(){
		var wb = dojo.window.getBox();
		dojo.byId("login_container").style.left = (wb.w / 2) - (dojo.byId("login_container").offsetWidth / 2)+ 'px';
		dojo.byId("login_container").style.top = (wb.h / 2) - (dojo.byId("login_container").offsetHeight / 2) + 'px';
	};
	
	var toggleAddBox = function(){
		var display = add_box.style.display == "block" ? false : true;
		
		add_box.style.display = (display ? "block" : "none");
		error_box.style.display = (display ? "none" : "block");
		
		add_toggle.innerHTML = (display ? "[x] " : "") + "Additional Options";
	};

	return {
		showLogin: showLogin,
		onSubmit: onSubmit,
		onKeyPress: onKeyPress,
		onKeyDown: onKeyDown,
		onFocus: onFocus,
		onBlur: onBlur,
		resizeLogin: resizeLogin,
		toggleAddBox: toggleAddBox
	};
}();
