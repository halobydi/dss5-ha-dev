dojo.provide("sot.tools.ajax");

sot.tools.ajax = function(){
	var ajaxInstances = {};
	var loaders = [];
	var ct = 0;
	var loader = null;
	var loadingImg = null;
	
	var loadingOnOff = function(isLoading, cover){
		var wb = dojo.window.getBox();
		
		if(isLoading){	
			if(cover) {sot.tools.cover(true)};
			loaders.push({});
			
			if(!loader){
				loader = document.createElement('div');
				loader.setAttribute('id', 'loader');
				loader.style.textAlign = 'center';	
				loader.style.position = 'fixed';
				loader.style.backgroundColor = '#fff';
				loader.style.width = '60px';
				loader.style.height = '40px';
				loader.style.zIndex = 100001;
				loader.style.paddingTop = '8px';
				loader.style.border = '1px solid #000';
				loader.style.top = (wb.h / 2) - 15 + 'px';
				loader.style.left = (wb.w / 2) - 35 + 'px';
			
				loadingImg = document.createElement('img');
				loadingImg.setAttribute('src', baseDir + '/src/img/loading.gif');
			
				loader.appendChild(loadingImg);
			}
			
			loader.style.display = 'block';
			
		} else {
			if(loaders.length == 1){
				sot.tools.cover(false);
				loader.style.display = 'none';
			} 
			
			loaders.pop();
		}
	};
	
	return {
		loadingOnOff: loadingOnOff,
		submit: function(method, handleAs, content, callback, loading, loadingCover){
			if(loading){
				loadingOnOff(true, loadingCover);
			}
			
			mcl.ajax.submit(method, {
				url: baseDir + '/src/php/engine.php',
				handleAs: handleAs || 'json',
				content: content,
				load: function(_e){
					if(_e){
						if(typeof callback == 'function'){
							if(callback(_e)){
								loadingOnOff(false, loadingCover);
								return;
							} else {
								loaders.pop();
							}
							return;
						}
					}
					
					if(loading){
						loadingOnOff(false);
					}
					
				},
				error: function(_e){
					loadingOnOff(false);
				}
			});
		}
	};
}();
