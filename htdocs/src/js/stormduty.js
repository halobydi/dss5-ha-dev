dojo.provide("sot.stormduty");
var baseDir = window.location.protocol + "//" + window.location.host;

sot.stormduty = function() {
	return {
		view: function(sd_id) {
			window.open('../../forms/stormduty.php?sd_id=' + sd_id + '&readonly=true',  "_blank", "width=1000, toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes");
		},
		disable: function() {
			$('form').find('input,textarea,button,select').prop('disabled', true);
		},
		_delete: function(sd_id) {
			if(confirm("Are you sure you want to delete this Storm Dhtg Observation (Observation# " + sd_id + ")?\nThis action cannot be undone.")) {
				sot.tools.ajax.submit("get", "json", {
					f: "deleteStormDuty",
					sd_id: sd_id
				}, function(_e){
					window.location.reload();
					
					return true;
				}, true, true);
			}
		},
		populateName: function(input, name_field) {
			var input = $(input);
			sot.tools.ajax.submit("get", "json", {
				f: "getInfo",
				usid: input.val()
			}, function(_e){
				
				if(!_e.name) {
					$(input).addClass('error');
					$("#" + name_field).addClass('error');
				} else {
					$(input).removeClass('error');
					$("#" + name_field).removeClass('error');
				}
				
				$("#" + name_field).val(_e.name || '');
				
				return true;
				
			}, true, false);
		},
		validate: function() {
			var invalid = false;
			$('input[type=text], textarea').each(function(key, object) {
				object = $(object);
				var id = object.attr('id');
				if(id) {
					if(id.indexOf('member') == 0) {
						if(id != 'member_1_usid' && id != 'member_1_name') {
							return;
						}
					}
				}
				
				if(!object.css('display') != 'none' && (object.parents('tr').css('display') !== 'none')) {
					val = object.val();
					if(val == '') {
						object.addClass('error');
						object.change(function() {
							$(this).removeClass('error');
						});
						
						invalid = true;
					}
				}
			});
			
			if(invalid) {
				alert('Please complete ALL fields before submitting this observation.');
				return false;
			}
			
			var allChecked = true;
			var radios = [];
			$('input[value=IO]').each(function(key, object) {
				object = $(object);
				var name = object.attr('name');
				
				var groupChecked = false;
				var naRadio = null;
				$('[name=' + name + ']').each(function(key, radio) {
					radio = $(radio);
					if(radio.is(":checked")) {
						groupChecked = true;
					}
					
					if(radio.val() == 'NA') {
						naRadio = radio;
					}
				});
				
				if(!groupChecked) {
					allChecked = false;
					radios.push(naRadio);
				}
			});
			
					
			if(!allChecked) {
				var confirmed = confirm('Not all responses have been entered. Click Ok to mark empty responses as N/A or click Cancel to complete these responses.');
				if(confirmed) {
					for(var i in radios) { radios[i].prop('checked', true);	}
				} else {
					return false;
				}
			}
			
			sot.tools.cover(true);
			return true;
		}
	};
	
}();

$(document).ready(function() {
	$('img[src$="comments.png"]').each(function(key, object) {
		object = $(object);
		object.parent().click(function() {
			var item = $(this).find('img').attr('id');
			
			var comments = $('#' + item + '_comments');
			var row = comments.parents('tr');
			
			if(row.css('display') == 'none') {
				row.css('display', '');
				comments.focus();
				$(this).find('img').attr('src', '../src/img/removecomments.png');
			} else {
				var val = comments.val();
				
				if(val.replace(/\s+/g, '') !== '') {
					if(!confirm('You have un-saved comments, confirming this action will remove any comments you have inputted. Do you wish to continue?')) {
						return;
					}
				}
				
				row.css('display', 'none');
				$(this).find('img').attr('src', '../src/img/comments.png');
				comments.val('');
				
			}
		});
	});
});