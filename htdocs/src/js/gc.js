dojo.provide("sot.gc");
var baseDir = window.location.protocol + "//" + window.location.host;

sot.gc = function() {
	return {
		view: function(gc_id) {
			window.open(baseDir + "/forms/nearmiss.php?gc=true&gc_id=" + gc_id + "&mode=readonly", "_blank", "width=1000, toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes");
		},
		_delete: function(gc_id) {
			if(confirm("Are you sure you want to delete this Good Catch (#" + gc_id + ")?\nThis action cannot be undone.")) {
				sot.tools.ajax.submit("get", "json", {
					f: "deleteGoodCatch",
					gc_id: gc_id
				}, function(_e){
					window.location.reload();
					
					return true;
				}, true, true);
			}
		}
	};
	
}();