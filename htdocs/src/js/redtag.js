dojo.provide('sot.redtag');
var baseDir = window.location.protocol + "//" + window.location.host;

var parent = 'attachments';
var a_count = 0;
var count = 'a_count';
var last_incomplete;
		
sot.redtag = function(){
	var js_calendar;

	return {
		req: function(id, month, year, total) {
			sot.tools.ajax.submit("get", "json", {
				f: "RTAReq",
				id: id,
				month: month,
				year: year,
				total: total
			}, function(e){
				//if(e) {
				//	alert("Error saving requirments: " + e.message);
				//}
				alert();
				return true;
			
			}, true, true);
		},
		cal: function(id, field) {
			if (!js_calendar) {
				js_calendar = Calendar.setup({
						animation: false,
						onSelect: function() { this.hide() }
				});
			}
			
			js_calendar.manageFields(id, field, '%m/%d/%Y');
		},
		
		switchOrg: function(){
			locations = dojo.byId("location");
			locations.innerHTML = "";
			dojo.byId("reminder").innerHTML = "";
			sot.tools.ajax.submit("get", "json", {
				f: "getLocations_redtag",
				org: dojo.byId("org").value
			}, function(_e){
				for(var i in _e){
					var option = document.createElement('option');
					option.setAttribute('value', _e[i].LOCATION)
					option.innerHTML = _e[i].LOCATION;
					locations.appendChild(option);
				}
				return true;
			}, true, false);
		},
		
		switchCreditMonth: function(){
			dojo.byId("reminder").innerHTML = "";
			var location_ = dojo.byId("location").value;
			var org = dojo.byId("org").value;
			
			if(location_ == ""){
				return;
			}
			
			sot.tools.ajax.submit("get", "json", {
				f: "getLastAudit",
				location: location_,
				org: org,
				credit_month: dojo.byId("credit_month").value
			}, function(_e){
				if(_e){
					dojo.byId("reminder").innerHTML = "Last audit performed for " + location_ + (org == "" ? "" : " ("  + org + ")") + " was on " + _e.MONTH + " " + _e.YEAR ;
				} 
				return true;
			}, true, false);
		},
		
		imposeMaxLength: function(Object, MaxLen){
			return (Object.value.length <= MaxLen);
		},

		onSubmit: function(){
			var inputs = document.getElementsByTagName('input');  
			var org = dojo.byId("org");
			var location_ = dojo.byId("location");
			var credit_month = dojo.byId("credit_month");
			
			if(org.value == ""){
				alert("Select org audited.");
				org.focus();
				return false;
			}
			if(location_.value == ""){
				alert("Select location.");
				location_.focus();
				return false;
			}
			if(credit_month.value == ""){
				alert("Select credit month.");
				credit_month.focus();
				return false;
			}
			
			if(isNaN(dojo.byId("rsd").value)){
				alert('Invalid RSD# entered. Only numeric characters allowed.');
				dojo.byId("rsd").focus();
				return false;
			}
			
			var x = 0; 
			var complete = true; 
			var type_checked = false;
			for( i = 0; i < inputs.length; i++ ) {
				if(inputs[i].name == "id"){continue;}
				if (inputs[i].type == 'radio'){
					var group = document.getElementsByName(inputs[i].name); 
					var marked = false;
					if (group != null){
						for (n = 0; n < group.length; n++){
							if (group[n].checked == true)
								marked = true;
						}
					}
					
					if(!marked){
						complete = false;
					}
				}
				
				if(inputs[i].type == 'text'){
					if(inputs[i].value == '' && inputs[i].className != 'a_name'){
						complete = false;
						alert("One or more questions have not been answered.");
						inputs[i].focus();
						return false;
					}
				}
				
				if(inputs[i].type == 'checkbox'){
					if(inputs[i].checked == true && isNaN(inputs[i].name)){
						type_checked = true;
					}
				}	
			}
			
			var textareas = document.getElementsByTagName('textarea');  
			for( i = 0; i < textareas.length; i++ ) {
				if(textareas[i].value == ''){
					complete = false;
					alert("One or more fields are blank.");
					textareas[i].focus();
					return false;
				}
			}

			if(!complete){
				alert("One or more fields are blank.");
				return false;
			}
			
			if(!type_checked) {
				alert("Select one or more audit type.");
				return false;
			}
			
			if(a_count == 0){
				alert('Please attach images for individual crew members.');
				return false;
			}
			
			sot.tools.cover(true);
			return true;
		},
		
		addAttachment: function(){
			var attachment = document.createElement('div');
			attachment.style.paddingBottom = '3px';
			attachment.id = 'attachment_' + a_count;

			attachment.innerHTML = "<input type = 'hidden' name = 'aid_" + a_count + "' value = ''/>\
									&bull;<input type = 'hidden' name = 'deleted_" + a_count + "' id = 'deleted_" + a_count + "' value = '0'/>\
									<input style = 'border: 1px solid black; margin-right: 20px;' type = 'file' name = 'file_" + a_count + "'/>\
									Name <input style = 'border: 1px solid black;' type = 'text' name = 'a_name_" + a_count + "' value = '' class='a_name'/>\
									&nbsp;<span class = 'delete_a' style = 'cursor: pointer; font-size: 10px;' onclick = 'sot.redtag.removeAttachment(" + a_count + ");'>\
										[ <img src=\"https://soteria.dteco.com/src/img/x.png\" width='12' height='11' style='vertical-align: bottom;'/> <a href='#' onclick='return false;'>Remove</a> ]\
									</span>\
								";

			a_count++;

			document.getElementById(parent).appendChild(attachment);
			document.getElementById(count).value = a_count;

		},
		
		removeAttachment: function(num){
			try {
				document.getElementById('deleted_' + num).value = '1';
				document.getElementById('attachment_' + num).style.display = 'none';
				document.getElementById(count).value = a_count;
			} catch(ex) {
				
			}
		},
		view: function(rta_id){
			window.open(baseDir + '/forms/viewRedtag.php?id=' + rta_id, '_blank', 'width=1000, toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes');
		},
		viewAttachment: function(a_id){
			window.open(baseDir + "/forms/viewAttachment.php?id=" + a_id, "_blank", "menubar=0, toolbar=0, width=50, height=50")
		},
		_delete: function(rta_id) {
			if(confirm("Are you sure you want to delete this Red Tag Audit (Audit #" + rta_id + ")?\nThis action cannot be undone.")) {
				sot.tools.ajax.submit("get", "json", {
					f: "deleteRedtag",
					rta_id: rta_id
				}, function(_e){
					window.location.reload();
					
					return true;
				}, true, true);
			}
		}
	};
}();