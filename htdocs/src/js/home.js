dojo.provide('sot.home');

sot.home = function(){
	var quarter = null;
	var reportUsid = null;
	var reportDelegate = null;
	var reportsGenerated = [];
	var dates = {};
	
	var onBackHandler;
	
	var red = '#ffaaad';
	var yellow = '#ffffbd';
	var green = '#b5d3b5';
	
	var setQuarter = function(_quarter){
		quarter = _quarter;
	};
	
	var quarterOnClick = function(_quarter){
		dojo.byId("q" + quarter + "box").setAttribute("className", "q" + quarter + "box");
		dojo.byId("q" + quarter + "box").setAttribute("class", "q" + quarter + "box");
		
		setQuarter(_quarter);
		
		quarterlyReport();
	};
	
	var setDates = function(start, end){
		dates["start"] = start;
		dates["end"] = end;
	};
	
	var yearToggle = function(dir){
		dojo.byId("year").innerHTML = parseInt(dojo.byId("year").innerHTML) + dir;
		quarterlyReport();
	};
	
	var setReportUsid = function(usid){
		reportUsid = usid;
	};
	
	var setReportDelegate = function(key){
		reportDelegate = key || 0;
	};
	
	var setOnBackDisplay = function(){
		if(reportsGenerated.length == 1){
			dojo.byId('back').style.display = 'none';
			if(onBackHandler){
				dojo.disconnect(onBackHandler);
				onBackHandler = null;
			}
		} else {
			dojo.byId('back').style.display = 'inline';
			if(!onBackHandler){
				onBackHandler = dojo.connect(document, 'keyup', function(){
					var e = window.event;
					if(e.keyCode == 27 || e.keyCode == 37){
						sot.home.back();
					}
				});
			}
		}		
	};
	
	var back = function(){
		reportsGenerated.pop();
		var usid = reportsGenerated[reportsGenerated.length - 1];
		initiate(usid, true);
			
		setOnBackDisplay();
	};
	
	var initiate = function(usid, back){
		reportUsid = usid;

		if(!back){
			reportsGenerated.push(usid);
		}

		sot.tools.ajax.submit("get", "text", {
			f: "getDirectTeam",
			usid: reportUsid,
			quarter: quarter,
			year: dojo.byId("year") ? dojo.byId("year").innerHTML : "",
			delegate: reportDelegate
		}, function(_e){
			dojo.byId('container').innerHTML = _e;
			
			weeklyReport();
			quarterlyReport();
			
			return true;
		}, true, true);
		
		setOnBackDisplay();
	};
	
	var weeklyReport = function(){
		sot.tools.ajax.submit("get", "json", {
			f: "weeklyMetric",
			usid: reportUsid,
			startDate: dates["start"],
			endDate: dates["end"]
		}, function(_e){
			//alert(_e);
			
			if(_e["io"]){
				dojo.byId("io").innerHTML = _e["io"] || "";
				delete _e["io"];
			}
			
			for(var _u in _e){
				if(_u != reportUsid){
					dojo.byId(_u + "_observedweek").innerHTML = _e[_u].observed ? "<img src='src/img/check.png' style='cursor: pointer;' onclick='sot.swo.view(" + _e[_u].observed + ");'/>" : "";
				} else {
					dojo.byId(_u + "_observedweek").innerHTML = '--';
				}
				
				if(isNaN(_e[_u].completed) && _e[_u].completed != "") {
					dojo.byId(_u + "_completedweek").innerHTML = _e[_u].completed == "--" ? "--" : "<span style='font-size: 10px;'>" + _e[_u].completed + "</span>";
				} else {
					dojo.byId(_u + "_completedweek").innerHTML = (_e[_u].completed ? "<img src='src/img/check.png' style='cursor: pointer;' onclick='sot.swo.view(" + _e[_u].completed + ");'/>" : "<img src='src/img/x.png'/>");
				}
	
				if(dojo.byId(_u + "_lc")){
					dojo.byId(_u + "_lc").innerHTML = _e[_u].lc + (_e[_u].lc != "--" ? "%" : "");	
					if(_u == reportUsid){
						if(_e[_u].lc != "--"){
							if(_e[_u].lc < 60){
								dojo.byId(_u + "_lc").parentNode.style.backgroundColor = red;
							} else if(_e[_u].lc < 80){
								dojo.byId(_u + "_lc").parentNode.style.backgroundColor = yellow;
							} else if(_e[_u].lc >= 80){
								dojo.byId(_u + "_lc").parentNode.style.backgroundColor = green;
							} 
						}
					}
				} else {
					if(_u == reportUsid && _e[_u].completed != "--" && !isNaN(_e[_u].completed)){
						if(_e[_u].completed){
							dojo.byId(_u + "_completedweek").parentNode.style.backgroundColor = green;
						} else {
							dojo.byId(_u + "_completedweek").parentNode.style.backgroundColor = red;
						}
					}
				}
			}
			
			return true;
		}, true, true);
	};
	
	var quarterlyReport = function(){
		sot.tools.ajax.submit("get", "json", {
			f: "quarterlyMetric",
			usid: reportUsid,
			quarter: quarter,
			year: dojo.byId("year").innerHTML
		}, function(_e){
			dojo.byId("quarter").innerHTML = quarter;
			dojo.byId("q" + quarter + "box").setAttribute("className", "activeqbox");
			dojo.byId("q" + quarter + "box").setAttribute("className", "activeqbox");
			
			for(var _u in _e){
				
				if(_u != reportUsid){
					dojo.byId(_u + "_observedquarter").innerHTML = _e[_u].observed ? "<img src='src/img/check.png' style='cursor: pointer;' onclick='sot.swo.view(" + _e[_u].observed + ");'/>" : "<img src='src/img/x.png'/>";
				} else {
					dojo.byId(_u + "_observedquarter").innerHTML = '--';
				}
				
				dojo.byId(_u + "_completedquarter").innerHTML = _e[_u].totalemployeesobserved;
				
				if(dojo.byId(_u + "_qc")){
					dojo.byId(_u + "_qc").innerHTML = _e[_u].percentcomplete + (_e[_u].percentcomplete != "--" ? "%" : "");	
					
					if(_u == reportUsid){
						if(_e[_u].percentcomplete != "--"){
							if(_e[_u].percentcomplete < 60){
								dojo.byId(_u + "_qc").parentNode.style.backgroundColor = red;
							} else if(_e[_u].percentcomplete < 80){
								dojo.byId(_u + "_qc").parentNode.style.backgroundColor = yellow;
							} else if(_e[_u].percentcomplete >= 80){
								dojo.byId(_u + "_qc").parentNode.style.backgroundColor = green;
							} 
						}
					}
				}
			}
			
			return true;
		}, true, true);
	};
	
	var dateFilter = function(){

		setDates(dojo.byId("start").value, dojo.byId("end").value);
		weeklyReport(false);
		
	};
	
	var showAllIO = function(link){
		var tbl = dojo.byId("io_tbl");
		
		if(tbl){
			for(var i in tbl.rows){
				if(typeof tbl.rows[i] == "object"){
					tbl.rows[i].style.display = '';
				}
			}
		}
		
		link.style.color = '#000';
		link.style.cursor = 'text';
		link.focus();

	};
	
	var exportExcel = function(){
		sot.tools.cover(true, 3);
		var wb = dojo.window.getBox();
		
		var popup_box = document.createElement('div');
			popup_box.setAttribute('id', 'popup_box');
			popup_box.style.padding = '0px';
			popup_box.style.position = 'fixed';
			popup_box.style.left = "-1000px";
			popup_box.style.top = "-1000px";
			//popup_box.style.visibility = 'hidden';
			popup_box.style.zIndex = 8;
			
			var box = "<table class='tbl'>";
			box += "<tr><th colspan='6'><div class='inner_title'>Export Observations</div></th></tr>";
			box += "<tr>";
			box += "<th><div class='inner' style='width: 150px; height: 15px;'>Start Date</div></th>";
			box += "<th><div class='inner' style='width: 150px; height: 15px;'>End Date</div></th>";
			box += "<th><div class='inner' style='width: 180px; height: 15px;'>Observation</div></th>";
			box += "<th><div class='inner' style='width: 270px; height: 15px;'>*Employee</div></th>";
			box += "<th><div class='inner' style='width: 200px; height: 15px;'>Excel Format</div></th>";
			box += "<th><div class='inner' style='width: 150px; height: 15px;'></div></th>";
			box += "</tr>";
			
			var options = "";
			options += "<option value='pp'>Paired Performace</option>";
			options += "<option value='qew'>Qualified Electrical Worker</option>";
			options += "<option value='swo_lc' selected=selected>Safe Worker</option>";
			//options += "<option value='swo_lc'>Safe Worker - Life Critical</option>";
			options += "<option value='raw'>Safe Worker - Raw Export</option>";

			var options2 = "";
			options2 += "<option value='EMPLOYEE'>Employee Observed</option>";
			options2 += "<option value='OBSERVATION_CREDIT' selected=selected>Employee Performing Observation</option>";
			
			box += "<tr class='odd'>";
			box += "<td style='border: none;'><input style = 'height: 12px; width: 100px;' type = 'text' name = 'from' id = 'from' value='" + (dates["start"] || "") + "'/> <img style = 'vertical-align: bottom;' src='" + baseDir + "/src/img/calendar.gif' alt='' id='fromCal' onmouseover='setup_cal(\"fromCal\", \"from\");'/></td>";
			box += "<td style='border: none;'><input style = 'height: 12px; width: 100px;' type = 'text' name = 'to' id = 'to' value='" + (dates["end"] || "") + "' /> <img style = 'vertical-align: bottom;' src='" + baseDir + "/src/img/calendar.gif' alt='' id='toCal' onmouseover='setup_cal(\"toCal\", \"to\");' /></td>";
			box += "<td style='border: none;'><select id='exportFormType'>" + options + "</select></td>";
			box += "<td style='border: none;'><select id='employee'>" + options2 + "</select></td>";
			box += "<td style='border: none;'>";
			box += "<input type='radio' id='single' onclick='dojo.byId(\"multi\").checked=false; this.checked=true;' checked=checked>*Single Line <input type='radio' id='multi' onclick='dojo.byId(\"single\").checked=false; this.checked=true;'>Multi Line";
			box += "</td>";
			box += "<td style='border: none;'><button style='width: 70px; height: 19px; position: relative;' onclick='sot.home.doExportExcel();'>Export</button>&nbsp;<button style='width: 70px; height: 19px; position: relative;' onclick='sot.home.doExportExcel(true);'>Cancel</button></td>";
			box += "</tr>";
			box += "<tr  class='odd' >";
			box += "<td colspan='6' style='text-align: right;'><span style='font-size: 10px;'>*Single Line does not display drill down items and <b>only</b> displays additional comments entered at the end of the observation</span></td>";
			box += "</tr>";
			//box += "<tr  class='odd' >";
			//box += "<td colspan='6' style='text-align: right;'><span style='font-size: 10px;'>*Employee Performing Observation and Employee who Submitted the Observation will differ when an observation is completed by a delegate</span></td>";
			//box += "</tr>";
	
			box += "</table>";
			popup_box.innerHTML = box;
			
		document.body.appendChild(popup_box);
		setTimeout(function() {
			resizer();
			if(dojo.byId("popup_box")) {
				//dojo.byId("popup_box").style.visibility = 'visible';
			}
		}, 100);
	};
	
	var doExportExcel = function(cancel){
		if(cancel){
			document.body.removeChild(dojo.byId("popup_box"));
			sot.tools.cover(false);
			return;
		} 
		
		if(dojo.byId("from").value == ""){
			alert("Enter start date for time frame.");
			dojo.byId("from").focus();
			return;
		}
		
		if(dojo.byId("to").value == ""){
			alert("Enter end date for time frame.");
			dojo.byId("to").focus();
			return;
		}
		
		var file = "excel";
		if(dojo.byId("single").checked == true) {
			file = "excelsingleline";
		}

		var formType = dojo.byId("exportFormType").value;
		if (formType == "raw"){
			file = "rawExcel";
		}

		// Opens Download Pop-up
		window.open(baseDir
            + '/reports/individual/'
            + file // excel.php / excelsingleline.php
            + '.php?method=dates&form='
            + formType // Paired Perf, QEW, Safe Worker, Safe Worker LC
            + '&employee='
            + dojo.byId("employee").value // Employee Observerd / Employee Performing Observation
            + '&start='
            + (dojo.byId("from").value) // From Date
            + '&end='
            + (dojo.byId("to").value) // To Date
            + (sot.tools.menu.getActiveDelegate() ? "&delegate="
            + sot.tools.menu.getActiveDelegate() : "")
            , ""
            , "width=50, height=50");
		doExportExcel(true);
	};
	
	/*
	var excelOldEmployee = "OBSERVATION_CREDIT";
	var excelSwitchEmployee = function(form) {
		var curExcelOldEmployee = dojo.byId("employee").value;
		if(form == "PP") {
			dojo.byId("employee").value = "COMPLETED_BY"; //usually corp safety employee is completed this observation
		} else {
			dojo.byId("employee").value = excelOldEmployee;
		}
		excelOldEmployee = curExcelOldEmployee;
		
	};
	*/

	var addEmail = function() {
		var email = dojo.byId("email");
		var observation = dojo.byId("observation");
		if(email.value == "") {
			alert("Enter a email address to add to the Mailing List.");
			email.focus();
			return;
		}
		if(observation.value == "") {
			alert("Choose an obseration to subscribe to.");
			observation.focus();
			return;
		}

		sot.tools.ajax.submit("get", "json", {
			f: "addEmail",
			email: email.value,
			observation: observation.value
		}, function(_e){
			if(_e.error) {
				alert(_e.error);
			} else {
				alert((email.value) + " has been added to the mailing list.");
				window.location.reload();
			} 
			
			return true;
		}, true, true);
	};

	var deleteEmail = function(email, observation) {
		if(confirm("Are you sure you want to delete this email from the mailing list (" + email + ")?\nThis action cannot be undone.")) {
			sot.tools.ajax.submit("get", "json", {
				f: "deleteEmail",
				email: email,
				observation: observation
			}, function(_e){
				window.location.reload();
				
				return true;
			}, true, true);
		}
	};

	return {
		setQuarter: setQuarter,
		quarterOnClick: quarterOnClick,
		setDates: setDates,
		yearToggle: yearToggle,
		setReportUsid: setReportUsid,
		setReportDelegate: setReportDelegate,
		back: back,
		initiate: initiate,
		weeklyReport: weeklyReport,
		quarterlyReport: quarterlyReport,
		dateFilter: dateFilter,
		showAllIO: showAllIO,
		exportExcel: exportExcel,
		doExportExcel: doExportExcel,
		addEmail: addEmail,
		deleteEmail: deleteEmail
	};
}();