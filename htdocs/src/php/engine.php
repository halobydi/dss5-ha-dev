<?php
/*Change Log
 * DS-148 12-21-2016 Serious? update/insert GalaxE Detroit Edward King
 *
 *
 */
require_once("mcl_Oci.php");
require_once("auth.php");

if(!function_exists('errorHandler')) {
	function errorHandler($errno, $errstr, $errfile, $errline, $errcontext){}
	set_error_handler('errorHandler');
}

class engine {

	private static $db = array();
	private static $reportsGenerated = array(); /*Username*/
	private static $lc_stmt = array();
    //generic sql calls
	private static $SQLGen = array();
	/*DATABASE CONNECTIONS*/
	 private static function db($alias)
	{
		if (empty(self::$db[$alias])) {
			self::$db[$alias] = new mcl_Oci($alias);
		}

		return self::$db[$alias];
	}

	protected static function sot()
	{
		self::db("soteria")->dateFormat();
		return self::db("soteria");
	}

	protected static function dot()
	{
		return self::db("dot");
	}

	static function reportsGenerated($usid){
		if(!isset(self::$reportsGenerated[$_REQUEST["usid"]])){
			$sql = "
				SELECT 	USID, 
						NAME, 
						SUPERVISOR,
						REPORTS, 
						LEADERS, 
						TEAM,
						CASE WHEN USID = '{$_REQUEST["usid"]}' THEN 1 ELSE 0 END AS ORDINAL
				FROM 	EMPLOYEES 
				WHERE 	(SUPERVISOR = '{$_REQUEST["usid"]}' OR USID = '{$_REQUEST["usid"]}')
				ORDER BY ORDINAL, NAME
			";


			while($row = self::sot()->fetch($sql)){
				$directTeam[$row["USID"]] = $row;
			}
			self::addReportsGenerated($_REQUEST["usid"], $directTeam);
		}

		return self::$reportsGenerated[$_REQUEST["usid"]];
	}
	public static function setReportsGenerated(){
		self::$reportsGenerated = $_SESSION["reportsGenerated"];
	}

	protected static function addReportsGenerated($usid, $directTeam){
		$_SESSION["reportsGenerated"][$usid] = $directTeam;
		self::$reportsGenerated[$usid] = $directTeam;
	}

	public static function lcCompletion($usid, $leaderCt = 0){

		$startDate = @strtotime($_REQUEST['startDate'], 0);
		$endDate = @strtotime($_REQUEST['endDate'], 0);
		$weeks = @ceil(($endDate - $startDate) / 604800);

		$expected = 0;
		$actual = 0;

		for($i = 0; $i < $weeks; $i++) {

			$start = "(TRUNC(TO_DATE(:startDate, 'MM/DD/YYYY HH24:MI:SS'), 'IW') + :week * 7)";
			$end = "(NEXT_DAY(TRUNC(TO_DATE(:startDate, 'MM/DD/YYYY HH24:MI:SS'),'IW'), 'SUNDAY') + :week * 7) + .999";

			if(!self::$lc_stmt["lc_ex"]){
				self::$lc_stmt["lc_ex"] = self::sot()->parse("
					SELECT 	COUNT(USID) AS CT 
					FROM 	EMPLOYEES E
					WHERE 	PATH LIKE :usid 
							AND LEADERS > 0
							AND NOT EXISTS(
								SELECT 	1 
								FROM 	EXCLUSIONS EX 
								WHERE 	EX.USID = E.USID 
										AND ( START_DT <=  {$start}  OR START_DT IS NULL)
										AND ( END_DT >=  {$end}  OR END_DT IS NULL	)
							)
				");
			}

			self::sot()->bind(self::$lc_stmt["lc_ex"], array(
				":usid"			=> "%{$usid}%",
				":startDate"	=> "{$_REQUEST["startDate"]} 00:00:00",
				":week"			=> $i
			));

			$row = self::sot()->fetch(self::$lc_stmt["lc_ex"]);
			$expected += intval($row['CT']);

			if(!self::$lc_stmt["lc"]){
				self::$lc_stmt["lc"] = self::sot()->parse("
					WITH T AS (
						SELECT 	USID 
						FROM 	EMPLOYEES E
						WHERE 	PATH LIKE :usid 
								AND LEADERS > 0
								AND NOT EXISTS(
									SELECT 	1 
									FROM 	EXCLUSIONS EX 
									WHERE 	EX.USID = E.USID 
											AND ( START_DT <=  {$start}  OR START_DT IS NULL)
											AND ( END_DT >=  {$end}  OR END_DT IS NULL	)
								)
					) 
					SELECT	COUNT(*) AS CT 
					FROM(
						SELECT 	OBSERVATION_CREDIT
						FROM 	T, 
								EPOP_OBSERVATIONS O 
						WHERE 	T.USID = O.OBSERVATION_CREDIT
								AND OBSERVED_DATE BETWEEN {$start} AND {$end}
								--AND (SWO = 1 OR PP = 1)
						GROUP BY OBSERVATION_CREDIT
					)
				");
			}

			self::sot()->bind(self::$lc_stmt["lc"], array(
				":usid"			=> "%{$usid}%",
				":startDate"	=> "{$_REQUEST["startDate"]} 00:00:00",
				":week"			=> $i
			));

			$row = self::sot()->fetch(self::$lc_stmt["lc"]);
			$actual += intval($row['CT']);

		}

		if($expected == 0) {
			return '--';
		} else {
			$percent = @round(($actual / $expected) * 100, 2);
			return $percent;
		}

		/*
		--OLD METHOD
		//Add exclusions
		if($leaderCt == 0){
			return '--';
		}

		//Added 11/2/2012, datediff was returning 2 when it should be 1
		$diff = @strtotime($_REQUEST["endDate"], 0) - @strtotime($_REQUEST["startDate"], 0);
		$weeks = @ceil($diff / 604800);

		if(!self::$lc_stmt["lc_ex"]){
			self::$lc_stmt["lc_ex"] = self::sot()->parse("
				SELECT COUNT(USID) AS CT FROM EMPLOYEES E
					WHERE PATH LIKE :usid
					AND LEADERS > 0
					AND EXISTS(
					 SELECT 1 FROM EXCLUSIONS EX
						 WHERE EX.USID = E.USID
						 AND ( START_DT <=  TO_DATE(:startDate, 'MM/DD/YYYY HH24:MI:SS')  OR START_DT IS NULL)
						 AND ( END_DT >=  TO_DATE(:endDate, 'MM/DD/YYYY HH24:MI:SS')  OR END_DT IS NULL	)
					)
			");
		}

		self::sot()->bind(self::$lc_stmt["lc_ex"], array(
			":usid"			=> "%{$usid}%",
			":startDate"	=> $_REQUEST["startDate"] . ' 00:00:00',
			":endDate"		=> $_REQUEST["endDate"] . ' 23:59:59'
		));

		$row = self::sot()->fetch(self::$lc_stmt["lc_ex"] );
		$leaderCt -= $row["CT"];

		if($leaderCt == 0){
			return '--';
		}

		$expected = $leaderCt * $weeks;

		if(!self::$lc_stmt["lc"]){
			self::$lc_stmt["lc"] = self::sot()->parse("
				WITH T AS (
					SELECT USID FROM EMPLOYEES E
						WHERE PATH LIKE :usid
						AND LEADERS > 0
						AND NOT EXISTS(
						 SELECT 1 FROM EXCLUSIONS EX
							 WHERE EX.USID = E.USID
							 AND ( START_DT <=  TO_DATE(:startDate, 'MM/DD/YYYY HH24:MI:SS')  OR START_DT IS NULL)
							 AND ( END_DT >=  TO_DATE(:endDate, 'MM/DD/YYYY HH24:MI:SS')  OR END_DT IS NULL	)
						)
				)
				SELECT COUNT(*) AS CT FROM(
					SELECT OBSERVATION_CREDIT, NEXT_DAY(TRUNC(OBSERVED_DATE,'IW'), 'SUNDAY')
					FROM T, EPOP_OBSERVATIONS O
					WHERE T.USID = O.OBSERVATION_CREDIT
					AND OBSERVED_DATE BETWEEN TO_DATE(:startDate, 'MM/DD/YYYY HH24:MI:SS')
						AND TO_DATE(:endDate, 'MM/DD/YYYY HH24:MI:SS')
					AND SWO = 1
					GROUP BY OBSERVATION_CREDIT, NEXT_DAY(TRUNC(OBSERVED_DATE,'IW'), 'SUNDAY')
				)
			");
		}

		self::sot()->bind(self::$lc_stmt["lc"], array(
			":usid"			=> "%{$usid}%",
			":startDate"	=> $_REQUEST["startDate"] . ' 00:00:00',
			":endDate"		=> $_REQUEST["endDate"] . ' 23:59:59'
		));

		$row = self::sot()->fetch(self::$lc_stmt["lc"]);

		$actual = $row["CT"];
		$percent = @round(($actual / $expected) * 100, 2);


		return $percent;
		*/
	}

	static function printer($out){
		echo json_encode($out);
	}

	static function quarterDates($quarter, $year){
		if(!$year){
			$year = date("Y");
		}

		if($quarter == 1){
			$qstart = '01/01/' . $year;
			$qend = '03/31/' . $year;
		} else if($quarter == 2){
			$qstart = '04/01/' . $year;
			$qend = '06/30/' . $year;
		} else if($quarter == 3){
			$qstart = '07/01/' . $year;
			$qend = '09/30/' . $year;
		} else {
			$qstart = '10/01/' . $year;
			$qend = '12/31/' . $year;
		}

		return array("start" => $qstart, "end" => $qend);
	}

	public static function getDirectTeam(){

		$directTeam = array();
		$directTeam = self::reportsGenerated($_REQUEST["usid"]);

		$user = auth::check();
		$delegate = (isset($_REQUEST["delegate"]) != 0 && !empty($_REQUEST["delegate"])) ? "&delegate={$_REQUEST["delegate"]}" : "";

		$hasLeaders = $directTeam[$_REQUEST["usid"]]["LEADERS"] > 1 ? true : false;

		$weekly .= "<table class='tbl'>";
		$weekly .= "<tr><th colspan='" . ($hasLeaders ? 4 : 3) . "'><div class='inner_title' >Weekly Observation Metric</div></th></tr>";
		$weekly .= "<tr>";
		$weekly .= "<th class='double'><div style='width:100px;' class='inner'>Has Employee</br>Been Observed?</div></th>";
		$weekly .= "<th class='double'><div style='width:300px;' class='inner'>Employee</div></th>";
		$weekly .= "<th class='double'><div style='width:150px;' class='inner'>Has Employee Performed</br>1 Observation?</div></th>";
		if($hasLeaders) {$weekly .= "<th class='double'><div style='width:200px;' class='inner'>Employee & Their Team</br>Completion</div></th>";}
		$weekly .= "</tr>";

		if(!empty($directTeam)){
			foreach($directTeam as $key=>$value){
				if($_REQUEST["usid"] == $value["USID"]){
					$border = 'border-top:1px solid black;';
				}else {
					$border = '';
				}

				$weekly .= "<tr class = '" . ($x++ % 2 == 0 ? 'even' : 'odd') . "'>";
				$weekly .= "<td style='width:100px; {$border}' id='{$value["USID"]}_observedweek'></td>";
				$weekly .= "<td style='width: 300px; text-align: left; {$border}'>" . ($value["USID"] != $_REQUEST["usid"] ?
					"<a href='forms/epop.php?usid={$value["USID"]}{$delegate}'><img src='src/img/newdocument.png' alt='Observe {$value["NAME"]}'/></a> " : "") .
					(($value["REPORTS"] == 0 || $value["USID"] == $_REQUEST["usid"]) ? $value["NAME"] :
					"<a href = '#' style='' onclick = 'sot.home.initiate(\"{$value["USID"]}\");'>{$value["NAME"]}</a>")
				. "</td>";

				$weekly .= "<td style='width:150px; {$border}' id='{$value["USID"]}_completedweek'></td>";
				if($hasLeaders) {$weekly .= "<td style='width:200px; {$border}' id='{$value["USID"]}_lc'>" . ($value["LEADERS"] == 0 ? "--" : "") ."</td>";}
				$weekly .= "</tr>";

			}
		} else {
			$weekly .= "<tr><td colspan='3'>There are no metrics to display.</td></tr>";
		}

		$weekly .= "</table>";

		$quarterly .= "
		<div style='width: 895px; text-align: right;'>
			<button onclick='sot.home.quarterOnClick(1);' id='q1box' class='" . ($_REQUEST["quarter"] == 1 ? "activeqbox" : "q1box") . "'>Q1</button>
			<button onclick='sot.home.quarterOnClick(2);' id='q2box' class='" . ($_REQUEST["quarter"] == 2 ? "activeqbox" : "q2box") . "'>Q2</button>
			<button onclick='sot.home.quarterOnClick(3);' id='q3box' class='" . ($_REQUEST["quarter"] == 3 ? "activeqbox" : "q3box") . "'>Q3</button>
			<button onclick='sot.home.quarterOnClick(4);' id='q4box' class='" . ($_REQUEST["quarter"] == 4 ? "activeqbox" : "q4box") . "'>Q4</button>
			<button onclick='sot.home.yearToggle(-1);' class='btn'><img src='src/img/arrowBack.png' style='width: 12px; height: 12px; position: relative; top: -2px;'/></button>
			<button class='qyear' id='year' onmousedown='return false;'>" . (empty($_REQUEST["year"]) ? date("Y") : $_REQUEST["year"]) ."</button>
			<button onclick='sot.home.yearToggle(1);' class='btn' ><img src='src/img/arrowForward.png' style='width: 12px; height: 12px; position: relative; top: -1px;'/></button>
		</div>
		<table class='tbl'>";
		$quarterly .= "<tr>
			<th colspan='5'>
				<div class='inner_title'>
						Quarterly Observation Metric (Quarter <span id='quarter'>{$_REQUEST["quarter"]}</span>)
				</div>
			</th>
		</tr>";
		$quarterly .= "<tr>";
		$quarterly .= "<th class='double'><div style='width:100px;' class='inner'>Has Employee</br>Been Observed?</div></th>";
		$quarterly .= "<th class='double'><div style='width:300px;' class='inner'>Employee</div></th>";
		$quarterly .= "<th class='double'><div style='width:150px;' class='inner'>Total No. Of</br>Employees</div></th>";
		$quarterly .= "<th class='double'><div style='width:150px;' class='inner'>Total No. Of</br>Employees Observed</div></th>";
		$quarterly .= "<th class='double'><div style='width:150px;' class='inner'>Percent Complete</div></th>";
		$quarterly .= "</tr>";

		if(!empty($directTeam)){
			foreach($directTeam as $key=>$value){
				if($_REQUEST["usid"] == $value["USID"]){
					$border = 'border-top:1px solid black;';
				}else {
					$border = '';
				}

				$quarterly .= "<tr class = '" . ($x++ % 2 == 0 ? 'even' : 'odd') . "' {$border}>";
				$quarterly .= "<td style='width:100px; {$border}' id='{$value["USID"]}_observedquarter'></td>";
				$quarterly .= "<td style='width:300px; text-align: left; {$border}'>" .
					($value["USID"] != $_REQUEST["usid"] ? "<a href='forms/epop.php?usid={$value["USID"]}{$delegate}'><img src='src/img/newdocument.png' alt='Observe {$value["NAME"]}'/></a> " : "") .
					(($value["REPORTS"] == 0 || $value["USID"] == $_REQUEST["usid"]) ? $value["NAME"] :
					"<a href = '#' onclick = 'sot.home.initiate(\"{$value["USID"]}\");'>{$value["NAME"]}</a>")
				. "</td>";
				$quarterly .= "<td style='width:150px; {$border}'>{$value["TEAM"]}</td>";
				$quarterly .= "<td style='width:150px; {$border}' id='{$value["USID"]}_completedquarter'>" . ($value["TEAM"] == 0 ? "--" : "") ."</td>";
				$quarterly .= "<td style='width:150px; {$border}' id='{$value["USID"]}_qc'>" . ($value["TEAM"] == 0 ? "--" : "") ."</td>";
				$quarterly .= "</tr>";

			}
		} else {
			$quarterly .= "<tr><td colspan='5'>There are no metrics to display.</td></tr>";
		}
		$quarterly .= "</table>";

		die("
			<div id='weekly'>{$weekly}</div>
			<div id='quarterly'>{$quarterly}</div>
			<div id='io'>{$io}</div>
		");
	}

	static function weeklyMetric(){
		$directTeam = self::reportsGenerated($_REQUEST["usid"]);

		if(empty($directTeam)){ self::printer(array()); return;}

		$stmt_w_o = self::sot()->parse("				
			SELECT 	MAX(EPOP_ID) AS OBSERVED 
			FROM 	EPOP_OBSERVATIONS
					WHERE EMPLOYEE = :usid
					AND OBSERVED_DATE BETWEEN TO_DATE(:startDate, 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE(:endDate, 'MM/DD/YYYY HH24:MI:SS')		
		");


		$stmt_w_c = self::sot()->parse("				
			SELECT 	MAX(EPOP_ID) AS COMPLETED 
			FROM 	EPOP_OBSERVATIONS
					WHERE OBSERVATION_CREDIT = :usid
					AND OBSERVED_DATE BETWEEN TO_DATE(:startDate, 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE(:endDate, 'MM/DD/YYYY HH24:MI:SS')
		");

		$stmt_excluded = self::sot()->parse("				
			SELECT 	1
			FROM 	EXCLUSIONS
					WHERE USID = :usid
					AND (START_DT <= TO_DATE(:startDate, 'MM/DD/YYYY HH24:MI:SS') OR START_DT IS NULL)
					AND (END_DT >= TO_DATE(:endDate, 'MM/DD/YYYY HH24:MI:SS') OR END_DT IS NULL)
					
		");

		foreach($directTeam as $key=>$value){

			//-----------------------------------------------
			//Check if employee is excluded
			self::sot()->bind($stmt_excluded, array(
				"usid" => $value["USID"],
				"startDate" => $_REQUEST["startDate"] . ' 00:00:00',
				"endDate" => $_REQUEST["endDate"] . ' 23:59:59'
			));
			$excluded = self::sot()->fetch($stmt_excluded, false) ? true : false;
			//-----------------------------------------------

			self::sot()->bind($stmt_w_o, array(
				"usid" => $value["USID"],
				"startDate" => $_REQUEST["startDate"] . ' 00:00:00',
				"endDate" => $_REQUEST["endDate"] . ' 23:59:59'
			));

			$row = self::sot()->fetch($stmt_w_o, false);
			$out[$value["USID"]]["observed"] = $row["OBSERVED"];

			self::sot()->bind($stmt_w_c, array(
				"usid" => $value["USID"],
				"startDate" => $_REQUEST["startDate"] . ' 00:00:00',
				"endDate" => $_REQUEST["endDate"] . ' 23:59:59'
			));
			$row = self::sot()->fetch($stmt_w_c, false);

			if($excluded) {
				$out[$value["USID"]]["completed"] = "Excluded";
			} else {
				$out[$value["USID"]]["completed"] = (empty($row["COMPLETED"]) ? ($value["LEADERS"] == 0 ? "--" : "") : $row["COMPLETED"]);
			}

			$out[$value["USID"]]["lc"] = self::lcCompletion($value["USID"], $directTeam[$value["USID"]]["LEADERS"]);

		}

		//print_r(self::sot()->error());
		$stmt_io = self::sot()->parse("
			WITH T AS (
				SELECT USID FROM EMPLOYEES WHERE PATH LIKE :usid
			)
			SELECT I.ITEM_CATEGORY, I.ITEM_TEXT, COUNT(*) AS CT 
			FROM EPOP_OBSERVATIONS O, 
				T,
				EPOP_ANSWERS A,
				EPOP_ITEMS I 
			WHERE T.USID = NVL(O.EMPLOYEE, O.COMPLETED_BY)
			AND A.ANSWER = 'IO'
			AND A.ITEM_NUM = I.ITEM_NUM
			AND A.EPOP_ID = O.EPOP_ID
			AND  OBSERVED_DATE BETWEEN TO_DATE(:startDate, 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE(:endDate, 'MM/DD/YYYY HH24:MI:SS')
			GROUP BY ITEM_CATEGORY, ITEM_TEXT
			ORDER BY COUNT(*) DESC
		");

		self::sot()->bind($stmt_io, array(
			"usid" => "%{$_REQUEST["usid"]}%",
			"startDate" => $_REQUEST["startDate"] . ' 00:00:00',
			"endDate" => $_REQUEST["endDate"] . ' 23:59:59'
		));

		$io = "<table class='tbl' id='io_tbl'>";
		$io .= "<tr><th colspan='3'><div class='inner_title'>Improvement Opportunities</div></th></tr>";
		$io .= "<tr>";
		$io .= "<th><div style='width:100px;' class='inner'>No.</div></th>";
		$io .= "<th><div style='width:600px;' class='inner'>Observation Item</div></th>";
		$io .= "<th><div style='width:100px;' class='inner'>IO Count</div></th>";
		$io .= "</tr>";

		$x = 0;
		$total = 0;
		$style = "style='text-align: left; width:600px;'";
		while($row = self::sot()->fetch($stmt_io)){

			if($x < 5){
				$display = "";
			} else {
				$display = "style='display: none;'";
			}

			$io .= "<tr class = '" . ($x++ % 2 == 0 ? 'even' : 'odd') . "' {$display}>";
			$io .= "<td>{$x}</td>";
			$io .= "<td {$style}>{$row["ITEM_TEXT"]}</td>";
			$io .= "<td>{$row["CT"]}</td>";
			$io .= "</tr>";

			$total += $row["CT"];
		}

		if($x == 0){
			$io .= "<tr><td colspan='3'>No improvement opportunites found for the current time frame ({$_REQUEST["startDate"]} - {$_REQUEST["endDate"]}).</td></tr>";
		} else if($x > 1){
			$border = "style='border: 1px solid black;'";
			$link = "<a href='#' onclick='sot.home.showAllIO(this);return false;'>{$total}</a>";

			$io .= "<tr {$border} >";
			$io .= "<td {$border} >--</td>";
			$io .= "<td {$style} {$border} >Grand Total Improvement Opportunities</td>";
			$io .= "<td {$border} >" . (($x - 1) > 5 ? $link : $total) ."</td>";
			$io .= "</tr>";
		}

		$end = (float) array_sum(explode(' ', microtime()));

		$run_time = "start: {$start} end: {$end} - " . (round($end - $start, 2)) . " seconds to calculate weekly metric.";
		$out["io"] = $io;
		self::printer($out);
	}

	static function quarterlyMetric(){
		//array("usid" => array("hasbeenobserved" => 1, "totalemployeesobserved" => 1));
		$directTeam = self::reportsGenerated($_REQUEST["usid"]);
		if(empty($directTeam)){ self::printer(array()); return;}

		$dates = self::quarterDates($_REQUEST["quarter"], $_REQUEST["year"]);

		$stmt_q_o = self::sot()->parse( "				
			SELECT 	MAX(EPOP_ID) AS OBSERVED 
			FROM 	EPOP_OBSERVATIONS
					WHERE EMPLOYEE = :usid
					AND OBSERVED_DATE BETWEEN TO_DATE(:startDate, 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE(:endDate, 'MM/DD/YYYY HH24:MI:SS')	
		");

		//CHANGE HERE
		$stmt_q_c = self::sot()->parse("
			WITH T AS (
					SELECT 	USID 
					FROM 	EMPLOYEES 
					START WITH SUPERVISOR = :usid 
					CONNECT BY PRIOR USID = SUPERVISOR
			)
			SELECT 	COUNT(DISTINCT EMPLOYEE) AS CT 
			FROM 	EPOP_OBSERVATIONS O, T 
					WHERE T.USID = O.EMPLOYEE
					AND OBSERVED_DATE BETWEEN TO_DATE(:startDate, 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE(:endDate, 'MM/DD/YYYY HH24:MI:SS')
		
		");

		foreach($directTeam as $key=>$value){
			self::sot()->bind($stmt_q_o, array(
				"usid" => $value["USID"],
				"startDate" => $dates["start"] . ' 00:00:00',
				"endDate" => $dates["end"] . ' 23:59:59'
			));

			$row = self::sot()->fetch($stmt_q_o, false);

			$out[$value["USID"]]["observed"] = $row["OBSERVED"];

			if($value["TEAM"] > 0){
				self::sot()->bind($stmt_q_c, array(
					"usid" => $value["USID"],
					"startDate" => $dates["start"] . ' 00:00:00',
					"endDate" => $dates["end"] . ' 23:59:59'
				));

				$row = self::sot()->fetch($stmt_q_c, false);
				$out[$value["USID"]]["totalemployeesobserved"] = $row["CT"];
				$out[$value["USID"]]["percentcomplete"] = @round(($row["CT"]/$value["TEAM"]) * 100, 2);
			} else {
				$out[$value["USID"]]["totalemployeesobserved"] = '--';
				$out[$value["USID"]]["percentcomplete"] = '--';
			}
		}
		self::printer($out);
	}

	static function getInfo(){
		$stid = self::sot()->parse("
			SELECT NAME, ORG_CODE, LOCATION FROM EMPLOYEES WHERE USID = :usid
		");

		self::sot()->bind($stid, array(
			":usid"	=> $_REQUEST["usid"]
		));

		$employee = self::sot()->fetch($stid);

		if(empty($employee)){
			$employee = @mcl_ldap::lookup($_REQUEST["usid"]);

			if(empty($employee["fname"])) {
				self::printer(array());
				return;
			}

			$org_desc = substr($employee["all"][0]["dtebusinessunitdeptidlongdesc"][0], 5);
			$loc = $employee["all"][0]["positionlocationlongdesc"][0];
			$name = $employee["fname"];

			$stid = self::sot()->parse("
				SELECT ORG_CODE FROM ORGANIZATIONS WHERE ORG_DESCRIPTION LIKE :org_desc
			");

			self::sot()->bind($stid, array(
				":org_desc"	=> "%{$org_desc}%"
			));

			$org = self::sot()->fetch($stid);
			$org = $org["ORG_CODE"];

		} else {
			$org = $employee["ORG_CODE"];
			$name = $employee["NAME"];
			$loc = $employee["LOCATION"];
		}
		self::printer(array(
			"name"	=> $name,
			"org"	=> $org,
			"loc"	=> $loc
		));
	}

	static function subItems(){
		$officefield = !empty($_REQUEST["officefield"]) ? $_REQUEST["officefield"] : "OFFICE";
		$org = $_REQUEST["org"];
		$itemnum = $_REQUEST["itemnum"];


		if($officefield == "OFFICE"){
			$key = "OFFICE";
		} else {
			$key = "FIELD";
		}

		$stmt = self::sot()->parse( "
			SELECT
				*
			FROM
				SWO_SUBITEMS S,
				SWO_ORG_SUBITEMS SO
			WHERE
				S.SUBITEM_NUM = SO.SUBITEM_NUM
				AND ORG_CODE = :org
				AND ACTIVE = 1
				AND PARENT_ITEM_NUM = :itemnum
				AND {$key} = :officefield
		");

		self::sot()->bind($stmt, array(
			":itemnum" => $itemnum,
			":org" => $org,
			":officefield" => 1
		));

		$out = array();
		while($row = self::sot()->fetch($stmt)){
			$out[] = $row;
		}

		self::printer($out);

	}

	static function addItems(){
		$lfc = !empty($_REQUEST["lfc"]) ? intval($_REQUEST["lfc"]) : "0";

		if($_REQUEST["category"] == 'qew' ) {
			$category = 'Qualified Electrical Worker';
		} else if($_REQUEST["category"] == 'pp' ) {
			$category = 'Paired Performance';
		} else {
			$category = $_REQUEST['category'];
		}

		//office only
		$sql = "
			SELECT
				*
			FROM
				EPOP_ITEMS
			WHERE
				ACTIVE = 1
				AND LOWER(ITEM_CATEGORY) = '" . strtolower($category) . "'
				" . ($lfc ? "AND LIFE_CRITICAL = 1" : "") . "
			ORDER BY
				ITEM_NUM";

		$out = array();
		while($row = self::sot()->fetch($sql)){
			$out[] = $row;
		}

		self::printer($out);
	}

	static function saveEPOP(){
		$usid_a = $_REQUEST["usid"];
		$name_a = $_REQUEST["name"];

		$user = auth::check();
		//anonymous without logging in has no oberved by or complete by
		//if user is logged in, obesrved by is empty but we still capture the completed by (this field is not shown in any reports currently)
		if(!empty($_REQUEST["observed_by"])){ //observed_by is the field that shows the user who is logged in their name, if they delete it it indicates they dont want to be tagged to observation hence anonymous
			$observation_credit = trim($_REQUEST["completed_by"]);
		} else {
			$observation_credit = "";
		}

		$stmt = self::sot()->parse("
			INSERT INTO EPOP_OBSERVATIONS(
				EPOP_ID,
				EMPLOYEE,
				NAME,
				SAPID,
				TITLE,
				LOCATION,
				SUPERVISOR,
				ORGANIZATION,
				ORG_CODE,
				OFFICE_FIELD,
				OBSERVATION_CREDIT,
				OBSERVED_DATE,
				COMPLETED_BY,
				COMPLETED_DATE,
				BEGIN_DATE,
				SWO,
				QEW,
				PP,
				COMMENTS,
				CONVERSATION_TOOK_PLACE,
				RECOGNIZED_GOOD_BEHAVIOR,
				LC_PILOT,
				PROCEDURE_FOR_WORK,
				PROCEDURE_FOR_WORK_DETAIL,
				LC_HEIGHTS,
				LC_VEHICLE,
				LC_LIFTING,
				LC_CONFINED,
				LC_TRENCHING,
				LC_HOT_WORK,
				LC_HAZ_ENERGY,
				LC_CONTRACTOR,
				LC_ANONYMOUS
			) VALUES (
				EPOP_UNIQUE.NEXTVAL,
				:employee,
				:name,
				:sapid,
				:title,
				:location,
				:supervisor,
				:organization,
				:org_code,
				:officefield,
				:observationcredit,
				TO_DATE(:observeddate, 'MM/DD/YYYY'),
				:completedby,
				SYSDATE,
				TO_DATE(:begindate, 'MM/DD/YYYY HH24:MI:SS'),
				:swo,
				:qew,
				:pp,
				:comments,
				:conversation,
				:good_behavior,
				:lc_pilot,
				:procedure_for_work,
				:procedure_for_work_detail,
				:lc_heights,
				:lc_vehicle,
				:lc_lifting,
				:lc_confined,
				:lc_trenching,
				:lc_hot_work,
				:lc_haz_energy,
				:lc_contractor,
				:lc_anonymous
			) RETURNING EPOP_ID INTO :epopid
		");
		$stmt_items = self::sot()->parse("
			INSERT INTO EPOP_ANSWERS (
				EPOP_ID,
				ITEM_NUM,
				ANSWER
			) VALUES(
				:epopid,
				:itemnum,
				:answer
			)
		");

		$stmt_comments = self::sot()->parse("
			INSERT INTO EPOP_COMMENTS (
				EPOP_ID,
				ITEM_NUM,
				COMMENTS
			) VALUES(
				:epopid,
				:itemnum,
				:comments
			)
		");

		$inserted = 0;
		foreach($usid_a as $i=>$value){
			$usid = $usid_a[$i];
			$name= $name_a[$i];


			$employee = array();
			if(!empty($usid)){
				$employee = @mcl_Ldap::lookup($usid);
			}

            $contractor = ($_REQUEST["personType"] == "2") ? 1 : 0;
            $anonymous = ($_REQUEST["personType"] == "1") ? 1 : 0;

			$epop = self::sot()->bind($stmt, array(
					":employee"				=> strtolower($usid),
					":name"					=> $name,
					":sapid"				=> $employee["all"][0]["employeenumber"][0],
					":title"				=> $employee["title"],
					":location"				=> $_REQUEST["location"],
					":supervisor"			=> $employee["manager"],
					":organization"			=> $employee["all"][0]["dtebusinessunitdeptidlongdesc"][0],
					":officefield"			=> $_REQUEST["office_field"],
					":org_code"				=> $_REQUEST["org"],
					":observationcredit" 	=> strtolower($observation_credit),
					":observeddate"			=> $_REQUEST["observed_date"],
					":completedby"			=> !trim(empty($_REQUEST["completed_by"])) ? trim(strtolower($_REQUEST["completed_by"])) : strtolower($user["usid"]),
					":swo"					=> isset($_REQUEST["swo"]) ? 1 : 0,
					":qew"					=> isset($_REQUEST["qew"]) || $_REQUEST['qew_add'] == 1 ? 1 : 0,
					":pp"					=> isset($_REQUEST["pp"]) ? 1 : 0,
					":comments"				=> $_REQUEST["comments"],
					":epopid"				=> array(0, SQLT_INT),
					":begindate"			=> $_REQUEST["begin_date"],
					":conversation"			=> $_REQUEST["conversation"] ? 1 : 0,
					":good_behavior"		=> $_REQUEST["good_behavior"] ? 1 : 0,
					":lc_pilot"		=> $_REQUEST["lc_pilot"] ? 1 : 0,
					":procedure_for_work"		=> $_REQUEST["procedure_for_work"] ? 1 : 0,
					":procedure_for_work_detail"		=> trim($_REQUEST["procedure_for_work_detail"]),
					":lc_heights"		=> $_REQUEST["LC_HEIGHTS"] ? 1 : 0,
					":lc_vehicle"		=> $_REQUEST["LC_VEHICLE"] ? 1 : 0,
					":lc_lifting"		=> $_REQUEST["LC_LIFTING"] ? 1 : 0,
					":lc_confined"		=> $_REQUEST["LC_CONFINED"] ? 1 : 0,
					":lc_trenching"		=> $_REQUEST["LC_TRENCHING"] ? 1 : 0,
					":lc_hot_work"		=> $_REQUEST["LC_HOT_WORK"] ? 1 : 0,
					":lc_haz_energy"		=> $_REQUEST["LC_HAZ_ENERGY"] ? 1 : 0,
					":lc_contractor"		=> $contractor,
					":lc_anonymous"		=> $anonymous
			), true, true);


			$epopid = $epop[":epopid"];
			var_dump($epop);
			if(!empty($epopid)){
			 foreach($_REQUEST as $key=>$value){
					if(is_numeric($key)){
						self::sot()->bind($stmt_items, array(
							":itemnum"	=> $key,
							":answer"	=> $value,
							":epopid"	=> $epopid
						));

						if(isset($_REQUEST["{$key}_comments"]) && !empty($_REQUEST["{$key}_comments"])){
							self::sot()->bind($stmt_comments, array(
								":itemnum"	=> $key,
								":comments"	=> $_REQUEST["{$key}_comments"],
								":epopid"	=> array($epopid, SQLT_INT)
							));
						}
					}
				}
				self::sot()->commit();
				$inserted++;

				if($_REQUEST["f"] != "saveCrewEPOP"){
					self::savePhotos('EPOP', $epopid);
				}

				self::epopNotification($epopid);
			}
		}

		if($_REQUEST["f"] == "saveEPOP"){
			if($inserted == 0) {
				$err = self::sot()->error();
				die("<script type = 'text/javascript'>
						window.top.window.sot.tools.cover(false);
						alert('Unable to save observation. {$err["message"]}');
				</script>");
			}

			die("<script type = 'text/javascript'>
					window.top.window.location=\"../../" . ($_REQUEST['mobile'] ? "mobile/" : "") . "index.php?s=1" . (empty($_REQUEST["delegator"]) || $_REQUEST["delegator"] == 0 ? "" : "&delegate={$_REQUEST["delegate"]}") . "\";
			</script>");
		}
	}

	static function epopNotification($id) {
		$sql = "
			SELECT
				EMPLOYEE AS OBSERVED,
				OBSERVATION_CREDIT AS OBSERVER,
				SWO,
				QEW
			FROM
				EPOP_OBSERVATIONS
			WHERE
				EPOP_ID = {$id}
				AND EMPLOYEE IS NOT NULL
				AND OBSERVATION_CREDIT IS NOT NULL
		";

		if($row = self::sot()->fetch($sql)) {
			$observed = @mcl_Ldap::lookup($row['OBSERVED']);
			$observer = @mcl_Ldap::lookup($row['OBSERVER']);
			$swo = $row['SWO'];
			$qew = $row['QEW'];

			$type = ($swo ? "Safe Worker" : "") . ($swo && $qew ? " and " : "") . ($qew ? "Qualified Electrical Worker" : "");

			if($observer) {
				$message = "Thank you for recently performing a {$type} observation on {$observed["fname"]} using SOTeria. You can retrieve this observation anytime using your 'My Activity' report.";
				self::epopNotificationHelper($observer, $message, $id);
			}

			$manager = @mcl_Ldap::lookup($observer['manager']);
			if($manager) {
				$message = "This message is to notify you of a recent {$type} observation completed by {$observer['fname']} using SOTeria.";
				self::epopNotificationHelper($manager, $message, $id);
			}

			if($observed) {
				$message = "A {$type} observation has been recently conducted on you using SOTeria by {$observer["fname"]}.";
				self::epopNotificationHelper($observed, $message, $id);
			}
		}
	}

	static function epopNotificationHelper($employee, $message, $epopId) {
		$from = "Soteria <SOTERIA@dteenergy.com>";

		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= "From: {$from}" . "\r\n";
		$headers .= "Reply-To: {$from}" . "\r\n";

		$email = "{$employee["fname"]} <{$employee['email']}>";
		if(!$email) {
			return false;
		}

        $subject = 'SOTeria Observation Notification';

		$sName = $_SERVER['SERVER_NAME'];
		$dev =  (($sName == 'lnx829') || ($sName == 'lnx829.dteco.com')) ? true : false;
		if($dev) {
			$email = 'SOTERIA <SOTERIA@dteenergy.com>';
            $subject = '***TEST*** :: ' . $subject;
		}

		$test = (($sName == 'soteriatest')
            || ($sName == 'lnx830')
            || ($sName == 'lnx830.dteco.com')
            || ($sName == 'soteriatest.dteco.com')) ? true : false;

        if ($test){
            $subject = '***TEST*** :: ' . $subject;
        }

		$url = 'http' . ($_SERVER['SERVER_PORT'] == '443' ? 's' : '') . '://' . $_SERVER['HTTP_HOST'];
		$fullName = $employee['fname'];
		$splitName = explode(" ", $fullName);
		$firstName = $splitName[0];

		$message = "<div style='font-size: 12px; font-family: arial'>
			Hello {$firstName}, <br/><br/>
			{$message}<br/><br/>
			<a href='{$url}/forms/viewEpop.php?epopid={$epopId}'>Click here</a> to view this observation.<br/></br>
			Be Safe!
		</div>";

		mail($email, $subject, $message, $headers);
	}

	static function doubleQuote($str) {
		$unsafeChars = array("'", '"');
		$repChars = array("''", '""');
		return str_replace($unsafeChars, $repChars, $str);
	}

	static function saveHP(){
		$form = json_decode($_REQUEST['inputObjs'], true);

		$update = ($form['header_info']['hp_id'] != 0) ? true : false;

		if($update) {
			$hp_id = $form['header_info']['hp_id'];
			$updateStmt = self::sot()->parse("
				UPDATE HP_OBSERVATIONS
				SET
				    ORGANIZATION = :organization,
				    ORG_CODE = :org_code,
				    COMPLETED_DATE = SYSDATE,
				    INCIDENT_DATE = TO_DATE(:incident_date, 'MM/DD/YYYY'),
				    COMMENTS = :comments,
				    EVENT_TYPE = :event_type,
				    COMPLETED = :completed,
				    INCIDENT_TYPE = :incident_type,
				    CATEGORY = :category
				WHERE HP_ID = :hp_id
			");
			$hp = self::sot()->bind($updateStmt, array(
				":organization"			=> $form['header_info']['org_name'],
				":org_code"				=> $form['header_info']['org_code'],
				":incident_date"		=> $form['header_info']['incident_date'],
				":comments"				=> $form['header_info']["comments"],
				":event_type"			=> $form['header_info']["event_type"],
				":completed"            => $form['header_info']['completed'],
				":incident_type"        => $form['header_info']['incident_type'],
				":category"             => $form['header_info']['category'],
				":hp_id"				=> $hp_id
			));

			$clear_stmt = self::sot()->parse("
				DELETE FROM HP_ANSWERS
				WHERE HP_ID = :hp_id
			");
			self::sot()->bind($clear_stmt, array(":hp_id"=>$hp_id));
		}
		else {
			$user = auth::check();
			$employee = mcl_Ldap::lookup($user['usid']);
			$manager = $employee['manager'];
			$leafusid = $user["usid"];
			$loop = true;
			$foundDirector = false;
			$director = '';
			do {
				$leaf = @mcl_Ldap::lookup($leafusid);
				$isDirector = strpos(strtolower($leaf["title"]), "director");

				if($isDirector || $isDirector === 0) {
					$loop = false;
					$director = $leaf['username'];
					$foundDirector = true;
				}

				if(!$leaf["manager"]) {	$loop = false; }
				$leafusid = $leaf["manager"];

			} while($loop);
			if(!$foundDirector) {
				$director = @mcl_Ldap::lookup($manager);
				$director = $directory['manager'];
			}
			$newStmt = self::sot()->parse("
				INSERT INTO HP_OBSERVATIONS (
					HP_ID,
					ORGANIZATION,
					ORG_CODE,
					COMPLETED_BY,
					COMPLETED_DATE,
					INCIDENT_DATE,
					COMMENTS,
					COMPLETED_BY_SUPERVISOR,
					COMPLETED_BY_DIRECTOR,
					EVENT_TYPE,
					COMPLETED,
					INCIDENT_TYPE,
					CATEGORY
				) VALUES (
					HP_SEQ.NEXTVAL,
					:organization,
					:org_code,
					:completed_by,
					SYSDATE,
					TO_DATE(:incident_date, 'MM/DD/YYYY'),
					:comments,
					:supervisor,
					:director,
					:event_type,
					:completed,
					:incident_type,
					:category
				) RETURNING HP_ID INTO :hp_id
			");

			$hp = self::sot()->bind($newStmt, array(
				":organization"			=> $form['header_info']['org_name'],
				":org_code"				=> $form['header_info']['org_code'],
				":completed_by"			=> strtolower($user["usid"]),
				":incident_date"		=> $form['header_info']['incident_date'],
				":comments"				=> $form['header_info']["comments"],
				":supervisor"			=> $manager,
				":director"				=> $director,
				":event_type"			=> $form['header_info']["event_type"],
				":completed"            => $form['header_info']['completed'],
				":incident_type"        => $form['header_info']['incident_type'],
				":category"             => $form['header_info']['category'],
				":hp_id"				=> array(0, SQLT_INT)
			), true, true);

			$hp_id = $hp[":hp_id"];
		}

		self::sot()->commit();

		$err = self::sot()->error();
		if($err) {
			die("<script type = 'text/javascript'>
			    window.top.window.sot.tools.cover(false);
		    	alert('Failed to save (error occured while creating form). {$err['message']}');
		   	</script>");
			return;
		}

		$stmt_items = self::sot()->parse("
			INSERT INTO HP_ANSWERS (
				HP_ID,
				ITEM_NUM,
				SUBITEM_NUM,
				FREEFORM,
				ANSWER
			) VALUES(
				:hp_id,
				:item_num,
				:subitem_num,
				:freeform,
				:answer
			)
		");

		$twin_items = self::sot()->parse("
			MERGE INTO TWIN_ANSWERS USING DUAL ON (OBSERVATION_ID = {$hp_id}
					AND OBSERVATION_FORM = 'HP'
					AND ITEM_NUM = :item_num)
			WHEN MATCHED THEN UPDATE  
				SET ANSWER = :answer,
					EXPLANATION = :explanation
				WHERE OBSERVATION_ID = {$hp_id}
					AND OBSERVATION_FORM = 'HP'
					AND ITEM_NUM = :item_num
			WHEN NOT MATCHED THEN
				INSERT (
					OBSERVATION_FORM,
					OBSERVATION_ID,
					ITEM_NUM,
					ANSWER,
					EXPLANATION
				) VALUES(
					'HP',
					:hp_id,
					:item_num,
					:answer,
					:explanation
				)
		");

		$inserted = 0;
		if(!empty($hp_id)) {
			foreach($form['answers'] as $item) {
				$item_num = $item['item_id'];
				$subitem_num = $item['subitem_id'];
				$freeform = $item['freeform'];
				$answer = $item['val'];
				self::sot()->bind($stmt_items, array(
				    ':hp_id' => $hp_id,
					':item_num' => $item_num,
					':subitem_num' => $subitem_num,
					':freeform' => $freeform,
					':answer' => $answer
				));

				self::sot()->commit();

				$err = self::sot()->error();
				if($err) {
					die("<script type = 'text/javascript'>
					    window.top.window.sot.tools.cover(false);
				    	alert('Failed to save (answers). {$err['message']}');
				   	</script>");
					return;
				}
				$inserted++;
			}
			$err = self::sot()->error();
			if($err || $inserted==0) {
					die("<script type = 'text/javascript'>
					    window.top.window.sot.tools.cover(false);
				    	alert('Failed to save (twin). {$err['message']}');
				   	</script>");
				return;
			}
			foreach($form['twin'] as $key => $value) {
				self::sot()->bind($twin_items, array(
					":hp_id" => $hp_id,
					":item_num" => $key,
					":answer" => $value['answer'] == 1 ? "YES" : "NO",
					":explanation" => $value['comment']
				));

				self::sot()->commit();

				$err = self::sot()->error();
				if($err) {
					die("<script type = 'text/javascript'>
					    	window.top.window.sot.tools.cover(false);
					    	alert('Failed to save. {$err['message']}');
					   	</script>");
					return;
				}
				$inserted++;
			}
		}

		$err = self::sot()->error();
		if($err || $inserted==0) {
			die("<script type = 'text/javascript'>
			    	window.top.window.sot.tools.cover(false);
			    	alert('Failed to save. {$err['message']}');
			   	</script>");
			return;
		}
		self::savePhotos('HP', $hp_id);
		self::hpWF($hp_id, $update);
		die("<script type = 'text/javascript'>
		    	window.top.window.sot.tools.cover(false);
				window.top.window.location=\"../../" . ($_REQUEST['mobile'] ? "mobile/" : "") . "index.php?h=1\";
			</script>");
	}

	static function saveRedtag(){
		$update = false;
		if(!empty($_REQUEST["id"])){
			$rtaid = $_REQUEST["id"];
			$update = true;
		}

		$stmt_update = self::sot()->parse("
			UPDATE RED_TAG_AUDITS SET 
				LOCATION = :location,
				ORG = :org,
				PROTECTION_LEADER = :leader_name,
				RSD = :rsd,
				OPERATION_AUTHORITY = :operation_authority,
				AUDITORS = :auditors_name,
				LOCATION_DESC_EQP_SHUTDOWN = :loc_desc_eqp_shutdown,
				NATURE_OF_WORK = :nature_of_work,
				AUDIT_DATE = TO_DATE(:audit_date, 'MM/DD/YYYY'),
				CREDIT_MONTH = :credit_month,
				COMPLETED_BY = 	:completed_by,
				COMPLETED_DATE = SYSDATE,
				EMPLOYEES_INTERVIEW = :employees_interview,
				EMPLOYEES_SUPERVISORS = :employees_sup,
				QUESTIONS_COMMENTS = :questions_comments,
				FINDINGS = 	:findings,
				RECOMMENDATIONS = :recommendations,
				FOLLOWUP_RESOLUTION = :followup_resolution,
				AUDIT_MONTHLY = :audit_monthly,
				AUDIT_QUARTERLY = :audit_quarterly,
				AUDIT_FOLLOWUP_RECHECK = :audit_followup_recheck,
				AUDIT_ON_SITE = :audit_on_site,
				AUDIT_PROTECTION_AUTHORITY = :audit_protection_authority,
				COMPLETE = :complete
			WHERE RTA_ID = :rtaid
			");

		$stmt_insert = self::sot()->parse("
			INSERT INTO RED_TAG_AUDITS (
				RTA_ID,
				LOCATION,
				ORG,
				PROTECTION_LEADER,
				RSD, 
				OPERATION_AUTHORITY,
				AUDITORS,
				LOCATION_DESC_EQP_SHUTDOWN,
				NATURE_OF_WORK, 
				AUDIT_DATE,
				CREDIT_MONTH, 
				COMPLETED_BY,
				COMPLETED_DATE,
				EMPLOYEES_INTERVIEW,
				EMPLOYEES_SUPERVISORS,
				QUESTIONS_COMMENTS,
				FINDINGS,
				RECOMMENDATIONS,
				FOLLOWUP_RESOLUTION,
				AUDIT_MONTHLY,
				AUDIT_QUARTERLY, 
				AUDIT_FOLLOWUP_RECHECK, 
				AUDIT_ON_SITE, 
				AUDIT_PROTECTION_AUTHORITY,
				COMPLETE
			) VALUES (
				RTA_UNIQUE.NEXTVAL,
				:location,
				:org,
				:leader_name,
				:rsd,
				:operation_authority,
				:auditors_name,
				:loc_desc_eqp_shutdown,
				:nature_of_work,
				TO_DATE(:audit_date, 'MM/DD/YYYY'),
				:credit_month,
				:completed_by,
				SYSDATE,
				:employees_interview,
				:employees_sup,
				:questions_comments,
				:findings,
				:recommendations,
				:followup_resolution,
				:audit_monthly,
				:audit_quarterly,
				:audit_followup_recheck,
				:audit_on_site,
				:audit_protection_authority,
				:complete
			) RETURNING RTA_ID INTO :rtaid
		");

		$binds = array(
			":location"						=> $_REQUEST["location"] ? $_REQUEST["location"] : "",
			":org"							=> $_REQUEST["org"],
			":leader_name"					=> $_REQUEST["protection_leader"],
			":rsd"							=> is_numeric($_REQUEST["rsd"]) ? $_REQUEST["rsd"] : NULL,
			":operation_authority"			=> $_REQUEST["operating_authority"],
			":auditors_name"				=> $_REQUEST["auditor"],
			":loc_desc_eqp_shutdown"		=> $_REQUEST["loc_desc_equip"],
			":nature_of_work"				=> $_REQUEST["nature_work"],
			":audit_date"					=> $_REQUEST["date"],
			":credit_month"					=> $_REQUEST["credit_month"],
			":completed_by"					=> auth::check('usid'),
			":employees_interview"			=> $_REQUEST["employees_interviewed"],
			":employees_sup"				=> $_REQUEST["employees_supervisors"],
			":questions_comments"			=> $_REQUEST["questions_comments"],
			":findings"						=> $_REQUEST["findings"],
			":recommendations"				=> $_REQUEST["recommendations"],
			":followup_resolution"			=> $_REQUEST["followup_resolution"],
			":audit_monthly"				=> (isset($_POST["audit_monthly"]) && $_POST["audit_monthly"] == "on" ? 1 : 0) ,
			":audit_quarterly"				=> (isset($_POST["audit_quarterly"]) && $_POST["audit_quarterly"] == "on" ? 1 : 0),
			":audit_followup_recheck"		=> (isset($_POST["audit_followup_recheck"]) && $_POST["audit_followup_recheck"] == "on" ? 1 : 0) ,
			":audit_on_site"				=> (isset($_POST["audit_onsite"]) && $_POST["audit_onsite"] == "on" ? 1 : 0) ,
			":audit_protection_authority"	=> (isset($_POST["audit_protection_authority"]) && $_POST["audit_protection_authority"] == "on" ? 1 : 0),
			":complete"						=> isset($_REQUEST["save"]) ? 0 : 1
		);

		if(!$update){
			$binds[":rtaid"] = array(0, SQLT_INT);
			$rta = self::sot()->bind($stmt_insert, $binds, true, true);
			$rtaid = $rta[":rtaid"];
		} else {
			$binds[":rtaid"] = $rtaid;
			self::sot()->bind($stmt_update, $binds);
		}

		$stmt_items_insert = self::sot()->parse("
			INSERT INTO RED_TAG_ANSWERS (
				RTA_ID,
				ITEM_NUM,
				ANSWER
			) VALUES(
				:rtaid,
				:itemnum,
				:answer
			)
		");

		$stmt_items_update = self::sot()->parse("
			UPDATE RED_TAG_ANSWERS 
				SET ANSWER = :answer
				WHERE RTA_ID = :rtaid
					AND ITEM_NUM = :itemnum
		");

		$stmt_items = self::sot()->parse("
			MERGE INTO RED_TAG_ANSWERS USING DUAL ON (RTA_ID = :rtaid
					AND ITEM_NUM = :itemnum)
			WHEN MATCHED THEN UPDATE  
				SET ANSWER = :answer
				WHERE RTA_ID = :rtaid
					AND ITEM_NUM = :itemnum
			WHEN NOT MATCHED THEN
				INSERT (
					RTA_ID,
					ITEM_NUM,
					ANSWER
				) VALUES(
					:rtaid,
					:itemnum,
					:answer
				)
		");

		if(!empty($rtaid)){
			foreach($_REQUEST as $key=>$value){
				if(is_numeric($key)){
					if(!self::sot()->bind($stmt_items, array(
						":itemnum"	=> $key,
						":answer"	=> $value,
						":rtaid"	=> $rtaid
					))){

					}
				}
			}
			self::sot()->commit();
		}

		if(is_numeric($_REQUEST["a_count"]) && $_REQUEST["a_count"] > 0){
			$stid_a = self::sot()->parse("
				INSERT INTO RED_TAG_ATTACHMENTS
				VALUES (
					:rtaid,
					RED_TAG_ATTACH_UNIQUE.NEXTVAL,
					:a_name,
					:extension,
					SYSDATE,
					:createdby,
					:contents
				)
			");

			for($x = 0; $x < $_REQUEST["a_count"]; $x++){
				if(isset($_REQUEST["aid_{$x}"]) && !empty($_REQUEST["aid_{$x}"]) && $_REQUEST["deleted_{$x}"] == "1"){
					//delete
					$d = self::sot()->query("
						DELETE FROM RED_TAG_ATTACHMENTS WHERE ATTACHMENT_ID  = " . $_REQUEST["aid_{$x}"] . "
					");
				} else if($_REQUEST["deleted_{$x}"] == "0" && !empty($_FILES["file_{$x}"])){

					$file = basename($_FILES["file_{$x}"]["name"]);

					$extension = pathinfo($file, PATHINFO_EXTENSION);
					$filename = pathinfo($file, PATHINFO_FILENAME);
					$a_name = empty($_REQUEST["a_name_{$x}"]) ? $filename : $_REQUEST["a_name_{$x}"];
					$contents = urlencode(@file_get_contents($_FILES["file_{$x}"]["tmp_name"]));

					if(empty($contents)) {
						$contents = "Error uploading photo: " . $_FILES["file_{$x}"]["error"];
					}

					$b = self::sot()->bind($stid_a, array(
						":rtaid"		=> $rtaid,
						":a_name"		=> $a_name,
						":extension"	=> $extension,
						":createdby"	=> auth::check("usid"),
						":contents"		=> $contents
					));
				}
			}

			self::sot()->commit();
		}

		if(empty($rtaid)) {
			$err = self::sot()->error();
			die("<script type = 'text/javascript'>
					window.top.window.sot.tools.cover(false);
					alert('Unable to save audit. {$err["message"]}');
			</script>");
		}

		$returnto = "index";
		if(isset($_REQUEST["returnto"]) && !$_GET["mobile"]){
			$returnto = $_REQUEST["returnto"];
		}

		//only saving, not complete, reload page with current form
		if(isset($_REQUEST["save"])){
			die("<script type = 'text/javascript'>
				var url = window.top.window.location.toString();
				if(url.indexOf(\"id\") == -1){
					if(url.indexOf(\"?\") == -1){
						url += '?id={$rtaid}';
					} else {
						url += '&id={$rtaid}';
					}
				}
	
				window.top.window.sot.tools.cover(false);
				window.top.window.location=url;
			</script>");
		}

		die("<script type = 'text/javascript'>
				window.top.window.sot.tools.cover(false);
				window.top.window.location=\"../../" . ($_GET['mobile'] ? "mobile/" : "") . "{$returnto}.php?r=1" . (empty($_REQUEST["delegator"]) || $_REQUEST["delegator"] == 0 ? "" : "&delegate={$_REQUEST["delegate"]}") . "\";
		</script>");
	}

	static function getLocations_redtag(){
		if($_REQUEST["org"] == "Engineering") {
			$where = "ORG IN('Primary', 'PERT', 'Cable Test')";
		} else if($_REQUEST["org"] == "Nuclear Generation") {
			$where = "ORG IN('E. Fermi 2')";
		} else if($_REQUEST["org"] == "System Operations") {
			$where = "ORG IN('Distribution', 'Tranmission/Sub-Transmission')";
		} else {
			$where = "ORG = '{$_REQUEST["org"]}'";
		}

		$sql = "
			SELECT DISTINCT LOCATION 
				FROM RED_TAG_AUDITS_LOCATIONS
				WHERE {$where}
			ORDER BY LOCATION
		";


		$out = array();
		while($row = self::sot()->fetch($sql)){
			$out[] = $row;
		}

		self::printer($out);
	}

	static function getLastAudit(){
		$row = false;
		$sql = "
				SELECT LOWER(TO_CHAR(TO_DATE(CREDIT_MONTH, 'MM'), 'MONTH')) AS MONTH, TO_CHAR(AUDIT_DATE, 'YYYY') AS YEAR FROM
					RED_TAG_AUDITS
					WHERE LOCATION = '{$_REQUEST["location"]}'
						AND ORG =  '{$_REQUEST["org"]}'
						AND CREDIT_MONTH = {$_REQUEST["credit_month"]}
						AND ROWNUM = 1
						AND COMPLETE = 1
					ORDER BY AUDIT_DATE DESC
			";

		if(!$row = self::sot()->fetch($sql)){
			$sql = "
				SELECT LOWER(TO_CHAR(TO_DATE(CREDIT_MONTH, 'MM'), 'MONTH')) AS MONTH, TO_CHAR(AUDIT_DATE, 'YYYY') AS YEAR FROM
					RED_TAG_AUDITS
					WHERE LOCATION = '{$_REQUEST["location"]}'
						AND ORG =  '{$_REQUEST["org"]}'
						AND COMPLETE = 1
					ORDER BY AUDIT_DATE DESC
			";
			$row = self::sot()->fetch($sql);
		}


		if($row){
			$row["MONTH"] = ucfirst($row["MONTH"]);
		}
		self::printer($row);
	}

	static function subItemsCrew() {

		$org = 'DIST OPS';
		$key = 'FIELD';
		$itemnum = $_REQUEST["itemnum"];

		$sql = "
			SELECT 	* 
			FROM 	SWO_SUBITEMS S, 
					SWO_ORG_SUBITEMS SO
			WHERE 	S.SUBITEM_NUM = SO.SUBITEM_NUM
					AND ORG_CODE = 'DIST OPS'
					AND ACTIVE = 1
					AND PARENT_ITEM_NUM = {$itemnum}
					AND {$key} = 1
		";

		$out = array();
		while($row = self::sot()->fetch($sql)){
			$out[] = $row;
		}

		self::printer($out);
	}

	static function saveCrewEpop(){
		$error = false;
		if($_REQUEST["observe_crew"] == "on") {
			$stmt_items = self::sot()->parse("
				INSERT INTO EPOP_ANSWERS (
					EPOP_ID,
					ITEM_NUM,
					ANSWER
				) VALUES(
					:epopid,
					:itemnum,
					:answer
				)
			");

			$stmt_comments = self::sot()->parse("
				INSERT INTO EPOP_COMMENTS (
					EPOP_ID,
					ITEM_NUM,
					COMMENTS
				) VALUES(
					:epopid,
					:itemnum,
					:comments
				)
			");

			$stmt = self::sot()->parse("
				INSERT INTO EPOP_CREW_OBSERVATIONS (
					EPOP_ID,
					SUPERVISOR,
					LEADER,
					CREW,
					OHUG,
					DATE_VISITED,
					TIME_VISITED,
					WORK_LOCATION,
					NOTES,
					COMPLETED_BY,
					COMPLETED_DATE,
					COMMENTS,
					SC,
					CONVERSATION_TOOK_PLACE,
					RECOGNIZED_GOOD_BEHAVIOR
				) VALUES (
					EPOP_UNIQUE.NEXTVAL,
					:sup,
					:leader,
					:crew,
					:ohug,
					:date_visited,
					:time_visited,
					:work_location,
					:notes,
					:completedby,
					SYSDATE,
					:comments,
					:sc,
					:conversation,
					:good_behavior
				) RETURNING EPOP_ID INTO :epopid
			");

			$usid = auth::check('usid');
			$epop = self::sot()->bind($stmt, array(
				":sup"					=> strtolower($usid),
				":leader"				=> $_REQUEST["leader"],
				":crew"					=> $_REQUEST["crew"],
				":ohug"					=> $_REQUEST["ohug"],
				":sc"					=> $_REQUEST["sc"],
				":date_visited"			=> $_REQUEST["observed_date"],
				":time_visited"			=> $_REQUEST["time"],
				":work_location"		=> $_REQUEST["location"],
				":notes"				=> $_REQUEST["notes"],
				":completedby"			=> strtolower($usid),
				":comments"				=> $_REQUEST["comments"],
                ":conversation"         => $_REQUEST["conversation"],
                ":good_behavior"        => $_REQUEST["good_behavior"],
				":epopid"				=> array(0, SQLT_INT)
			), true, true);

			$epopid = $epop[":epopid"];
			if(!empty($epopid)){
				foreach($_REQUEST as $key=>$value){
					if(is_numeric($key)){
						self::sot()->bind($stmt_items, array(
							":itemnum"	=> $key,
							":answer"	=> $value,
							":epopid"	=> $epopid
						));

						if(isset($_REQUEST["{$key}_comments"]) && !empty($_REQUEST["{$key}_comments"])){
							self::sot()->bind($stmt_comments, array(
								":itemnum"	=> $key,
								":comments"	=> $_REQUEST["{$key}_comments"],
								":epopid"	=> array($epopid, SQLT_INT)
							));
						}
					}
				}
				self::sot()->commit();
			} else {
				$error = true;
			}
		}

		self::saveEpop();

		if($error == false){
			die("<script type = 'text/javascript'>
				window.top.window.sot.tools.cover(false);
				if(confirm('Your observation has been successfully saved. Click OK to go back to the crew roster or click Cancel to remain on this page.')){
					window.top.window.location = '/so/index.php';
				}
			</script>");
		} else {
			$err = self::sot()->error();
			die("<script type = 'text/javascript'>
					window.top.window.sot.tools.cover(false);
					alert('Unable to save observation. {$err["message"]}');
			</script>");
		}
	}

	static function savePhotos($form, $id) {

		if(is_numeric($_REQUEST["p_count"]) && $_REQUEST["p_count"] > 0){
			$stid_a = self::sot()->parse("
				INSERT INTO PHOTOS
				VALUES (
					'{$form}',
					'{$id}',
					PHOTO_UNIQUE.NEXTVAL,
					:description,
					:extension,
					SYSDATE,
					:createdby,
					:contents
				)
			");

			for($x = 0; $x < $_REQUEST["p_count"]; $x++){
				if(isset($_REQUEST["pid_{$x}"]) && !empty($_REQUEST["pid_{$x}"]) && $_REQUEST["deleted_{$x}"] == "1"){
					//delete
					$d = self::sot()->query("
						DELETE FROM PHOTOS WHERE PHOTO_ID  = " . $_REQUEST["pid_{$x}"] . "
					");
				} else if($_REQUEST["deleted_{$x}"] == "0" && !empty($_FILES["file_{$x}"])){

					$file = basename($_FILES["file_{$x}"]["name"]);

					$extension = pathinfo($file, PATHINFO_EXTENSION);
					$filename = pathinfo($file, PATHINFO_FILENAME);
					$p_desc = empty($_REQUEST["p_desc_{$x}"]) ? $filename : $_REQUEST["p_desc_{$x}"];
					$contents = urlencode(@file_get_contents($_FILES["file_{$x}"]["tmp_name"]));
					if(empty($contents)) {
						$contents = "Error uploading photo: " . $_FILES["file_{$x}"]["error"];
					}

					if(!$b = self::sot()->bind($stid_a, array(
						":description"	=> $p_desc,
						":extension"	=> $extension,
						":createdby"	=> auth::check("usid"),
						":contents"		=> $contents
					))) {

					}

					/*if(true) {
						$file = fopen("photo.txt", "x+");
						$data = $_FILES["file_{$x}"]["tmp_name"];
						$data .= "\n" . $_FILES["file_{$x}"]["error"];
						$data .= "\n" . @file_get_contents($_FILES["file_{$x}"]["tmp_name"]);
						//echo $data;
						fwrite($file, $data);
					}
					*/
				}
			}
			self::sot()->commit();
		}

	}

	static function saveNearMiss() {
		$stmt_insert = self::sot()->parse("
		INSERT INTO NEAR_MISS_OBSERVATIONS (
		            NM_ID,
                    INCIDENT_TYPE,
                    COMPLETED_BY,
                    COMPLETED_BY_PHONE,
                    COMPLETED_BY_TITLE,
                    COMPLETED_BY_SUPERVISOR,
                    COMPLETED_BY_SUPERVISOR_PHONE,
                    INCIDENT_DATE,
                    INCIDENT_TIME,
                    CATEGORY,
                    SUB_CATEGORY,
                    
                    ORG_CODE,
                    PLANT,
                    COMPLETED_BY_PLANT,
                    
                    LOCATION,
                    LOCATION_DESCRIPTION,
                    HAPPENED_BEFORE,
                    HAPPENED_DURING,
                    HAPPENED_AFTER,
                    INVESTIGATION,
                    
                    SUPERVISOR_COMMENTS,
                    SUPERVISOR_COMMENTS_ENTERED,
                    
                    DIRECTOR,
                    DIRECTOR_COMMENTS,
                    DIRECTOR_COMMENTS_ENTERED,
                    COMPLETED_DATE,
                    LAST_UPDATED,
                    
                    ASSIST_INVESTIGATION,
                    ASSIST_SOLUTION,
                    REQUIRES_INVESTIGATION,
                    INVESTIGATION_COMPLETE,
                    IMMEDIATE_INVESTIGATION,
                    EDIT_USID,
                    PRIORITY,
                    ISSERIOUS
			) 
			VALUES (
				NM_UNIQUE.NEXTVAL,
				:type,
				:completed_by,
				:completed_by_phone,
				:completed_by_title,
				:completed_by_supervisor,
				:completed_by_supervisor_phone,
				TO_DATE(:dt, 'MM/DD/YYYY'),
				:tm,
				:category,
				:sub_category,
				:org,
				:plant,
				:completed_by_plant,
				:location,
				:location_description,
				:happened_before,
				:happened_during,
				:happened_after,
				:investigation,
				:supervisor_comments,
				CASE WHEN :supervisor_comments IS NULL THEN NULL ELSE SYSDATE END,
				:director,
				:director_comments,
				CASE WHEN :director_comments IS NULL THEN NULL ELSE SYSDATE END,
				SYSDATE,
				SYSDATE,
				:assist_investigation,
				:assist_solution,
				:requires_investigation,
				:investigation_complete,
				:immediate_investigation,
				:edit_usid,
				:priority,
				:isserious
			) RETURNING NM_ID INTO :nm_id
		");

		$stmt_update = self::sot()->parse("
			UPDATE NEAR_MISS_OBSERVATIONS SET
				INCIDENT_TYPE = :type,
				INCIDENT_DATE = TO_DATE(:dt, 'MM/DD/YYYY'),
				INCIDENT_TIME = :tm,
				CATEGORY = :category,
				SUB_CATEGORY = :sub_category,
				ORG_CODE = :org,
				PLANT = :plant,
				COMPLETED_BY_PLANT = :completed_by_plant,
				LOCATION = :location,
				LOCATION_DESCRIPTION = :location_description,
				HAPPENED_BEFORE = :happened_before,
				HAPPENED_DURING = :happened_during,
				HAPPENED_AFTER = :happened_after,
				INVESTIGATION = :investigation,
				SUPERVISOR_COMMENTS = :supervisor_comments,
				SUPERVISOR_COMMENTS_ENTERED = CASE WHEN :supervisor_comments IS NULL AND SUPERVISOR_COMMENTS IS NULL THEN NULL ELSE SYSDATE END,
				DIRECTOR_COMMENTS_ENTERED = CASE WHEN :director_comments IS NULL AND DIRECTOR_COMMENTS IS NULL THEN NULL ELSE SYSDATE END,
				DIRECTOR_COMMENTS = :director_comments,
				LAST_UPDATED = SYSDATE,
				ASSIST_INVESTIGATION = :assist_investigation,
				ASSIST_SOLUTION = :assist_solution,
				REQUIRES_INVESTIGATION = :requires_investigation,
				INVESTIGATION_COMPLETE = :investigation_complete,
				IMMEDIATE_INVESTIGATION = :immediate_investigation,
				EDIT_USID = :edit_usid,
				PRIORITY = :priority,
				ISSERIOUS = :isserious
			WHERE NM_ID = :nm_id
		");

		if(empty($_REQUEST["hour"])) {
			$_REQUEST["hour"] = date("h");
		}
		if(strlen(trim($_REQUEST["hour"])) == 1) {
			$_REQUEST["hour"] = "0{$_REQUEST["hour"]}";
		}
		if(empty($_REQUEST["minute"])) {
			$_REQUEST["minute"] = date("i");
		}
		if(empty($_REQUEST["date"])) {
			$_REQUEST["date"] = date("m/d/Y");
		}
		if(strlen(trim($_REQUEST["minute"])) == 1) {
			$_REQUEST["minute"] = "0{$_REQUEST["minute"]}";
		}
		$time = "{$_REQUEST["hour"]}:{$_REQUEST["minute"]} {$_REQUEST["ampm"]}";

		$investigation_complete = 0;
		$investigation = trim($_REQUEST["investigation"]);
		if(!empty($investigation)) {
			$investigation_complete = 1;
		} else {
			$investigation_complete = $_REQUEST["investigation_complete"];
		}

        $priority = 'N';

		$update_error = -1;
		$created_error = -1;

		$created = false;

		if(isset($_REQUEST["nm_id"]) && !empty($_REQUEST["nm_id"])) {
			$nm_id = $_REQUEST["nm_id"];
			if(self::sot()->bind($stmt_update, array(
				":dt"							=> $_REQUEST["date"],
				":tm"							=> $time,
				":type" 						=> $_REQUEST["incident_type"],
				":category" 					=> $_REQUEST["category"],
				":sub_category" 				=> $_REQUEST["sub_category_{$_REQUEST["category"]}"],
				":org" 							=> $_REQUEST["org"],
				":plant" 						=> $_REQUEST["plant"],
				":completed_by_plant" 			=> $_REQUEST["completed_by_plant"],
				":location" 					=> $_REQUEST["location"],
				":location_description" 		=> $_REQUEST["location_description"],
				":happened_before" 				=> $_REQUEST["happened_before"],
				":happened_during" 				=> $_REQUEST["happened_during"],
				":happened_after" 				=> $_REQUEST["happened_after"],
				":investigation" 				=> $_REQUEST["investigation"],
				":supervisor_comments" 			=> $_REQUEST["supervisor_comments"],
				":director" 					=> $_REQUEST["director"],
				":director_comments" 			=> $_REQUEST["director_comments"],
				":nm_id"						=> $nm_id,
				":assist_investigation"			=> $_REQUEST["assist_investigation"],
				":assist_solution"				=> $_REQUEST["assist_solution"],
				":requires_investigation"		=> $_REQUEST["requires_investigation"],
				":investigation_complete"		=> $investigation_complete,
				":immediate_investigation"		=> $_REQUEST["immediate_investigation"],
				":edit_usid"					=> strtolower(trim($_REQUEST["edit_usid"])),
				":priority" 					=> $priority,
                ":isserious"                    => $_REQUEST["isserious"]
			))) {

			} else {
				$update_error = 1;
				$error = self::sot()->error();
				die("<script type = 'text/javascript'>
					window.top.window.sot.tools.cover(false);
					alert(\"Error: Unable to update incident for Near Miss. Error Message: {$error["message"]}\");
				");
			}
		} else {
			if($nm_id = self::sot()->bind($stmt_insert, array(
				":completed_by" 				=> $_REQUEST["completed_by"],
				":completed_by_title" 			=> $_REQUEST["completed_by_title"],
				":completed_by_phone" 			=> preg_replace("/[^0-9]/", "", $_REQUEST["completed_by_phone"]),
				":completed_by_supervisor" 		=> $_REQUEST["completed_by_supervisor"],
				":completed_by_supervisor_phone" => preg_replace("/[^0-9]/", "", $_REQUEST["completed_by_supervisor_phone"]),
				":type" 						=> $_REQUEST["incident_type"],
				":dt"							=> $_REQUEST["date"],
				":tm"							=> $time,
				":category" 					=> $_REQUEST["category"],
				":sub_category" 				=> $_REQUEST["sub_category_{$_REQUEST["category"]}"],
				":org" 							=> $_REQUEST["org"],
				":plant" 						=> $_REQUEST["plant"],
				":completed_by_plant" 			=> $_REQUEST["completed_by_plant"],
				":location" 					=> $_REQUEST["location"],
				":location_description" 		=> $_REQUEST["location_description"],
				":happened_before" 				=> $_REQUEST["happened_before"],
				":happened_during" 				=> $_REQUEST["happened_during"],
				":happened_after" 				=> $_REQUEST["happened_after"],
				":investigation" 				=> $_REQUEST["investigation"],
				":supervisor_comments" 			=> $_REQUEST["supervisor_comments"],
				":director" 					=> $_REQUEST["director"],
				":director_comments" 			=> $_REQUEST["director_comments"],
				":nm_id"						=> array(SQL_INT, 0),
				":assist_investigation"			=> $_REQUEST["assist_investigation"],
				":assist_solution"				=> $_REQUEST["assist_solution"],
				":requires_investigation"		=> $_REQUEST["requires_investigation"],
				":investigation_complete"		=> $investigation_complete,
				":immediate_investigation"		=> $_REQUEST["immediate_investigation"],
				":edit_usid"					=> strtolower(trim($_REQUEST["edit_usid"])),
				":priority" 					=> $priority,
                ":isserious"                    => $_REQUEST["isserious"]
			), true, true)) {//return binds
				$created = true;
			} else {
				$created_error = 1;
				$error = self::sot()->error();
				die("<script type = 'text/javascript'>
					window.top.window.sot.tools.cover(false);
					alert(\"Error: Unable to create incident for Near Miss. Please report this error message. Error Message: {$error["message"]}\");
				</script>");
			}

			$nm_id = $nm_id[":nm_id"];
		}

		$stmt_twin = self::sot()->parse("
			MERGE INTO TWIN_ANSWERS USING DUAL ON (OBSERVATION_ID = {$nm_id}
					AND OBSERVATION_FORM = 'NM'
					AND ITEM_NUM = :itemnum)
			WHEN MATCHED THEN UPDATE  
				SET ANSWER = :answer,
					EXPLANATION = :explanation
				WHERE OBSERVATION_ID = {$nm_id}
					AND OBSERVATION_FORM = 'NM'
					AND ITEM_NUM = :itemnum
			WHEN NOT MATCHED THEN
				INSERT (
					OBSERVATION_FORM,
					OBSERVATION_ID,
					ITEM_NUM,
					ANSWER,
					EXPLANATION
				) VALUES(
					'NM',
					{$nm_id},
					:itemnum,
					:answer,
					:explanation
				)
		");
		//safety measures
		$stmt_sm = self::sot()->parse("
			MERGE INTO NEAR_MISS_SM_ANSWERS 
				USING DUAL ON (NM_ID = {$nm_id}
					AND SAP_NUMBER = :sapnum)
			WHEN MATCHED THEN UPDATE  
				SET ANSWER = :answer,
				ADDITIONAL_DESCRIPTION = :ad_desc
				WHERE NM_ID = {$nm_id}
					AND SAP_NUMBER = :sapnum
			WHEN NOT MATCHED THEN
				INSERT (
					NM_ID,
					SAP_NUMBER,
					ANSWER,
					ADDITIONAL_DESCRIPTION
				) VALUES(
					{$nm_id},
					:sapnum,
					:answer,
					:ad_desc
				)
		");

		$twin_error = -1;
		foreach($_REQUEST["twin"] as $key=>$value) {
			if(!self::sot()->bind($stmt_twin, array(
				":itemnum"		=> $key,
				":answer"		=> $value['answer'],
				":explanation"	=> $value['explanation']
			))) {
				$twin_error++;
			}
		}

		$sm_error = -1;
		foreach($_REQUEST["sm"] as $key=>$value) {
			if(!self::sot()->bind($stmt_sm, array(
				":sapnum"  	=> $key,
				":answer"	=> $value,
				":ad_desc"	=> $_REQUEST["sm_ad_{$key}"]
			))) {
				$sm_error++;
			}
		}

		self::savePhotos('NM', $nm_id);

		if(($_REQUEST["workflow"] == "1" || $created) && ($created_error == -1 && $update_error == -1)) {
			self::nearMissWF($nm_id, $created);
		}

		if($twin_error != -1) {
			die("<script type = 'text/javascript'>
				window.top.window.sot.tools.cover(false);
				alert('Error: Unable to save Twin Analysis responses for this Near Miss Incident.');
			");
		}
		if($sm_error != -1) {
			die("<script type = 'text/javascript'>
				window.top.window.sot.tools.cover(false);
				alert('Error: Unable to save Safety Measures entered for this Near Miss Incident.');
			");
		}

		$returnto = "index";
		if(isset($_REQUEST["returnto"]) && !$_GET["mobile"]){
			$returnto = $_REQUEST["returnto"];
		}

		die("<script type = 'text/javascript'>
			window.top.window.sot.tools.cover(false);
			window.top.window.location=\"../../" . ($_GET['mobile'] ? "mobile/" : "") . "{$returnto}.php?nm=1" . (empty($_REQUEST["delegator"]) || $_REQUEST["delegator"] == 0 ? "" : "&delegate={$_REQUEST["delegate"]}") . "\";
		</script>");
	}

	static function saveGoodCatch() {

		$stmt_insert = self::sot()->parse("
			INSERT INTO GOOD_CATCH_OBSERVATIONS (
				GC_ID,
				COMPLETED_BY,
				COMPLETED_BY_PHONE,
				COMPLETED_BY_TITLE,
				COMPLETED_BY_SUPERVISOR,
				COMPLETED_BY_SUPERVISOR_PHONE,
				DIRECTOR,
				INCIDENT_DATE,
				INCIDENT_TIME,
				CATEGORY,
				
				ORG_CODE,
				PLANT,
				COMPLETED_BY_PLANT,
				LOCATION,
				LOCATION_DESCRIPTION,
				
				HAPPENED_BEFORE,
				HAPPENED_DURING,
				HAPPENED_AFTER,
				COMPLETED_DATE,
				
				PERSON_RECOGNIZED,
				CLOSED,
				TOOLS_USED
			) VALUES (
				GC_UNIQUE.NEXTVAL,
				:completed_by,
				:completed_by_phone,
				:completed_by_title,
				:completed_by_supervisor,
				:completed_by_supervisor_phone,
				:director,
				TO_DATE(:dt, 'MM/DD/YYYY'),
				:tm,
				:category,
				:org,
				:plant,
				:completed_by_plant,
				:location,
				:location_description,
				:happened_before,
				:happened_during,
				:happened_after,
				SYSDATE,
				:person_recognized,
				:closed,
				:tools_used
			) RETURNING GC_ID INTO :gc_id
		");

		$stmt_update = self::sot()->parse("
			UPDATE GOOD_CATCH_OBSERVATIONS SET
				INCIDENT_DATE = TO_DATE(:dt, 'MM/DD/YYYY'),
				INCIDENT_TIME = :tm,
				CATEGORY = :category,
				ORG_CODE = :org,
				PLANT = :plant,
				COMPLETED_BY_PLANT = :completed_by_plant,
				LOCATION = :location,
				LOCATION_DESCRIPTION = :location_description,
				HAPPENED_BEFORE = :happened_before,
				HAPPENED_DURING = :happened_during,
				HAPPENED_AFTER = :happened_after,
				LAST_UPDATED = SYSDATE,
				PERSON_RECOGNIZED = :person_recognized,
				CLOSED = :closed,
				TOOLS_USED = :tools_used
			WHERE GC_ID = :gc_id
		");
		if(empty($_REQUEST["hour"])) {
			$_REQUEST["hour"] = date("h");
		}
		if(strlen(trim($_REQUEST["hour"])) == 1) {
			$_REQUEST["hour"] = "0{$_REQUEST["hour"]}";
		}
		if(empty($_REQUEST["minute"])) {
			$_REQUEST["minute"] = date("i");
		}
		if(strlen(trim($_REQUEST["minute"])) == 1) {
			$_REQUEST["minute"] = "0{$_REQUEST["minute"]}";
		}
		$time = "{$_REQUEST["hour"]}:{$_REQUEST["minute"]} {$_REQUEST["ampm"]}";

		$closed = $_REQUEST["closed"] == "1" ? 1 : 0;

		$update_error = -1;
		$created_error = -1;
		$created = false;
		if(isset($_REQUEST["gc_id"]) && !empty($_REQUEST["gc_id"])) {
			$gc_id = $_REQUEST["gc_id"];
			if(self::sot()->bind($stmt_update, array(
				":dt"							=> $_REQUEST["date"],
				":tm"							=> $time,
				":type" 						=> $_REQUEST["incident_type"],
				":category" 					=> $_REQUEST["category"],
				":org" 							=> $_REQUEST["org"],
				":plant" 						=> $_REQUEST["plant"],
				":completed_by_plant" 			=> $_REQUEST["completed_by_plant"],
				":location" 					=> $_REQUEST["location"],
				":location_description" 		=> $_REQUEST["location_description"],
				":happened_before" 				=> $_REQUEST["happened_before"],
				":happened_during" 				=> $_REQUEST["happened_during"],
				":happened_after" 				=> $_REQUEST["happened_after"],
				":gc_id"						=> $gc_id,
				":closed"						=> $closed,
				":person_recognized"			=> strtolower($_REQUEST["person_recognized"]),
				":tools_used"					=> json_encode($_REQUEST["tools_used"])
			), true, true)) {

			} else {

				$update_error = 1;
				$error = self::sot()->error();
				die("<script type = 'text/javascript'>
					window.top.window.sot.tools.cover(false);
					alert(\"Error: Unable to update incident for Good Catch. Error Message: {$error["message"]}\");
					</script>
				");
			}
		} else {
			$new = true;
			if($gc_id = self::sot()->bind($stmt_insert, array(
				":completed_by" 				=> $_REQUEST["completed_by"],
				":completed_by_title" 			=> $_REQUEST["completed_by_title"],
				":completed_by_phone" 			=> preg_replace("/[^0-9]/", "", $_REQUEST["completed_by_phone"]),
				":completed_by_supervisor" 		=> $_REQUEST["completed_by_supervisor"],
				":completed_by_supervisor_phone" => preg_replace("/[^0-9]/", "", $_REQUEST["completed_by_supervisor_phone"]),
				":director" 					=> $_REQUEST["director"],
				":dt"							=> $_REQUEST["date"],
				":tm"							=> $time,
				":category" 					=> $_REQUEST["category"],
				":org" 							=> $_REQUEST["org"],
				":plant" 						=> $_REQUEST["plant"],
				":completed_by_plant" 			=> $_REQUEST["completed_by_plant"],
				":location" 					=> $_REQUEST["location"],
				":location_description" 		=> $_REQUEST["location_description"],
				":happened_before" 				=> $_REQUEST["happened_before"],
				":happened_during" 				=> $_REQUEST["happened_during"],
				":happened_after" 				=> $_REQUEST["happened_after"],
				":gc_id"						=> array(SQL_INT, 0),
				":person_recognized"			=> strtolower($_REQUEST["person_recognized"]),
				":closed"						=> $closed,
				":tools_used"					=> json_encode($_REQUEST["tools_used"])
			), true, true)) {//return binds
				$created = true;
			} else {
				$created_error = 1;
				$error = self::sot()->error();
				die("<script type = 'text/javascript'>
					window.top.window.sot.tools.cover(false);
					alert(\"Error: Unable to create incident for Good Catch. Error Message: {$error["message"]}\");
					</script>
				");
			}

			$gc_id = $gc_id[":gc_id"];
		}

		self::savePhotos('GC', $gc_id);
		$returnto = "index";
		if(isset($_REQUEST["returnto"]) && !$_GET["mobile"]){
			$returnto = $_REQUEST["returnto"];
		}

		self::gcWF($gc_id, $new);

		die("<script type = 'text/javascript'>
				window.top.window.sot.tools.cover(false);
				window.top.window.location=\"../../" . ($_GET['mobile'] ? "mobile/" : "") . "{$returnto}.php?gc=1" . (empty($_REQUEST["delegator"]) || $_REQUEST["delegator"] == 0 ? "" : "&delegate={$_REQUEST["delegate"]}") . "\";
		</script>");
	}
	static function deleteGoodCatch() {
		$sql = "DELETE FROM GOOD_CATCH_OBSERVATIONS WHERE GC_ID = {$_REQUEST["gc_id"]}";
		self::sot()->query($sql);

		$sql = "DELETE FROM PHOTOS WHERE OBSERVATION_FORM = 'GC' AND OBSERVATION_ID = {$_REQUEST["gc_id"]}";
		self::sot()->query($sql);

		self::printer(true);
	}

	static function deleteRedtag() {
		$sql = "DELETE FROM RED_TAG_AUDITS WHERE RTA_ID = {$_REQUEST["rta_id"]}";
		self::sot()->query($sql);

		$sql = "DELETE FROM RED_TAG_ATTACHMENTS WHERE RTA_ID = {$_REQUEST["rta_id"]}";
		self::sot()->query($sql);

		$sql = "DELETE FROM RED_TAG_ANSWERS WHERE RTA_ID = {$_REQUEST["rta_id"]}";
		self::sot()->query($sql);

		self::printer(true);

	}

	static function deleteEpop() {
		$sql = "DELETE FROM EPOP_OBSERVATIONS WHERE EPOP_ID = {$_REQUEST["epopid"]}";
		self::sot()->query($sql);

		$sql = "DELETE FROM EPOP_ANSWERS WHERE EPOP_ID = {$_REQUEST["epopid"]}";
		self::sot()->query($sql);

		$sql = "DELETE FROM EPOP_COMMENTS WHERE EPOP_ID = {$_REQUEST["epopid"]}";
		self::sot()->query($sql);

		$sql = "DELETE FROM PHOTOS WHERE OBSERVATION_FORM = 'NM' AND OBSERVATION_ID = {$_REQUEST["epopid"]}";
		self::sot()->query($sql);

		self::printer(true);

	}

	static function deleteNearMiss() {
		$sql = "DELETE FROM NEAR_MISS_OBSERVATIONS WHERE NM_ID = {$_REQUEST["nm_id"]}";
		self::sot()->query($sql);

		$sql = "DELETE FROM NEAR_MISS_SM_ANSWERS WHERE NM_ID = {$_REQUEST["nm_id"]}";
		self::sot()->query($sql);

		$sql = "DELETE FROM TWIN_ANSWERS WHERE OBSERVATION_FORM = 'NM' AND OBSERVATION_ID = {$_REQUEST["nm_id"]}";
		self::sot()->query($sql);

		$sql = "DELETE FROM PHOTOS WHERE OBSERVATION_FORM = 'NM' AND OBSERVATION_ID = {$_REQUEST["nm_id"]}";
		self::sot()->query($sql);

		self::printer(true);

	}

	static function deleteHP($hp_id, $draft) {
		$hp_id = ($hp_id) ? $hp_id : $_REQUEST["hp_id"]; //can be called by other member functions or via AJAX

		$sql = "DELETE FROM HP_OBSERVATIONS WHERE HP_ID = {$hp_id}";
		self::sot()->query($sql);

		$sql = "DELETE FROM HP_ANSWERS WHERE HP_ID = {$hp_id}";
		self::sot()->query($sql);

		$sql = "DELETE FROM TWIN_ANSWERS WHERE OBSERVATION_FORM = 'HP' AND OBSERVATION_ID = {$hp_id}";
		self::sot()->query($sql);

		if(!$draft) {
			$sql = "DELETE FROM PHOTOS WHERE OBSERVATION_FORM = 'HP' AND OBSERVATION_ID = {$hp_id}";
			self::sot()->query($sql);
		}

		self::printer(true);

	}

	static function deleteStormDuty() {
		$sql = "DELETE FROM STORM_DUTY_OBSERVATIONS WHERE SD_ID = {$_REQUEST["sd_id"]}";
		self::sot()->query($sql);

		$sql = "DELETE FROM STORM_DUTY_ANSWERS WHERE SD_ID = {$_REQUEST["sd_id"]}";
		self::sot()->query($sql);

		$sql = "DELETE FROM STORM_DUTY_MEMBERS WHERE SD_ID = {$_REQUEST["sd_id"]}";
		self::sot()->query($sql);

		self::printer(true);

	}

	static function deleteNearMissEmail() {
		$sql = "DELETE FROM NM_MAILING_LIST WHERE EMAIL = '{$_REQUEST["email"]}'";
		self::sot()->query($sql);

		self::printer(true);

	}
	static function addNearMissEmail() {
		$user = auth::check('usid');
		$email = $_REQUEST["email"];
		$sql = "INSERT INTO NM_MAILING_LIST (EMAIL, ADDED_BY) VALUES('{$email}', '{$user["usid"]}')";
		if(self::sot()->query($sql)) {
			self::printer(true);
		} else {
			self::printer(array("error" => "Unable to add {$email} to the Mailing List. Make sure the email is not already in the Mailing List."));
		}
	}

	static function addEmail() {
		$user = auth::check('usid');
		$email = $_REQUEST["email"];
		$observation = $_REQUEST["observation"];
		$sql = "INSERT INTO MAILING_LIST (EMAIL, ADDED_BY, OBSERVATION) VALUES('{$email}', '{$user["usid"]}', '{$observation}')";
		if(self::sot()->query($sql)) {
			self::printer(true);
		} else {
			self::printer(array("error" => "Unable to add {$email} to the Mailing List. Make sure the email is not already in the Mailing List."));
		}
	}

	static function deleteEmail() {
		$sql = "DELETE FROM MAILING_LIST WHERE EMAIL = '{$_REQUEST["email"]}' AND OBSERVATION = '{$_REQUEST["observation"]}'";
		self::sot()->query($sql);

		self::printer(true);

	}

	static function nearMissWF($nm_id, $new) {
		$cc = '';
		$completed_by_email = "";
		$sql = "
		SELECT  NM.NM_ID,
				NM.COMPLETED_BY,
				C.CAT_TEXT,
				ORG.ORG_DESCRIPTION,
				P.PLANT,
				P2.PLANT AS COMPLETED_BY_PLANT,
				NM.HAPPENED_BEFORE,
				NM.HAPPENED_DURING,
				NM.HAPPENED_AFTER,
				INVESTIGATION,
				REQUIRES_INVESTIGATION,
				IMMEDIATE_INVESTIGATION,
				NM.EDIT_USID,
				NM.PRIORITY,
				NM.ISSERIOUS
		FROM    NEAR_MISS_OBSERVATIONS NM
		LEFT	JOIN (SELECT DISTINCT ORG_CODE, ORG_DESCRIPTION FROM ORGANIZATIONS) ORG
				ON ORG.ORG_CODE = NM.ORG_CODE
		LEFT	JOIN NEAR_MISS_PLANTS P
				ON P.PLANT_ID = NM.PLANT
		LEFT	JOIN NEAR_MISS_PLANTS P2
				ON P2.PLANT_ID = NM.COMPLETED_BY_PLANT		
		LEFT	JOIN NEAR_MISS_CATEGORIES C
				ON NM.CATEGORY = C.CAT_ID
		WHERE   NM_ID = {$nm_id}";

		if($row = self::sot()->fetch($sql)) {
			$leafusid = $row["COMPLETED_BY"];
			$completed_by = $leafusid;
			$priority = $row['PRIORITY'];
            $serious = $row['ISSERIOUS'];
			if(!empty($leafusid)) {
				//find director
				$loop = true;
				$foundDirector = false;
				do {
					$leaf = @mcl_Ldap::lookup($leafusid);
					$isDirector = strpos(strtolower($leaf["title"]), "director");
					$title = strtolower($leaf["title"]);

					$cc .= (empty($cc) ? "" : ", ") . $leaf["email"];

					if($isDirector || $isDirector === 0) {
						$loop = false;
						$director = $leaf;
						$foundDirector = true;
					}

					if(!$leaf["manager"]) {	$loop = false; }
					$leafusid = $leaf["manager"];

				} while($loop);
			}
		}

		$sql = "SELECT EMAIL FROM MAILING_LIST WHERE OBSERVATION = 'NM'";
		while($row2 = self::sot()->fetch($sql)) {
			$cc .= (empty($cc) ? "" : ", ") . $row2["EMAIL"];
		}

		$from = "SOTERIA@dteenergy.com";

		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= "From: {$from}" . "\r\n";
		$headers .= "Reply-To: {$from}" . "\r\n";

		if($row['IMMEDIATE_INVESTIGATION'] == '1' || $serious=='1') {
			$headers .= "X-Priority: 1 (Highest)\n";
			$headers .= "X-MSMail-Priority: High\n";
			$headers .= "Importance: High\n";
		}

		$headers .= "Bcc: {$cc}" . "\r\n";

		if($priority == 'H') $priorityFormatted = 'High';
		else if($priority == 'M') $priorityFormatted = 'Medium';
		else $priorityFormatted = 'Low';

		if($serious=='1'){
		    $seriousSubject = "Yes";
        }
        else{
            $seriousSubject = "No";
        }
		$subject = 'SOTeria Near Miss Incident - ' . ($new ? "New" : "Update") . ($row['IMMEDIATE_INVESTIGATION'] == 1 ? ' (Requires Immediate Investigation)' : '')." Serious?(".$seriousSubject.")" ;//. " (Priority: " . $priorityFormatted . ')';

		$date = date("m/d/Y");
		$url = 'http' . ($_SERVER['SERVER_PORT'] == '443' ? 's' : '') . '://' . $_SERVER['HTTP_HOST'];

		if($new) {
			$message = "<div>A new Near Miss Incident has been submitted using <a href='{$url}'>SOTeria</a> on {$date}.</div>";
		} else {
			$message = "<div>A Near Miss Incident has been updated using <a href='{$url}'>SOTeria</a> on {$date}.</div>";
		}

		$message .= "<div style='margin-top: 10px;'>";
			$message .= "<span style='color: #000;'>Organization:</span> {$row["ORG_DESCRIPTION"]}<br/>";
			$message .= "<span style='color: #000;'>Reporting Employee Location:</span> {$row["COMPLETED_BY_PLANT"]}<br/>";
			$message .= "<span style='color: #000;'>Location Where Near Miss Occurred:</span> {$row["PLANT"]}<br/>";
			$message .= "<span style='color: #000;'>Category:</span> " . (empty($row["CAT_TEXT"]) ? "Not Selected" : $row["CAT_TEXT"]) ."<br/>";
			$message .= "<span style='color: #000;'>Investigation Results:</span> " . (empty($row["INVESTIGATION"]) ? ($row["REQUIRES_INVESTIGATION"] == 1 ? 'Not Complete' : 'Not Required') : $row["INVESTIGATION"]) . "<br/>";

		$message .= "</div>";

		$message .= "Happened Before: {$row["HAPPENED_BEFORE"]}<br/>";
		$message .= "Happened During: {$row["HAPPENED_DURING"]}<br/>";
		$message .= "Happened After: {$row["HAPPENED_AFTER"]}<br/>";

		$message .= "<div style='margin-top: 10px;'>";
			$message .= "For more information, log into <a href='{$url}'>SOTeria</a> and view the Near Miss at 'Organization Reports' > 'Near Miss'.";
		$message .= "</div>";


		$message .= "<div>To view the Near Miss directly, please visit the following link: <a href='{$url}/forms/nearmiss.php?mode=readonly&nm_id={$nm_id}'>Near Miss Incident #{$nm_id}</a></div>";
		$message .= "<div style='margin-top: 10px;'><br/>Thank You</div>";
		$message .= "<div style='margin-top: 20px; font-style: italic;'>You are receiving this message because you are either on the mailing list or you are a Leader of the employee who submitted the Near Miss Incident.<br/>Do not reply to this email. For any questions, please contact Corporate Safety.</div>";

		mail($from, $subject, $message, $headers);

	}

	static function gcWF($gc_id, $new) {
		$cc = '';
		$completed_by_email = "";
		$sql = "
		SELECT  NM.NM_ID,
				NM.ORGANIZATION,
				NM.ORG_CODE,
				ORG.ORG_DESCRIPTION
		FROM    NEAR_MISS_OBSERVATIONS NM
		LEFT	JOIN (SELECT DISTINCT ORG_CODE, ORG_DESCRIPTION FROM ORGANIZATIONS) ORG
				ON ORG.ORG_CODE = NM.ORG_CODE
		WHERE   NM_ID = {$gc_id}";

		$sql = "SELECT EMAIL FROM MAILING_LIST WHERE OBSERVATION = 'GC'";
		while($row2 = self::sot()->fetch($sql)) {
			$cc .= (empty($cc) ? "" : ", ") . $row2["EMAIL"];
		}

		$from = "SOTERIA@dteenergy.com";

		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= "From: {$from}" . "\r\n";
		$headers .= "Reply-To: {$from}" . "\r\n";

		$headers .= "Bcc: {$cc}" . "\r\n";
		$subject = 'SOTeria Good Catch';

		$date = date("m/d/Y");
		$url = 'http' . ($_SERVER['SERVER_PORT'] == '443' ? 's' : '') . '://' . $_SERVER['HTTP_HOST'];

		if($new) {
			$message = "<div>A new Good Catch has been submitted using <a href='{$url}'>SOTeria</a> on {$date}.</div>";
		} else {
			$message = "<div>An Good Catch has been updated using <a href='{$url}'>SOTeria</a> on {$date}.</div>";
		}

		$message .= "<div style='margin-top: 10px;'>";
			$message .= "<span style='color: #000;'>Organization:</span> {$row["ORG_DESCRIPTION"]}<br/>";

		$message .= "</div>";

		$message .= "<div>To view the Good Catch directly, please visit the following link: <a href='{$url}/forms/nearmiss.php?mode=readonly&gc_id={$gc_id}&gc=true'>Good Catch #{$gc_id}</a></div>";
		$message .= "<div style='margin-top: 10px;'><br/>Thank You</div>";
		$message .= "<div style='margin-top: 20px; font-style: italic;'>You are receiving this message because you are either on the mailing list or you are a Leader of the employee who submitted the Anatomy of an Event.<br/>Do not reply to this email. For any questions, please contact Corporate Safety.</div>";

		mail($from, $subject, $message, $headers);
	}

	static function hpWF($hp_id, $new) {
		$cc = '';
		$completed_by_email = "";
		$sql = "
		SELECT  HP.HP_ID,
				HP.ORGANIZATION,
				HP.ORG_CODE,
				ORG.ORG_DESCRIPTION
		FROM    HP_OBSERVATIONS HP
		LEFT	JOIN (SELECT DISTINCT ORG_CODE, ORG_DESCRIPTION FROM ORGANIZATIONS) ORG
				ON ORG.ORG_CODE = HP.ORG_CODE
		WHERE   HP_ID = {$hp_id}";

		$sql = "SELECT EMAIL FROM MAILING_LIST WHERE OBSERVATION = 'HP'";
		while($row2 = self::sot()->fetch($sql)) {
			$cc .= (empty($cc) ? "" : ", ") . $row2["EMAIL"];
		}

		$url = 'http' . ($_SERVER['SERVER_PORT'] == '443' ? 's' : '') . '://' . $_SERVER['HTTP_HOST'];

		$from = "SOTERIA@dteenergy.com";

		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= "From: {$from}" . "\r\n";
		$headers .= "Reply-To: {$from}" . "\r\n";

		$headers .= "Bcc: {$cc}" . "\r\n";
		$subject = 'SOTeria Anatomy of an Event';

		$date = date("m/d/Y");

		if($new) {
			$message = "<div>A new Anatomy of an Event has been submitted using <a href='{$url}'>SOTeria</a> on {$date}.</div>";
		} else {
			$message = "<div>An Anatomy of an Event has been updated using <a href='{$url}'>SOTeria</a> on {$date}.</div>";
		}

		$message .= "<div style='margin-top: 10px;'>";
			$message .= "<span style='color: #000;'>Organization:</span> {$row["ORG_DESCRIPTION"]}<br/>";

		$message .= "</div>";

		$message .= "<div>To view the Anatomy of an Event directly, please visit the following link: <a href='{$url}/forms/viewHp.php?hp_id={$hp_id}'>Anatomy of an Event #{$hp_id}</a></div>";
		$message .= "<div style='margin-top: 10px;'><br/>Thank You</div>";
		$message .= "<div style='margin-top: 20px; font-style: italic;'>You are receiving this message because you are either on the mailing list or you are a Leader of the employee who submitted the Anatomy of an Event.<br/>Do not reply to this email. For any questions, please contact Corporate Safety.</div>";

		mail($from, $subject, $message, $headers);
	}

	static function sendFeedback() {

		$user = auth::check();

		$name = $user["name"];
		$usid = $user["usid"];
		$date = date("m/d/Y");
		$org = $user["org"];
		$phone = $user["details"]["all"][0]["telephonenumber"][0];
		$title = $user["details"]["title"];
		$from = "{$name} <{$user["details"]["email"]}>";

		$to = "SOTERIA@dteenergy.com";

		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= "From: {$from}" . "\r\n";
		$headers .= "Reply-To: {$to}" . "\r\n";

		$sql = "
			SELECT	*
			FROM	EMPLOYEES E,
					USERS U
			WHERE	U.USID = E.USID
					AND U.MANAGE_DELEGATES_EXCLUSIONS = 1
					AND E.ORG_DESCRIPTION = ( SELECT ORG_DESCRIPTION FROM EMPLOYEES WHERE USID = '{$usid}')
		";
		$oci = new mcl_Oci("soteria");

		$cc_array = array();
		$cc_array[$to] = true;
		$cc_array[$user["details"]["email"]] = true;
		while($row = $oci->fetch($sql)) {
			if($cc_array[$row["EMAIL"]]) { continue; }
			$buReps .= ",". "{$row["NAME"]} <{$row["EMAIL"]}>";
			$cc_array[$row["EMAIL"]] = true;
		}
		$cc = "Cc: {$from}{$buReps}" . "\r\n";

		if($_REQUEST["exclusion"] == "true") {
			if($_REQUEST["other"] == "true") {
				$excludeUsid = $_REQUEST["usid"];
				$lookup = @mcl_Ldap::lookup($excludeUsid);
				if(empty($lookup["fname"])) {
					self::printer(array("error" => "Invaid username provided for Emploee To Be Excluded."));
					return;
				}

				$name = $lookup["fname"];
				$cc .= ",{$name} <{$lookup["email"]}>";

			} else {
				$excludeUsid = $usid;
			}

			$excludeUsid = strtolower($excludeUsid);

			$sql = ("
				INSERT INTO EXCLUSIONS(
					EX_ID,
					USID,
					START_DT,
					END_DT,
					DESCRIPTION,
					ADDED_BY,
					ADDED
				) VALUES(
					EX_UNIQUE.NEXTVAL,
					'{$excludeUsid}',
					TO_DATE('{$_REQUEST["from"]} 00:00:00', 'MM/DD/YYYY HH24:MI:SS'),
					TO_DATE('{$_REQUEST["to"]} 23:59:59', 'MM/DD/YYYY HH24:MI:SS'),
					'" . trim($_REQUEST["remarks"]) . "',
					'{$usid}',
					SYSDATE
				)
			");

			$success = $oci->query($sql);
			if(!$success) {
				$error = $oci->error();
				$error = $error["message"];
				self::printer(array("error" => "There was an error submitting your exclusion - {$error}. Please email SOTERIA@dteenergy.com directly with this information."));
				return;
			}
			$subject = "SOTeria Exclusion Submitted - {$org}" ;
			$message = "<div>Employee To Be Excluded: {$name}</div>";
			$message .= "<div>Exclude From: {$_REQUEST["from"]}</div>";
			$message .= "<div>Exclude To: {$_REQUEST["to"]}</div>";
			$message .= "<div>Remarks: {$_REQUEST["remarks"]}</div>";
			$message .= "<div style='margin-top: 10px;'>--<br/>{$user["name"]} ({$usid})<br/>{$title}<br/>{$org}<br/>{$phone}</div>";

			$message .= "<div style='font-size:10px; margin-top: 10px;'>
				You are receiving this message because you are either the user who submitted this exclusion or you are a Business Unit Representative who manages exclusions in <a href='https://soteria.dteco.com'>SOTeria.</a>
			</div>";

		} else {
			$subject = "SOTeria Feedback - {$org}" ;
			$message = "<div>{$_REQUEST["feedback"]}</div>";
			$message .= "<div style='margin-top: 10px;'>--<br/>{$name} ({$usid})<br/>{$title}<br/>{$org}<br/>{$phone}</div>";
		}

		$headers .= $cc;
		$success = mail($to, $subject, $message, $headers);
		if(!$success) {
			self::printer(array("error" => "There was an error while processing your input. Please send your input directly to SOTERIA@dteenergy.com."));
			return;
		}
		self::printer(array("result" => $message));
	}

	static function RTAReq() {
		$temp = $sql = ("		
			MERGE INTO RED_TAG_AUDITS_REQUIREMENTS USING DUAL ON 
				(LOCATION_ID = {$_REQUEST["id"]} AND MONTH='{$_REQUEST["month"]}' AND YEAR = {$_REQUEST["year"]})
			WHEN MATCHED THEN 
				UPDATE SET TOTAL = {$_REQUEST["total"]}
			WHEN NOT MATCHED THEN 
				INSERT (LOCATION_ID, \"MONTH\", \"YEAR\", TOTAL) VALUES (
					{$_REQUEST["id"]}, 
					{$_REQUEST["month"]}, 
					{$_REQUEST["year"]},
					{$_REQUEST["total"]}
				)
		");

		$result = self::sot()->query($sql);
		self::printer(self::sot()->error());
	}

	static function saveStormDuty() {
		echo '<pre>';

		$sd_id = $_REQUEST['sd_id'];
		$new = false;
		if(!$sd_id) {
			$sql = 'SELECT STORM_DUTY_UNIQUE.NEXTVAL AS SD_ID FROM DUAL';
			$row = self::sot()->fetch($sql);
			$sd_id = $row['SD_ID'];
			$new = true;
		}

		$stmt_main = self::sot()->parse("
			MERGE INTO STORM_DUTY_OBSERVATIONS SDO
			USING DUAL ON (SDO.SD_ID = :sd_id)
			WHEN NOT MATCHED THEN
			INSERT (
				SD_ID,
				OBSERVED_BY,
				OBSERVED_DATE,
				OBSERVED_TIME,
				COMPLETED_BY,
				COMPLETED_DATE,
				LOCATION,
				PUBLIC_SAFETY_TEAM_NUMBER,
				ORG_CODE
				
			) VALUES (
				:sd_id,
				:observed_by,
				TO_DATE(:observed_date, 'MM/DD/YYYY'),
				:observed_time,
				:completed_by,
				SYSDATE,
				:location,
				:team_number,
				:org
			)
			WHEN MATCHED THEN
			UPDATE SET
				OBSERVED_BY = :observed_by,
				OBSERVED_DATE = TO_DATE(:observed_date, 'MM/DD/YYYY'),
				OBSERVED_TIME = :observed_time,
				LOCATION = :location,
				PUBLIC_SAFETY_TEAM_NUMBER = :team_number,
				ORG_CODE = :org
		");

		$stmt_item = self::sot()->parse("
			MERGE INTO STORM_DUTY_ANSWERS SDA
			USING DUAL ON (SDA.SD_ID = :sd_id AND ITEM_NUM = :item)
			WHEN NOT MATCHED THEN
			INSERT (
				SD_ID,
				ITEM_NUM,
				ANSWER,
				COMMENTS
				
			) VALUES (
				:sd_id,
				:item,
				:answer,
				:comments
			)
			WHEN MATCHED THEN
			UPDATE SET
				ANSWER = :answer,
				COMMENTS = :comments
		");
		$stmt_member = self::sot()->parse("
			INSERT INTO STORM_DUTY_MEMBERS (
				SD_ID,
				MEMBER_USID
			) VALUES (
				:sd_id,
				:member
			)
		");

		$binds = array();
		if(empty($_REQUEST["hour"])) {
			$_REQUEST["hour"] = date("h");
		}
		if(strlen(trim($_REQUEST["hour"])) == 1) {
			$_REQUEST["hour"] = "0{$_REQUEST["hour"]}";
		}
		if(empty($_REQUEST["minute"])) {
			$_REQUEST["minute"] = date("i");
		}
		if(strlen(trim($_REQUEST["minute"])) == 1) {
			$_REQUEST["minute"] = "0{$_REQUEST["minute"]}";
		}
		$time = "{$_REQUEST["hour"]}:{$_REQUEST["minute"]} {$_REQUEST["ampm"]}";

		$binds = array(
			":observed_date" 	=> $_REQUEST['date'],
			":observed_time" 	=> $time,
			":sd_id"			=> $sd_id,
			":observed_by" 		=> $_REQUEST['observed_by'],
			":location"			=> trim($_REQUEST['location']),
			":team_number" 		=> $_REQUEST['team_number'],
			":org" 				=> $_REQUEST['org'],
			":completed_by" 	=> $_REQUEST['completed_by'],
		);

		self::sot()->bind($stmt_main, $binds);

		$error = self::sot()->error();
		if($error) {
			die("<script type = 'text/javascript'>
				window.top.window.sot.tools.cover(false);
				alert(\"Error: Unable to submit Storm Duty Observation. Error Message: {$error["message"]}\");
				</script>
			");
		}

		$members = $_REQUEST['member'];
		$sql = "DELETE FROM STORM_DUTY_MEMBERS WHERE SD_ID = {$sd_id}";
		self::sot()->query($sql);
		foreach($members as $key=>$value) {
			self::sot()->bind($stmt_member, array(
				":sd_id" => $sd_id,
				":member" => $value
			));
		}

		foreach($_REQUEST as $key=>$value) {
			if(is_numeric($key)) {
				$comments = trim($_REQUEST["{$key}_comments"]);
				self::sot()->bind($stmt_item, array(
					":sd_id" => $sd_id,
					":item" => $key,
					":answer" => empty($value) ? 'NA' : $value,
					":comments" => $comments
				));
			}
		}

		self::sot()->commit();
		self::savePhotos('SD', $sd_id);

		$returnto = "index";
		if(isset($_REQUEST["returnto"]) && !$_GET["mobile"]){
			$returnto = $_REQUEST["returnto"];
		}
		die("<script type = 'text/javascript'>
				window.top.window.sot.tools.cover(false);
				window.top.window.location=\"../../" . ($_GET['mobile'] ? "mobile/" : "") . "{$returnto}.php?sd=1" . (empty($_REQUEST["delegator"]) || $_REQUEST["delegator"] == 0 ? "" : "&delegate={$_REQUEST["delegate"]}") . "\";
		</script>");

	}

    /**
     *
     */
    static function getObsTarget($NUM){
        //$oci = new mcl_Oci("soteria");
        $NUM = $_GET["TARGETTYPE"];
        $year = $_GET["yr"];
        $Temp2="right here";
        $startDayYear  = mktime(0, 0, 0, 1, 1 , $year);
        $endDayYear =  mktime(0, 0, 0, 12, 31 , $year);
       /* $sql =  "select TARGETNUM,TARGETTYPE
        from OBSTARGETS 
        where ROWNUM = 1 AND TARGETTYPE = {$NUM}
        ORDER BY DATESET desc;";*/

        $sql = "
		SELECT  TARGETNUM,TARGETTYPE
        from OBSTARGETS 
        where ROWNUM = 1 AND TARGETTYPE = '{$NUM}'
        AND YR ='{$year}'
        ORDER BY DATESET desc";



        if($row = self::sot()->fetch($sql)) {
           $sd_id = $row["TARGETNUM"];
           $Temp = strval($sd_id);
            $Temp2=$Temp;
        }
        else{
            $Temp2 = -1;
        }


        $StartMonthMon = mktime(0, 0, 0, 1, 1 - (date("w",$startDayYear) == 0 ? 7 : date("w", $startDayYear))+1, $year);

        $lastday = mktime(0, 0, 0, 12, 31 - date("w",$endDayYear), $year);

        if(date("Y")>$year)
        {
            $sendTarget = array("target"        => $Temp2,
                                "startDate"     => date("m-d-y",$StartMonthMon),
                                "endDate"       => date("m-d-y",$lastday),
                                "currentWeek"   => -1);
        }
        else
        {
            $end = mktime(0,0,0, date("m"), date("d") - (date("w") == 0 ? 7 : date("w")) + 7, date("Y"));
            $weeks = ceil(($end - $StartMonthMon) / 604800);
            $sendTarget =
            array(  "target"        => $Temp2,
                    "startDate"     => date("m-d-y",$StartMonthMon),
                    "endDate"       => date("m-d-y",$lastday),
                    "currentWeek"   => $weeks);
        }

        echo json_encode($sendTarget);
        //echo $sql;

        //echo $_GET["TARGETTYPE"];
    }

    static function getObsData()
    {
        $_GET["userId"];
        $_GET["startDate"];
        $_GET["endDate"];


        $obsData = new stdClass();
        $obsData->Month = 0;
        $obsData->Percentage = 0;
        $obsData->ytd = 0;

        $obsDataYear = array();

        $data1 = new $obsData();
        $data1->Month = 1;
        $data1->Percentage = 92;
        $data1->ytd = 92;


        $obsDataYear[]= $data1;

        $data2 = new $obsData();
        $data2->Month = 2;
        $data2->Percentage = 96;
        $data2->ytd = 94;


        $obsDataYear[]= $data2;

        $data3 = new $obsData();
        $data3->Month = 3;
        $data3->Percentage = 85;
        $data3->ytd = 91;


        $obsDataYear[]= $data3;

        $data4 = new $obsData();
        $data4->Month = 4;
        $data4->Percentage = 0;
        $data4->ytd = -1;


        $obsDataYear[]= $data4;
        $Date = "2/5/2017";
        $year = date("Y",strtotime($Date));
        $StartMonthMon = mktime(0,0,0, 1, 1 - (date("w") == 0 ? 7 : date("w")), $year);
        //$StartMonthMon =  date("m/d/Y", strtotime($StartMonthMon));

        $lastday = mktime(0, 0, 0, 1, date("t", strtotime($StartMonthMon)), $year);
        $lastdayd = date("m-d-Y",$lastday);

        //$startDate = @strtotime($StartMonthMon, 0);
        //$endDate = @strtotime($lastday, 0);
        $weeks = @ceil(($lastday - $StartMonthMon) / 604800);

        if(strtotime(date("m/d/Y"))< strtotime($Date))
        {
            $obsDataYear[]= "I work !!".date("Y",strtotime($Date))." Weeks ".$weeks. "  year ".$year."Last Day ".$lastdayd;
        }
        else{
            $obsDataYear[]= "BOOO !!";
        }

        echo json_encode($obsDataYear);
    }

    static function getObsComplianceData()
    {
        $userId = $_GET["userId"];
        $year = $_GET["yr"];
//        $obsDataYear = array();
//        for($i = 1; $i<=12;$i++){
//
//            $startDayMonth = mktime(0, 0, 0, $i, 1, $year);
//                $endDayMonth = mktime(0, 0, 0, $i, date("t", $startDayMonth), $year);
//
//                $StartMonthMon = mktime(0, 0, 0, $i, 1 - (date("w", $startDayMonth) == 0 ? 7 : date("w", $startDayMonth)) + 1, $year);
//                $lastday = mktime(0, 0, 0, $i, date("t", $startDayMonth) - date("w", $endDayMonth), $year);
//
//                $obsDataYear[] = date("m-d-Y",$StartMonthMon);
//                $obsDataYear[] = date("m-d-Y",$lastday);
//        }
//        echo json_encode($obsDataYear);

        //$year = date("Y",strtotime($endDateTime));

        $obsDataObject = new stdClass();
        $obsDataObject->status =0;
        $obsDataObject->month = 0;
        $obsDataObject->percentage = 0;
        $obsDataObject->ytd = 0;

        $obsDataYear = array();

//        $directTeam = self::GetDiectTeamCompliance($userId);
//        if(empty($directTeam)){
//            $obsDataObject->status =1;
//            $obsDataObject->month = 0;
//            $obsDataObject->percentage = 0;
//            $obsDataObject->ytd = 0;
//            $obsDataYear[] = $obsDataObject;
//            echo json_encode($obsDataYear);
//        }
//        else {

        //$totalPerctentYTD = 0;
        $actualTotal = 0;
        $expectedTotal = 0;
        $todaysDate = mktime(0,0,0,date("m"),date("d"),date("Y"));
        for ($i = 1; $i <= 12; $i++) {
            //We are establish the first day of the month then we are finding the first monday of the month and pass that
            //as the start date parameter to keep in line with the business rules we established

            //$firstDay = mktime(0, 0, 0, $i, 1, date("Y", strtotime($year)));
            //$firstDay =  date("m/d/Y", $firstDay);

            /*if(date("w",strtotime($firstDay)) == 0){
                $startDay=2;
            }
            elseif(date("w",$firstDay) == 1){
                $startDay = 1;
            }
            else{
                $startDay= date("d",strtotime($firstDay)) - date("w",strtotime($firstDay)) + 8;
            }*/
            $actualTotalMonth = 0;
            $expectedTotalMonth = 0;
            //$isPartial = false;

            $startDayMonth = mktime(0, 0, 0, $i, 1, $year);
            $endDayMonth = mktime(0, 0, 0, $i, date("t", $startDayMonth), $year);

            $StartMonthMon = mktime(0, 0, 0, $i, 1 - (date("w", $startDayMonth) == 0 ? 7 : date("w", $startDayMonth)) + 1, $year);
            $lastday = mktime(0, 0, 0, $i, date("t", $startDayMonth) - date("w", $endDayMonth), $year);

            //$StartMonthMon = mktime(0, 0, 0, $i, 1 - (date("w",$i.'-1-'.$year) == 0 ? 7 : date("w",$i.'-1-'.$year)) + 1, $year);
            //$lastday = mktime(0, 0, 0, $i, date("t", $StartMonthMon)-date("w",strototime(date("t", $StartMonthMon))), $year);

            //we are test if the lastday is greater than the endDateTime passed in if it is then its a partial month
            if ($lastday > strtotime(date("m-d-Y"))) {
                $lastday = mktime(0, 0, 0, $i, date("d") - date("w"), $year);
                //$isPartial = true;
            }
            //Initialize running percent total and how many user are being incremented this is need because of the exclusion table
//            $percentUser = 0;
//            $indexUser = 0;
            //Declare a new observation object to be inserted into an arrary
            $obsData = new $obsDataObject();
            //this checks to see if the month is greater the time on the system if it is then its out of range and we default it with numbers that
            //indicate its out of range
            if ($todaysDate < $StartMonthMon) {
                $obsData->status = 0;
                $obsData->month = $i;
                $obsData->percentage = 0;
                $obsData->ytd = -1;
                $obsDataYear[] = $obsData;
            } else {
//                foreach ($directTeam as $key => $value) {
                $returned = self::lcCompletionCompliance($userId, $StartMonthMon, $lastday);
                if ($returned["expected"] != -1) {
                    //totals per month
                    $actualTotalMonth += $returned["actual"];
                    $expectedTotalMonth += $returned["expected"];
                    //totals per year
                    $actualTotal += $returned["actual"];
                    $expectedTotal += $returned["expected"];
                }
//                }
                if ($expectedTotalMonth > 0) {
                    $percent = ($actualTotalMonth / $expectedTotalMonth) * 100;
                } else {
                    $percent = 0;
                }
                //ytd calculated total actual counts / total expected counts
                $totalPerctentYTD = ($actualTotal / $expectedTotal) * 100;
                $obsData->status = 0;
                /*
                 *Take out partial functionality
                if ($isPartial) {
                    $partialInd = $i;
                    $obsData->month = $partialInd;

                } else {
                    $obsData->month = $i;
                }
                */
                $obsData->month = $i;
                $obsData->percentage = round($percent, 2);
                $obsData->ytd = round($totalPerctentYTD, 2);
                $obsDataYear[] = $obsData;
            }
        }
        echo json_encode($obsDataYear);

    }

    public static function lcCompletionCompliance($usid,$startDt,$endDt){

        $startDate = @$startDt;
        $endDate = @$endDt;
        $weeks = @ceil(($endDate - $startDate) / 604800);

        $expected = 0;
        $actual = 0;

        for($i = 0; $i < $weeks; $i++) {

            $start = "(TRUNC(TO_DATE(:startDate, 'MM/DD/YYYY HH24:MI:SS'), 'IW') + :week * 7)";
            $end = "(NEXT_DAY(TRUNC(TO_DATE(:startDate, 'MM/DD/YYYY HH24:MI:SS'),'IW'), 'SUNDAY') + :week * 7) + .999";

            if(!self::$lc_stmt["lc_ex"]){
                self::$lc_stmt["lc_ex"] = self::sot()->parse("
					SELECT 	COUNT(USID) AS CT 
					FROM 	EMPLOYEES E
					WHERE 	PATH LIKE :usid 
							AND LEADERS > 0
							AND NOT EXISTS(
								SELECT 	1 
								FROM 	EXCLUSIONS EX 
								WHERE 	EX.USID = E.USID 
										AND ( START_DT <=  {$start}  OR START_DT IS NULL)
										AND ( END_DT >=  {$end}  OR END_DT IS NULL	)
							)
				");
            }

            self::sot()->bind(self::$lc_stmt["lc_ex"], array(
                ":usid"			=> "%{$usid}%",
                ":startDate"	=> date("m-d-Y",$startDate),
                ":week"			=> $i
            ));

            $row = self::sot()->fetch(self::$lc_stmt["lc_ex"]);
            $expected += intval($row['CT']);

            if(!self::$lc_stmt["lc"]){
                self::$lc_stmt["lc"] = self::sot()->parse("
					WITH T AS (
						SELECT 	USID 
						FROM 	EMPLOYEES E
						WHERE 	PATH LIKE :usid 
								AND LEADERS > 0
								AND NOT EXISTS(
									SELECT 	1 
									FROM 	EXCLUSIONS EX 
									WHERE 	EX.USID = E.USID 
											AND ( START_DT <=  {$start}  OR START_DT IS NULL)
											AND ( END_DT >=  {$end}  OR END_DT IS NULL	)
								)
					) 
					SELECT	COUNT(*) AS CT 
					FROM(
						SELECT 	OBSERVATION_CREDIT
						FROM 	T, 
								EPOP_OBSERVATIONS O 
						WHERE 	T.USID = O.OBSERVATION_CREDIT
								AND OBSERVED_DATE BETWEEN {$start} AND {$end}
								--AND (SWO = 1 OR PP = 1)
						GROUP BY OBSERVATION_CREDIT
					)
				");
            }

            self::sot()->bind(self::$lc_stmt["lc"], array(
                ":usid"			=> "%{$usid}%",
                ":startDate"	=> date("m-d-Y",$startDate),
                ":week"			=> $i
            ));

            $row = self::sot()->fetch(self::$lc_stmt["lc"]);
            $actual += intval($row['CT']);
        }
        //Creating an arrary to list the actual and expected for ytd totals

        if($expected == 0) {
            $expected -1;
        }

        $lcResults = array("expected" => $expected, "actual" => $actual);
        return $lcResults;

    }

    static function GetDiectTeamCompliance($userId){
        $directTeam = array();
        $sql = "
            SELECT 	USID, 
                    NAME, 
                    SUPERVISOR,
                    REPORTS, 
                    LEADERS, 
                    TEAM,
                    CASE WHEN USID = '{$userId}' THEN 1 ELSE 0 END AS ORDINAL
            FROM 	EMPLOYEES 
            WHERE 	(SUPERVISOR = '{$userId}' OR USID = '{$userId}')
            ORDER BY ORDINAL, NAME
        ";


        while($row = self::sot()->fetch($sql)){
            $directTeam[$row["USID"]] = $row;
        }
        return $directTeam;
    }

    static function getObsQualityData()
    {
        //DS-144
        $userId = $_GET["userId"];
        $year = $_GET["yr"];

        $obsDataObject = new stdClass();
        $obsDataObject->status =0;
        $obsDataObject->month = 0;
        $obsDataObject->percentage = 0.00;
        $obsDataObject->ytd = 0.00;

        $obsDataYear = array();

        $actualTotal = 0.00;
        $expectedTotal = 0.00;
        $todaysDate = mktime(0,0,0,date("m"),date("d"),date("Y"));
        for ($i = 1; $i <= 12; $i++) {
            //We are establish the first day of the month then we are finding the first monday of the month and pass that
            //as the start date parameter to keep in line with the business rules we established
            $actualTotalMonth = 0.00;
            $expectedTotalMonth = 0.00;
            //$isPartial = false;

            $startDayMonth = mktime(0, 0, 0, $i, 1, $year);
            $endDayMonth = mktime(0, 0, 0, $i, date("t", $startDayMonth), $year);

            $StartMonthMon = mktime(0, 0, 0, $i, 1 - (date("w", $startDayMonth) == 0 ? 7 : date("w", $startDayMonth)) + 1, $year);
            $lastday = mktime(0, 0, 0, $i, date("t", $startDayMonth) - date("w", $endDayMonth), $year);

            //we are test if the lastday is greater than the endDateTime passed in if it is then its a partial month
            if ($lastday > strtotime(date("m-d-Y"))) {
                $lastday = mktime(0, 0, 0, $i, date("d") - date("w"), $year);
            }
            //Initialize running percent total and how many user are being incremented this is need because of the exclusion table

            //Declare a new observation object to be inserted into an arrary
            $obsData = new $obsDataObject();
            //this checks to see if the month is greater the time on the system if it is then its out of range and we default it with numbers that
            //indicate its out of range
            if ($todaysDate < $StartMonthMon) {
                $obsData->status = 0;
                $obsData->month = $i;
                $obsData->percentage = 0;
                $obsData->ytd = -1;
                $obsDataYear[] = $obsData;
            } else {
//                foreach ($directTeam as $key => $value) {
                $returned = self::observationQuality($userId, $StartMonthMon, $lastday);
                if ($returned["expected"] != -1) {
                    //totals per month
                    $actualTotalMonth += $returned["actual"];
                    $expectedTotalMonth += $returned["expected"];
                    //totals per year
                    $actualTotal += $returned["actual"];
                    $expectedTotal += $returned["expected"];
                }
//                }
                if ($expectedTotalMonth > 0) {
                    $percent = ($actualTotalMonth / $expectedTotalMonth) * 100;
                } else {
                    $percent = 0;
                }
                //ytd calculated total actual counts / total expected counts
                $totalPerctentYTD = ($actualTotal / $expectedTotal) * 100;
                $obsData->status = 0;
                $obsData->month = $i;
                $obsData->percentage = round($percent, 2);
                $obsData->ytd = round($totalPerctentYTD, 2);
                $obsDataYear[] = $obsData;
            }
        }
        echo json_encode($obsDataYear);

    }

    public static function observationQuality($usid,$startDt,$endDt){

        $startDate = @$startDt;
        $endDate = @$endDt;
        $weeks = @ceil(($endDate - $startDate) / 604800);

        $expected = 0;
        $actual = 0;

        for($i = 0; $i < $weeks; $i++) {

            $start = "(TRUNC(TO_DATE(:startDate, 'MM/DD/YYYY HH24:MI:SS'), 'IW') + :week * 7)";
            $end = "(NEXT_DAY(TRUNC(TO_DATE(:startDate, 'MM/DD/YYYY HH24:MI:SS'),'IW'), 'SUNDAY') + :week * 7) + .999";

            if(!self::$lc_stmt["lc_ex"]){
                self::$lc_stmt["lc_ex"] = self::sot()->parse("
					WITH T AS (
						SELECT 	USID 
						FROM 	EMPLOYEES E
						WHERE 	PATH LIKE :usid 
								AND LEADERS > 0
								AND NOT EXISTS(
									SELECT 	1 
									FROM 	EXCLUSIONS EX 
									WHERE 	EX.USID = E.USID 
											AND ( START_DT <=  {$start}  OR START_DT IS NULL)
											AND ( END_DT >=  {$end}  OR END_DT IS NULL	)
								)
					) 
					SELECT	COUNT(*) AS CT 
					FROM(
						SELECT 	O.EPOP_ID
						FROM 	T, 
								EPOP_OBSERVATIONS O 
						WHERE 	T.USID = O.OBSERVATION_CREDIT
								AND O.OBSERVED_DATE BETWEEN {$start} AND {$end}

						GROUP BY O.EPOP_ID
					)
				");
            }

            self::sot()->bind(self::$lc_stmt["lc_ex"], array(
                ":usid"			=> "%{$usid}%",
                ":startDate"	=> date("m-d-Y",$startDate),
                ":week"			=> $i
            ));

            $row = self::sot()->fetch(self::$lc_stmt["lc_ex"]);
            $expected += intval($row['CT']);

            if(!self::$lc_stmt["lc"]){
                self::$lc_stmt["lc"] = self::sot()->parse("
					WITH T AS (
						SELECT 	USID 
						FROM 	EMPLOYEES E
						WHERE 	PATH LIKE :usid 
								AND LEADERS > 0
								AND NOT EXISTS(
									SELECT 	1 
									FROM 	EXCLUSIONS EX 
									WHERE 	EX.USID = E.USID 
											AND ( START_DT <=  {$start}  OR START_DT IS NULL)
											AND ( END_DT >=  {$end}  OR END_DT IS NULL	)
								)
					) 
					SELECT	COUNT(*) AS CT 
					FROM(
						SELECT 	O.EPOP_ID
						FROM 	T, 
								EPOP_OBSERVATIONS O
						JOIN EPOP_ANSWERS EA on EA.EPOP_ID = O.EPOP_ID 
						WHERE 	T.USID = O.OBSERVATION_CREDIT
								AND O.OBSERVED_DATE BETWEEN {$start} AND {$end}
                        AND O.CONVERSATION_TOOK_PLACE = 1
                        AND (O.RECOGNIZED_GOOD_BEHAVIOR = 1 OR EA.ANSWER='IO')
						GROUP BY O.EPOP_ID
                        
					)
				");
            }

            self::sot()->bind(self::$lc_stmt["lc"], array(
                ":usid"			=> "%{$usid}%",
                ":startDate"	=> date("m-d-Y",$startDate),
                ":week"			=> $i
            ));

            $row = self::sot()->fetch(self::$lc_stmt["lc"]);
            $actual += intval($row['CT']);
        }
        //Creating an arrary to list the actual and expected for ytd totals

        if($expected == 0) {
            $expected = -1;
            $actual = 0;
        }

        $lcResults = array("expected" => $expected, "actual" => $actual);
        return $lcResults;

    }

}

$user = auth::check();

if($user["status"] = "authorized"){
	if(!empty($_REQUEST["f"])){
		engine::setReportsGenerated();
		call_user_func("engine::{$_REQUEST["f"]}");
	}
} else {
	engine::printer('unathorized');
}
?>