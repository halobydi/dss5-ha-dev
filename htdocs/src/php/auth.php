<?php
require_once("mcl_Ldap.php");
mcl_Ldap::session_start('SOTERIA');

class auth
{
	static $user;
	public static function check($key = false) {
		if (empty(self::$user)) {		
			$user = false;
			if (mcl_Ldap::authorized()) {
				$user = mcl_Ldap::lookup($_SESSION["username"]);
			} 
			
			if (!$user) {
				self::$user = array('status' 		=> mcl_Ldap::get_error());
			} else {
				$oci = new mcl_Oci('soteria');
				
				$usid = strtolower($user['username']);
				$sql = "
					SELECT 
						DELEGATOR, 
						E.NAME,
						D_ID,
						ORG_CODE
					FROM  DELEGATES D
					LEFT JOIN EMPLOYEES E
							ON E.USID = D.DELEGATOR
					WHERE 	DELEGATEE = '{$usid}'
					ORDER BY E.NAME
				";
				
				$delegates = array();
				while($row = $oci->fetch($sql)){
					if(!empty($row["D_ID"]) && !empty($row["NAME"])){
						$delegates[$row["D_ID"]] = array(
							"name" => $row["NAME"],
							"usid"	=> $row["DELEGATOR"]
						);
					}
				}
				
				$sql = "SELECT * FROM USERS WHERE USID = '{$usid}'";
				$admin = false;
				$privileges = array();
				if($row = $oci->fetch($sql)) {
					$admin = 1;
					$privileges["MANAGE_ADMINS"] = $row["MANAGE_ADMINS"] == 1 ? true : false;
					$privileges["MANAGE_DELEGATES_EXCLUSIONS"] = $row["MANAGE_DELEGATES_EXCLUSIONS"] == 1 ? true : false;
					$privileges["MANAGE_SWO_ITEMS"] = $row["MANAGE_SWO_ITEMS"] == 1 ? true : false;
					$privileges["MANAGE_RED_TAG_AUDITS"] = $row["MANAGE_RED_TAG_AUDITS"] == 1 ? true : false;
					$privileges["MANAGE_AOE"] = $row["MANAGE_AOE"] == 1 ? true : false;
					$privileges["MANAGE_NEAR_MISS"] = $row["MANAGE_NEAR_MISS"] == 1 ? true : false;
					$privileges["ACCESS_STORM_DUTY"] = $row["ACCESS_STORM_DUTY"] == 1 ? true : false;
				}
				
				$sql = "
					SELECT  *
					FROM    EMPLOYEES E
					JOIN    EMPLOYEES E2
							ON E.SUPERVISOR = E2.USID
					WHERE   E.USID = '{$usid}'
							AND (UPPER(E.TITLE) LIKE '%SAFET%' OR UPPER(E2.TITLE) LIKE '%SAFETY%')
				";
				if($row = $oci->fetch($sql)) {
					$privileges["ACCESS_STORM_DUTY"] = true;
				}
				
				$org = $user["all"][0]["dtebusinessunitdeptidlongdesc"][0];
				self::$user = array(
					'status' 		=> 'authorized',
					'admin' 		=> $admin,
					'privileges' 	=> $privileges,
					'delegates' 	=> empty($delegates) ? false : $delegates,
					'usid' 			=> strtolower($user['username']),
					'name' 			=> $user['fname'],
					'org' 			=> trim(substr($org, 4)),
					'details' 		=> $user
				);
			}
		}
		
		if (isset($key) && !empty($key)) {
			if(!isset(self::$user[$key])) {
				return "";
			}
			return self::$user[$key];
		} else {
			return self::$user;
		}
	}
	
	public static function deny(){
		if (!empty(self::$user)) {
			self::$user['status'] = 'denied';
		}
		$url = 'http' . ($_SERVER['SERVER_PORT'] == '443' ? 's' : '') . '://' . $_SERVER['HTTP_HOST'];
		header("Location: {$url}");
	}
}

?>