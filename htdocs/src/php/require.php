<?php

	function errorHandler($errno, $errstr, $errfile, $errline, $errcontext){
		
	}
	set_error_handler('errorHandler');
	
	$baseDir =  'http' . ($_SERVER['SERVER_PORT'] == '443' ? 's' : '') . '://' . $_SERVER['SERVER_NAME'] . ":" . $_SERVER['SERVER_PORT'];
	
	require_once("mcl_Html.php");
	require_once("mcl_Header.php");
	require_once("mcl_Oci.php");
	require_once("mcl_Ldap.php");
	
	$colors = array(
		"border-top" 		=> "#b9c1c6",
		"border-bottom" 	=> "#6598cb",
		"color" 			=> "#d6d3d6",
		"background-color" 	=> "#292829",
		"splitter" 			=> "#848284"
	);
	
	mcl_Html::title('SOTeria - Safety Observation Tool');
	mcl_Header::colors($colors);

	mcl_Html::html5(true);
	mcl_Html::no_cache(true);
	
	mcl_Html::js(mcl_Html::DOJO);
	mcl_Html::js(mcl_Html::DOJO_WINDOW);
	mcl_Html::js(mcl_Html::AJAX);
	mcl_Html::js(mcl_Html::CALENDAR);
	mcl_Html::s(mcl_Html::INC_JS, "{$baseDir}/src/sot.js");
	
	require_once('/opt/apache/servers/soteria/htdocs/src/php/auth.php');
	
	$colors = array(
		"border-top" 		=> "#b9c1c6",
		"border-bottom" 	=> "#000",
		"color" 			=> "#d6d3d6",
		"background-color" 	=> "#292829",
		"splitter" 			=> "#848284"
	);

	
	mcl_Html::s(mcl_Html::INC_CSS, "{$baseDir}/src/css/general.css");
	mcl_Html::s(mcl_Html::INC_CSS, "{$baseDir}/src/css/menu.css");
	mcl_Html::s(mcl_Html::INC_CSS, "{$baseDir}/src/css/table.css");
	mcl_Html::s(mcl_Html::INC_CSS, "{$baseDir}/src/css/login.css");
	mcl_Html::s(mcl_Html::INC_CSS, "{$baseDir}/src/css/jscal2.css");

	if($_GET['desktop']){
		$_SESSION['desktop'] = true;
	}
	
	$ua = $_SERVER['HTTP_USER_AGENT'];
	$checker = array(
	  'iphone'=>preg_match('/iPhone|iPod|iPad/', $ua),
	  'blackberry'=>preg_match('/BlackBerry/', $ua),
	  'android'=>preg_match('/Android/', $ua),
	);
	
	if(!$_SESSION['desktop']){
		if($checker['iphone'] || $checker['android'])
			header("Location: {$baseDir}/mobile/index.php");
	}
	
	function quarter($returnDays = true){
		$tm = strtotime("now");  
		$q =  ceil(date("m", $tm) / 3);
		
		$days = date("z", strtotime("now"));
		$year = date("Y");
		
		if ($q == 1){
			$days_left = date("z", strtotime('03/31/' . $year)) - $days;
		} else if ($q == 2){
			$days_left = date("z", strtotime('06/30/' . $year)) - $days;
		} else if ($q == 3){
			$days_left = date("z", strtotime('09/30/' . $year)) - $days;
		} else if ($q == 4){
			$days_left = date("z", strtotime('12/31/' . $year)) - $days;
		}

		return $returnDays ? $days_left : $q;
	}

	//SUNDAY IS 0 (it goes 1, 2, 3, 4, 5, 6 0)
	
	$now = getDate();
	$start = mktime(0,0,0, date("m"), date("d") - (date("w") == 0 ? 7 : date("w")) - 6, date("Y"));
	$start =  date("m/d/Y", $start);

	$end = mktime(0,0,0, date("m"), date("d") - (date("w") == 0 ? 7 : date("w")), date("Y"));
	$end = date("m/d/Y", $end);
	
	mcl_Html::s(mcl_Html::SRC_JS, "
		var startWeek = '{$start}';
		var endWeek = '{$end}';
		
		sot.home.setDates('{$start}', '{$end}');
	");
	
	$startWeekPast = $start;
	$endWeekPast = $end;
	
	$start = mktime(0,0,0, date("m"), date("d") - (date("w") == 0 ? 7 : date("w")) + 1, date("Y"));
	$start =  date("m/d/Y", $start);

	$end = mktime(0,0,0, date("m"), date("d") - (date("w") == 0 ? 7 : date("w")) + 7, date("Y"));
	$end =  date("m/d/Y", $end);
	
	$startWeek = $start;
	$endWeek = $end;
	
	if (!empty($_REQUEST["start"]) && !empty($_REQUEST["end"])){
		$start = $_REQUEST["start"];
		$end = $_REQUEST["end"];
	}
	
	$about = "{$baseDir}/help/about.php" . ($_GET["delegate"] == 0 || empty($_GET["delegate"]) ? "" : "?delegate=" . $_GET["delegate"]);
	mcl_Header::version("<span style='cursor: pointer;' onclick='window.location=\"{$about}\"'>2.4.19</span>");
	
	$user = auth::check();
	$privileges = 0;
	if($user["status"] == "authorized"){
		mcl_Header::auth($user["name"]);
		mcl_Header::build('SOTeria');
		if(is_array($user["delegates"])){
			foreach($user["delegates"] as $key=>$value){
				$active = 0;
				if($_GET["delegate"] == $key){
					$active = 1;
				} else {
					$active = 0;
				}
				
				mcl_Html::s(mcl_Html::SRC_JS, "
					dojo.ready(function(){
						sot.tools.menu.addDelegator({name: \"{$value["name"]}\", d_id: {$key}, active: {$active}});
					});
				");
			}
			
			mcl_Html::s(mcl_Html::SRC_JS, "
				dojo.ready(function(){
					sot.tools.menu.addDelegator({name: 'Turn Off', d_id: 0, active: 0});
				});
			");
		}
	
		foreach($user["privileges"] as $key=>$value){
			if($value){
				mcl_Html::s(mcl_Html::SRC_JS, "
					dojo.ready(function(){
						sot.tools.menu.addPrivilege('{$key}');
					});
				");
				
				$privileges++;
			}
		}
		mcl_Html::s(mcl_Html::SRC_JS, "
			dojo.ready(function(){
				sot.tools.menu.setStormDutyAccess('{$user['privileges']['ACCESS_STORM_DUTY']}');
			});
		");
		
		$usid = (isset($_GET["delegate"]) && $_GET["delegate"] != 0 ? $user["delegates"][$_GET["delegate"]]["usid"] : $user["usid"]);
		$quarter = quarter(false);

		
		$location =  'http' . ($_SERVER['SERVER_PORT'] == '443' ? 's' : '') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		//Clean POST
		if (isset($_POST['username'])) {
			header("Location: " . $location);
		}
		
		//logout
		mcl_Html::s(mcl_Html::SRC_JS, "
			dojo.ready(function(){
				var logoutDiv = dojo.byId('header_login_auth');
				for(var i in logoutDiv.childNodes){
					if(typeof logoutDiv.childNodes[i] == 'object'){
						if(logoutDiv.childNodes[i].href){
							logoutDiv.childNodes[i].onclick = function(){
								window.location = '{$baseDir}/index.php?logout=true';
							}
						}
					}
				}
			});
		");
	
	} else {
		mcl_Header::hide_auth(true);
		mcl_Header::build('SOTeria');
		
		$err = $user["status"];
		mcl_Html::s(mcl_Html::SRC_JS, "
			dojo.ready(function(){
				sot.tools.login.showLogin('{$err}');
			});
		");
		
		//Cleanup Logout
		if (isset($_GET["logout"]) && $_GET["logout"] == "true") {
			header("Location: " . $location);
			die();
		}
		
		$menu = <<<MENU
			<div id = 'menu_divider' style='border-top: 1px solid black; background-color:black; width: 100%; height: 0px;'></div>
			<div class = 'menu' style = 'border-bottom: none;' id = 'menu'/>
				<a href = '#' >Home</a>
				<a href = '#' >Observations</a>
				<a href = '#' >Individual Reports</a>
				<a href = '#' >Organization Reports</a>
				<a href = '#' >Settings</a>
				<a href = '#' >Print</a>
				<a href = '#' >Help</a>
			</div>	
MENU
;
		die($menu);
	}
	

	$home = $baseDir . ($_GET["delegate"] == 0 || empty($_GET["delegate"]) ? "" : "?delegate=" . $_GET["delegate"]);
	$admin = ($privileges > 0 ? "<a href = '#' onmouseover = 'sot.tools.menu.showMenu(\"admin\");' id = 'admin'>Administration</a>" : "");
	$delegates = ($user["delegates"] ? "<a href = '#' onmouseover = 'sot.tools.menu.showMenu(\"delegate\");'  id = 'delegate'>Delegate</a>" : "");
	
	//build org selct
	$sql = "
		SELECT 	distinct 
				O.ORG_CODE, 
				O.ORG_TITLE,
				CASE WHEN E.ORG_CODE IS NOT NULL THEN 'SELECTED' ELSE NULL END AS SELECTED
		FROM 	ORGANIZATIONS O
		LEFT JOIN EMPLOYEES E 
			ON E.USID = '{$usid}'
			AND E.ORG_CODE = O.ORG_CODE
		WHERE 	ACTIVE = 1 
		ORDER BY O.ORG_CODE";

	if(isset($_GET["org"])){ $org = $_GET["org"]; }	
	$oci = new mcl_Oci('soteria');
	while($row = $oci->fetch($sql)){
		if($row['SELECTED'] == 'SELECTED' && !isset($org)) {
			$org = $row['ORG_CODE'];
			$selected = 'selected=selected';
		} else if(isset($org)) {
			$selected = ($org == $row['ORG_CODE'] ? 'selected=selcted' : '');
		}
		
		$org_select .= "<option value='{$row['ORG_CODE']}' {$selected}>{$row["ORG_TITLE"]}</option>";
	}
	

?>
<div id = 'menu_divider' style='border-top: 1px solid black; background-color:black; width: 100%; height: 0px;'></div>
<div class = 'menu' style = 'border-bottom: none;' id = 'menu'/>
	<a href = '<?php echo $home; ?>' id = 'home' onclick = ''>Home</a>
	<?php echo $delegates; ?>
	<?php echo $admin; ?>
	<a href = '#' onmouseover = 'sot.tools.menu.showMenu("obs");' id = 'obs'>Observations</a>
	<a href = '#' onmouseover = 'sot.tools.menu.showMenu("individualreports");' id = 'individualreports'>Individual Reports</a>
	<a href = '#' onmouseover = 'sot.tools.menu.showMenu("orgreports");' id = 'orgreports'>Organization Reports</a>
	<a href = '#' onmouseover = 'sot.tools.menu.showMenu("settings");' id = 'settings'>Settings</a>
	<a href = '#' onmouseover = 'sot.tools.menu.showMenu("print");' id = 'print'>Print</a>
	<a href = '#' onmouseover = 'sot.tools.menu.showMenu("help");' id = 'help'>Help</a>
	<a href='#' onclick='sot.home.back(); return false;' id='back' style='display: none;'>Back</a>
</div>
