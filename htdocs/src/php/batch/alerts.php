<?php
	
	require_once("mcl_Oci.php");
	$oci = new mcl_Oci("soteria");
	$dev = $_SERVER['SERVER_NAME']  == 'lnx829' ? true : false;
	
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	$headers .= 'From: SOTERIA@dteenergy.com' . "\r\n";
	$headers .= 'Reply-To: SOTERIA@dteenergy.com' . "\r\n";
	
	$fromEmail = 'SOTERIA@dteenergy.com';
	
	$subject = 'SOTeria Weekly SWO Reminder';

	function buildMessage($params, $lc = '0%', $lcReport = ""){
		
		$cell = $params['TYPE'] == 'C' ? true : false;
		$complete = $params['COMPLETE'] >= 1 ?  true : false;
		$date = $params['DT'];
		$name = $params['NAME'];
	
		if($cell){
			$message = ($complete ? "" : "This is a reminder that you have not completed a Safe Worker Observation for the current week. ");
			$message .= "Your current Weekly SWO Leader Completion is {$lc}.";
		} else {
			if(empty($date)){
				$records = "Our records indicate you have not completed any observations using <a href='https://soteria.dteco.com'>SOTeria</a>.";
			} else {
				$records = "Our records indicate that the last observation you completed using <a href='https://soteria.dteco.com'>SOTeria</a> was on {$date}.";
			}
			
			$message = "<div style='font-size: 12px; font-family: Arial;'>";
			$message .= "{$name},";
			$message .= "<br />";
			$message .= "<br />";
			
			if(!$complete) {
				$message .= "This is a reminder that you have not completed a Safe Worker Observation for the current week. Each leader is required to complete at least one observation per week. ";
				$message .= "{$records} ";
				$message .= "If this is incorrect, please disregard this message; otherwise please use <a href='https://soteria.dteco.com'>SOTeria</a> to complete an observation by the end of the day Sunday.";
			} else {
				$message .= "Thank you for completing your SWO observation this week!";
			}
			
			$today = date('l h:i:s A');
			$message .= " Please find below your team's SWO completion as of {$today}. See <a href='https://soteria.dteco.com'>SOTeria</a> for further details.";
			
			$message .= "<br />";
			$message .= "<br />";
			
			if(!empty($lcReport)) {
				$message .= $lcReport;
			}
			
			global $dev;
			$message .= ($dev ? '<h4>TESTING ENVIRONMENT - PLEASE DISREGARD THIS MESSAGE</h4>' : '');
			$message .= "</div>";
		}
				
		return $message;
	}	
	
	require_once('/opt/apache/servers/soteria/htdocs/src/php/engine.php');

	$start = mktime(0,0,0, date("m"), date("d") - (date("w") == 0 ? 7 : date("w")) + 1, date("Y"));
	$start =  date("m/d/Y", $start);

	$end = mktime(0,0,0, date("m"), date("d") - (date("w") == 0 ? 7 : date("w")) + 7, date("Y"));
	$end =  date("m/d/Y", $end);
	
	$_REQUEST['startDate'] = $start;
	$_REQUEST['endDate'] = $end;
	
	$direct_team_stmt = $oci->parse("
		SELECT	*
		FROM	EMPLOYEES
		WHERE	(SUPERVISOR = :usid OR USID = :usid)
				AND LEADERS > 0
		ORDER	BY CASE WHEN USID = :usid THEN 0 ELSE 1 END DESC, NAME
	");
	
	$sql = "
		WITH T AS(
			SELECT 	E.USID,
					NAME,  
					ADDRESS,
					TYPE
			FROM NOTIFICATION_SETTINGS S,
				EMPLOYEES E
			WHERE E.ORG_CODE IN (
				SELECT 	ORG_CODE 
				FROM	ORGANIZATIONS
				WHERE ACTIVE = 1
			)
			AND S.NOTIFY = 1
			AND S.USID = E.USID
			AND LEADERS > 0
		)
		SELECT 	USID,
				NAME,
				ADDRESS,
				TYPE,
				TO_CHAR(DT, 'MM/DD/YYYY') AS DT,
				M.COMPLETE
		FROM 	T 
		LEFT JOIN (
			SELECT 	MAX(OBSERVED_DATE) AS DT, 
					SUM(CASE WHEN OBSERVED_DATE BETWEEN TRUNC(SYSDATE, 'IW') AND TRUNC(SYSDATE, 'IW') + 6
						AND OBSERVATION_CREDIT IS NOT NULL THEN 1 ELSE 0 END) AS COMPLETE,
					OBSERVATION_CREDIT 
			FROM 	EPOP_OBSERVATIONS O 
				GROUP BY OBSERVATION_CREDIT
		) M
		ON M.OBSERVATION_CREDIT = T.USID
		WHERE NOT EXISTS (
			  SELECT 1 FROM EXCLUSIONS E
			  WHERE E.USID = T.USID
			  AND ( START_DT <=  TRUNC(SYSDATE, 'IW')  OR START_DT IS NULL	) 
			  AND ( END_DT >=  TRUNC(SYSDATE, 'IW') + 6  OR END_DT IS NULL	)
		)
		AND NOT EXISTS (
			SELECT 	1
			FROM	NOTIFICATIONS_SENT NS
			WHERE DT BETWEEN TRUNC(SYSDATE, 'IW') AND TRUNC(SYSDATE, 'IW') + 6
				AND NS.USID = T.USID 
		)
	";
	
	$notifications = array();
	while($row = $oci->fetch($sql)){
		$usid = $row["USID"];
		
		$oci->bind($direct_team_stmt, array(
			":usid" => $usid
		));
		
		$lcReport = "<table style='border: 1px solid #000; font-size: 11px; font-family: Arial; border-collapse: collapse;'>";
		$lcReport .= "<thead>";
		$lcReport .= "<tr>";
		$lcReport .= "<th style='border: 1px solid #000; text-align: center; background-color: #6598cb; font-weight: normal;'>Leader</th>";
		$lcReport .= "<th style='border: 1px solid #000; text-align: center; background-color: #6598cb; font-weight: normal;'>Employee & Their Team<br/>Completion</th>";
		$lcReport .= "</thead>";
		$lcReport .= "<tbody>";
		
		while($row2 = $oci->fetch($direct_team_stmt, false)) {
			
			$lc = engine::lcCompletion($row2['USID'], $row2['LEADERS']);
			$rowColor = ($i++ % 2 == 0 ? '#f0f0f0' : '#cecece');
					
			if($row2['USID'] == $usid) {
				if($lc < 60) {
					$bgColor = '#ffaaad';
				} elseif($lc < 80) {
					$bgColor = '#ffffbd';
				} else {
					$bgColor = '#b5d3b5';
				}

				$style = "border: 1px solid #000; font-weight: bold; background-color: {$bgColor};";
			} else {
				$style = 'border-left: 1px solid #000; border-right: 1px solid #000;';
			}
			
			if($lc !== '--') { $lc .= '%'; }
			
			$lcReport .= "<tr style='background-color: {$rowColor};'>";
			$lcReport .= "<td style='{$style} padding: 3px; width: 250px;'>{$row2['NAME']}</td>";
			$lcReport .= "<td style='{$style} text-align: center; width: 150px;'>{$lc}</td>";
			$lcReport .= "</tr>";
		}
		
		$lcReport .= "</tbody>";
		$lcReport .= "</table>";

		$complete = $row['COMPLETE'];
		
		if($complete && $lc >= 80) {
			continue; //complete for week
		}
		
		if(!$lc) {
			$lc = '0%';
		}
		
		$message = buildMessage($row, $lc, $lcReport);
		$to = $row["ADDRESS"];

		$notifications[$usid] = array(
			"to" => $to,
			"message" => $message,
			"subject" => "Soteria Weekly SWO Reminder - {$lc}"
		);
		
	}
	
	$stmt_insert = $oci->parse("
		INSERT INTO NOTIFICATIONS_SENT VALUES (:usid, SYSDATE)
	");
	
	foreach($notifications as $key=>$value){
	
		if($dev) {
			$value['to'] = 'SOTERIA@dteenergy.com';
		}
		
		if(mail($value["to"], $value['subject'] ?: $subject, $value["message"], $headers, "-f{$fromEmail}")){
			$oci->bind($stmt_insert, array(
				":usid"	=> $key
			));
		} else {
			echo '<div>Unable to send message to ' . $value['to'] . '</div>';
		}
		
	}
	
	$oci->commit();
?>