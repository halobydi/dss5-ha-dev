<?php
	set_time_limit(3000000);
	require_once("mcl_Oci.php");

	echo "<pre>";
	function error_handler($errorNumber, $errorMessage, $errorFile, $errorLine, $errorContext) {
		if($errorNumber == 2) return; //ignore date error messages
		$message = var_export($errorMessage, true);
		sendMail($message);
		
	}
	set_error_handler('error_handler');
	
	function sendMail($message) {
		mail('SOTERIA@DTEENERGY.COM,mohamedm@dteenergy.com', 'Soteria Employee Batch Failure', $message, 'From:SOTERIA@DTEENERGY.COM');
	}
	
	$maximo = new mcl_Oci("maximo");
	$sot = new mcl_Oci("soteria");
	
	$start = microtime(true);
	
	if(!$maximo->getConn()) {
		sendMail("Unable to connect to Maximo Database");
	}
	
	$sql = "SELECT COUNT(*) AS CT FROM DOBW.EMPLOYEE WHERE TRIM(SUPERVISOR_USER_ID) IS NULL";
	$row = $maximo->fetch($sql);
	if($row['CT'] != 1) {
		sendMail('Unable to build employee table because employee hierarchy is not defined!');
		exit;
	}
	
	echo "Updating Employee Table for SOTeria Production on " . date("m/d/Y H:i:s") . "</br>";
	$sql = "
		SELECT 	USER_ID AS USID,
				GIVEN_NAME || ' '  || LAST_NAME AS NAME,
				SUPERVISOR_USER_ID AS SUPERVISOR,
				WORK_LOCATION AS LOCATION,
				TITLE,
				LEVEL,
				SYS_CONNECT_BY_PATH(USER_ID, '/') AS PATH,
				A.DESCRIPTION AS ORG_DESCRIPTION,
				EMAIL_ID,
				SAP_ID,
				ORG_DEPTID
		FROM 	DOBW.EMPLOYEE E
		LEFT JOIN ALNDOMAIN A
	        ON A.VALUE = E.BUSINESS_UNIT_DEPTID 
			AND A.DOMAINID = 'DTE_DEPTCODE'
		WHERE EMP_GROUP != 4
			AND TITLE NOT LIKE '%Temporary%'
			AND TITLE NOT LIKE '%Student%'
		START WITH TRIM(SUPERVISOR_USER_ID) IS NULL
		CONNECT BY PRIOR USER_ID = SUPERVISOR_USER_ID
	";
	

	echo "<pre>Deleting from employees<br/></pre>";
	$sql_delete = "
		DELETE FROM EMPLOYEES
	";
	if(!$sot->query($sql_delete)) {
		sendMail("Unable to delete from employees to refresh hierarchy.");
	}
	
	$stmt_insert = $sot->parse("
		INSERT INTO EMPLOYEES (
			USID,
			NAME,
			TITLE,
			LOCATION,
			SUPERVISOR,
			\"PATH\",
			ORG_CODE,
			ORG_DESCRIPTION,
			REPORTS,
			LEADERS,
			TEAM,
			EMAIL,
			SAP_ID,
			ORGH
		) VALUES (
			:usid,
			:name,
			:title,
			:location,
			:supervisor,
			:path,
			(SELECT ORG_CODE FROM ORGANIZATIONS WHERE ORG_DESCRIPTION = :org_desc),
			:org_desc,
			:reports,
			:leaders,
			:team,
			:email,
			:sapid,
			:orgh
		)
	");
	
	$inserted = 0;
	while($row = $maximo->fetch($sql)){
		if($sot->bind($stmt_insert, array(
				":usid"			=> $row["USID"],
				":name"			=> $row["NAME"],
				":title"		=> $row["TITLE"],
				":location"		=> $row["LOCATION"],
				":supervisor"	=> $row["SUPERVISOR"],
				":path"			=> $row["PATH"],
				":org_desc"		=> $row["ORG_DESCRIPTION"],
				":reports"		=> 0,
				":leaders"		=> 0,
				":team"			=> 0,
				":email"		=> $row["EMAIL_ID"],
				":sapid"		=> $row["SAP_ID"],
				":orgh"			=> $row["ORG_DEPTID"]
		))){
				$inserted++;
				$sot->commit();
			} else {
			
				echo "Unable to insert employee {$row['USID']}.<br/>";
				echo "<pre>";
				var_dump($sot->error());
				echo "</pre>";
				
			}
	}
	
	echo "<pre>Updating Employees Reports ...";
	$sql = "
		UPDATE EMPLOYEES E 
		SET REPORTS = (
			SELECT 	COUNT(*) 
			FROM 	EMPLOYEES 
			WHERE 	SUPERVISOR = E.USID
		)
	";
	var_dump($sot->query($sql));
	echo "</pre>";
	
	echo "<pre>Updating Employees Team";
	$sql = "
		UPDATE EMPLOYEES E 
		SET TEAM = (
			SELECT 	COUNT(*)
			FROM 	EMPLOYEES 
			START 	WITH SUPERVISOR = E.USID 
			CONNECT BY PRIOR USID = SUPERVISOR
		) 
	";
	var_dump($sot->query($sql));
	echo "</pre>";
	
	echo "<pre>Updating Employees Leaders ...";
	$sql = "
		UPDATE EMPLOYEES E 
		SET 	LEADERS = (
					SELECT 
						COUNT(*) AS CT 
					FROM 
						EMPLOYEES 
					WHERE (((
							TITLE LIKE '%Supervisor%' OR 
							TITLE LIKE '%Manager%' OR 
							TITLE LIKE '%Director%' 
							OR TITLE LIKE 'Supervising%'
							OR TITLE LIKE 'VP%'
							OR TITLE LIKE 'President%'
							OR TITLE LIKE 'Vice President%'
						) AND TITLE NOT IN (
							'Project Manager - IT',
							'Supervisor - Dist Ops/ProcessCoordinator',
							'Senior System Supervisor',
							'IT Project Manager',
							'Account Case Manager - Assigned Accounts',
							'Senior Project Manager - Construction',
							'Project Manager - Construction',
							'Principal Project Manager - Construction',
							'Associate Project Manager - Construction'
						) AND TITLE NOT LIKE 'System Supervisor%') 
						OR (TITLE = 'Senior System Supervisor' AND REPORTS > 0) 
						OR  REPORTS > 0 
					) START WITH USID = E.USID
				CONNECT BY PRIOR USID = SUPERVISOR
			) 
	";
	var_dump($sot->query($sql));
	echo "</pre>";

	echo "<pre>Updating VP/President to respective Organization instead of Corporate Office";
	$sql = "
		WITH T AS (
				SELECT  E.USID,
				E.NAME,
				E2.ORG_CODE AS NEW_ORG_CODE,
				E2.ORG_DESCRIPTION AS NEW_ORG_DESCRIPTION,
				COUNT(*) AS CT
		FROM    EMPLOYEES E
		LEFT    JOIN EMPLOYEES E2
				ON E2.SUPERVISOR = E.USID
		WHERE   E.ORG_CODE = 'CORP OFF'
				AND E2.ORG_CODE != 'CORP OFF'
				GROUP BY  E.USID,
						E.NAME,
						E2.ORG_CODE,
						E2.ORG_DESCRIPTION
		)
		SELECT  USID,
				NEW_ORG_CODE,
				NEW_ORG_DESCRIPTION,
				CT
		FROM    T
				ORDER BY USID, CT ASC
	";
	$stmt_update = $sot->parse("
		UPDATE EMPLOYEES
			SET ORG_CODE = :org_code,
				ORG_DESCRIPTION = :org_description
			WHERE	USID = :usid
	");
	$total = 0;
	while($row = $sot->fetch($sql)) {
		$total++;
		$sot->bind($stmt_update, array(
			":org_code" => $row['NEW_ORG_CODE'],
			':org_description' => $row['NEW_ORG_DESCRIPTION'],
			':usid' => $row['USID']
		));
		$sot->commit();
	}
	echo "... Done updated {$total} employees</pre>";
	
	$end = microtime(true);
	echo "</br>Batch took " .  ($end - $start) . " seconds to run. Inserted {$inserted} Employees";
	
	$ob = ob_get_contents();
	$ob = str_replace("<br/>", "\n", $ob);
	$ob = str_replace("</br>", "\n", $ob);
	$ob = str_replace("<pre>", "", $ob);
	$ob = str_replace("</pre>", "", $ob);
	
	$file = "employeeBatch.txt";
	$fh = @fopen($file, 'w') or die(" Can't open file ");
	@fwrite($fh, $ob);
	fclose($fh);

?>