<?
function getPhotos($form, $id, &$oci, $edit = false) {
	$x = 0;
	$photos = array();
	$sql = "SELECT * FROM PHOTOS WHERE OBSERVATION_FORM = '{$form}' AND OBSERVATION_ID = {$id}";
	while($row = $oci->fetch($sql)) {
		$photos[] = $row; 
	}
	echo "<div>";
	foreach($photos as $value){
		
		echo "<div style='float: left; width: 155px; margin-top: 2px; margin-right: 2px;'>";
			echo "<input type = 'hidden' name = 'deleted_{$x}' id = 'deleted_{$x}' value = '0'/>";
			echo "<input type = 'hidden' name = 'pid_{$x}' value = '{$value["PHOTO_ID"]}'/>";
			echo "<div id = 'photo_{$x}'>";
				echo "<div style='border: 1px solid #000; padding: 1px; background-color: #fff; margin-right: 1px; cursor: pointer;' onclick='var win = window.open(\"viewPhoto.php?photo_id={$value["PHOTO_ID"]}&original=true\", \"\", \"width=1000, toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes\");'>";
					echo "<img width='150' height='100' src='viewPhoto.php?photo_id={$value["PHOTO_ID"]}' />";	
				echo "</div>";
				echo "<div style='height: 40px; overflow: auto; font-size: 10px; color: gray; background-color: #ececec; padding: 2px; border-right: 1px solid #000; border-left: 1px solid #000; border-bottom: 1px solid #000; margin-right: 1px;'>{$value["SHORT_DESCRIPTION"]}</div>";
				
				if($edit) {
					echo "<span style='cursor: pointer; font-size: 10px;' onclick = 'removePhoto({$x}, true);'>";
					echo "[ <img width='12' height='12' style='vertical-align: bottom;' src=\"https://soteria.dteco.com/src/img/x.png\" /> <a href='#' onclick='return false;'>Remove</a> ]";
					echo "</span>";
				}
				
			echo "</div>";
		echo "</div>";

		$x++;
		
		echo "<script type = 'text/javascript'>p_count++;</script>";
	}
	
	if($x > 0) echo "<div style='clear:both;'></div>";
	echo "</div>";
	
	return $x;
}
?>