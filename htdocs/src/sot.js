var baseDir = window.location.protocol + "//" + window.location.host;

var resizer = function() {
	var login = dojo.byId("login_container"); 	
	var popup_box = dojo.byId("popup_box");

	var wb = dojo.window.getBox();
	if(login){
		login.style.top = wb.h / 2 - login.offsetHeight / 2 + "px";
		login.style.left = wb.w / 2 - login.offsetWidth / 2 + "px";
		if(popup_box){ document.body.removeChild(popup_box); }
	}
	
	if(popup_box){
		popup_box.style.top = wb.h / 2 - popup_box.offsetHeight / 2 + "px";
		popup_box.style.left = wb.w / 2 - popup_box.offsetWidth / 2 + "px";
	}
	
	var menu = document.getElementById('menu');

	if (!menu) {
		return;
	}
	
	var menu_divider = dojo.byId("menu_divider");
	
	var minWidth = 900;
	if (wb.w < minWidth) {
		menu.style.width = minWidth + 'px';
		menu_divider.style.width = (minWidth + 1) + 'px';
		menu_divider.style.height = '0px';
	} else {
		menu.style.width = (document.body.scrollWidth - 1) + 'px';
		menu_divider.style.width = (document.body.scrollWidth) + 'px';
		menu_divider.style.height = '0px';
	}

};


dojo.ready(function() {
	resizer();
	dojo.connect(window, "onresize", function() {
		resizer();
	});
});

var p_count = 0;
var addPhoto = function() {
	var photo = document.createElement('div');
	photo.style.padding = '3px';
	photo.id = 'photo_' + p_count;
	
	var innerHTML =  "<span style='font-size: 10px;'>New Photo</span><br/><input type = 'hidden' name = 'pid_" + p_count + "' value = ''/>";
		innerHTML += "<input type = 'hidden' name = 'deleted_" + p_count + "' id = 'deleted_" + p_count + "' value = '0'/>";
		innerHTML += "<input style = 'border: 1px solid black; margin-right: 20px;' type = 'file' name = 'file_" + p_count + "'/><br/>";
		innerHTML += "<span style='font-size: 10px;'>Short Description of Photo</span><br/><textarea style = 'border: 1px solid black; width: 232px; height: 30px;' name = 'p_desc_" + p_count + "'></textarea>";
		innerHTML += "<span class = 'delete_a' style = 'cursor: pointer; font-size: 10px;' onclick = 'removePhoto(" + p_count + ", false);'>";
		innerHTML += " [ <img src=\"https://soteria.dteco.com/src/img/x.png\" width='12' height='11' style='vertical-align: bottom;'/> <a href='#' onclick='return false;'>Remove</a> ]";
		innerHTML += "</span>";
		
	photo.innerHTML = innerHTML;
	p_count++;

	document.getElementById("photos").appendChild(photo);
	document.getElementById("p_count").value = p_count;
};

var removePhoto = function(num, provideConfirmation) {
	if(provideConfirmation && !confirm("Are you sure you want to remove this photo?\nThis action cannot be undone.")) {
		return;
	}
	
	try {
		document.getElementById('deleted_' + num).value = '1';
		document.getElementById('photo_' + num).style.display = 'none';
	} catch(ex) {
		
	}
};

var excel = function(removeLastCell) {
	var thisTable = document.getElementById('exportArea').innerHTML;
	dojo.byId("temp_clipboard").style.display = 'none';
	dojo.byId("temp_clipboard").innerHTML = thisTable;
	
	var original = document.getElementById('exportArea').getElementsByTagName("table")[0];
	var table = document.getElementById("temp_clipboard").getElementsByTagName("table")[0];
	for ( var i = 0; row = table.rows[i], row_o = original.rows[i]; i++ ) {
		
		row = table.rows[i];
		
		for ( var j = 0; col = row.cells[j]; j++ ) {
			
			if(removeLastCell && j == (row.cells.length - 1)) {
				row.cells[j].innerHTML = "";
				continue;
			}
			
			if(row.cells[j].innerHTML.indexOf("BUTTON") != -1) {
				row.cells[j].innerHTML = "";
				continue;
			}
			if(row.cells[j].innerHTML.indexOf("INPUT") != -1) {
				try {
					var value = dojo.byId(row_o.cells[j].childNodes[0].id).value;
					row.cells[j].innerHTML = "";
				} catch(ex) {
					
				}
			}
			if(row.cells[j].innerHTML.indexOf("SELECT") != -1) {
				try {
					row.cells[j].innerHTML = "";
				} catch(ex) {
					
				}
			}
			if(row.cells[j].innerHTML.indexOf("FORM") != -1) {
				try {
					row.cells[j].innerHTML = "";
				} catch(ex) {
					
				}
			}
			if(row.cells[j].innerHTML.indexOf("TEXTAREA") != -1) {
				try {
					var value = dojo.byId(row_o.cells[j].childNodes[0].id).value;
					row.cells[j].innerHTML = value;
				} catch(ex) {
					
				}
			}
			if(row.cells[j].innerHTML.indexOf("IMG") != -1) {
				row.cells[j].innerHTML = "";
			}
			if(row.cells[j].innerHTML.indexOf("A") != -1 || row.cells[j].innerHTML.indexOf("href") || j == row.cells.length) {
				try {
					var value = dojo.byId(row_o.cells[j].childNodes[0].id).value;
					row.cells[j].innerHTML = "";
				} catch(ex) {
					
				}
			}
		}
	}
	

	window.clipboardData.setData('text', table.parentNode.innerHTML);
	var objExcel = new ActiveXObject ('Excel.Application');
	objExcel.visible = true;
	var objWorkbook = objExcel.Workbooks.Add;
	var objWorksheet = objWorkbook.Worksheets(1);
	objWorksheet.Paste; 
	
};

var defaultValueHelper = function(o, defaultVal) {
	if(o.value == "") {
		o.value = defaultVal;
	} else if(o.value == defaultVal){
		o.value = "";
	}
};

var position = function(obj){
	var curLeft = 0;
	var curTop = 0;
	if (obj.offsetParent) {
		do {
			curLeft += obj.offsetLeft;
			curTop += obj.offsetTop;
		} while (obj = obj.offsetParent);
		
		return {left: curLeft, top: curTop};
	}
	
	curLeft = obj.offsetLeft;
	curTop = obj.offsetTop;
	
	return {left: curLeft, top: curTop};
};

var allowDays = function(field, days) {
	var date = new Date(dojo.byId(field).value);
	console.log(date);
	console.log(date.getDay());
	if(!days[date.getDay()]) {
		
	}
};

var js_calendar;
var setup_cal = function(id, field, disable) {
	if (!js_calendar) {
		js_calendar = Calendar.setup({
			animation: false,
			onSelect: function() { 
				this.hide();
			},
			disabled: function(date) {
				if(!disable) {
					return false;
				}
				
				if (date.getDay() == 1 || date.getDay() == 0) {
					return false;
				} else {
					return true;
				}
			}
		});
	}
	
	js_calendar.manageFields(id, field, '%m/%d/%Y');
};

dojo.registerModulePath('sot', baseDir + '/src/js');

dojo.require('sot.tools.cover');
dojo.require('sot.tools.login');
dojo.require('sot.tools.menu');
dojo.require('sot.tools.ajax');

dojo.require('sot.home');
dojo.require('sot.swo');
dojo.require('sot.redtag');
dojo.require('sot.hp');
dojo.require('sot.nm');
dojo.require('sot.gc');
dojo.require('sot.feedback');