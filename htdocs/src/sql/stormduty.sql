ALTER TABLE NEAR_MISS_OBSERVATIONS ADD EDIT_USID VARCHAR2(6);

CREATE TABLE STORM_DUTY_ITEMS (
        ITEM_NUM NUMBER PRIMARY KEY,
        ITEM_CATEGORY VARCHAR2(500),
        ITEM VARCHAR2(4000),
        COMMENTS_ONLY NUMBER DEFAULT 0
);

CREATE SEQUENCE STORM_DUTY_ITEM_UNIQUE;

INSERT INTO STORM_DUTY_ITEMS
SELECT  
        STORM_DUTY_ITEM_UNIQUE.NEXTVAL, 
        'Observed Employee', 
        'When did you last take the Public Safety Training?', 
        0
FROM    DUAL

INSERT INTO STORM_DUTY_ITEMS
SELECT  
        STORM_DUTY_ITEM_UNIQUE.NEXTVAL, 
        'Observed Employee', 
        'Was a Pre Job Breif conducted at the location?', 
        0
FROM    DUAL

INSERT INTO STORM_DUTY_ITEMS
SELECT  
        STORM_DUTY_ITEM_UNIQUE.NEXTVAL, 
        'Observed Employee', 
        'Does the Public Safety Team understand the actual or potential hazards? ', 
        0
FROM    DUAL

INSERT INTO STORM_DUTY_ITEMS
SELECT  
        STORM_DUTY_ITEM_UNIQUE.NEXTVAL, 
        'Observed Employee', 
        'Do the Public Safety Team members understand the minimum approach distance (distance between themselves and the electrical hazard? 20 Feet).',
        0
FROM    DUAL

INSERT INTO STORM_DUTY_ITEMS
SELECT  
        STORM_DUTY_ITEM_UNIQUE.NEXTVAL, 
        'Observed Employee', 
        'Is the barrier tape located 20 feet from the electrical hazard?',
        0
FROM    DUAL

INSERT INTO STORM_DUTY_ITEMS
SELECT  
        STORM_DUTY_ITEM_UNIQUE.NEXTVAL, 
        'Observed Employee', 
        'Practicing Good Housekeeping?',
        0
FROM    DUAL

INSERT INTO STORM_DUTY_ITEMS
SELECT  
        STORM_DUTY_ITEM_UNIQUE.NEXTVAL, 
        'Observed Employee', 
        'Is the member wearing the appropriate PPE? (Hard and reflective vest)?',
        0
FROM    DUAL

INSERT INTO STORM_DUTY_ITEMS
SELECT  
        STORM_DUTY_ITEM_UNIQUE.NEXTVAL, 
        'Observed Employee', 
        'Is the team member aware of the effect of his/her work on others?',
        0
FROM    DUAL

INSERT INTO STORM_DUTY_ITEMS
SELECT  
        STORM_DUTY_ITEM_UNIQUE.NEXTVAL, 
        'Observed Employee', 
        'Were the customers notified of the hazards?',
        1
FROM    DUAL


INSERT INTO STORM_DUTY_ITEMS
SELECT  
        STORM_DUTY_ITEM_UNIQUE.NEXTVAL, 
        'Safety Equipment/Proper Dress', 
        'Barrier Type',
        0
FROM    DUAL

INSERT INTO STORM_DUTY_ITEMS
SELECT  
        STORM_DUTY_ITEM_UNIQUE.NEXTVAL, 
        'Safety Equipment/Proper Dress', 
        'Two Safety Vests',
        0
FROM    DUAL

INSERT INTO STORM_DUTY_ITEMS
SELECT  
        STORM_DUTY_ITEM_UNIQUE.NEXTVAL, 
        'Safety Equipment/Proper Dress', 
        'Map & Customer Information',
        0
FROM    DUAL

INSERT INTO STORM_DUTY_ITEMS
SELECT  
        STORM_DUTY_ITEM_UNIQUE.NEXTVAL, 
        'Safety Equipment/Proper Dress', 
        'Are vehicles parked in a safe manner? <br/>Vehicles should not be used as a barrier or warning device. Vehicles should be parked in a manner which will not expose employees to being struck by vehicular traffic.',
        0
FROM    DUAL

INSERT INTO STORM_DUTY_ITEMS
SELECT  
        STORM_DUTY_ITEM_UNIQUE.NEXTVAL, 
        'Safety Equipment/Proper Dress', 
        'Two rain suits',
        0
FROM    DUAL

INSERT INTO STORM_DUTY_ITEMS
SELECT  
        STORM_DUTY_ITEM_UNIQUE.NEXTVAL, 
        'Safety Equipment/Proper Dress', 
        'Clipboard and pencils',
        0
FROM    DUAL

INSERT INTO STORM_DUTY_ITEMS
SELECT  
        STORM_DUTY_ITEM_UNIQUE.NEXTVAL, 
        'Safety Equipment/Proper Dress', 
        'Two magnetic signs',
        0
FROM    DUAL

INSERT INTO STORM_DUTY_ITEMS
SELECT  
        STORM_DUTY_ITEM_UNIQUE.NEXTVAL, 
        'Safety Equipment/Proper Dress', 
        'First Aid Kit',
        0
FROM    DUAL

INSERT INTO STORM_DUTY_ITEMS
SELECT  
        STORM_DUTY_ITEM_UNIQUE.NEXTVAL, 
        'Safety Equipment/Proper Dress', 
        '5-Cell Flashlight & Flares',
        0
FROM    DUAL

INSERT INTO STORM_DUTY_ITEMS
SELECT  
        STORM_DUTY_ITEM_UNIQUE.NEXTVAL, 
        'Safety Equipment/Proper Dress', 
        'Jeans or Dockers',
        0
FROM    DUAL

INSERT INTO STORM_DUTY_ITEMS
SELECT  
        STORM_DUTY_ITEM_UNIQUE.NEXTVAL, 
        'Safety Equipment/Proper Dress', 
        'Cotton Clothing',
        0
FROM    DUAL

INSERT INTO STORM_DUTY_ITEMS
SELECT  
        STORM_DUTY_ITEM_UNIQUE.NEXTVAL, 
        'Safety Equipment/Proper Dress', 
        'Good hard soled shoes',
        0
FROM    DUAL

INSERT INTO STORM_DUTY_ITEMS
SELECT  
        STORM_DUTY_ITEM_UNIQUE.NEXTVAL, 
        NULL, 
        'What did you observe?',
        1
FROM    DUAL

INSERT INTO STORM_DUTY_ITEMS
SELECT  
        STORM_DUTY_ITEM_UNIQUE.NEXTVAL, 
        NULL,
        'At-Risk practices addressed?',
        0
FROM    DUAL


CREATE TABLE STORM_DUTY_OBSERVATIONS (
	SD_ID NUMBER PRIMARY KEY,
	OBSERVED_BY VARCHAR2(6),
	OBSERVED_DATE DATE,
   
	OBSERVED_TIME VARCHAR(7),
	COMPLETED_BY VARCHAR2(6),
	COMPLETED_DATE DATE,
	LOCATION VARCHAR2(4000),
	PUBLIC_SAFETY_TEAM_NUMBER NUMBER,
	ORG_CODE VARCHAR2(50)
);

CREATE SEQUENCE STORM_DUTY_UNIQUE;
CREATE TABLE STORM_DUTY_ANSWERS (
	SD_ID NUMBER,
	ITEM_NUM NUMBER,
	ANSWER VARCHAR(2),
	COMMENTS VARCHAR2(4000),
	
	PRIMARY KEY(SD_ID, ITEM_NUM)
);

CREATE TABLE STORM_DUTY_MEMBERS (
	SD_ID NUMBER,
	MEMBER_USID VARCHAR2(6),
	
	PRIMARY KEY(SD_ID, MEMBER_USID)
);

ALTER TABLE USERS ADD ACCESS_STORM_DUTY NUMBER DEFAULT 0;
-- related to AOE changes
ALTER TABLE HP_OBSERVATIONS MODIFY CATEGORY VARCHAR2(500);
 
-- workflow changes
CREATE TABLE MAILING_LIST AS (SELECT * FROM NM_MAILING_LIST);
ALTER TABLE MAILING_LIST ADD (OBSERVATION VARCHAR2(10));
UPDATE MAILING_LIST SET OBSERVATION = 'NM'; --all copied over subscriptions will be nm
ALTER TABLE MAILING_LIST ADD CONSTRAINT emailsubscription_unique UNIQUE (email, observation);

--question added to good catch
ALTER TABLE GOOD_CATCH_OBSERVATIONS ADD (TOOLS_USED VARCHAR2(4000));
