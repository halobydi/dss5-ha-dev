CREATE TABLE RED_TAG_AUDITS_LOCATIONS (
    ID NUMBER PRIMARY KEY,
    LOCATION VARCHAR2(150),
    ORG VARCHAR2(150),
    ORG_MAIN VARCHAR2(150)
);

CREATE TABLE RED_TAG_AUDITS_REQUIREMENTS (
    LOCATION_ID NUMBER,
    MONTH NUMBER NOT NULL,
    YEAR NUMBER NOT NULL,
    TOTAL NUMBER DEFAULT 0,

    PRIMARY KEY (LOCATION_ID, MONTH, YEAR)
);
ALTER TABLE RED_TAG_AUDITS_LOCATIONS ADD TITLE VARCHAR2(150);

ALTER TABLE NEAR_MISS_OBSERVATIONS ADD IMMEDIATE_INVESTIGATION NUMBER DEFAULT 0;

INSERT INTO RED_TAG_AUDITS_LOCATIONS (
        SELECT 
                ROWNUM, 
                LOCATION, 
                ORG,
                NULL,
                CASE 
                        WHEN ORG IN('Primary', 'PERT', 'Cable Test') THEN 'Engineering'
                        WHEN ORG IN ('Distribution', 'Tranmission/Sub-Transmission') THEN 'Systems Operations (SOC)' 
                        WHEN ORG = 'Substation' THEN 'Substations'
                        WHEN ORG LIKE '%Lines%' THEN 'Service Operations'
                        ELSE NULL
                END AS TITLE
                
        FROM RED_TAG_AUDITS_REQ
        
);
/*Set default monthly requirements for 2013 to 1*/
DECLARE
	X NUMBER := 0;
BEGIN
	LOOP
		X := X + 1;
		INSERT INTO RED_TAG_AUDITS_REQUIREMENTS ( SELECT ID, X, 2013, 1 FROM RED_TAG_AUDITS_LOCATIONS);
		IF X > 12 THEN
		  EXIT;
		END IF;
	END LOOP;
END;
