UPDATE EPOP_ITEMS SET OFFICE = 1, FIELD = 1
UPDATE EPOP_ITEMS SET OFFICE = 0 WHERE ITEM_CATEGORY IN ('Individual', 'Leader', 'Orgaization') AND ITEM_NUM < 200
UPDATE EPOP_ITEMS SET OFFICE = 1, FIELD = 0 WHERE ITEM_NUM >= 200 OR ITEM_NUM IN (13, 29)
UPDATE EPOP_ITEMS SET OFFICE = 1, FIELD = 1 WHERE OFFICE = 0 AND FIELD = 0

--EPOP_ITEM_UNIQUE IS SET AT 2001

/*
	Add to House Keeping
	----------------------------------------
	Are housekeeping deficiencies corrected in real time, are actions initiated to prevent reoccurrence?
	Is 5S being followed?
	Was idling procedure being followed?
	Were chemicals secured and labeled?
	Are load securements being followed?
	Are tools secured and properly placed and in good working order?
	Is there loose debris in the trailer?
	Are tools insulated properly, as needed?
	Clear path to gas meter?
	Clear path to appliances?
	Are stairways clear and stable?
	Were appliance relighting procedures followed?
	Floor / Work space clear of debris?

********************************************/

CREATE INDEX I_EPOP_OFFICE ON EPOP_ITEMS(OFFICE);
CREATE INDEX I_EPOP_FIELD ON EPOP_ITEMS(FIELD);

INSERT INTO EPOP_ITEMS (
        ITEM_NUM,
        ITEM_CATEGORY,
        ITEM_TEXT,
        HAS_SUBITEMS,
        ACTIVE,
        OFFICE,
        FIELD
) VALUES (
        EPOP_ITEM_UNIQUE.NEXTVAL,
        'Leader',
        'Have there been any safety communications this month?',
        0, 
        1,
        1,
        0
);
INSERT INTO EPOP_ITEMS (
        ITEM_NUM,
        ITEM_CATEGORY,
        ITEM_TEXT,
        HAS_SUBITEMS,
        ACTIVE,
        OFFICE,
        FIELD
) VALUES (
        EPOP_ITEM_UNIQUE.NEXTVAL,
        'Leader',
        'Do PRT/Huddles include a safety items? (ie. safety concerns, safety metrics, safety cross, etc...)',
        0, 
        1,
        1,
        0
);
INSERT INTO EPOP_ITEMS (
        ITEM_NUM,
        ITEM_CATEGORY,
        ITEM_TEXT,
        HAS_SUBITEMS,
        ACTIVE,
        OFFICE,
        FIELD
) VALUES (
        EPOP_ITEM_UNIQUE.NEXTVAL,
        'Individual',
        'Do employees conduct work activities in conformance to organizational unit and company safety procedures, orders, policies, and requirements?',
        0, 
        1,
        1,
        0
);
INSERT INTO EPOP_ITEMS (
        ITEM_NUM,
        ITEM_CATEGORY,
        ITEM_TEXT,
        HAS_SUBITEMS,
        ACTIVE,
        OFFICE,
        FIELD
) VALUES (
        EPOP_ITEM_UNIQUE.NEXTVAL,
        'Individual',
        'Do employees identify safety hazards and report them to their leader?',
		0, 
        1,
        1,
        0
);
INSERT INTO EPOP_ITEMS (
        ITEM_NUM,
        ITEM_CATEGORY,
        ITEM_TEXT,
        HAS_SUBITEMS,
        ACTIVE,
        OFFICE,
        FIELD
) VALUES (
        EPOP_ITEM_UNIQUE.NEXTVAL,
        'Individual',
        'Do employees know how to input Near Misses and Good Catches into SOTeria?',
		0, 
        1,
        1,
        0
);
INSERT INTO EPOP_ITEMS (
        ITEM_NUM,
        ITEM_CATEGORY,
        ITEM_TEXT,
        HAS_SUBITEMS,
        ACTIVE,
        OFFICE,
        FIELD
) VALUES (
        EPOP_ITEM_UNIQUE.NEXTVAL,
        'Individual',
        'Do employees identify safety precursors through their huddles?',
		0, 
        1,
        1,
        0
);

INSERT INTO EPOP_ITEMS (
        ITEM_NUM,
        ITEM_CATEGORY,
        ITEM_TEXT,
        HAS_SUBITEMS,
        ACTIVE,
        OFFICE,
        FIELD
) VALUES (
        EPOP_ITEM_UNIQUE.NEXTVAL,
        'Onsite',
        'Do employees maintain a safe work environment?',
		1, 
        1,
        1,
        0
);
INSERT INTO EPOP_ITEMS (
        ITEM_NUM,
        ITEM_CATEGORY,
        ITEM_TEXT,
        HAS_SUBITEMS,
        ACTIVE,
        OFFICE,
        FIELD
) VALUES (
        EPOP_ITEM_UNIQUE.NEXTVAL,
        'Onsite',
        '<b>Proper Body Position</b>? Is the employee performing work in a position that will eliminate ergonomic strains?',
		0, 
        1,
        1,
        0
);
INSERT INTO EPOP_ITEMS (
        ITEM_NUM,
        ITEM_CATEGORY,
        ITEM_TEXT,
        HAS_SUBITEMS,
        ACTIVE,
        OFFICE,
        FIELD
) VALUES (
        EPOP_ITEM_UNIQUE.NEXTVAL,
        'Onsite',
		'Do employees understand and employ HP Tools, e.g., STAR, 3-Question Technique, Peer Checking?',
		0, 
        1,
        1,
        0
);
INSERT INTO EPOP_ITEMS (
        ITEM_NUM,
        ITEM_CATEGORY,
        ITEM_TEXT,
        HAS_SUBITEMS,
        ACTIVE,
        OFFICE,
        FIELD
) VALUES (
        EPOP_ITEM_UNIQUE.NEXTVAL,
        'Onsite',
		'Do employees understand the injury reporting procedures?',
		0, 
        1,
        1,
        0
);
INSERT INTO EPOP_ITEMS (
        ITEM_NUM,
        ITEM_CATEGORY,
        ITEM_TEXT,
        HAS_SUBITEMS,
        ACTIVE,
        OFFICE,
        FIELD
) VALUES (
        EPOP_ITEM_UNIQUE.NEXTVAL,
        'Onsite',
		'Do employees know what to do in case of an emergency (evacuation procedure/tornado shelter)?',
		0, 
        1,
        1,
        0
);
INSERT INTO EPOP_ITEMS (
        ITEM_NUM,
        ITEM_CATEGORY,
        ITEM_TEXT,
        HAS_SUBITEMS,
        ACTIVE,
        OFFICE,
        FIELD
) VALUES (
        EPOP_ITEM_UNIQUE.NEXTVAL,
        'Onsite',
		'Do employees know the Emergency number for the facility?',
		0, 
        1,
        1,
        0
);
INSERT INTO EPOP_ITEMS (
        ITEM_NUM,
        ITEM_CATEGORY,
        ITEM_TEXT,
        HAS_SUBITEMS,
        ACTIVE,
        OFFICE,
        FIELD
) VALUES (
        EPOP_ITEM_UNIQUE.NEXTVAL,
        'Onsite',
		'Do employees know where the nearest first aid kit, AED, and fire extinguisher are located?',
		0, 
        1,
        1,
        0
);

INSERT INTO EPOP_ITEMS (
        ITEM_NUM,
        ITEM_CATEGORY,
        ITEM_TEXT,
        HAS_SUBITEMS,
        ACTIVE,
        OFFICE,
        FIELD
) VALUES (
        EPOP_ITEM_UNIQUE.NEXTVAL,
        'Offsite',
		'Were employees distracted when walking/driving to the off-site meeting (using cellphone, carrying large items, etc...)?',
		0, 
        1,
        1,
        0
);

INSERT INTO EPOP_ITEMS (
        ITEM_NUM,
        ITEM_CATEGORY,
        ITEM_TEXT,
        HAS_SUBITEMS,
        ACTIVE,
        OFFICE,
        FIELD
) VALUES (
        EPOP_ITEM_UNIQUE.NEXTVAL,
        'Offsite',
		'When walking up or down stairs is the employee maintaining 3 points of contact?',
		0, 
        1,
        1,
        0
);
INSERT INTO EPOP_ITEMS (
        ITEM_NUM,
        ITEM_CATEGORY,
        ITEM_TEXT,
        HAS_SUBITEMS,
        ACTIVE,
        OFFICE,
        FIELD
) VALUES (
        EPOP_ITEM_UNIQUE.NEXTVAL,
        'Offsite',
		'Was a Safety Brief performed at the off-site location? (exits, fire extinguishers, AED\'s, muster points, bathrooms, etc...)',
		0, 
        1,
        1,
        0
);
INSERT INTO EPOP_ITEMS (
        ITEM_NUM,
        ITEM_CATEGORY,
        ITEM_TEXT,
        HAS_SUBITEMS,
        ACTIVE,
        OFFICE,
        FIELD
) VALUES (
        EPOP_ITEM_UNIQUE.NEXTVAL,
        'Offsite',
		'Did employee identify hazards while traveling to or at the offsite meeting location?',
		0, 
        1,
        1,
        0
);
INSERT INTO EPOP_ITEMS (
        ITEM_NUM,
        ITEM_CATEGORY,
        ITEM_TEXT,
        HAS_SUBITEMS,
        ACTIVE,
        OFFICE,
        FIELD
) VALUES (
        EPOP_ITEM_UNIQUE.NEXTVAL,
        'Offsite',
		'Do employees know the address to the offsite location?',
		0, 
        1,
        1,
        0
);
INSERT INTO EPOP_ITEMS (
        ITEM_NUM,
        ITEM_CATEGORY,
        ITEM_TEXT,
        HAS_SUBITEMS,
        ACTIVE,
        OFFICE,
        FIELD
) VALUES (
        EPOP_ITEM_UNIQUE.NEXTVAL,
        'Offsite',
		'Do employees understand the injury reporting procedures?',
		0, 
        1,
        1,
        0
);
INSERT INTO EPOP_ITEMS (
        ITEM_NUM,
        ITEM_CATEGORY,
        ITEM_TEXT,
        HAS_SUBITEMS,
        ACTIVE,
        OFFICE,
        FIELD
) VALUES (
        EPOP_ITEM_UNIQUE.NEXTVAL,
        'Offsite',
		'Are there any safety concerns observed at this location?',
		0, 
        1,
        1,
        0
);
INSERT INTO EPOP_ITEMS (
        ITEM_NUM,
        ITEM_CATEGORY,
        ITEM_TEXT,
        HAS_SUBITEMS,
        ACTIVE,
        OFFICE,
        FIELD
) VALUES (
        EPOP_ITEM_UNIQUE.NEXTVAL,
        'Offsite',
		'Are standard safety procedures adhered to while offsite?',
		0, 
        1,
        1,
        0
);
INSERT INTO EPOP_ITEMS (
        ITEM_NUM,
        ITEM_CATEGORY,
        ITEM_TEXT,
        HAS_SUBITEMS,
        ACTIVE,
        OFFICE,
        FIELD
) VALUES (
        EPOP_ITEM_UNIQUE.NEXTVAL,
        'Offsite',
		'Do employees know the emergency contact for the facility?',
		0, 
        1,
        1,
        0
);

