<?php
	function errorHandler($errno, $errstr, $errfile, $errline, $errcontext){}
	set_error_handler('errorHandler');
	require_once('mcl_Html.php');
	require_once('mcl_Oci.php');
	require_once("/opt/apache/servers/soteria/htdocs/src/php/auth.php");
	
	$disabled = $_GET['readonly'] == 'true' ? true : false;
	
	mcl_Html::js(mcl_Html::DOJO);
	mcl_Html::js(mcl_Html::DOJO_WINDOW);
	mcl_Html::js(mcl_Html::AJAX);
	mcl_Html::js(mcl_Html::JQUERY);
	mcl_Html::js(mcl_Html::CALENDAR);
	mcl_Html::s(mcl_Html::INC_JS, "../src/sot.js");
	mcl_Html::s(mcl_Html::INC_JS, "../src/js/stormduty.js");
	mcl_Html::s(mcl_Html::INC_JS, "../src/js/other/jquery.mask.js");
	mcl_Html::s(mcl_Html::INC_JS, "../src/js/other/jquery.qtip-1.0.0-rc3.min.js");

	//mcl_Html::css(mcl_Html::CALENDAR);
	mcl_Html::s(mcl_Html::INC_CSS, "../src/css/jscal2.css");
	mcl_Html::s(mcl_Html::INC_CSS, "../src/css/form.css");
	mcl_Html::s(mcl_Html::INC_CSS, "../src/css/form_alt.css");

	$form = "Public Safety Team Observation";
	mcl_Html::title("SOTeria - {$form}");
	
	$user = auth::check();
	if($user["status"] != "authorized"){
		header('Location: ../index.php');
	}
	
	$oci = new mcl_Oci('soteria');
	$edit = false;
	$user = auth::check();
	$completed_by = $user['usid'];
	if(!empty($_GET['delegate']) && !empty($user['delegates'][$_GET['delegate']])) {
		$usid = $user['delegates'][$_GET['delegate']]['usid'];
	} else {
		$usid = $user['usid'];
	}

	if(isset($_GET["print"]) && $_GET["print"] == true) {
		mcl_Html::s(mcl_Html::SRC_JS, "
		window.onload = function() {
			window.print();
			window.close();
		}
	");

		echo <<<DIV
		<div id='print' style='width: 100%; height: 100px; font-size: 26px; text-align: center;'/>
			{$form}
		</div>
DIV;
	}
	
	$observed_by = $usid;
	$date = date('m/d/Y');
	$hour = date('h');
	$minute = date('i');
	$ampm = date('H') > 12 ? 'PM' : 'AM';
	
	$sd_id = $_REQUEST['sd_id'];
	
	$valid_usid_access = false;
	if($sd_id) {
		$oci->dateFormat('mm/dd/yyyy');
		$sql = "SELECT * FROM STORM_DUTY_OBSERVATIONS WHERE SD_ID = {$sd_id}";
		$data = $oci->fetch($sql);
		
		$location = $data['LOCATION'];
		$completed_by = $data['COMPLETED_BY'];
		$observed_by = $data['OBSERVED_BY'];
		$date = date('m/d/Y', strtotime($data['OBSERVED_DATE']));
		$team_number = $data['PUBLIC_SAFETY_TEAM_NUMBER'];
		$time = $data['OBSERVED_DATE'];
		$hour = substr($time, 0, 2);
		$minute = substr($time, 3, 2);
		$ampm = substr($time, 5, 2);
		
		$sql = "SELECT * FROM STORM_DUTY_MEMBERS WHERE SD_ID = {$sd_id}";
		$x = 0;
		while($row = $oci->fetch($sql)) {
			$member = "member_" . (++$x);
			$member_name = "member_" . ($x) . '_name';
			
			$usid =  $row['MEMBER_USID'];
			$lookup = @mcl_Ldap::lookup($usid);
			$name = $lookup['fname'];

			$$member = $usid;
			$$member_name = $name;
			
			if($usid == $user["usid"]) {
				$valid_usid_access = true;
			}
		}
		
		if($observed_by == $user["usid"]) {
			$valid_usid_access = true;
		}
	}
	
	$privileges = auth::check('privileges');
	if(!$privileges["ACCESS_STORM_DUTY"] && !$valid_usid_access){
		auth::deny();
	}
	
	
	$lookup = @mcl_Ldap::lookup($observed_by);
	$observed_by_name = $lookup['fname'];

	$sql = "SELECT * FROM ORGANIZATIONS ORDER BY ORG_CODE";
	$orgs = array();
	$user_ = @mcl_Ldap::lookup($user["usid"]);
	$org_u = substr($user_["all"][0]["dtebusinessunitdeptidlongdesc"][0], 4);
	$orgs_a = array();

	while($row = $oci->fetch($sql)){
		$pos = strpos($row["ORG_DESCRIPTION"], trim($org_u));
		if ($pos === false) {
		} else {
			$org = $row["ORG_CODE"];
		}
		
		$orgs_a[$row["ORG_CODE"]] = array(
			"code"		=> $row["ORG_CODE"],
			"title"		=> $row["ORG_TITLE"]
		);	
	}
	
	if(isset($data["ORG_CODE"])){ $org = $data["ORG_CODE"]; }
	foreach($orgs_a as $key=>$value){
		$selected = ($org == $key ? 'selected=selected' : '');
		$org_s .= "<option value='{$key}' {$selected}>{$value["title"]}</option>";
	}
	
	
?>
<div class='header' style='width: 800px; margin: 0 auto; margin-top: 5px;'>
	<?=$form?>
</div>
<div id='container' style='margin-top: 0px; width: 800px; margin: 0 auto;'>
<form action = '../src/php/engine.php?f=saveStormDuty<?= (isset($_GET['mobile']) ? "&mobile=true" : "") ?>' method='POST' onsubmit='return sot.stormduty.validate();' target='submit' enctype="multipart/form-data">
	<div class='header2' style='margi-top: 0px;'>Team Member Information</div>
	<div class='content'>
		<table class=''>
			<tr>
				<td class='header-group' style='width: 150px;'>Observer</td>
				<td>
					Username<br/>
					<input type='text' name='observed_by' id='observer' maxlength='6' value='<?=$observed_by?>' onkeydown='return false;'/>
				</td>
				<td>
					Name<br/>
					<input type='text' id='observer_name' value='<?=$observed_by_name?>' onkeydown='return false;'/>
				</td>
				<td>
					Organization<br/>
					<select name='org'>
						<?=$org_s?>
					</select>
				</td>
			</tr>
			<tr>
				<td class='header-group'>Public Safety Member</td>
				<td>
					Username <span class='help'>(ie. u12345)</span><br/>
					<input type='text' value='<?=$member_1?>' maxlength='6' id='member_1_usid' name='member[]' onchange='sot.stormduty.populateName(this, "member_1_name");'/>
				</td>
				<td>
					Name <span class='help'>(auto-populated)</span><br/>
					<input type='text' id='member_1_name' value='<?=$member_1_name?>' onkeydown='return false;'/>
				</td>
				<td>
					Public Safety Team Number<br/>
					<input type='text' value='<?=$team_number?>' name='team_number'/>
				</td>
			</tr>
			<tr>
				<td class='header-group'>Public Safety Member</td>
				<td>
					Username <span class='help'>(ie. u12345)</span><br/>
					<input type='text'  value='<?=$member_2?>' maxlength='6' id='member_2_usid' name='member[]' onchange='sot.stormduty.populateName(this, "member_2_name");'/>
				</td>
				<td>
					Name <span class='help'>(auto populated)</span><br/>
					<input type='text' id='member_2_name' value='<?=$member_2_name?>' onkeypress='return false;'/>
				</td>
			</tr>
		</table>
	</div>
	<div class='content'>
		<table>
			<tr>
				<td style='width:150px; text-align: right; padding-right: 5px;'>Location</td>
				<td>
					<input type='text' value='<?=$location?>' id='location' name='location' style='width: 405px;'/>
				</td>
			</tr>
			<tr>
				<td style='width:150px; text-align: right; padding-right: 5px;'>Date</td>
				<td>
					<input style='width: 100px;' type='text' id='date' maxlength='10' onkeydown='if(window.event.keyCode != 9) { return false; }' name='date' id='date' value="<?php echo $date; ?>"/>
					<img src='../src/img/calendar.gif' alt='' id="tcal" onmouseover="sot.swo.cal('tcal', 'date');" />
				</td>
			</tr>
			<tr>
				<td style='width:150px; text-align: right; padding-right: 5px;'>Time</td>
				<td>
					<input name='hour' id='hour' type='text' style='width: 25px;' value='<?php echo $hour ?>' maxlength='2'>:<input id='minute' name='minute' type='text' style='width: 30px;' value='<?php echo $minute; ?>' maxlength='2'>
					<select name='ampm' style='width: 50px; position: relative; top: 2px;'>
						<option <?php echo ($ampm == "AM" ? "selected=selected" : ""); ?>>AM</option>
						<option <?php echo ($ampm == "PM" ? "selected=selected" : ""); ?>>PM</option>
					</select>
				</td>
			</tr>
		</table>
		<br/>
	</div>
	
	<?php 
		if(!$sd_id) $sd_id = 0;
		$sql = "
		SELECT  SDI.*,
				SDA.COMMENTS,
				SDA.ANSWER
		FROM    STORM_DUTY_ITEMS SDI
		LEFT	JOIN STORM_DUTY_ANSWERS SDA
				ON SDA.ITEM_NUM = SDI.ITEM_NUM AND SDA.SD_ID = {$sd_id}
		ORDER   BY ITEM_CATEGORY,
				SDI.ITEM_NUM
		";
		$category = array();
		while($row = $oci->fetch($sql)) {
			$category[$row['ITEM_CATEGORY']][] = $row;
		}
		
		foreach($category as $key=>$items):
	?>
		<br/>
		
		<div class='header2'>
			<?php if(!empty($key)): ?>
			<table class='stormduty'>
				<tr>
					<th class='radio center'>
						S
					</th>
					<th class='radio center'>
						IO
					</th>
					<th class='radio center'>
						N/A
					</th>
					<th colspan='2' class='center question'>
						<?=$key?>
					</th>
				</tr>
			</table>
			<?php else: ?>
				Comments
			<?php endif; ?>
		</div>
		<div class='content'>
			<table class='stormduty'>
				<?php $odd = true; ?>
				<?php foreach($items as $item): ?>
					<tr class='<?=($odd ? 'odd' : 'even')?>'>
						<?php if($item['COMMENTS_ONLY'] == '1'): ?>
							<td class='radio center'>
								<input type='radio' name='<?=$item['ITEM_NUM']?>' value='NA' checked=checked style='display: none;'/>
								&nbsp;
							</td>
							<td class='radio center'>
								&nbsp;
							</td>
							<td class='radio center'>
								&nbsp;
							</td>
						<?php else: ?>
							<td class='radio center'>
								<input type='radio' name='<?=$item['ITEM_NUM']?>' <?=$item['ANSWER'] == 'S' ? 'checked=checked' : ''?> value='S'/>
							</td>
							<td class='radio center'>
								<input type='radio' name='<?=$item['ITEM_NUM']?>' <?=$item['ANSWER'] == 'IO' ? 'checked=checked' : ''?> value='IO'/>
							</td>
							<td class='radio center'>
								<input type='radio' name='<?=$item['ITEM_NUM']?>' <?=$item['ANSWER'] == 'NA' ? 'checked=checked' : ''?> value='NA'/>
							</td>
						<?php endif; ?>
						<td class='question'>
							<?=$item['ITEM']?>
						</td>
						<td style='text-align: right; cursor: pointer;'>
							<?php if($item['COMMENTS_ONLY'] == '0'): ?>
								<img src='../src/img/comments.png' id='<?=$item['ITEM_NUM']?>'/>
							<?php endif; ?>
						</td>
					</tr>
					<tr class='<?=($odd ? 'odd' : 'even')?>' style='display:<?=((!empty($item['COMMENTS']) || $item['COMMENTS_ONLY'] == '1') ? '' : 'none')?>;'>
						<td class='radio center'>
							&nbsp;
						</td>
						<td class='radio center'>
							&nbsp;
						</td>
						<td class='radio center'>
							&nbsp;
						</td>
						<td colspan='2'>
							<textarea id='<?=$item['ITEM_NUM']?>_comments' name='<?=$item['ITEM_NUM']?>_comments'><?=$item['COMMENTS']?></textarea>
						</td>
					</tr>
					<?php 
						$odd = !$odd; 
					?>
				<?php endforeach; ?>
			</table>
		</div>
	<?php 
		endforeach;
	?>	
	<div class='header2'>Photos <span class='right' onclick='addPhoto();'>[ Add Photo ]</span></div>
	<div class='content' id='photos' style='position: relative; overflow: hidden; padding: 0px;'>
		<?php
			require_once("/opt/apache/servers/soteria/htdocs/src/php/photos.php");
			$x = getPhotos('SD',  $_REQUEST["sd_id"], $oci, true);
			echo "<input type='hidden' name='p_count' id='p_count' value='{$x}'/>";
		?>
	</div>	
	<?php if(!$disabled): ?>
		<center style='margin-top:10px; margin-bottom: 10px;'>
			<input type = 'hidden' value = '<?php echo $_GET["delegate"]; ?>' name = 'delegator'/>
			<?php $returnto = "index"; if(!empty($_GET["returnto"])) { $returnto = $_GET["returnto"]; } ?>
			<?php if($_GET["mobile"] == "true") { $returnto = "mobile/index"; } ?>
			<input type='hidden' name='returnto' value='<?php echo $returnto; ?>' />
			<input type='hidden' name='completed_by' value='<?php echo $completed_by; ?>' />
			<input type='hidden' name='sd_id' value='<?php echo $sd_id; ?>' />
			<input type = 'submit' id='submit' value='Submit'>
			<button style='margin-left: 5px;' onclick='window.location="../<?php echo $returnto ?>.php<?php echo (!empty($_GET["delegate"]) ? "?delegate={$_GET["delegate"]}" : ""); ?>"; return false;'>Cancel & Go Back</button>
		</center>
	<?php endif; ?>
</div>
</form>
<iframe name='submit' style='width:0px; height:0px;'></iframe>

<?php if($disabled): ?>
	<script type='text/javascript'>
		sot.stormduty.disable();
	</script>
<?php endif; ?>