<div style='text-align; right; margin-top: 5px; width: 800px;' id='print' onclick='window.print(); return false;'>
	<div style='text-align:right;'>
		<img src='../src/img/print.png'/> <a href='#' onclick='return false;' style=''>Print </a>
	</div>
</div>
<!-- <a href='#' onclick='sot.hp.getHtml();' style=''>Save to PDF </a> -->
<?php
	function errorHandler($errno, $errstr, $errfile, $errline, $errcontext){
		
	}
	set_error_handler('errorHandler');
	require_once('mcl_Html.php');
	require_once('mcl_Oci.php');
	require_once("/opt/apache/servers/soteria/htdocs/src/php/auth.php");
	$user = auth::check();
	$login_warning = '';
	if($user["status"] != "authorized"){
		header("Location: ../index.php");
	}
	
	mcl_Html::title("SOTeria - Anatomy of an Event Report");
	
	mcl_Html::js(mcl_Html::DOJO);
	mcl_Html::js(mcl_Html::JQUERY);
	mcl_Html::js(mcl_Html::DOJO_WINDOW);
	mcl_Html::js(mcl_Html::AJAX);
	mcl_Html::js(mcl_Html::CALENDAR);
	mcl_Html::s(mcl_Html::INC_JS, "../src/js/tools/cover.js");
	mcl_Html::s(mcl_Html::INC_JS, "../src/js/tools/ajax.js");
	mcl_Html::s(mcl_Html::INC_JS, "../src/js/other/json2.min.js");
	mcl_Html::s(mcl_Html::INC_JS, "../src/js/other/jquery.qtip-1.0.0-rc3.min.js");
	mcl_Html::s(mcl_Html::INC_CSS, "{$baseDir}/src/css/jscal2.css");
	mcl_Html::s(mcl_Html::INC_JS, "../src/sot.js");
	mcl_Html::s(mcl_Html::INC_JS, "../src/js/hp.js");
	mcl_Html::s(mcl_Html::INC_CSS, "../src/css/form.css");
	
	mcl_Html::s(mcl_Html::SRC_CSS, "
	input[type=text], textarea { border: 1px solid #000;}
");

	if($_GET["print"] == true) {
	mcl_Html::s(mcl_Html::SRC_JS, "
	window.onload = function() {
		window.print();
		window.close();
	}
");

	$form = "Human Performance Report Form";

	echo <<<DIV
	<div id='print' style='width: 100%; height: 100px; font-size: 26px; text-align: center;'/>
		{$form}
	</div>
DIV;
	}
	
	$oci = new mcl_Oci('soteria');
	
	$sql = "
		SELECT * FROM ORGANIZATIONS  ORDER BY ORG_CODE
	";
	
	$orgs = array();
	while($row = $oci->fetch($sql)){
		$orgs[$row["ORG_CODE"]] = array(
			"code"		=> $row["ORG_CODE"],
			"title"		=> $row["ORG_TITLE"]
		);	
	}
	
	$category = "";
	$swo = false;
	$qew = false;
	$pp = false;
	
	// if(!empty($_GET["usid"])){
	// 	mcl_Html::s(mcl_Html::SRC_JS,"
	// 		window.onload = function(){
	// 			sot.hp.inputUsid(dojo.byId('usid_1'), 1);
	// 		}
	// 	");
	// }

	if(isset($_GET['hpid'])) {
		$hp_id = $_GET['hpid'];
		$answers = array();
		echo "<input type='text' style='display:none' id='hp_id' value='{$hp_id}'></input>";
		$sql = 
			"SELECT 
			    comments,
			    completed_by,
			    completed_by_supervisor,
			    completed_by_director,
			    event_type,
			    hp_id,
			    to_char(incident_date, 'MM/DD/YYYY') as incident_date,
			    organization,
			    completed,
			    category,
			    incident_type
			FROM 
			    hp_observations
			WHERE 
			    hp_id = {$hp_id} 
			";
		while($row = $oci->fetch($sql)) {
			// if($row['COMPLETED_BY']!=$user['usid']) {
			// 	header("Location: ../index.php");
			// }
			// if($row['COMPLETED']=='Y') {
			// 	die("<br><br>This report has been completed and submitted. <a href='viewHp.php?hp_id={$_GET['hp_id']}'>Click here</a> to view the report.");
			// }
			$answers['header'] = $row;
		}
		$sql = 
			"SELECT 
			    item_num, 
			    subitem_num, 
			    freeform, 
			    answer 
			FROM 
			    hp_answers 
			WHERE 
			    hp_id = {$hp_id} 
			ORDER BY 
			    item_num, 
			    subitem_num
			";
		while($row = $oci->fetch($sql)) {
			$sub = (isset($row['SUBITEM_NUM'])) ? $row['SUBITEM_NUM'] : 'null';
			$answers[$row['ITEM_NUM']][$sub] = $row['ANSWER'];
		}
		$sql = 
			"SELECT 
			    item_num,
			    answer,
			    explanation 
			FROM 
			    twin_answers
			WHERE 
				observation_form = 'HP'
			    AND observation_id = {$hp_id} 
			ORDER BY 
			    item_num
			";
		while($row = $oci->fetch($sql)) {
			$answers[$row['ITEM_NUM']]['twin'] = array('answer'=>$row['ANSWER'], 'explanation'=>$row['EXPLANATION']);
		}
		// echo "<script>console.log(".var_dump($answers).")</script>";
		echo '<input type="text" style="display:none" id="prev_answers" value="'.htmlspecialchars(json_encode($answers)).'"></input>';
	}

?>
<div class='header' style='margin:0px auto'>
	Anatomy of an Event Report Form <? echo ($hp_id) ? "<b>#$hp_id</b>" : ""; ?>
</div>
<?php
	$url = "hp.php?sid=" . time();
	$url2 = "";
	if(!empty($login_warning)){
		echo "<div class='form_warning'>
			{$login_warning	}	
		</div>";
	}
?>
<div id='container' style='margin: 0px auto'>
	<table id = 'header'>
		<tr>
			<td style='width:150px; font-weight:bold;'>Incident Date:</td>
			<td style='width:250px;'>
				<input type='text' class='header' id='incident_date' maxlength='10' onkeydown='if(window.event.keyCode != 9) { return false; }' name='incident_date' id='incident_date' value="<?php echo date("m/d/Y"); ?>"/>
				<img src='../src/img/calendar.gif' alt='' id="tcal" onmouseover="sot.hp.cal('tcal', 'incident_date');" />
			</td>
			<td style='width:147px; font-weight:bold;'>Organization:</td>
			<td>
				<select name='org' class='header' id='org' onchange='sot.hp.switchOrg();' style="width:250px">
					<option value=''></option>
					<?php
						foreach($orgs as $key=>$value){
							$selected = ($org == $key ? 'selected=selected' : '');
							echo "<option value='{$key}' {$selected}>{$value["title"]}</option>";
						}
					?>
				</select>
			</td>
		</tr>
		<tr>
			<td/>
			<td/>
			<td><b>HP Event/Reset Category: </b></td>
			<td><select name='500' id='500' data-subitem='null' data-item='500'>
				<option value=""></option>
				<option value="Safety (Reset = OSHA recordable)">Safety (Reset = OSHA recordable)</option>
				<option value="Environmental (Reset = requiring report in less than 30 days)">Environmental (Reset = requiring report in less than 30 days)</option>
				<option value="Protection Process (Reset = protection violation)">Protection Process (Reset = protection violation)</option>
				<option value="Regulatory (Reset = requiring report in less than 30 days)">Regulatory (Reset = requiring report in less than 30 days)</option>
				<option value="Company Vehicle Accident (Reset = resulting in damage in excess of $5000)">Company Vehicle Accident (Reset = resulting in damage in excess of $5000)</option>
				<option value="Property or Equipment Damage (Reset = damage in excess of $15000)">Property or Equipment Damage (Reset = damage in excess of $15000)</option>
			</select></td>
		</tr>
		<tr>
			<td style='width:150px; font-weight:bold;'>Submitted By:</br></td>
			<td style='width:250px;'><input class='header' type='text' id='observed_by' name='observed_by' value="<?php echo (is_array(auth::check("name")) ? "" : auth::check("name"));?>" disabled/></td>
			<td style='width:150px; font-weight:bold;'>Safety Event Type:</td>
			<td>
				<select name='event_type' class='header' id='event_type' onchange='sot.hp.switchType();' style="width:250px">
					<option value=''></option>
					<option value='Animal Bite'>Animal Bite</option>
					<option value='Asbestos Exposure'>Asbestos Exposure</option>
					<option value='Assault'>Assault</option>
					<option value='Burns'>Burns</option>
					<option value='Cut by Object'>Cut by Object</option>
					<option value='Contact with radiation, caustics, toxic'>Contact with radiation, caustics, toxic</option>
					<option value='Contact with temperature extremes'>Contact with temperature extremes</option>
					<option value='Cumulative Trauma/Repetitive Motion'>Cumulative Trauma/Repetitive Motion</option>
					<option value='Caught in, under, or between'>Caught in, under, or between</option>
					<option value='Electrical Equipment Failure'>Electrical Equipment Failure</option>
					<option value='Electrical'>Electrical</option
					<option value='Eye Injury'>Eye Injury</option>
					<option value='Fall from elevation'>Fall from elevation</option>
					<option value='Insect bite'>Insect bite</option>
					<option value='Overexertion'>Overexertion</option>
					<option value='Possible Asbestos Exposure'>Possible Asbestos Exposure</option
					<option value='Poison Ivy'>Poison Ivy</option>
					<option value='Possible Standard Threshold Shift'>Possible Standard Threshold Shift</option>
					<option value='Rubbed or abraded'>Rubbed or abraded</option>
					<option value='Struck by/against'>Struck by/against</option>
					<option value='Slip, Trip, Fall'>Slip, Trip, Fall</option>
					<option value='Standard Threshold Shift'>Standard Threshold Shift</option
					<option value='Unsanitary Conditions'>Unsanitary Conditions</option>
					<option value='Vehicle accidents'>Vehicle accidents</option
					<option value='Welding Flash'>Welding Flash</option>
					<option value='Other'>Other</option>
				</select>
			</td>
		</tr>
		<tr>
			<td style='width:150px; font-weight:bold;vertical-align:top'>Incident Type:</td>
			<td style='width:250px;vertical-align:top'>
				<input type='CHECKBOX' class='header2' name='incident_type' value='Event'></input>Event<br>
				<!-- <input type='CHECKBOX' class='header2' name='incident_type' value='Positive Demonstration'</input>Positive Demonstration<br> -->
				<input type='CHECKBOX' class='header2' name='incident_type' value='HP Reset'</input>Reset<br>
			</td>
			<td style='width:150px; font-weight:bold;vertical-align:top' id='resCat1'></td>
			<td id='resCat2'>
			</td>
		</tr>
		<tr>
			<td><b>Incident Summary:</b></td>
			<td colspan="3"><textarea style="width:100%;height:40px;" name='501' id='501' data-subitem='null' data-item='501'></textarea></td>
		<tr>
			<td colspan="4"><input type = "checkbox" style="margin-right:5px;" name='502' id='502' data-subitem='null' data-item='502'/> A Learning Team was convened and completed the Anatomy of an Event Review together.</td>
		</tr>
	</table>
	<table style='table-layout:fixed;width:800px;'>
		<colgroup>
			<col width="25%">
			<col width="5%">
			<col width="5%">
			<col width="65%">
		</colgroup>
	<?php
		function print_row(&$row, &$prev_category, &$prev_type, &$x, &$oci) {
			$type_specific = array('Electrical Event', 'Electrical Contact Event', 'Arc Flash Event', 'Task Demands', 'Individual Capabilities', 'Work Environment', 'Human Nature');
			$electrical = array('Electrical Contact Event', 'Arc Flash Event');
		
			if($row["ITEM_CATEGORY"] != $prev_category){
				$x = 0;
				$prev_type = '';
				if(!$firstpass){
					echo "</tbody>";
					if (in_array($row["ITEM_CATEGORY"], $electrical)) {
							$num = ($row["ITEM_CATEGORY"] == 'Electrical Contact Event') ? 305 : 304;
							echo "<tbody class='elecPicker' style='width:800px'>
								<tr style='height:30px;' id='{$row['ITEM_CATEGORY']}_checkRow'><td colspan='4'><input data-item='{$num}' data-subitem='null' type='checkbox' id='{$row['ITEM_CATEGORY']}_check' onclick='sot.hp.switchType();'>{$row['ITEM_CATEGORY']}</td></tr>
							     </tbody>
							     <script>$('.elecPicker').hide();</script>
							     <tbody id='{$row['ITEM_CATEGORY']}' style='width:800px'>
							";
					}
					else {
						echo "<tbody id='{$row['ITEM_CATEGORY']}' style='width:800px'><tr style='height:30px;'>
							<td colspan='4'></td>
						</tr>";
					}
				}
				if (!in_array($row["ITEM_CATEGORY"], $electrical)) {
					echo "<tr style='font-weight:bold;background-color:#dadada;'>
							<td style='text-align:left; padding: 2px; border: 1px solid #000;' colspan='4'>&nbsp;{$row["ITEM_CATEGORY"]}</td>
						</tr>";
				}
				if (in_array($row["ITEM_CATEGORY"], $type_specific)) {
					echo "<script type='text/javascript'>document.getElementById('{$row['ITEM_CATEGORY']}').style.display = 'none';</script>";
				}
				if($row['ITEM_CATEGORY'] == 'Location of Event') {
					$sql = "SELECT * FROM NEAR_MISS_ACCIDENT_LOCATIONS ORDER BY AL_LOCATION";
					$locations = "<option value=''></option>";
					while($row3 = $oci->fetch($sql)){
						$locations .= "<option value='{$row3["AL_ID"]}' " . ($answers[300]['null'] == $row3["AL_LOCATION"] ? "selected=selected" : "") . ">{$row3["AL_LOCATION"]}</option>";
					}
					$sql = "SELECT * FROM NEAR_MISS_PLANTS ORDER BY CASE WHEN PLANT_ID = 0 THEN 1 ELSE 0 END, PLANT";
					$plants = "<option value=''></option>";
					while($row3 = $oci->fetch($sql)){
						$plants .= "<option value='{$row3["PLANT_ID"]}' " . ($answers[301]['null'] == $row3["PLANT"] ? "selected=selected" : "") . ">{$row3["PLANT"]}</option>";
					}
					echo "<tr><td>Location where Event Occurred</td><td colspan='3'><select data-item='300'  data-subitem='null' style='width:100%'>{$plants}</select></td></tr>";
					echo "<tr><td>Location Description</td><td colspan='3'><select data-item='301' data-subitem='null' style='width:100%'>{$locations}</select></td></tr>";
				}
				$firstpass = false;

			}
			
			$has_subItems = 'false';
			if($row["HAS_SUBITEMS"] == '1'){
				$has_subItems = 'true';
			}
			
			echo "<tr >";
			if($row["ITEM_TYPE"] == 'RADIO') {
				if('RADIO' != $prev_type) {
					$x--;
					echo "</tr><tr  style='font-weight:bold;'>
						<td/>
						<td style='width:30px; text-align:center;'>Y</td>
						<td style='width:30px; text-align:center;'>N</td>
						<td style='padding-left:13px;'>NA</td>
					</tr>
					<tr >";
				}
				echo 	"<td ".((!empty($row['HINT'])) ? "title='{$row['HINT']}'" : '').">{$row['ITEM_TEXT']}</td>
							<td style='width:30px; text-align:center; vertical-align:top;'><input type='radio' class='Y' name='{$row['ITEM_NUM']}' id='{$row['ITEM_NUM']}' data-subitem='null' data-item='{$row['ITEM_NUM']}'  /></td>
							<td style='width:30px; text-align:center; vertical-align:top;'><input type='radio' class='N' name='{$row['ITEM_NUM']}' id='{$row['ITEM_NUM']}' value='N' data-subitem='null' data-item='{$row['ITEM_NUM']}' /></td>
							<td style='padding-left:10px; vertical-align:top;'><input type='radio' class='NA' name='{$row['ITEM_NUM']}' id='{$row['ITEM_NUM']}' data-subitem='null' data-item='{$row['ITEM_NUM']}' value='NA' /></td>
							
					</tr>";
			}
			else if($row["ITEM_TYPE"] == 'TEXT') {
				echo "<td style = 'text-align: left;' ".((!empty($row['HINT'])) ? "title='{$row['HINT']}'" : '').">{$row['ITEM_TEXT']}</td>
						<td colspan = '3'><input type='text' name='{$row['ITEM_NUM']}' id='{$row['ITEM_NUM']}' data-subitem='null' data-item='{$row['ITEM_NUM']}' maxlength='400' style ='border:1px solid #000; width:595px;'/></td>";
			}
			else if($row["ITEM_TYPE"] == 'TEXTAREA') {
				echo "<td colspan style = 'text-align: left;' ".((!empty($row['HINT'])) ? "title='{$row['HINT']}'" : '').">{$row['ITEM_TEXT']}</td>
						<td colspan = '3'><textarea name='{$row['ITEM_NUM']}' id='{$row['ITEM_NUM']}' data-subitem='null' data-item='{$row['ITEM_NUM']}' rows='3' style='border:1px solid #000; width:595px;overflow:auto' maxlength='400' onkeypress='return sot.hp.imposeMaxLength(this, 1000);'></textarea></td>";
			}
			else if($row["ITEM_TYPE"] == 'SELECT') {
				echo "<td ".((!empty($row['HINT'])) ? "title='{$row['HINT']}'" : '').">{$row['ITEM_TEXT']}</td>
					<td colspan='3'><select style='width:100%' name='{$row['ITEM_NUM']}' id='{$row['ITEM_NUM']}' data-subitem='null' data-item='{$row['ITEM_NUM']}'><option value=''>Choose</option>";
				$sql2 = "select * from hp_subitems where parent_id = {$row['ITEM_NUM']} and subitem_type = 'OPTION'";
				while($row2 = $oci->fetch($sql2)){
					echo "<option name='{$row['ITEM_NUM']}' id='{$row['ITEM_NUM']}' data-subitem='{$row2["SUBITEM_ID"]}' data-item='{$row['ITEM_NUM']}' value='{$row2["SUBITEM_ID"]}'>{$row2["SUBITEM_TEXT"]}</option>";
				}
				echo "</select>
				</td>";
			}
			else if($row["ITEM_TYPE"] == 'CHECKBOX') {
				echo "<td style = 'text-align: left;'>
							<input type='checkbox' name='{$row['ITEM_NUM']}' id='{$row['ITEM_NUM']}' data-subitem='null' data-item='{$row['ITEM_NUM']}'/>
						</td>
						<td colspan = '3' style = 'text-align: left;' ".((!empty($row['HINT'])) ? "title='{$row['HINT']}'" : '').">
							{$row['ITEM_TEXT']}
						</td>
					";
			}
			echo "</tr>";
				
			$prev_category = $row["ITEM_CATEGORY"];
			$prev_type = $row["ITEM_TYPE"];
			
			if($has_subItems == 'true'){
				$firstSub = true;
				echo "<script type='text/javascript'>
						elements = document.getElementsByName('{$row['ITEM_NUM']}');
						for(var i = 0; i < elements.length; i++)
							elements[i].onclick = function() {sot.hp.subClick(this, '{$row['ITEM_NUM']}', '{$row['SUB_CONDITION']}')};
					</script>";	
				$sql2 = "select * from hp_subitems where parent_id = {$row['ITEM_NUM']} and active=1";
				while($row2 = $oci->fetch($sql2)){
					$x--;
					if($row2['SUBITEM_TYPE'] == 'CHECKBOX') {
						if($firstSub) {
							echo "<tr name='{$row['ITEM_NUM']}' id='subitem_{$row['ITEM_NUM']}' style='display:none;'><td>Check all that apply:</td><td colspan='3'>
										<input type='CHECKBOX'  id='{$row2['SUBITEM_ID']}' name='{$row2['SUBITEM_ID']}' id='{$row2['SUBITEM_ID']}'  data-subitem='{$row2['SUBITEM_ID']}' data-item='{$row['ITEM_NUM']}'/>
										{$row2['SUBITEM_TEXT']}
									</td>
								</tr>";
						}
						else {
							echo "<tr id='subitem_{$row['ITEM_NUM']}' style='display:none;'>
								<td/>
								<td colspan='3'>
									<input name='{$row2["SUBITEM_ID"]}' id='{$row2['SUBITEM_ID']}' type='CHECKBOX'  data-subitem='{$row2['SUBITEM_ID']}' data-item='{$row['ITEM_NUM']}''/>
									{$row2['SUBITEM_TEXT']}
								</td>
							</tr>";
						}
					}
					else if($row2['SUBITEM_TYPE'] == 'RADIO') {
						if((($prev_type != 'RADIO')&&($firstSub))||($row['ITEM_NUM']=='5')) {
							echo "<tr id='subitem_{$row['ITEM_NUM']}'  style='font-weight:bold;display:none'>
								<td></td>
								<td style='width:30px; text-align:center;'>Y</td>
								<td style='width:30px; text-align:center;'>N</td>
								<td><span style='width:30px; text-align:center;float:left;'>NA</span></td>
							</tr>";
								
						}
						echo "<tr id='subitem_{$row['ITEM_NUM']}' style='display:none'>
								<td style='width:710px;'>{$row2["SUBITEM_TEXT"]}</td>
								<td style='width:30px; text-align:center; vertical-align:top;'><input type='radio' class='Y' name='subitem_{$row2["SUBITEM_ID"]}' id='{$row2["SUBITEM_ID"]}' value='Y' data-subitem='{$row2['SUBITEM_ID']}' data-item='{$row['ITEM_NUM']}' /></td>
								<td style='width:30px; text-align:center; vertical-align:top;'><input type='radio' class='N' id = '{$row2["SUBITEM_ID"]}' name='subitem_{$row2["SUBITEM_ID"]}' value='N' data-subitem='{$row2['SUBITEM_ID']}' data-item='{$row['ITEM_NUM']}' /></td>
								<td style='width:300px;vertical-align:top;'><span style='width:30px; text-align:center; vertical-align:top;float:left;'><input type='radio' class='NA' name='subitem_{$row2["SUBITEM_ID"]}' id='{$row2["SUBITEM_ID"]}' value='NA' data-subitem='{$row2['SUBITEM_ID']}' data-item='{$row['ITEM_NUM']}' /></span></td>
							</tr>";
					}
					else if($row2['SUBITEM_TYPE'] == 'TEXTAREA') {
						echo "<tr id='subitem_{$row['ITEM_NUM']}' style='display:none;'>
							<td>{$row2["SUBITEM_TEXT"]}</td>
							<td colspan='3'><textarea id='{$row2["SUBITEM_ID"]}' name='{$row2["SUBITEM_ID"]}' rows='3' style='border:1px solid black; width:595px;overflow:auto' maxlength='400' onkeypress='return sot.hp.imposeMaxLength(this, 1000);' data-subitem='{$row2['SUBITEM_ID']}' data-item='{$row['ITEM_NUM']}'></textarea></td>
						</tr>";
					}
					else {
						if($row['ITEM_NUM'] != 32) {
							echo "<tr id='subitem_{$row['ITEM_NUM']}' data-subitem2='{$row2['SUBITEM_ID']}' style='display:none'>
									<td>
										{$row2['SUBITEM_TEXT']}
									</td><td colspan='3'>
										<input type='{$row2['SUBITEM_TYPE']}'  name='subitem_{$row2['SUBITEM_TEXT']}' id='{$row2['SUBITEM_TEXT']}' style='width:595px;' data-subitem='{$row2['SUBITEM_ID']}' data-item='{$row['ITEM_NUM']}'/>
									</td>
								</tr>";
						}
						else {
							echo "<tr id='subitem_{$row['ITEM_NUM']}' data-subitem2='{$row2['SUBITEM_ID']}' style='display:none'>
									<td colspan='4' style='text-align:center'><div id='warningMsg_{$row2['SUBITEM_ID']}'>sdfsdf</div></td>
									</tr>
								<tr id='subitem_{$row['ITEM_NUM']}' data-subitem2='{$row2['SUBITEM_ID']}' style='display:none'>
									<td>
										{$row2['SUBITEM_TEXT']}
									</td><td colspan='3'>
										<input type='{$row2['SUBITEM_TYPE']}'  name='subitem_{$row2['SUBITEM_TEXT']}' id='{$row2['SUBITEM_TEXT']}' style='width:595px;' data-subitem='{$row2['SUBITEM_ID']}' data-item='{$row['ITEM_NUM']}'/>
									</td>
								</tr>";
						}
					}
					$firstSub = false;
				}		
			}
		}
		$categories = array('Location of Event', 'Activity (everything leading up to Event)', 'Event', 'C2 Counter Measures', 'C3 Shared Learnings', 'Electrical Event', 'Electrical Contact Event'
						, 'Arc Flash Event', 'Task Demands');
		
		for($i = 0; $i < count($categories); $i++) {
			$sql = "SELECT * 
					FROM HP_ITEMS 
					WHERE 
						ACTIVE = 1 
					AND
						ITEM_CATEGORY = '{$categories[$i]}'
					ORDER BY ITEM_NUM";
			$prev_category = '';
			$prev_type = '';
			$firstpass = true;
			$x = 0;
			// echo "<script>console.log('{$categories[$i]}', $i, ".count($categories).")</script>";
			$twinPrint = false;
			if($categories[$i] == 'C2 Counter Measures') {
				if(!$twinPrint) {
					echo "</div></table><div style='width:794px;font-weight:bold;background-color:#dadada;margin-top:40px;text-align:left;border:black 1px solid; padding: 2px;'><center>PRECURSORS (CHECK ALL THAT APPLY) Note: Items in <em>italics</em> are more prevalent.</div></center>";
					$twinSql = "SELECT * FROM 
							HP_ITEMS
							WHERE ITEM_CATEGORY IN ('Task Demands', 'Individual Capabilities', 'Work Environment', 'Human Nature')
							ORDER BY ITEM_CATEGORY, ITEM_NUM";
					$twin_analysis = array();
					$twin_analysis['Task Demands'] = array();
					$twin_analysis['Individual Capabilities'] = array();
					$twin_analysis['Work Environment'] = array();
					$twin_analysis['Human Nature'] = array();
					while($row = $oci->fetch($twinSql)) {
						$twin_analysis[$row['ITEM_CATEGORY']][] = array(
							'ITEM_TEXT' => $row['ITEM_TEXT'],
							"HAS_SUBITEMS" => $row['HAS_SUBITEMS'],
							"ACTIVE" => $row['ACTIVE'],
							"SUB_CONDITION" => $row['SUB_CONDITION'],
							'ITEM_NUM' => $row['ITEM_NUM'],
							'HINT' => $row['HINT']
						);
					};
					$x = 0;
					foreach($twin_analysis as $catName => $catValue) {
						echo "<div style='width:50%;float:left;margin-top:5px;margin-bottom:5px;'><b>{$catName}</b>";
						echo "<table style='table-layout:fixed;width:800px;margin:0px;'><col width='3%'><col width='97%'>";
						for($j = 0; $j < count($catValue);$j++) {
							echo "<tr><td><input id='{$catValue[$j]['ITEM_NUM']}' data-item='{$catValue[$j]['ITEM_NUM']}' data-subitem='twin' name='{$catValue[$j]['ITEM_NUM']}' type='CHECKBOX' onclick='sot.hp.twinClick(this);'></td><td class='twin_cell' ".((!empty($catValue[$j]['HINT'])) ? "title='{$catValue[$j]['HINT']}'" : '').">{$catValue[$j]['ITEM_TEXT']}</td></tr>";
							echo "<tr id='{$catValue[$j]['ITEM_NUM']}row' style='display:none'><td>Describe:</td><td><textarea id='ex{$catValue[$j]['ITEM_NUM']}' data-item='{$catValue[$j]['ITEM_NUM']}' data-subitem='twin' name='ex{$catValue[$j]['ITEM_NUM']}' style='width:300px'></textarea></td></tr>";

						}
						echo "</table></div>";
						if($x++ == 1) echo "<div style='clear:both'/>";
					}
					$twinPrint = true;
				}
				echo "</tbody></table><table style='table-layout:fixed;width:800px' id='C2 Counter Measures'><col width='34%'><col width='33%'><col width='33%'>
					<tr style='height:30px;'>
						<td colspan=3></td>
					</tr><th style='font-weight:bold;background-color:#dadada;border:black 1px solid;text-align:left; padding: 2px;' colspan='3'>
						C2 Counter Measures
						</th>
					</tr>";
				$result = array();
				while($row = $oci->fetch($sql)) {
					$result[$row['ITEM_NUM']] = $row; 
				}
				$c2Html = "<tr>
						<td colspan='3'>
						<div id='{$result[150]['ITEM_NUM']}' ".((!empty($row['HINT'])) ? "title='{$row['HINT']}'" : '')." style='width:100px;margin-right:25px;vertical-align:bottom;float:left;margin-top:20px;'>{$result[150][ITEM_TEXT]}</div>
							<div height='30px' style='float:left;width:30px;text-align:center;font-weight:bold;'>Y<br><input type='radio' class='Y' name='{$result[150]['ITEM_NUM']}-entry0' id='{$result[150]['ITEM_NUM']}' value='Y' data-item='{$result[150]['ITEM_NUM']}' data-subitem='0' /></div>
							<div height='30px' style='float:left;width:30px;text-align:center;font-weight:bold;'>N<br><input type='radio' class='N' name='{$result[150]['ITEM_NUM']}-entry0' id='{$result[150]['ITEM_NUM']}' value='N' data-item='{$result[150]['ITEM_NUM']}' data-subitem='0' /></div>
							<div height='30px' style='float:left;width:30px;text-align:center;font-weight:bold;'>N/A<br><input type='radio' class='NA' name='{$result[150]['ITEM_NUM']}-entry0' id='{$result[150]['ITEM_NUM']}' value='N/A' data-item='{$result[150]['ITEM_NUM']}' /></div>
						<div id='{$result[151]['ITEM_NUM']}' title='{$result[151]['HINT']}' style='width:100px;margin-left:350px;margin-right:25px;vertical-align:bottom;float:left;margin-top:20px;'>{$result[151][ITEM_TEXT]}</div>
						<span>
							<div height='30px' style='float:left;width:30px;text-align:center;font-weight:bold;'>Y<br><input type='radio' class='Y' name='{$result[151]['ITEM_NUM']}-entry0' id='{$result[151]['ITEM_NUM']}' value='Y' data-item='{$result[151]['ITEM_NUM']}' data-subitem='0' onclick='sot.hp.markComp();'/></div>
							<div height='30px' style='float:left;width:30px;text-align:center;font-weight:bold;'>N<br><input type='radio' class='N' name='{$result[151]['ITEM_NUM']}-entry0' id='{$result[151]['ITEM_NUM']}' value='N' data-item='{$result[151]['ITEM_NUM']}' data-subitem='0' onclick='sot.hp.markComp();'/></div>
							<div height='30px' style='float:left;width:30px;text-align:center;font-weight:bold;'>N/A<br><input type='radio' class='NA' name='{$result[151]['ITEM_NUM']}-entry0' id='{$result[151]['ITEM_NUM']}' value='N/A' data-item='{$result[151]['ITEM_NUM']}' data-subitem='0' onclick='sot.hp.markComp();'/></div>
						</span></td>
						</tr>
						<tr>
							<td ".((!empty($row['HINT'])) ? "title='{$row['HINT']}'" : '').">{$result[152]['ITEM_TEXT']}</td>
							<td colspan='2'><textarea name='{$result[152]['ITEM_NUM']}' id='{$result[152]['ITEM_NUM']}' data-item='{$result[152]['ITEM_NUM']}' data-subitem='0' rows='3'  style='border:1px solid black; width:99%;overflow:auto' maxlength='400' onkeypress='return sot.hp.imposeMaxLength(this, 1000);'></textarea></td>
						</tr>
						<tr>
							<td/>
							<td ".((!empty($row['HINT'])) ? "title='{$row['HINT']}'" : '').">{$result[153]['ITEM_TEXT']}</td>
							<td> Owner Name (Auto Populated) </td>
						</tr><tr>
							<td/>
							<td><input type='text' style='width:97%;border:1px solid black'  id='{$result[153]['ITEM_NUM']}' data-item='{$result[153]['ITEM_NUM']}' data-subitem='0' name='{$result[153]['ITEM_NUM']}' onkeyup='sot.hp.inputUsid(this, 0)' onkeyup='sot.hp.inputUsid(this, 153)'/></td>
							<td><input type='text' style='width:97%;border:1px solid black'  id='{$result['153.5']['ITEM_NUM']}_auto' data-item='{$result['153.5']['ITEM_NUM']}' data-subitem='0' name='{$result['153.5']['ITEM_NUM']}' disabled/></td>
						</tr><tr>
							<td/>
							<td ".((!empty($row['HINT'])) ? "title='{$row['HINT']}'" : '').">{$result[154]['ITEM_TEXT']}</td>
							<td ".((!empty($row['HINT'])) ? "title='{$row['HINT']}'" : '').">{$result['154.5']['ITEM_TEXT']}</td>
						</tr><tr>
							<td/>
							<td><input type='text' style='width:97%;border:1px solid black'  id='{$result[154]['ITEM_NUM']}-entry0' data-item='{$result[154]['ITEM_NUM']}' data-subitem='0' name='{$result[154]['ITEM_NUM']}' onkeydown='if(!((window.event.keyCode == 9)||(window.event.keyCode == 8))) { return false; }' onclick='sot.hp.cal(\"{$result[154]['ITEM_NUM']}-entry0\", \"{$result[154]['ITEM_NUM']}-entry0\");'/></td>
							<td><input type='text' style='width:97%;border:1px solid black'  id='{$result['154.5']['ITEM_NUM']}-entry0' data-item='{$result['154.5']['ITEM_NUM']}' data-subitem='0' name='{$result['154.5']['ITEM_NUM']}' onkeydown='if(!((window.event.keyCode == 9)||(window.event.keyCode == 8))) { return false; }' onclick='sot.hp.cal(\"{$result['154.5']['ITEM_NUM']}-entry0\", \"{$result['154.5']['ITEM_NUM']}-entry0\");'/></td>
						</tr><tr>	
							<td ".((!empty($row['HINT'])) ? "title='{$row['HINT']}'" : '').">{$result[155]['ITEM_TEXT']}</td>
							<td ".((!empty($row['HINT'])) ? "title='{$row['HINT']}'" : '').">{$result[156]['ITEM_TEXT']}</td>
							<td ".((!empty($row['HINT'])) ? "title='{$row['HINT']}'" : '').">{$result[157]['ITEM_TEXT']}</td>
						</tr><tr>
							<td><select style='width:97%;border:1px solid black'  id='{$result[155]['ITEM_NUM']}' data-item='{$result[155]['ITEM_NUM']}' data-subitem='0' name='{$result[155]['ITEM_NUM']}'>
								<option data-item='155' data-subitem='' value=''></option>
								<option data-item='156' data-subitem='103' value='P0 No Process in place'>P0 No process in place</option>
								<option data-item='156' data-subitem='104' value='P1 Process not followed'>P1 Process not followed</option>
								<option data-item='156' data-subitem='105' value='P2 Process needs improvement'>P2 Process needs improvement</option>
							</select></td>
							<td><select style='width:97%;border:1px solid black'  id='{$result[156]['ITEM_NUM']}' data-item='{$result[156]['ITEM_NUM']}' data-subitem='0' name='{$result[156]['ITEM_NUM']}'>
								<option data-item='156' data-subitem='' value=''></option>
								<option data-item='155' data-subitem='106' value='Output'>Output</option>
								<option data-item='155' data-subitem='107' value='Pathway'>Pathway</option>
								<option data-item='155' data-subitem='108' value='Connection'>Connection</option>
								<option data-item='155' data-subitem='109' value='Activity'>Activity</option>
							</select></td>
							<td><input type='text' style='width:97%;border:1px solid black'  id='{$result[157]['ITEM_NUM']}' data-item='{$result[157]['ITEM_NUM']}' data-subitem='0' name='{$result[157]['ITEM_NUM']}'/></td>
						</tr>";
				echo "</table><div id='c2Additional' style='display:none'>
											<div id='c2ContentBlock'>
												<table style='table-layout:fixed;width:800px;' id='c20'>
													<col width='34%'><col width='33%'><col width='33%'>
														{$c2Html}
													<tr>
														<td colspan='3' style='text-align:right'>
															[&nbsp;<span onclick='x = this.parentNode.parentNode.parentNode.parentNode; x.parentNode.removeChild(x);sot.hp.markComp();' onMouseOver='this.style.cursor=\"pointer\"' style='text-decoration:underline;color:#0066cc;font-size:10px;'>Remove</span>&nbsp;]
														</td>
													</tr>
												</table>
											</div>
										</div>";
				echo "[&nbsp;<span onclick='sot.hp.addCountermeasure(\"0\"); return false;' onMouseOver='this.style.cursor=\"pointer\"' style='text-decoration:underline;color:#0066cc;font-size:10px'>Add a Counter Measure</span>&nbsp;]";
				echo "</table><table style='table-layout:fixed;width:800px;'><col width='25%'><col width='5%'><col width='5%'><col width='65%'>";
			}
			if($categories[$i] == 'C3 Shared Learnings') {
				echo "<table style='table-layout:fixed;width:800px;'><tbody>";
				while($row = $oci->fetch($sql)) {
					echo "</tbody><tbody id='{$row['ITEM_CATEGORY']}'><tr style='height:10px;'>
							<td colspan=4></td>
						</tr>
						<tr>
							<td><div style='font-weight:bold;background-color:#dadada;border:black solid 1px;padding: 2px;'>{$row["ITEM_TEXT"]}</div></td>
							<td colspan = '3'><input type='text' name='{$row['ITEM_NUM']}' id='{$row['ITEM_NUM']}' data-subitem='null' data-item='{$row['ITEM_NUM']}' maxlength='400' style ='border:1px solid #000; width:595px;height:17px;/></td>
						</tr>";
						$prev_category = 'C3 Shared Learnings';
				}
				echo "</tbody></table><table style='table-layout:fixed;width:800px;'><tbody>";
			}
			else {
				while($row = $oci->fetch($sql)) {
					print_row($row, $prev_category, $prev_type, $x, $oci);
				}
			}
		}
		
		//twin analysis

	?>
	<div style='clear:both;'/>
	<div style='margin-top:20px;'>
		<form name='photoInp' action='../src/php/engine.php?f=saveHP' method='POST' target='submit' target='submit' enctype="multipart/form-data">
			<input type="text" id='inputObjs' name='inputObjs' style='display:none' hidden>
			<div style='font-weight: bold;'>Photos</div>
			<div id='photos'>
		<?php
			require_once("/opt/apache/servers/soteria/htdocs/src/php/photos.php");
			$x = getPhotos('HP', empty($hp_id) ? 0 : $hp_id, $oci, true);
			echo "<input type='hidden' name='p_count' id='p_count' value='{$x}'/>";
		?>
			</div>
			<span style='font-size: 10px;'>[ <a href='#' onclick='addPhoto(); return false;'>Add Photo</a> ]</span>
		</form>
	</div>
	<table>
		<tr>
			<td style='font-weight:bold;vertical-align: bottom;'>Additional Comments</td>
		</tr>
		<tr>
			<td><textarea style='width:795px;' name='comments' id='comments' class='header' onkeypress='return sot.hp.imposeMaxLength(this, 1000);'></textarea></td>
		</tr>
	</table><input id='complete' style='display:none' type='checkbox'/>
	<center style='margin-top:10px; margin-bottom: 10px;'>
		<!-- <input style='display:none' id='complete' value='0'></input> -->
		<button id='submit' onclick='sot.hp.onSubmit();return false;'>Submit</button>&nbsp;
		<button onclick='window.location="../index.php"; return false;'>Cancel & Go Back</button>
	</center>
<iframe name='submit' style='width:0px; height:0px;'></iframe>
<script>
	$('td[title]').not('.twin_cell').qtip({ 
		position: {
			corner: {
				target: 'topMiddle',
				tooltip: 'bottomMiddle'
			}
		},
		show: {
			delay: 0
		},
		style: {
			tip:'bottomMiddle',
			width: 200,
			border: {
				width: 2
			}
		}
	});
	$('.twin_cell[title]').qtip({ 
		position: {
			corner: {
				target: 'topLeft',
				tooltip: 'bottomLeft'
			}
		},
		show: {
			delay: 0
		},
		style: {
			tip:'bottomLeft',
			width: 200,
			border: {
				width: 2
			}
		}
	});
	sot.hp.edits();
	// sot.hp.disable();
</script>

