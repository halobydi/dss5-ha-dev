<?php
	function errorHandler($errno, $errstr, $errfile, $errline, $errcontext){
		
	}
	set_error_handler('errorHandler');
	require_once("mcl_Oci.php");
	$id = $_GET["id"];

	$oci = new mcl_Oci("soteria");
	$sql = "SELECT * FROM RED_TAG_ATTACHMENTS WHERE ATTACHMENT_ID = {$id}";

	$row = $oci->fetch($sql);

	//6756
	if(empty($row)){
		die("<script type = 'text/javascript'>
			alert('Unable to open file!');
			//window.close();
		</script>");
	}
	
	$name = $row["NAME"] . "." . $row["EXTENSION"];
	
	$contents = "";
	if(!empty($row["CONTENTS"])) {$contents = $row["CONTENTS"]->load(); }
	
	while(ob_get_level()) ob_end_clean();
	
	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Cache-Control: private", false);
	header("Content-Type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"{$name}\";" );
	header("Content-Transfer-Encoding: binary");

	echo urldecode($contents);	
?>