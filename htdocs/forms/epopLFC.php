   <script>
function openFieldForm() {
    window.location.assign("lifeCritical.php")
}
</script>
<?php
function errorHandler($errno, $errstr, $errfile, $errline, $errcontext){

}
set_error_handler('errorHandler');
require_once('mcl_Html.php');
require_once('mcl_Oci.php');
require_once("/opt/apache/servers/soteria/htdocs/src/php/auth.php");
$user = auth::check();
$login_warning = '';
if($user["status"] != "authorized"){
	$login_warning = '<b>Warning</b>: You are not logged into the application. Submitting an observation without logging in will result in an anonymous observation. <a href="../index.php">Click here</a> to log in.';
}

mcl_Html::title("SOTeria - Enterprise Performance Observation");

mcl_Html::js(mcl_Html::DOJO);
mcl_Html::js(mcl_Html::DOJO_WINDOW);
mcl_Html::js(mcl_Html::AJAX);
mcl_Html::js(mcl_Html::CALENDAR);

mcl_Html::no_cache(true);
mcl_Html::s(mcl_Html::INC_JS, "../src/sot.js");

//mcl_Html::css(mcl_Html::CALENDAR);
mcl_Html::s(mcl_Html::INC_CSS, "{$baseDir}/src/css/jscal2.css");
mcl_Html::s(mcl_Html::INC_CSS, "../src/css/form.css");

$type = $_GET['type'] == 'office' ? 'office' : 'field';
$switchType = $_GET['type'] == 'office' ? 'field' : 'office';

if($_GET["print"] == true) {
	mcl_Html::s(mcl_Html::SRC_JS, "
	window.onload = function() {
		window.print();
		window.close();
	}
");

	$form = "Safe Worker Observation - Life Critical";
	if($_GET["pp"] == true) {
		$form = "Paired Performance Observation";
	} else if($_GET["qew"] == true) {
		$form = "Qualified Electrical Worker Observation";
	}

	echo <<<DIV
	<div id='print' style='width: 100%; height: 100px; font-size: 26px; text-align: center;'/>
		{$form}
	</div>
DIV;
}

$oci = new mcl_Oci('soteria');

$sql = "SELECT * FROM ORGANIZATIONS ORDER BY ORG_CODE";

$orgs = array();
while($row = $oci->fetch($sql)){
	$orgs[$row["ORG_CODE"]] = array(
		"code"		=> $row["ORG_CODE"],
		"title"		=> $row["ORG_TITLE"]
	);
}

function debug_to_console($data) {
    if(is_array($data) || is_object($data))
	{
		echo("<script>console.log('PHP: ".json_encode($data)."');</script>");
		if(is_array($data))
		{
			echo("<script>console.log('PHP: ".count($data)."');</script>;");
		}
	} else {
		echo("<script>console.log('PHP: ".$data."');</script>");
	}
}

$yesNoQuestions = array();
echo "<script>console.log(\"Getting life critical questions\");</script>";
$sql = "SELECT * FROM LIFE_CRITICAL ORDER BY LC_NUM";
while($row = $oci->fetch($sql)) {
	$yesNoQuestions[$row['LC_CODE']] = $row["LC_TEXT"];
}

debug_to_console($yesNoQuestions);
	
$category = "";
$swo = false;
$qew = false;
$pp = false;

if(isset($_GET["swo"])){ $swo = true; }
if(isset($_GET["qew"]) && !$swo){ $qew = true; }
if(isset($_GET["pp"])){ $pp = true; }
if(!$pp && !$qew){ $swo = true; }
if($swo){ $category .= "'Leader', 'Individual', 'Organization'"; }
if($qew){ $category .= (empty($category) ? "" : ", ") . "'Qualified Electrical Worker'"; }
if($pp) { $category .= (empty($category) ? "" : ", ") . "'Paired Performance'";}

if(!empty($_GET["usid"])){
	mcl_Html::s(mcl_Html::SRC_JS,"
			window.onload = function(){
				sot.swo.inputUsid(dojo.byId('usid_1'), 1);
			}
		");
}

$goBack = $_GET['back'] ?: '../index.php';


mcl_Html::s(mcl_Html::SRC_JS,<<<JS
	function onClickYesNo(name, yes) {
		if(dojo.byId(name + '_required_comments')) {
			if(yes) {
				dojo.byId(name + '_required_comments').style.display = '';
			} else {
				dojo.byId(name + '_required_comments').style.display = 'none';
			}
		}
		if(dojo.byId(name + '_io_questions')) {
			if(yes) {
				dojo.byId(name + '_io_questions').style.display = '';
			} else {
				dojo.byId(name + '_io_questions').style.display = 'none';
			}
		}
	}
	function toggleComments() {
		if (dojo.byId('conversation').value == 1) {
			dojo.byId("comment_box").style.display = '';
		} else {
			dojo.byId("comment_box").style.display = 'none';
		}
	}
JS
);
?>
<div class='header' style='margin: 0 auto;'>
	Enterprise Performance Observation - Life Critical
</div>
<?php
$url = "epopLFC.php?sid=" . time();
//$url2 = "";
//if($swo){ $url .= "&swo"; $url2 .= "&swo"; }
//if($qew){ $url .= "&qew"; $url2 .= "&qew";}
//if($pp){ $url .= "&pp"; $url2 .= "&pp";}

$completed_by = auth::check("usid");
if(!empty($_GET["delegate"])){
	$delegates = auth::check('delegates');
	if(!empty($delegates[$_GET["delegate"]]["name"])){
		$delegator = " for {$delegates[$_GET["delegate"]]["name"]}";
		$completed_by = $delegates[$_GET["delegate"]]["usid"];
	}

	echo "<div class='form_warning'>
			<b>Warning</b>: You are in delegate mode{$delegator}. This observation will be saved as completed by you, but your delegator will receive the leader completion credit. <a href='{$url}'>Click here</a> to turn off delegate mode.
		</div>";
}
if(!empty($login_warning)){
	echo "<div class='form_warning'>
			{$login_warning	}	
		</div>";
}
?>
<div id='container' style='margin: 0 auto;'>
	<form name = 'swo' action = '../src/php/engine.php?f=saveEPOP<?php echo "{$url2}"; echo (!empty($_GET["delegate"]) ? "&delegate={$_GET["delegate"]}" : ""); ?>' method='POST' target='submit' onsubmit='return sot.swo.onSubmit();' target='submit' enctype="multipart/form-data">
		<table>
			<tr>
				<td style='width:150px; font-weight:bold;'>
					Employee Observed:</br>
					<span style='font-size: 10px; font-weight: normal;'>Enter username (e.g. u12345)</span>
				</td>
				<td style='width:250px;'><input type='text' maxlength='6' id='usid_1' name='usid[]' autocomplete='off' value="<?php echo $_GET["usid"]; ?>" maxlength='6' onkeyup='sot.swo.inputUsid(this, 1); return false;' onchange='sot.swo.inputUsid(this, 1); return false;'/></td>
				<td style='width:150px; font-weight:bold;'>Date Observed:</td>
				<td style='width:250px;'>
					<input type='text' id='observed_date' maxlength='10' onkeydown='if(window.event.keyCode != 9) { return false; }' name='observed_date' id='observed_date' value="<?php echo date("m/d/Y"); ?>"/>
					<img src='../src/img/calendar.gif' alt='' id="tcal" onmouseover="sot.swo.cal('tcal', 'observed_date');" />
				</td>
			</tr>
			<tr>
				<td style='width:150px; font-weight:bold;'>Employee Name:</br><span style='font-size: 10px; font-weight: normal;'>Auto-populated</span></td>
				<td style='width:250px;'><input type='text' maxlength='350' id='name_1' name='name[]' value="<?php echo $name; ?>"/><input type='hidden' id='nameValidate_1' value="<?php echo $name; ?>"/></td>
				<td style='width:150px; font-weight:bold;'>Observed By:</br><span style='font-size: 10px; font-weight: normal;'><?php if(empty($_GET["delegate"])) { echo "Leave blank for anonymous"; } ?></span></td>
				<td style='width:250px;'>
					<input type='text' id='observed_by' name='observed_by' value="<?php echo (is_array(auth::check("name")) ? "" : auth::check("name"));?>"/>
					<input type='hidden' id='completed_by' name='completed_by' value="<?php echo $completed_by; ?>"/>
				</td>
			</tr>
			<tr>
				<td style='width:147px; font-weight:bold;'>Organization:</td>
				<td>
					<select name='org' id='org' onchange='sot.swo.switchOrg();'>
						<option value=''></option>
						<?php
						foreach($orgs as $key=>$value){
							$selected = ($org == $key ? 'selected=selected' : '');
							echo "<option value='{$key}' {$selected}>{$value["title"]}</option>";
						}
						?>
					</select>
				</td>
				<td style='width:147px; font-weight:bold;'>Observation Type:</td>
				<td>
					<?php if($swo): ?>
						<?=ucfirst($type)?>
						- <a onclick='openFieldForm()' href='#' id='switchType'>Switch to <?=ucfirst($switchType)?></a>
						<input type='hidden' name='office_field' id='office_field' value='<?=strtoupper($type)?>' />
					<?php else: ?>
						<select name='office_field'>
							<option value='FIELD'>Field</option>
							<option value='OFFICE'>Office</option>
						</select>
					<?php endif; ?>
				</td>
			</tr>
			<tr id='location_c' style=''>
				<td style='width:147px; font-weight:bold;'>Location:<br/><span style='font-size: 10px; font-weight: normal;'>Location of observation</span></td>
				<td colspan='3'>
					<input type='text' maxlength='350'  style='width: 603px;' id='location' name='location' value=""/>
				</td>
			</tr>
		</table>
		<div style="text-align: center">
			<img src="../src/img/lifeCriticalStandards.jpg" />
		</div>
		<div style='text-align: center; margin-top:10px; margin-bottom: 10px;'>
			<button
				id="markAllSatisfactoryBtn"
				onclick="sot.swo.markAllSat(); return false;"
				type="button">
					Mark All Satisfactory
			</button>
			&nbsp;
			<button
				onclick='window.location="<?=$goBack?><?php echo (!empty($_GET["delegate"]) ? "?delegate={$_GET["delegate"]}" : "");?>"; return false;'
				type="button">
					Cancel & Go Back
			</button>
		</div>

		<table>
			<tr>
				<td colspan='4' style='padding-bottom:10px; font-size: 11px;'>
					<b>S</b> - Satisfactory <b>
						IO</b> - Improvement Opportunity <b>
						NA</b> - Not Applicable
				</td>
			</tr>
			<?php
				if($swo){
					echo "<input type='hidden' name='swo' value ='on'/>";
				}

				$sql = "
					SELECT
						*
					FROM
						EPOP_ITEMS
					WHERE
						ACTIVE = 1
						AND ITEM_CATEGORY IN({$category})
						AND {$type} = 1
						AND LIFE_CRITICAL = 1
					ORDER BY
						ITEM_CATEGORY DESC,
						ITEM_NUM
					";
				
				//echo "<pre>{$sql}</pre>";
				
				$prevCategory = '';
				$firstPass = true;

				function printYesNoQuestion($name, $text, $requireComments = false) {
					global $oci;
					
					
					$sql = "SELECT * FROM EPOP_ITEMS WHERE ITEM_CATEGORY = '{$name}'";
					$subItems = array();
					while($row = $oci->fetch($sql)) {
						$subItems[] = $row;
					}
					
					$disabled = count($subItems) == 0 && $name != 'procedure_for_work';
					
					echo "
						<table>
								<tr style='font-weight:normal; background-color: " . ($disabled ? "#F0F0F0" : "#FFE019") . ";'>
									<td colspan='3' style='padding-top: 3px; padding-bottom: 3px; padding-left: 7px; width: 135px; '>
										<input type='radio' name='{$name}' id='{$name}' value='1' onclick='onClickYesNo(\"{$name}\", true)'/ " . ($disabled ? "disabled=disabled" : "") . "> Yes
										&nbsp;<input type='radio' name='{$name}' id='{$name}' value='0' onclick='onClickYesNo(\"{$name}\", false)'/ " . ($disabled ? "disabled=disabled" : "") . "> No
									</td>
									<td colspan='2' style='width: 690px; font-weight: bold;'>{$text}</td>
								</tr>
							";

					if($requireComments) {
						echo "<tr id='{$name}_required_comments' style='display: none;'>
										<td colspan='5'>
											<textarea name='{$name}_detail' id='{$name}_detail' style='width: 100%;' onkeypress='return sot.swo.imposeMaxLength(this, 1000);'></textarea>
										</td>
								</tr>
							";
					}
					echo "</table>";

					$x = 0;
					foreach($subItems as $row) {
						if($x == 0) {
							echo "<table id='{$name}_io_questions' style='display: none;'>";
							printHeader("Additional Questions for <i>" . $text . '</i>');
						}
						printQuestion($row, ++$x, true);
					}
					if($x > 0) { echo "</table>"; }
				}
				
				function printHeader($category) {
						$style = "text-align:center; border-bottom: 1px solid #000; border-top: 1px solid #000; padding: 3px;";

						echo "
							<tr style='font-weight:normal; background-color: #f0f0f0;'>
								<td style='width:30px; {$style}'>S</td>
								<td style='width:30px; {$style}'>IO</td>
								<td style='width:30px; {$style}'>NA</td>
								<td style='width:710px; {$style}'>{$category}</td>
								<td style='border-bottom: 1px solid #000; border-top: 1px solid #000; padding: 3px;'></td>
							</tr>
						";
				}

				function printQuestion($row, $x, $lcPilot = '') {
					
					if($row["HAS_SUBITEMS"] == '1'){
							$hasSubItems = 'true';
					} else {
						$hasSubItems = 'false';
					}

					$class = ($x % 2 == 0 ? 'even' : 'odd');
					echo "
						<tr class='{$class}'>
							<td style='width:30px; text-align:center; vertical-align:top;'>
								<input lc='{$lcPilot}' type='radio' class='S' name='{$row["ITEM_NUM"]}' value='S' onclick='sot.swo.markItem({$row["ITEM_NUM"]}, \"S\", {$hasSubItems}, true);' />
							</td>
							<td style='width:30px; text-align:center; vertical-align:top;'>
								<input lc='{$lcPilot}' type='radio' class='IO' id = '{$row["ITEM_NUM"]}' name='{$row["ITEM_NUM"]}' value='IO' onclick='sot.swo.markItem({$row["ITEM_NUM"]}, \"IO\", {$hasSubItems}, true);' />
							</td>
							<td style='width:30px; text-align:center; vertical-align:top;'>
								<input lc='{$lcPilot}' type='radio' class='NA' name='{$row["ITEM_NUM"]}' value='NA' onclick='sot.swo.markItem({$row["ITEM_NUM"]}, \"NA\", {$hasSubItems}, true);' />
							</td>
							<td style='width:710px;'>
								{$row["ITEM_TEXT"]}
							</td>
							<td style='text-align:right;'>
								<!---<img src='../src/img/comments.png' id='{$row["ITEM_NUM"]}_comments_img' style='cursor:pointer;' alt='Add Comment' onclick='sot.swo.addComment({$row["ITEM_NUM"]}, this);'/>---!>
							</td>
						</tr>
						<tr style=''>
							<td style='width:800px;' colspan='5' id='{$row["ITEM_NUM"]}_comments'></td>
							</td>
						</tr>
					";

					if($hasSubItems == 'true') {
						echo "
							<tr style='padding: 0px; margin: 0px;'>
								<td id='{$row["ITEM_NUM"]}_subitems'
									colspan='5' style='width: 800px;
									padding: 0px; margin: 0px;
									background-color: #ffffca;'>
								</td>
							</tr>
						";
					}
				}

				while($row = $oci->fetch($sql)){
					if($row["ITEM_CATEGORY"] != $prevCategory){
						$x = 0;

						if(!$firstPass){
							echo "
								<tr style='height:30px;'>
									<td colspan=5></td>
								</tr>
							";
						}
						printHeader($row['ITEM_CATEGORY']);

						$firstPass = false;
					}

					$x++;

					printQuestion($row, $x);

					$prevCategory = $row["ITEM_CATEGORY"];

				}

				foreach($yesNoQuestions as $key=>$value) {
					printYesNoQuestion($key, $value);
				}
			?>
		</table>
		<?php	if(strtoupper($type) == 'OFFICE' && $swo): ?>
<!--			<div style='margin-top: 10px; margin-bottom: 5px; text-align: right;'>-->
<!--				Was this observation conducted at an <b>offsite</b> location?-->
<!--				<select id='offsite_onsite' style='width: 80px;' onchange='sot.swo.toggleSiteItems(this.value);'>-->
<!--					<option value='onsite'>-->
<!--						No-->
<!--					</option>-->
<!--					<option value='offsite'>-->
<!--						Yes-->
<!--					</option>-->
<!--				</select>-->
<!--			</div>-->
<!--			<div id='offsite' style='display: none;'>-->
<!---->
<!--			</div>-->
<!--			<div id='onsite'>-->
				<?php
//				$sql = "
//					SELECT
//						*
//					FROM
//						EPOP_ITEMS
//					WHERE
//						ITEM_CATEGORY = 'Onsite'
//						AND OFFICE = 1
//						AND LIFE_CRITICAL = 1
//					";
//				$siteItems = array();
//				while($row = $oci->fetch($sql)) {
//					$siteItems[] = $row;
//				}
//				?>
<!--				--><?php //if($siteItems): ?>
<!--					<table>-->
<!--						--><?//=printHeader('Onsite')?>
<!--						--><?php //foreach($siteItems as $x=>$row): ?>
<!--							--><?//=printQuestion($row, $x); ?>
<!--						--><?php //endforeach; ?>
<!--					</table>-->
<!--				--><?php //endif; ?>
<!--			</div>-->
<!--		--><?php //endif; ?>
<!--		--><?php //if(!$qew && $swo): ?>
<!--			<div style='margin-top: 10px; margin-bottom: 5px; text-align: right;'>-->
<!--				Would you like to also complete a <b>Qualified Electrical Worker</b> observation with this submission?-->
<!--				<select  style='width: 80px;' onchange='sot.swo.addItems(this.value == 1 ? true : false, "qew", "Qualified Electrical Worker");' name='qew_add'>-->
<!--					<option value='0'>No</option>-->
<!--					<option value='1'>Yes</option>-->
<!--				</select>-->
<!--			</div>-->
		<?php endif; ?>
<!--		<div id='qew' style='display: none;'></div>-->
	     <div style='margin-top: 10px; margin-bottom: 5px; text-align: right;'>
			Did a conversation take place with the observed individual?
			<select  style='width: 80px;'name='conversation' id='conversation' onchange='toggleComments();'>
				<option value=''></option>
				<option value='0'>No</option>
				<option value='1'>Yes</option>
			</select>
		</div>
		<table style='display: none;' id='comment_box'>
			<tr>
				<td style='font-weight:bold;vertical-align: bottom;'>
					Comments
				</td>
			</tr>
			<tr>
				<td>
					<textarea style='width:795px;' name='comments' id='comments' onkeypress='return sot.swo.imposeMaxLength(this, 1000);'></textarea>
				</td>
			</tr>
		</table>
		<div style='margin-top: 5px; margin-bottom: 5px; text-align: right;'>
			Was good safety behavior recognized during this observation?
			<select style='width: 80px;'name='good_behavior'>
				<option value=''></option>
				<option value='0'>No</option>
				<option value='1'>Yes</option>
			</select>
		</div>
		
		<div style='font-weight: bold; position: relative; margin-top: 5px;'>
			Photos
		</div>
		<div id='photos'>
			<input type='hidden' id='p_count' name='p_count' value='0'>
		</div>
		<span style='font-size: 10px;'>[ <a href='#' onclick='addPhoto(); return false;'>Add Photo</a> ]</span>
		
		<span style='font-size: 10px;' >[ <a href="#" onclick='sot.swo.addEmployee(); return false;' >Add an Additional Employee to Observation</a> ]</span>
		<div id='add_emp' style='margin-top:5px;'></div>
		<div style='text-align: center; margin-top:10px; margin-bottom: 10px;'>
			<input type = 'hidden' value = '<?php echo $_GET["delegate"]; ?>' name = 'delegator'/>
			<input type = 'hidden' value = '<?php echo date("m/d/Y H:i:s"); ?>' name = 'begin_date'/>
			<input type = 'submit' id='submit' value='Submit'>&nbsp;
			<button onclick='window.location="<?=$goBack?><?php echo (!empty($_GET["delegate"]) ? "?delegate={$_GET["delegate"]}" : ""); ?>"; return false;'>Cancel & Go Back</button>
		</div>
		<input type="hidden" id="lc_pilot" name="lc_pilot" value="1" />
	</form>
</div>
<iframe name='submit' style='width:0px; height:0px;'></iframe>