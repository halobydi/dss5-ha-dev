<?php
/*
Change Log
DS-148 12-21-2016  priority replaced with Serious? GalaxE Detroit Edward King

*/
	function errorHandler($errno, $errstr, $errfile, $errline, $errcontext){
		
	}
	set_error_handler('errorHandler');
	require_once('mcl_Html.php');
	require_once('mcl_Oci.php');
	require_once("/opt/apache/servers/soteria/htdocs/src/php/auth.php");
	
	mcl_Html::js(mcl_Html::DOJO);
	mcl_Html::js(mcl_Html::DOJO_WINDOW);
	mcl_Html::js(mcl_Html::AJAX);
	mcl_Html::js(mcl_Html::JQUERY);
	mcl_Html::js(mcl_Html::CALENDAR);
	mcl_Html::s(mcl_Html::INC_JS, "../src/sot.js");
	mcl_Html::s(mcl_Html::INC_JS, "../src/js/other/jquery.mask.js");
	mcl_Html::s(mcl_Html::INC_JS, "../src/js/other/jquery.qtip-1.0.0-rc3.min.js");

	//mcl_Html::css(mcl_Html::CALENDAR);
	mcl_Html::s(mcl_Html::INC_CSS, "../src/css/jscal2.css");
	mcl_Html::s(mcl_Html::INC_CSS, "../src/css/form.css");
	mcl_Html::s(mcl_Html::INC_CSS, "../src/css/form_alt.css");
	mcl_Html::s(mcl_Html::INC_CSS, "../src/css/menu.css");

	$form = $_GET["gc"] == "true" ? "Good Catch" : "Near Miss";
	$gc = $form == "Good Catch" ? true : false;
	mcl_Html::title("SOTeria - {$form}");
	
	$login_warning = '';
	$user = auth::check();
	if($user["status"] != "authorized"){
		$login_warning = '<b>Warning</b>: You are not logged into the application. Submitting an observation without logging in will result in an anonymous observation. 
		<a href="../' . ($_GET["mobile"] == "true" ? "mobile/" : "") . 'index.php">Click here</a> to log in.';
		if($gc) {
			header('Location: ../index.php');
		}
	}
	
	$definition = array(
		"Near Miss"		=> "Unplanned events that do not cause injury, damage, or workplace illness, but could have. This definition would include issues, acts, and behaviors.",
		"Good Catch"	=> "Identifying, reporting and correcting potential safety hazards, issues, or other types of human performance errors before a near miss or actual incident/injury occurs."
	);
	$type = array(
		"Near Miss"		=> "NM - Near Miss",
		"Good Catch"	=> "GC - Good Catch"
	);
	
	$oci = new mcl_Oci('soteria');
	$edit = false;
	$user = auth::check();
	if(!empty($_GET['delegate']) && !empty($user['delegates'][$_GET['delegate']])) {
		$usid = $user['delegates'][$_GET['delegate']]['usid'];
	} else {
		$usid = $user['usid'];
	}

	if(!empty($_REQUEST["nm_id"]) || !empty($_REQUEST["gc_id"])) {
	
		$where = ($gc ? "GC_ID = {$_REQUEST["gc_id"]}" : "NM_ID = {$_REQUEST["nm_id"]}");
		
		$sql = "ALTER SESSION SET NLS_DATE_FORMAT = 'MM/DD/YYYY'";
		$oci->query($sql);
		
		$sql = "SELECT 		*
				FROM 		" . ($gc ? "GOOD_CATCH" : "NEAR_MISS") ."_OBSERVATIONS O
				LEFT		JOIN EMPLOYEES E ON E.USID = O.COMPLETED_BY
				WHERE 		{$where}";
		
		if($data = $oci->fetch($sql)) { $edit = true; }
		$in_path = strpos($data["PATH"], $usid);
	
		if($usid == $data['COMPLETED_BY'] || $usid == $data['COMPLETED_BY_SUPERVISOR'] || $usid == $data['DIRECTOR'] || $usid == $data['EDIT_USID'] || $usid == $data['PERSON_RECOGNIZED']) {
			$in_path = true;
		}
			
		$completed_by = $data["COMPLETED_BY"];
		$completed_by_phone = preg_replace("/^(\d{3})(\d{3})(\d{4})$/", "$1.$2.$3", $data["COMPLETED_BY_PHONE"]);
		$completed_by_title = $data["COMPLETED_BY_TITLE"];
		$completed_by_supervisor = $data["COMPLETED_BY_SUPERVISOR"];
		$completed_by_supervisor_phone =  preg_replace("/^(\d{3})(\d{3})(\d{4})$/", "$1.$2.$3", $data["COMPLETED_BY_SUPERVISOR_PHONE"]);
		$manager = mcl_Ldap::lookup($data["COMPLETED_BY_SUPERVISOR"]);
		
		$director = @mcl_Ldap::lookup($data["DIRECTOR"]);
		
		if(!$gc) {
			mcl_Html::s(mcl_Html::SRC_JS, "
				dojo.addOnLoad(function() {
					change_cat({$data["CATEGORY"]});
				});
			");
		}
		
		$date = $data["INCIDENT_DATE"];
		$time = $data["INCIDENT_TIME"];
		$hour = substr($time, 0, 2);
		$minute = substr($time, 3, 2);
		$ampm = substr($time, 6, 2);
		//$priority = $data['PRIORITY'];

		if(!$gc) {
			$twin = array();
			$sm = array();
			
			$nm_id = $_REQUEST['nm_id'];
			$sql = "SELECT * FROM TWIN_ANSWERS WHERE OBSERVATION_FORM = 'NM' AND OBSERVATION_ID = '{$nm_id}'";
			while($row = $oci->fetch($sql)) {
				$twin[$row["ITEM_NUM"]] = $row;
			}
			
			$sql = "SELECT * FROM NEAR_MISS_SM_ANSWERS WHERE NM_ID = '{$nm_id}'";
			while($row = $oci->fetch($sql)) {
				$sm[$row["SAP_NUMBER"]] = $row;
			}
		}
		
		if($_GET["mode"] == "readonly") {
			mcl_Html::s(mcl_Html::SRC_JS, "
				dojo.addOnLoad(function() {
					disabled = true;
					disable();
				});
			");
		}
	
		$privileges = $user['privileges'];
		$admin = false;
		if($privileges["MANAGE_NEAR_MISS"]){
			$admin = true;
		}

		if($_GET["mode"] == "edit" && !($admin || $in_path)) {
			mcl_Html::s(mcl_Html::SRC_JS, "
				dojo.addOnLoad(function() {
					disabled = true;
					disable();
				});
			");
		}
		
	} else {
	
		$manager = mcl_Ldap::lookup($user["details"]["manager"]);
		//find director
		$leafusid = $user["usid"];
		$data["PERSON_RECOGNIZED"] = $leafusid;
		$loop = true;
		$foundDirector = false;
		do {
			$leaf = @mcl_Ldap::lookup($leafusid);
			$isDirector = strpos(strtolower($leaf["title"]), "director");
			
			if($isDirector || $isDirector === 0) {
				$loop = false;
				$director = $leaf;
				$foundDirector = true;
			}
			
			if(!$leaf["manager"]) {	$loop = false; } 
			$leafusid = $leaf["manager"];
			
		} while($loop); 
		
		if(!$foundDirector) {
			$director = @mcl_Ldap::lookup($manager["manager"]);
		}
		
		$completed_by = $user["usid"];
		$completed_by_title = $user["details"]["title"];
		$completed_by_phone = $user["details"]["all"][0]["telephonenumber"][0];
		$completed_by_supervisor = $manager["username"]; 
		$completed_by_supervisor_phone = $manager["all"][0]["telephonenumber"][0]; 
		
		$date = date("m/d/Y");
		$hour = date("h");
		$minute = date("i");
		$ampm = date("H") > 12 ? "PM" : "AM";
	}
mcl_Html::s(mcl_Html::SRC_CSS, "
	.tooltip {
    position: relative;
    display: inline-block;

}

.tooltip .tooltiptext {
    visibility: hidden;
    width: 200px;
    background-color: #ffefde;
    color: black;
    font-weight: bold;
    font-size: 0.8em;
    text-align: left;
    border-radius: 6px;
    padding: 5px 5px 5px 5px;
    display: inline;
    /* Position the tooltip */
    position: absolute;
    z-index: 1;
}

.tooltip:hover .tooltiptext {
    visibility: visible;
}
");
	 mcl_Html::s(mcl_Html::SRC_JS, <<<JS
		dojo.addOnLoad(function() {
			if(dojo.byId('person_recognized')) { 
				inputUsid(dojo.byId('person_recognized'), 'person_recognized_name');
			}
			if(dojo.byId('edit_usid')) { 
				inputUsid(dojo.byId('edit_usid'), 'edit_usid_name');
			}
			
			$('[title]').qtip({ 
				position: {
					corner: {
						target: 'topMiddle',
						tooltip: 'bottomMiddle'
					}
				},
				show: {
					delay: 0
				},
				style: {
					tip:'bottomMiddle',
					width: 200,
					border: {
						width: 2
					}
				}
			});
			
			$(".phone").mask("999.999.9999", {placeholder:"#"});
			
		});
		var validate = function() {
		 			
            var radios = document.getElementsByName("isserious");
            var formValid = false;
            
            var i = 0;
            while (i < radios.length) {
            if (radios[i].checked) formValid = true;
            i++;        
            }
            
            if (dojo.byId("assist_solution") && !formValid){
				alert("Please enter YES or No in the Near Miss Log Details: Serious? field.");
				
				return false;
			}
			
			if(dojo.byId("category").value == "0" || dojo.byId("category").value == "") {
				alert("You must select a category. If you do not know what category this {$form} would fall under, please select \"Other\".");
				dojo.byId("category").focus();
				return false;
			}
			if(dojo.byId("org").value == "") {
				alert("You must select an organization. If you do not know what organization this {$form} would fall under, please select your organization.");
				dojo.byId("org").focus();
				return false;
			}
			if(dojo.byId("completed_by_plant").value == "") {
				alert("You must select the Reporting Employee Location to be able to report out how many {$form}es are reported by employee location. If your location is not available in the list, please select Other.");
				dojo.byId("completed_by_plant").focus();
				return false;
			}
			if(dojo.byId("plant").value == "") {
				alert("You must select the location where the {$form} occured. If the location is not available in the list, please select Other.");
				dojo.byId("plant").focus();
				return false;
			}
			if(dojo.byId("location_description").value == "") {
				alert("You must provide a detailed description of the location where the {$form} occured to help conduct an investigation if needed.");
				dojo.byId("location_description").focus();
				return false;
			}
			if(dojo.byId("date").value == "") {
				alert("You must specify the date the {$form} occured.");
				dojo.byId("date").focus();
				return false;
			}
			if(isNaN(dojo.byId("hour").value) || dojo.byId("hour").value == "") {
				alert("Invalid hour entered. Only numeric characters allowed and you must specify the time the {$form} occured.");
				dojo.byId("hour").focus();
				return false;
			}
			if(isNaN(dojo.byId("minute").value) || dojo.byId("minute").value == "") {
				alert("Invalid minute entered. Only numeric characters allowed and you must specify the time the {$form} occured.");
				dojo.byId("minute").focus();
				return false;
			}
			if(dojo.byId("happened_before").value.replace(/\s/g, '') == "") {
				alert("Enter what happened before the {$form}.");
				dojo.byId("happened_before").focus();
				return false;
			}
			if(dojo.byId("happened_during").value.replace(/\s/g, '') == "") {
				alert("Enter what happened during the {$form}.");
				dojo.byId("happened_during").focus();
				return false;
			}
			
			if(dojo.byId("happened_after").value.replace(/\s/g, '') == "") {
				alert("Enter what happened after the {$form}.");
				dojo.byId("happened_after").focus();
				return false;
			}
			
			if(dojo.byId("requires_investigation") && dojo.byId("requires_investigation").value.replace(/\s/g, '') == "") {
				alert("Please provide a response to whether this Near Miss requires an investigation.");
				dojo.byId("requires_investigation").focus();
				return false;
			}
			if(dojo.byId("assist_investigation") && dojo.byId("assist_investigation").value == "" && dojo.byId("requires_investigation").value == 1) {
				alert("Please provide a response to whether you would be willing to assist with the Near Miss investigation.");
				dojo.byId("assist_investigation").focus();
				return false;
			}
			if(dojo.byId("assist_solution") && dojo.byId("assist_solution").value == "" && dojo.byId("requires_investigation").value == 1) {
				alert("Please provide a response to whether you would be willing to assist with developing the solution for this Near Miss.");
				dojo.byId("assist_solution").focus();
				return false;
			}
			if(dojo.byId("immediate_investigation") && dojo.byId("immediate_investigation").value == "" && dojo.byId("requires_investigation").value == 1) {
				alert("Please provide a response to whether this is an immediate safety issue that needs to be address for this Near Miss.");
				dojo.byId("immediate_investigation").focus();
				return false;
			}
			if(dojo.byId("investigation_complete") && dojo.byId("investigation_complete").value.replace(/\s/g, '') == "" && dojo.byId("requires_investigation").value == 1) {
				alert("Please provide a response to whether this Near Miss investigation is complete.");
				dojo.byId("investigation_complete").focus();
				return false;
			}
			
			if(dojo.byId("closed") && dojo.byId("closed").value.replace(/\s/g, '') == "") {
				alert("Please provide a response to whether this Good Catch is closed.");
				dojo.byId("closed").focus();
				return false;
			}
			
			this.twinCallback = [];
			var validate = this;
			$('[id^=twin]').each(function(i) {
				if(this.value === 'YES') {
					var num = $(this).attr('data-num');
					if($('#' + num + 'ex').val() === '') {
						validate.twinCallback.push(num+'ex');
						return;
					}
				}
			});
			if(this.twinCallback.length > 0) {
				alert('A description of the precursor is required.');
				dojo.byId("twin").style.display = 'block';
				dojo.byId(this.twinCallback[0]).focus();
				
				return false;
			}
			sot.tools.cover(true);
			return true;
		};
		
		var disabled = false;
		var safetyMeasures = {};
		var checkSM = function(checked, sapNum) {
			if(checked) {
				if(safetyMeasures[sapNum]) { 
					dojo.byId('sm_ad_' + sapNum).value = safetyMeasures[sapNum];
				}
				dojo.byId('sm_ad_' + sapNum).style.backgroundColor = '#fff';
			} else {
				safetyMeasures[sapNum] = dojo.byId('sm_ad_' + sapNum).value; //store old value
				dojo.byId('sm_ad_' + sapNum).value = ''; //clear
				dojo.byId('sm_ad_' + sapNum).style.backgroundColor = '#ececec';
			}
			
			dojo.byId('sm_ad_' + sapNum).disabled = !checked;
		};
		var toggleTwin = function(t) {
			if(dojo.byId('twin').style.display == 'block') {
				dojo.byId('twin').style.display = 'none';
				t.innerHTML = '[ Expand ]';
			} else {
				dojo.byId('twin').style.display = 'block';
				t.innerHTML = '[ Collapse ]';
			}
		};
		
		var inputUsid = function(input, name_container){
			var usid = input.value.replace(/\s/g, '');
			var event = window.event;
			if(usid.length == 6 || event.keyCode == 13){
				sot.tools.ajax.submit("get", "json", {
					f: "getInfo",
					usid: usid
				}, function(_e){
					dojo.byId(name_container).innerHTML = _e.name;
					dojo.byId(name_container).style.color = 'black';
							
					return true;
				}, false, false);
			} else {
				dojo.byId(name_container).innerHTML = 'Name will be displayed after you enter a valid username';
				dojo.byId(name_container).style.color = 'red';
			}
			
			return false;
		};
		var switchFilter = function(val) {
			var def = {
				'NM': ['Near Miss', 'Unplanned events that do not cause injury, damage, or workplace illness, but could have. This definition would include issues, acts, and behaviors.'],
				'P': ['Precursor', 'One that procedes and indicates or announces someone or something to come. A hazard that if left in it\'s normal state could potentially cause a near miss or accident.'],
				'GC': ['Good Catch', 'A incident or hazard that could lead to a near miss if not prevented from reoccurring.']
			};
			
			dojo.byId('casedefinition').innerHTML = '<b>' + def[val][0] + '</b></br>' + def[val][1];
		};
	
		var expandCollapse = function(toggle) {
			var content = dojo.query(toggle.parentNode).next()[0]
			if(toggle.innerHTML == '[ Expand ]') {
				toggle.innerHTML = '[ Collapse ]';
				content.style.display = 'block';
			} else {
				toggle.innerHTML = '[ Expand ]';
				content.style.display = 'none';
			}
		};
		
		var current_cat;
		var change_cat = function(val) {
			if(current_cat && dojo.byId('sub_category_' + current_cat)) {
				dojo.byId('sub_category_' + current_cat).style.display = 'none';
			}
			
			if(dojo.byId('sub_category_' + val)) {
				dojo.byId('sub_category_' + val).style.display = 'block';
				dojo.byId('no_sub_cat').style.display = 'none';
			} else {
				dojo.byId('no_sub_cat').style.display = 'block';
			}
			current_cat = val;
		
		};
		
		var anonymous = function(toggle) {
			dojo.byId('completed_by').value = '';
			dojo.byId('completed_by_supervisor').value = '';
			dojo.byId('completed_by_title').value = '';
			dojo.byId('completed_by_phone').value = '';
			dojo.byId('completed_by_supervisor_phone').value = '';
			
			
			dojo.byId('first_level_leader_name').innerHTML = '';
			dojo.byId('director_name').innerHTML = '';
			dojo.byId('director').value = '';
			
			toggle.innerHTML = '[ Undo Anynomous ]';
			toggle.onclick = function() { undoanonymous(toggle); }
		};
		
		var undoanonymous = function(toggle) {
			dojo.byId('completed_by').value = '{$completed_by}';
			dojo.byId('completed_by_supervisor').value = '{$completed_by_supervisor}';
			dojo.byId('completed_by_title').value = '{$completed_by_title}';
			dojo.byId('completed_by_phone').value = '{$completed_by_phone}';
			dojo.byId('completed_by_supervisor_phone').value = '{$completed_by_supervisor_phone}';
			
			dojo.byId('first_level_leader_name').innerHTML = "{$manager["fname"]}";
			dojo.byId('director_name').innerHTML = "{$director["fname"]}";
			dojo.byId('director').value = "{$director["username"]}";
			
			toggle.innerHTML = '[ Anynomous ]';
			toggle.onclick = function() { anonymous(toggle); }
		};
		dojo.require('dojo.NodeList-traverse');
		
		var disable = function() {
			if (document.all || document.getElementById) {
				for (i = 0; i < document.forms[0].length; i++) {
					var formElement = document.forms[0].elements[i];
					
					if(formElement.type == 'select' || formElement.type == 'select-one' || formElement.type == 'checkbox'||formElement.type == 'radio') {
						formElement.disabled = true;
					}
					
					formElement.readonly = 'readonly';	
					formElement.onkeypress = function() { return false;};	
					
					resize_textbox(formElement);
				}
			}
			
			dojo.byId('buttons').style.display = 'none';
		};
		
		var resize_textbox = function(textField) {
			if (textField.clientHeight < textField.scrollHeight) {
				textField.style.height = textField.scrollHeight + "px";
				if (textField.clientHeight < textField.scrollHeight) {
					textField.style.height = (textField.scrollHeight * 2 - textField.clientHeight) + "px";
				}
			}
		};
		
		var toggle_yes_no_inv = function(val) {
			if(val == 1) {
				dojo.byId("assist_investigation_container").style.display = '';
				dojo.byId("assist_solution_container").style.display = '';
				dojo.byId("investigation_complete_container").style.display = '';
				dojo.byId("immediate_investigation_container").style.display = '';
			} else {
				dojo.byId("assist_investigation_container").style.display = 'none';
				dojo.byId("assist_solution_container").style.display = 'none';
				dojo.byId("investigation_complete_container").style.display = 'none';
				dojo.byId("immediate_investigation_container").style.display = 'none';
			}
		}
		
		
		/* DS-199
		var change_priority = function(val) {
			if(val == 1) {
				dojo.byId('immediate_investigation').value = 1;
			} else {
				dojo.byId('immediate_investigation').value = 0;
			}
		};
        */
JS
);

	if(isset($_GET["print"]) && $_GET["print"] == true) {
		mcl_Html::s(mcl_Html::SRC_JS, "
		window.onload = function() {
			window.print();
			window.close();
		}
	");

		echo <<<DIV
		<div id='print' style='width: 100%; height: 100px; font-size: 26px; text-align: center;'/>
			{$form}
		</div>
DIV;
	}
	
	$sql = "SELECT * FROM ORGANIZATIONS ORDER BY ORG_CODE";
	$orgs = array();
	$user_ = @mcl_Ldap::lookup($user["usid"]);
	$org_u = substr($user_["all"][0]["dtebusinessunitdeptidlongdesc"][0], 4);
	$orgs_a = array();
	$org = "0";

	while($row = $oci->fetch($sql)){
		$pos = strpos($row["ORG_DESCRIPTION"], trim($org_u));
		if ($pos === false) {
		} else {
			$org = $row["ORG_CODE"];
		}
		
		$orgs_a[$row["ORG_CODE"]] = array(
			"code"		=> $row["ORG_CODE"],
			"title"		=> $row["ORG_TITLE"]
		);	
	}
	if(isset($data["ORG_CODE"])){ $org = $data["ORG_CODE"]; }
	foreach($orgs_a as $key=>$value){
		$selected = ($org == $key ? 'selected=selected' : '');
		$org_s .= "<option value='{$key}' {$selected}>{$value["title"]}</option>";
	}
	
	$sql = "SELECT * FROM NEAR_MISS_ACCIDENT_LOCATIONS ORDER BY AL_LOCATION";
	$locations = "";
	
	while($row = $oci->fetch($sql)){
		$locations .= "<option value='{$row["AL_ID"]}' " . ($data["LOCATION"] == $row["AL_ID"] ? "selected=selected" : "") . ">{$row["AL_LOCATION"]}</option>";
	}

	$sql = "SELECT * FROM NEAR_MISS_PLANTS ORDER BY CASE WHEN PLANT_ID = 0 THEN 1 ELSE 0 END, PLANT";
	$plants = "";
	while($row = $oci->fetch($sql)){
		$plants .= "<option value='{$row["PLANT_ID"]}' " . ($data["PLANT"] == $row["PLANT_ID"] ? "selected=selected" : "") . ">{$row["PLANT"]}</option>";
		$completed_by_plants .= "<option value='{$row["PLANT_ID"]}' " . ($data["COMPLETED_BY_PLANT"] == $row["PLANT_ID"] ? "selected=selected" : "") . ">{$row["PLANT"]}</option>";
	}
	
	$sql = "SELECT * FROM NEAR_MISS_CATEGORIES WHERE " . str_replace(" ", "_", $form) . " = 1 OR NEAR_MISS = 1 ORDER BY PARENT_CAT_ID, CASE WHEN CAT_TEXT = 'Other' THEN 1 ELSE 0 END, CAT_TEXT";

	$categories = "";
	$sub_categories = "";
	$sub_categories_arr = "";
	while($row = $oci->fetch($sql)){
		if($row["PARENT_CAT_ID"] == "") {
			$categories .= "<option value='{$row["CAT_ID"]}' " . ($data["CATEGORY"] == $row["CAT_ID"] ? "selected=selected" : "") . ">{$row["CAT_TEXT"]}</option>";
		} else {
			$sub_categories_arr[$row["PARENT_CAT_ID"]][] = $row;
		}	
	}
	
	foreach($sub_categories_arr as $key=>$value) {
		$sub_categories .= "<select name='sub_category_{$key}' id='sub_category_{$key}' style='display: none;'>";
		$sub_categories .= "<option></option>";
		foreach($value as $sub_cat) {
			$sub_categories .= "<option value='{$sub_cat["CAT_ID"]}' " . ($data["SUB_CATEGORY"] == $sub_cat["CAT_ID"] ? "selected=selected" : "") . " >{$sub_cat["CAT_TEXT"]}</option>";
		}
		$sub_categories .= "</select>";
	}
?>
<div class='header' style='width: 800px; margin: 0 auto; margin-top: 5px;'>
	<?=$form?>
	<?
		if(!empty($_REQUEST["nm_id"]) || !empty($_REQUEST["gc_id"])) {
			echo (!empty($_REQUEST["nm_id"])) ? "<b>#".$_REQUEST["nm_id"] : "<b>#".$_REQUEST["gc_id"];
			echo "</b>";
		}
	?>
</div>
<?php
	$url = "nearmiss.php?sid=" . time();
	if(!empty($login_warning)){
		echo "<div class='form_warning'>
			{$login_warning	}	
		</div>";
	}

?>
<div id='container' style='margin-top: 0px; width: 800px; margin: 0 auto;'>
<form action = '../src/php/engine.php?f=save<?=($gc ? "GoodCatch" : "NearMiss")?>&<?=($gc ? "gc_id" : "nm_id")?>=<?=($gc ? $_REQUEST["gc_id"] : $_REQUEST["nm_id"]) . (!empty($_GET["delegate"]) ? "&delegate={$_GET["delegate"]}" : ""); ?><?= (isset($_GET['mobile']) ? "&mobile=true" : "") ?>' method='POST' onsubmit='return validate();' target='submit' enctype="multipart/form-data">
	<div class='header2' style='margi-top: 0px;'>Completed By Details
		<?php if(!$gc): ?>
		<span style='<?php echo ($edit ? "display: none;" : ""); ?>' class='right' onclick='anonymous(this);'>
			[ Anonymous ]
		</span>
		<?php endif; ?>
	</div>
	<div class='content'>
	<table>
		<tr>
			<td style='width:150px; text-align: right;'>Submitted By</td>
			<td style='width:50px;'><input type='text' onkeydown='return false;' id='completed_by' name='completed_by' value="<?php echo $completed_by; ?>" /></td>
			
			<td style='width:150px; text-align: right; padding-right: 5px;'>Phone Number</td>
			<td style=''>
				<input type='text' name='completed_by_phone' id='completed_by_phone' class='phone' value="<?php echo $completed_by_phone; ?>"/>
			</td>
		</tr>
		<tr>
			<td style='width:150px; text-align: right; padding-right: 5px;'>Title</td>
			<td style=''>
				<input type='text' onkeydown='return false;' style='font-size: 12px; width: 250px;' name='completed_by_title' id='completed_by_title' value="<?php echo $completed_by_title; ?>"/>
			</td>
		</tr>
		<tr>
			<td style='width:150px; text-align: right; padding-right: 5px;'>First Level Leader</td>
			<td style=''>
				<input type='text' onkeydown='return false;' name='completed_by_supervisor' id='completed_by_supervisor' value="<?php echo $completed_by_supervisor; ?>"/>
			</td>
			<td style='width:150px; text-align: right; padding-right: 5px;'>Phone Number</td>
			<td style=''>
				<input type='text' name='completed_by_supervisor_phone' id='completed_by_supervisor_phone' class='phone' value="<?php echo $completed_by_supervisor_phone;  ?>"/>
			</td>
		</tr>
	</table>
	</div>
	<div class='header2'><?=$form?> Log Details</div>
	<div class='content'>
	<table>
		<tr>
			<td style='width:150px; text-align: right; padding-right: 5px;'>Event Type</td>
			<td>
				<div style='font-size: 13px; background-color: #fff; padding: 3px; width: 140px; border: 1px solid #000;'><?=$type[$form]?></div>
			</td>
			<td rowspan='5' style='padding-left: 10px; vertical-align: top;'>
				<div style='border: 1px solid #000; width: 300px; height: 70px; padding: 5px; background-color: #ffefde;' id='casedefinition'>
					<b><?=$form?></b><br/>
					<?=$definition[$form]?>
				</div>
                <br>
                <?php if(!$gc): ?>
                <div  style='position: relative; z-index: 100; border: 1px solid #000; width: 300px; height: 110px; padding: 5px; background-color: #ffa366; '>
                    <b>Serious Near Miss:</b>
                    <div style='margin-left:3px;margin-right: 3px'>An unplanned event where given a slight shift in time or position, damage and/or injury easily could have occurred
                        and may have resulted in a <b>Serious Injury or Fatality (SIF)</b>.
                        <div style="display: inline-block;" class="tooltip"><img src="../src/img/about.png"/>

                            <span class="tooltiptext" style='padding-left:15px'>
                                SIF Criteria* is as follows:<ul style="padding-left:5px;">
                            <li>Fatalities</li>
                            <li>Bone amputations</li>
                            <li>Concussions</li>
                            <li>Injury to internal organs</li>
                            <li>Bone fractures</li>
                            <li>Tendon and ligament tears</li>
                            <li>Herniated disks</li>
                            <li>Internal stitches</li>
                            <li>3rd degree burns</li>
                            <li>Eye damage/loss of vision</li>
                            <li>Injections of foreign materials</li>
                            <li>Heat stroke</li>
                            <li>Joint dislocation</li></ul>
                            *Edison Electrical Institute definition as of Jan 2016
                            </span>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
			</td>
		</tr>
                <?php if(!$gc): ?>
        <!--<tr>
			<td style='width:150px; text-align: right; padding-right: 5px; vertical-align: top;'>
				Priority (H, M, L)
			</td>
			<td style=' vertical-align: top;'>
				<select name='priority' style='width: 100px;' onchange='change_priority(this.value)'>
					<option value='L' <//?=$priority == 'L' ? 'selected=selected' : ''?> style='background-color: yellow;'>Low</option>
					<option value='M' <//?=$priority == 'M' ? 'selected=selected' : ''?> style='background-color: orange;'>Medium</option>
					<option value='H' <//?=$priority == 'H' ? 'selected=selected' : ''?> style='background-color: red;'>High</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>
			</td>
			<td style='padding-top: 5px; padding-bottom: 5px;'>
				<div style='font-size: 11px;'>
					<div style='padding-bottom: 5px;'>
						<b style='background-color: red;'>&nbsp;High&nbsp;</b> - A hazardous condition where there is a substantial probability that death or serious physical harm could result from the condition.
					</div>
					<div style='padding-bottom: 5px;'>
						<b style='background-color: orange;'>&nbsp;Medium&nbsp;</b> - A condition where the most serious injury or illness that would be likely to result from a hazardous condition cannot reasonably be predicted to cause death or serious physical harm.
					</div>
					<div>
						<b style='background-color: yellow;'>&nbsp;Low&nbsp;</b> - A condition where there is very little to no employee exposure and the hazard would not be likely to cause a injury or illness.
					</div>
				</div>
			</td>
		</tr>-->
        <tr>
            <!--DS-169 -->
            <td style='width:150px; text-align: right; padding-right: 5px; vertical-align: top;'>
                * Serious?
            </td>
            <!--DS-170 -->
            <td style=' vertical-align: top;'>
                <input  type="radio" name="isserious" value='1'<?php echo ($data["ISSERIOUS"] == "1" ? "checked=checked" : "")?>>Yes
                <input type="radio" name="isserious" value='0'<?php echo ($data["ISSERIOUS"] == "0" ? "checked=checked" : "")?>>No<br>
            </td>
        <!--</tr>
            <tr>
                <td>
                </td>
                <td style='padding-top: 5px; padding-bottom: 5px;'>
                    <div style='font-size: 11px;'>
                        <div style='padding-bottom: 5px;'>
                            <b>&nbsp;YES&nbsp;</b> - Place Holder
                        </div>
                        <div style='padding-bottom: 5px;'>
                            <b>&nbsp;No&nbsp;</b> - Place Holder
                    </div>
                </td>
            </tr>-->
		<?php endif; ?>
		<tr>
			<td style='width:150px; text-align: right; padding-right: 5px;'>* Category</td>
			<td>
				<select name='category' id='category' <?=(!$gc ? "onchange='change_cat(this.value);'" : "")?> style='width: 300px;'>
					<option value='0'>- Select a Category -</option>
					<?php echo $categories; ?>
				</select>
			</td>
		</tr>
		<?php if(!$gc): ?>
		<tr style=''>
			<td style='width:150px; text-align: right; padding-right: 5px;'>Sub-Category</td>
			<td>
				<span id='no_sub_cat' style='padding: 1px; font-size: 11px;'>
					Sub-categories are populated by category selected<br/>
					<span style='font-size: 10px;'>(currently there are no sub-categories to choose from)</span>
				</span>
				<?php echo $sub_categories; ?>
			</td>
		</tr>
		<?php endif; ?>
		<?php if($gc): ?>
		<tr>
			<td style='width:150px; text-align: right; padding-right: 5px;'>HP Tools Used<br/><span style="font-size:10px; font-style:italic;">(Hold down ctrl key to select multiple)</span></td>
			<td>
				<select id='tools_used' name='tools_used[]' multiple>
		<?
				$tools_used_qs = array('3-Way Communication','Concurrent Verification','Flagging/Operational Barriers', 'Independent Verification', 'Peer Check', 'Phoenetic Alphabet','Placekeeping','Post job review','Pre-task brief','Procedure review and adherence','Questioning attitude','Self-checking','STAR (touch and verbalize)','Stop when unsure and seek out help','Turnover','Two minute rule','Validate assumptions','"Do not disturb" sign','Vendor oversight','Product review meeting');

				$tools_used_answers = json_decode($data["TOOLS_USED"], true);

				if($_GET["mode"] == "readonly") {
					foreach($tools_used_qs as $q) {
						echo (in_array($q, $tools_used_answers) ? "<option value= '{$q}' selected='selected' >{$q}</option>" : "");
					}	
				}
				else { //multiple selects don't show up if field disabled in IE, so only show rows selected
					foreach($tools_used_qs as $q) {
						echo "<option value= '{$q}' ".(in_array($q, $tools_used_answers) ? "selected='selected'" : "").">{$q}</option>";
					}				
				}
		
		?>

				</select>
			</td>
		</tr>
		<?php endif; ?>
		<tr>
			<td style='width:150px; text-align: right; padding-right: 5px;'>* <?=$form?> Date</td>
			<td>
				<input style='width: 100px;' type='text' id='date' maxlength='10' onkeydown='if(window.event.keyCode != 9) { return false; }' name='date' id='date' value="<?php echo $date; ?>"/>
				<img src='../src/img/calendar.gif' alt='' id="tcal" onmouseover="sot.swo.cal('tcal', 'date');" />
			</td>
		</tr>
		<tr>
			<td style='width:150px; text-align: right; padding-right: 5px;'>* <?=$form?> Time</td>
			<td>
				<input name='hour' id='hour' type='text' style='width: 25px;' value='<?php echo $hour ?>' maxlength='2'>:<input id='minute' name='minute' type='text' style='width: 30px;' value='<?php echo $minute; ?>' maxlength='2'>
				<select name='ampm' style='width: 50px; position: relative; top: 2px;'>
					<option <?php echo ($ampm == "AM" ? "selected=selected" : ""); ?>>AM</option>
					<option <?php echo ($ampm == "PM" ? "selected=selected" : ""); ?>>PM</option>
				</select>
			</td>
		</tr>
	</table>
	</div>
	<div class='header2' style='position: relative; z-index: 0;'><?=$form?> <?php echo ($gc) ? "" : " Event" ?> Details</div>
	<div class='content'>
		<table>
			<tr>
				<td style='width:225px; text-align: right; padding-right: 5px;'>* Organization</td>
				<td style=''>
					<select name='org' id='org' onchange=''>
						<option value=''></option>
						<?php echo $org_s; ?>
					</select> <span style='font-size: 10px; font-style: italic;'>(ie: Distribution Operations)</span>
				</td>	
			</tr>
			<tr>
				<td style='width:225px; text-align: right; padding-right: 5px;'>* Reporting Employee Location</td>
				<td style=''>
					<select name='completed_by_plant' style='width: 250px;' id='completed_by_plant'>
						<option value=''></option>
						<?php echo $completed_by_plants; ?>
					<select> <span style='font-size: 10px; font-style: italic;'>(ie: Ann Arbor Service Center)</span>
				</td>
			</tr>
			<tr>
				<td style='width:225px; text-align: right; padding-right: 5px;'>* Location Where <?=$form?> Occurred</td>
				<td style=''>
					<select name='plant' style='width: 250px;' id='plant'>
						<option value=''></option>
						<?php echo $plants; ?>
					<select> <span style='font-size: 10px; font-style: italic;'>(ie: Pontiac Service Center)</span>
				</td>	
			</tr>
			<tr>
				<td style='width:225px; text-align: right; padding-right: 5px;'>Location Description</td>
				<td>
					<select name='location' id='location'>
						<option value='0'></option>
						<?php echo $locations; ?>
					<select> <span style='font-size: 10px; font-style: italic;'>(ie: Parking Lot)</span>
				</td>	
			</tr>
			<tr>
				<td style='width:225px; text-align: right; padding-right: 5px;'>* Detailed Description of Location</td>
				<td style=''><input name='location_description' id='location_description' type='text' style='width: 300px' value='<?php echo $data["LOCATION_DESCRIPTION"] ?>'/> <span style='font-size: 10px; font-style: italic;'>(ie: Address / Cross Streets)</span></td>	
			</tr>
			<?php
				if($gc):
			?>
			<tr>
				<td style='width:225px; text-align: right; padding-right: 5px;'>
					Person Recognized <br/>for Identifying the Good Catch
				</td>
				<td style='vertical-align: top;'>
					<input name='person_recognized' id='person_recognized' type='text' style='width: 150px' maxlength='6' value='<?php echo $data["PERSON_RECOGNIZED"] ?>' onkeyup='inputUsid(this, "person_recognized_name");'/>
					<span style='font-size: 10px; font-style: italic;'>
						(username ie: u12345)<br/>
						<span id='person_recognized_name'>Name will be displayed after you enter a valid username</span>
					</span>
				</td>	
			</tr>
			<?php
				endif;
			?>
			<tr>
				<td style='width:225px; text-align: right; padding-right: 5px;'>* What Happened <br/><b>Before</b> the <?=$form?>?</td>
				<td style=''><textarea name='happened_before' id='happened_before' style='width: 400px; height: 60px;'><?php echo $data["HAPPENED_BEFORE"] ?></textarea></td>	
			</tr>
			<tr>
				<td style='width:225px; text-align: right; padding-right: 5px;'>* What Happened <br/><b>During</b> the <?=$form?>?
				<?=($gc ? "<br/><span style='font-size: 10px; color: #319aff;'>Describe the situation (hazard, issue, or error likely situation), and how it was discovered. Note any HP tools used.</span>" : "")?>
				</td>
				<td style=';'><textarea name='happened_during' id='happened_during' style='width: 400px; height: 60px;'><?php echo $data["HAPPENED_DURING"] ?></textarea></td>	
			</tr>
			<tr>
				<td style='width:225px; text-align: right; padding-right: 5px;'>
					* What Happened <br/><b>After</b> the <?=$form?>?
					<?=($gc ? "<br/><span style='font-size: 10px; color: #319aff;'>Specifically, what was done to resolve this issue? Note what was done to eliminate this error likely situation and improve defenses.</span>" : "")?>
				</td>
				<td style=''><textarea name='happened_after' id='happened_after' style='width: 400px; height: 60px;'><?php echo $data["HAPPENED_AFTER"] ?></textarea></td>	
			</tr>
		</table>
	</div>
	<?php if(!$gc): ?>
	<div class='header2'>
		Twin Analysis Precursors (check all that apply) Note: items in <i>italics</i> are more prevalent
		<span class='right' onclick='expandCollapse(this);'>[ Expand ]</span>
	</div>
	<div class='content' style='border: 1px solid transparent; padding: 0px; display: none;' id='twin'>
	<?php
		$sql = "
		SELECT  * 
		FROM    HP_ITEMS 
		WHERE   ITEM_CATEGORY 
				IN ('Task Demands', 'Individual Capabilities', 'Work Environment', 'Human Nature')
				ORDER BY ITEM_CATEGORY, ITEM_NUM
		";
		
		$prevCategory = '';
		$section = 0;
		echo "<table style=''>";
			echo "<tr>";
				echo "<td style='vertical-align: top; padding-top: 0px;'>";
		while($row = $oci->fetch($sql)) {
			if($prevCategory != $row["ITEM_CATEGORY"]) {
				
				if($section % 2 == 0) {
					echo "</tr>";
					echo "<tr>";
					echo "<td style='vertical-align: top; padding-top: 0px;'>";
				} else if($section > 0) {
					echo "</td>";
					echo "<td style='vertical-align: top; padding-top: 0px;'>";
				}
				echo "<div class='header2' style='background-color: #d1d1d1;'>{$row["ITEM_CATEGORY"]}</div>";
				$section++;
			}
			//the header will extend past right boundary but can't get rid of space of left, so use different widths to give consistent look
			$width = ($section % 2 == 0 ? 397 : 398);
			echo "<div style='width: {$width}px;'>";
			echo "<input " . ($twin[$row["ITEM_NUM"]]["ANSWER"] == "YES" ? "checked=checked" : "") . " type='checkbox' onclick='dojo.byId(\"twin_{$row["ITEM_NUM"]}\").value = this.checked ? \"YES\" :\"NO\"; dojo.byId(\"ex{$row["ITEM_NUM"]}\").style.display = this.checked ? \"block\" : \"none\";'>
			<span " . (!empty($row["HINT"]) ? "title='{$row["HINT"]}'" : "") . ">{$row["ITEM_TEXT"]}</span>";
			
			echo "<div id='ex{$row['ITEM_NUM']}' ". ($twin[$row["ITEM_NUM"]]["ANSWER"] == "YES" ? "" : "style='display:none'") .">
				<span style='color: #a3a5a3; font-size: 10px;'>*Please Describe:<br/></span>
				<textarea id='{$row["ITEM_NUM"]}ex' name='twin[{$row["ITEM_NUM"]}][explanation]' style='width: 393px; height: 30px;'>" . $twin[$row['ITEM_NUM']]['EXPLANATION'] . "</textarea>
			</div>";
			echo "<input type='hidden' name='twin[{$row["ITEM_NUM"]}][answer]' value='" . ($twin[$row["ITEM_NUM"]]["ANSWER"] == "YES" ? "YES" : "NO") . "' id='twin_{$row["ITEM_NUM"]}' data-num='{$row["ITEM_NUM"]}'>";
			
			echo"</div>";
			
			$prevCategory = $row["ITEM_CATEGORY"];
		}
				echo "</td>";
			echo "</tr>";
		echo "</table>";
	?>
	</div>
	<div class='header2'>Investigation Results <span class='right' onclick='expandCollapse(this);'>[ Expand ]</span></div>
	<div class='content' style='padding: 0px; display: none;'>
		<textarea name='investigation' style='width: 796px;' ><?php echo $data["INVESTIGATION"] ?></textarea>
	</div>
	<div class='header2'>
		Safety Measures 
		<!--<span class='right' style='right: 80px;' onclick='addMeasure();'>[ Add Safety Measure ]</span>-->
		<span class='right' onclick='expandCollapse(this);'>[ Expand ]</span>
	</div>
	<div class='content' style='display: none;'>
		<table style=''>
			<tr>
				<td><div class='header1' style='padding: 3px; background-color: #d1d1d1; border: 1px solid #000;'>&nbsp;</div></td>
				<td style='width: 500px;'><div style='padding: 3px; background-color: #d1d1d1; border: 1px solid #000;'>Safety Measure</div></td>
				<td style='width: 300px;'><div style='padding: 3px; background-color: #d1d1d1; border: 1px solid #000;'>Additional Description</div></td>
			</tr>
			<?php
			$sql = "SELECT * FROM NEAR_MISS_SAFETY_MEASURES ORDER BY SAFETY_MEASURE";
			$safety_measures = "";
			while($row = $oci->fetch($sql)){
				//$safety_measures .= "<option value='{$row["SAP_NUMBER"]}'>{$row["SAP_NUMBER"]} - {$row["SAFETY_MEASURE"]}</option>";
				echo "<tr>";
					echo "<td>";
					echo "<input " . ($sm[$row["SAP_NUMBER"]]["ANSWER"] == "YES" ? "checked=checked" : "") . "' type='checkbox' style='background-color: transparent;' onclick='checkSM(this.checked, {$row["SAP_NUMBER"]}); dojo.byId(\"sm_{$row["SAP_NUMBER"]}\").value = this.checked ? \"YES\" :\"NO\";' >";
					echo "<input value='" . ($sm[$row["SAP_NUMBER"]]["ANSWER"] == "YES" ? "YES" : "NO") . "' type='hidden' name='sm[{$row["SAP_NUMBER"]}]' id='sm_{$row["SAP_NUMBER"]}' value='NO' />";
					echo "</td>";
					echo "<td style='width: 500px;'>{$row["SAFETY_MEASURE"]}</td>";
					echo "<td style='width: 300px;'>";
						echo "<textarea name='sm_ad_{$row["SAP_NUMBER"]}' id='sm_ad_{$row["SAP_NUMBER"]}' style='height: 30px; width: 295px; " . ($sm[$row["SAP_NUMBER"]]["ANSWER"] == "YES" ? "" : "background-color: #ececec;") . "' " . ($sm[$row["SAP_NUMBER"]]["ANSWER"] == "YES" ? "" : "disabled=disabled") . " value='" . ($sm[$row["SAP_NUMBER"]] == "YES" ? "" : "disabled=disabled") . "'>{$sm[$row["SAP_NUMBER"]]["ADDITIONAL_DESCRIPTION"]}</textarea>";
					echo "</td>";
				echo "</tr>";
			}
			?>
		</table>
	</div>
	
	<div class='header2'>First Level Leader Comments <span style='font-size: 10px; font-style: italic;' id='first_level_leader_name'><?php echo $manager["fname"] ?></span> <span class='right' onclick='expandCollapse(this);'>[ Expand ]</span></div>
	<div class='content' style='padding: 0px; display: none;'>
		<textarea name='supervisor_comments' style='width: 796px;' ><?php echo $data["SUPERVISOR_COMMENTS"] ?></textarea>
	</div>
	<div class='header2'>Director Comments <span style='font-size: 10px; font-style: italic;' id='director_name'><?php echo $director["fname"]; ?></span> <span class='right' onclick='expandCollapse(this);'>[ Expand ]</span></div>
	<div class='content' style='padding: 0px; display: none;'>
		<textarea name='director_comments' style='width: 796px;' ><?php echo $data["DIRECTOR_COMMENTS"] ?></textarea>
	</div>
	<?php endif; ?>
	<div class='header2'>Photos <span class='right' onclick='addPhoto();'>[ Add Photo ]</span></div>
	<div class='content' id='photos' style='position: relative; overflow: hidden; padding: 0px;'>
	<?php
		require_once("/opt/apache/servers/soteria/htdocs/src/php/photos.php");
		$x = getPhotos($gc ? 'GC' : 'NM', $gc ? $_REQUEST["gc_id"] : $_REQUEST["nm_id"], $oci, true);
		echo "<input type='hidden' name='p_count' id='p_count' value='{$x}'/>";
	?>
	</div>
	<table>
		<?php if(!$gc): ?>
			<tr>
				<td style='padding-top: 5px; width: 380px;'>
					Investigator / Edit Role 
					<span style='font-size: 10px; display: block;'>Person responsible for updating this Near Miss; enter username ie. u12345</span>
				</td>
				<td>
					<input type='text' name='edit_usid' id='edit_usid' value='<?=$data['EDIT_USID']?>' maxlength='6' onkeyup='inputUsid(this, "edit_usid_name");'/>
					<span style='font-size: 10px; font-style: italic;'>
						(username ie: u12345)<br/>
						<span id='edit_usid_name'>Name will be displayed after you enter a valid username</span>
					</span>
				</td>
			</tr>
		<?php endif; ?>
		<?php
		if(($edit && $_GET["mode"] != "readonly")):
		?>
			<tr>
				<td style='padding-top: 5px; width: 380px;'>* Would you like worflow to be triggered when saving this <?=$form?>?</td>
				<td>
					<select style='width: 60px; font-size: 12px;' name='workflow'>
						<option value='1'>Yes</option>
						<option value='0' selected=selected>No</option>
					</select>
				</td>
				<td style='width: 350px; vertical-align: top;' rowspan='5'>				
					<span style='font-size: 10px; font-style: italic;'>
						Worflow includes the Completed By Person and their leadership up to the Director level.
					</span>
				</td>
			</tr>
		<?php
		endif;
		if(!$gc):
		?>
		<tr>
			<td style='padding-top: 5px; width: 380px;'>* Is an investigation for this Near Miss required?</td>
			<td>
				<select style='width: 60px; font-size: 12px;' id='requires_investigation' name='requires_investigation' onchange='toggle_yes_no_inv(this.value);'>
					<option></option>
					<option value='1' <?php echo ($data["REQUIRES_INVESTIGATION"] == "1" ? "selected=selected" : "") ?> >Yes</option>
					<option value='0' <?php echo ($data["REQUIRES_INVESTIGATION"] == "0" ? "selected=selected" : "") ?> >No</option>
				</select>
			</td>
		</tr>
		<tr id='assist_investigation_container'>
			<td style='padding-top: 5px; width: 380px;'>* Would you be willing to assist with the Near Miss Investigation?</td>
			<td>
				<select style='width: 60px; font-size: 12px;' id='assist_investigation' name='assist_investigation'>
					<option></option>
					<option value='1' <?php echo ($data["ASSIST_INVESTIGATION"] == "1" ? "selected=selected" : "") ?> >Yes</option>
					<option value='0' <?php echo ($data["ASSIST_INVESTIGATION"] == "0" ? "selected=selected" : "") ?> >No</option>
				</select>
			</td>
		</tr>
		<tr id='assist_solution_container'>
			<td style='padding-top: 5px; width: 380px;'>* Would you be willing to assist with developing the solution?</td>
			<td>
				<select style='width: 60px; font-size: 12px;' id='assist_solution' name='assist_solution'>
					<option></option>
					<option value='1' <?php echo ($data["ASSIST_SOLUTION"] == "1" ? "selected=selected" : "") ?> >Yes</option>
					<option value='0' <?php echo ($data["ASSIST_SOLUTION"] == "0" ? "selected=selected" : "") ?> >No</option>
				</select>
			</td>
		</tr>
		<tr id='immediate_investigation_container'>
			<td style='padding-top: 5px; width: 380px; color: red;'>* Is this an immediate safety issue that needs to be addressed?</td>
			<td>
				<select style='width: 60px; font-size: 12px;' id='immediate_investigation' name='immediate_investigation'>
					<option></option>
					<option value='1' <?php echo ($data["IMMEDIATE_INVESTIGATION"] == "1" ? "selected=selected" : "") ?> >Yes</option>
					<option value='0' <?php echo ($data["IMMEDIATE_INVESTIGATION"] == "0" ? "selected=selected" : "") ?> >No</option>
				</select>
			</td>
		</tr>
		<tr id='investigation_complete_container'>
			<td style='padding-top: 5px; width: 380px;'>* Has the investigation been completed?</td>
			<td>
				<select style='width: 60px; font-size: 12px;' id='investigation_complete' name='investigation_complete'>
					<option></option>
					<option value='1' <?php echo ($data["INVESTIGATION_COMPLETE"] == "1" ? "selected=selected" : "") ?> >Yes</option>
					<option value='0' <?php echo ($data["INVESTIGATION_COMPLETE"] == "0" ? "selected=selected" : "") ?> >No</option>
				</select>
			</td>
		</tr>
		<?php else: ?>
		<tr>
			<td style='padding-top: 5px; width: 200px;'>* Is the Good Catch Closed?</td>
			<td>
				<select style='width: 60px; font-size: 12px;' id='closed' name='closed'>
					<option></option>
					<option value='1' <?php echo ($data["CLOSED"] == "1" ? "selected=selected" : "") ?> >Yes</option>
					<option value='0' <?php echo ($data["CLOSED"] == "0" ? "selected=selected" : "") ?> >No</option>
				</select>
			</td>
		</tr>
		<?php endif; ?>
	</table>
	<center style='margin-top:10px; margin-bottom: 10px;' id='buttons'>
		<input type = 'hidden' value = '<?php echo $_GET["delegate"]; ?>' name = 'delegator'/>
		<input type = 'hidden' name='director' id='director' value='<?php echo $director["username"] ?>'/>
		<?php $returnto = "index"; if(!empty($_GET["returnto"])) { $returnto = $_GET["returnto"]; } ?>
		<?php if($_GET["mobile"] == "true") { $returnto = "mobile/index"; } ?>
		<input type='hidden' name='returnto' value='<?php echo $returnto; ?>' />
		<input type = 'submit' id='submit' value='Submit'>
		<button style='margin-left: 5px;' onclick='window.location="../<?php echo $returnto ?>.php<?php echo (!empty($_GET["delegate"]) ? "?delegate={$_GET["delegate"]}" : ""); ?>"; return false;'>Cancel & Go Back</button>
	</center>
</div>
</form>
<iframe name='submit' style='width:0px; height:0px;'></iframe>