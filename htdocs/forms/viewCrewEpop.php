<?php
	function errorHandler($errno, $errstr, $errfile, $errline, $errcontext){
		$test = 5;
	}
	set_error_handler('errorHandler');
	$baseDir = "https://" . $_SERVER['SERVER_NAME'] . ":" . $_SERVER['SERVER_PORT'];

	require_once('mcl_Html.php');
	require_once('mcl_Oci.php');
	require_once('../src/php/auth.php');
	
	mcl_Html::s(mcl_Html::INC_CSS, "../src/css/form.css");
	mcl_Html::title('View Enterprise Performance Observation');
	$epopId = $_GET["epopid"];
	

	if(empty($epopId)){
		die('An observation ID was not found!');
	}
	

	$sql = "SELECT 	EPOP_ID,
					LOWER(SUPERVISOR) AS SUPERVISOR_ID,
					LEADER,
					CREW,
					TO_CHAR(DATE_VISITED, 'MM/DD/YYYY') AS DATE_VISITED,
					TIME_VISITED,
					WORK_LOCATION,
					OHUG,
					COMMENTS,
					SC AS SERVICE_CENTER,
					NOTES,
					CONVERSATION_TOOK_PLACE,
					RECOGNIZED_GOOD_BEHAVIOR
			FROM	EPOP_CREW_OBSERVATIONS
			WHERE	EPOP_ID = {$epopId}
		";
	
	$oci = new mcl_Oci("soteria");
	$row = $oci->fetch($sql);

	$conversation = $row["CONVERSATION_TOOK_PLACE"];
	$behavior = $row["RECOGNIZED_GOOD_BEHAVIOR"];

	$comments = $row["COMMENTS"];
	$supervisor = @mcl_Ldap::lookup($row["SUPERVISOR_ID"]);

?>
<div style='text-align; right; margin-top: 5px; width: 800px;' onclick='window.print(); return false;'>
	<div style='text-align:right;'>
		<img src='../src/img/print.png'/> <a href='#' onclick='return false;' style=''>Print </a>
	</div>
</div>
<div class='header' style='width: 800px;'>
	Enterprise Performance Observation
</div>
<table border = "0" style = "width: 800px; font-weight: normal;">
	<tr>
		<td style = "font-weight: bold; text-align: left;">
			Supervisor
		</td>
		<td style = "border-bottom: 1px solid black; text-align: left;">
			<?php echo $supervisor["fname"]; ?>
		</td>
		<td style = "font-weight: bold; padding-left: 10px; text-align: left;">
			Service Center
		</td>
		<td style = "border-bottom: 1px solid black; text-align: left;">
			<?php echo $row["SERVICE_CENTER"]; ?>
		</td>
	</tr>
	<tr>
		<td style = "font-weight: bold; text-align: left;">
			Crew Num
		</td>
		<td style = "border-bottom: 1px solid black; text-align: left;">
			<?php echo $row["CREW"]; ?>
		</td>
		<td style = "font-weight: bold;  padding-left: 10px; text-align: left;">
			Leader
		</td>
		<td style = "border-bottom: 1px solid black; text-align: left;">
			<?php echo $row["LEADER"]; ?>
		</td>
	</tr>
	<tr>
		<td style = "font-weight: bold; text-align: left;">
			Date
		</td>
		<td style = "border-bottom: 1px solid black; text-align: left;">
			<?php echo $row["DATE_VISITED"]; ?>
		</td>
		<td style = "font-weight: bold;  padding-left: 10px; text-align: left;">
			Time Visited
		</td>
		<td style = "border-bottom: 1px solid black; text-align: left;">
			<?php echo $row["TIME_VISITED"]; ?>
		</td>
	</tr>
	</tr>
		<tr>
		<td style = "font-weight: bold; text-align: left;">
			OHUG
		</td>
		<td style = "border-bottom: 1px solid black;text-align: left;">
			<?php echo $row["OHUG"]; ?>
		</td>
		<td style = "font-weight: bold;  padding-left: 10px; text-align: left;">
			Work Location
		</td>
		<td style = "border-bottom: 1px solid black; text-align: left;">
			<?php echo $row["WORK_LOCATION"]; ?>
		</td>
	</tr>
	<tr>
		<td style = "font-weight: bold; text-align: left;">
			Notes
		</td>
		<td style = "border-bottom: 1px solid black; text-align: left;" colspan = "3">
			<?php echo str_replace("//", "", $row["NOTES"]); ?>
		</td>
	</tr>
</table>
<table>
<?php
	$sql = "
		WITH T AS(
			SELECT 	I.ITEM_NUM,
					I.ITEM_TEXT,
					I.ITEM_CATEGORY,
					HAS_SUBITEMS,
					ANSWER
			FROM	EPOP_ANSWERS A,
					 EPOP_ITEMS I
			WHERE	EPOP_ID = {$epopId}
					AND A.ITEM_NUM = I.ITEM_NUM
		)
		SELECT 	T.*, 
				C.COMMENTS
		FROM T
		LEFT JOIN EPOP_COMMENTS C
			ON C.EPOP_ID = {$epopId}
			AND C.ITEM_NUM = T.ITEM_NUM
		ORDER BY ITEM_CATEGORY DESC, 
				T.ITEM_NUM
	";
	
	$prev_category = '';
	$firstpass = true;
	
//	$stmt_sub = $oci->parse("
//		SELECT 	I.ITEM_NUM,
//					I.ITEM_TEXT,
//					ANSWER
//			FROM	EPOP_ANSWERS A,
//					 EPOP_ITEMS I,
//					 EPOP_SUBITEMS SI
//			WHERE	EPOP_ID = {$epopId}
//					AND A.ITEM_NUM = I.ITEM_NUM
//					AND SI.SUBITEM_NUM = A.ITEM_NUM
//					AND SI.PARENT_ITEM_NUM = :parent_item_num
//	");
	$stmt_sub = $oci->parse("
			SELECT 	SI.SUBITEM_NUM,
					SI.SUBITEM_TEXT,
					ANSWER
			FROM	EPOP_ANSWERS A,
					SWO_SUBITEMS SI
			WHERE	EPOP_ID = {$epopId}
					AND A.ITEM_NUM = SI.SUBITEM_NUM
					AND SI.PARENT_ITEM_NUM = :parent_item_num
		");
	while($row = $oci->fetch($sql)){
		if($row["ITEM_CATEGORY"] != $prev_category){
			$x = 0;

			if(!$firstpass){
				echo "<tr style='height:30px;'>
					<td colspan=4></td>
				</tr>";
			}
			echo "<tr style='font-weight:bold; background-color:#f0f0f0;'>
					<td style='width:710px; text-align:center; padding: 5px; font-weight: normal; border-top: 1px solid #000; border-bottom: 1px solid #000;'>{$row["ITEM_CATEGORY"]}</td>
					<td style='width:30px; text-align:center; font-weight: normal; border-top: 1px solid #000; border-bottom: 1px solid #000;'>S</td>
					<td style='width:30px; text-align:center; font-weight: normal; border-top: 1px solid #000; border-bottom: 1px solid #000;'>IO</td>
					<td style='width:30px; text-align:center; font-weight: normal; border-top: 1px solid #000; border-bottom: 1px solid #000;'>NA</td>
				</tr>";
			$firstpass = false;
		}
		
		echo "<tr class='" . ($x++ % 2 == 0 ? 'even' : 'odd') . "'>
					<td style='width:710px; text-align:left; padding-top: 3px; padding-bottom: 3px;'>
						{$row["ITEM_TEXT"]}
						" . (!empty($row["COMMENTS"]) ? "</br><i>{$row["COMMENTS"]}</i>" : "") ."
					</td>
					<td style='width:30px; text-align:center; vertical-align:top; border-left: 1px solid #dedede;'>" . ($row["ANSWER"] == "S" ? "<img style='padding: 3px;' src='../src/img/check.gif'/>" : ""). "</td>
					<td style='width:30px; text-align:center; vertical-align:top; border-left: 1px solid #dedede;'>" . ($row["ANSWER"] == "IO" ? "<img style='padding: 3px;' src='../src/img/check.gif'/>" : ""). "</td>
					<td style='width:30px; text-align:center; vertical-align:top; border-left: 1px solid #dedede;'>" . ($row["ANSWER"] == "NA" ? "<img style='padding: 3px;' src='../src/img/check.gif'/>" : ""). "</td>
				</tr>
			";
			
		$prev_category = $row["ITEM_CATEGORY"];
		
		if($row["HAS_SUBITEMS"] == 1){
			$oci->bind($stmt_sub, array(
				":parent_item_num" => $row["ITEM_NUM"]
			));
			while($row2 = $oci->fetch($stmt_sub, false)){
				echo "<tr class='" . ($x++ % 2 == 0 ? 'even' : 'odd') . "'>
						<td style='width:690px; text-align:left; padding-top: 3px; padding-bottom: 3px;  padding-left: 20px;'>
							<div>&bull; {$row2["SUBITEM_TEXT"]}</div>
						</td>
						<td style='width:30px; text-align:center; vertical-align:top; border-left: 1px solid #dedede;'>" . ($row2["ANSWER"] == "S" ? "<img style='padding: 3px;' src='../src/img/check.gif'/>" : ""). "</td>
						<td style='width:30px; text-align:center; vertical-align:top; border-left: 1px solid #dedede;'>" . ($row2["ANSWER"] == "IO" ? "<img style='padding: 3px;' src='../src/img/check.gif'/>" : ""). "</td>
						<td style='width:30px; text-align:center; vertical-align:top; border-left: 1px solid #dedede;'>" . ($row2["ANSWER"] == "NA" ? "<img style='padding: 3px;' src='../src/img/check.gif'/>" : ""). "</td>
					</tr>
				";

			}
		}	
	}
?>
	<table style='width: 815px;'>
		<tr>
			<td style='font-weight:bold;vertical-align: bottom; text-align: left;'>
				Did a conversation take place with the observed individual?
			</td>
		</tr>
		<tr>
			<td style= 'text-align: left;'>
				<?=$conversation ? "Yes" : "No"?>
			</td>
		</tr>
		<tr>
			<td style='font-weight:bold;vertical-align: bottom; text-align: left;'>
				Was good safety behavior recognized during this observation?
			</td>
		</tr>
		<tr>
			<td style='text-align: left;'>
				<?=$behavior ? "Yes" : "No"?>
			</td>
		</tr>
	</table>
<table>
	<tr>
		<td style='font-weight:bold;vertical-align: bottom; text-align: left;'>Additional Comments:</td>
	</tr>
	<tr>
		<td><textarea style='width:800px;' name='comments' onkeypress='return false;'><?php echo str_replace("\\", "", $comments); ?></textarea></td>
	</tr>
</table>