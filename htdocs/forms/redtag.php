<?php
	function errorHandler($errno, $errstr, $errfile, $errline, $errcontext){
		
	}
	set_error_handler('errorHandler');
	$baseDir = "https://" . $_SERVER['SERVER_NAME'] . ":" . $_SERVER['SERVER_PORT'];

	require_once('mcl_Html.php');
	require_once('mcl_Oci.php');
	require_once("/opt/apache/servers/soteria/htdocs/src/php/auth.php");
	$user = auth::check();
	if($user["status"] != "authorized"){
		header("Location: ../index.php");
	}
	
	mcl_Html::title("SOTeria - Red Tag Audits");
	
	
	mcl_Html::js(mcl_Html::DOJO);
	mcl_Html::js(mcl_Html::DOJO_WINDOW);
	mcl_Html::js(mcl_Html::AJAX);
	mcl_Html::js(mcl_Html::CALENDAR);
	mcl_Html::s(mcl_Html::INC_JS, "../src/js/tools/cover.js");
	mcl_Html::s(mcl_Html::INC_JS, "../src/js/tools/ajax.js");
	mcl_Html::s(mcl_Html::INC_JS, "../src/js/redtag.js");
	
	if($_GET["print"] == true) {
	mcl_Html::s(mcl_Html::SRC_JS, "
	window.onload = function() {
		window.print();
		window.close();
	}
");
		echo <<<DIV
	<div id='print' style='width: 100%; height: 100px; font-size: 26px; text-align: center;'/>
		Red Tag Audit Form
	</div>
DIV;
	}
	
	mcl_Html::s(mcl_Html::INC_CSS, "../src/css/form.css");
	mcl_Html::s(mcl_Html::INC_CSS, "../src/css/jscal2.css");
	
	mcl_Html::s(mcl_Html::SRC_CSS, "
		input[type=text] {
			border: 	none;
		}
		textarea {
			width: 		775px;
		}
		
		td {
			text-align: left;
		}
	");
	$oci = new mcl_Oci("soteria");
	$locations = "";
	if(!empty($_GET["id"])){
		
		$id = $_GET["id"];
		$sql = "SELECT R.*, TO_CHAR(R.AUDIT_DATE, 'MM/DD/YYYY') AS AUDIT_DATE  FROM RED_TAG_AUDITS R WHERE RTA_ID = {$id}";
	
		$data = $oci->fetch($sql);
		if($data["COMPLETE"] == 1){
			die("This audit has been completed and submitted. <a href='viewRedtag.php?id={$id}'>Click here</a> to view the audit.");
		}
		
		$sql = "SELECT * FROM RED_TAG_ANSWERS WHERE RTA_ID = {$id}";
		
		$data_answers = array();
		while($row = $oci->fetch($sql)){
			$data_answers[$row["ITEM_NUM"]] = $row["ANSWER"];
		}
		
		$sql = "SELECT RTA_ID, ATTACHMENT_ID, NAME AS A_NAME, EXTENSION FROM RED_TAG_ATTACHMENTS 
			WHERE RTA_ID = {$id}";
		
		$attachments = array();
		while($row = $oci->fetch($sql)){
			$attachments[] = $row;
		}
		
		if(!empty($data["ORG"])){
			$sql = "
				SELECT LOCATION FROM RED_TAG_AUDITS_REQ 
					WHERE ORG = '{$data["ORG"]}'
					ORDER BY LOCATION
			";
			
			while($row = $oci->fetch($sql)){
				$selected = ($row["LOCATION"] == $data["LOCATION"] ? "selected=selected" : "");
				$locations .= "<option value='{$row["LOCATION"]}' {$selected}>{$row["LOCATION"]}</option>";
			}
			
		}
	}
	
	/*
	$orgs = '';
	$sql = "SELECT DISTINCT ORG FROM RED_TAG_AUDITS_LOCATIONS";
	while($row = $oci->fetch($sql)) {
		$orgs .= "<option values='{$row['ORG']}'  " . ($data["ORG"] == $row['ORG'] ? "selected=selected" : "") . "  >{$row['ORG']}</option>";
	}
	*/
	
	
?>

<form action = '../src/php/engine.php?f=saveRedtag<?php echo (!empty($id) ? "&id={$id}" : ""); echo (!empty($_GET["delegate"]) ? "&delegate={$_GET["delegate"]}" : ""); ?>' method = 'POST' onsubmit = '' target= 'submit' enctype="multipart/form-data">
	<div class='header'>
		Red Tag Audit Checklist
	</div>
	<div class = 'container'>
		<table>
			<tr>
				<td style = 'text-align: left; width: 100px; height: 20px; border-bottom: 1px solid black; font-weight: bold;' >
					Org. Audited &nbsp;
				</td>
				<td style = 'text-align: left; border-bottom: 1px solid black;'>
					<div class = 'container'>
						<select name = 'org' id='org' class = 'select' onchange='sot.redtag.switchOrg();'>
							<option></option>
							<option value = 'Engineering' <?php echo ($data["ORG"] == "Engineering" ? "selected=selected" : "")?>>Engineering</option>
							<option value = 'Fuel Supply' <?php echo ($data["ORG"] == "Fuel Supply" ? "selected=selected" : "")?>>Fuel Supply</option>
							<option value = 'OH Lines' <?php echo ($data["ORG"] == "OH Lines" ? "selected=selected" : "")?>>OH Lines</option>
							<option value = 'Nuclear Generation' <?php echo ($data["ORG"] == "Nuclear Generation" ? "selected=selected" : "")?>>Nuclear Generation</option>
							<option value = 'Production' <?php echo ($data["ORG"] == "Production" ? "selected=selected" : "")?>>Production</option>
							<option value = 'Substation' <?php echo ($data["ORG"] == "Substation" ? "selected=selected" : "")?>>Substation</option>
							<option value = 'System Operations' <?php echo ($data["ORG"] == "System Operations" ? "selected=selected" : "")?>>System Operations</option>
							<option value = 'UG Lines' <?php echo ($data["ORG"] == "UG Lines" ? "selected=selected" : "")?>>UG Lines</option>
						</select>
					</div>
				</td>
				<td style = 'text-align: left; border-bottom: 1px solid black; font-weight: bold; ' rowspan = '1'>
					RSD #
					<input type = 'text' name = 'rsd' id='rsd' maxlength = '20' style = 'width: 100px;' value = '<?php echo $data["RSD"]?>'/>
				</td>
				<td style = 'text-align: left; border-bottom: 1px solid black; font-weight: bold;' >
					Location &nbsp;
				</td>
				<td style = 'text-align: left; border-bottom: 1px solid black; width: 100px;'>
					<select name = 'location' class = 'select' id='location' onchange='sot.redtag.switchCreditMonth();'>
						<option></option>
						<?php echo $locations; ?>
					</select>
				</td>
			</tr>
			<tr>
				<td colspan = '5' style = 'border-bottom: 1px solid black; text-align: left;  font-weight: bold;'>
					Protection Leader(s) Name / ID
					<input type = 'text' name = 'protection_leader' maxlength = '250' style = 'width: 590px;' value = '<?php echo $data["PROTECTION_LEADER"]?>'/>
				</td>
			</tr>
			<tr>
				<td colspan = '5' style = 'border-bottom: 1px solid black; text-align: left;  font-weight: bold;'>
					Operating Authority
					<input type = 'text' name = 'operating_authority' maxlength = '495' style = 'width: 655px;' value = '<?php echo $data["OPERATION_AUTHORITY"]?>'/>
				</td>
			</tr>
			<tr>
				<td colspan = '5' style = 'border-bottom: 1px solid black; text-align: left;  font-weight: bold;'>
					Auditor(s) ID / Name / Signatures(s)
					<input type = 'text' name = 'auditor' maxlength = '250' style = 'width: 555px;' value = '<?php echo $data["AUDITORS"]?>''/>
				</td>
			</tr>
			<tr>
				<td colspan = '5' style = 'border-bottom: 1px solid black; text-align: left;  font-weight: bold;'>
					Location/Description of Equipment Shut Down
					<input type = 'text' name = 'loc_desc_equip' maxlength = '495' style = 'width: 490px;' value = '<?php echo $data["LOCATION_DESC_EQP_SHUTDOWN"]?>'/>
				</td>
			</tr>
			<tr>
				<td colspan = '5' style = 'border-bottom: 1px solid black; text-align: left;  font-weight: bold;'>
					Nature of Work
					<input type = 'text' name = 'nature_work' maxlength = '495' style = 'width: 685px;' value = '<?php echo $data["NATURE_OF_WORK"]?>'/>
				</td>
			</tr>
			<tr style = 'height: 20px;'>
				<td colspan = '3' style = 'height: 20px; text-align: left;  font-weight: bold;'>
					Date of Audit
					<input type = 'text' name = 'date' onkeydown='return false;' maxlength = '10' value = '<?php echo $data["AUDIT_DATE"]?>'/> <img src = '../src/img/calendar.gif' id = 'cal' style = 'vertical-align: bottom;' onmouseover = 'sot.redtag.cal("cal", "date")'/>
				</td>
				<td colspan = '1' style = 'text-align: right; height: 20px;  font-weight: bold;'>
					Credit Month
				</td>
				<td style = 'height: 25px;'>
					<div class = 'container' >
						<select name = 'credit_month' id='credit_month' class = 'select' onchange='sot.redtag.switchCreditMonth();'>
						   <option value="1" <?php echo ($data["CREDIT_MONTH"] == "1" ? "selected=selected" : "")?>> January
						   <option value="2" <?php echo ($data["CREDIT_MONTH"] == "2" ? "selected=selected" : "")?>> February
						   <option value="3" <?php echo ($data["CREDIT_MONTH"] == "3" ? "selected=selected" : "")?>> March
						   <option value="4" <?php echo ($data["CREDIT_MONTH"] == "4" ? "selected=selected" : "")?>> April
						   <option value="5" <?php echo ($data["CREDIT_MONTH"] == "5" ? "selected=selected" : "")?>> May
						   <option value="6" <?php echo ($data["CREDIT_MONTH"] == "6" ? "selected=selected" : "")?>> June
						   <option value="7" <?php echo ($data["CREDIT_MONTH"] == "7" ? "selected=selected" : "")?>> July
						   <option value="8" <?php echo ($data["CREDIT_MONTH"] == "8" ? "selected=selected" : "")?>> August
						   <option value="9" <?php echo ($data["CREDIT_MONTH"] == "9" ? "selected=selected" : "")?>> September
						   <option value="10" <?php echo ($data["CREDIT_MONTH"] == "10" ? "selected=selected" : "")?>> October
						   <option value="11" <?php echo ($data["CREDIT_MONTH"] == "11" ? "selected=selected" : "")?>> November
						   <option value="12" <?php echo ($data["CREDIT_MONTH"] == "12" ? "selected=selected" : "")?>> December
						</select>
					</div>
				</td>
		</table>
	</div>
	<div style='width: 790px; color: red; text-align: right;' id='reminder'>
		&nbsp;
	</div>
	<div>
		<table>
		<?php

			$sql = "
				SELECT * FROM RED_TAG_ITEMS ORDER BY ITEM_NUM
			";
			
			while($row = $oci->fetch($sql)) {
				if($row["CATEGORY"] == "Interview - EM5-3 and Discipline Specific Procedure Comprehension:"){
					$checkbox = "<input type='checkbox' " . ($data_answers[$row["ITEM_NUM"]] == "YES" ? "checked=checked" : "" ) ." onclick='dojo.byId(\"{$row["ITEM_NUM"]}\").value= this.checked ? \"YES\" : \"NO\"'/>
								<input type='hidden' name='{$row["ITEM_NUM"]}' id='{$row["ITEM_NUM"]}' value='" . ($data_answers[$row["ITEM_NUM"]] == "YES" ? "YES" : "NO" ) ."'/>";
					$radios = "";
					$width = 770;
				} else {
					$radios = "
						<td style = 'width: 20px;'><input type = 'radio' name = '{$row["ITEM_NUM"]}' value = 'YES' " . ($data_answers[$row["ITEM_NUM"]] == 'YES' ? "checked=checked" : "") ."/></td>
						<td style = 'width: 20px;'><input type = 'radio' name = '{$row["ITEM_NUM"]}' value = 'NO' " . ($data_answers[$row["ITEM_NUM"]] == 'NO' ? "checked=checked" : "") ."/></td>
						<td style = 'width: 20px;'><input type = 'radio' name = '{$row["ITEM_NUM"]}' value = 'NA' " . ($data_answers[$row["ITEM_NUM"]] == 'NA' ? "checked=checked" : "") ."/></td>
					";
					$checkbox = "";
					$width = 705;
					
					if($row["CATEGORY"] != $prev) {
						echo "<tr class='" . ($x++ % 2 == 0 ? 'even' : 'odd') . "'>
							<td style = 'border-top: 1px solid black; font-weight: bold;'>&nbsp;Y</td>
							<td style = 'border-top: 1px solid black; font-weight: bold;'>&nbsp;&nbsp;N</td>
							<td style = 'border-top: 1px solid black; font-weight: bold;'>&nbsp;NA</td>
							<td style = 'border-top: 1px solid black;'>&nbsp;" . str_replace("\\", "", $row["CATEGORY"]) . "</td>
						</tr>";
					}
				}
				
				echo "
				<tr class='" . ($x++ % 2 == 0 ? 'even' : 'odd') . "'>
					{$radios}
					<td style = 'width: {$width}px; padding-left: 15px;'>{$checkbox}" . str_replace("\\", "", $row["ITEM_TEXT"]) . "</td>
				</tr>
				";
				if($row["ITEM_NUM"] == 23) {
					echo "
					</table>
					<table>
						<tr style = 'border-top: 1px solid black; background-color:" . ($x++ % 2 == 0 ? '#d8d8d8' : '#ffffff') .";'>
							<td style = 'border-top: 1px solid black; '>&nbsp;Review all documents for times and dates to see that steps were handled in the appropriate, chronological order.</td>
						</tr>
					";
				}
				
				$prev = $row["CATEGORY"];
			}
		?>
		</table>
	</div>
	<div>
		<table style='font-weight: bold;'>
			<tr>
				<td style = 'font-weight: normal;'>
					<b>Type of Audit (check all that apply)</b>
						<input type = 'checkbox' name = 'audit_monthly' <?php echo ($data["AUDIT_MONTHLY"] == "1" ? "checked=checked" : "")?>/>Monthly
						<input type = 'checkbox' name = 'audit_quarterly' <?php echo ($data["AUDIT_QUARTERLY"] == "1" ? "checked=checked" : "")?>/>Quarterly
						<input type = 'checkbox' name = 'audit_followup_recheck' <?php echo ($data["AUDIT_FOLLOWUP_RECHECK"] == "1" ? "checked=checked" : "")?>/>Follow-Up / Recheck
						<input type = 'checkbox' name = 'audit_onsite' <?php echo ($data["AUDIT_ON_SITE"] == "1" ? "checked=checked" : "")?>/>On-Site
						<input type = 'checkbox' name = 'audit_protection_authority' <?php echo ($data["AUDIT_PROTECTION_AUTHORITY"] == "1" ? "checked=checked" : "")?>/>Protection Authority
				</td>
			</tr>
			<tr>
				<td>
					Employees Interviewed: (Name / ID)
				</td>
			</tr>
			<tr>
				<td>
					<textarea name = 'employees_interviewed' onkeypress='return (this.value.length < 1000);'><?php echo str_replace("\\", "", $data["EMPLOYEES_INTERVIEW"]);?></textarea>
				</td>
			</tr>
			<tr>
				<td>
					Employees Supervisor(s): (Name / ID)
				</td>
			</tr>
				<tr>
				<td>
					<textarea name = 'employees_supervisors' onkeypress='return (this.value.length < 1000);'><?php echo str_replace("\\", "", $data["EMPLOYEES_SUPERVISORS"]);?></textarea>
				</td>
			</tr>		
			<tr>
				<td>
					Questions/Comments from Employees:
				</td>
			</tr>
			<tr>
				<td>
					<textarea name = 'questions_comments' onkeypress='return (this.value.length < 1000);'><?php echo str_replace("\\", "", $data["QUESTIONS_COMMENTS"]);?></textarea>
				</td>
			</tr>		
			<tr>
				<td>
					Findings:
				</td>
			</tr>
			<tr>
				<td>
					<textarea name = 'findings' onkeypress='return (this.value.length < 1000);'><?php echo str_replace("\\", "", $data["FINDINGS"]);?></textarea>
				</td>
			</tr>			
			<tr>
				<td>
					Recommendations:
				</td>
			</tr>
			<tr>
				<td>
					<textarea name = 'recommendations' onkeypress='return (this.value.length < 1000);'><?php echo str_replace("\\", "", $data["RECOMMENDATIONS"]);?></textarea>
				</td>
			</tr>			
			<tr>
				<td>
					Follow-Up / Resolution:
				</td>
			</tr>	
			<tr>
				<td>
					<textarea name = 'followup_resolution' onkeypress='return (this.value.length < 1000);'><?php echo str_replace("\\", "", $data["FOLLOWUP_RESOLUTION"]);?></textarea>
				</td>
			</tr>			
		</table>
	</div>
	<div style = 'font-size: 12px; margin-top: 5px; margin-bottom: 5px; text-align: left; width: 775px;'>
		<div style = 'font-weight: bold;'>
			Attached Images for Individual Crew Member Forms
		</div>
		<input type = 'hidden' id = 'a_count' name = 'a_count' value = '0'/>
		<?php
			$x = 0;
			foreach($attachments as $value){
				echo "<input type = 'hidden' name = 'deleted_{$x}' id = 'deleted_{$x}' value = '0'/>
				<input type = 'hidden' name = 'aid_{$x}' value = '{$value["ATTACHMENT_ID"]}'/>
				<div id = 'attachment_{$x}'>
					&bull;<a href = '#' target = '_blank' onclick = 'sot.redtag.viewAttachment({$value["ATTACHMENT_ID"]}); return false;'>{$value["A_NAME"]}</a>&nbsp;
					<span class = 'delete_a' style='cursor: pointer; font-size: 10px;' onclick = 'sot.redtag.removeAttachment({$x});'>
						[<img src=\"https://soteria.dteco.com/src/img/x.png\" style='width: 10px; height: 10px; vertical-align: bottom;' /> <a href='#' onclick='return false;'>Remove</a>]
					</span>
				</div>";

				$x++;
				
				echo "<script type = 'text/javascript'>
						a_count++;
					</script>
				";
			}

		?>
		<div id = 'attachments' style='width: 775px;'></div>
		<div style='margin-top: 5px; font-size: 10px;'>
			[ <a href='#' onclick='sot.redtag.addAttachment(); return false;'>Add Attachment / Image</a> ]
		</div>
	</div>
	<div>
		<input type = 'hidden' name = 'id' value = '<?php echo $_GET["id"]?>'/>
		<?php if(!empty($_GET["returnto"])) { echo "<input type='hidden' name='returnto' value='{$_GET["returnto"]}'/>"; } ?>
		<input type = 'submit' value = 'Save' name = 'save' style = 'border: 1px solid black; cursor: pointer;' onclick = 'sot.tools.cover(true); return true;'/>
		<input type = 'submit' value = 'Save & Submit' name = 'submit' style = 'border: 1px solid black; cursor: pointer;' onclick = 'return sot.redtag.onSubmit();'/>
		<?php $returnto = "index"; if(!empty($_GET["returnto"])) { $returnto = $_GET["returnto"]; } ?>
		<button style='margin-left: 5px;' onclick='window.location="../<?php echo $returnto ?>.php<?php echo (!empty($_GET["delegate"]) ? "?delegate={$_GET["delegate"]}" : ""); ?>"; return false;'>Cancel & Go Back</button>
	</div>
</form>
	<iframe name='submit' style='width:0px; height:0px;'></iframe>	