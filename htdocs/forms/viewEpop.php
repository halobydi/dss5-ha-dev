<?php
	function errorHandler($errno, $errstr, $errfile, $errline, $errcontext){	
	}
	set_error_handler('errorHandler');
	require_once('mcl_Html.php');
	require_once('mcl_Oci.php');
	require_once('../src/php/auth.php');
	
	$baseDir = "https://" . $_SERVER['SERVER_NAME'] . ":" . $_SERVER['SERVER_PORT'];
	
	mcl_Html::s(mcl_Html::INC_CSS, "../src/css/form.css");
	mcl_Html::title('View Enterprise Performance Observation');
	
	$epopId = $_GET["epopid"];
	if(empty($epopId)){
		die('An observation ID was not found!');
	}
	
	$sql = "SELECT
					EPOP_OBSERVATIONS.*,
					EMPLOYEE,
					NAME,
					TO_CHAR(OBSERVED_DATE, 'MM/DD/YYYY') AS OBSERVED_DATE,
					COMPLETED_BY,
					OBSERVATION_CREDIT,
					COMMENTS,
					LOCATION,
					CASE WHEN COMPLETED_DATE < TO_DATE('08/30/2014', 'MM/DD/YYYY') AND CONVERSATION_TOOK_PLACE = 0 THEN 'NA' ELSE DECODE(CONVERSATION_TOOK_PLACE, 1, 'Yes', 'No') END AS CONVERSATION,
					CASE WHEN COMPLETED_DATE < TO_DATE('08/30/2014', 'MM/DD/YYYY') AND RECOGNIZED_GOOD_BEHAVIOR = 0 THEN 'NA' ELSE DECODE(RECOGNIZED_GOOD_BEHAVIOR, 1, 'Yes', 'No') END AS GOOD_BEHAVIOR
			FROM	EPOP_OBSERVATIONS
			WHERE	EPOP_ID = {$epopId}
	";
	
	$oci = new mcl_Oci("soteria");
	$row = $header = $oci->fetch($sql);
	//echo "<pre>"; var_dump($header); echo "</pre>";
	$comments = $row["COMMENTS"];
	$delegate = "";
	if($row["OBSERVATION_CREDIT"] != $row["COMPLETED_BY"] && !empty($row["COMPLETED_BY"])) {
		$delegate = "<span style='color: red; font-size: 10px;'>Observation submitted by a delegate</span>";
	}
	
	$observedBy = @mcl_Ldap::lookup($row["OBSERVATION_CREDIT"]);
	
	if(empty($observedBy)){
		$observedBy = $row["OBSERVATION_CREDIT"];
	} else {
		$observedBy = $observedBy["fname"];
	}

?>
<script src="js/jquery.js"></script>
<div style='text-align; right; margin-top: 5px; width: 800px;' id='print' onclick='window.print(); return false;'>
	<div style='text-align:right;'>
		<img src='../src/img/print.png'/> <a href='#' onclick='return false;' style=''>Print </a>
	</div>
</div>
<div class='header'>
	Enterprise Performance Observation
</div>
<table>
	<tr>
		<td style='width:150px; font-weight:bold; text-align: left;'>Employee Username:</td>
		<td style='width:250px;  text-align: left;'><input type='text' onkeydown='return false;' value="<?php echo $row["EMPLOYEE"]; ?>"/></td>
		<td style='width:150px; font-weight:bold;  text-align: left;'>Date Observed:</td>
		<td style='width:250px;  text-align: left;'>
			<input type='text' onkeydown='return false;' value="<?php echo $row["OBSERVED_DATE"]; ?>"/>
		</td>
	</tr>
	<tr>
		<td style='width:150px; font-weight:bold;  text-align: left;'>Employee Name: </td>
		<td style='width:250px;  text-align: left;'><input type='text' onkeydown='return false;' value="<?php echo $row["NAME"]; ?>"/></td>
		<td style='width:150px; font-weight:bold;  text-align: left;'>Observed By:</td>
		<td style='width:250px;  text-align: left;'><input type='text' onkeydown='return false;' value="<?php echo $observedBy;  ?>"/><br/><?php echo $delegate; ?></td>
	</tr>
	<tr>
		<td colspan='4' style='text-align: right;'></td>
	</tr>
	<tr id='location_c' style=''>
		<td style='width:147px; font-weight:bold; text-align: left;'>Location:</td>
		<td colspan='3' style='text-align: left;'>
			<input type='text' maxlength='350'  style='width: 604px;' id='location' name='location' value="<?php echo $row["LOCATION"]; ?>"/>
		</td>
	</tr>
</table>
<table>
<?php

	$yesNoQuestions = array();
	$sql = "SELECT * FROM LIFE_CRITICAL";
	while($row = $oci->fetch($sql)) {
		$yesNoQuestions[$row['LC_CODE']] = $row["LC_TEXT"];
	}

	$sql = "
		WITH T AS(
			SELECT
				I.ITEM_NUM,
				I.ITEM_TEXT,
				nvl(lc_text, item_category) as item_category,
				HAS_SUBITEMS,
				ANSWER
			FROM
				EPOP_ANSWERS A
			join
					EPOP_ITEMS I
					on A.ITEM_NUM = I.ITEM_NUM
				left join life_critical
					on life_critical.lc_code = item_category
			WHERE
				EPOP_ID = {$epopId}
	)
	SELECT
		T.*,
		C.COMMENTS
	FROM
		T
	LEFT JOIN
		EPOP_COMMENTS C
		ON C.EPOP_ID = {$epopId}
		AND C.ITEM_NUM = T.ITEM_NUM
	ORDER BY
		ITEM_CATEGORY,
		T.ITEM_NUM
	";

//echo "<pre>" . $sql . "</pre>";
	
	$prev_category = '';
	$firstpass = true;
	
	$stmt_sub = $oci->parse("
		SELECT 	SI.SUBITEM_NUM,
				SI.SUBITEM_TEXT,
				ANSWER
		FROM	EPOP_ANSWERS A,
				SWO_SUBITEMS SI
		WHERE	EPOP_ID = {$epopId}
				AND A.ITEM_NUM = SI.SUBITEM_NUM
				AND SI.PARENT_ITEM_NUM = :parent_item_num
	");

	while($row = $oci->fetch($sql)){
		if($row["ITEM_CATEGORY"] != $prev_category){
			$x = 0;
			
			if(!$firstpass){
				echo "<tr style='height:30px;'>
					<td colspan=4></td>
				</tr>";
			}
			echo "<tr style='font-weight:bold; background-color:#f0f0f0;'>
					<td style='width:710px; text-align:center; padding: 5px; font-weight: normal; border-top: 1px solid #000; border-bottom: 1px solid #000;'>{$row["ITEM_CATEGORY"]}</td>
					<td style='width:30px; text-align:center; font-weight: normal; border-top: 1px solid #000; border-bottom: 1px solid #000;'>S</td>
					<td style='width:30px; text-align:center; font-weight: normal; border-top: 1px solid #000; border-bottom: 1px solid #000;'>IO</td>
					<td style='width:30px; text-align:center; font-weight: normal; border-top: 1px solid #000; border-bottom: 1px solid #000;'>NA</td>
				</tr>";
			$firstpass = false;
		}
		
		echo "<tr class='" . ($x++ % 2 == 0 ? 'even' : 'odd') . "'>
					<td style='width:710px; text-align:left; padding-top: 3px; padding-bottom: 3px;'>
						{$row["ITEM_TEXT"]}
						" . (!empty($row["COMMENTS"]) ? "</br><i>{$row["COMMENTS"]}</i>" : "") ."
					</td>
					<td style='width:30px; text-align:center; vertical-align:top; border-left: 1px solid #dedede;'>" . ($row["ANSWER"] == "S" ? "<img style='padding: 3px;' src='../src/img/check.gif'/>" : ""). "</td>
					<td style='width:30px; text-align:center; vertical-align:top; border-left: 1px solid #dedede;'>" . ($row["ANSWER"] == "IO" ? "<img style='padding: 3px;' src='../src/img/check.gif'/>" : ""). "</td>
					<td style='width:30px; text-align:center; vertical-align:top; border-left: 1px solid #dedede;'>" . ($row["ANSWER"] == "NA" ? "<img style='padding: 3px;' src='../src/img/check.gif'/>" : ""). "</td>
				</tr>
			";
			
		$prev_category = $row["ITEM_CATEGORY"];
		
		if($row["HAS_SUBITEMS"] == "1"){
	
			$oci->bind($stmt_sub, array(
				":parent_item_num" => $row["ITEM_NUM"]
			));
			while($row2 = $oci->fetch($stmt_sub, false)){
				echo "<tr class='" . ($x++ % 2 == 0 ? 'even' : 'odd') . "'>
						<td style='width:690px; text-align:left; padding-top: 3px; padding-bottom: 3px;  padding-left: 20px;'>
							<div>&bull; {$row2["SUBITEM_TEXT"]}</div>
						</td>
						<td style='width:30px; text-align:center; vertical-align:top; border-left: 1px solid #dedede;'>" . ($row2["ANSWER"] == "S" ? "<img style='padding: 3px;' src='../src/img/check.gif'/>" : ""). "</td>
						<td style='width:30px; text-align:center; vertical-align:top; border-left: 1px solid #dedede;'>" . ($row2["ANSWER"] == "IO" ? "<img style='padding: 3px;' src='../src/img/check.gif'/>" : ""). "</td>
						<td style='width:30px; text-align:center; vertical-align:top; border-left: 1px solid #dedede;'>" . ($row2["ANSWER"] == "NA" ? "<img style='padding: 3px;' src='../src/img/check.gif'/>" : ""). "</td>
					</tr>
				";

			}
		}	
	}
?>
</table>
<?php if(true): ?>
<script>
	function run(){
		var qew = $("td:contains('Qualified Electrical Worker')");
		if (qew.length > 0){
			$("#qewAnswer").html("Yes")
		} else {
			$("#qewAnswer").html("No")
		}
	}
	setTimeout(run, 500);
</script>
<table style='width: 815px;'>
	<tr>
	<td style='font-weight:bold;vertical-align: bottom; text-align: left;'>
		Would you like to also complete a QEW observation with this submission?
	</td>
	</tr>
	<tr>
		<td style='text-align: left;'>
			<div id="qewAnswer"></div>
		</td>
	</tr>
	<tr>
		<td style='font-weight:bold;vertical-align: bottom; text-align: left;'>
			Did a conversation take place with the observed individual?
		</td>
	</tr>
	<tr>
		<td style= 'text-align: left;'>
			<?=$header['CONVERSATION']?>
		</td>
	</tr>
	<tr>
		<td style='font-weight:bold;vertical-align: bottom; text-align: left;'>
			Was good safety behavior recognized during this observation?
		</td>
	</tr>
	<tr>
		<td style='text-align: left;'>
			<?=$header['GOOD_BEHAVIOR']?>
		</td>
	</tr>
</table>
<?php else: ?>
	<table style='width: 815px;'>
		<tr>
			<td style='font-weight:bold;vertical-align: bottom; text-align: left;'>
				Is there a procedure for the work being performed?
			</td>
		</tr>
		<tr>
			<td style= 'text-align: left;'>
				<?=$header['PROCEDURE_FOR_WORK'] ? "Yes - " . $header['PROCEDURE_FOR_WORK_DETAIL']  : "No"?>
			</td>
		</tr>
		<?php foreach($yesNoQuestions as $key=>$value): ?>
			<?php if(!$header[$key]) continue; ?>
			<tr>
				<td style='font-weight:bold;vertical-align: bottom; text-align: left;'>
					<?=$value?>
				</td>
			</tr>
			<tr>
				<td style= 'text-align: left;'>
					<?=$header[$key] ? "Yes"  : "No"?>
				</td>
			</tr>
		<?php endforeach; ?>
	</table>
<?php endif; ?>
<table>
	<tr>
		<td style='font-weight:bold;vertical-align: bottom; text-align: left;'>Photos</td>
	</tr>
	<tr>
		<td style='width: 815px;'>
		<?php
			require_once("/opt/apache/servers/soteria/htdocs/src/php/photos.php");
			getPhotos('EPOP', empty($epopId) ? 0 : $epopId, $oci, false);
		?>
		</td>
	</tr>
</table>

<table>
	<tr>
		<td style='font-weight:bold;vertical-align: bottom; text-align: left;'>Additional Comments</td>
	</tr>
	<tr>
		<td><textarea style='width:810px;' name='comments' onkeydown='return false;'><?php echo str_replace("\\", "", $comments); ?></textarea></td>
	</tr>
	<!--
	<tr>
		<td style='font-weight:bold;vertical-align: bottom; text-align: left;'>Improvement Opportunity / Situation</td>
	</tr>
	<tr>
		<td><textarea style='width:810px;' name='comments' onkeydown='return false;'><?php echo str_replace("\\", "", $header['SITUATION']); ?></textarea></td>
	</tr>
	<tr>
		<td style='font-weight:bold;vertical-align: bottom; text-align: left;'>Actions</td>
	</tr>
	<tr>
		<td><textarea style='width:810px;' name='comments' onkeydown='return false;'><?php echo str_replace("\\", "", $header['ACTIONS']); ?></textarea></td>
	</tr>
	<tr>
		<td style='font-weight:bold;vertical-align: bottom; text-align: left;'>Results</td>
	</tr>
	<tr>
		<td><textarea style='width:810px;' name='comments' onkeydown='return false;'><?php echo str_replace("\\", "", $header['RESULTS']); ?></textarea></td>
	</tr>
	-->
</table>
