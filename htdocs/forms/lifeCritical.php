<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->

    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,minimum-scale=1,user-scalable=no""/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>

    <link rel="stylesheet" type="text/css" href="Content/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="Content/app/Styles.css">
    <link rel="stylesheet" type="text/css" href="Content/app/DTELogo.css">
    <link rel="stylesheet" type="text/css" href="Content/app/Header.css">
</head>
<body>
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $(window).load(function(){
            $("#personTypeSelect").val(0);
            adjustButtons();
        });
        $(document).on('change', "#personTypeSelect", function () {
            togglePersonType();
        });


        function detectMob() {
            if( navigator.userAgent.match(/Android/i)
                || navigator.userAgent.match(/webOS/i)
                || navigator.userAgent.match(/iPhone/i)
                || navigator.userAgent.match(/iPad/i)
                || navigator.userAgent.match(/iPod/i)
                || navigator.userAgent.match(/BlackBerry/i)
                || navigator.userAgent.match(/Windows Phone/i)
            ){
                return true;
            }
            else {
                return false;
            }
        }
        function adjustButtons(){
            if (detectMob()){
                $("#markAllSatisfactoryBtn").addClass("btn-lg");
                $("#cancelAndGoBackBtn").addClass("btn-lg");
            }
        }


        function toggleHidden (onOrOff) { // TRUE for ON, FALSE for OFF
            if (onOrOff) {
                if ($("#idenDiv").hasClass("hidden")) {
                    $("#idenDiv").removeClass("hidden");
                }
            } else {
                if (!$("#idenDiv").hasClass("hidden")) {
                    $("#idenDiv").addClass("hidden");
                }
            }
        };

        function togglePersonType(){
            var t = $("#personTypeSelect");
            var idenVisible = false;
            var tVal = t.val();
            if (tVal == 0) {
                $("#empNameLabel").html("Employee Name:");
                toggleHidden(true);
                $("#empName").removeClass("hidden");
            } else if (tVal == 1) {
                $("#empNameLabel").html("Name (Optional):");
                toggleHidden(false);
                $("#empName").addClass("hidden");
            } else if (tVal == 2) {
                $("#empNameLabel").html("Contractor Name:");
                toggleHidden(false);
                $("#empName").removeClass("hidden");
            }
            if (idenVisible) {
                document.getElementById("usid_1").required = true;
            } else {
                document.getElementById("usid_1").required = false;
            }
        };
    });
</script>
<script>
    <?php
    $uri = "?type=office";
    if (!empty($_REQUEST['usid'])){
        $uri = $uri . "&usid=" . $_GET["usid"];
    }
    if (!empty($_REQUEST['delegate'])){
        $uri = $uri . "&delegate=" . $_GET["delegate"];
    }

    ?>
    function openOfficeForm(forMobile) {
        if (forMobile){
            window.location.assign("../mobile/forms/epop.php<?=$uri?>");
        } else {
            window.location.assign("epop.php<?=$uri?>");
        }
    }
</script>

<?php
function errorHandler($errno, $errstr, $errfile, $errline, $errcontext){

}
set_error_handler('errorHandler');
require_once('mcl_Html.php');
require_once('mcl_Oci.php');
require_once("/opt/apache/servers/soteria/htdocs/src/php/auth.php");
$user = auth::check();
$login_warning = '';
if($user["status"] != "authorized"){
    $login_warning = '<b>Warning</b>: You are not logged into the application. Submitting an observation without logging in will result in an anonymous observation. <a href="../index.php">Click here</a> to log in.';
}

mcl_Html::title("SOTeria - Enterprise Performance Observation");

mcl_Html::js(mcl_Html::DOJO);
mcl_Html::js(mcl_Html::DOJO_WINDOW);
mcl_Html::js(mcl_Html::AJAX);
mcl_Html::js(mcl_Html::CALENDAR);

mcl_Html::no_cache(true);
mcl_Html::s(mcl_Html::INC_JS, "../src/sot.js");

//mcl_Html::css(mcl_Html::CALENDAR);
mcl_Html::s(mcl_Html::INC_CSS, "{$baseDir}/src/css/jscal2.css");
mcl_Html::s(mcl_Html::INC_CSS, "../src/css/form.css");

$type = $_GET['type'] == 'office' ? 'office' : 'field';
$switchType = $_GET['type'] == 'office' ? 'field' : 'office';


if($_GET["print"] == true) {
    mcl_Html::s(mcl_Html::SRC_JS, "
  window.onload = function() {
    window.print();
    window.close();
  }
");

    $form = "Safe Worker Observation - Life Critical";
    if($_GET["pp"] == true) {
        $form = "Paired Performance Observation";
    } else if($_GET["qew"] == true) {
        $form = "Qualified Electrical Worker Observation";
    }

    echo <<<DIV
  <div id='print' style='width: 100%; height: 100px; font-size: 26px; text-align: center;'/>
    {$form}
  </div>
DIV;
}

$oci = new mcl_Oci('soteria');

$sql = "SELECT * FROM ORGANIZATIONS ORDER BY ORG_CODE";

$orgs = array();
while($row = $oci->fetch($sql)){
    $orgs[$row["ORG_CODE"]] = array(
        "code"    => $row["ORG_CODE"],
        "title"   => $row["ORG_TITLE"]
    );
}

function debug_to_console($data) {
    if(is_array($data) || is_object($data))
    {
        echo("<script>console.log('PHP: ".json_encode($data)."');</script>");
        if(is_array($data))
        {
            echo("<script>console.log('PHP: ".count($data)."');</script>;");
        }
    } else {
        echo("<script>console.log('PHP: ".$data."');</script>");
    }
}


$yesNoQuestions = array();
echo "<script>console.log(\"Getting life critical questions\");</script>";
$sql = "SELECT * FROM LIFE_CRITICAL ORDER BY SORT_ORDER";
while($row = $oci->fetch($sql)) {
    $yesNoQuestions[$row['LC_CODE']] = $row["LC_TEXT"];
}

debug_to_console($yesNoQuestions);

$category = "";
$swo = false;
$qew = false;
$pp = false;

if(isset($_GET["swo"])){ $swo = true; }
if(isset($_GET["qew"]) && !$swo){ $qew = true; }
if(isset($_GET["pp"])){ $pp = true; }
if(!$pp && !$qew){ $swo = true; }
if($swo){ $category .= "'Leader', 'Individual', 'Organization'"; }
if($qew){ $category .= (empty($category) ? "" : ", ") . "'Qualified Electrical Worker'"; }
if($pp) { $category .= (empty($category) ? "" : ", ") . "'Paired Performance'";}

if(!empty($_GET["usid"])){
    mcl_Html::s(mcl_Html::SRC_JS,"
      window.onload = function(){
        sot.swo.inputUsid(dojo.byId('usid_1'), 1);
      }
    ");
}

$goBack = $_GET['back'] ?: '../index.php';


mcl_Html::s(mcl_Html::SRC_JS,<<<JS
  function onClickYesNo(name, yes) {
    if(dojo.byId(name + '_required_comments')) {
      if(yes) {
        dojo.byId(name + '_required_comments').style.display = '';
      } else {
        dojo.byId(name + '_required_comments').style.display = 'none';
      }
    }
    if(dojo.byId(name + '_io_questions')) {
      if(yes) {
        dojo.byId(name + '_io_questions').style.display = '';
      } else {
        dojo.byId(name + '_io_questions').style.display = 'none';
      }
    }
    
    //This unchecks all sub questions when you select NO for the LC question
    var children = $("#" + name + "_io_questions input:radio:checked");
    if (!!children) {
      for (var i = 0; i < children.length ; i++) {
        children[i].checked = false;
       // children[i].className = "";
      }
    }
  
  }
  function toggleComments() {
    if (dojo.byId('conversation').value == 1) {
     // dojo.byId("comment_box").style.display = '';
      document.getElementById("comments").required = true;
    } else {
     // dojo.byId("comment_box").style.display = 'none';
      document.getElementById("comments").required = false;
    }
  }
JS
);
?>


<?php
$url = "lifeCritical.php?sid=" . time();
$url2 = "";
if($swo){ $url .= "&swo"; $url2 .= "&swo"; }
//if($qew){ $url .= "&qew"; $url2 .= "&qew";}
//if($pp){ $url .= "&pp"; $url2 .= "&pp";}

$completed_by = auth::check("usid");
if(!empty($_GET["delegate"])){
    $delegates = auth::check('delegates');
    if(!empty($delegates[$_GET["delegate"]]["name"])){
        $delegator = " for {$delegates[$_GET["delegate"]]["name"]}";
        $completed_by = $delegates[$_GET["delegate"]]["usid"];
    }

    echo "<div class='form_warning'>
      <b>Warning</b>: You are in delegate mode{$delegator}. This observation will be saved as completed by you, but your delegator will receive the leader completion credit. <a href='{$url}'>Click here</a> to turn off delegate mode.
    </div>";
}
if(!empty($login_warning)){
    echo "<div class='form_warning'>
      {$login_warning } 
    </div>";
}
?>

<div class="container">

    <div id="content" class="row well well-lg">


        <div class="well">
            <div class="col-md-3 hidden-sm hidden-xs">
                <div class="dte-logo">
                </div>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div>
                    <h4>Enterprise Performance Observation - Life Critical</h4>
                </div>
            </div>
            <div class="col-md-3 hidden-sm hidden-xs text-muted">
                <div class="icon-black text-right">
                    <b><?php echo ((auth::check("name") !== '') ? ("Welcome " . auth::check("name")) : ""); ?></b>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>


        <form role="form" data-toggle="validator" name='swo' id="form_swo"  action='../src/php/engine.php?f=saveEPOP<?php echo "{$url2}"; echo (!empty($_GET["delegate"]) ? "&delegate={$_GET["delegate"]}" : ""); ?>' method='POST' target='submit' onsubmit='return sot.swo.onSubmit();' enctype="multipart/form-data">

            <div class="col-md-6 form-horizontal">

                <div class="form-group">
                    <!--<div class="col-sm-8 col-sm-offset-4">-->
                    <label for="personTypeSelect" class="col-sm-4 control-label">Type:</label>
                    <div class="col-sm-8">
                        <select class="form-control" name='personType' id='personTypeSelect' required>
                            <option value='0'>Employee</option>
                            <option value='1'>Anonymous</option>
                            <option value='2'>Contractor</option>
                        </select>
                    </div>
                </div>

                <div id="idenDiv" class="form-group">
                    <label for="usid_1" class="col-sm-4 control-label">Employee Observed:</label>
                    <div class="col-sm-8">
                        <input  placeholder="User Name (e.g. u12345)"
                                class="form-control"
                                type='text'
                                maxlength='6'
                                id='usid_1'
                                name='usid[]'
                                autocomplete='off'
                                value="<?php echo $_GET["usid"]; ?>"
                                onkeyup='sot.swo.inputUsid(this, 1); return false;'
                                onchange='sot.swo.inputUsid(this, 1); return false;'
                                required />
                    </div>
                </div>

                <div id="empName" class="form-group">
                    <label id="empNameLabel" for="name_1" class="col-sm-4 control-label">Employee Name:</label>
                    <div class="col-sm-8">
                        <input placeholder="Full Name"
                               class="form-control"
                               type='text'
                               maxlength='350' id='name_1' name='name[]' value="<?php echo $name; ?>" />
                        <input type='hidden' id='nameValidate_1' value="<?php echo $name; ?>" />
                    </div>
                </div>

                <div class="form-group">
                    <label for="Organization" class="col-sm-4 control-label">Organization:</label>
                    <div class="col-sm-8">
                        <select class="form-control" name='org' id='org' onchange='sot.swo.switchOrg();'>
                            <option value=''></option>
                            <?php
                            foreach($orgs as $key=>$value){
                                $selected = ($org == $key ? 'selected=selected' : '');
                                echo "<option value='{$key}' {$selected}>{$value["title"]}</option>";
                            }
                            ?>
                        </select>
                    </div>
                </div>

            </div>

            <div class="col-md-6 form-horizontal">
                <div class="form-group">
                    <label for="location" class="col-sm-4 control-label">Location:</label>
                    <div class="col-sm-8">
                        <input class="form-control" type='text' id='location' name='location' value="" required="" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="observed_date" class="col-sm-4 control-label">Date Observed:</label>
                    <div class="col-sm-7">
                        <input class="form-control" type='text' id='observed_date' maxlength='10' onkeydown='if(window.event.keyCode != 9) { return false; }' name='observed_date' id='observed_date' value="<?php echo date("m/d/Y"); ?>" />

                    </div>
                    <div class="col-sm-1">
                        <img src='../src/img/calendar.gif' alt='' id="tcal" onmouseover="sot.swo.cal('tcal', 'observed_date');" required />
                    </div>
                </div>
                <div class="form-group">
                    <label for="observed_by" class="col-sm-4 control-label">Observed By:</label>
                    <div class="col-sm-7">
                        <input class="form-control" type='text' id='observed_by' name='observed_by' value="<?php echo (is_array(auth::check("name")) ? "" : auth::check("name"));?>" />
                        <input type='hidden' id='completed_by' name='completed_by' value="<?php echo $completed_by; ?>" />
                        <span class="error hide"><?php if(empty($_GET["delegate"])) { echo "Leave blank for anonymous"; } ?></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="office_field" class="col-sm-4 control-label">Observation Type:</label>
                    <div class="col-sm-7">
                        <?php if($swo): ?>
                        <b> <?=ucfirst($type)?> <b/> <a class="btn btn-primary hidden-sm hidden-xs" onclick='openOfficeForm(false)' id='switchType'>Switch to <?=ucfirst($switchType)?></a>
                            <a class="btn btn-primary hidden-md hidden-lg" onclick='openOfficeForm(true)' id='switchTypeMobile'>Switch to <?=ucfirst($switchType)?></a>
                            <input class="form-control" type='hidden' name='office_field' id='office_field' value='<?=strtoupper($type)?>' />
                            <?php else: ?>
                                <select class="form-control" name='office_field'>
                                    <option value='FIELD'>Field</option>
                                    <option value='OFFICE'>Office</option>
                                </select>
                            <?php endif; ?>
                            <span class="error hide">Must select a Observation Type!</span>
                    </div>
                </div>
            </div>
            <hr/>
            <div class="clearfix"></div>
            <div class="row">
                <img src="../src/img/lifeCriticalStandards.png" />
            </div>
            <br /><br />
            <div class="row">
                <script>
                    function doWork (){
                        sot.swo.markAllSat();
                        var naChecked = $("td:contains('there a PROC')").prev().children(":input")[0].checked;
                        if (!naChecked) {
                            var iden = $("td:contains('there a PROC')").prev().prev().prev().find("[class*='S']:first")[0].name;
                            sot.swo.markItem(iden, "S", true, true, 1, "", false, "*Required*  List the name of the procedure, Pre-Job Brief, SWI, JIT, etc.");
                        }
                    }

                </script>

                <button class="btn btn-success"
                        id="markAllSatisfactoryBtn"
                        onclick="doWork(); return false;"
                        type="button">  Mark All Satisfactory </button>

                <button class="btn btn-danger"  id="cancelAndGoBackBtn" onclick='window.location="<?=$goBack?><?php echo (!empty($_GET["delegate"]) ? "?delegate={$_GET["delegate"]}" : "");?>"; return false;' type="button"> Cancel & Go Back </button>
            </div>
            <div class="clearfix"></div>
            <center>
                <div class="panel-group">
                    <div class="panel-body">
                        <table>
                            <tr>
                                <td colspan='4' style='padding-bottom:10px; font-size: 11px;'>
                                    <b>S</b> - Satisfactory <b>
                                        IO
                                    </b> - Improvement Opportunity <b>
                                        NA
                                    </b> - Not Applicable
                                </td>
                            </tr>
                            <?php
                            if($swo){
                                echo "<input type='hidden' name='swo' value ='on'/>";
                            }
                            $sql = "
          SELECT
            *
          FROM
            EPOP_ITEMS
          WHERE
            ACTIVE = 1
            AND ITEM_CATEGORY IN({$category})
            AND {$type} = 1
            AND LIFE_CRITICAL = 1
          ORDER BY
            ITEM_CATEGORY DESC,
            ITEM_NUM
          ";
                            //echo "<pre>{$sql}</pre>";
                            $prevCategory = '';
                            $firstPass = true;
                            function printYesNoQuestion($name, $text, $requireComments = false) {
                                global $oci;
                                $sql = "SELECT * FROM EPOP_ITEMS WHERE ITEM_CATEGORY = '{$name}' ORDER BY ITEM_NUM";
                                $subItems = array();
                                while($row = $oci->fetch($sql)) {
                                    $subItems[] = $row;
                                }
                                $disabled = count($subItems) == 0 && $name != 'procedure_for_work';
                                $iden = "iden" . $name;
                                echo "
            <table>
            
            
             <tr style='font-weight:normal; background-color: " . ($disabled ? "#F0F0F0" : "#FFE019") . ";'>
                         <td colspan='3' style='padding-top: 3px; padding-bottom: 3px; padding-left: 7px; width: 90px; '>
             
                     <input style='vertical-align: sub;' type='radio' name='{$name}' id='{$name}' value='1' onclick='onClickYesNo(\"{$name}\", true)'/ " . ($disabled ? "disabled=disabled" : "") . "> Yes
                     &nbsp;<input style='vertical-align: sub;' type='radio' name='{$name}' id='{$name}' value='0' onclick='onClickYesNo(\"{$name}\", false)'/ " . ($disabled ? "disabled=disabled" : "") . "> No
                   </td>

                 <td colspan='2' style='padding-top: 6px; padding-bottom: 3px;width: 716px; font-weight: bold;'>{$text}</td>
             
                 </tr>
               
              ";
                                if($requireComments) {
                                    echo "<tr id='{$name}_required_comments' style='display: none;'>
                    <td colspan='5'>
                      <textarea name='{$name}_detail' id='{$name}_detail' style='width: 100%;' onkeypress='return sot.swo.imposeMaxLength(this, 1000);'></textarea>
                    </td>
                </tr>
              ";
                                }
                                echo "</table>";
                                $x = 0;
                                foreach($subItems as $row) {
                                    if($x == 0) {
                                        echo "<table id='{$name}_io_questions' style='display: none;'>";
                                        printHeader("Additional Questions for <i>" . $text . '</i>');
                                    }
                                    printQuestion($row, ++$x, true);
                                }
                                if($x > 0) { echo "</table>"; }
                            }
                            function printHeader($category) {
                                $style = "text-align:center; border-bottom: 1px solid #000; border-top: 1px solid #000; padding: 3px;";
                                echo "
              <tr style='font-weight:normal; background-color: #f0f0f0;'>
                <td style='width:30px; {$style}'>S</td>
                <td style='width:30px; {$style}'>IO</td>
                <td style='width:30px; {$style}'>NA</td>
                <td style='width:710px; {$style}'>{$category}</td>
                <td style='border-bottom: 1px solid #000; border-top: 1px solid #000; padding: 3px;'></td>
              </tr>
            ";
                            }
                            function printQuestion($row, $x, $lcPilot = '') {
                                if($row["HAS_SUBITEMS"] == '1'){
                                    $hasSubItems = 'true';
                                } else {
                                    $hasSubItems = 'false';
                                }
                                $class = ($x % 2 == 0 ? 'even' : 'odd');

                                $forceHideComments = 'true';
                                $customPlaceholder = "";
                                if (strpos($row["ITEM_TEXT"], 'for the work being observed') !== false) {
                                    $forceHideComments = 'false';
                                    $customPlaceholder = "*Required*  List the name of the procedure, Pre-Job Brief, SWI, JIT, etc.";
                                }

                                echo "
            <tr class='{$class}'>
              <td style='width:30px; text-align:center; vertical-align:top;'>
                <input lc='{$lcPilot}' type='radio' class='S' name='{$row["ITEM_NUM"]}' value='S' onclick='sot.swo.markItem({$row["ITEM_NUM"]}, \"S\", {$hasSubItems}, true, {$x}, \"{$lcPilot}\", {$forceHideComments}, \"{$customPlaceholder}\" );' />
              </td>
              <td style='width:30px; text-align:center; vertical-align:top;'>
                <input lc='{$lcPilot}' type='radio' class='IO' id = '{$row["ITEM_NUM"]}' name='{$row["ITEM_NUM"]}' value='IO' onclick='sot.swo.markItem({$row["ITEM_NUM"]}, \"IO\", {$hasSubItems}, true, {$x}, \"{$lcPilot}\", {$forceHideComments}, \"{$customPlaceholder}\" );' />
              </td>
              <td style='width:30px; text-align:center; vertical-align:top;'>
                <input lc='{$lcPilot}' type='radio' class='NA' name='{$row["ITEM_NUM"]}' value='NA' onclick='sot.swo.markItem({$row["ITEM_NUM"]}, \"NA\", {$hasSubItems}, true, {$x}, \"{ $lcPilot}\", true, \"{$customPlaceholder}\" );' />
              </td>
              <td style='width:710px;'>
                {$row["ITEM_TEXT"]}
              </td>
              <td style='text-align:right;'>
                <img src='../src/img/comments.png' id='{$row["ITEM_NUM"]}_comments_img' style='cursor:pointer; display: none;' alt='Add Comment' onclick='sot.swo.addComment({$row["ITEM_NUM"]}, this);'/>
              </td>
            </tr>
            <tr style=''>
              <td style='width:800px;' colspan='5' id='{$row["ITEM_NUM"]}_comments'></td>
              </td>
            </tr>
          ";
                                if($hasSubItems == 'true') {
                                    echo "
              <tr style='padding: 0px; margin: 0px;'>
                <td id='{$row["ITEM_NUM"]}_subitems'
                  colspan='5' style='width: 800px;
                  padding: 0px; margin: 0px;
                  background-color: #ffffca;'>
                </td>
              </tr>
            ";
                                }
                            }
                            while($row = $oci->fetch($sql)){
                                if($row["ITEM_CATEGORY"] != $prevCategory){
                                    $x = 0;
                                    if(!$firstPass){
                                        echo "
                <tr style='height:30px;'>
                  <td colspan=5></td>
                </tr>
              ";
                                    }
                                    printHeader($row['ITEM_CATEGORY']);
                                    $firstPass = false;
                                }
                                $x++;
                                printQuestion($row, $x);
                                $prevCategory = $row["ITEM_CATEGORY"];
                            }




                            echo "
                   <table>
                  <tr style=' background-color: #FFE019'>
                    <td  style='width:806px; text-align:center; border-bottom: 1px solid #000; border-top: 1px solid #000; padding: 3px;'>Life Critical</td>
                   </tr>
                  </table>";


                            foreach($yesNoQuestions as $key=>$value) {
                                printYesNoQuestion($key, $value);
                            }
                            ?>
                        </table>
                    </div>
                </div>
            </center>
            <div class="col-lg-8 col-lg-offset-2 col-md-12 col-sm-12 form-horizontal">
                <div class="form-group">
                    <label for="qworker" class="col-sm-8 control-label">
                        Would you like to also complete a <b>Qualified Electrical Worker</b> observation with this submission?
                    </label>
                    <div class="col-sm-4">
                        <select  class="form-control"
                                 onchange='sot.swo.addQewForLifeCritical(this.value == 1 ? true : false);'
                                 name='qew_add'>
                            <option value='0'>No</option>
                            <option value='1'>Yes</option>
                        </select>
                    </div>
                    <div id='qew' class="col-sm-12" style='display: none;'></div>
                </div>
                <div class="form-group">
                    <label for="conversation" class="col-sm-8 control-label"> Was <b>good safety behavior</b> recognized during this observation?</label>
                    <div class="col-sm-4">
                        <select id="goodBehaviorSelect" class="form-control" name='good_behavior' required>
                            <option value=''></option>
                            <option value='0'>No</option>
                            <option value='1'>Yes</option>
                        </select>
                        <span class="error hide">Must select conversation !</span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="conversation" class="col-sm-8 control-label"> Did a <b>conversation take place</b> with the observed individual?</label>
                    <div class="col-sm-4">
                        <select class="form-control" name='conversation' id='conversation' onchange='toggleComments();' required>
                            <option value=''></option>
                            <option value='0'>No</option>
                            <option value='1'>Yes</option>
                        </select>
                        <span class="error hide">Must select conversation !</span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="comment" class="col-md-2 control-label"></label>
                    <div class="col-md-12">
                        <table style=' width:100%' id='comment_box'>
                            <tr>
                                <td style='font-weight:bold;vertical-align: bottom;'>
                                    Comments
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <textarea class="form-control"  placeholder="*Required* Detailed conversation + any additional comments"  name='comments' id='comments' onkeypress='return sot.swo.imposeMaxLength(this, 1000);' ></textarea>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row form-horizontal">
                <div class="col-sm-12 form-group">
                    <button class="btn btn-primary" onclick='addPhoto(); return false;'>
                        <i class="icon-plus icon-black"></i>Add Photo
                    </button>
                    <button class="btn btn-primary" onclick='sot.swo.addEmployee(); return false;'>
                        <i class="icon-plus icon-black"></i>Add Additional Employee to Observation
                    </button>
                </div>
                <div class="col-sm-12 form-group">
                    <button class="btn btn-success btn-lg" type='submit' id='submit' value='Submit'>
                        Submit
                    </button>
                    <button class="btn btn-danger btn-lg" onclick='window.location="<?=$goBack?><?php echo (!empty($_GET["delegate"]) ? "?delegate={$_GET["delegate"]}" : ""); ?>"; return false;'>
                        Cancel & Go Back
                    </button>
                </div>
            </div>
            <br /><br />
            <div class="row">
                <div id='photos'>
                    <input class="form-control" type='hidden' id='p_count' name='p_count' value='0'>
                </div>
            </div>
            <div class="row">
                <div id='add_emp'></div>
            </div>
            <div class="row">
                <input  class="form-control" type ='hidden' value = '<?php echo $_GET["delegate"]; ?>' name = 'delegator'/>
                <input class="form-control" type ='hidden' value = '<?php echo date("m/d/Y H:i:s"); ?>' name = 'begin_date'/>
                <input class="form-control" type="hidden" id="lc_pilot" name="lc_pilot" value="1" />
            </div>
        </form>
    </div>
    <iframe name='submit' style='width:0px; height:0px;'></iframe>
</div>
<div id="footer">
    <div class="container" buttom">
    <hr/>
    <footer>
        <p>&copy; 2016 - Powered by DTE Engery</p>
    </footer>
</div>
</div>

</body>
</html>