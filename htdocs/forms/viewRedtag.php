<?php
	function errorHandler($errno, $errstr, $errfile, $errline, $errcontext){
		
	}
	set_error_handler('errorHandler');
	require_once('mcl_Html.php');
	require_once('mcl_Oci.php');
	require_once('mcl_Ldap.php');

	mcl_Html::title("SOTeria - Red Tag Audits");
		mcl_Html::js(mcl_Html::DOJO);
	mcl_Html::s(mcl_Html::INC_CSS, "../src/css/form.css");
	mcl_Html::s(mcl_Html::INC_JS, "../src/js/redtag.js");
	
	mcl_Html::s(mcl_Html::SRC_CSS, "
		input[type=text] {
			border: 	none;
		}
		textarea {
			width: 		805px;
		}
		
		td {
			text-align: left;
		}
	");
	

	$oci = new mcl_Oci("soteria");
		
	if(!empty($_GET["id"])){
		
		$id = $_GET["id"];
		$sql = "SELECT 	R.*, 
						TO_CHAR(R.AUDIT_DATE, 'MM/DD/YYYY') AS AUDIT_DATE, 
						TO_CHAR(R.COMPLETED_DATE, 'MM/DD/YYYY HH24:MI:SS') AS COMPLETED_DATE,
						TO_CHAR(TO_DATE(CREDIT_MONTH, 'MM'), 'MONTH') AS CREDIT_MONTH
				FROM 	RED_TAG_AUDITS R 
				WHERE 	RTA_ID = {$id}";
	
		$data = $oci->fetch($sql);
	
		$sql = "SELECT * FROM RED_TAG_ANSWERS WHERE RTA_ID = {$id}";
		
		$data_answers = array();
		while($row = $oci->fetch($sql)){
			$data_answers[$row["ITEM_NUM"]] = $row["ANSWER"];
		}
		
		$sql = "SELECT RTA_ID, ATTACHMENT_ID, NAME AS A_NAME, EXTENSION FROM RED_TAG_ATTACHMENTS 
			WHERE RTA_ID = {$id}";
		
		$attachments = array();
		while($row = $oci->fetch($sql)){
			$attachments[] = $row;
		}
		
	} else {
		die();
	}
	
	
	$completedby = @mcl_Ldap::lookup($data["COMPLETED_BY"]);
?>
<div style='text-align; right; margin-top: 5px; width: 820px;' onclick='window.print(); return false;'>
	<div style='text-align:right;'>
		<img src='../src/img/print.png'/> <a href='#' onclick='return false;' style=''>Print </a>
	</div>
</div>
<div class='header' style='width: 820px;'>
	Red Tag Audit Checklist
</div>
<div style='width: 825px; color: blue;'>
	Completed by <?php echo $completedby["fname"] ?> on <?php echo $data["COMPLETED_DATE"] ?>
</div>
<div class = 'container'>
	<table style='width: 820px;'>
		<tr>
			<td style = 'text-align: left; width: 100px; height: 20px; border-bottom: 1px solid black; font-weight: bold;' >
				Org. Audited &nbsp;
			</td>
			<td style = 'text-align: left; border-bottom: 1px solid black;'>
				<div class = 'container'>
					<select name = 'org' id='org' class = 'select'>
						<option value='<?php echo $data["ORG"]?>'><?php echo $data["ORG"]?></option>
					</select>
				</div>
			</td>
			<td style = 'text-align: left; border-bottom: 1px solid black; font-weight: bold; ' rowspan = '1'>
				RSD #
				<input type = 'text' name = 'rsd' maxlength = '20' style = 'width: 100px;' value = '<?php echo $data["RSD"]?>'/>
			</td>
			<td style = 'text-align: left; border-bottom: 1px solid black; font-weight: bold;' >
				Location &nbsp;
			</td>
			<td style = 'text-align: left; border-bottom: 1px solid black; width: 100px;'>
				<select name = 'location' class = 'select' id='location'>
					<option value='<?php echo $data["LOCATION"]?>'><?php echo $data["LOCATION"]?></option>
				</select>
			</td>
		</tr>
		<tr>
			<td colspan = '5' style = 'border-bottom: 1px solid black; text-align: left;  font-weight: bold;'>
				Protection Leader(s) Name / ID
				<input type = 'text' name = 'protection_leader' maxlength = '250' style = 'width: 590px;' value = '<?php echo $data["PROTECTION_LEADER"]?>'/>
			</td>
		</tr>
		<tr>
			<td colspan = '5' style = 'border-bottom: 1px solid black; text-align: left;  font-weight: bold;'>
				Operating Authority
				<input type = 'text' name = 'operating_authority' maxlength = '495' style = 'width: 655px;' value = '<?php echo $data["OPERATION_AUTHORITY"]?>'/>
			</td>
		</tr>
		<tr>
			<td colspan = '5' style = 'border-bottom: 1px solid black; text-align: left;  font-weight: bold;'>
				Auditor(s) ID / Name / Signatures(s)
				<input type = 'text' name = 'auditor' maxlength = '250' style = 'width: 555px;' value = '<?php echo $data["AUDITORS"]?>''/>
			</td>
		</tr>
		<tr>
			<td colspan = '5' style = 'border-bottom: 1px solid black; text-align: left;  font-weight: bold;'>
				Location/Description of Equipment Shut Down
				<input type = 'text' name = 'loc_desc_equip' maxlength = '495' style = 'width: 490px;' value = '<?php echo $data["LOCATION_DESC_EQP_SHUTDOWN"]?>'/>
			</td>
		</tr>
		<tr>
			<td colspan = '5' style = 'border-bottom: 1px solid black; text-align: left;  font-weight: bold;'>
				Nature of Work
				<input type = 'text' name = 'nature_work' maxlength = '495' style = 'width: 685px;' value = '<?php echo $data["NATURE_OF_WORK"]?>'/>
			</td>
		</tr>
		<tr style = 'height: 20px;'>
			<td colspan = '3' style = 'height: 20px; text-align: left;  font-weight: bold;'>
				Date of Audit
				<input type = 'text' name = 'date' onkeydown='return false;' maxlength = '10' value = '<?php echo $data["AUDIT_DATE"]?>'/> <img src = '../src/img/calendar.gif' id = 'cal' style = 'vertical-align: bottom;' onmouseover = 'sot.redtag.cal("cal", "date")'/>
			</td>
			<td colspan = '1' style = 'text-align: right; height: 20px;  font-weight: bold;'>
				Credit Month
			</td>
			<td style = 'height: 25px;'>
				<div class = 'container' >
					<select name = 'credit_month' id='credit_month' class = 'select' onselect='return false;'>
					   <option><?php echo ucfirst(strtolower($data["CREDIT_MONTH"])); ?></option>
					</select>
				</div>
			</td>
	</table>
</div>
<div style='width: 820px; color: red; text-align: right;' id='reminder'>
	&nbsp;
</div>
<div>
	<table>
	<?php

		$sql = "
			SELECT * FROM RED_TAG_ITEMS ORDER BY ITEM_NUM
		";
		
		while($row = $oci->fetch($sql)) {
			if($row["CATEGORY"] == "Interview - EM5-3 and Discipline Specific Procedure Comprehension:"){
				$checkbox = "<input type='checkbox' " . ($data_answers[$row["ITEM_NUM"]] == "YES" ? "checked=checked" : "" ) ." onclick='return false;'/>";
				$radios = "";
				$width = 803;
			} else {
				$radios = "
					<td style = 'width: 30px; text-align: center; vertical-align:top; border-right: 1px solid #dedede;'>" . ($data_answers[$row["ITEM_NUM"]] == 'YES' ? "<img src='../src/img/check.gif'/>" : "") . "</td>
					<td style = 'width: 30px; text-align: center; vertical-align:top; border-right: 1px solid #dedede;'>" . ($data_answers[$row["ITEM_NUM"]] == 'NO' ? "<img src='../src/img/check.gif'/>" : "") . "</td>
					<td style = 'width: 30px; text-align: center; vertical-align:top; border-right: 1px solid #dedede;'>" . ($data_answers[$row["ITEM_NUM"]] == 'NA' ? "<img src='../src/img/check.gif'/>" : "") . "</td>
				";
				$checkbox = "";
				$width = 705;
				
				if($row["CATEGORY"] != $prev) {
					echo "<tr class='" . ($x++ % 2 == 0 ? 'even' : 'odd') . "'>
						<td style = 'border-top: 1px solid black; font-weight: bold; text-align: center;'>Y</td>
						<td style = 'border-top: 1px solid black; font-weight: bold; text-align: center;'>N</td>
						<td style = 'border-top: 1px solid black; font-weight: bold; text-align: center;'>NA</td>
						<td style = 'border-top: 1px solid black;'>&nbsp;" . str_replace("\\", "", $row["CATEGORY"]) . "</td>
					</tr>";
				}
			}
			
			echo "
			<tr class='" . ($x++ % 2 == 0 ? 'even' : 'odd') . "'>
				{$radios}
				<td style = 'width: {$width}px; padding-left: 15px;'>{$checkbox}" . str_replace("\\", "", $row["ITEM_TEXT"]) . "</td>
			</tr>
			";
			if($row["ITEM_NUM"] == 23) {
				echo "
				</table>
				<table>
					<tr style = 'border-top: 1px solid black; background-color:" . ($x++ % 2 == 0 ? '#d8d8d8' : '#ffffff') .";'>
						<td style = 'border-top: 1px solid black; '>&nbsp;Review all documents for times and dates to see that steps were handled in the appropriate, chronological order.</td>
					</tr>
				";
			}
			
			$prev = $row["CATEGORY"];
		}
	?>
	</table>
</div>
<div style='width: 820px;'>
	<table style='font-weight: bold;'>
		<tr>
			<td style = 'font-weight: normal;'>
				<b>Type of Audit (check all that apply)</b>
					<input type = 'checkbox' onclick = 'return false'; name = 'audit_monthly' <?php echo ($data["AUDIT_MONTHLY"] == "1" ? "checked=checked" : "")?>/>Monthly
					<input type = 'checkbox' onclick = 'return false'; name = 'audit_quarterly' <?php echo ($data["AUDIT_QUARTERLY"] == "1" ? "checked=checked" : "")?>/>Quarterly
					<input type = 'checkbox' onclick = 'return false'; name = 'audit_followup_recheck' <?php echo ($data["AUDIT_FOLLOWUP_RECHECK"] == "1" ? "checked=checked" : "")?>/>Follow-Up / Recheck
					<input type = 'checkbox' onclick = 'return false'; name = 'audit_onsite' <?php echo ($data["AUDIT_ON_SITE"] == "1" ? "checked=checked" : "")?>/>On-Site
					<input type = 'checkbox' onclick = 'return false'; name = 'audit_protection_authority' <?php echo ($data["AUDIT_PROTECTION_AUTHORITY"] == "1" ? "checked=checked" : "")?>/>Protection Authority
			</td>
		</tr>
		<tr>
			<td>
				Employees Interviewed: (Name / ID)
			</td>
		</tr>
		<tr>
			<td>
				<textarea name = 'employees_interviewed' onkeydown='return false;'><?php echo str_replace("\\", "", $data["EMPLOYEES_INTERVIEW"]);?></textarea>
			</td>
		</tr>
		<tr>
			<td>
				Employees Supervisor(s): (Name / ID)
			</td>
		</tr>
			<tr>
			<td>
				<textarea name = 'employees_supervisors' onkeydown='return false;'><?php echo str_replace("\\", "", $data["EMPLOYEES_SUPERVISORS"]);?></textarea>
			</td>
		</tr>		
		<tr>
			<td>
				Questions/Comments from Employees:
			</td>
		</tr>
		<tr>
			<td>
				<textarea name = 'questions_comments' onkeydown='return false;'><?php echo str_replace("\\", "", $data["QUESTIONS_COMMENTS"]);?></textarea>
			</td>
		</tr>		
		<tr>
			<td>
				Findings:
			</td>
		</tr>
		<tr>
			<td>
				<textarea name = 'findings' onkeydown='return false;'><?php echo str_replace("\\", "", $data["FINDINGS"]);?></textarea>
			</td>
		</tr>			
		<tr>
			<td>
				Recommendations:
			</td>
		</tr>
		<tr>
			<td>
				<textarea name = 'recommendations' onkeydown='return false;'><?php echo str_replace("\\", "", $data["RECOMMENDATIONS"]);?></textarea>
			</td>
		</tr>			
		<tr>
			<td>
				Follow-Up / Resolution:
			</td>
		</tr>	
		<tr>
			<td>
				<textarea name = 'followup_resolution' onkeydown='return false;'><?php echo str_replace("\\", "", $data["FOLLOWUP_RESOLUTION"]);?></textarea>
			</td>
		</tr>			
	</table>
</div>
<div style = 'font-size: 12px; margin-top: 5px; margin-bottom: 5px; text-align: left; width: 820px;'>
	<div style = 'font-weight: bold;'>
		Attached Images for Individual Crew Member Forms
	</div>
	<?php
		$x = 0;
		foreach($attachments as $value){
			echo "<div id = 'attachment_{$x}'>
				&bull;<a href = '#' target = '_blank' onclick = 'sot.redtag.viewAttachment({$value["ATTACHMENT_ID"]}); return false;'>{$value["A_NAME"]}</a>&nbsp;
			</div>";

			$x++;
		}

	?>
</div>
