<?php
require_once("../src/php/require.php");
$oci = new mcl_Oci('soteria');

if(!empty($_POST)){

	$error = false;
	if(!empty($_POST["d_id"])){
		$sql = "
			DELETE FROM DELEGATES
			WHERE D_ID = {$_POST["d_id"]}
		";
		
		$oci->query($sql);
	} else {
	
		$lookup = @mcl_ldap::lookup(trim(strtolower($_POST["delegatee"])));
		if(!empty($lookup["fname"])) {
			$sql = "
				INSERT INTO DELEGATES VALUES(
					DELEGATE_UNIQUE.NEXTVAL,
					'" . auth::check('usid') . "',
					'" . trim(strtolower($_POST["delegatee"])) . "'
				)
			";
			
			if(!$oci->query($sql)){
				$error = true;
			}
		} else {
			$error = true;
			$msg = "Invalid employee username entered.";
		}
	}
	//Clean Post
	if(!$error){
		header("Location: delegates.php");
	}
}

if($error === true){
	$error = $oci->error();
	
	if(substr($error["message"], 0, 9) == 'ORA-12899'){
		$msg = "You can only use an employee usernames (ie. u12345) to add a delegate (max character length is 6).";
	} else if(substr($error["message"], 0, 9) == 'ORA-00001'){
		$msg = "Delegatee {$_POST["delegatee"]} already exists for Delegator {$usid}.";
	}
	
	echo "<div class = 'error' style = 'margin: 5px; width: 400px;'>Unable to add delegate. {$msg}</div>";
}

$sql = "
	SELECT  D_ID,
			DELEGATOR,
			NVL(E.NAME, DELEGATEE) AS DELEGATEE
	FROM	DELEGATES D
	LEFT JOIN EMPLOYEES E
		ON E.USID = D.DELEGATEE
	WHERE DELEGATOR = '" . auth::check('usid') . "'
	ORDER BY E.NAME
";
?>
<div style='margin: 5px; width: 500px; word-break: break-word;'>
	A Delegate is able to view all reports as their delegator and complete a SWO for their delegator.
</div>
<table class = 'tbl'>
	<form method = 'POST' id = 'delegates'>
		<th colspan = '1'><div class = 'inner'>Add Delegate</div></th>
		<tr class = 'odd'>
			<td style='border:none;width: 388px; text-align: left; padding-left: 5px;'>Delegatee (username) <input type = 'text' name='delegatee' maxlength='6'/>&nbsp;
			<input type = 'hidden' name = 'd_id' id = 'd_id'/>
			<input type = 'submit' value = 'Add' style = 'height: 18px; width: 60px;'/>
			</td>
		</tr>
	</form>
</table>

<table class = 'tbl'>
	<tr>
		<th><div style = 'width: 300px;'class = 'inner'>Delegatee</div></th>
		<th><div style = 'width: 80px;'class = 'inner'></div></th>
	</tr>

<?php

while($row = $oci->fetch($sql)){
	$style = "style = 'text-align: left;'";
	echo "<tr class = '" . ($x++ % 2 == 0 ? 'even' : 'odd') ."'>
			<td {$style}>{$row["DELEGATEE"]}</td>
			<td><a href = '#' onclick = 'if(confirm(\"Are you sure you want to delete this delegate?\\nThis action cannot be undone.\")){ dojo.byId(\"d_id\").value = \"{$row["D_ID"]}\"; dojo.byId(\"delegates\").submit();} return false;'>Delete</a></td>
		</tr>";
}

if($x == 0){
	echo "<tr>
			<td colspan=2>
				There are currently no delgates for " . auth::check("name") .".
			</td>
		</tr>";
}

?>
</table>