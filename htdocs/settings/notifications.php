<?php
require_once("../src/php/require.php");
$oci = new mcl_Oci('soteria');
mcl_Html::s(mcl_Html::INC_JS, "../src/js/other/jquery.mask.js");

if(!empty($_POST)){
	$error = false;
	
	$address = "";
	if($_POST["type"] == "C"){
		$address = "{$_POST["phone_number"]}@{$_POST["phone_carrier"]}";
	} else {
		$address = "{$_POST["email_address"]}";
	}
	
	if(substr($address, 0, 1) == "@" || substr($address, -1, 1) == "@" && $_POST["enabled"] == "on"){
		$error = true;
		if($_POST["type"] == "C"){
			$msg = "Phone number or phone carrier cannot be blank.";
		} else {
			$msg = "Email address or email provider cannot be blank.";
		}
	} else {	
		$address = str_replace("'", "''", $address);	
		$sql = "
			MERGE INTO NOTIFICATION_SETTINGS USING DUAL ON (USID = '" . auth::check('usid') . "')
			WHEN MATCHED THEN UPDATE
				SET ADDRESS = '{$address}',
					NOTIFY = " . (isset($_POST["enabled"]) && $_POST["enabled"] == "on" ? 1 : 0) .",
					TYPE = '" . ($_POST["type"] == "C" ? "C" : "E") ."'
				WHERE USID = '" . auth::check('usid') . "'
			WHEN NOT MATCHED THEN INSERT (
				USID,
				ADDRESS,
				NOTIFY,
				TYPE
			) VALUES(
				'" . auth::check('usid') . "',
				'{$address}',
				" . (isset($_POST["enabled"]) && $_POST["enabled"] == "on" ? 1 : 0) .",
				'" . ($_POST["type"] == "C" ? "C" : "E") ."'
			)
		";
		
		$oci->query($sql);
		$error = $oci->error();

	}
	
	if(!$error){
		header("Location: notifications.php");
	}
}

if($error === true){
	$error = $oci->error();
	//ORA-12899 to large 
	echo "<div class = 'error' style = 'margin: 5px; width: 300px;'>Unable to save notification setttings. {$msg}</div>";
}

$sql = "
	SELECT * FROM NOTIFICATION_SETTINGS WHERE USID = '" . auth::check("usid") ."'
";

if($row = $oci->fetch($sql)){
	$phone = explode("@", $row["ADDRESS"], 2);
	mcl_Html::s(mcl_Html::SRC_JS, <<<JS
		dojo.addOnLoad(function(){
			var type = "{$row["TYPE"]}";
			var phone1 = "{$phone[0]}";
			var phone2 = "{$phone[1]}";
			var email = "{$row["ADDRESS"]}";
			var notify = "{$row["NOTIFY"]}"
			dojo.byId("enabled").checked = notify == 1 ? true : false;
			if(type == "C"){
				dojo.byId('phone').style.display = '';
				dojo.byId('phone_number').value = phone1;
				dojo.byId('phone_carrier').value = phone2;
				dojo.byId('C').checked = true;
			} else if(type == "E"){
				dojo.byId('email').style.display = '';
				dojo.byId('email_address').value = email;
				
				dojo.byId('E').checked = true;
			}
			$(".phone").mask("999.999.9999", {placeholder:"#"});
		});
JS
	);
}

$row = $oci->fetch($sql);

?>

<div style='margin: 5px; width: 500px; word-break: break-word;'>
	Notifications are scheduled to be sent every Thursday at 1:00 PM to leaders who have not completed a SWO for the current week.
</div>
<form method = 'POST' id = 'notifications'>
	<table class='tbl'>
		<tr>
			<th colspan='2'>
				<div class = 'inner_title'>
					Notifications
				</div>
			</th>
		</tr>
		<tr>
			<th>
				<div class = 'inner' style='width: 150px;'>
					Enable
				</div>
			</th>
			<th style='text-align: left;'>
				<div class = 'inner' style='width: 300px;  text-align: left;'>
					Method
				</div>
			</th>
		</tr>
		<tr class='even'>
			<td style='border: none;'>
				<input type='checkbox' name='enabled' id='enabled'/>
			</td>
			<td style='text-align: left; border: none;'>
				<input type = 'radio' id='E' name='type' value='E' onclick='dojo.byId("email").style.display="block"; dojo.byId("phone").style.display="none";'> *Email 
				<input type = 'radio' id='C' name='type' value='C' onclick='dojo.byId("phone").style.display="block"; dojo.byId("email").style.display="none";'> Phone
			</td>
		</tr>
	</table>
	<table class='tbl' id='phone' style='display: none;'>
		<tr>
			<th colspan='2'>
				<div class = 'inner_title'>
					Contact Information
				</div>
			</th>
		</tr>
		<tr class='odd'>
			<td style='border: none; width: 150px;'>
				*Phone Number
			</td>
			<td style='text-align: left; border: none; width: 300px;'>
				<input type = 'text' name='phone_number' id='phone_number' class='phone' style='width: 200px;' value=''>
			</td>
		</tr>
		<tr class='even'>
			<td style='border: none; width: 150px;'>
				Phone Carrier
			</td>
			<td style='text-align: left; border: none; width: 307px;'>
				<select name='phone_carrier' id = 'phone_carrier' style='width: 207px;'>
					<option value=''></option>
					<option value='messaging.sprintpcs.com'>Sprint</option>
					<option value='vtext.com'>Verizon</option>
					<option value='txt.att.net'>AT&T</option>
					<option value='tmomail.net'>T-Mobile</option>
				</select>
			</td>
		</tr>
	</table>
	<table class='tbl' id='email' style='display: none;'>
		<tr>
			<th colspan='2'>
				<div class = 'inner_title'>
					Contact Information
				</div>
			</th>
		</tr>
		<tr class='odd'>
			<td style='border: none; width: 150px;'>
				<span style='color: red; font-weight: bold;'></span>Email Address
			</td>
			<td style='text-align: left; border: none; width: 307px;'>
				<input type = 'text' name='email_address' id='email_address' style='width: 200px;' value=''>
			</td>
		</tr>
	</table>
	<input type='submit' value='Save' style='width: 100px; margin-left: 5px;'/>
</form>
<div style='margin: 5px; width: 500px; word-break: break-word;'>
	*Email messages will contain more information then text messages (ie: the date when you last completed a SWO). Text messages will only contain a short reminder that you have not completed a SWO for the current week.
</div>
<div style='margin: 5px; width: 500px; word-break: break-word;'>
	*Phone number format xxxxxxxxxx 
</div>
